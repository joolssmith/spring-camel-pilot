
package com.flexnet.operations.webservices.n;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedAdvancedFmtLCResponseDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedAdvancedFmtLCResponseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedFulfillment" type="{urn:v1.webservices.operations.flexnet.com}failedAdvancedFmtLCDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedAdvancedFmtLCResponseDataType", propOrder = {
    "failedFulfillment"
})
public class FailedAdvancedFmtLCResponseDataType {

    @XmlElement(required = true)
    protected List<FailedAdvancedFmtLCDataType> failedFulfillment;

    /**
     * Gets the value of the failedFulfillment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedFulfillment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedFulfillment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedAdvancedFmtLCDataType }
     * 
     * 
     */
    public List<FailedAdvancedFmtLCDataType> getFailedFulfillment() {
        if (failedFulfillment == null) {
            failedFulfillment = new ArrayList<FailedAdvancedFmtLCDataType>();
        }
        return this.failedFulfillment;
    }

}
