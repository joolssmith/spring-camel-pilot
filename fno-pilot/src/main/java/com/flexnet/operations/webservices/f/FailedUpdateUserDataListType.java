
package com.flexnet.operations.webservices.f;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateUserDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateUserDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedUser" type="{urn:com.macrovision:flexnet/operations}failedUpdateUserDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateUserDataListType", propOrder = {
    "failedUser"
})
public class FailedUpdateUserDataListType {

    protected List<FailedUpdateUserDataType> failedUser;

    /**
     * Gets the value of the failedUser property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedUser property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedUser().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUpdateUserDataType }
     * 
     * 
     */
    public List<FailedUpdateUserDataType> getFailedUser() {
        if (failedUser == null) {
            failedUser = new ArrayList<FailedUpdateUserDataType>();
        }
        return this.failedUser;
    }

}
