
package com.flexnet.operations.webservices.k;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for splitLineItemInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="splitLineItemInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="numberOfCopies" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="targetTierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="targetOrganizationUnit" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType"/&gt;
 *         &lt;element name="targetContact" type="{urn:com.macrovision:flexnet/operations}userIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="matchingLineItemIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "splitLineItemInfoType", propOrder = {
    "lineItemIdentifier",
    "numberOfCopies",
    "targetTierName",
    "targetOrganizationUnit",
    "targetContact",
    "matchingLineItemIdentifier"
})
public class SplitLineItemInfoType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType lineItemIdentifier;
    @XmlElement(required = true)
    protected BigInteger numberOfCopies;
    @XmlElement(required = true)
    protected String targetTierName;
    @XmlElement(required = true)
    protected OrganizationIdentifierType targetOrganizationUnit;
    protected UserIdentifierType targetContact;
    protected EntitlementLineItemIdentifierType matchingLineItemIdentifier;

    /**
     * Gets the value of the lineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getLineItemIdentifier() {
        return lineItemIdentifier;
    }

    /**
     * Sets the value of the lineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.lineItemIdentifier = value;
    }

    /**
     * Gets the value of the numberOfCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * Sets the value of the numberOfCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCopies(BigInteger value) {
        this.numberOfCopies = value;
    }

    /**
     * Gets the value of the targetTierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetTierName() {
        return targetTierName;
    }

    /**
     * Sets the value of the targetTierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetTierName(String value) {
        this.targetTierName = value;
    }

    /**
     * Gets the value of the targetOrganizationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getTargetOrganizationUnit() {
        return targetOrganizationUnit;
    }

    /**
     * Sets the value of the targetOrganizationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setTargetOrganizationUnit(OrganizationIdentifierType value) {
        this.targetOrganizationUnit = value;
    }

    /**
     * Gets the value of the targetContact property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifierType }
     *     
     */
    public UserIdentifierType getTargetContact() {
        return targetContact;
    }

    /**
     * Sets the value of the targetContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifierType }
     *     
     */
    public void setTargetContact(UserIdentifierType value) {
        this.targetContact = value;
    }

    /**
     * Gets the value of the matchingLineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getMatchingLineItemIdentifier() {
        return matchingLineItemIdentifier;
    }

    /**
     * Sets the value of the matchingLineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setMatchingLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.matchingLineItemIdentifier = value;
    }

}
