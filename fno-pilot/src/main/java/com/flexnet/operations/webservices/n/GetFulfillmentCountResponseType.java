
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFulfillmentCountResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFulfillmentCountResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="responseData" type="{urn:v1.webservices.operations.flexnet.com}getFulfillmentCountResponseDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFulfillmentCountResponseType", propOrder = {
    "statusInfo",
    "responseData"
})
public class GetFulfillmentCountResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected GetFulfillmentCountResponseDataType responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link GetFulfillmentCountResponseDataType }
     *     
     */
    public GetFulfillmentCountResponseDataType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetFulfillmentCountResponseDataType }
     *     
     */
    public void setResponseData(GetFulfillmentCountResponseDataType value) {
        this.responseData = value;
    }

}
