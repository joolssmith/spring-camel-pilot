
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consolidateFulfillmentsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consolidateFulfillmentsRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillments" type="{urn:com.macrovision:flexnet/operations}fulfillmentIdentifierListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consolidateFulfillmentsRequestType", propOrder = {
    "fulfillments"
})
public class ConsolidateFulfillmentsRequestType {

    @XmlElement(required = true)
    protected FulfillmentIdentifierListType fulfillments;

    /**
     * Gets the value of the fulfillments property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierListType }
     *     
     */
    public FulfillmentIdentifierListType getFulfillments() {
        return fulfillments;
    }

    /**
     * Sets the value of the fulfillments property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierListType }
     *     
     */
    public void setFulfillments(FulfillmentIdentifierListType value) {
        this.fulfillments = value;
    }

}
