
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deletePartNumberDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deletePartNumberDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partNumberIdentifier" type="{urn:com.macrovision:flexnet/operations}partNumberIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deletePartNumberDataType", propOrder = {
    "partNumberIdentifier"
})
public class DeletePartNumberDataType {

    @XmlElement(required = true)
    protected PartNumberIdentifierType partNumberIdentifier;

    /**
     * Gets the value of the partNumberIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getPartNumberIdentifier() {
        return partNumberIdentifier;
    }

    /**
     * Sets the value of the partNumberIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setPartNumberIdentifier(PartNumberIdentifierType value) {
        this.partNumberIdentifier = value;
    }

}
