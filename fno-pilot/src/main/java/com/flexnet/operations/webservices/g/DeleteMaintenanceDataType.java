
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteMaintenanceDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteMaintenanceDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="maintenanceIdentifier" type="{urn:com.macrovision:flexnet/operations}maintenanceIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteMaintenanceDataType", propOrder = {
    "maintenanceIdentifier"
})
public class DeleteMaintenanceDataType {

    @XmlElement(required = true)
    protected MaintenanceIdentifierType maintenanceIdentifier;

    /**
     * Gets the value of the maintenanceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public MaintenanceIdentifierType getMaintenanceIdentifier() {
        return maintenanceIdentifier;
    }

    /**
     * Sets the value of the maintenanceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public void setMaintenanceIdentifier(MaintenanceIdentifierType value) {
        this.maintenanceIdentifier = value;
    }

}
