
package com.flexnet.operations.webservices.f;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateSubOrganizationsListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateSubOrganizationsListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subOrganization" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="opType" type="{urn:com.macrovision:flexnet/operations}CollectionOperationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateSubOrganizationsListType", propOrder = {
    "subOrganization",
    "opType"
})
public class UpdateSubOrganizationsListType {

    @XmlElement(required = true)
    protected List<OrganizationIdentifierType> subOrganization;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected CollectionOperationType opType;

    /**
     * Gets the value of the subOrganization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subOrganization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubOrganization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganizationIdentifierType }
     * 
     * 
     */
    public List<OrganizationIdentifierType> getSubOrganization() {
        if (subOrganization == null) {
            subOrganization = new ArrayList<OrganizationIdentifierType>();
        }
        return this.subOrganization;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link CollectionOperationType }
     *     
     */
    public CollectionOperationType getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionOperationType }
     *     
     */
    public void setOpType(CollectionOperationType value) {
        this.opType = value;
    }

}
