
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for featureStateDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="featureStateDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureIdentifier" type="{urn:v1.webservices.operations.flexnet.com}featureIdentifierType"/&gt;
 *         &lt;element name="stateToSet" type="{urn:v1.webservices.operations.flexnet.com}StateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "featureStateDataType", propOrder = {
    "featureIdentifier",
    "stateToSet"
})
public class FeatureStateDataType {

    @XmlElement(required = true)
    protected FeatureIdentifierType featureIdentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType stateToSet;

    /**
     * Gets the value of the featureIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureIdentifierType }
     *     
     */
    public FeatureIdentifierType getFeatureIdentifier() {
        return featureIdentifier;
    }

    /**
     * Sets the value of the featureIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureIdentifierType }
     *     
     */
    public void setFeatureIdentifier(FeatureIdentifierType value) {
        this.featureIdentifier = value;
    }

    /**
     * Gets the value of the stateToSet property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getStateToSet() {
        return stateToSet;
    }

    /**
     * Sets the value of the stateToSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setStateToSet(StateType value) {
        this.stateToSet = value;
    }

}
