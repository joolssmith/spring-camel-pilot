
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WSDeviceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="WSDeviceType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="CLIENT"/&gt;
 *     &lt;enumeration value="SERVER"/&gt;
 *     &lt;enumeration value="SERVED_CLIENT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "WSDeviceType")
@XmlEnum
public enum WSDeviceType {

    CLIENT,
    SERVER,
    SERVED_CLIENT;

    public String value() {
        return name();
    }

    public static WSDeviceType fromValue(String v) {
        return valueOf(v);
    }

}
