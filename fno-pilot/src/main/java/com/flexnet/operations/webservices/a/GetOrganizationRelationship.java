
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Organization1Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Organization2Id" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "producerId",
    "organization1Id",
    "organization2Id"
})
@XmlRootElement(name = "getOrganizationRelationship")
public class GetOrganizationRelationship {

    @XmlElement(name = "ProducerId", required = true)
    protected String producerId;
    @XmlElement(name = "Organization1Id", required = true)
    protected String organization1Id;
    @XmlElement(name = "Organization2Id", required = true)
    protected String organization2Id;

    /**
     * Gets the value of the producerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerId() {
        return producerId;
    }

    /**
     * Sets the value of the producerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerId(String value) {
        this.producerId = value;
    }

    /**
     * Gets the value of the organization1Id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganization1Id() {
        return organization1Id;
    }

    /**
     * Sets the value of the organization1Id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganization1Id(String value) {
        this.organization1Id = value;
    }

    /**
     * Gets the value of the organization2Id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganization2Id() {
        return organization2Id;
    }

    /**
     * Sets the value of the organization2Id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganization2Id(String value) {
        this.organization2Id = value;
    }

}
