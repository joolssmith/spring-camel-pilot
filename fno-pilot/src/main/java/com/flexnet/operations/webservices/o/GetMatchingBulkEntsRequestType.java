
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMatchingBulkEntsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMatchingBulkEntsRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntList" type="{urn:v1.webservices.operations.flexnet.com}getMatchingBulkEntsListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMatchingBulkEntsRequestType", propOrder = {
    "bulkEntList"
})
public class GetMatchingBulkEntsRequestType {

    @XmlElement(required = true)
    protected GetMatchingBulkEntsListType bulkEntList;

    /**
     * Gets the value of the bulkEntList property.
     * 
     * @return
     *     possible object is
     *     {@link GetMatchingBulkEntsListType }
     *     
     */
    public GetMatchingBulkEntsListType getBulkEntList() {
        return bulkEntList;
    }

    /**
     * Sets the value of the bulkEntList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMatchingBulkEntsListType }
     *     
     */
    public void setBulkEntList(GetMatchingBulkEntsListType value) {
        this.bulkEntList = value;
    }

}
