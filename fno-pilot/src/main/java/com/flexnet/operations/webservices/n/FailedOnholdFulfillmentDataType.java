
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedOnholdFulfillmentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedOnholdFulfillmentDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentIdentifier" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentIdentifierType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedOnholdFulfillmentDataType", propOrder = {
    "fulfillmentIdentifier",
    "reason"
})
public class FailedOnholdFulfillmentDataType {

    @XmlElement(required = true)
    protected FulfillmentIdentifierType fulfillmentIdentifier;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the fulfillmentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }

    /**
     * Sets the value of the fulfillmentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setFulfillmentIdentifier(FulfillmentIdentifierType value) {
        this.fulfillmentIdentifier = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
