
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for unlinkMaintenanceLineItemRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="unlinkMaintenanceLineItemRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="unlinkMaintenanceLineItemList" type="{urn:v2.webservices.operations.flexnet.com}unlinkMaintenanceLineItemListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unlinkMaintenanceLineItemRequestType", propOrder = {
    "unlinkMaintenanceLineItemList"
})
public class UnlinkMaintenanceLineItemRequestType {

    @XmlElement(required = true)
    protected UnlinkMaintenanceLineItemListType unlinkMaintenanceLineItemList;

    /**
     * Gets the value of the unlinkMaintenanceLineItemList property.
     * 
     * @return
     *     possible object is
     *     {@link UnlinkMaintenanceLineItemListType }
     *     
     */
    public UnlinkMaintenanceLineItemListType getUnlinkMaintenanceLineItemList() {
        return unlinkMaintenanceLineItemList;
    }

    /**
     * Sets the value of the unlinkMaintenanceLineItemList property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnlinkMaintenanceLineItemListType }
     *     
     */
    public void setUnlinkMaintenanceLineItemList(UnlinkMaintenanceLineItemListType value) {
        this.unlinkMaintenanceLineItemList = value;
    }

}
