
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedSplitBulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedSplitBulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlementInfo" type="{urn:v1.webservices.operations.flexnet.com}splitBulkEntitlementInfoType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedSplitBulkEntitlementDataType", propOrder = {
    "bulkEntitlementInfo",
    "reason"
})
public class FailedSplitBulkEntitlementDataType {

    @XmlElement(required = true)
    protected SplitBulkEntitlementInfoType bulkEntitlementInfo;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the bulkEntitlementInfo property.
     * 
     * @return
     *     possible object is
     *     {@link SplitBulkEntitlementInfoType }
     *     
     */
    public SplitBulkEntitlementInfoType getBulkEntitlementInfo() {
        return bulkEntitlementInfo;
    }

    /**
     * Sets the value of the bulkEntitlementInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitBulkEntitlementInfoType }
     *     
     */
    public void setBulkEntitlementInfo(SplitBulkEntitlementInfoType value) {
        this.bulkEntitlementInfo = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
