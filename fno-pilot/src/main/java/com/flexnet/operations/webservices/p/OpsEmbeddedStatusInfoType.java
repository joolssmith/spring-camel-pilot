
package com.flexnet.operations.webservices.p;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OpsEmbeddedStatusInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OpsEmbeddedStatusInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="status" type="{urn:v1.fne.webservices.operations.flexnet.com}OpsEmbeddedStatusType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpsEmbeddedStatusInfoType", propOrder = {
    "status",
    "reason"
})
public class OpsEmbeddedStatusInfoType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected OpsEmbeddedStatusType status;
    protected String reason;

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link OpsEmbeddedStatusType }
     *     
     */
    public OpsEmbeddedStatusType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpsEmbeddedStatusType }
     *     
     */
    public void setStatus(OpsEmbeddedStatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
