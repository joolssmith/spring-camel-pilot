
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for emailActivatableItemRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="emailActivatableItemRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ignoreExistingEmailList" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="validateEmailAddresses" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="emailIdList" type="{urn:v1.webservices.operations.flexnet.com}emailContactListType" minOccurs="0"/&gt;
 *         &lt;element name="locale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "emailActivatableItemRequestType", propOrder = {
    "activationId",
    "ignoreExistingEmailList",
    "validateEmailAddresses",
    "emailIdList",
    "locale"
})
public class EmailActivatableItemRequestType {

    @XmlElement(required = true)
    protected String activationId;
    protected Boolean ignoreExistingEmailList;
    protected Boolean validateEmailAddresses;
    protected EmailContactListType emailIdList;
    protected String locale;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationId(String value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the ignoreExistingEmailList property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIgnoreExistingEmailList() {
        return ignoreExistingEmailList;
    }

    /**
     * Sets the value of the ignoreExistingEmailList property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIgnoreExistingEmailList(Boolean value) {
        this.ignoreExistingEmailList = value;
    }

    /**
     * Gets the value of the validateEmailAddresses property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isValidateEmailAddresses() {
        return validateEmailAddresses;
    }

    /**
     * Sets the value of the validateEmailAddresses property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setValidateEmailAddresses(Boolean value) {
        this.validateEmailAddresses = value;
    }

    /**
     * Gets the value of the emailIdList property.
     * 
     * @return
     *     possible object is
     *     {@link EmailContactListType }
     *     
     */
    public EmailContactListType getEmailIdList() {
        return emailIdList;
    }

    /**
     * Sets the value of the emailIdList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailContactListType }
     *     
     */
    public void setEmailIdList(EmailContactListType value) {
        this.emailIdList = value;
    }

    /**
     * Gets the value of the locale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Sets the value of the locale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLocale(String value) {
        this.locale = value;
    }

}
