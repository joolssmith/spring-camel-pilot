
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mapEntitlementsToUserRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mapEntitlementsToUserRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="idList" type="{urn:v1.webservices.operations.flexnet.com}idListType"/&gt;
 *         &lt;element name="userOrg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mapEntitlementsToUserRequestType", propOrder = {
    "userId",
    "idList",
    "userOrg"
})
public class MapEntitlementsToUserRequestType {

    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected IdListType idList;
    protected String userOrg;

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the idList property.
     * 
     * @return
     *     possible object is
     *     {@link IdListType }
     *     
     */
    public IdListType getIdList() {
        return idList;
    }

    /**
     * Sets the value of the idList property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdListType }
     *     
     */
    public void setIdList(IdListType value) {
        this.idList = value;
    }

    /**
     * Gets the value of the userOrg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserOrg() {
        return userOrg;
    }

    /**
     * Sets the value of the userOrg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserOrg(String value) {
        this.userOrg = value;
    }

}
