
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateBulkEntitlementDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateBulkEntitlementDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedBulkEntitlement" type="{urn:com.macrovision:flexnet/operations}failedUpdateBulkEntitlementDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateBulkEntitlementDataListType", propOrder = {
    "failedBulkEntitlement"
})
public class FailedUpdateBulkEntitlementDataListType {

    protected List<FailedUpdateBulkEntitlementDataType> failedBulkEntitlement;

    /**
     * Gets the value of the failedBulkEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedBulkEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedBulkEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUpdateBulkEntitlementDataType }
     * 
     * 
     */
    public List<FailedUpdateBulkEntitlementDataType> getFailedBulkEntitlement() {
        if (failedBulkEntitlement == null) {
            failedBulkEntitlement = new ArrayList<FailedUpdateBulkEntitlementDataType>();
        }
        return this.failedBulkEntitlement;
    }

}
