
package com.flexnet.operations.webservices.n;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.n package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetFulfillmentCountRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentCountRequest");
    private final static QName _GetFulfillmentCountResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentCountResponse");
    private final static QName _GetFulfillmentsQueryRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentsQueryRequest");
    private final static QName _GetFulfillmentsQueryResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentsQueryResponse");
    private final static QName _GetFulfillmentPropertiesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentPropertiesRequest");
    private final static QName _GetFulfillmentPropertiesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentPropertiesResponse");
    private final static QName _RehostLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "rehostLicenseRequest");
    private final static QName _RehostLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "rehostLicenseResponse");
    private final static QName _ReturnLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "returnLicenseRequest");
    private final static QName _ReturnLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "returnLicenseResponse");
    private final static QName _RepairLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "repairLicenseRequest");
    private final static QName _RepairLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "repairLicenseResponse");
    private final static QName _EmergencyLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emergencyLicenseRequest");
    private final static QName _EmergencyLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emergencyLicenseResponse");
    private final static QName _PublisherErrorLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "publisherErrorLicenseRequest");
    private final static QName _PublisherErrorLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "publisherErrorLicenseResponse");
    private final static QName _StopGapLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "stopGapLicenseRequest");
    private final static QName _StopGapLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "stopGapLicenseResponse");
    private final static QName _GetFulfillmentAttributesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentAttributesRequest");
    private final static QName _GetFulfillmentAttributesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentAttributesResponse");
    private final static QName _GetHostAttributesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getHostAttributesRequest");
    private final static QName _GetHostAttributesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getHostAttributesResponse");
    private final static QName _VerifyCreateLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "verifyCreateLicenseRequest");
    private final static QName _VerifyCreateLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "verifyCreateLicenseResponse");
    private final static QName _CreateLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createLicenseRequest");
    private final static QName _CreateLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createLicenseResponse");
    private final static QName _ActivateShortCodeRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "activateShortCodeRequest");
    private final static QName _ActivateShortCodeResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "activateShortCodeResponse");
    private final static QName _RepairShortCodeRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "repairShortCodeRequest");
    private final static QName _RepairShortCodeResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "repairShortCodeResponse");
    private final static QName _ReturnShortCodeRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "returnShortCodeRequest");
    private final static QName _ReturnShortCodeResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "returnShortCodeResponse");
    private final static QName _EmailLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailLicenseRequest");
    private final static QName _EmailLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailLicenseResponse");
    private final static QName _ConsolidateFulfillmentsRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "consolidateFulfillmentsRequest");
    private final static QName _ConsolidateFulfillmentsResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "consolidateFulfillmentsResponse");
    private final static QName _GetConsolidatedFulfillmentCountRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getConsolidatedFulfillmentCountRequest");
    private final static QName _GetConsolidatedFulfillmentCountResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getConsolidatedFulfillmentCountResponse");
    private final static QName _GetConsolidatedFulfillmentsQueryRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getConsolidatedFulfillmentsQueryRequest");
    private final static QName _GetConsolidatedFulfillmentsQueryResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getConsolidatedFulfillmentsQueryResponse");
    private final static QName _GetFmtAttributesForBatchActivationRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFmtAttributesForBatchActivationRequest");
    private final static QName _GetFmtAttributesForBatchActivationResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFmtAttributesForBatchActivationResponse");
    private final static QName _CreateLicensesAsBatchRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createLicensesAsBatchRequest");
    private final static QName _CreateLicensesAsBatchResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createLicensesAsBatchResponse");
    private final static QName _CreateLicensesAsBatchAndConsolidateRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createLicensesAsBatchAndConsolidateRequest");
    private final static QName _CreateLicensesAsBatchAndConsolidateResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createLicensesAsBatchAndConsolidateResponse");
    private final static QName _EmailConsolidatedLicensesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailConsolidatedLicensesRequest");
    private final static QName _EmailConsolidatedLicensesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailConsolidatedLicensesResponse");
    private final static QName _ManualActivationRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "manualActivationRequest");
    private final static QName _ManualActivationResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "manualActivationResponse");
    private final static QName _ManualRepairRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "manualRepairRequest");
    private final static QName _ManualRepairResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "manualRepairResponse");
    private final static QName _ManualReturnRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "manualReturnRequest");
    private final static QName _ManualReturnResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "manualReturnResponse");
    private final static QName _GetFulfillmentHistoryRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentHistoryRequest");
    private final static QName _GetFulfillmentHistoryResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getFulfillmentHistoryResponse");
    private final static QName _CreateChildLineItemFulfillmentRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createChildLineItemFulfillmentRequest");
    private final static QName _CreateChildLineItemFulfillmentResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createChildLineItemFulfillmentResponse");
    private final static QName _UpgradeFulfillmentRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upgradeFulfillmentRequest");
    private final static QName _UpgradeFulfillmentResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upgradeFulfillmentResponse");
    private final static QName _UpsellFulfillmentRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upsellFulfillmentRequest");
    private final static QName _UpsellFulfillmentResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upsellFulfillmentResponse");
    private final static QName _RenewFulfillmentRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "renewFulfillmentRequest");
    private final static QName _RenewFulfillmentResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "renewFulfillmentResponse");
    private final static QName _SetLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setLicenseRequest");
    private final static QName _SetLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setLicenseResponse");
    private final static QName _DeleteOnholdFulfillmentsRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteOnholdFulfillmentsRequest");
    private final static QName _DeleteOnholdFulfillmentsResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteOnholdFulfillmentsResponse");
    private final static QName _ActivateLicensesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "activateLicensesRequest");
    private final static QName _ActivateLicensesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "activateLicensesResponse");
    private final static QName _TransferHostRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "transferHostRequest");
    private final static QName _TransferHostResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "transferHostResponse");
    private final static QName _TypeLineItemReason_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "Reason");
    private final static QName _TypeLineItemCount_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "Count");
    private final static QName _TypeLineItemVendorDictionary_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "VendorDictionary");
    private final static QName _DictionaryEntries_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "Entries");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.n
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetFulfillmentCountRequestType }
     * 
     */
    public GetFulfillmentCountRequestType createGetFulfillmentCountRequestType() {
        return new GetFulfillmentCountRequestType();
    }

    /**
     * Create an instance of {@link GetFulfillmentCountResponseType }
     * 
     */
    public GetFulfillmentCountResponseType createGetFulfillmentCountResponseType() {
        return new GetFulfillmentCountResponseType();
    }

    /**
     * Create an instance of {@link GetFulfillmentsQueryRequestType }
     * 
     */
    public GetFulfillmentsQueryRequestType createGetFulfillmentsQueryRequestType() {
        return new GetFulfillmentsQueryRequestType();
    }

    /**
     * Create an instance of {@link GetFulfillmentsQueryResponseType }
     * 
     */
    public GetFulfillmentsQueryResponseType createGetFulfillmentsQueryResponseType() {
        return new GetFulfillmentsQueryResponseType();
    }

    /**
     * Create an instance of {@link GetFulfillmentPropertiesRequestType }
     * 
     */
    public GetFulfillmentPropertiesRequestType createGetFulfillmentPropertiesRequestType() {
        return new GetFulfillmentPropertiesRequestType();
    }

    /**
     * Create an instance of {@link GetFulfillmentPropertiesResponseType }
     * 
     */
    public GetFulfillmentPropertiesResponseType createGetFulfillmentPropertiesResponseType() {
        return new GetFulfillmentPropertiesResponseType();
    }

    /**
     * Create an instance of {@link RehostFulfillmentRequestType }
     * 
     */
    public RehostFulfillmentRequestType createRehostFulfillmentRequestType() {
        return new RehostFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link RehostFulfillmentResponseType }
     * 
     */
    public RehostFulfillmentResponseType createRehostFulfillmentResponseType() {
        return new RehostFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link ReturnFulfillmentRequestType }
     * 
     */
    public ReturnFulfillmentRequestType createReturnFulfillmentRequestType() {
        return new ReturnFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link ReturnFulfillmentResponseType }
     * 
     */
    public ReturnFulfillmentResponseType createReturnFulfillmentResponseType() {
        return new ReturnFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link RepairFulfillmentRequestType }
     * 
     */
    public RepairFulfillmentRequestType createRepairFulfillmentRequestType() {
        return new RepairFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link RepairFulfillmentResponseType }
     * 
     */
    public RepairFulfillmentResponseType createRepairFulfillmentResponseType() {
        return new RepairFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link EmergencyFulfillmentRequestType }
     * 
     */
    public EmergencyFulfillmentRequestType createEmergencyFulfillmentRequestType() {
        return new EmergencyFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link EmergencyFulfillmentResponseType }
     * 
     */
    public EmergencyFulfillmentResponseType createEmergencyFulfillmentResponseType() {
        return new EmergencyFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link PublisherErrorFulfillmentRequestType }
     * 
     */
    public PublisherErrorFulfillmentRequestType createPublisherErrorFulfillmentRequestType() {
        return new PublisherErrorFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link PublisherErrorFulfillmentResponseType }
     * 
     */
    public PublisherErrorFulfillmentResponseType createPublisherErrorFulfillmentResponseType() {
        return new PublisherErrorFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link StopGapFulfillmentRequestType }
     * 
     */
    public StopGapFulfillmentRequestType createStopGapFulfillmentRequestType() {
        return new StopGapFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link StopGapFulfillmentResponseType }
     * 
     */
    public StopGapFulfillmentResponseType createStopGapFulfillmentResponseType() {
        return new StopGapFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link GetFulfillmentAttributesRequestType }
     * 
     */
    public GetFulfillmentAttributesRequestType createGetFulfillmentAttributesRequestType() {
        return new GetFulfillmentAttributesRequestType();
    }

    /**
     * Create an instance of {@link GetFulfillmentAttributesResponseType }
     * 
     */
    public GetFulfillmentAttributesResponseType createGetFulfillmentAttributesResponseType() {
        return new GetFulfillmentAttributesResponseType();
    }

    /**
     * Create an instance of {@link GetHostAttributesRequestType }
     * 
     */
    public GetHostAttributesRequestType createGetHostAttributesRequestType() {
        return new GetHostAttributesRequestType();
    }

    /**
     * Create an instance of {@link GetHostAttributesResponseType }
     * 
     */
    public GetHostAttributesResponseType createGetHostAttributesResponseType() {
        return new GetHostAttributesResponseType();
    }

    /**
     * Create an instance of {@link CreateFulfillmentRequestType }
     * 
     */
    public CreateFulfillmentRequestType createCreateFulfillmentRequestType() {
        return new CreateFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link CreateFulfillmentResponseType }
     * 
     */
    public CreateFulfillmentResponseType createCreateFulfillmentResponseType() {
        return new CreateFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link ActivateShortCodeRequestType }
     * 
     */
    public ActivateShortCodeRequestType createActivateShortCodeRequestType() {
        return new ActivateShortCodeRequestType();
    }

    /**
     * Create an instance of {@link ActivateShortCodeResponseType }
     * 
     */
    public ActivateShortCodeResponseType createActivateShortCodeResponseType() {
        return new ActivateShortCodeResponseType();
    }

    /**
     * Create an instance of {@link RepairShortCodeRequestType }
     * 
     */
    public RepairShortCodeRequestType createRepairShortCodeRequestType() {
        return new RepairShortCodeRequestType();
    }

    /**
     * Create an instance of {@link RepairShortCodeResponseType }
     * 
     */
    public RepairShortCodeResponseType createRepairShortCodeResponseType() {
        return new RepairShortCodeResponseType();
    }

    /**
     * Create an instance of {@link ReturnShortCodeRequestType }
     * 
     */
    public ReturnShortCodeRequestType createReturnShortCodeRequestType() {
        return new ReturnShortCodeRequestType();
    }

    /**
     * Create an instance of {@link ReturnShortCodeResponseType }
     * 
     */
    public ReturnShortCodeResponseType createReturnShortCodeResponseType() {
        return new ReturnShortCodeResponseType();
    }

    /**
     * Create an instance of {@link EmailLicenseRequestType }
     * 
     */
    public EmailLicenseRequestType createEmailLicenseRequestType() {
        return new EmailLicenseRequestType();
    }

    /**
     * Create an instance of {@link EmailLicenseResponseType }
     * 
     */
    public EmailLicenseResponseType createEmailLicenseResponseType() {
        return new EmailLicenseResponseType();
    }

    /**
     * Create an instance of {@link ConsolidateFulfillmentsRequestType }
     * 
     */
    public ConsolidateFulfillmentsRequestType createConsolidateFulfillmentsRequestType() {
        return new ConsolidateFulfillmentsRequestType();
    }

    /**
     * Create an instance of {@link ConsolidateFulfillmentsResponseType }
     * 
     */
    public ConsolidateFulfillmentsResponseType createConsolidateFulfillmentsResponseType() {
        return new ConsolidateFulfillmentsResponseType();
    }

    /**
     * Create an instance of {@link GetConsolidatedFulfillmentCountRequestType }
     * 
     */
    public GetConsolidatedFulfillmentCountRequestType createGetConsolidatedFulfillmentCountRequestType() {
        return new GetConsolidatedFulfillmentCountRequestType();
    }

    /**
     * Create an instance of {@link GetConsolidatedFulfillmentCountResponseType }
     * 
     */
    public GetConsolidatedFulfillmentCountResponseType createGetConsolidatedFulfillmentCountResponseType() {
        return new GetConsolidatedFulfillmentCountResponseType();
    }

    /**
     * Create an instance of {@link GetConsolidatedFulfillmentsQueryRequestType }
     * 
     */
    public GetConsolidatedFulfillmentsQueryRequestType createGetConsolidatedFulfillmentsQueryRequestType() {
        return new GetConsolidatedFulfillmentsQueryRequestType();
    }

    /**
     * Create an instance of {@link GetConsolidatedFulfillmentsQueryResponseType }
     * 
     */
    public GetConsolidatedFulfillmentsQueryResponseType createGetConsolidatedFulfillmentsQueryResponseType() {
        return new GetConsolidatedFulfillmentsQueryResponseType();
    }

    /**
     * Create an instance of {@link GetFmtAttributesForBatchActivationRequestType }
     * 
     */
    public GetFmtAttributesForBatchActivationRequestType createGetFmtAttributesForBatchActivationRequestType() {
        return new GetFmtAttributesForBatchActivationRequestType();
    }

    /**
     * Create an instance of {@link GetFmtAttributesForBatchActivationResponseType }
     * 
     */
    public GetFmtAttributesForBatchActivationResponseType createGetFmtAttributesForBatchActivationResponseType() {
        return new GetFmtAttributesForBatchActivationResponseType();
    }

    /**
     * Create an instance of {@link CreateLicensesAsBatchRequestType }
     * 
     */
    public CreateLicensesAsBatchRequestType createCreateLicensesAsBatchRequestType() {
        return new CreateLicensesAsBatchRequestType();
    }

    /**
     * Create an instance of {@link CreateLicensesAsBatchResponseType }
     * 
     */
    public CreateLicensesAsBatchResponseType createCreateLicensesAsBatchResponseType() {
        return new CreateLicensesAsBatchResponseType();
    }

    /**
     * Create an instance of {@link EmailConsolidatedLicensesRequestType }
     * 
     */
    public EmailConsolidatedLicensesRequestType createEmailConsolidatedLicensesRequestType() {
        return new EmailConsolidatedLicensesRequestType();
    }

    /**
     * Create an instance of {@link EmailConsolidatedLicensesResponseType }
     * 
     */
    public EmailConsolidatedLicensesResponseType createEmailConsolidatedLicensesResponseType() {
        return new EmailConsolidatedLicensesResponseType();
    }

    /**
     * Create an instance of {@link TrustedRequestType }
     * 
     */
    public TrustedRequestType createTrustedRequestType() {
        return new TrustedRequestType();
    }

    /**
     * Create an instance of {@link TrustedResponseType }
     * 
     */
    public TrustedResponseType createTrustedResponseType() {
        return new TrustedResponseType();
    }

    /**
     * Create an instance of {@link GetFulfillmentHistoryRequestType }
     * 
     */
    public GetFulfillmentHistoryRequestType createGetFulfillmentHistoryRequestType() {
        return new GetFulfillmentHistoryRequestType();
    }

    /**
     * Create an instance of {@link GetFulfillmentHistoryResponseType }
     * 
     */
    public GetFulfillmentHistoryResponseType createGetFulfillmentHistoryResponseType() {
        return new GetFulfillmentHistoryResponseType();
    }

    /**
     * Create an instance of {@link CreateChildLineItemFulfillmentRequestType }
     * 
     */
    public CreateChildLineItemFulfillmentRequestType createCreateChildLineItemFulfillmentRequestType() {
        return new CreateChildLineItemFulfillmentRequestType();
    }

    /**
     * Create an instance of {@link CreateChildLineItemFulfillmentResponseType }
     * 
     */
    public CreateChildLineItemFulfillmentResponseType createCreateChildLineItemFulfillmentResponseType() {
        return new CreateChildLineItemFulfillmentResponseType();
    }

    /**
     * Create an instance of {@link AdvancedFulfillmentLCRequestType }
     * 
     */
    public AdvancedFulfillmentLCRequestType createAdvancedFulfillmentLCRequestType() {
        return new AdvancedFulfillmentLCRequestType();
    }

    /**
     * Create an instance of {@link AdvancedFulfillmentLCResponseType }
     * 
     */
    public AdvancedFulfillmentLCResponseType createAdvancedFulfillmentLCResponseType() {
        return new AdvancedFulfillmentLCResponseType();
    }

    /**
     * Create an instance of {@link SetLicenseRequestType }
     * 
     */
    public SetLicenseRequestType createSetLicenseRequestType() {
        return new SetLicenseRequestType();
    }

    /**
     * Create an instance of {@link SetLicenseResponseType }
     * 
     */
    public SetLicenseResponseType createSetLicenseResponseType() {
        return new SetLicenseResponseType();
    }

    /**
     * Create an instance of {@link DeleteOnholdFulfillmentsRequestType }
     * 
     */
    public DeleteOnholdFulfillmentsRequestType createDeleteOnholdFulfillmentsRequestType() {
        return new DeleteOnholdFulfillmentsRequestType();
    }

    /**
     * Create an instance of {@link DeleteOnholdFulfillmentsResponseType }
     * 
     */
    public DeleteOnholdFulfillmentsResponseType createDeleteOnholdFulfillmentsResponseType() {
        return new DeleteOnholdFulfillmentsResponseType();
    }

    /**
     * Create an instance of {@link ActivateLicensesRequestType }
     * 
     */
    public ActivateLicensesRequestType createActivateLicensesRequestType() {
        return new ActivateLicensesRequestType();
    }

    /**
     * Create an instance of {@link ActivateLicensesResponseType }
     * 
     */
    public ActivateLicensesResponseType createActivateLicensesResponseType() {
        return new ActivateLicensesResponseType();
    }

    /**
     * Create an instance of {@link TransferHostRequestType }
     * 
     */
    public TransferHostRequestType createTransferHostRequestType() {
        return new TransferHostRequestType();
    }

    /**
     * Create an instance of {@link TransferHostResponseType }
     * 
     */
    public TransferHostResponseType createTransferHostResponseType() {
        return new TransferHostResponseType();
    }

    /**
     * Create an instance of {@link SimpleQueryType }
     * 
     */
    public SimpleQueryType createSimpleQueryType() {
        return new SimpleQueryType();
    }

    /**
     * Create an instance of {@link DateQueryType }
     * 
     */
    public DateQueryType createDateQueryType() {
        return new DateQueryType();
    }

    /**
     * Create an instance of {@link DateTimeQueryType }
     * 
     */
    public DateTimeQueryType createDateTimeQueryType() {
        return new DateTimeQueryType();
    }

    /**
     * Create an instance of {@link StateQueryType }
     * 
     */
    public StateQueryType createStateQueryType() {
        return new StateQueryType();
    }

    /**
     * Create an instance of {@link NumberQueryType }
     * 
     */
    public NumberQueryType createNumberQueryType() {
        return new NumberQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributeQueryType }
     * 
     */
    public CustomAttributeQueryType createCustomAttributeQueryType() {
        return new CustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributesQueryListType }
     * 
     */
    public CustomAttributesQueryListType createCustomAttributesQueryListType() {
        return new CustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link FulfillmentsQueryParametersType }
     * 
     */
    public FulfillmentsQueryParametersType createFulfillmentsQueryParametersType() {
        return new FulfillmentsQueryParametersType();
    }

    /**
     * Create an instance of {@link StatusInfoType }
     * 
     */
    public StatusInfoType createStatusInfoType() {
        return new StatusInfoType();
    }

    /**
     * Create an instance of {@link GetFulfillmentCountResponseDataType }
     * 
     */
    public GetFulfillmentCountResponseDataType createGetFulfillmentCountResponseDataType() {
        return new GetFulfillmentCountResponseDataType();
    }

    /**
     * Create an instance of {@link EntitlementPKType }
     * 
     */
    public EntitlementPKType createEntitlementPKType() {
        return new EntitlementPKType();
    }

    /**
     * Create an instance of {@link EntitlementIdentifierType }
     * 
     */
    public EntitlementIdentifierType createEntitlementIdentifierType() {
        return new EntitlementIdentifierType();
    }

    /**
     * Create an instance of {@link FulfillmentPKType }
     * 
     */
    public FulfillmentPKType createFulfillmentPKType() {
        return new FulfillmentPKType();
    }

    /**
     * Create an instance of {@link FulfillmentIdentifierType }
     * 
     */
    public FulfillmentIdentifierType createFulfillmentIdentifierType() {
        return new FulfillmentIdentifierType();
    }

    /**
     * Create an instance of {@link EntitlementLineItemPKType }
     * 
     */
    public EntitlementLineItemPKType createEntitlementLineItemPKType() {
        return new EntitlementLineItemPKType();
    }

    /**
     * Create an instance of {@link EntitlementLineItemIdentifierType }
     * 
     */
    public EntitlementLineItemIdentifierType createEntitlementLineItemIdentifierType() {
        return new EntitlementLineItemIdentifierType();
    }

    /**
     * Create an instance of {@link ProductPKType }
     * 
     */
    public ProductPKType createProductPKType() {
        return new ProductPKType();
    }

    /**
     * Create an instance of {@link ProductIdentifierType }
     * 
     */
    public ProductIdentifierType createProductIdentifierType() {
        return new ProductIdentifierType();
    }

    /**
     * Create an instance of {@link ServerIDsType }
     * 
     */
    public ServerIDsType createServerIDsType() {
        return new ServerIDsType();
    }

    /**
     * Create an instance of {@link NodeIDsType }
     * 
     */
    public NodeIDsType createNodeIDsType() {
        return new NodeIDsType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorType }
     * 
     */
    public AttributeDescriptorType createAttributeDescriptorType() {
        return new AttributeDescriptorType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorDataType }
     * 
     */
    public AttributeDescriptorDataType createAttributeDescriptorDataType() {
        return new AttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link HostTypePKType }
     * 
     */
    public HostTypePKType createHostTypePKType() {
        return new HostTypePKType();
    }

    /**
     * Create an instance of {@link CustomHostIDType }
     * 
     */
    public CustomHostIDType createCustomHostIDType() {
        return new CustomHostIDType();
    }

    /**
     * Create an instance of {@link ConsolidatedHostLicenseDataType }
     * 
     */
    public ConsolidatedHostLicenseDataType createConsolidatedHostLicenseDataType() {
        return new ConsolidatedHostLicenseDataType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyPKType }
     * 
     */
    public LicenseTechnologyPKType createLicenseTechnologyPKType() {
        return new LicenseTechnologyPKType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyIdentifierType }
     * 
     */
    public LicenseTechnologyIdentifierType createLicenseTechnologyIdentifierType() {
        return new LicenseTechnologyIdentifierType();
    }

    /**
     * Create an instance of {@link LicenseFileDataType }
     * 
     */
    public LicenseFileDataType createLicenseFileDataType() {
        return new LicenseFileDataType();
    }

    /**
     * Create an instance of {@link LicenseFileDataListType }
     * 
     */
    public LicenseFileDataListType createLicenseFileDataListType() {
        return new LicenseFileDataListType();
    }

    /**
     * Create an instance of {@link EntitledProductDataType }
     * 
     */
    public EntitledProductDataType createEntitledProductDataType() {
        return new EntitledProductDataType();
    }

    /**
     * Create an instance of {@link EntitledProductDataListType }
     * 
     */
    public EntitledProductDataListType createEntitledProductDataListType() {
        return new EntitledProductDataListType();
    }

    /**
     * Create an instance of {@link FulfillmentDataType }
     * 
     */
    public FulfillmentDataType createFulfillmentDataType() {
        return new FulfillmentDataType();
    }

    /**
     * Create an instance of {@link GetFulfillmentsQueryResponseDataType }
     * 
     */
    public GetFulfillmentsQueryResponseDataType createGetFulfillmentsQueryResponseDataType() {
        return new GetFulfillmentsQueryResponseDataType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorType }
     * 
     */
    public CustomAttributeDescriptorType createCustomAttributeDescriptorType() {
        return new CustomAttributeDescriptorType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorDataType }
     * 
     */
    public CustomAttributeDescriptorDataType createCustomAttributeDescriptorDataType() {
        return new CustomAttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link FulfillmentResponseConfigRequestType }
     * 
     */
    public FulfillmentResponseConfigRequestType createFulfillmentResponseConfigRequestType() {
        return new FulfillmentResponseConfigRequestType();
    }

    /**
     * Create an instance of {@link PartNumberPKType }
     * 
     */
    public PartNumberPKType createPartNumberPKType() {
        return new PartNumberPKType();
    }

    /**
     * Create an instance of {@link PartNumberIdentifierType }
     * 
     */
    public PartNumberIdentifierType createPartNumberIdentifierType() {
        return new PartNumberIdentifierType();
    }

    /**
     * Create an instance of {@link LicenseModelPKType }
     * 
     */
    public LicenseModelPKType createLicenseModelPKType() {
        return new LicenseModelPKType();
    }

    /**
     * Create an instance of {@link LicenseModelIdentifierType }
     * 
     */
    public LicenseModelIdentifierType createLicenseModelIdentifierType() {
        return new LicenseModelIdentifierType();
    }

    /**
     * Create an instance of {@link AccountPKType }
     * 
     */
    public AccountPKType createAccountPKType() {
        return new AccountPKType();
    }

    /**
     * Create an instance of {@link AccountIdentifierType }
     * 
     */
    public AccountIdentifierType createAccountIdentifierType() {
        return new AccountIdentifierType();
    }

    /**
     * Create an instance of {@link FulfillmentPropertiesType }
     * 
     */
    public FulfillmentPropertiesType createFulfillmentPropertiesType() {
        return new FulfillmentPropertiesType();
    }

    /**
     * Create an instance of {@link RehostFulfillmentDataType }
     * 
     */
    public RehostFulfillmentDataType createRehostFulfillmentDataType() {
        return new RehostFulfillmentDataType();
    }

    /**
     * Create an instance of {@link RehostFulfillmentResponseDataType }
     * 
     */
    public RehostFulfillmentResponseDataType createRehostFulfillmentResponseDataType() {
        return new RehostFulfillmentResponseDataType();
    }

    /**
     * Create an instance of {@link RehostResponseDataType }
     * 
     */
    public RehostResponseDataType createRehostResponseDataType() {
        return new RehostResponseDataType();
    }

    /**
     * Create an instance of {@link FailedRehostResponseDataType }
     * 
     */
    public FailedRehostResponseDataType createFailedRehostResponseDataType() {
        return new FailedRehostResponseDataType();
    }

    /**
     * Create an instance of {@link FailedRehostResponselistDataType }
     * 
     */
    public FailedRehostResponselistDataType createFailedRehostResponselistDataType() {
        return new FailedRehostResponselistDataType();
    }

    /**
     * Create an instance of {@link ReturnFulfillmentDataType }
     * 
     */
    public ReturnFulfillmentDataType createReturnFulfillmentDataType() {
        return new ReturnFulfillmentDataType();
    }

    /**
     * Create an instance of {@link ReturnFulfillmentResponseDataType }
     * 
     */
    public ReturnFulfillmentResponseDataType createReturnFulfillmentResponseDataType() {
        return new ReturnFulfillmentResponseDataType();
    }

    /**
     * Create an instance of {@link ReturnResponseDataType }
     * 
     */
    public ReturnResponseDataType createReturnResponseDataType() {
        return new ReturnResponseDataType();
    }

    /**
     * Create an instance of {@link FailedReturnResponseDataType }
     * 
     */
    public FailedReturnResponseDataType createFailedReturnResponseDataType() {
        return new FailedReturnResponseDataType();
    }

    /**
     * Create an instance of {@link FailedReturnResponselistDataType }
     * 
     */
    public FailedReturnResponselistDataType createFailedReturnResponselistDataType() {
        return new FailedReturnResponselistDataType();
    }

    /**
     * Create an instance of {@link RepairFulfillmentDataType }
     * 
     */
    public RepairFulfillmentDataType createRepairFulfillmentDataType() {
        return new RepairFulfillmentDataType();
    }

    /**
     * Create an instance of {@link RepairFulfillmentResponseDataType }
     * 
     */
    public RepairFulfillmentResponseDataType createRepairFulfillmentResponseDataType() {
        return new RepairFulfillmentResponseDataType();
    }

    /**
     * Create an instance of {@link RepairResponseDataType }
     * 
     */
    public RepairResponseDataType createRepairResponseDataType() {
        return new RepairResponseDataType();
    }

    /**
     * Create an instance of {@link FailedRepairResponseDataType }
     * 
     */
    public FailedRepairResponseDataType createFailedRepairResponseDataType() {
        return new FailedRepairResponseDataType();
    }

    /**
     * Create an instance of {@link FailedRepairResponselistDataType }
     * 
     */
    public FailedRepairResponselistDataType createFailedRepairResponselistDataType() {
        return new FailedRepairResponselistDataType();
    }

    /**
     * Create an instance of {@link EmergencyFulfillmentDataType }
     * 
     */
    public EmergencyFulfillmentDataType createEmergencyFulfillmentDataType() {
        return new EmergencyFulfillmentDataType();
    }

    /**
     * Create an instance of {@link EmergencyFulfillmentResponseDataType }
     * 
     */
    public EmergencyFulfillmentResponseDataType createEmergencyFulfillmentResponseDataType() {
        return new EmergencyFulfillmentResponseDataType();
    }

    /**
     * Create an instance of {@link EmergencyResponseDataType }
     * 
     */
    public EmergencyResponseDataType createEmergencyResponseDataType() {
        return new EmergencyResponseDataType();
    }

    /**
     * Create an instance of {@link FailedEmergencyResponseDataType }
     * 
     */
    public FailedEmergencyResponseDataType createFailedEmergencyResponseDataType() {
        return new FailedEmergencyResponseDataType();
    }

    /**
     * Create an instance of {@link FailedEmergencyResponselistDataType }
     * 
     */
    public FailedEmergencyResponselistDataType createFailedEmergencyResponselistDataType() {
        return new FailedEmergencyResponselistDataType();
    }

    /**
     * Create an instance of {@link PublisherErrorFulfillmentDataType }
     * 
     */
    public PublisherErrorFulfillmentDataType createPublisherErrorFulfillmentDataType() {
        return new PublisherErrorFulfillmentDataType();
    }

    /**
     * Create an instance of {@link PublisherErrorFulfillmentResponseDataType }
     * 
     */
    public PublisherErrorFulfillmentResponseDataType createPublisherErrorFulfillmentResponseDataType() {
        return new PublisherErrorFulfillmentResponseDataType();
    }

    /**
     * Create an instance of {@link PublisherErrorResponseDataType }
     * 
     */
    public PublisherErrorResponseDataType createPublisherErrorResponseDataType() {
        return new PublisherErrorResponseDataType();
    }

    /**
     * Create an instance of {@link FailedPublisherErrorResponseDataType }
     * 
     */
    public FailedPublisherErrorResponseDataType createFailedPublisherErrorResponseDataType() {
        return new FailedPublisherErrorResponseDataType();
    }

    /**
     * Create an instance of {@link FailedPublisherErrorResponselistDataType }
     * 
     */
    public FailedPublisherErrorResponselistDataType createFailedPublisherErrorResponselistDataType() {
        return new FailedPublisherErrorResponselistDataType();
    }

    /**
     * Create an instance of {@link StopGapFulfillmentDataType }
     * 
     */
    public StopGapFulfillmentDataType createStopGapFulfillmentDataType() {
        return new StopGapFulfillmentDataType();
    }

    /**
     * Create an instance of {@link StopGapFulfillmentResponseDataType }
     * 
     */
    public StopGapFulfillmentResponseDataType createStopGapFulfillmentResponseDataType() {
        return new StopGapFulfillmentResponseDataType();
    }

    /**
     * Create an instance of {@link StopGapResponseDataType }
     * 
     */
    public StopGapResponseDataType createStopGapResponseDataType() {
        return new StopGapResponseDataType();
    }

    /**
     * Create an instance of {@link FailedStopGapResponseDataType }
     * 
     */
    public FailedStopGapResponseDataType createFailedStopGapResponseDataType() {
        return new FailedStopGapResponseDataType();
    }

    /**
     * Create an instance of {@link FailedStopGapResponselistDataType }
     * 
     */
    public FailedStopGapResponselistDataType createFailedStopGapResponselistDataType() {
        return new FailedStopGapResponselistDataType();
    }

    /**
     * Create an instance of {@link ValueType }
     * 
     */
    public ValueType createValueType() {
        return new ValueType();
    }

    /**
     * Create an instance of {@link AttributeMetaDescriptorType }
     * 
     */
    public AttributeMetaDescriptorType createAttributeMetaDescriptorType() {
        return new AttributeMetaDescriptorType();
    }

    /**
     * Create an instance of {@link AttributeMetaDescriptorDataType }
     * 
     */
    public AttributeMetaDescriptorDataType createAttributeMetaDescriptorDataType() {
        return new AttributeMetaDescriptorDataType();
    }

    /**
     * Create an instance of {@link GetFulfillmentAttributesDataType }
     * 
     */
    public GetFulfillmentAttributesDataType createGetFulfillmentAttributesDataType() {
        return new GetFulfillmentAttributesDataType();
    }

    /**
     * Create an instance of {@link GetHostAttributesDataType }
     * 
     */
    public GetHostAttributesDataType createGetHostAttributesDataType() {
        return new GetHostAttributesDataType();
    }

    /**
     * Create an instance of {@link CreateFulfillmentDataType }
     * 
     */
    public CreateFulfillmentDataType createCreateFulfillmentDataType() {
        return new CreateFulfillmentDataType();
    }

    /**
     * Create an instance of {@link FailedFulfillmentDataType }
     * 
     */
    public FailedFulfillmentDataType createFailedFulfillmentDataType() {
        return new FailedFulfillmentDataType();
    }

    /**
     * Create an instance of {@link FailedFulfillmentDataListType }
     * 
     */
    public FailedFulfillmentDataListType createFailedFulfillmentDataListType() {
        return new FailedFulfillmentDataListType();
    }

    /**
     * Create an instance of {@link CreatedFulfillmentDataType }
     * 
     */
    public CreatedFulfillmentDataType createCreatedFulfillmentDataType() {
        return new CreatedFulfillmentDataType();
    }

    /**
     * Create an instance of {@link VerifiedFulfillmentDataType }
     * 
     */
    public VerifiedFulfillmentDataType createVerifiedFulfillmentDataType() {
        return new VerifiedFulfillmentDataType();
    }

    /**
     * Create an instance of {@link CreatedFulfillmentDataListType }
     * 
     */
    public CreatedFulfillmentDataListType createCreatedFulfillmentDataListType() {
        return new CreatedFulfillmentDataListType();
    }

    /**
     * Create an instance of {@link SimpleAttributeDataType }
     * 
     */
    public SimpleAttributeDataType createSimpleAttributeDataType() {
        return new SimpleAttributeDataType();
    }

    /**
     * Create an instance of {@link PublisherAttributesListDataType }
     * 
     */
    public PublisherAttributesListDataType createPublisherAttributesListDataType() {
        return new PublisherAttributesListDataType();
    }

    /**
     * Create an instance of {@link CreateShortCodeDataType }
     * 
     */
    public CreateShortCodeDataType createCreateShortCodeDataType() {
        return new CreateShortCodeDataType();
    }

    /**
     * Create an instance of {@link DuplicateFulfillmentRecordListDataType }
     * 
     */
    public DuplicateFulfillmentRecordListDataType createDuplicateFulfillmentRecordListDataType() {
        return new DuplicateFulfillmentRecordListDataType();
    }

    /**
     * Create an instance of {@link FailedShortCodeDataType }
     * 
     */
    public FailedShortCodeDataType createFailedShortCodeDataType() {
        return new FailedShortCodeDataType();
    }

    /**
     * Create an instance of {@link CreatedShortCodeDataType }
     * 
     */
    public CreatedShortCodeDataType createCreatedShortCodeDataType() {
        return new CreatedShortCodeDataType();
    }

    /**
     * Create an instance of {@link RepairShortCodeDataType }
     * 
     */
    public RepairShortCodeDataType createRepairShortCodeDataType() {
        return new RepairShortCodeDataType();
    }

    /**
     * Create an instance of {@link FailedRepairShortCodeDataType }
     * 
     */
    public FailedRepairShortCodeDataType createFailedRepairShortCodeDataType() {
        return new FailedRepairShortCodeDataType();
    }

    /**
     * Create an instance of {@link RepairedShortCodeDataType }
     * 
     */
    public RepairedShortCodeDataType createRepairedShortCodeDataType() {
        return new RepairedShortCodeDataType();
    }

    /**
     * Create an instance of {@link ReturnShortCodeDataType }
     * 
     */
    public ReturnShortCodeDataType createReturnShortCodeDataType() {
        return new ReturnShortCodeDataType();
    }

    /**
     * Create an instance of {@link FailedReturnShortCodeDataType }
     * 
     */
    public FailedReturnShortCodeDataType createFailedReturnShortCodeDataType() {
        return new FailedReturnShortCodeDataType();
    }

    /**
     * Create an instance of {@link ReturnedShortCodeDataType }
     * 
     */
    public ReturnedShortCodeDataType createReturnedShortCodeDataType() {
        return new ReturnedShortCodeDataType();
    }

    /**
     * Create an instance of {@link EmailContactListType }
     * 
     */
    public EmailContactListType createEmailContactListType() {
        return new EmailContactListType();
    }

    /**
     * Create an instance of {@link FulfillmentIdentifierListType }
     * 
     */
    public FulfillmentIdentifierListType createFulfillmentIdentifierListType() {
        return new FulfillmentIdentifierListType();
    }

    /**
     * Create an instance of {@link ConsolidatedLicenseDataType }
     * 
     */
    public ConsolidatedLicenseDataType createConsolidatedLicenseDataType() {
        return new ConsolidatedLicenseDataType();
    }

    /**
     * Create an instance of {@link ConsolidatedResponseDataType }
     * 
     */
    public ConsolidatedResponseDataType createConsolidatedResponseDataType() {
        return new ConsolidatedResponseDataType();
    }

    /**
     * Create an instance of {@link ConsolidatedFulfillmentsQPType }
     * 
     */
    public ConsolidatedFulfillmentsQPType createConsolidatedFulfillmentsQPType() {
        return new ConsolidatedFulfillmentsQPType();
    }

    /**
     * Create an instance of {@link GetConsolidatedFulfillmentCountResponseDataType }
     * 
     */
    public GetConsolidatedFulfillmentCountResponseDataType createGetConsolidatedFulfillmentCountResponseDataType() {
        return new GetConsolidatedFulfillmentCountResponseDataType();
    }

    /**
     * Create an instance of {@link GetConsolidatedFulfillmentsQueryResponseDataType }
     * 
     */
    public GetConsolidatedFulfillmentsQueryResponseDataType createGetConsolidatedFulfillmentsQueryResponseDataType() {
        return new GetConsolidatedFulfillmentsQueryResponseDataType();
    }

    /**
     * Create an instance of {@link ActivationIdsListType }
     * 
     */
    public ActivationIdsListType createActivationIdsListType() {
        return new ActivationIdsListType();
    }

    /**
     * Create an instance of {@link ActivationIdOverDraftMapType }
     * 
     */
    public ActivationIdOverDraftMapType createActivationIdOverDraftMapType() {
        return new ActivationIdOverDraftMapType();
    }

    /**
     * Create an instance of {@link OverDraftDataListType }
     * 
     */
    public OverDraftDataListType createOverDraftDataListType() {
        return new OverDraftDataListType();
    }

    /**
     * Create an instance of {@link GetFmtAttributesForBatchDataType }
     * 
     */
    public GetFmtAttributesForBatchDataType createGetFmtAttributesForBatchDataType() {
        return new GetFmtAttributesForBatchDataType();
    }

    /**
     * Create an instance of {@link HostIdDataType }
     * 
     */
    public HostIdDataType createHostIdDataType() {
        return new HostIdDataType();
    }

    /**
     * Create an instance of {@link HostIdDataSetType }
     * 
     */
    public HostIdDataSetType createHostIdDataSetType() {
        return new HostIdDataSetType();
    }

    /**
     * Create an instance of {@link CountForHostsType }
     * 
     */
    public CountForHostsType createCountForHostsType() {
        return new CountForHostsType();
    }

    /**
     * Create an instance of {@link CountDataType }
     * 
     */
    public CountDataType createCountDataType() {
        return new CountDataType();
    }

    /**
     * Create an instance of {@link CountDataSetType }
     * 
     */
    public CountDataSetType createCountDataSetType() {
        return new CountDataSetType();
    }

    /**
     * Create an instance of {@link CommonBatchDataSetType }
     * 
     */
    public CommonBatchDataSetType createCommonBatchDataSetType() {
        return new CommonBatchDataSetType();
    }

    /**
     * Create an instance of {@link CreateLicensesAsBatchResponseDataType }
     * 
     */
    public CreateLicensesAsBatchResponseDataType createCreateLicensesAsBatchResponseDataType() {
        return new CreateLicensesAsBatchResponseDataType();
    }

    /**
     * Create an instance of {@link ConsolidatedLicenseIdListType }
     * 
     */
    public ConsolidatedLicenseIdListType createConsolidatedLicenseIdListType() {
        return new ConsolidatedLicenseIdListType();
    }

    /**
     * Create an instance of {@link ActivationDataType }
     * 
     */
    public ActivationDataType createActivationDataType() {
        return new ActivationDataType();
    }

    /**
     * Create an instance of {@link FulfillmentHistoryRecordType }
     * 
     */
    public FulfillmentHistoryRecordType createFulfillmentHistoryRecordType() {
        return new FulfillmentHistoryRecordType();
    }

    /**
     * Create an instance of {@link FulfillmentHistoryDetailsType }
     * 
     */
    public FulfillmentHistoryDetailsType createFulfillmentHistoryDetailsType() {
        return new FulfillmentHistoryDetailsType();
    }

    /**
     * Create an instance of {@link FulfillmentHistoryDataType }
     * 
     */
    public FulfillmentHistoryDataType createFulfillmentHistoryDataType() {
        return new FulfillmentHistoryDataType();
    }

    /**
     * Create an instance of {@link HostIdDataDetailsType }
     * 
     */
    public HostIdDataDetailsType createHostIdDataDetailsType() {
        return new HostIdDataDetailsType();
    }

    /**
     * Create an instance of {@link HostIdDetailsType }
     * 
     */
    public HostIdDetailsType createHostIdDetailsType() {
        return new HostIdDetailsType();
    }

    /**
     * Create an instance of {@link CreateChildLineItemFulfillmentDataType }
     * 
     */
    public CreateChildLineItemFulfillmentDataType createCreateChildLineItemFulfillmentDataType() {
        return new CreateChildLineItemFulfillmentDataType();
    }

    /**
     * Create an instance of {@link CreatedChildLIFulfillmentInfoType }
     * 
     */
    public CreatedChildLIFulfillmentInfoType createCreatedChildLIFulfillmentInfoType() {
        return new CreatedChildLIFulfillmentInfoType();
    }

    /**
     * Create an instance of {@link CreatedChildLIFulfillmentDataType }
     * 
     */
    public CreatedChildLIFulfillmentDataType createCreatedChildLIFulfillmentDataType() {
        return new CreatedChildLIFulfillmentDataType();
    }

    /**
     * Create an instance of {@link CreatedChildLIFmtResponseDataType }
     * 
     */
    public CreatedChildLIFmtResponseDataType createCreatedChildLIFmtResponseDataType() {
        return new CreatedChildLIFmtResponseDataType();
    }

    /**
     * Create an instance of {@link FailedChildLIFulfillmentDataType }
     * 
     */
    public FailedChildLIFulfillmentDataType createFailedChildLIFulfillmentDataType() {
        return new FailedChildLIFulfillmentDataType();
    }

    /**
     * Create an instance of {@link FailedChildLIFmtResponseDataType }
     * 
     */
    public FailedChildLIFmtResponseDataType createFailedChildLIFmtResponseDataType() {
        return new FailedChildLIFmtResponseDataType();
    }

    /**
     * Create an instance of {@link AdvancedFulfillmentLCDataType }
     * 
     */
    public AdvancedFulfillmentLCDataType createAdvancedFulfillmentLCDataType() {
        return new AdvancedFulfillmentLCDataType();
    }

    /**
     * Create an instance of {@link AdvancedFulfillmentLCListType }
     * 
     */
    public AdvancedFulfillmentLCListType createAdvancedFulfillmentLCListType() {
        return new AdvancedFulfillmentLCListType();
    }

    /**
     * Create an instance of {@link AdvancedFulfillmentLCInfoType }
     * 
     */
    public AdvancedFulfillmentLCInfoType createAdvancedFulfillmentLCInfoType() {
        return new AdvancedFulfillmentLCInfoType();
    }

    /**
     * Create an instance of {@link AdvancedFmtLCResponseDataType }
     * 
     */
    public AdvancedFmtLCResponseDataType createAdvancedFmtLCResponseDataType() {
        return new AdvancedFmtLCResponseDataType();
    }

    /**
     * Create an instance of {@link AdvancedFmtLCResponseDataListType }
     * 
     */
    public AdvancedFmtLCResponseDataListType createAdvancedFmtLCResponseDataListType() {
        return new AdvancedFmtLCResponseDataListType();
    }

    /**
     * Create an instance of {@link FailedAdvancedFmtLCDataType }
     * 
     */
    public FailedAdvancedFmtLCDataType createFailedAdvancedFmtLCDataType() {
        return new FailedAdvancedFmtLCDataType();
    }

    /**
     * Create an instance of {@link FailedAdvancedFmtLCResponseDataType }
     * 
     */
    public FailedAdvancedFmtLCResponseDataType createFailedAdvancedFmtLCResponseDataType() {
        return new FailedAdvancedFmtLCResponseDataType();
    }

    /**
     * Create an instance of {@link OnHoldFmtLicenseDataType }
     * 
     */
    public OnHoldFmtLicenseDataType createOnHoldFmtLicenseDataType() {
        return new OnHoldFmtLicenseDataType();
    }

    /**
     * Create an instance of {@link OnholdFulfillmentListType }
     * 
     */
    public OnholdFulfillmentListType createOnholdFulfillmentListType() {
        return new OnholdFulfillmentListType();
    }

    /**
     * Create an instance of {@link FailedSetLicenseOnholdFulfillmentDataType }
     * 
     */
    public FailedSetLicenseOnholdFulfillmentDataType createFailedSetLicenseOnholdFulfillmentDataType() {
        return new FailedSetLicenseOnholdFulfillmentDataType();
    }

    /**
     * Create an instance of {@link FailedSetLicenseOnholdFulfillmentListType }
     * 
     */
    public FailedSetLicenseOnholdFulfillmentListType createFailedSetLicenseOnholdFulfillmentListType() {
        return new FailedSetLicenseOnholdFulfillmentListType();
    }

    /**
     * Create an instance of {@link FulfillmentIdListType }
     * 
     */
    public FulfillmentIdListType createFulfillmentIdListType() {
        return new FulfillmentIdListType();
    }

    /**
     * Create an instance of {@link FailedOnholdFulfillmentDataType }
     * 
     */
    public FailedOnholdFulfillmentDataType createFailedOnholdFulfillmentDataType() {
        return new FailedOnholdFulfillmentDataType();
    }

    /**
     * Create an instance of {@link FailedOnholdFulfillmentListType }
     * 
     */
    public FailedOnholdFulfillmentListType createFailedOnholdFulfillmentListType() {
        return new FailedOnholdFulfillmentListType();
    }

    /**
     * Create an instance of {@link DictionaryEntry }
     * 
     */
    public DictionaryEntry createDictionaryEntry() {
        return new DictionaryEntry();
    }

    /**
     * Create an instance of {@link DictionaryEntriesCollection }
     * 
     */
    public DictionaryEntriesCollection createDictionaryEntriesCollection() {
        return new DictionaryEntriesCollection();
    }

    /**
     * Create an instance of {@link Dictionary }
     * 
     */
    public Dictionary createDictionary() {
        return new Dictionary();
    }

    /**
     * Create an instance of {@link TypeLineItem }
     * 
     */
    public TypeLineItem createTypeLineItem() {
        return new TypeLineItem();
    }

    /**
     * Create an instance of {@link FailedLineItem }
     * 
     */
    public FailedLineItem createFailedLineItem() {
        return new FailedLineItem();
    }

    /**
     * Create an instance of {@link TransferHostIdDataType }
     * 
     */
    public TransferHostIdDataType createTransferHostIdDataType() {
        return new TransferHostIdDataType();
    }

    /**
     * Create an instance of {@link TransferHostList }
     * 
     */
    public TransferHostList createTransferHostList() {
        return new TransferHostList();
    }

    /**
     * Create an instance of {@link FailedTransferHostDataType }
     * 
     */
    public FailedTransferHostDataType createFailedTransferHostDataType() {
        return new FailedTransferHostDataType();
    }

    /**
     * Create an instance of {@link FailedTransferHostListDataType }
     * 
     */
    public FailedTransferHostListDataType createFailedTransferHostListDataType() {
        return new FailedTransferHostListDataType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentCountRequest")
    public JAXBElement<GetFulfillmentCountRequestType> createGetFulfillmentCountRequest(GetFulfillmentCountRequestType value) {
        return new JAXBElement<GetFulfillmentCountRequestType>(_GetFulfillmentCountRequest_QNAME, GetFulfillmentCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentCountResponse")
    public JAXBElement<GetFulfillmentCountResponseType> createGetFulfillmentCountResponse(GetFulfillmentCountResponseType value) {
        return new JAXBElement<GetFulfillmentCountResponseType>(_GetFulfillmentCountResponse_QNAME, GetFulfillmentCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentsQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentsQueryRequest")
    public JAXBElement<GetFulfillmentsQueryRequestType> createGetFulfillmentsQueryRequest(GetFulfillmentsQueryRequestType value) {
        return new JAXBElement<GetFulfillmentsQueryRequestType>(_GetFulfillmentsQueryRequest_QNAME, GetFulfillmentsQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentsQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentsQueryResponse")
    public JAXBElement<GetFulfillmentsQueryResponseType> createGetFulfillmentsQueryResponse(GetFulfillmentsQueryResponseType value) {
        return new JAXBElement<GetFulfillmentsQueryResponseType>(_GetFulfillmentsQueryResponse_QNAME, GetFulfillmentsQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentPropertiesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentPropertiesRequest")
    public JAXBElement<GetFulfillmentPropertiesRequestType> createGetFulfillmentPropertiesRequest(GetFulfillmentPropertiesRequestType value) {
        return new JAXBElement<GetFulfillmentPropertiesRequestType>(_GetFulfillmentPropertiesRequest_QNAME, GetFulfillmentPropertiesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentPropertiesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentPropertiesResponse")
    public JAXBElement<GetFulfillmentPropertiesResponseType> createGetFulfillmentPropertiesResponse(GetFulfillmentPropertiesResponseType value) {
        return new JAXBElement<GetFulfillmentPropertiesResponseType>(_GetFulfillmentPropertiesResponse_QNAME, GetFulfillmentPropertiesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RehostFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "rehostLicenseRequest")
    public JAXBElement<RehostFulfillmentRequestType> createRehostLicenseRequest(RehostFulfillmentRequestType value) {
        return new JAXBElement<RehostFulfillmentRequestType>(_RehostLicenseRequest_QNAME, RehostFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RehostFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "rehostLicenseResponse")
    public JAXBElement<RehostFulfillmentResponseType> createRehostLicenseResponse(RehostFulfillmentResponseType value) {
        return new JAXBElement<RehostFulfillmentResponseType>(_RehostLicenseResponse_QNAME, RehostFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "returnLicenseRequest")
    public JAXBElement<ReturnFulfillmentRequestType> createReturnLicenseRequest(ReturnFulfillmentRequestType value) {
        return new JAXBElement<ReturnFulfillmentRequestType>(_ReturnLicenseRequest_QNAME, ReturnFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "returnLicenseResponse")
    public JAXBElement<ReturnFulfillmentResponseType> createReturnLicenseResponse(ReturnFulfillmentResponseType value) {
        return new JAXBElement<ReturnFulfillmentResponseType>(_ReturnLicenseResponse_QNAME, ReturnFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepairFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "repairLicenseRequest")
    public JAXBElement<RepairFulfillmentRequestType> createRepairLicenseRequest(RepairFulfillmentRequestType value) {
        return new JAXBElement<RepairFulfillmentRequestType>(_RepairLicenseRequest_QNAME, RepairFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepairFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "repairLicenseResponse")
    public JAXBElement<RepairFulfillmentResponseType> createRepairLicenseResponse(RepairFulfillmentResponseType value) {
        return new JAXBElement<RepairFulfillmentResponseType>(_RepairLicenseResponse_QNAME, RepairFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmergencyFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emergencyLicenseRequest")
    public JAXBElement<EmergencyFulfillmentRequestType> createEmergencyLicenseRequest(EmergencyFulfillmentRequestType value) {
        return new JAXBElement<EmergencyFulfillmentRequestType>(_EmergencyLicenseRequest_QNAME, EmergencyFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmergencyFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emergencyLicenseResponse")
    public JAXBElement<EmergencyFulfillmentResponseType> createEmergencyLicenseResponse(EmergencyFulfillmentResponseType value) {
        return new JAXBElement<EmergencyFulfillmentResponseType>(_EmergencyLicenseResponse_QNAME, EmergencyFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublisherErrorFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "publisherErrorLicenseRequest")
    public JAXBElement<PublisherErrorFulfillmentRequestType> createPublisherErrorLicenseRequest(PublisherErrorFulfillmentRequestType value) {
        return new JAXBElement<PublisherErrorFulfillmentRequestType>(_PublisherErrorLicenseRequest_QNAME, PublisherErrorFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PublisherErrorFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "publisherErrorLicenseResponse")
    public JAXBElement<PublisherErrorFulfillmentResponseType> createPublisherErrorLicenseResponse(PublisherErrorFulfillmentResponseType value) {
        return new JAXBElement<PublisherErrorFulfillmentResponseType>(_PublisherErrorLicenseResponse_QNAME, PublisherErrorFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StopGapFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "stopGapLicenseRequest")
    public JAXBElement<StopGapFulfillmentRequestType> createStopGapLicenseRequest(StopGapFulfillmentRequestType value) {
        return new JAXBElement<StopGapFulfillmentRequestType>(_StopGapLicenseRequest_QNAME, StopGapFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StopGapFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "stopGapLicenseResponse")
    public JAXBElement<StopGapFulfillmentResponseType> createStopGapLicenseResponse(StopGapFulfillmentResponseType value) {
        return new JAXBElement<StopGapFulfillmentResponseType>(_StopGapLicenseResponse_QNAME, StopGapFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentAttributesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentAttributesRequest")
    public JAXBElement<GetFulfillmentAttributesRequestType> createGetFulfillmentAttributesRequest(GetFulfillmentAttributesRequestType value) {
        return new JAXBElement<GetFulfillmentAttributesRequestType>(_GetFulfillmentAttributesRequest_QNAME, GetFulfillmentAttributesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentAttributesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentAttributesResponse")
    public JAXBElement<GetFulfillmentAttributesResponseType> createGetFulfillmentAttributesResponse(GetFulfillmentAttributesResponseType value) {
        return new JAXBElement<GetFulfillmentAttributesResponseType>(_GetFulfillmentAttributesResponse_QNAME, GetFulfillmentAttributesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHostAttributesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getHostAttributesRequest")
    public JAXBElement<GetHostAttributesRequestType> createGetHostAttributesRequest(GetHostAttributesRequestType value) {
        return new JAXBElement<GetHostAttributesRequestType>(_GetHostAttributesRequest_QNAME, GetHostAttributesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetHostAttributesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getHostAttributesResponse")
    public JAXBElement<GetHostAttributesResponseType> createGetHostAttributesResponse(GetHostAttributesResponseType value) {
        return new JAXBElement<GetHostAttributesResponseType>(_GetHostAttributesResponse_QNAME, GetHostAttributesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "verifyCreateLicenseRequest")
    public JAXBElement<CreateFulfillmentRequestType> createVerifyCreateLicenseRequest(CreateFulfillmentRequestType value) {
        return new JAXBElement<CreateFulfillmentRequestType>(_VerifyCreateLicenseRequest_QNAME, CreateFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "verifyCreateLicenseResponse")
    public JAXBElement<CreateFulfillmentResponseType> createVerifyCreateLicenseResponse(CreateFulfillmentResponseType value) {
        return new JAXBElement<CreateFulfillmentResponseType>(_VerifyCreateLicenseResponse_QNAME, CreateFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createLicenseRequest")
    public JAXBElement<CreateFulfillmentRequestType> createCreateLicenseRequest(CreateFulfillmentRequestType value) {
        return new JAXBElement<CreateFulfillmentRequestType>(_CreateLicenseRequest_QNAME, CreateFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createLicenseResponse")
    public JAXBElement<CreateFulfillmentResponseType> createCreateLicenseResponse(CreateFulfillmentResponseType value) {
        return new JAXBElement<CreateFulfillmentResponseType>(_CreateLicenseResponse_QNAME, CreateFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivateShortCodeRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "activateShortCodeRequest")
    public JAXBElement<ActivateShortCodeRequestType> createActivateShortCodeRequest(ActivateShortCodeRequestType value) {
        return new JAXBElement<ActivateShortCodeRequestType>(_ActivateShortCodeRequest_QNAME, ActivateShortCodeRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivateShortCodeResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "activateShortCodeResponse")
    public JAXBElement<ActivateShortCodeResponseType> createActivateShortCodeResponse(ActivateShortCodeResponseType value) {
        return new JAXBElement<ActivateShortCodeResponseType>(_ActivateShortCodeResponse_QNAME, ActivateShortCodeResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepairShortCodeRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "repairShortCodeRequest")
    public JAXBElement<RepairShortCodeRequestType> createRepairShortCodeRequest(RepairShortCodeRequestType value) {
        return new JAXBElement<RepairShortCodeRequestType>(_RepairShortCodeRequest_QNAME, RepairShortCodeRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RepairShortCodeResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "repairShortCodeResponse")
    public JAXBElement<RepairShortCodeResponseType> createRepairShortCodeResponse(RepairShortCodeResponseType value) {
        return new JAXBElement<RepairShortCodeResponseType>(_RepairShortCodeResponse_QNAME, RepairShortCodeResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnShortCodeRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "returnShortCodeRequest")
    public JAXBElement<ReturnShortCodeRequestType> createReturnShortCodeRequest(ReturnShortCodeRequestType value) {
        return new JAXBElement<ReturnShortCodeRequestType>(_ReturnShortCodeRequest_QNAME, ReturnShortCodeRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnShortCodeResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "returnShortCodeResponse")
    public JAXBElement<ReturnShortCodeResponseType> createReturnShortCodeResponse(ReturnShortCodeResponseType value) {
        return new JAXBElement<ReturnShortCodeResponseType>(_ReturnShortCodeResponse_QNAME, ReturnShortCodeResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailLicenseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailLicenseRequest")
    public JAXBElement<EmailLicenseRequestType> createEmailLicenseRequest(EmailLicenseRequestType value) {
        return new JAXBElement<EmailLicenseRequestType>(_EmailLicenseRequest_QNAME, EmailLicenseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailLicenseResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailLicenseResponse")
    public JAXBElement<EmailLicenseResponseType> createEmailLicenseResponse(EmailLicenseResponseType value) {
        return new JAXBElement<EmailLicenseResponseType>(_EmailLicenseResponse_QNAME, EmailLicenseResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsolidateFulfillmentsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "consolidateFulfillmentsRequest")
    public JAXBElement<ConsolidateFulfillmentsRequestType> createConsolidateFulfillmentsRequest(ConsolidateFulfillmentsRequestType value) {
        return new JAXBElement<ConsolidateFulfillmentsRequestType>(_ConsolidateFulfillmentsRequest_QNAME, ConsolidateFulfillmentsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsolidateFulfillmentsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "consolidateFulfillmentsResponse")
    public JAXBElement<ConsolidateFulfillmentsResponseType> createConsolidateFulfillmentsResponse(ConsolidateFulfillmentsResponseType value) {
        return new JAXBElement<ConsolidateFulfillmentsResponseType>(_ConsolidateFulfillmentsResponse_QNAME, ConsolidateFulfillmentsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsolidatedFulfillmentCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getConsolidatedFulfillmentCountRequest")
    public JAXBElement<GetConsolidatedFulfillmentCountRequestType> createGetConsolidatedFulfillmentCountRequest(GetConsolidatedFulfillmentCountRequestType value) {
        return new JAXBElement<GetConsolidatedFulfillmentCountRequestType>(_GetConsolidatedFulfillmentCountRequest_QNAME, GetConsolidatedFulfillmentCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsolidatedFulfillmentCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getConsolidatedFulfillmentCountResponse")
    public JAXBElement<GetConsolidatedFulfillmentCountResponseType> createGetConsolidatedFulfillmentCountResponse(GetConsolidatedFulfillmentCountResponseType value) {
        return new JAXBElement<GetConsolidatedFulfillmentCountResponseType>(_GetConsolidatedFulfillmentCountResponse_QNAME, GetConsolidatedFulfillmentCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsolidatedFulfillmentsQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getConsolidatedFulfillmentsQueryRequest")
    public JAXBElement<GetConsolidatedFulfillmentsQueryRequestType> createGetConsolidatedFulfillmentsQueryRequest(GetConsolidatedFulfillmentsQueryRequestType value) {
        return new JAXBElement<GetConsolidatedFulfillmentsQueryRequestType>(_GetConsolidatedFulfillmentsQueryRequest_QNAME, GetConsolidatedFulfillmentsQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetConsolidatedFulfillmentsQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getConsolidatedFulfillmentsQueryResponse")
    public JAXBElement<GetConsolidatedFulfillmentsQueryResponseType> createGetConsolidatedFulfillmentsQueryResponse(GetConsolidatedFulfillmentsQueryResponseType value) {
        return new JAXBElement<GetConsolidatedFulfillmentsQueryResponseType>(_GetConsolidatedFulfillmentsQueryResponse_QNAME, GetConsolidatedFulfillmentsQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFmtAttributesForBatchActivationRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFmtAttributesForBatchActivationRequest")
    public JAXBElement<GetFmtAttributesForBatchActivationRequestType> createGetFmtAttributesForBatchActivationRequest(GetFmtAttributesForBatchActivationRequestType value) {
        return new JAXBElement<GetFmtAttributesForBatchActivationRequestType>(_GetFmtAttributesForBatchActivationRequest_QNAME, GetFmtAttributesForBatchActivationRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFmtAttributesForBatchActivationResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFmtAttributesForBatchActivationResponse")
    public JAXBElement<GetFmtAttributesForBatchActivationResponseType> createGetFmtAttributesForBatchActivationResponse(GetFmtAttributesForBatchActivationResponseType value) {
        return new JAXBElement<GetFmtAttributesForBatchActivationResponseType>(_GetFmtAttributesForBatchActivationResponse_QNAME, GetFmtAttributesForBatchActivationResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateLicensesAsBatchRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createLicensesAsBatchRequest")
    public JAXBElement<CreateLicensesAsBatchRequestType> createCreateLicensesAsBatchRequest(CreateLicensesAsBatchRequestType value) {
        return new JAXBElement<CreateLicensesAsBatchRequestType>(_CreateLicensesAsBatchRequest_QNAME, CreateLicensesAsBatchRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateLicensesAsBatchResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createLicensesAsBatchResponse")
    public JAXBElement<CreateLicensesAsBatchResponseType> createCreateLicensesAsBatchResponse(CreateLicensesAsBatchResponseType value) {
        return new JAXBElement<CreateLicensesAsBatchResponseType>(_CreateLicensesAsBatchResponse_QNAME, CreateLicensesAsBatchResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateLicensesAsBatchRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createLicensesAsBatchAndConsolidateRequest")
    public JAXBElement<CreateLicensesAsBatchRequestType> createCreateLicensesAsBatchAndConsolidateRequest(CreateLicensesAsBatchRequestType value) {
        return new JAXBElement<CreateLicensesAsBatchRequestType>(_CreateLicensesAsBatchAndConsolidateRequest_QNAME, CreateLicensesAsBatchRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsolidateFulfillmentsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createLicensesAsBatchAndConsolidateResponse")
    public JAXBElement<ConsolidateFulfillmentsResponseType> createCreateLicensesAsBatchAndConsolidateResponse(ConsolidateFulfillmentsResponseType value) {
        return new JAXBElement<ConsolidateFulfillmentsResponseType>(_CreateLicensesAsBatchAndConsolidateResponse_QNAME, ConsolidateFulfillmentsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailConsolidatedLicensesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailConsolidatedLicensesRequest")
    public JAXBElement<EmailConsolidatedLicensesRequestType> createEmailConsolidatedLicensesRequest(EmailConsolidatedLicensesRequestType value) {
        return new JAXBElement<EmailConsolidatedLicensesRequestType>(_EmailConsolidatedLicensesRequest_QNAME, EmailConsolidatedLicensesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailConsolidatedLicensesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailConsolidatedLicensesResponse")
    public JAXBElement<EmailConsolidatedLicensesResponseType> createEmailConsolidatedLicensesResponse(EmailConsolidatedLicensesResponseType value) {
        return new JAXBElement<EmailConsolidatedLicensesResponseType>(_EmailConsolidatedLicensesResponse_QNAME, EmailConsolidatedLicensesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrustedRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "manualActivationRequest")
    public JAXBElement<TrustedRequestType> createManualActivationRequest(TrustedRequestType value) {
        return new JAXBElement<TrustedRequestType>(_ManualActivationRequest_QNAME, TrustedRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrustedResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "manualActivationResponse")
    public JAXBElement<TrustedResponseType> createManualActivationResponse(TrustedResponseType value) {
        return new JAXBElement<TrustedResponseType>(_ManualActivationResponse_QNAME, TrustedResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrustedRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "manualRepairRequest")
    public JAXBElement<TrustedRequestType> createManualRepairRequest(TrustedRequestType value) {
        return new JAXBElement<TrustedRequestType>(_ManualRepairRequest_QNAME, TrustedRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrustedResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "manualRepairResponse")
    public JAXBElement<TrustedResponseType> createManualRepairResponse(TrustedResponseType value) {
        return new JAXBElement<TrustedResponseType>(_ManualRepairResponse_QNAME, TrustedResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrustedRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "manualReturnRequest")
    public JAXBElement<TrustedRequestType> createManualReturnRequest(TrustedRequestType value) {
        return new JAXBElement<TrustedRequestType>(_ManualReturnRequest_QNAME, TrustedRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TrustedResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "manualReturnResponse")
    public JAXBElement<TrustedResponseType> createManualReturnResponse(TrustedResponseType value) {
        return new JAXBElement<TrustedResponseType>(_ManualReturnResponse_QNAME, TrustedResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentHistoryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentHistoryRequest")
    public JAXBElement<GetFulfillmentHistoryRequestType> createGetFulfillmentHistoryRequest(GetFulfillmentHistoryRequestType value) {
        return new JAXBElement<GetFulfillmentHistoryRequestType>(_GetFulfillmentHistoryRequest_QNAME, GetFulfillmentHistoryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFulfillmentHistoryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getFulfillmentHistoryResponse")
    public JAXBElement<GetFulfillmentHistoryResponseType> createGetFulfillmentHistoryResponse(GetFulfillmentHistoryResponseType value) {
        return new JAXBElement<GetFulfillmentHistoryResponseType>(_GetFulfillmentHistoryResponse_QNAME, GetFulfillmentHistoryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateChildLineItemFulfillmentRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createChildLineItemFulfillmentRequest")
    public JAXBElement<CreateChildLineItemFulfillmentRequestType> createCreateChildLineItemFulfillmentRequest(CreateChildLineItemFulfillmentRequestType value) {
        return new JAXBElement<CreateChildLineItemFulfillmentRequestType>(_CreateChildLineItemFulfillmentRequest_QNAME, CreateChildLineItemFulfillmentRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateChildLineItemFulfillmentResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createChildLineItemFulfillmentResponse")
    public JAXBElement<CreateChildLineItemFulfillmentResponseType> createCreateChildLineItemFulfillmentResponse(CreateChildLineItemFulfillmentResponseType value) {
        return new JAXBElement<CreateChildLineItemFulfillmentResponseType>(_CreateChildLineItemFulfillmentResponse_QNAME, CreateChildLineItemFulfillmentResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdvancedFulfillmentLCRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upgradeFulfillmentRequest")
    public JAXBElement<AdvancedFulfillmentLCRequestType> createUpgradeFulfillmentRequest(AdvancedFulfillmentLCRequestType value) {
        return new JAXBElement<AdvancedFulfillmentLCRequestType>(_UpgradeFulfillmentRequest_QNAME, AdvancedFulfillmentLCRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdvancedFulfillmentLCResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upgradeFulfillmentResponse")
    public JAXBElement<AdvancedFulfillmentLCResponseType> createUpgradeFulfillmentResponse(AdvancedFulfillmentLCResponseType value) {
        return new JAXBElement<AdvancedFulfillmentLCResponseType>(_UpgradeFulfillmentResponse_QNAME, AdvancedFulfillmentLCResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdvancedFulfillmentLCRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upsellFulfillmentRequest")
    public JAXBElement<AdvancedFulfillmentLCRequestType> createUpsellFulfillmentRequest(AdvancedFulfillmentLCRequestType value) {
        return new JAXBElement<AdvancedFulfillmentLCRequestType>(_UpsellFulfillmentRequest_QNAME, AdvancedFulfillmentLCRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdvancedFulfillmentLCResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upsellFulfillmentResponse")
    public JAXBElement<AdvancedFulfillmentLCResponseType> createUpsellFulfillmentResponse(AdvancedFulfillmentLCResponseType value) {
        return new JAXBElement<AdvancedFulfillmentLCResponseType>(_UpsellFulfillmentResponse_QNAME, AdvancedFulfillmentLCResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdvancedFulfillmentLCRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "renewFulfillmentRequest")
    public JAXBElement<AdvancedFulfillmentLCRequestType> createRenewFulfillmentRequest(AdvancedFulfillmentLCRequestType value) {
        return new JAXBElement<AdvancedFulfillmentLCRequestType>(_RenewFulfillmentRequest_QNAME, AdvancedFulfillmentLCRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdvancedFulfillmentLCResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "renewFulfillmentResponse")
    public JAXBElement<AdvancedFulfillmentLCResponseType> createRenewFulfillmentResponse(AdvancedFulfillmentLCResponseType value) {
        return new JAXBElement<AdvancedFulfillmentLCResponseType>(_RenewFulfillmentResponse_QNAME, AdvancedFulfillmentLCResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetLicenseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setLicenseRequest")
    public JAXBElement<SetLicenseRequestType> createSetLicenseRequest(SetLicenseRequestType value) {
        return new JAXBElement<SetLicenseRequestType>(_SetLicenseRequest_QNAME, SetLicenseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetLicenseResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setLicenseResponse")
    public JAXBElement<SetLicenseResponseType> createSetLicenseResponse(SetLicenseResponseType value) {
        return new JAXBElement<SetLicenseResponseType>(_SetLicenseResponse_QNAME, SetLicenseResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteOnholdFulfillmentsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteOnholdFulfillmentsRequest")
    public JAXBElement<DeleteOnholdFulfillmentsRequestType> createDeleteOnholdFulfillmentsRequest(DeleteOnholdFulfillmentsRequestType value) {
        return new JAXBElement<DeleteOnholdFulfillmentsRequestType>(_DeleteOnholdFulfillmentsRequest_QNAME, DeleteOnholdFulfillmentsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteOnholdFulfillmentsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteOnholdFulfillmentsResponse")
    public JAXBElement<DeleteOnholdFulfillmentsResponseType> createDeleteOnholdFulfillmentsResponse(DeleteOnholdFulfillmentsResponseType value) {
        return new JAXBElement<DeleteOnholdFulfillmentsResponseType>(_DeleteOnholdFulfillmentsResponse_QNAME, DeleteOnholdFulfillmentsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivateLicensesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "activateLicensesRequest")
    public JAXBElement<ActivateLicensesRequestType> createActivateLicensesRequest(ActivateLicensesRequestType value) {
        return new JAXBElement<ActivateLicensesRequestType>(_ActivateLicensesRequest_QNAME, ActivateLicensesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ActivateLicensesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "activateLicensesResponse")
    public JAXBElement<ActivateLicensesResponseType> createActivateLicensesResponse(ActivateLicensesResponseType value) {
        return new JAXBElement<ActivateLicensesResponseType>(_ActivateLicensesResponse_QNAME, ActivateLicensesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferHostRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "transferHostRequest")
    public JAXBElement<TransferHostRequestType> createTransferHostRequest(TransferHostRequestType value) {
        return new JAXBElement<TransferHostRequestType>(_TransferHostRequest_QNAME, TransferHostRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferHostResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "transferHostResponse")
    public JAXBElement<TransferHostResponseType> createTransferHostResponse(TransferHostResponseType value) {
        return new JAXBElement<TransferHostResponseType>(_TransferHostResponse_QNAME, TransferHostResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "Reason", scope = TypeLineItem.class)
    public JAXBElement<String> createTypeLineItemReason(String value) {
        return new JAXBElement<String>(_TypeLineItemReason_QNAME, String.class, TypeLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "Count", scope = TypeLineItem.class)
    public JAXBElement<String> createTypeLineItemCount(String value) {
        return new JAXBElement<String>(_TypeLineItemCount_QNAME, String.class, TypeLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Dictionary }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "VendorDictionary", scope = TypeLineItem.class)
    public JAXBElement<Dictionary> createTypeLineItemVendorDictionary(Dictionary value) {
        return new JAXBElement<Dictionary>(_TypeLineItemVendorDictionary_QNAME, Dictionary.class, TypeLineItem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DictionaryEntriesCollection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "Entries", scope = Dictionary.class)
    public JAXBElement<DictionaryEntriesCollection> createDictionaryEntries(DictionaryEntriesCollection value) {
        return new JAXBElement<DictionaryEntriesCollection>(_DictionaryEntries_QNAME, DictionaryEntriesCollection.class, Dictionary.class, value);
    }

}
