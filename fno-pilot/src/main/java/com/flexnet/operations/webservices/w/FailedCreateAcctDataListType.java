
package com.flexnet.operations.webservices.w;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedCreateAcctDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedCreateAcctDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedAcct" type="{urn:v2.webservices.operations.flexnet.com}failedCreateAcctDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedCreateAcctDataListType", propOrder = {
    "failedAcct"
})
public class FailedCreateAcctDataListType {

    protected List<FailedCreateAcctDataType> failedAcct;

    /**
     * Gets the value of the failedAcct property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedAcct property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedAcct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedCreateAcctDataType }
     * 
     * 
     */
    public List<FailedCreateAcctDataType> getFailedAcct() {
        if (failedAcct == null) {
            failedAcct = new ArrayList<FailedCreateAcctDataType>();
        }
        return this.failedAcct;
    }

}
