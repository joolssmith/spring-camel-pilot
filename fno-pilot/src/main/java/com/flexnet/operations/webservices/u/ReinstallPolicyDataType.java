
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reinstallPolicyDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reinstallPolicyDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="defaultPolicy" type="{urn:v2.webservices.operations.flexnet.com}policyDataType" minOccurs="0"/&gt;
 *         &lt;element name="advancedPolicy" type="{urn:v2.webservices.operations.flexnet.com}advancedReinstallPolicyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reinstallPolicyDataType", propOrder = {
    "defaultPolicy",
    "advancedPolicy"
})
public class ReinstallPolicyDataType {

    protected PolicyDataType defaultPolicy;
    protected AdvancedReinstallPolicyType advancedPolicy;

    /**
     * Gets the value of the defaultPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getDefaultPolicy() {
        return defaultPolicy;
    }

    /**
     * Sets the value of the defaultPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setDefaultPolicy(PolicyDataType value) {
        this.defaultPolicy = value;
    }

    /**
     * Gets the value of the advancedPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link AdvancedReinstallPolicyType }
     *     
     */
    public AdvancedReinstallPolicyType getAdvancedPolicy() {
        return advancedPolicy;
    }

    /**
     * Sets the value of the advancedPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvancedReinstallPolicyType }
     *     
     */
    public void setAdvancedPolicy(AdvancedReinstallPolicyType value) {
        this.advancedPolicy = value;
    }

}
