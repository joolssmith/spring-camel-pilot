
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedLinkMaintenanceLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedLinkMaintenanceLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="linkMaintenanceLineItem" type="{urn:com.macrovision:flexnet/operations}linkMaintenanceLineItemDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedLinkMaintenanceLineItemDataType", propOrder = {
    "linkMaintenanceLineItem",
    "reason"
})
public class FailedLinkMaintenanceLineItemDataType {

    @XmlElement(required = true)
    protected LinkMaintenanceLineItemDataType linkMaintenanceLineItem;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the linkMaintenanceLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link LinkMaintenanceLineItemDataType }
     *     
     */
    public LinkMaintenanceLineItemDataType getLinkMaintenanceLineItem() {
        return linkMaintenanceLineItem;
    }

    /**
     * Sets the value of the linkMaintenanceLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkMaintenanceLineItemDataType }
     *     
     */
    public void setLinkMaintenanceLineItem(LinkMaintenanceLineItemDataType value) {
        this.linkMaintenanceLineItem = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
