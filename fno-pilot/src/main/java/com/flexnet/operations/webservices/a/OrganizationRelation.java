
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganizationRelation.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganizationRelation"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="IDENTICAL"/&gt;
 *     &lt;enumeration value="ANCESTOR"/&gt;
 *     &lt;enumeration value="DESCENDANT"/&gt;
 *     &lt;enumeration value="UNRELATED"/&gt;
 *     &lt;enumeration value="UNKNOWN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OrganizationRelation")
@XmlEnum
public enum OrganizationRelation {

    IDENTICAL,
    ANCESTOR,
    DESCENDANT,
    UNRELATED,
    UNKNOWN;

    public String value() {
        return name();
    }

    public static OrganizationRelation fromValue(String v) {
        return valueOf(v);
    }

}
