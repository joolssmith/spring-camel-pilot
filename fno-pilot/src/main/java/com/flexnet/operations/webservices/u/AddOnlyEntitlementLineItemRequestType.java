
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addOnlyEntitlementLineItemRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addOnlyEntitlementLineItemRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItem" type="{urn:v2.webservices.operations.flexnet.com}addEntitlementLineItemDataType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="opType" type="{urn:v2.webservices.operations.flexnet.com}CreateOrUpdateOperationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addOnlyEntitlementLineItemRequestType", propOrder = {
    "lineItem",
    "opType"
})
public class AddOnlyEntitlementLineItemRequestType {

    @XmlElement(required = true)
    protected List<AddEntitlementLineItemDataType> lineItem;
    @XmlSchemaType(name = "NMTOKEN")
    protected CreateOrUpdateOperationType opType;

    /**
     * Gets the value of the lineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddEntitlementLineItemDataType }
     * 
     * 
     */
    public List<AddEntitlementLineItemDataType> getLineItem() {
        if (lineItem == null) {
            lineItem = new ArrayList<AddEntitlementLineItemDataType>();
        }
        return this.lineItem;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrUpdateOperationType }
     *     
     */
    public CreateOrUpdateOperationType getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrUpdateOperationType }
     *     
     */
    public void setOpType(CreateOrUpdateOperationType value) {
        this.opType = value;
    }

}
