
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for modelAttributesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="modelAttributesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="needServerId" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needNodeLockId" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needCount" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needOverdraftCount" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "modelAttributesType", propOrder = {
    "needServerId",
    "needNodeLockId",
    "needCount",
    "needOverdraftCount"
})
public class ModelAttributesType {

    protected boolean needServerId;
    protected boolean needNodeLockId;
    protected boolean needCount;
    protected boolean needOverdraftCount;

    /**
     * Gets the value of the needServerId property.
     * 
     */
    public boolean isNeedServerId() {
        return needServerId;
    }

    /**
     * Sets the value of the needServerId property.
     * 
     */
    public void setNeedServerId(boolean value) {
        this.needServerId = value;
    }

    /**
     * Gets the value of the needNodeLockId property.
     * 
     */
    public boolean isNeedNodeLockId() {
        return needNodeLockId;
    }

    /**
     * Sets the value of the needNodeLockId property.
     * 
     */
    public void setNeedNodeLockId(boolean value) {
        this.needNodeLockId = value;
    }

    /**
     * Gets the value of the needCount property.
     * 
     */
    public boolean isNeedCount() {
        return needCount;
    }

    /**
     * Sets the value of the needCount property.
     * 
     */
    public void setNeedCount(boolean value) {
        this.needCount = value;
    }

    /**
     * Gets the value of the needOverdraftCount property.
     * 
     */
    public boolean isNeedOverdraftCount() {
        return needOverdraftCount;
    }

    /**
     * Sets the value of the needOverdraftCount property.
     * 
     */
    public void setNeedOverdraftCount(boolean value) {
        this.needOverdraftCount = value;
    }

}
