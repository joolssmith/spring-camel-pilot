
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedRepairShortCodeDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedRepairShortCodeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shortCodeData" type="{urn:v1.webservices.operations.flexnet.com}repairShortCodeDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedRepairShortCodeDataType", propOrder = {
    "shortCodeData",
    "reason"
})
public class FailedRepairShortCodeDataType {

    @XmlElement(required = true)
    protected RepairShortCodeDataType shortCodeData;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the shortCodeData property.
     * 
     * @return
     *     possible object is
     *     {@link RepairShortCodeDataType }
     *     
     */
    public RepairShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }

    /**
     * Sets the value of the shortCodeData property.
     * 
     * @param value
     *     allowed object is
     *     {@link RepairShortCodeDataType }
     *     
     */
    public void setShortCodeData(RepairShortCodeDataType value) {
        this.shortCodeData = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
