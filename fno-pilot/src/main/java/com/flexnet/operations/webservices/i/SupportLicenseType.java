
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SupportLicenseType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="SupportLicenseType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="MASTER"/&gt;
 *     &lt;enumeration value="REHOST"/&gt;
 *     &lt;enumeration value="RETURN"/&gt;
 *     &lt;enumeration value="REPAIR"/&gt;
 *     &lt;enumeration value="STOPGAP"/&gt;
 *     &lt;enumeration value="EMERGENCY"/&gt;
 *     &lt;enumeration value="PUBLISHER_ERROR"/&gt;
 *     &lt;enumeration value="TRANSFER"/&gt;
 *     &lt;enumeration value="UPGRADE"/&gt;
 *     &lt;enumeration value="UPSELL"/&gt;
 *     &lt;enumeration value="REGENERATE"/&gt;
 *     &lt;enumeration value="REINSTALL"/&gt;
 *     &lt;enumeration value="RENEW"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SupportLicenseType")
@XmlEnum
public enum SupportLicenseType {

    MASTER,
    REHOST,
    RETURN,
    REPAIR,
    STOPGAP,
    EMERGENCY,
    PUBLISHER_ERROR,
    TRANSFER,
    UPGRADE,
    UPSELL,
    REGENERATE,
    REINSTALL,
    RENEW;

    public String value() {
        return name();
    }

    public static SupportLicenseType fromValue(String v) {
        return valueOf(v);
    }

}
