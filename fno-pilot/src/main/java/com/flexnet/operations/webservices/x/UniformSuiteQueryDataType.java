
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for uniformSuiteQueryDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="uniformSuiteQueryDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="suiteName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v2.webservices.operations.flexnet.com}StateType"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v2.webservices.operations.flexnet.com}licenseTechnologyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseGenerator" type="{urn:v2.webservices.operations.flexnet.com}licenseGeneratorIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="packageProperties" type="{urn:v2.webservices.operations.flexnet.com}packagePropertiesDataType" minOccurs="0"/&gt;
 *         &lt;element name="products" type="{urn:v2.webservices.operations.flexnet.com}productsListType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModels" type="{urn:v2.webservices.operations.flexnet.com}licenseModelsListType" minOccurs="0"/&gt;
 *         &lt;element name="trustedKey" type="{urn:v2.webservices.operations.flexnet.com}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="virtualTrustedKey" type="{urn:v2.webservices.operations.flexnet.com}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumbers" type="{urn:v2.webservices.operations.flexnet.com}partNumbersListType" minOccurs="0"/&gt;
 *         &lt;element name="hostType" type="{urn:v2.webservices.operations.flexnet.com}hostTypePKType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypes" type="{urn:v2.webservices.operations.flexnet.com}hostTypeListType" minOccurs="0"/&gt;
 *         &lt;element name="usedOnDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v2.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInAdmin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="productAttributes" type="{urn:v2.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "uniformSuiteQueryDataType", propOrder = {
    "uniqueId",
    "suiteName",
    "version",
    "description",
    "state",
    "licenseTechnology",
    "licenseGenerator",
    "packageProperties",
    "products",
    "licenseModels",
    "trustedKey",
    "virtualTrustedKey",
    "partNumbers",
    "hostType",
    "hostTypes",
    "usedOnDevice",
    "productCategory",
    "customAttributes",
    "allowDownloadObsoleteFrInAdmin",
    "allowDownloadObsoleteFrInPortal",
    "creationDate",
    "lastModifiedDate",
    "productAttributes"
})
public class UniformSuiteQueryDataType {

    @XmlElement(required = true)
    protected String uniqueId;
    @XmlElement(required = true)
    protected String suiteName;
    protected String version;
    protected String description;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    protected LicenseTechnologyIdentifierType licenseTechnology;
    protected LicenseGeneratorIdentifierType licenseGenerator;
    protected PackagePropertiesDataType packageProperties;
    protected ProductsListType products;
    protected LicenseModelsListType licenseModels;
    protected TrustedKeyIdentifierType trustedKey;
    protected TrustedKeyIdentifierType virtualTrustedKey;
    protected PartNumbersListType partNumbers;
    protected HostTypePKType hostType;
    protected HostTypeListType hostTypes;
    protected Boolean usedOnDevice;
    protected String productCategory;
    protected AttributeDescriptorDataType customAttributes;
    protected Boolean allowDownloadObsoleteFrInAdmin;
    protected Boolean allowDownloadObsoleteFrInPortal;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDate;
    protected AttributeDescriptorDataType productAttributes;

    /**
     * Gets the value of the uniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Sets the value of the uniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueId(String value) {
        this.uniqueId = value;
    }

    /**
     * Gets the value of the suiteName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSuiteName() {
        return suiteName;
    }

    /**
     * Sets the value of the suiteName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSuiteName(String value) {
        this.suiteName = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the licenseGenerator property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public LicenseGeneratorIdentifierType getLicenseGenerator() {
        return licenseGenerator;
    }

    /**
     * Sets the value of the licenseGenerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public void setLicenseGenerator(LicenseGeneratorIdentifierType value) {
        this.licenseGenerator = value;
    }

    /**
     * Gets the value of the packageProperties property.
     * 
     * @return
     *     possible object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public PackagePropertiesDataType getPackageProperties() {
        return packageProperties;
    }

    /**
     * Sets the value of the packageProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public void setPackageProperties(PackagePropertiesDataType value) {
        this.packageProperties = value;
    }

    /**
     * Gets the value of the products property.
     * 
     * @return
     *     possible object is
     *     {@link ProductsListType }
     *     
     */
    public ProductsListType getProducts() {
        return products;
    }

    /**
     * Sets the value of the products property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductsListType }
     *     
     */
    public void setProducts(ProductsListType value) {
        this.products = value;
    }

    /**
     * Gets the value of the licenseModels property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelsListType }
     *     
     */
    public LicenseModelsListType getLicenseModels() {
        return licenseModels;
    }

    /**
     * Sets the value of the licenseModels property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelsListType }
     *     
     */
    public void setLicenseModels(LicenseModelsListType value) {
        this.licenseModels = value;
    }

    /**
     * Gets the value of the trustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getTrustedKey() {
        return trustedKey;
    }

    /**
     * Sets the value of the trustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setTrustedKey(TrustedKeyIdentifierType value) {
        this.trustedKey = value;
    }

    /**
     * Gets the value of the virtualTrustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getVirtualTrustedKey() {
        return virtualTrustedKey;
    }

    /**
     * Sets the value of the virtualTrustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setVirtualTrustedKey(TrustedKeyIdentifierType value) {
        this.virtualTrustedKey = value;
    }

    /**
     * Gets the value of the partNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumbersListType }
     *     
     */
    public PartNumbersListType getPartNumbers() {
        return partNumbers;
    }

    /**
     * Sets the value of the partNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumbersListType }
     *     
     */
    public void setPartNumbers(PartNumbersListType value) {
        this.partNumbers = value;
    }

    /**
     * Gets the value of the hostType property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypePKType }
     *     
     */
    public HostTypePKType getHostType() {
        return hostType;
    }

    /**
     * Sets the value of the hostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypePKType }
     *     
     */
    public void setHostType(HostTypePKType value) {
        this.hostType = value;
    }

    /**
     * Gets the value of the hostTypes property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypeListType }
     *     
     */
    public HostTypeListType getHostTypes() {
        return hostTypes;
    }

    /**
     * Sets the value of the hostTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypeListType }
     *     
     */
    public void setHostTypes(HostTypeListType value) {
        this.hostTypes = value;
    }

    /**
     * Gets the value of the usedOnDevice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsedOnDevice() {
        return usedOnDevice;
    }

    /**
     * Sets the value of the usedOnDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsedOnDevice(Boolean value) {
        this.usedOnDevice = value;
    }

    /**
     * Gets the value of the productCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategory() {
        return productCategory;
    }

    /**
     * Sets the value of the productCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategory(String value) {
        this.productCategory = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInAdmin() {
        return allowDownloadObsoleteFrInAdmin;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInAdmin(Boolean value) {
        this.allowDownloadObsoleteFrInAdmin = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInPortal() {
        return allowDownloadObsoleteFrInPortal;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInPortal(Boolean value) {
        this.allowDownloadObsoleteFrInPortal = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the lastModifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets the value of the lastModifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDate(XMLGregorianCalendar value) {
        this.lastModifiedDate = value;
    }

    /**
     * Gets the value of the productAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getProductAttributes() {
        return productAttributes;
    }

    /**
     * Sets the value of the productAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setProductAttributes(AttributeDescriptorDataType value) {
        this.productAttributes = value;
    }

}
