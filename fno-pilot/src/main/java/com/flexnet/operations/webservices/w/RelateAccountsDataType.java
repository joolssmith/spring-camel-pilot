
package com.flexnet.operations.webservices.w;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for relateAccountsDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="relateAccountsDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountToRelate" type="{urn:v2.webservices.operations.flexnet.com}accountIdentifierType"/&gt;
 *         &lt;element name="relatedAccount" type="{urn:v2.webservices.operations.flexnet.com}accountIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "relateAccountsDataType", propOrder = {
    "accountToRelate",
    "relatedAccount"
})
public class RelateAccountsDataType {

    @XmlElement(required = true)
    protected AccountIdentifierType accountToRelate;
    @XmlElement(required = true)
    protected AccountIdentifierType relatedAccount;

    /**
     * Gets the value of the accountToRelate property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifierType }
     *     
     */
    public AccountIdentifierType getAccountToRelate() {
        return accountToRelate;
    }

    /**
     * Sets the value of the accountToRelate property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifierType }
     *     
     */
    public void setAccountToRelate(AccountIdentifierType value) {
        this.accountToRelate = value;
    }

    /**
     * Gets the value of the relatedAccount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifierType }
     *     
     */
    public AccountIdentifierType getRelatedAccount() {
        return relatedAccount;
    }

    /**
     * Sets the value of the relatedAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifierType }
     *     
     */
    public void setRelatedAccount(AccountIdentifierType value) {
        this.relatedAccount = value;
    }

}
