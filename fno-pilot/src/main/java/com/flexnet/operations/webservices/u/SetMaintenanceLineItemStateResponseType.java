
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setMaintenanceLineItemStateResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setMaintenanceLineItemStateResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v2.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="failedMaintenanceData" type="{urn:v2.webservices.operations.flexnet.com}failedMaintenanceLineItemStateDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setMaintenanceLineItemStateResponseType", propOrder = {
    "statusInfo",
    "failedMaintenanceData"
})
public class SetMaintenanceLineItemStateResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected FailedMaintenanceLineItemStateDataListType failedMaintenanceData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedMaintenanceData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedMaintenanceLineItemStateDataListType }
     *     
     */
    public FailedMaintenanceLineItemStateDataListType getFailedMaintenanceData() {
        return failedMaintenanceData;
    }

    /**
     * Sets the value of the failedMaintenanceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedMaintenanceLineItemStateDataListType }
     *     
     */
    public void setFailedMaintenanceData(FailedMaintenanceLineItemStateDataListType value) {
        this.failedMaintenanceData = value;
    }

}
