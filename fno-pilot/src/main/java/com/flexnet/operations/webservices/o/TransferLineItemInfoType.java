
package com.flexnet.operations.webservices.o;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transferLineItemInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferLineItemInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="numberOfCopies" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="useSameActivationId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="customActivationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="generateActivationId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="entitlementBelongsTo" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferLineItemInfoType", propOrder = {
    "lineItemIdentifier",
    "numberOfCopies",
    "useSameActivationId",
    "customActivationId",
    "generateActivationId",
    "entitlementBelongsTo"
})
public class TransferLineItemInfoType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType lineItemIdentifier;
    @XmlElement(required = true)
    protected BigInteger numberOfCopies;
    protected Boolean useSameActivationId;
    protected String customActivationId;
    protected Boolean generateActivationId;
    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementBelongsTo;

    /**
     * Gets the value of the lineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getLineItemIdentifier() {
        return lineItemIdentifier;
    }

    /**
     * Sets the value of the lineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.lineItemIdentifier = value;
    }

    /**
     * Gets the value of the numberOfCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * Sets the value of the numberOfCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCopies(BigInteger value) {
        this.numberOfCopies = value;
    }

    /**
     * Gets the value of the useSameActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUseSameActivationId() {
        return useSameActivationId;
    }

    /**
     * Sets the value of the useSameActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUseSameActivationId(Boolean value) {
        this.useSameActivationId = value;
    }

    /**
     * Gets the value of the customActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomActivationId() {
        return customActivationId;
    }

    /**
     * Sets the value of the customActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomActivationId(String value) {
        this.customActivationId = value;
    }

    /**
     * Gets the value of the generateActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isGenerateActivationId() {
        return generateActivationId;
    }

    /**
     * Sets the value of the generateActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setGenerateActivationId(Boolean value) {
        this.generateActivationId = value;
    }

    /**
     * Gets the value of the entitlementBelongsTo property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementBelongsTo() {
        return entitlementBelongsTo;
    }

    /**
     * Sets the value of the entitlementBelongsTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementBelongsTo(EntitlementIdentifierType value) {
        this.entitlementBelongsTo = value;
    }

}
