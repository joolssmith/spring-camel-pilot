
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for productRelationshipDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="productRelationshipDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productToRelate" type="{urn:com.macrovision:flexnet/operations}productIdentifierType"/&gt;
 *         &lt;element name="relatedProduct" type="{urn:com.macrovision:flexnet/operations}productIdentifierType"/&gt;
 *         &lt;element name="relation" type="{urn:com.macrovision:flexnet/operations}relationshipType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "productRelationshipDataType", propOrder = {
    "productToRelate",
    "relatedProduct",
    "relation"
})
@XmlSeeAlso({
    UpdateProductRelationshipDataType.class
})
public class ProductRelationshipDataType {

    @XmlElement(required = true)
    protected ProductIdentifierType productToRelate;
    @XmlElement(required = true)
    protected ProductIdentifierType relatedProduct;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected RelationshipType relation;

    /**
     * Gets the value of the productToRelate property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getProductToRelate() {
        return productToRelate;
    }

    /**
     * Sets the value of the productToRelate property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setProductToRelate(ProductIdentifierType value) {
        this.productToRelate = value;
    }

    /**
     * Gets the value of the relatedProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getRelatedProduct() {
        return relatedProduct;
    }

    /**
     * Sets the value of the relatedProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setRelatedProduct(ProductIdentifierType value) {
        this.relatedProduct = value;
    }

    /**
     * Gets the value of the relation property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipType }
     *     
     */
    public RelationshipType getRelation() {
        return relation;
    }

    /**
     * Sets the value of the relation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipType }
     *     
     */
    public void setRelation(RelationshipType value) {
        this.relation = value;
    }

}
