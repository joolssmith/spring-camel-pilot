
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedSetLicenseOnholdFulfillmentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedSetLicenseOnholdFulfillmentDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="onholdFmtLicenseData" type="{urn:v1.webservices.operations.flexnet.com}onHoldFmtLicenseDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedSetLicenseOnholdFulfillmentDataType", propOrder = {
    "onholdFmtLicenseData",
    "reason"
})
public class FailedSetLicenseOnholdFulfillmentDataType {

    @XmlElement(required = true)
    protected OnHoldFmtLicenseDataType onholdFmtLicenseData;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the onholdFmtLicenseData property.
     * 
     * @return
     *     possible object is
     *     {@link OnHoldFmtLicenseDataType }
     *     
     */
    public OnHoldFmtLicenseDataType getOnholdFmtLicenseData() {
        return onholdFmtLicenseData;
    }

    /**
     * Sets the value of the onholdFmtLicenseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnHoldFmtLicenseDataType }
     *     
     */
    public void setOnholdFmtLicenseData(OnHoldFmtLicenseDataType value) {
        this.onholdFmtLicenseData = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
