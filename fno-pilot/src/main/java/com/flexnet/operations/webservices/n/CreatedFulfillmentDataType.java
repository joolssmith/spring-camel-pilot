
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdFulfillmentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdFulfillmentDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="recordRefNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="uniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fulfillmentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="licenseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="binaryLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="licenseFiles" type="{urn:v1.webservices.operations.flexnet.com}licenseFileDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdFulfillmentDataType", propOrder = {
    "recordRefNo",
    "uniqueId",
    "fulfillmentId",
    "licenseText",
    "binaryLicense",
    "licenseFiles"
})
public class CreatedFulfillmentDataType {

    @XmlElement(required = true)
    protected String recordRefNo;
    @XmlElement(required = true)
    protected String uniqueId;
    @XmlElement(required = true)
    protected String fulfillmentId;
    protected String licenseText;
    protected byte[] binaryLicense;
    protected LicenseFileDataListType licenseFiles;

    /**
     * Gets the value of the recordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordRefNo() {
        return recordRefNo;
    }

    /**
     * Sets the value of the recordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordRefNo(String value) {
        this.recordRefNo = value;
    }

    /**
     * Gets the value of the uniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Sets the value of the uniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueId(String value) {
        this.uniqueId = value;
    }

    /**
     * Gets the value of the fulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentId() {
        return fulfillmentId;
    }

    /**
     * Sets the value of the fulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentId(String value) {
        this.fulfillmentId = value;
    }

    /**
     * Gets the value of the licenseText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseText() {
        return licenseText;
    }

    /**
     * Sets the value of the licenseText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseText(String value) {
        this.licenseText = value;
    }

    /**
     * Gets the value of the binaryLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }

    /**
     * Sets the value of the binaryLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBinaryLicense(byte[] value) {
        this.binaryLicense = value;
    }

    /**
     * Gets the value of the licenseFiles property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public LicenseFileDataListType getLicenseFiles() {
        return licenseFiles;
    }

    /**
     * Sets the value of the licenseFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public void setLicenseFiles(LicenseFileDataListType value) {
        this.licenseFiles = value;
    }

}
