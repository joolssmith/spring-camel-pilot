
package com.flexnet.operations.webservices.z;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for returnHostRequestListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="returnHostRequestListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="returnHosts" type="{urn:v3.fne.webservices.operations.flexnet.com}returnHostType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "returnHostRequestListType", propOrder = {
    "returnHosts"
})
public class ReturnHostRequestListType {

    @XmlElement(required = true)
    protected List<ReturnHostType> returnHosts;

    /**
     * Gets the value of the returnHosts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the returnHosts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReturnHosts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReturnHostType }
     * 
     * 
     */
    public List<ReturnHostType> getReturnHosts() {
        if (returnHosts == null) {
            returnHosts = new ArrayList<ReturnHostType>();
        }
        return this.returnHosts;
    }

}
