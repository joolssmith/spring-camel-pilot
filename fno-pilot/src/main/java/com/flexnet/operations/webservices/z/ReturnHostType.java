
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for returnHostType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="returnHostType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hostIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceIdentifier"/&gt;
 *         &lt;element name="targetHostIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceIdentifier" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "returnHostType", propOrder = {
    "hostIdentifier",
    "targetHostIdentifier"
})
public class ReturnHostType {

    @XmlElement(required = true)
    protected DeviceIdentifier hostIdentifier;
    protected DeviceIdentifier targetHostIdentifier;

    /**
     * Gets the value of the hostIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getHostIdentifier() {
        return hostIdentifier;
    }

    /**
     * Sets the value of the hostIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setHostIdentifier(DeviceIdentifier value) {
        this.hostIdentifier = value;
    }

    /**
     * Gets the value of the targetHostIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getTargetHostIdentifier() {
        return targetHostIdentifier;
    }

    /**
     * Sets the value of the targetHostIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setTargetHostIdentifier(DeviceIdentifier value) {
        this.targetHostIdentifier = value;
    }

}
