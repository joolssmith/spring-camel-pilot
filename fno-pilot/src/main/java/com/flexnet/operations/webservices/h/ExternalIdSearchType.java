
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for externalIdSearchType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="externalIdSearchType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="EQUALS"/&gt;
 *     &lt;enumeration value="NOT_EQUALS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "externalIdSearchType")
@XmlEnum
public enum ExternalIdSearchType {

    EQUALS,
    NOT_EQUALS;

    public String value() {
        return name();
    }

    public static ExternalIdSearchType fromValue(String v) {
        return valueOf(v);
    }

}
