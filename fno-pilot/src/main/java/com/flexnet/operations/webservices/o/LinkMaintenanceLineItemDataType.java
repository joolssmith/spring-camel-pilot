
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for linkMaintenanceLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="linkMaintenanceLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="maintenanceLineItemIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linkMaintenanceLineItemDataType", propOrder = {
    "lineItemIdentifier",
    "maintenanceLineItemIdentifier"
})
public class LinkMaintenanceLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType lineItemIdentifier;
    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType maintenanceLineItemIdentifier;

    /**
     * Gets the value of the lineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getLineItemIdentifier() {
        return lineItemIdentifier;
    }

    /**
     * Sets the value of the lineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.lineItemIdentifier = value;
    }

    /**
     * Gets the value of the maintenanceLineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getMaintenanceLineItemIdentifier() {
        return maintenanceLineItemIdentifier;
    }

    /**
     * Sets the value of the maintenanceLineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setMaintenanceLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.maintenanceLineItemIdentifier = value;
    }

}
