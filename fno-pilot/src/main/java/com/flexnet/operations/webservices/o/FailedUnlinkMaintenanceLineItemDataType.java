
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUnlinkMaintenanceLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUnlinkMaintenanceLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="unlinkMaintenanceLineItem" type="{urn:v1.webservices.operations.flexnet.com}unlinkMaintenanceLineItemDataType" minOccurs="0"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUnlinkMaintenanceLineItemDataType", propOrder = {
    "unlinkMaintenanceLineItem",
    "reason"
})
public class FailedUnlinkMaintenanceLineItemDataType {

    protected UnlinkMaintenanceLineItemDataType unlinkMaintenanceLineItem;
    protected String reason;

    /**
     * Gets the value of the unlinkMaintenanceLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link UnlinkMaintenanceLineItemDataType }
     *     
     */
    public UnlinkMaintenanceLineItemDataType getUnlinkMaintenanceLineItem() {
        return unlinkMaintenanceLineItem;
    }

    /**
     * Sets the value of the unlinkMaintenanceLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnlinkMaintenanceLineItemDataType }
     *     
     */
    public void setUnlinkMaintenanceLineItem(UnlinkMaintenanceLineItemDataType value) {
        this.unlinkMaintenanceLineItem = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
