
package com.flexnet.operations.webservices.y;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for renewParametersDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="renewParametersDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{urn:v3.webservices.operations.flexnet.com}idType"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationTerms" type="{urn:v3.webservices.operations.flexnet.com}expirationTermsDataType" minOccurs="0"/&gt;
 *         &lt;element name="numberOfCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="startDateOption" type="{urn:v3.webservices.operations.flexnet.com}StartDateOptionType" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDateAttributes" type="{urn:v3.webservices.operations.flexnet.com}versionDateAttributesType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseModel" type="{urn:v3.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel1" type="{urn:v3.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel2" type="{urn:v3.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModelAttributes" type="{urn:v3.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="policyAttributes" type="{urn:v3.webservices.operations.flexnet.com}policyAttributesListType" minOccurs="0"/&gt;
 *         &lt;element name="FNPTimeZoneValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lineItemAttributes" type="{urn:v3.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "renewParametersDataType", propOrder = {
    "activationId",
    "isPermanent",
    "expirationTerms",
    "numberOfCopies",
    "startDate",
    "startDateOption",
    "versionDate",
    "versionDateAttributes",
    "description",
    "orderId",
    "orderLineNumber",
    "licenseModel",
    "alternateLicenseModel1",
    "alternateLicenseModel2",
    "licenseModelAttributes",
    "policyAttributes",
    "fnpTimeZoneValue",
    "lineItemAttributes"
})
public class RenewParametersDataType {

    @XmlElement(required = true)
    protected IdType activationId;
    protected Boolean isPermanent;
    protected ExpirationTermsDataType expirationTerms;
    protected BigInteger numberOfCopies;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "NMTOKEN")
    protected StartDateOptionType startDateOption;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar versionDate;
    protected VersionDateAttributesType versionDateAttributes;
    protected String description;
    protected String orderId;
    protected String orderLineNumber;
    protected LicenseModelIdentifierType licenseModel;
    protected LicenseModelIdentifierType alternateLicenseModel1;
    protected LicenseModelIdentifierType alternateLicenseModel2;
    protected AttributeDescriptorDataType licenseModelAttributes;
    protected PolicyAttributesListType policyAttributes;
    @XmlElement(name = "FNPTimeZoneValue")
    protected String fnpTimeZoneValue;
    protected AttributeDescriptorDataType lineItemAttributes;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link IdType }
     *     
     */
    public IdType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdType }
     *     
     */
    public void setActivationId(IdType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the expirationTerms property.
     * 
     * @return
     *     possible object is
     *     {@link ExpirationTermsDataType }
     *     
     */
    public ExpirationTermsDataType getExpirationTerms() {
        return expirationTerms;
    }

    /**
     * Sets the value of the expirationTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpirationTermsDataType }
     *     
     */
    public void setExpirationTerms(ExpirationTermsDataType value) {
        this.expirationTerms = value;
    }

    /**
     * Gets the value of the numberOfCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * Sets the value of the numberOfCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCopies(BigInteger value) {
        this.numberOfCopies = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the startDateOption property.
     * 
     * @return
     *     possible object is
     *     {@link StartDateOptionType }
     *     
     */
    public StartDateOptionType getStartDateOption() {
        return startDateOption;
    }

    /**
     * Sets the value of the startDateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link StartDateOptionType }
     *     
     */
    public void setStartDateOption(StartDateOptionType value) {
        this.startDateOption = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersionDate(XMLGregorianCalendar value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the versionDateAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public VersionDateAttributesType getVersionDateAttributes() {
        return versionDateAttributes;
    }

    /**
     * Sets the value of the versionDateAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public void setVersionDateAttributes(VersionDateAttributesType value) {
        this.versionDateAttributes = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineNumber(String value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the licenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModel() {
        return licenseModel;
    }

    /**
     * Sets the value of the licenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModel(LicenseModelIdentifierType value) {
        this.licenseModel = value;
    }

    /**
     * Gets the value of the alternateLicenseModel1 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel1() {
        return alternateLicenseModel1;
    }

    /**
     * Sets the value of the alternateLicenseModel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel1(LicenseModelIdentifierType value) {
        this.alternateLicenseModel1 = value;
    }

    /**
     * Gets the value of the alternateLicenseModel2 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel2() {
        return alternateLicenseModel2;
    }

    /**
     * Sets the value of the alternateLicenseModel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel2(LicenseModelIdentifierType value) {
        this.alternateLicenseModel2 = value;
    }

    /**
     * Gets the value of the licenseModelAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLicenseModelAttributes() {
        return licenseModelAttributes;
    }

    /**
     * Sets the value of the licenseModelAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLicenseModelAttributes(AttributeDescriptorDataType value) {
        this.licenseModelAttributes = value;
    }

    /**
     * Gets the value of the policyAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyAttributesListType }
     *     
     */
    public PolicyAttributesListType getPolicyAttributes() {
        return policyAttributes;
    }

    /**
     * Sets the value of the policyAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyAttributesListType }
     *     
     */
    public void setPolicyAttributes(PolicyAttributesListType value) {
        this.policyAttributes = value;
    }

    /**
     * Gets the value of the fnpTimeZoneValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNPTimeZoneValue() {
        return fnpTimeZoneValue;
    }

    /**
     * Sets the value of the fnpTimeZoneValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNPTimeZoneValue(String value) {
        this.fnpTimeZoneValue = value;
    }

    /**
     * Gets the value of the lineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLineItemAttributes() {
        return lineItemAttributes;
    }

    /**
     * Sets the value of the lineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLineItemAttributes(AttributeDescriptorDataType value) {
        this.lineItemAttributes = value;
    }

}
