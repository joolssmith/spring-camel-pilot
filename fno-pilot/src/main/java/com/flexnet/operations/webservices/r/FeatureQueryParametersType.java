
package com.flexnet.operations.webservices.r;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for featureQueryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="featureQueryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureName" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="version" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="versionFormat" type="{urn:v1.webservices.operations.flexnet.com}VersionFormatQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v1.webservices.operations.flexnet.com}StateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="creationDate" type="{urn:v1.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDate" type="{urn:v1.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "featureQueryParametersType", propOrder = {
    "featureName",
    "version",
    "versionFormat",
    "description",
    "state",
    "creationDate",
    "lastModifiedDate"
})
public class FeatureQueryParametersType {

    @XmlElementRef(name = "featureName", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> featureName;
    @XmlElementRef(name = "version", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> version;
    @XmlElementRef(name = "versionFormat", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<VersionFormatQueryType> versionFormat;
    @XmlElementRef(name = "description", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> description;
    @XmlElementRef(name = "state", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<StateQueryType> state;
    @XmlElementRef(name = "creationDate", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<DateQueryType> creationDate;
    @XmlElementRef(name = "lastModifiedDate", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<DateQueryType> lastModifiedDate;

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setFeatureName(JAXBElement<SimpleQueryType> value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setVersion(JAXBElement<SimpleQueryType> value) {
        this.version = value;
    }

    /**
     * Gets the value of the versionFormat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VersionFormatQueryType }{@code >}
     *     
     */
    public JAXBElement<VersionFormatQueryType> getVersionFormat() {
        return versionFormat;
    }

    /**
     * Sets the value of the versionFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VersionFormatQueryType }{@code >}
     *     
     */
    public void setVersionFormat(JAXBElement<VersionFormatQueryType> value) {
        this.versionFormat = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setDescription(JAXBElement<SimpleQueryType> value) {
        this.description = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}
     *     
     */
    public JAXBElement<StateQueryType> getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}
     *     
     */
    public void setState(JAXBElement<StateQueryType> value) {
        this.state = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public JAXBElement<DateQueryType> getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public void setCreationDate(JAXBElement<DateQueryType> value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the lastModifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public JAXBElement<DateQueryType> getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets the value of the lastModifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public void setLastModifiedDate(JAXBElement<DateQueryType> value) {
        this.lastModifiedDate = value;
    }

}
