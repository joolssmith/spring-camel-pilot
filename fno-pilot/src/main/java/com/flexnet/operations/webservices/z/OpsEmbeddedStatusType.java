
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OpsEmbeddedStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OpsEmbeddedStatusType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="SUCCESS"/&gt;
 *     &lt;enumeration value="FAILURE"/&gt;
 *     &lt;enumeration value="PARTIAL_FAILURE"/&gt;
 *     &lt;enumeration value="NOT_SUPPORTED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OpsEmbeddedStatusType")
@XmlEnum
public enum OpsEmbeddedStatusType {

    SUCCESS,
    FAILURE,
    PARTIAL_FAILURE,
    NOT_SUPPORTED;

    public String value() {
        return name();
    }

    public static OpsEmbeddedStatusType fromValue(String v) {
        return valueOf(v);
    }

}
