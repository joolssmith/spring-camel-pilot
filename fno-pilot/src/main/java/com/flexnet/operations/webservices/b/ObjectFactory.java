
package com.flexnet.operations.webservices.b;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.b package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.b
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetUsageRequest }
     * 
     */
    public GetUsageRequest createGetUsageRequest() {
        return new GetUsageRequest();
    }

    /**
     * Create an instance of {@link GetUsageResponse }
     * 
     */
    public GetUsageResponse createGetUsageResponse() {
        return new GetUsageResponse();
    }

    /**
     * Create an instance of {@link StatusInfoType }
     * 
     */
    public StatusInfoType createStatusInfoType() {
        return new StatusInfoType();
    }

    /**
     * Create an instance of {@link Usage }
     * 
     */
    public Usage createUsage() {
        return new Usage();
    }

    /**
     * Create an instance of {@link Period }
     * 
     */
    public Period createPeriod() {
        return new Period();
    }

}
