
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ShortCodeActivationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ShortCodeActivationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="REINSTALL"/&gt;
 *     &lt;enumeration value="REACTIVATE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ShortCodeActivationType")
@XmlEnum
public enum ShortCodeActivationType {

    REINSTALL,
    REACTIVATE;

    public String value() {
        return name();
    }

    public static ShortCodeActivationType fromValue(String v) {
        return valueOf(v);
    }

}
