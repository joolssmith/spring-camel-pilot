
package com.flexnet.operations.webservices.g;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUniformSuiteDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUniformSuiteDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedUniformSuite" type="{urn:com.macrovision:flexnet/operations}failedUniformSuiteDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUniformSuiteDataListType", propOrder = {
    "failedUniformSuite"
})
public class FailedUniformSuiteDataListType {

    protected List<FailedUniformSuiteDataType> failedUniformSuite;

    /**
     * Gets the value of the failedUniformSuite property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedUniformSuite property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedUniformSuite().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUniformSuiteDataType }
     * 
     * 
     */
    public List<FailedUniformSuiteDataType> getFailedUniformSuite() {
        if (failedUniformSuite == null) {
            failedUniformSuite = new ArrayList<FailedUniformSuiteDataType>();
        }
        return this.failedUniformSuite;
    }

}
