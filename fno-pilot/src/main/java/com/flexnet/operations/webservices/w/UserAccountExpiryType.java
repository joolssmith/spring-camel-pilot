
package com.flexnet.operations.webservices.w;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userAccountExpiryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userAccountExpiryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userData" type="{urn:v2.webservices.operations.flexnet.com}userDetailDataType" minOccurs="0"/&gt;
 *         &lt;element name="accountExpiryData" type="{urn:v2.webservices.operations.flexnet.com}accountExpiryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userAccountExpiryType", propOrder = {
    "userData",
    "accountExpiryData"
})
public class UserAccountExpiryType {

    protected UserDetailDataType userData;
    protected AccountExpiryType accountExpiryData;

    /**
     * Gets the value of the userData property.
     * 
     * @return
     *     possible object is
     *     {@link UserDetailDataType }
     *     
     */
    public UserDetailDataType getUserData() {
        return userData;
    }

    /**
     * Sets the value of the userData property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserDetailDataType }
     *     
     */
    public void setUserData(UserDetailDataType value) {
        this.userData = value;
    }

    /**
     * Gets the value of the accountExpiryData property.
     * 
     * @return
     *     possible object is
     *     {@link AccountExpiryType }
     *     
     */
    public AccountExpiryType getAccountExpiryData() {
        return accountExpiryData;
    }

    /**
     * Sets the value of the accountExpiryData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountExpiryType }
     *     
     */
    public void setAccountExpiryData(AccountExpiryType value) {
        this.accountExpiryData = value;
    }

}
