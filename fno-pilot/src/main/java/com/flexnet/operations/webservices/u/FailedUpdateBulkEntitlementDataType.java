
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateBulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateBulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlement" type="{urn:v2.webservices.operations.flexnet.com}updateBulkEntitlementDataType" minOccurs="0"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateBulkEntitlementDataType", propOrder = {
    "bulkEntitlement",
    "reason"
})
public class FailedUpdateBulkEntitlementDataType {

    protected UpdateBulkEntitlementDataType bulkEntitlement;
    protected String reason;

    /**
     * Gets the value of the bulkEntitlement property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateBulkEntitlementDataType }
     *     
     */
    public UpdateBulkEntitlementDataType getBulkEntitlement() {
        return bulkEntitlement;
    }

    /**
     * Sets the value of the bulkEntitlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateBulkEntitlementDataType }
     *     
     */
    public void setBulkEntitlement(UpdateBulkEntitlementDataType value) {
        this.bulkEntitlement = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
