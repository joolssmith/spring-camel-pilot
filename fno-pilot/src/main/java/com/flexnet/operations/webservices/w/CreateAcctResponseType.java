
package com.flexnet.operations.webservices.w;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createAcctResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createAcctResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v2.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="failedData" type="{urn:v2.webservices.operations.flexnet.com}failedCreateAcctDataListType" minOccurs="0"/&gt;
 *         &lt;element name="responseData" type="{urn:v2.webservices.operations.flexnet.com}createdAccountDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createAcctResponseType", propOrder = {
    "statusInfo",
    "failedData",
    "responseData"
})
public class CreateAcctResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected FailedCreateAcctDataListType failedData;
    protected CreatedAccountDataListType responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedCreateAcctDataListType }
     *     
     */
    public FailedCreateAcctDataListType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedCreateAcctDataListType }
     *     
     */
    public void setFailedData(FailedCreateAcctDataListType value) {
        this.failedData = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link CreatedAccountDataListType }
     *     
     */
    public CreatedAccountDataListType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatedAccountDataListType }
     *     
     */
    public void setResponseData(CreatedAccountDataListType value) {
        this.responseData = value;
    }

}
