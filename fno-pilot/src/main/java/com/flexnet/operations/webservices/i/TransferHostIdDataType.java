
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transferHostIdDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferHostIdDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="serverIds" type="{urn:com.macrovision:flexnet/operations}ServerIDsType" minOccurs="0"/&gt;
 *         &lt;element name="nodeId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customHostId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customHostType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customLicenseTechnology" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferHostIdDataType", propOrder = {
    "soldTo",
    "serverIds",
    "nodeId",
    "customHostId",
    "customHostType",
    "customLicenseTechnology"
})
public class TransferHostIdDataType {

    @XmlElement(required = true)
    protected String soldTo;
    protected ServerIDsType serverIds;
    protected String nodeId;
    protected String customHostId;
    protected String customHostType;
    protected String customLicenseTechnology;

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldTo(String value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the serverIds property.
     * 
     * @return
     *     possible object is
     *     {@link ServerIDsType }
     *     
     */
    public ServerIDsType getServerIds() {
        return serverIds;
    }

    /**
     * Sets the value of the serverIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerIDsType }
     *     
     */
    public void setServerIds(ServerIDsType value) {
        this.serverIds = value;
    }

    /**
     * Gets the value of the nodeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeId() {
        return nodeId;
    }

    /**
     * Sets the value of the nodeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeId(String value) {
        this.nodeId = value;
    }

    /**
     * Gets the value of the customHostId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomHostId() {
        return customHostId;
    }

    /**
     * Sets the value of the customHostId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomHostId(String value) {
        this.customHostId = value;
    }

    /**
     * Gets the value of the customHostType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomHostType() {
        return customHostType;
    }

    /**
     * Sets the value of the customHostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomHostType(String value) {
        this.customHostType = value;
    }

    /**
     * Gets the value of the customLicenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomLicenseTechnology() {
        return customLicenseTechnology;
    }

    /**
     * Sets the value of the customLicenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomLicenseTechnology(String value) {
        this.customLicenseTechnology = value;
    }

}
