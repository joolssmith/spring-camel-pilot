
package com.flexnet.operations.webservices.o;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateEntitlementLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateEntitlementLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="lineItemData" type="{urn:v1.webservices.operations.flexnet.com}updateLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItemData" type="{urn:v1.webservices.operations.flexnet.com}updateMaintenanceLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="autoDeploy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateEntitlementLineItemDataType", propOrder = {
    "entitlementIdentifier",
    "lineItemData",
    "maintenanceLineItemData",
    "autoDeploy"
})
public class UpdateEntitlementLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementIdentifier;
    protected List<UpdateLineItemDataType> lineItemData;
    protected List<UpdateMaintenanceLineItemDataType> maintenanceLineItemData;
    protected Boolean autoDeploy;

    /**
     * Gets the value of the entitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }

    /**
     * Sets the value of the entitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementIdentifier(EntitlementIdentifierType value) {
        this.entitlementIdentifier = value;
    }

    /**
     * Gets the value of the lineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateLineItemDataType }
     * 
     * 
     */
    public List<UpdateLineItemDataType> getLineItemData() {
        if (lineItemData == null) {
            lineItemData = new ArrayList<UpdateLineItemDataType>();
        }
        return this.lineItemData;
    }

    /**
     * Gets the value of the maintenanceLineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maintenanceLineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaintenanceLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateMaintenanceLineItemDataType }
     * 
     * 
     */
    public List<UpdateMaintenanceLineItemDataType> getMaintenanceLineItemData() {
        if (maintenanceLineItemData == null) {
            maintenanceLineItemData = new ArrayList<UpdateMaintenanceLineItemDataType>();
        }
        return this.maintenanceLineItemData;
    }

    /**
     * Gets the value of the autoDeploy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoDeploy() {
        return autoDeploy;
    }

    /**
     * Sets the value of the autoDeploy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoDeploy(Boolean value) {
        this.autoDeploy = value;
    }

}
