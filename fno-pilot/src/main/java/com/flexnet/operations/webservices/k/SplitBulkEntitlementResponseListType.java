
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for splitBulkEntitlementResponseListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="splitBulkEntitlementResponseListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="splitBulkEntitlement" type="{urn:com.macrovision:flexnet/operations}splitBulkEntitlementDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "splitBulkEntitlementResponseListType", propOrder = {
    "splitBulkEntitlement"
})
public class SplitBulkEntitlementResponseListType {

    @XmlElement(required = true)
    protected List<SplitBulkEntitlementDataType> splitBulkEntitlement;

    /**
     * Gets the value of the splitBulkEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splitBulkEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplitBulkEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SplitBulkEntitlementDataType }
     * 
     * 
     */
    public List<SplitBulkEntitlementDataType> getSplitBulkEntitlement() {
        if (splitBulkEntitlement == null) {
            splitBulkEntitlement = new ArrayList<SplitBulkEntitlementDataType>();
        }
        return this.splitBulkEntitlement;
    }

}
