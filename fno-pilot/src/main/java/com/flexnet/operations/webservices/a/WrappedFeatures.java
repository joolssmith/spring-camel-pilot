
package com.flexnet.operations.webservices.a;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for WrappedFeatures complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="WrappedFeatures"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RightsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AvailableCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="FeaturesWithStatus" type="{http://producersuite.flexerasoftware.com/EntitlementService/}FeaturesWithStatus" minOccurs="0"/&gt;
 *         &lt;element name="Problem" type="{http://producersuite.flexerasoftware.com/EntitlementService/}RightsIdProblem" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "WrappedFeatures", propOrder = {
    "rightsId",
    "availableCount",
    "featuresWithStatus",
    "problem"
})
public class WrappedFeatures {

    @XmlElement(name = "RightsId", required = true, nillable = true)
    protected String rightsId;
    @XmlElementRef(name = "AvailableCount", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> availableCount;
    @XmlElementRef(name = "FeaturesWithStatus", type = JAXBElement.class, required = false)
    protected JAXBElement<FeaturesWithStatus> featuresWithStatus;
    @XmlElementRef(name = "Problem", type = JAXBElement.class, required = false)
    protected JAXBElement<RightsIdProblem> problem;

    /**
     * Gets the value of the rightsId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRightsId() {
        return rightsId;
    }

    /**
     * Sets the value of the rightsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRightsId(String value) {
        this.rightsId = value;
    }

    /**
     * Gets the value of the availableCount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getAvailableCount() {
        return availableCount;
    }

    /**
     * Sets the value of the availableCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setAvailableCount(JAXBElement<BigInteger> value) {
        this.availableCount = value;
    }

    /**
     * Gets the value of the featuresWithStatus property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FeaturesWithStatus }{@code >}
     *     
     */
    public JAXBElement<FeaturesWithStatus> getFeaturesWithStatus() {
        return featuresWithStatus;
    }

    /**
     * Sets the value of the featuresWithStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FeaturesWithStatus }{@code >}
     *     
     */
    public void setFeaturesWithStatus(JAXBElement<FeaturesWithStatus> value) {
        this.featuresWithStatus = value;
    }

    /**
     * Gets the value of the problem property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RightsIdProblem }{@code >}
     *     
     */
    public JAXBElement<RightsIdProblem> getProblem() {
        return problem;
    }

    /**
     * Sets the value of the problem property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RightsIdProblem }{@code >}
     *     
     */
    public void setProblem(JAXBElement<RightsIdProblem> value) {
        this.problem = value;
    }

}
