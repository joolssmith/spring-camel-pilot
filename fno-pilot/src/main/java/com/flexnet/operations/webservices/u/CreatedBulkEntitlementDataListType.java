
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdBulkEntitlementDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdBulkEntitlementDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="createdBulkEntitlement" type="{urn:v2.webservices.operations.flexnet.com}createdBulkEntitlementDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdBulkEntitlementDataListType", propOrder = {
    "createdBulkEntitlement"
})
public class CreatedBulkEntitlementDataListType {

    protected List<CreatedBulkEntitlementDataType> createdBulkEntitlement;

    /**
     * Gets the value of the createdBulkEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdBulkEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedBulkEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatedBulkEntitlementDataType }
     * 
     * 
     */
    public List<CreatedBulkEntitlementDataType> getCreatedBulkEntitlement() {
        if (createdBulkEntitlement == null) {
            createdBulkEntitlement = new ArrayList<CreatedBulkEntitlementDataType>();
        }
        return this.createdBulkEntitlement;
    }

}
