
package com.flexnet.operations.webservices.o;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for entitlementLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entitlementLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:v1.webservices.operations.flexnet.com}createEntitlementLineItemDataType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="numberOfRemainingCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="availableExtraActivations" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="isTrustedType" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v1.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v1.webservices.operations.flexnet.com}licenseTechnologyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="parentLineItem" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="createdOnDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="overdraftMax" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="remainingOverdraftCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entitlementLineItemDataType", propOrder = {
    "numberOfRemainingCopies",
    "availableExtraActivations",
    "isTrustedType",
    "state",
    "licenseTechnology",
    "parentLineItem",
    "createdOnDateTime",
    "lastModifiedDateTime",
    "overdraftMax",
    "remainingOverdraftCount"
})
public class EntitlementLineItemDataType
    extends CreateEntitlementLineItemDataType
{

    protected BigInteger numberOfRemainingCopies;
    protected BigInteger availableExtraActivations;
    protected Boolean isTrustedType;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    protected LicenseTechnologyIdentifierType licenseTechnology;
    protected EntitlementLineItemIdentifierType parentLineItem;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createdOnDateTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    protected BigInteger overdraftMax;
    protected BigInteger remainingOverdraftCount;

    /**
     * Gets the value of the numberOfRemainingCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfRemainingCopies() {
        return numberOfRemainingCopies;
    }

    /**
     * Sets the value of the numberOfRemainingCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfRemainingCopies(BigInteger value) {
        this.numberOfRemainingCopies = value;
    }

    /**
     * Gets the value of the availableExtraActivations property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAvailableExtraActivations() {
        return availableExtraActivations;
    }

    /**
     * Sets the value of the availableExtraActivations property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAvailableExtraActivations(BigInteger value) {
        this.availableExtraActivations = value;
    }

    /**
     * Gets the value of the isTrustedType property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTrustedType() {
        return isTrustedType;
    }

    /**
     * Sets the value of the isTrustedType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTrustedType(Boolean value) {
        this.isTrustedType = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the parentLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getParentLineItem() {
        return parentLineItem;
    }

    /**
     * Sets the value of the parentLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setParentLineItem(EntitlementLineItemIdentifierType value) {
        this.parentLineItem = value;
    }

    /**
     * Gets the value of the createdOnDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreatedOnDateTime() {
        return createdOnDateTime;
    }

    /**
     * Sets the value of the createdOnDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreatedOnDateTime(XMLGregorianCalendar value) {
        this.createdOnDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the overdraftMax property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOverdraftMax() {
        return overdraftMax;
    }

    /**
     * Sets the value of the overdraftMax property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOverdraftMax(BigInteger value) {
        this.overdraftMax = value;
    }

    /**
     * Gets the value of the remainingOverdraftCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRemainingOverdraftCount() {
        return remainingOverdraftCount;
    }

    /**
     * Sets the value of the remainingOverdraftCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRemainingOverdraftCount(BigInteger value) {
        this.remainingOverdraftCount = value;
    }

}
