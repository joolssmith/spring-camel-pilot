
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for isFulfillmentVisibleInTargetOrgRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="isFulfillmentVisibleInTargetOrgRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActivationID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="targetOrgID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "isFulfillmentVisibleInTargetOrgRequestType", propOrder = {
    "activationID",
    "targetOrgID"
})
public class IsFulfillmentVisibleInTargetOrgRequestType {

    @XmlElement(name = "ActivationID", required = true, nillable = true)
    protected String activationID;
    @XmlElement(required = true)
    protected String targetOrgID;

    /**
     * Gets the value of the activationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationID() {
        return activationID;
    }

    /**
     * Sets the value of the activationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationID(String value) {
        this.activationID = value;
    }

    /**
     * Gets the value of the targetOrgID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetOrgID() {
        return targetOrgID;
    }

    /**
     * Sets the value of the targetOrgID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetOrgID(String value) {
        this.targetOrgID = value;
    }

}
