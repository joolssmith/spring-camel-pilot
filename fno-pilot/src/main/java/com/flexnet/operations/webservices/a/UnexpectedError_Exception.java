
package com.flexnet.operations.webservices.a;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.4
 * 2018-05-09T18:58:50.841+01:00
 * Generated source version: 3.2.4
 */

@WebFault(name = "UnexpectedError", targetNamespace = "http://producersuite.flexerasoftware.com/EntitlementService/")
public class UnexpectedError_Exception extends Exception {

    private com.flexnet.operations.webservices.a.UnexpectedError unexpectedError;

    public UnexpectedError_Exception() {
        super();
    }

    public UnexpectedError_Exception(String message) {
        super(message);
    }

    public UnexpectedError_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public UnexpectedError_Exception(String message, com.flexnet.operations.webservices.a.UnexpectedError unexpectedError) {
        super(message);
        this.unexpectedError = unexpectedError;
    }

    public UnexpectedError_Exception(String message, com.flexnet.operations.webservices.a.UnexpectedError unexpectedError, java.lang.Throwable cause) {
        super(message, cause);
        this.unexpectedError = unexpectedError;
    }

    public com.flexnet.operations.webservices.a.UnexpectedError getFaultInfo() {
        return this.unexpectedError;
    }
}
