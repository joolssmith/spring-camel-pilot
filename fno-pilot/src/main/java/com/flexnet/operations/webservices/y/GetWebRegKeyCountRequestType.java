
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getWebRegKeyCountRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getWebRegKeyCountRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlementIdentifier" type="{urn:v3.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getWebRegKeyCountRequestType", propOrder = {
    "bulkEntitlementIdentifier"
})
public class GetWebRegKeyCountRequestType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType bulkEntitlementIdentifier;

    /**
     * Gets the value of the bulkEntitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getBulkEntitlementIdentifier() {
        return bulkEntitlementIdentifier;
    }

    /**
     * Sets the value of the bulkEntitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setBulkEntitlementIdentifier(EntitlementIdentifierType value) {
        this.bulkEntitlementIdentifier = value;
    }

}
