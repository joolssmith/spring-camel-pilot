
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdBulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdBulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="recordRefNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="uniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="bulkEntitlementId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdBulkEntitlementDataType", propOrder = {
    "recordRefNo",
    "uniqueId",
    "bulkEntitlementId"
})
public class CreatedBulkEntitlementDataType {

    @XmlElement(required = true)
    protected String recordRefNo;
    @XmlElement(required = true)
    protected String uniqueId;
    @XmlElement(required = true)
    protected String bulkEntitlementId;

    /**
     * Gets the value of the recordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordRefNo() {
        return recordRefNo;
    }

    /**
     * Sets the value of the recordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordRefNo(String value) {
        this.recordRefNo = value;
    }

    /**
     * Gets the value of the uniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Sets the value of the uniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueId(String value) {
        this.uniqueId = value;
    }

    /**
     * Gets the value of the bulkEntitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBulkEntitlementId() {
        return bulkEntitlementId;
    }

    /**
     * Sets the value of the bulkEntitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBulkEntitlementId(String value) {
        this.bulkEntitlementId = value;
    }

}
