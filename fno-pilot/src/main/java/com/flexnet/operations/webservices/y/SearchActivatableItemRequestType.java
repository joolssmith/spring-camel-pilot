
package com.flexnet.operations.webservices.y;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchActivatableItemRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchActivatableItemRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activatableItemSearchCriteria" type="{urn:v3.webservices.operations.flexnet.com}searchActivatableItemDataType"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchActivatableItemRequestType", propOrder = {
    "activatableItemSearchCriteria",
    "batchSize",
    "pageNumber"
})
public class SearchActivatableItemRequestType {

    @XmlElement(required = true)
    protected SearchActivatableItemDataType activatableItemSearchCriteria;
    @XmlElement(required = true)
    protected BigInteger batchSize;
    protected BigInteger pageNumber;

    /**
     * Gets the value of the activatableItemSearchCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link SearchActivatableItemDataType }
     *     
     */
    public SearchActivatableItemDataType getActivatableItemSearchCriteria() {
        return activatableItemSearchCriteria;
    }

    /**
     * Sets the value of the activatableItemSearchCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchActivatableItemDataType }
     *     
     */
    public void setActivatableItemSearchCriteria(SearchActivatableItemDataType value) {
        this.activatableItemSearchCriteria = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

}
