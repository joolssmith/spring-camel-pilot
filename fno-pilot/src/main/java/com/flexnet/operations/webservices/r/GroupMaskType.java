
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupMaskType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="GroupMaskType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="USER"/&gt;
 *     &lt;enumeration value="HOST"/&gt;
 *     &lt;enumeration value="DISPLAY"/&gt;
 *     &lt;enumeration value="VENDOR"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "GroupMaskType")
@XmlEnum
public enum GroupMaskType {

    USER,
    HOST,
    DISPLAY,
    VENDOR;

    public String value() {
        return name();
    }

    public static GroupMaskType fromValue(String v) {
        return valueOf(v);
    }

}
