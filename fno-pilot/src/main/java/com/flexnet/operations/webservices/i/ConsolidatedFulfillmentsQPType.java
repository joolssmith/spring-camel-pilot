
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consolidatedFulfillmentsQPType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consolidatedFulfillmentsQPType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="consolidatedLicenseId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillmentId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="activationId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="criteria" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:com.macrovision:flexnet/operations}StateQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consolidatedFulfillmentsQPType", propOrder = {
    "consolidatedLicenseId",
    "fulfillmentId",
    "activationId",
    "entitlementId",
    "soldTo",
    "criteria",
    "state"
})
public class ConsolidatedFulfillmentsQPType {

    protected SimpleQueryType consolidatedLicenseId;
    protected SimpleQueryType fulfillmentId;
    protected SimpleQueryType activationId;
    protected SimpleQueryType entitlementId;
    protected SimpleQueryType soldTo;
    protected SimpleQueryType criteria;
    protected StateQueryType state;

    /**
     * Gets the value of the consolidatedLicenseId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getConsolidatedLicenseId() {
        return consolidatedLicenseId;
    }

    /**
     * Sets the value of the consolidatedLicenseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setConsolidatedLicenseId(SimpleQueryType value) {
        this.consolidatedLicenseId = value;
    }

    /**
     * Gets the value of the fulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFulfillmentId() {
        return fulfillmentId;
    }

    /**
     * Sets the value of the fulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFulfillmentId(SimpleQueryType value) {
        this.fulfillmentId = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setActivationId(SimpleQueryType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setEntitlementId(SimpleQueryType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldTo(SimpleQueryType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the criteria property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCriteria() {
        return criteria;
    }

    /**
     * Sets the value of the criteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCriteria(SimpleQueryType value) {
        this.criteria = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateQueryType }
     *     
     */
    public StateQueryType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateQueryType }
     *     
     */
    public void setState(StateQueryType value) {
        this.state = value;
    }

}
