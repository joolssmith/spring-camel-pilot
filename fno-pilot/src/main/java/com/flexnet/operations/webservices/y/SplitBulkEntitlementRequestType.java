
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for splitBulkEntitlementRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="splitBulkEntitlementRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlementList" type="{urn:v3.webservices.operations.flexnet.com}splitBulkEntitlementListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "splitBulkEntitlementRequestType", propOrder = {
    "bulkEntitlementList"
})
public class SplitBulkEntitlementRequestType {

    @XmlElement(required = true)
    protected SplitBulkEntitlementListType bulkEntitlementList;

    /**
     * Gets the value of the bulkEntitlementList property.
     * 
     * @return
     *     possible object is
     *     {@link SplitBulkEntitlementListType }
     *     
     */
    public SplitBulkEntitlementListType getBulkEntitlementList() {
        return bulkEntitlementList;
    }

    /**
     * Sets the value of the bulkEntitlementList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitBulkEntitlementListType }
     *     
     */
    public void setBulkEntitlementList(SplitBulkEntitlementListType value) {
        this.bulkEntitlementList = value;
    }

}
