
package com.flexnet.operations.webservices.u;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchEntitlementMaintenanceLineItemPropertiesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchEntitlementMaintenanceLineItemPropertiesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v2.webservices.operations.flexnet.com}searchMaintenanceLineItemDataType"/&gt;
 *         &lt;element name="entitlementMaintenanceLineItemResponseConfig" type="{urn:v2.webservices.operations.flexnet.com}entitlementMaintenanceLineItemResponseConfigRequestType"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchEntitlementMaintenanceLineItemPropertiesRequestType", propOrder = {
    "queryParams",
    "entitlementMaintenanceLineItemResponseConfig",
    "batchSize",
    "pageNumber"
})
public class SearchEntitlementMaintenanceLineItemPropertiesRequestType {

    @XmlElement(required = true)
    protected SearchMaintenanceLineItemDataType queryParams;
    @XmlElement(required = true)
    protected EntitlementMaintenanceLineItemResponseConfigRequestType entitlementMaintenanceLineItemResponseConfig;
    @XmlElement(required = true)
    protected BigInteger batchSize;
    protected BigInteger pageNumber;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link SearchMaintenanceLineItemDataType }
     *     
     */
    public SearchMaintenanceLineItemDataType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchMaintenanceLineItemDataType }
     *     
     */
    public void setQueryParams(SearchMaintenanceLineItemDataType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the entitlementMaintenanceLineItemResponseConfig property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementMaintenanceLineItemResponseConfigRequestType }
     *     
     */
    public EntitlementMaintenanceLineItemResponseConfigRequestType getEntitlementMaintenanceLineItemResponseConfig() {
        return entitlementMaintenanceLineItemResponseConfig;
    }

    /**
     * Sets the value of the entitlementMaintenanceLineItemResponseConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementMaintenanceLineItemResponseConfigRequestType }
     *     
     */
    public void setEntitlementMaintenanceLineItemResponseConfig(EntitlementMaintenanceLineItemResponseConfigRequestType value) {
        this.entitlementMaintenanceLineItemResponseConfig = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

}
