
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedDeleteUniformSuiteDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedDeleteUniformSuiteDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="suiteIdentifier" type="{urn:v2.webservices.operations.flexnet.com}suiteIdentifierType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedDeleteUniformSuiteDataType", propOrder = {
    "suiteIdentifier",
    "reason"
})
public class FailedDeleteUniformSuiteDataType {

    @XmlElement(required = true)
    protected SuiteIdentifierType suiteIdentifier;
    protected String reason;

    /**
     * Gets the value of the suiteIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link SuiteIdentifierType }
     *     
     */
    public SuiteIdentifierType getSuiteIdentifier() {
        return suiteIdentifier;
    }

    /**
     * Sets the value of the suiteIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link SuiteIdentifierType }
     *     
     */
    public void setSuiteIdentifier(SuiteIdentifierType value) {
        this.suiteIdentifier = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
