
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchActivatableItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchActivatableItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentBulkEntitlementId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="activationId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="productName" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="productVersion" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="productType" type="{urn:v2.webservices.operations.flexnet.com}ProductType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{urn:v2.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{urn:v2.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{urn:v2.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="withNoOrderId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="restrictToItemsWithCount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="fulfilledAmount" type="{urn:v2.webservices.operations.flexnet.com}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v2.webservices.operations.flexnet.com}customAttributesQueryListType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="parentBulkEntSoldTo" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="activatableItemType" type="{urn:v2.webservices.operations.flexnet.com}ActivatableItemType" minOccurs="0"/&gt;
 *         &lt;element name="allowPortalLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="organizationUnitName" type="{urn:v2.webservices.operations.flexnet.com}PartnerTierQueryType" minOccurs="0"/&gt;
 *         &lt;element name="currentOwnerName" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lineItemParentLineItemId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="createdOnDateTime" type="{urn:v2.webservices.operations.flexnet.com}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{urn:v2.webservices.operations.flexnet.com}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lineItemAttributes" type="{urn:v2.webservices.operations.flexnet.com}lineItemCustomAttributesQueryListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchActivatableItemDataType", propOrder = {
    "parentBulkEntitlementId",
    "entitlementId",
    "activationId",
    "productName",
    "productVersion",
    "productType",
    "partNumber",
    "startDate",
    "isPermanent",
    "expirationDate",
    "versionDate",
    "licenseTechnology",
    "orderId",
    "withNoOrderId",
    "restrictToItemsWithCount",
    "fulfilledAmount",
    "userId",
    "customAttributes",
    "soldTo",
    "parentBulkEntSoldTo",
    "activatableItemType",
    "allowPortalLogin",
    "organizationUnitName",
    "currentOwnerName",
    "lineItemParentLineItemId",
    "createdOnDateTime",
    "lastModifiedDateTime",
    "lineItemAttributes"
})
public class SearchActivatableItemDataType {

    protected SimpleQueryType parentBulkEntitlementId;
    protected SimpleQueryType entitlementId;
    protected SimpleQueryType activationId;
    protected SimpleQueryType productName;
    protected SimpleQueryType productVersion;
    @XmlSchemaType(name = "NMTOKEN")
    protected ProductType productType;
    protected SimpleQueryType partNumber;
    protected DateQueryType startDate;
    protected Boolean isPermanent;
    protected DateQueryType expirationDate;
    protected DateQueryType versionDate;
    protected SimpleQueryType licenseTechnology;
    protected SimpleQueryType orderId;
    protected Boolean withNoOrderId;
    protected Boolean restrictToItemsWithCount;
    protected NumberQueryType fulfilledAmount;
    protected String userId;
    protected CustomAttributesQueryListType customAttributes;
    protected SimpleQueryType soldTo;
    protected SimpleQueryType parentBulkEntSoldTo;
    @XmlSchemaType(name = "NMTOKEN")
    protected ActivatableItemType activatableItemType;
    protected Boolean allowPortalLogin;
    protected PartnerTierQueryType organizationUnitName;
    protected SimpleQueryType currentOwnerName;
    protected SimpleQueryType lineItemParentLineItemId;
    protected DateTimeQueryType createdOnDateTime;
    protected DateTimeQueryType lastModifiedDateTime;
    protected LineItemCustomAttributesQueryListType lineItemAttributes;

    /**
     * Gets the value of the parentBulkEntitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getParentBulkEntitlementId() {
        return parentBulkEntitlementId;
    }

    /**
     * Sets the value of the parentBulkEntitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setParentBulkEntitlementId(SimpleQueryType value) {
        this.parentBulkEntitlementId = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setEntitlementId(SimpleQueryType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setActivationId(SimpleQueryType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setProductName(SimpleQueryType value) {
        this.productName = value;
    }

    /**
     * Gets the value of the productVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getProductVersion() {
        return productVersion;
    }

    /**
     * Sets the value of the productVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setProductVersion(SimpleQueryType value) {
        this.productVersion = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link ProductType }
     *     
     */
    public ProductType getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductType }
     *     
     */
    public void setProductType(ProductType value) {
        this.productType = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setPartNumber(SimpleQueryType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setStartDate(DateQueryType value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setExpirationDate(DateQueryType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setVersionDate(DateQueryType value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setLicenseTechnology(SimpleQueryType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setOrderId(SimpleQueryType value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the withNoOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithNoOrderId() {
        return withNoOrderId;
    }

    /**
     * Sets the value of the withNoOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithNoOrderId(Boolean value) {
        this.withNoOrderId = value;
    }

    /**
     * Gets the value of the restrictToItemsWithCount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictToItemsWithCount() {
        return restrictToItemsWithCount;
    }

    /**
     * Sets the value of the restrictToItemsWithCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictToItemsWithCount(Boolean value) {
        this.restrictToItemsWithCount = value;
    }

    /**
     * Gets the value of the fulfilledAmount property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getFulfilledAmount() {
        return fulfilledAmount;
    }

    /**
     * Sets the value of the fulfilledAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setFulfilledAmount(NumberQueryType value) {
        this.fulfilledAmount = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public CustomAttributesQueryListType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public void setCustomAttributes(CustomAttributesQueryListType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldTo(SimpleQueryType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the parentBulkEntSoldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getParentBulkEntSoldTo() {
        return parentBulkEntSoldTo;
    }

    /**
     * Sets the value of the parentBulkEntSoldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setParentBulkEntSoldTo(SimpleQueryType value) {
        this.parentBulkEntSoldTo = value;
    }

    /**
     * Gets the value of the activatableItemType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivatableItemType }
     *     
     */
    public ActivatableItemType getActivatableItemType() {
        return activatableItemType;
    }

    /**
     * Sets the value of the activatableItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivatableItemType }
     *     
     */
    public void setActivatableItemType(ActivatableItemType value) {
        this.activatableItemType = value;
    }

    /**
     * Gets the value of the allowPortalLogin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPortalLogin() {
        return allowPortalLogin;
    }

    /**
     * Sets the value of the allowPortalLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPortalLogin(Boolean value) {
        this.allowPortalLogin = value;
    }

    /**
     * Gets the value of the organizationUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public PartnerTierQueryType getOrganizationUnitName() {
        return organizationUnitName;
    }

    /**
     * Sets the value of the organizationUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public void setOrganizationUnitName(PartnerTierQueryType value) {
        this.organizationUnitName = value;
    }

    /**
     * Gets the value of the currentOwnerName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCurrentOwnerName() {
        return currentOwnerName;
    }

    /**
     * Sets the value of the currentOwnerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCurrentOwnerName(SimpleQueryType value) {
        this.currentOwnerName = value;
    }

    /**
     * Gets the value of the lineItemParentLineItemId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getLineItemParentLineItemId() {
        return lineItemParentLineItemId;
    }

    /**
     * Sets the value of the lineItemParentLineItemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setLineItemParentLineItemId(SimpleQueryType value) {
        this.lineItemParentLineItemId = value;
    }

    /**
     * Gets the value of the createdOnDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getCreatedOnDateTime() {
        return createdOnDateTime;
    }

    /**
     * Sets the value of the createdOnDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setCreatedOnDateTime(DateTimeQueryType value) {
        this.createdOnDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setLastModifiedDateTime(DateTimeQueryType value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the lineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link LineItemCustomAttributesQueryListType }
     *     
     */
    public LineItemCustomAttributesQueryListType getLineItemAttributes() {
        return lineItemAttributes;
    }

    /**
     * Sets the value of the lineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItemCustomAttributesQueryListType }
     *     
     */
    public void setLineItemAttributes(LineItemCustomAttributesQueryListType value) {
        this.lineItemAttributes = value;
    }

}
