
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReturnedShortCodeReturnReason.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ReturnedShortCodeReturnReason"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="UPGRADE"/&gt;
 *     &lt;enumeration value="UPSELL"/&gt;
 *     &lt;enumeration value="RENEW"/&gt;
 *     &lt;enumeration value="REHOST"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ReturnedShortCodeReturnReason")
@XmlEnum
public enum ReturnedShortCodeReturnReason {

    UPGRADE,
    UPSELL,
    RENEW,
    REHOST;

    public String value() {
        return name();
    }

    public static ReturnedShortCodeReturnReason fromValue(String v) {
        return valueOf(v);
    }

}
