
package com.flexnet.operations.webservices.x;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUniformSuitesQueryResponseDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getUniformSuitesQueryResponseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uniformSuite" type="{urn:v2.webservices.operations.flexnet.com}uniformSuiteQueryDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUniformSuitesQueryResponseDataType", propOrder = {
    "uniformSuite"
})
public class GetUniformSuitesQueryResponseDataType {

    protected List<UniformSuiteQueryDataType> uniformSuite;

    /**
     * Gets the value of the uniformSuite property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the uniformSuite property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUniformSuite().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UniformSuiteQueryDataType }
     * 
     * 
     */
    public List<UniformSuiteQueryDataType> getUniformSuite() {
        if (uniformSuite == null) {
            uniformSuite = new ArrayList<UniformSuiteQueryDataType>();
        }
        return this.uniformSuite;
    }

}
