
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedShortCodeDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedShortCodeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shortCodeData" type="{urn:v1.webservices.operations.flexnet.com}createShortCodeDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="duplicateFulfillmentRecords" type="{urn:v1.webservices.operations.flexnet.com}duplicateFulfillmentRecordListDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedShortCodeDataType", propOrder = {
    "shortCodeData",
    "reason",
    "duplicateFulfillmentRecords"
})
public class FailedShortCodeDataType {

    @XmlElement(required = true)
    protected CreateShortCodeDataType shortCodeData;
    @XmlElement(required = true)
    protected String reason;
    protected DuplicateFulfillmentRecordListDataType duplicateFulfillmentRecords;

    /**
     * Gets the value of the shortCodeData property.
     * 
     * @return
     *     possible object is
     *     {@link CreateShortCodeDataType }
     *     
     */
    public CreateShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }

    /**
     * Sets the value of the shortCodeData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateShortCodeDataType }
     *     
     */
    public void setShortCodeData(CreateShortCodeDataType value) {
        this.shortCodeData = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

    /**
     * Gets the value of the duplicateFulfillmentRecords property.
     * 
     * @return
     *     possible object is
     *     {@link DuplicateFulfillmentRecordListDataType }
     *     
     */
    public DuplicateFulfillmentRecordListDataType getDuplicateFulfillmentRecords() {
        return duplicateFulfillmentRecords;
    }

    /**
     * Sets the value of the duplicateFulfillmentRecords property.
     * 
     * @param value
     *     allowed object is
     *     {@link DuplicateFulfillmentRecordListDataType }
     *     
     */
    public void setDuplicateFulfillmentRecords(DuplicateFulfillmentRecordListDataType value) {
        this.duplicateFulfillmentRecords = value;
    }

}
