
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdRenewEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdRenewEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementRecordRefNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="parentEntitlementIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementIdentifierType"/&gt;
 *         &lt;element name="renewedLineItem" type="{urn:com.macrovision:flexnet/operations}renewedEntitlementLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdRenewEntitlementDataType", propOrder = {
    "entitlementRecordRefNo",
    "parentEntitlementIdentifier",
    "renewedLineItem"
})
public class CreatedRenewEntitlementDataType {

    @XmlElement(required = true)
    protected String entitlementRecordRefNo;
    @XmlElement(required = true)
    protected EntitlementIdentifierType parentEntitlementIdentifier;
    protected List<RenewedEntitlementLineItemDataType> renewedLineItem;

    /**
     * Gets the value of the entitlementRecordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementRecordRefNo() {
        return entitlementRecordRefNo;
    }

    /**
     * Sets the value of the entitlementRecordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementRecordRefNo(String value) {
        this.entitlementRecordRefNo = value;
    }

    /**
     * Gets the value of the parentEntitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getParentEntitlementIdentifier() {
        return parentEntitlementIdentifier;
    }

    /**
     * Sets the value of the parentEntitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setParentEntitlementIdentifier(EntitlementIdentifierType value) {
        this.parentEntitlementIdentifier = value;
    }

    /**
     * Gets the value of the renewedLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the renewedLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRenewedLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RenewedEntitlementLineItemDataType }
     * 
     * 
     */
    public List<RenewedEntitlementLineItemDataType> getRenewedLineItem() {
        if (renewedLineItem == null) {
            renewedLineItem = new ArrayList<RenewedEntitlementLineItemDataType>();
        }
        return this.renewedLineItem;
    }

}
