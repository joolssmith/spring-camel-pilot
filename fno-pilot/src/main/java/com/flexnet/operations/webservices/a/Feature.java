
package com.flexnet.operations.webservices.a;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Feature complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Feature"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="VersionFormat" type="{http://producersuite.flexerasoftware.com/EntitlementService/}VersionFormatOpt" minOccurs="0"/&gt;
 *         &lt;element name="VersionDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="VersionDateOption" type="{http://producersuite.flexerasoftware.com/EntitlementService/}VersionDateOpt" minOccurs="0"/&gt;
 *         &lt;element name="VersionDateDuration" type="{http://producersuite.flexerasoftware.com/EntitlementService/}Duration" minOccurs="0"/&gt;
 *         &lt;element name="Count" type="{http://producersuite.flexerasoftware.com/EntitlementService/}FeatureCountType"/&gt;
 *         &lt;element name="Expiration" type="{http://producersuite.flexerasoftware.com/EntitlementService/}Expiration"/&gt;
 *         &lt;element name="FeatureId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="StartDate" type="{http://producersuite.flexerasoftware.com/EntitlementService/}StartDate" minOccurs="0"/&gt;
 *         &lt;element name="VendorString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Notice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Issuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="SerialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="Metered" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="Overdraft" type="{http://producersuite.flexerasoftware.com/EntitlementService/}OverdraftType" minOccurs="0"/&gt;
 *         &lt;element name="Returnable" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="UndoInterval" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="BorrowInterval" type="{http://producersuite.flexerasoftware.com/EntitlementService/}Duration" minOccurs="0"/&gt;
 *         &lt;element name="RenewInterval" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="GracePeriod" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
 *         &lt;element name="VirtualClientProhibited" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Feature", propOrder = {
    "name",
    "version",
    "versionFormat",
    "versionDate",
    "versionDateOption",
    "versionDateDuration",
    "count",
    "expiration",
    "featureId",
    "startDate",
    "vendorString",
    "notice",
    "issuer",
    "serialNumber",
    "metered",
    "overdraft",
    "returnable",
    "undoInterval",
    "borrowInterval",
    "renewInterval",
    "gracePeriod",
    "virtualClientProhibited"
})
public class Feature {

    @XmlElement(name = "Name", required = true, nillable = true)
    protected String name;
    @XmlElementRef(name = "Version", type = JAXBElement.class, required = false)
    protected JAXBElement<String> version;
    @XmlElementRef(name = "VersionFormat", type = JAXBElement.class, required = false)
    protected JAXBElement<VersionFormatOpt> versionFormat;
    @XmlElementRef(name = "VersionDate", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> versionDate;
    @XmlElementRef(name = "VersionDateOption", type = JAXBElement.class, required = false)
    protected JAXBElement<VersionDateOpt> versionDateOption;
    @XmlElementRef(name = "VersionDateDuration", type = JAXBElement.class, required = false)
    protected JAXBElement<Duration> versionDateDuration;
    @XmlElement(name = "Count", required = true, nillable = true)
    protected FeatureCountType count;
    @XmlElement(name = "Expiration", required = true, nillable = true)
    protected Expiration expiration;
    @XmlElement(name = "FeatureId", required = true, nillable = true)
    protected String featureId;
    @XmlElementRef(name = "StartDate", type = JAXBElement.class, required = false)
    protected JAXBElement<StartDate> startDate;
    @XmlElementRef(name = "VendorString", type = JAXBElement.class, required = false)
    protected JAXBElement<String> vendorString;
    @XmlElementRef(name = "Notice", type = JAXBElement.class, required = false)
    protected JAXBElement<String> notice;
    @XmlElementRef(name = "Issuer", type = JAXBElement.class, required = false)
    protected JAXBElement<String> issuer;
    @XmlElementRef(name = "SerialNumber", type = JAXBElement.class, required = false)
    protected JAXBElement<String> serialNumber;
    @XmlElementRef(name = "Metered", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> metered;
    @XmlElementRef(name = "Overdraft", type = JAXBElement.class, required = false)
    protected JAXBElement<OverdraftType> overdraft;
    @XmlElementRef(name = "Returnable", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> returnable;
    @XmlElementRef(name = "UndoInterval", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> undoInterval;
    @XmlElementRef(name = "BorrowInterval", type = JAXBElement.class, required = false)
    protected JAXBElement<Duration> borrowInterval;
    @XmlElementRef(name = "RenewInterval", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> renewInterval;
    @XmlElementRef(name = "GracePeriod", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> gracePeriod;
    @XmlElementRef(name = "VirtualClientProhibited", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> virtualClientProhibited;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVersion(JAXBElement<String> value) {
        this.version = value;
    }

    /**
     * Gets the value of the versionFormat property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VersionFormatOpt }{@code >}
     *     
     */
    public JAXBElement<VersionFormatOpt> getVersionFormat() {
        return versionFormat;
    }

    /**
     * Sets the value of the versionFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VersionFormatOpt }{@code >}
     *     
     */
    public void setVersionFormat(JAXBElement<VersionFormatOpt> value) {
        this.versionFormat = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setVersionDate(JAXBElement<XMLGregorianCalendar> value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the versionDateOption property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link VersionDateOpt }{@code >}
     *     
     */
    public JAXBElement<VersionDateOpt> getVersionDateOption() {
        return versionDateOption;
    }

    /**
     * Sets the value of the versionDateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link VersionDateOpt }{@code >}
     *     
     */
    public void setVersionDateOption(JAXBElement<VersionDateOpt> value) {
        this.versionDateOption = value;
    }

    /**
     * Gets the value of the versionDateDuration property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Duration }{@code >}
     *     
     */
    public JAXBElement<Duration> getVersionDateDuration() {
        return versionDateDuration;
    }

    /**
     * Sets the value of the versionDateDuration property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Duration }{@code >}
     *     
     */
    public void setVersionDateDuration(JAXBElement<Duration> value) {
        this.versionDateDuration = value;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureCountType }
     *     
     */
    public FeatureCountType getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureCountType }
     *     
     */
    public void setCount(FeatureCountType value) {
        this.count = value;
    }

    /**
     * Gets the value of the expiration property.
     * 
     * @return
     *     possible object is
     *     {@link Expiration }
     *     
     */
    public Expiration getExpiration() {
        return expiration;
    }

    /**
     * Sets the value of the expiration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Expiration }
     *     
     */
    public void setExpiration(Expiration value) {
        this.expiration = value;
    }

    /**
     * Gets the value of the featureId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureId() {
        return featureId;
    }

    /**
     * Sets the value of the featureId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureId(String value) {
        this.featureId = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StartDate }{@code >}
     *     
     */
    public JAXBElement<StartDate> getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StartDate }{@code >}
     *     
     */
    public void setStartDate(JAXBElement<StartDate> value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the vendorString property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getVendorString() {
        return vendorString;
    }

    /**
     * Sets the value of the vendorString property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setVendorString(JAXBElement<String> value) {
        this.vendorString = value;
    }

    /**
     * Gets the value of the notice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNotice() {
        return notice;
    }

    /**
     * Sets the value of the notice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNotice(JAXBElement<String> value) {
        this.notice = value;
    }

    /**
     * Gets the value of the issuer property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIssuer() {
        return issuer;
    }

    /**
     * Sets the value of the issuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIssuer(JAXBElement<String> value) {
        this.issuer = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSerialNumber(JAXBElement<String> value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the metered property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getMetered() {
        return metered;
    }

    /**
     * Sets the value of the metered property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setMetered(JAXBElement<Boolean> value) {
        this.metered = value;
    }

    /**
     * Gets the value of the overdraft property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OverdraftType }{@code >}
     *     
     */
    public JAXBElement<OverdraftType> getOverdraft() {
        return overdraft;
    }

    /**
     * Sets the value of the overdraft property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OverdraftType }{@code >}
     *     
     */
    public void setOverdraft(JAXBElement<OverdraftType> value) {
        this.overdraft = value;
    }

    /**
     * Gets the value of the returnable property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getReturnable() {
        return returnable;
    }

    /**
     * Sets the value of the returnable property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setReturnable(JAXBElement<Boolean> value) {
        this.returnable = value;
    }

    /**
     * Gets the value of the undoInterval property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getUndoInterval() {
        return undoInterval;
    }

    /**
     * Sets the value of the undoInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setUndoInterval(JAXBElement<BigInteger> value) {
        this.undoInterval = value;
    }

    /**
     * Gets the value of the borrowInterval property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Duration }{@code >}
     *     
     */
    public JAXBElement<Duration> getBorrowInterval() {
        return borrowInterval;
    }

    /**
     * Sets the value of the borrowInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Duration }{@code >}
     *     
     */
    public void setBorrowInterval(JAXBElement<Duration> value) {
        this.borrowInterval = value;
    }

    /**
     * Gets the value of the renewInterval property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getRenewInterval() {
        return renewInterval;
    }

    /**
     * Sets the value of the renewInterval property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setRenewInterval(JAXBElement<BigInteger> value) {
        this.renewInterval = value;
    }

    /**
     * Gets the value of the gracePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getGracePeriod() {
        return gracePeriod;
    }

    /**
     * Sets the value of the gracePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setGracePeriod(JAXBElement<BigInteger> value) {
        this.gracePeriod = value;
    }

    /**
     * Gets the value of the virtualClientProhibited property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getVirtualClientProhibited() {
        return virtualClientProhibited;
    }

    /**
     * Sets the value of the virtualClientProhibited property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setVirtualClientProhibited(JAXBElement<Boolean> value) {
        this.virtualClientProhibited = value;
    }

}
