
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedTransferEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedTransferEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementInfo" type="{urn:v2.webservices.operations.flexnet.com}transferEntitlementInfoType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedTransferEntitlementDataType", propOrder = {
    "entitlementInfo",
    "reason"
})
public class FailedTransferEntitlementDataType {

    @XmlElement(required = true)
    protected TransferEntitlementInfoType entitlementInfo;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the entitlementInfo property.
     * 
     * @return
     *     possible object is
     *     {@link TransferEntitlementInfoType }
     *     
     */
    public TransferEntitlementInfoType getEntitlementInfo() {
        return entitlementInfo;
    }

    /**
     * Sets the value of the entitlementInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferEntitlementInfoType }
     *     
     */
    public void setEntitlementInfo(TransferEntitlementInfoType value) {
        this.entitlementInfo = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
