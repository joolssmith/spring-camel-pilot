
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LicenseCountChange complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LicenseCountChange"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RightsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Copies" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LicenseCountChange", propOrder = {
    "rightsId",
    "copies"
})
public class LicenseCountChange {

    @XmlElement(name = "RightsId", required = true, nillable = true)
    protected String rightsId;
    @XmlElement(name = "Copies")
    protected int copies;

    /**
     * Gets the value of the rightsId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRightsId() {
        return rightsId;
    }

    /**
     * Sets the value of the rightsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRightsId(String value) {
        this.rightsId = value;
    }

    /**
     * Gets the value of the copies property.
     * 
     */
    public int getCopies() {
        return copies;
    }

    /**
     * Sets the value of the copies property.
     * 
     */
    public void setCopies(int value) {
        this.copies = value;
    }

}
