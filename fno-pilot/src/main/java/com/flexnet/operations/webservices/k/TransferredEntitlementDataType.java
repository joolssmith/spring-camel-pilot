
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transferredEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferredEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementIdentifierType"/&gt;
 *         &lt;element name="lineItemIdentifierMap" type="{urn:com.macrovision:flexnet/operations}transferredLineItemMapType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferredEntitlementDataType", propOrder = {
    "entitlementIdentifier",
    "lineItemIdentifierMap"
})
public class TransferredEntitlementDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementIdentifier;
    protected List<TransferredLineItemMapType> lineItemIdentifierMap;

    /**
     * Gets the value of the entitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }

    /**
     * Sets the value of the entitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementIdentifier(EntitlementIdentifierType value) {
        this.entitlementIdentifier = value;
    }

    /**
     * Gets the value of the lineItemIdentifierMap property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemIdentifierMap property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemIdentifierMap().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransferredLineItemMapType }
     * 
     * 
     */
    public List<TransferredLineItemMapType> getLineItemIdentifierMap() {
        if (lineItemIdentifierMap == null) {
            lineItemIdentifierMap = new ArrayList<TransferredLineItemMapType>();
        }
        return this.lineItemIdentifierMap;
    }

}
