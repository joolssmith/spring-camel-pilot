
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMatchingLineItemsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMatchingLineItemsRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemList" type="{urn:v3.webservices.operations.flexnet.com}getMatchingLineItemsListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMatchingLineItemsRequestType", propOrder = {
    "lineItemList"
})
public class GetMatchingLineItemsRequestType {

    @XmlElement(required = true)
    protected GetMatchingLineItemsListType lineItemList;

    /**
     * Gets the value of the lineItemList property.
     * 
     * @return
     *     possible object is
     *     {@link GetMatchingLineItemsListType }
     *     
     */
    public GetMatchingLineItemsListType getLineItemList() {
        return lineItemList;
    }

    /**
     * Sets the value of the lineItemList property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMatchingLineItemsListType }
     *     
     */
    public void setLineItemList(GetMatchingLineItemsListType value) {
        this.lineItemList = value;
    }

}
