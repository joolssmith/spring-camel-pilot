
package com.flexnet.operations.webservices.v;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for capabilityRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="capabilityRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceIdentifier" type="{urn:v2.fne.webservices.operations.flexnet.com}deviceIdentifier"/&gt;
 *         &lt;element name="lastUpdateTime" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="force" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="vendorDictionary" type="{urn:v2.fne.webservices.operations.flexnet.com}generateCapabilityResponseDictionary" minOccurs="0"/&gt;
 *         &lt;element name="lineItem" type="{urn:v2.fne.webservices.operations.flexnet.com}linkLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="bufferLicense" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "capabilityRequestType", propOrder = {
    "deviceIdentifier",
    "lastUpdateTime",
    "force",
    "vendorDictionary",
    "lineItem",
    "bufferLicense"
})
public class CapabilityRequestType {

    @XmlElement(required = true)
    protected DeviceIdentifier deviceIdentifier;
    protected Long lastUpdateTime;
    protected Boolean force;
    protected GenerateCapabilityResponseDictionary vendorDictionary;
    protected List<LinkLineItemDataType> lineItem;
    protected Boolean bufferLicense;

    /**
     * Gets the value of the deviceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getDeviceIdentifier() {
        return deviceIdentifier;
    }

    /**
     * Sets the value of the deviceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setDeviceIdentifier(DeviceIdentifier value) {
        this.deviceIdentifier = value;
    }

    /**
     * Gets the value of the lastUpdateTime property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLastUpdateTime() {
        return lastUpdateTime;
    }

    /**
     * Sets the value of the lastUpdateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLastUpdateTime(Long value) {
        this.lastUpdateTime = value;
    }

    /**
     * Gets the value of the force property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForce() {
        return force;
    }

    /**
     * Sets the value of the force property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForce(Boolean value) {
        this.force = value;
    }

    /**
     * Gets the value of the vendorDictionary property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateCapabilityResponseDictionary }
     *     
     */
    public GenerateCapabilityResponseDictionary getVendorDictionary() {
        return vendorDictionary;
    }

    /**
     * Sets the value of the vendorDictionary property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateCapabilityResponseDictionary }
     *     
     */
    public void setVendorDictionary(GenerateCapabilityResponseDictionary value) {
        this.vendorDictionary = value;
    }

    /**
     * Gets the value of the lineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LinkLineItemDataType }
     * 
     * 
     */
    public List<LinkLineItemDataType> getLineItem() {
        if (lineItem == null) {
            lineItem = new ArrayList<LinkLineItemDataType>();
        }
        return this.lineItem;
    }

    /**
     * Gets the value of the bufferLicense property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBufferLicense() {
        return bufferLicense;
    }

    /**
     * Sets the value of the bufferLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBufferLicense(Boolean value) {
        this.bufferLicense = value;
    }

}
