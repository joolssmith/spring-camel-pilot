
package com.flexnet.operations.webservices.o;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for entitlementLifeCycleDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entitlementLifeCycleDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentEntitlementIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="lineItemData" type="{urn:v1.webservices.operations.flexnet.com}lineItemLifeCycleDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entitlementLifeCycleDataType", propOrder = {
    "parentEntitlementIdentifier",
    "lineItemData"
})
public class EntitlementLifeCycleDataType {

    protected EntitlementIdentifierType parentEntitlementIdentifier;
    @XmlElement(required = true)
    protected List<LineItemLifeCycleDataType> lineItemData;

    /**
     * Gets the value of the parentEntitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getParentEntitlementIdentifier() {
        return parentEntitlementIdentifier;
    }

    /**
     * Sets the value of the parentEntitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setParentEntitlementIdentifier(EntitlementIdentifierType value) {
        this.parentEntitlementIdentifier = value;
    }

    /**
     * Gets the value of the lineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LineItemLifeCycleDataType }
     * 
     * 
     */
    public List<LineItemLifeCycleDataType> getLineItemData() {
        if (lineItemData == null) {
            lineItemData = new ArrayList<LineItemLifeCycleDataType>();
        }
        return this.lineItemData;
    }

}
