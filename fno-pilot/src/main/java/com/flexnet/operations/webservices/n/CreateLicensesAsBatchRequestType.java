
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createLicensesAsBatchRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createLicensesAsBatchRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationIds" type="{urn:v1.webservices.operations.flexnet.com}activationIdsListType"/&gt;
 *         &lt;element name="hostIdDataSet" type="{urn:v1.webservices.operations.flexnet.com}hostIdDataSetType"/&gt;
 *         &lt;element name="countDataSet" type="{urn:v1.webservices.operations.flexnet.com}countDataSetType" minOccurs="0"/&gt;
 *         &lt;element name="commonBatchDataSet" type="{urn:v1.webservices.operations.flexnet.com}commonBatchDataSetType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createLicensesAsBatchRequestType", propOrder = {
    "activationIds",
    "hostIdDataSet",
    "countDataSet",
    "commonBatchDataSet"
})
public class CreateLicensesAsBatchRequestType {

    @XmlElement(required = true)
    protected ActivationIdsListType activationIds;
    @XmlElement(required = true)
    protected HostIdDataSetType hostIdDataSet;
    protected CountDataSetType countDataSet;
    @XmlElement(required = true)
    protected CommonBatchDataSetType commonBatchDataSet;

    /**
     * Gets the value of the activationIds property.
     * 
     * @return
     *     possible object is
     *     {@link ActivationIdsListType }
     *     
     */
    public ActivationIdsListType getActivationIds() {
        return activationIds;
    }

    /**
     * Sets the value of the activationIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivationIdsListType }
     *     
     */
    public void setActivationIds(ActivationIdsListType value) {
        this.activationIds = value;
    }

    /**
     * Gets the value of the hostIdDataSet property.
     * 
     * @return
     *     possible object is
     *     {@link HostIdDataSetType }
     *     
     */
    public HostIdDataSetType getHostIdDataSet() {
        return hostIdDataSet;
    }

    /**
     * Sets the value of the hostIdDataSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostIdDataSetType }
     *     
     */
    public void setHostIdDataSet(HostIdDataSetType value) {
        this.hostIdDataSet = value;
    }

    /**
     * Gets the value of the countDataSet property.
     * 
     * @return
     *     possible object is
     *     {@link CountDataSetType }
     *     
     */
    public CountDataSetType getCountDataSet() {
        return countDataSet;
    }

    /**
     * Sets the value of the countDataSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountDataSetType }
     *     
     */
    public void setCountDataSet(CountDataSetType value) {
        this.countDataSet = value;
    }

    /**
     * Gets the value of the commonBatchDataSet property.
     * 
     * @return
     *     possible object is
     *     {@link CommonBatchDataSetType }
     *     
     */
    public CommonBatchDataSetType getCommonBatchDataSet() {
        return commonBatchDataSet;
    }

    /**
     * Sets the value of the commonBatchDataSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link CommonBatchDataSetType }
     *     
     */
    public void setCommonBatchDataSet(CommonBatchDataSetType value) {
        this.commonBatchDataSet = value;
    }

}
