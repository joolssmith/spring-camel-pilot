
package com.flexnet.operations.webservices.v;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDevicesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDevicesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v2.fne.webservices.operations.flexnet.com}getDevicesParametersType" minOccurs="0"/&gt;
 *         &lt;element name="deviceResponseConfig" type="{urn:v2.fne.webservices.operations.flexnet.com}deviceResponseConfigRequestType" minOccurs="0"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDevicesRequestType", propOrder = {
    "queryParams",
    "deviceResponseConfig",
    "pageNumber",
    "batchSize"
})
public class GetDevicesRequestType {

    protected GetDevicesParametersType queryParams;
    protected DeviceResponseConfigRequestType deviceResponseConfig;
    @XmlElement(required = true, nillable = true)
    protected BigInteger pageNumber;
    @XmlElement(required = true, nillable = true)
    protected BigInteger batchSize;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link GetDevicesParametersType }
     *     
     */
    public GetDevicesParametersType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDevicesParametersType }
     *     
     */
    public void setQueryParams(GetDevicesParametersType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the deviceResponseConfig property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceResponseConfigRequestType }
     *     
     */
    public DeviceResponseConfigRequestType getDeviceResponseConfig() {
        return deviceResponseConfig;
    }

    /**
     * Sets the value of the deviceResponseConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceResponseConfigRequestType }
     *     
     */
    public void setDeviceResponseConfig(DeviceResponseConfigRequestType value) {
        this.deviceResponseConfig = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

}
