
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for splitLineItemRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="splitLineItemRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemList" type="{urn:com.macrovision:flexnet/operations}splitLineItemListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "splitLineItemRequestType", propOrder = {
    "lineItemList"
})
public class SplitLineItemRequestType {

    @XmlElement(required = true)
    protected SplitLineItemListType lineItemList;

    /**
     * Gets the value of the lineItemList property.
     * 
     * @return
     *     possible object is
     *     {@link SplitLineItemListType }
     *     
     */
    public SplitLineItemListType getLineItemList() {
        return lineItemList;
    }

    /**
     * Sets the value of the lineItemList property.
     * 
     * @param value
     *     allowed object is
     *     {@link SplitLineItemListType }
     *     
     */
    public void setLineItemList(SplitLineItemListType value) {
        this.lineItemList = value;
    }

}
