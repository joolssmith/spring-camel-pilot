
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFmtAttributesForBatchActivationRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFmtAttributesForBatchActivationRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationIds" type="{urn:v1.webservices.operations.flexnet.com}activationIdsListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFmtAttributesForBatchActivationRequestType", propOrder = {
    "activationIds"
})
public class GetFmtAttributesForBatchActivationRequestType {

    @XmlElement(required = true)
    protected ActivationIdsListType activationIds;

    /**
     * Gets the value of the activationIds property.
     * 
     * @return
     *     possible object is
     *     {@link ActivationIdsListType }
     *     
     */
    public ActivationIdsListType getActivationIds() {
        return activationIds;
    }

    /**
     * Sets the value of the activationIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivationIdsListType }
     *     
     */
    public void setActivationIds(ActivationIdsListType value) {
        this.activationIds = value;
    }

}
