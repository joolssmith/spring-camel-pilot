
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for splitBulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="splitBulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="newBulkEntIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "splitBulkEntitlementDataType", propOrder = {
    "bulkEntIdentifier",
    "newBulkEntIdentifier"
})
public class SplitBulkEntitlementDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType bulkEntIdentifier;
    protected EntitlementIdentifierType newBulkEntIdentifier;

    /**
     * Gets the value of the bulkEntIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getBulkEntIdentifier() {
        return bulkEntIdentifier;
    }

    /**
     * Sets the value of the bulkEntIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setBulkEntIdentifier(EntitlementIdentifierType value) {
        this.bulkEntIdentifier = value;
    }

    /**
     * Gets the value of the newBulkEntIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getNewBulkEntIdentifier() {
        return newBulkEntIdentifier;
    }

    /**
     * Sets the value of the newBulkEntIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setNewBulkEntIdentifier(EntitlementIdentifierType value) {
        this.newBulkEntIdentifier = value;
    }

}
