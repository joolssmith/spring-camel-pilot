
package com.flexnet.operations.webservices.p;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for licenseStateType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="licenseStateType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="UNDEFINED"/&gt;
 *     &lt;enumeration value="LICENSE_NOT_GENERATED"/&gt;
 *     &lt;enumeration value="LICENSE_GENERATED"/&gt;
 *     &lt;enumeration value="MARKED_FOR_REMOVAL"/&gt;
 *     &lt;enumeration value="REMOVED_FROM_LICENSE"/&gt;
 *     &lt;enumeration value="REMOVAL_PENDING_CONFIRMATION"/&gt;
 *     &lt;enumeration value="COPIES_INCREASING"/&gt;
 *     &lt;enumeration value="COPIES_DECREASING"/&gt;
 *     &lt;enumeration value="WAITING_FOR_CONFIRMATION"/&gt;
 *     &lt;enumeration value="EXPIRED"/&gt;
 *     &lt;enumeration value="OBSOLETE"/&gt;
 *     &lt;enumeration value="INACTIVE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "licenseStateType")
@XmlEnum
public enum LicenseStateType {

    UNDEFINED,
    LICENSE_NOT_GENERATED,
    LICENSE_GENERATED,
    MARKED_FOR_REMOVAL,
    REMOVED_FROM_LICENSE,
    REMOVAL_PENDING_CONFIRMATION,
    COPIES_INCREASING,
    COPIES_DECREASING,
    WAITING_FOR_CONFIRMATION,
    EXPIRED,
    OBSOLETE,
    INACTIVE;

    public String value() {
        return name();
    }

    public static LicenseStateType fromValue(String v) {
        return valueOf(v);
    }

}
