
package com.flexnet.operations.webservices.r;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateProductRelationshipDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateProductRelationshipDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedRelationship" type="{urn:v1.webservices.operations.flexnet.com}failedUpdateProductRelationshipDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateProductRelationshipDataListType", propOrder = {
    "failedRelationship"
})
public class FailedUpdateProductRelationshipDataListType {

    protected List<FailedUpdateProductRelationshipDataType> failedRelationship;

    /**
     * Gets the value of the failedRelationship property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedRelationship property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedRelationship().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUpdateProductRelationshipDataType }
     * 
     * 
     */
    public List<FailedUpdateProductRelationshipDataType> getFailedRelationship() {
        if (failedRelationship == null) {
            failedRelationship = new ArrayList<FailedUpdateProductRelationshipDataType>();
        }
        return this.failedRelationship;
    }

}
