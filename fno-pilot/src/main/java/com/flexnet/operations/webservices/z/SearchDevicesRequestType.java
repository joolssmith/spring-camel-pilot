
package com.flexnet.operations.webservices.z;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchDevicesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchDevicesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v3.fne.webservices.operations.flexnet.com}searchDevicesParametersType" minOccurs="0"/&gt;
 *         &lt;element name="responseConfig" type="{urn:v3.fne.webservices.operations.flexnet.com}searchDeviceResponseConfigType" minOccurs="0"/&gt;
 *         &lt;element name="sortBys" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceSortBys" minOccurs="0"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchDevicesRequestType", propOrder = {
    "queryParams",
    "responseConfig",
    "sortBys",
    "pageNumber",
    "batchSize"
})
public class SearchDevicesRequestType {

    protected SearchDevicesParametersType queryParams;
    protected SearchDeviceResponseConfigType responseConfig;
    protected DeviceSortBys sortBys;
    protected BigInteger pageNumber;
    protected BigInteger batchSize;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDevicesParametersType }
     *     
     */
    public SearchDevicesParametersType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDevicesParametersType }
     *     
     */
    public void setQueryParams(SearchDevicesParametersType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the responseConfig property.
     * 
     * @return
     *     possible object is
     *     {@link SearchDeviceResponseConfigType }
     *     
     */
    public SearchDeviceResponseConfigType getResponseConfig() {
        return responseConfig;
    }

    /**
     * Sets the value of the responseConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchDeviceResponseConfigType }
     *     
     */
    public void setResponseConfig(SearchDeviceResponseConfigType value) {
        this.responseConfig = value;
    }

    /**
     * Gets the value of the sortBys property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceSortBys }
     *     
     */
    public DeviceSortBys getSortBys() {
        return sortBys;
    }

    /**
     * Sets the value of the sortBys property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceSortBys }
     *     
     */
    public void setSortBys(DeviceSortBys value) {
        this.sortBys = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

}
