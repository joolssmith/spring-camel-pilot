
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedAddWebRegKeyDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedAddWebRegKeyDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="webRegKeyData" type="{urn:v2.webservices.operations.flexnet.com}addWebRegKeyDataType" minOccurs="0"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedAddWebRegKeyDataType", propOrder = {
    "webRegKeyData",
    "reason"
})
public class FailedAddWebRegKeyDataType {

    protected AddWebRegKeyDataType webRegKeyData;
    protected String reason;

    /**
     * Gets the value of the webRegKeyData property.
     * 
     * @return
     *     possible object is
     *     {@link AddWebRegKeyDataType }
     *     
     */
    public AddWebRegKeyDataType getWebRegKeyData() {
        return webRegKeyData;
    }

    /**
     * Sets the value of the webRegKeyData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddWebRegKeyDataType }
     *     
     */
    public void setWebRegKeyData(AddWebRegKeyDataType value) {
        this.webRegKeyData = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
