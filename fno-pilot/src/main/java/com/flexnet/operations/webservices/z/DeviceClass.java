
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceClass.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="deviceClass"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="CLIENT"/&gt;
 *     &lt;enumeration value="SERVER"/&gt;
 *     &lt;enumeration value="SERVED_CLIENT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "deviceClass")
@XmlEnum
public enum DeviceClass {

    CLIENT,
    SERVER,
    SERVED_CLIENT;

    public String value() {
        return name();
    }

    public static DeviceClass fromValue(String v) {
        return valueOf(v);
    }

}
