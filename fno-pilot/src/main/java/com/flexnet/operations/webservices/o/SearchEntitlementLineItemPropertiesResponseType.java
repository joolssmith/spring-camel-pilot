
package com.flexnet.operations.webservices.o;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchEntitlementLineItemPropertiesResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchEntitlementLineItemPropertiesResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="entitlementLineItem" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemPropertiesType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchEntitlementLineItemPropertiesResponseType", propOrder = {
    "statusInfo",
    "entitlementLineItem"
})
public class SearchEntitlementLineItemPropertiesResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected List<EntitlementLineItemPropertiesType> entitlementLineItem;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the entitlementLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entitlementLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntitlementLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntitlementLineItemPropertiesType }
     * 
     * 
     */
    public List<EntitlementLineItemPropertiesType> getEntitlementLineItem() {
        if (entitlementLineItem == null) {
            entitlementLineItem = new ArrayList<EntitlementLineItemPropertiesType>();
        }
        return this.entitlementLineItem;
    }

}
