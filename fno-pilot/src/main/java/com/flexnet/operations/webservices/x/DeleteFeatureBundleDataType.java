
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteFeatureBundleDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteFeatureBundleDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundleIdentifier" type="{urn:v2.webservices.operations.flexnet.com}featureBundleIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteFeatureBundleDataType", propOrder = {
    "featureBundleIdentifier"
})
public class DeleteFeatureBundleDataType {

    @XmlElement(required = true)
    protected FeatureBundleIdentifierType featureBundleIdentifier;

    /**
     * Gets the value of the featureBundleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public FeatureBundleIdentifierType getFeatureBundleIdentifier() {
        return featureBundleIdentifier;
    }

    /**
     * Sets the value of the featureBundleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public void setFeatureBundleIdentifier(FeatureBundleIdentifierType value) {
        this.featureBundleIdentifier = value;
    }

}
