
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for acpiGenerationIdLicensePolicyDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="acpiGenerationIdLicensePolicyDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="useACPIGenerationId" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "acpiGenerationIdLicensePolicyDataType", propOrder = {
    "useACPIGenerationId"
})
public class AcpiGenerationIdLicensePolicyDataType {

    protected boolean useACPIGenerationId;

    /**
     * Gets the value of the useACPIGenerationId property.
     * 
     */
    public boolean isUseACPIGenerationId() {
        return useACPIGenerationId;
    }

    /**
     * Sets the value of the useACPIGenerationId property.
     * 
     */
    public void setUseACPIGenerationId(boolean value) {
        this.useACPIGenerationId = value;
    }

}
