
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchBulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchBulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="productName" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="productVersion" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="productType" type="{urn:com.macrovision:flexnet/operations}ProductType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="shipToEmail" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{urn:com.macrovision:flexnet/operations}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="createdOn" type="{urn:com.macrovision:flexnet/operations}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{urn:com.macrovision:flexnet/operations}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDate" type="{urn:com.macrovision:flexnet/operations}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:com.macrovision:flexnet/operations}StateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="createdUserId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:com.macrovision:flexnet/operations}customAttributesQueryListType" minOccurs="0"/&gt;
 *         &lt;element name="allowPortalLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="organizationUnitName" type="{urn:com.macrovision:flexnet/operations}PartnerTierQueryType" minOccurs="0"/&gt;
 *         &lt;element name="currentOwnerName" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchBulkEntitlementDataType", propOrder = {
    "entitlementId",
    "description",
    "productName",
    "productVersion",
    "productType",
    "partNumber",
    "soldTo",
    "shipToEmail",
    "shipToAddress",
    "isPermanent",
    "expirationDate",
    "createdOn",
    "versionDate",
    "lastModifiedDate",
    "state",
    "licenseTechnology",
    "createdUserId",
    "customAttributes",
    "allowPortalLogin",
    "organizationUnitName",
    "currentOwnerName"
})
public class SearchBulkEntitlementDataType {

    protected SimpleQueryType entitlementId;
    protected SimpleQueryType description;
    protected SimpleQueryType productName;
    protected SimpleQueryType productVersion;
    @XmlSchemaType(name = "NMTOKEN")
    protected ProductType productType;
    protected SimpleQueryType partNumber;
    protected SimpleQueryType soldTo;
    protected SimpleQueryType shipToEmail;
    protected SimpleQueryType shipToAddress;
    protected Boolean isPermanent;
    protected DateQueryType expirationDate;
    protected DateQueryType createdOn;
    protected DateQueryType versionDate;
    protected DateQueryType lastModifiedDate;
    protected StateQueryType state;
    protected SimpleQueryType licenseTechnology;
    protected SimpleQueryType createdUserId;
    protected CustomAttributesQueryListType customAttributes;
    protected Boolean allowPortalLogin;
    protected PartnerTierQueryType organizationUnitName;
    protected SimpleQueryType currentOwnerName;

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setEntitlementId(SimpleQueryType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDescription(SimpleQueryType value) {
        this.description = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setProductName(SimpleQueryType value) {
        this.productName = value;
    }

    /**
     * Gets the value of the productVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getProductVersion() {
        return productVersion;
    }

    /**
     * Sets the value of the productVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setProductVersion(SimpleQueryType value) {
        this.productVersion = value;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *     possible object is
     *     {@link ProductType }
     *     
     */
    public ProductType getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductType }
     *     
     */
    public void setProductType(ProductType value) {
        this.productType = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setPartNumber(SimpleQueryType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldTo(SimpleQueryType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setShipToEmail(SimpleQueryType value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setShipToAddress(SimpleQueryType value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setExpirationDate(DateQueryType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the createdOn property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getCreatedOn() {
        return createdOn;
    }

    /**
     * Sets the value of the createdOn property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setCreatedOn(DateQueryType value) {
        this.createdOn = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setVersionDate(DateQueryType value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the lastModifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets the value of the lastModifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setLastModifiedDate(DateQueryType value) {
        this.lastModifiedDate = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateQueryType }
     *     
     */
    public StateQueryType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateQueryType }
     *     
     */
    public void setState(StateQueryType value) {
        this.state = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setLicenseTechnology(SimpleQueryType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the createdUserId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCreatedUserId() {
        return createdUserId;
    }

    /**
     * Sets the value of the createdUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCreatedUserId(SimpleQueryType value) {
        this.createdUserId = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public CustomAttributesQueryListType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public void setCustomAttributes(CustomAttributesQueryListType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the allowPortalLogin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPortalLogin() {
        return allowPortalLogin;
    }

    /**
     * Sets the value of the allowPortalLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPortalLogin(Boolean value) {
        this.allowPortalLogin = value;
    }

    /**
     * Gets the value of the organizationUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public PartnerTierQueryType getOrganizationUnitName() {
        return organizationUnitName;
    }

    /**
     * Sets the value of the organizationUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public void setOrganizationUnitName(PartnerTierQueryType value) {
        this.organizationUnitName = value;
    }

    /**
     * Gets the value of the currentOwnerName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCurrentOwnerName() {
        return currentOwnerName;
    }

    /**
     * Sets the value of the currentOwnerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCurrentOwnerName(SimpleQueryType value) {
        this.currentOwnerName = value;
    }

}
