
package com.flexnet.operations.webservices.x;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createPartNumberRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createPartNumberRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partNumber" type="{urn:v2.webservices.operations.flexnet.com}createPartNumberDataType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="processSync" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createPartNumberRequestType", propOrder = {
    "partNumber",
    "processSync"
})
public class CreatePartNumberRequestType {

    @XmlElement(required = true)
    protected List<CreatePartNumberDataType> partNumber;
    protected Boolean processSync;

    /**
     * Gets the value of the partNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatePartNumberDataType }
     * 
     * 
     */
    public List<CreatePartNumberDataType> getPartNumber() {
        if (partNumber == null) {
            partNumber = new ArrayList<CreatePartNumberDataType>();
        }
        return this.partNumber;
    }

    /**
     * Gets the value of the processSync property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProcessSync() {
        return processSync;
    }

    /**
     * Sets the value of the processSync property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessSync(Boolean value) {
        this.processSync = value;
    }

}
