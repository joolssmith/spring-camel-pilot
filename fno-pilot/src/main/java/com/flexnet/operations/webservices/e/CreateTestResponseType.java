
package com.flexnet.operations.webservices.e;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for createTestResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createTestResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="intCount" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="integerCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="doubleCount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="floatCount" type="{http://www.w3.org/2001/XMLSchema}float" minOccurs="0"/&gt;
 *         &lt;element name="longCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="shortCount" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/&gt;
 *         &lt;element name="unsignedIntCount" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" minOccurs="0"/&gt;
 *         &lt;element name="unsignedLongCount" type="{http://www.w3.org/2001/XMLSchema}unsignedLong" minOccurs="0"/&gt;
 *         &lt;element name="unsignedShortCount" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/&gt;
 *         &lt;element name="unsignedByteField" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" minOccurs="0"/&gt;
 *         &lt;element name="booleanField" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="byteField" type="{http://www.w3.org/2001/XMLSchema}byte" minOccurs="0"/&gt;
 *         &lt;element name="dateField" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="dateTimeField" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="decimalField" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="nonNegIntField" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="nonPosIntField" type="{http://www.w3.org/2001/XMLSchema}nonPositiveInteger" minOccurs="0"/&gt;
 *         &lt;element name="posIntField" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/&gt;
 *         &lt;element name="negIntField" type="{http://www.w3.org/2001/XMLSchema}negativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="weekday" type="{urn:com.flexerasoftware:operations/test}weekday" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{urn:com.flexerasoftware:operations/test}testStatusInfoType" minOccurs="0"/&gt;
 *         &lt;element name="weekdays" type="{urn:com.flexerasoftware:operations/test}weekDays" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createTestResponseType", propOrder = {
    "name",
    "intCount",
    "integerCount",
    "doubleCount",
    "floatCount",
    "longCount",
    "shortCount",
    "unsignedIntCount",
    "unsignedLongCount",
    "unsignedShortCount",
    "unsignedByteField",
    "booleanField",
    "byteField",
    "dateField",
    "dateTimeField",
    "decimalField",
    "nonNegIntField",
    "nonPosIntField",
    "posIntField",
    "negIntField",
    "weekday",
    "status",
    "weekdays"
})
public class CreateTestResponseType {

    protected String name;
    protected Integer intCount;
    protected BigInteger integerCount;
    protected Double doubleCount;
    protected Float floatCount;
    protected Long longCount;
    protected Short shortCount;
    @XmlSchemaType(name = "unsignedInt")
    protected Long unsignedIntCount;
    @XmlSchemaType(name = "unsignedLong")
    protected BigInteger unsignedLongCount;
    @XmlSchemaType(name = "unsignedShort")
    protected Integer unsignedShortCount;
    @XmlSchemaType(name = "unsignedByte")
    protected Short unsignedByteField;
    protected Boolean booleanField;
    protected Byte byteField;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateField;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dateTimeField;
    protected BigDecimal decimalField;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger nonNegIntField;
    @XmlSchemaType(name = "nonPositiveInteger")
    protected BigInteger nonPosIntField;
    @XmlSchemaType(name = "positiveInteger")
    protected BigInteger posIntField;
    @XmlSchemaType(name = "negativeInteger")
    protected BigInteger negIntField;
    @XmlSchemaType(name = "NMTOKEN")
    protected Weekday weekday;
    protected TestStatusInfoType status;
    protected WeekDays weekdays;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the intCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIntCount() {
        return intCount;
    }

    /**
     * Sets the value of the intCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIntCount(Integer value) {
        this.intCount = value;
    }

    /**
     * Gets the value of the integerCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIntegerCount() {
        return integerCount;
    }

    /**
     * Sets the value of the integerCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIntegerCount(BigInteger value) {
        this.integerCount = value;
    }

    /**
     * Gets the value of the doubleCount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getDoubleCount() {
        return doubleCount;
    }

    /**
     * Sets the value of the doubleCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setDoubleCount(Double value) {
        this.doubleCount = value;
    }

    /**
     * Gets the value of the floatCount property.
     * 
     * @return
     *     possible object is
     *     {@link Float }
     *     
     */
    public Float getFloatCount() {
        return floatCount;
    }

    /**
     * Sets the value of the floatCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Float }
     *     
     */
    public void setFloatCount(Float value) {
        this.floatCount = value;
    }

    /**
     * Gets the value of the longCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLongCount() {
        return longCount;
    }

    /**
     * Sets the value of the longCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLongCount(Long value) {
        this.longCount = value;
    }

    /**
     * Gets the value of the shortCount property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getShortCount() {
        return shortCount;
    }

    /**
     * Sets the value of the shortCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setShortCount(Short value) {
        this.shortCount = value;
    }

    /**
     * Gets the value of the unsignedIntCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUnsignedIntCount() {
        return unsignedIntCount;
    }

    /**
     * Sets the value of the unsignedIntCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUnsignedIntCount(Long value) {
        this.unsignedIntCount = value;
    }

    /**
     * Gets the value of the unsignedLongCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnsignedLongCount() {
        return unsignedLongCount;
    }

    /**
     * Sets the value of the unsignedLongCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnsignedLongCount(BigInteger value) {
        this.unsignedLongCount = value;
    }

    /**
     * Gets the value of the unsignedShortCount property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUnsignedShortCount() {
        return unsignedShortCount;
    }

    /**
     * Sets the value of the unsignedShortCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUnsignedShortCount(Integer value) {
        this.unsignedShortCount = value;
    }

    /**
     * Gets the value of the unsignedByteField property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getUnsignedByteField() {
        return unsignedByteField;
    }

    /**
     * Sets the value of the unsignedByteField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setUnsignedByteField(Short value) {
        this.unsignedByteField = value;
    }

    /**
     * Gets the value of the booleanField property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBooleanField() {
        return booleanField;
    }

    /**
     * Sets the value of the booleanField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBooleanField(Boolean value) {
        this.booleanField = value;
    }

    /**
     * Gets the value of the byteField property.
     * 
     * @return
     *     possible object is
     *     {@link Byte }
     *     
     */
    public Byte getByteField() {
        return byteField;
    }

    /**
     * Sets the value of the byteField property.
     * 
     * @param value
     *     allowed object is
     *     {@link Byte }
     *     
     */
    public void setByteField(Byte value) {
        this.byteField = value;
    }

    /**
     * Gets the value of the dateField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateField() {
        return dateField;
    }

    /**
     * Sets the value of the dateField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateField(XMLGregorianCalendar value) {
        this.dateField = value;
    }

    /**
     * Gets the value of the dateTimeField property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateTimeField() {
        return dateTimeField;
    }

    /**
     * Sets the value of the dateTimeField property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateTimeField(XMLGregorianCalendar value) {
        this.dateTimeField = value;
    }

    /**
     * Gets the value of the decimalField property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDecimalField() {
        return decimalField;
    }

    /**
     * Sets the value of the decimalField property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDecimalField(BigDecimal value) {
        this.decimalField = value;
    }

    /**
     * Gets the value of the nonNegIntField property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNonNegIntField() {
        return nonNegIntField;
    }

    /**
     * Sets the value of the nonNegIntField property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNonNegIntField(BigInteger value) {
        this.nonNegIntField = value;
    }

    /**
     * Gets the value of the nonPosIntField property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNonPosIntField() {
        return nonPosIntField;
    }

    /**
     * Sets the value of the nonPosIntField property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNonPosIntField(BigInteger value) {
        this.nonPosIntField = value;
    }

    /**
     * Gets the value of the posIntField property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPosIntField() {
        return posIntField;
    }

    /**
     * Sets the value of the posIntField property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPosIntField(BigInteger value) {
        this.posIntField = value;
    }

    /**
     * Gets the value of the negIntField property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNegIntField() {
        return negIntField;
    }

    /**
     * Sets the value of the negIntField property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNegIntField(BigInteger value) {
        this.negIntField = value;
    }

    /**
     * Gets the value of the weekday property.
     * 
     * @return
     *     possible object is
     *     {@link Weekday }
     *     
     */
    public Weekday getWeekday() {
        return weekday;
    }

    /**
     * Sets the value of the weekday property.
     * 
     * @param value
     *     allowed object is
     *     {@link Weekday }
     *     
     */
    public void setWeekday(Weekday value) {
        this.weekday = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link TestStatusInfoType }
     *     
     */
    public TestStatusInfoType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link TestStatusInfoType }
     *     
     */
    public void setStatus(TestStatusInfoType value) {
        this.status = value;
    }

    /**
     * Gets the value of the weekdays property.
     * 
     * @return
     *     possible object is
     *     {@link WeekDays }
     *     
     */
    public WeekDays getWeekdays() {
        return weekdays;
    }

    /**
     * Sets the value of the weekdays property.
     * 
     * @param value
     *     allowed object is
     *     {@link WeekDays }
     *     
     */
    public void setWeekdays(WeekDays value) {
        this.weekdays = value;
    }

}
