
package com.flexnet.operations.webservices.q;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for organizationQueryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="organizationQueryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="orgName" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="orgDisplayName" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="address1" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="address2" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="city" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="zipcode" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="country" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="region" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="onlyRootOrgs" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="orgType" type="{urn:v1.webservices.operations.flexnet.com}OrgType" minOccurs="0"/&gt;
 *         &lt;element name="orgTypeList" type="{urn:v1.webservices.operations.flexnet.com}OrgTypeList" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v1.webservices.operations.flexnet.com}orgCustomAttributesQueryListType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{urn:v1.webservices.operations.flexnet.com}DateTimeQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "organizationQueryParametersType", propOrder = {
    "orgName",
    "orgDisplayName",
    "description",
    "address1",
    "address2",
    "city",
    "state",
    "zipcode",
    "country",
    "region",
    "onlyRootOrgs",
    "orgType",
    "orgTypeList",
    "customAttributes",
    "lastModifiedDateTime"
})
public class OrganizationQueryParametersType {

    protected SimpleQueryType orgName;
    protected SimpleQueryType orgDisplayName;
    protected SimpleQueryType description;
    protected SimpleQueryType address1;
    protected SimpleQueryType address2;
    protected SimpleQueryType city;
    protected SimpleQueryType state;
    protected SimpleQueryType zipcode;
    protected SimpleQueryType country;
    protected SimpleQueryType region;
    protected Boolean onlyRootOrgs;
    @XmlSchemaType(name = "NMTOKEN")
    protected OrgType orgType;
    protected OrgTypeList orgTypeList;
    protected OrgCustomAttributesQueryListType customAttributes;
    protected DateTimeQueryType lastModifiedDateTime;

    /**
     * Gets the value of the orgName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getOrgName() {
        return orgName;
    }

    /**
     * Sets the value of the orgName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setOrgName(SimpleQueryType value) {
        this.orgName = value;
    }

    /**
     * Gets the value of the orgDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getOrgDisplayName() {
        return orgDisplayName;
    }

    /**
     * Sets the value of the orgDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setOrgDisplayName(SimpleQueryType value) {
        this.orgDisplayName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDescription(SimpleQueryType value) {
        this.description = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddress1(SimpleQueryType value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddress2(SimpleQueryType value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCity(SimpleQueryType value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setState(SimpleQueryType value) {
        this.state = value;
    }

    /**
     * Gets the value of the zipcode property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getZipcode() {
        return zipcode;
    }

    /**
     * Sets the value of the zipcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setZipcode(SimpleQueryType value) {
        this.zipcode = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCountry(SimpleQueryType value) {
        this.country = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setRegion(SimpleQueryType value) {
        this.region = value;
    }

    /**
     * Gets the value of the onlyRootOrgs property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOnlyRootOrgs() {
        return onlyRootOrgs;
    }

    /**
     * Sets the value of the onlyRootOrgs property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOnlyRootOrgs(Boolean value) {
        this.onlyRootOrgs = value;
    }

    /**
     * Gets the value of the orgType property.
     * 
     * @return
     *     possible object is
     *     {@link OrgType }
     *     
     */
    public OrgType getOrgType() {
        return orgType;
    }

    /**
     * Sets the value of the orgType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgType }
     *     
     */
    public void setOrgType(OrgType value) {
        this.orgType = value;
    }

    /**
     * Gets the value of the orgTypeList property.
     * 
     * @return
     *     possible object is
     *     {@link OrgTypeList }
     *     
     */
    public OrgTypeList getOrgTypeList() {
        return orgTypeList;
    }

    /**
     * Sets the value of the orgTypeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgTypeList }
     *     
     */
    public void setOrgTypeList(OrgTypeList value) {
        this.orgTypeList = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link OrgCustomAttributesQueryListType }
     *     
     */
    public OrgCustomAttributesQueryListType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgCustomAttributesQueryListType }
     *     
     */
    public void setCustomAttributes(OrgCustomAttributesQueryListType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setLastModifiedDateTime(DateTimeQueryType value) {
        this.lastModifiedDateTime = value;
    }

}
