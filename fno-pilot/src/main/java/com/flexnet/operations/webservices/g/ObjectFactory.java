
package com.flexnet.operations.webservices.g;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.g package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateFeatureRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createFeatureRequest");
    private final static QName _CreateFeatureResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createFeatureResponse");
    private final static QName _UpdateFeatureRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateFeatureRequest");
    private final static QName _UpdateFeatureResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateFeatureResponse");
    private final static QName _DeleteFeatureRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteFeatureRequest");
    private final static QName _DeleteFeatureResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteFeatureResponse");
    private final static QName _GetFeatureCountRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeatureCountRequest");
    private final static QName _GetFeatureCountResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeatureCountResponse");
    private final static QName _GetFeaturesQueryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeaturesQueryRequest");
    private final static QName _GetFeaturesQueryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeaturesQueryResponse");
    private final static QName _SetFeatureStateRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setFeatureStateRequest");
    private final static QName _SetFeatureStateResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setFeatureStateResponse");
    private final static QName _CreateFeatureBundleRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createFeatureBundleRequest");
    private final static QName _CreateFeatureBundleResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createFeatureBundleResponse");
    private final static QName _UpdateFeatureBundleRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateFeatureBundleRequest");
    private final static QName _UpdateFeatureBundleResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateFeatureBundleResponse");
    private final static QName _DeleteFeatureBundleRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteFeatureBundleRequest");
    private final static QName _DeleteFeatureBundleResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteFeatureBundleResponse");
    private final static QName _GetFeatureBundleCountRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeatureBundleCountRequest");
    private final static QName _GetFeatureBundleCountResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeatureBundleCountResponse");
    private final static QName _GetFeatureBundlesQueryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeatureBundlesQueryRequest");
    private final static QName _GetFeatureBundlesQueryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getFeatureBundlesQueryResponse");
    private final static QName _SetFeatureBundleStateRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setFeatureBundleStateRequest");
    private final static QName _SetFeatureBundleStateResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setFeatureBundleStateResponse");
    private final static QName _CreateProductRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createProductRequest");
    private final static QName _CreateProductResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createProductResponse");
    private final static QName _UpdateProductRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateProductRequest");
    private final static QName _UpdateProductResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateProductResponse");
    private final static QName _DeleteProductRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteProductRequest");
    private final static QName _DeleteProductResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteProductResponse");
    private final static QName _GetProductCountRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductCountRequest");
    private final static QName _GetProductCountResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductCountResponse");
    private final static QName _GetProductsQueryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductsQueryRequest");
    private final static QName _GetProductsQueryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductsQueryResponse");
    private final static QName _CreateProductCategoryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createProductCategoryRequest");
    private final static QName _CreateProductCategoryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createProductCategoryResponse");
    private final static QName _GetProductCategoriesRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductCategoriesRequest");
    private final static QName _GetProductCategoriesResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductCategoriesResponse");
    private final static QName _AssignProductCategoriesToUserRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "assignProductCategoriesToUserRequest");
    private final static QName _AssignProductCategoriesToUserResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "assignProductCategoriesToUserResponse");
    private final static QName _RemoveProductCategoriesFromUserRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "removeProductCategoriesFromUserRequest");
    private final static QName _RemoveProductCategoriesFromUserResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "removeProductCategoriesFromUserResponse");
    private final static QName _GetUsersForProductCategoryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getUsersForProductCategoryRequest");
    private final static QName _GetUsersForProductCategoryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getUsersForProductCategoryResponse");
    private final static QName _AssignProductCategoriesToOrgRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "assignProductCategoriesToOrgRequest");
    private final static QName _AssignProductCategoriesToOrgResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "assignProductCategoriesToOrgResponse");
    private final static QName _RemoveProductCategoriesFromOrgRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "removeProductCategoriesFromOrgRequest");
    private final static QName _RemoveProductCategoriesFromOrgResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "removeProductCategoriesFromOrgResponse");
    private final static QName _IsEntitlementVisibleInTargetOrgRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "isEntitlementVisibleInTargetOrgRequest");
    private final static QName _IsEntitlementVisibleInTargetOrgResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "isEntitlementVisibleInTargetOrgResponse");
    private final static QName _IsFulfillmentVisibleInTargetOrgRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "isFulfillmentVisibleInTargetOrgRequest");
    private final static QName _IsFulfillmentVisibleInTargetOrgResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "isFulfillmentVisibleInTargetOrgResponse");
    private final static QName _SetProductStateRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setProductStateRequest");
    private final static QName _SetProductStateResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setProductStateResponse");
    private final static QName _CreateMaintenanceRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createMaintenanceRequest");
    private final static QName _CreateMaintenanceResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createMaintenanceResponse");
    private final static QName _UpdateMaintenanceRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateMaintenanceRequest");
    private final static QName _UpdateMaintenanceResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateMaintenanceResponse");
    private final static QName _DeleteMaintenanceRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteMaintenanceRequest");
    private final static QName _DeleteMaintenanceResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteMaintenanceResponse");
    private final static QName _GetMaintenanceCountRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getMaintenanceCountRequest");
    private final static QName _GetMaintenanceCountResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getMaintenanceCountResponse");
    private final static QName _GetMaintenanceQueryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getMaintenanceQueryRequest");
    private final static QName _GetMaintenanceQueryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getMaintenanceQueryResponse");
    private final static QName _SetMaintenanceStateRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setMaintenanceStateRequest");
    private final static QName _SetMaintenanceStateResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setMaintenanceStateResponse");
    private final static QName _CreateUniformSuiteRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createUniformSuiteRequest");
    private final static QName _CreateUniformSuiteResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createUniformSuiteResponse");
    private final static QName _UpdateUniformSuiteRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateUniformSuiteRequest");
    private final static QName _UpdateUniformSuiteResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateUniformSuiteResponse");
    private final static QName _DeleteUniformSuiteRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteUniformSuiteRequest");
    private final static QName _DeleteUniformSuiteResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteUniformSuiteResponse");
    private final static QName _GetUniformSuiteCountRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getUniformSuiteCountRequest");
    private final static QName _GetUniformSuiteCountResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getUniformSuiteCountResponse");
    private final static QName _GetUniformSuitesQueryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getUniformSuitesQueryRequest");
    private final static QName _GetUniformSuitesQueryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getUniformSuitesQueryResponse");
    private final static QName _SetUniformSuiteStateRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setUniformSuiteStateRequest");
    private final static QName _SetUniformSuiteStateResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "setUniformSuiteStateResponse");
    private final static QName _CreatePartNumberRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createPartNumberRequest");
    private final static QName _CreatePartNumberResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createPartNumberResponse");
    private final static QName _DeletePartNumberRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deletePartNumberRequest");
    private final static QName _DeletePartNumberResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deletePartNumberResponse");
    private final static QName _GetPartNumberCountRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getPartNumberCountRequest");
    private final static QName _GetPartNumberCountResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getPartNumberCountResponse");
    private final static QName _GetPartNumbersQueryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getPartNumbersQueryRequest");
    private final static QName _GetPartNumbersQueryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getPartNumbersQueryResponse");
    private final static QName _CreateProductRelationshipRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createProductRelationshipRequest");
    private final static QName _CreateProductRelationshipResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createProductRelationshipResponse");
    private final static QName _UpdateProductRelationshipRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateProductRelationshipRequest");
    private final static QName _UpdateProductRelationshipResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "updateProductRelationshipResponse");
    private final static QName _DeleteProductRelationshipRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteProductRelationshipRequest");
    private final static QName _DeleteProductRelationshipResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "deleteProductRelationshipResponse");
    private final static QName _GetProductRelationshipsRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductRelationshipsRequest");
    private final static QName _GetProductRelationshipsResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getProductRelationshipsResponse");
    private final static QName _GetModelIdentifiersRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getModelIdentifiersRequest");
    private final static QName _GetModelIdentifiersResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getModelIdentifiersResponse");
    private final static QName _GetTransactionKeyIdentifiersRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getTransactionKeyIdentifiersRequest");
    private final static QName _GetTransactionKeyIdentifiersResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getTransactionKeyIdentifiersResponse");
    private final static QName _CreateOrganizationRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createOrganizationRequest");
    private final static QName _CreateOrganizationResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "createOrganizationResponse");
    private final static QName _GetLicenseTechnologyQueryRequest_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getLicenseTechnologyQueryRequest");
    private final static QName _GetLicenseTechnologyQueryResponse_QNAME = new QName("urn:com.macrovision:flexnet/operations", "getLicenseTechnologyQueryResponse");
    private final static QName _LicenseTechnologyQueryParametersTypeName_QNAME = new QName("urn:com.macrovision:flexnet/operations", "name");
    private final static QName _SuiteQueryParametersTypeSuiteName_QNAME = new QName("urn:com.macrovision:flexnet/operations", "suiteName");
    private final static QName _SuiteQueryParametersTypeVersion_QNAME = new QName("urn:com.macrovision:flexnet/operations", "version");
    private final static QName _SuiteQueryParametersTypeDescription_QNAME = new QName("urn:com.macrovision:flexnet/operations", "description");
    private final static QName _SuiteQueryParametersTypePartNumber_QNAME = new QName("urn:com.macrovision:flexnet/operations", "partNumber");
    private final static QName _SuiteQueryParametersTypeState_QNAME = new QName("urn:com.macrovision:flexnet/operations", "state");
    private final static QName _SuiteQueryParametersTypeCreationDate_QNAME = new QName("urn:com.macrovision:flexnet/operations", "creationDate");
    private final static QName _SuiteQueryParametersTypeLastModifiedDate_QNAME = new QName("urn:com.macrovision:flexnet/operations", "lastModifiedDate");
    private final static QName _SuiteQueryParametersTypeHostType_QNAME = new QName("urn:com.macrovision:flexnet/operations", "hostType");
    private final static QName _SuiteQueryParametersTypeUsedOnDevice_QNAME = new QName("urn:com.macrovision:flexnet/operations", "usedOnDevice");
    private final static QName _MaintenanceQueryParametersTypeMaintenanceName_QNAME = new QName("urn:com.macrovision:flexnet/operations", "maintenanceName");
    private final static QName _ProductQueryParametersTypeProductName_QNAME = new QName("urn:com.macrovision:flexnet/operations", "productName");
    private final static QName _FeatureQueryParametersTypeFeatureName_QNAME = new QName("urn:com.macrovision:flexnet/operations", "featureName");
    private final static QName _FeatureQueryParametersTypeVersionFormat_QNAME = new QName("urn:com.macrovision:flexnet/operations", "versionFormat");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.g
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateFeatureRequestType }
     * 
     */
    public CreateFeatureRequestType createCreateFeatureRequestType() {
        return new CreateFeatureRequestType();
    }

    /**
     * Create an instance of {@link CreateFeatureResponseType }
     * 
     */
    public CreateFeatureResponseType createCreateFeatureResponseType() {
        return new CreateFeatureResponseType();
    }

    /**
     * Create an instance of {@link UpdateFeatureRequestType }
     * 
     */
    public UpdateFeatureRequestType createUpdateFeatureRequestType() {
        return new UpdateFeatureRequestType();
    }

    /**
     * Create an instance of {@link UpdateFeatureResponseType }
     * 
     */
    public UpdateFeatureResponseType createUpdateFeatureResponseType() {
        return new UpdateFeatureResponseType();
    }

    /**
     * Create an instance of {@link DeleteFeatureRequestType }
     * 
     */
    public DeleteFeatureRequestType createDeleteFeatureRequestType() {
        return new DeleteFeatureRequestType();
    }

    /**
     * Create an instance of {@link DeleteFeatureResponseType }
     * 
     */
    public DeleteFeatureResponseType createDeleteFeatureResponseType() {
        return new DeleteFeatureResponseType();
    }

    /**
     * Create an instance of {@link GetFeatureCountRequestType }
     * 
     */
    public GetFeatureCountRequestType createGetFeatureCountRequestType() {
        return new GetFeatureCountRequestType();
    }

    /**
     * Create an instance of {@link GetFeatureCountResponseType }
     * 
     */
    public GetFeatureCountResponseType createGetFeatureCountResponseType() {
        return new GetFeatureCountResponseType();
    }

    /**
     * Create an instance of {@link GetFeaturesQueryRequestType }
     * 
     */
    public GetFeaturesQueryRequestType createGetFeaturesQueryRequestType() {
        return new GetFeaturesQueryRequestType();
    }

    /**
     * Create an instance of {@link GetFeaturesQueryResponseType }
     * 
     */
    public GetFeaturesQueryResponseType createGetFeaturesQueryResponseType() {
        return new GetFeaturesQueryResponseType();
    }

    /**
     * Create an instance of {@link SetFeatureStateRequestType }
     * 
     */
    public SetFeatureStateRequestType createSetFeatureStateRequestType() {
        return new SetFeatureStateRequestType();
    }

    /**
     * Create an instance of {@link SetFeatureStateResponseType }
     * 
     */
    public SetFeatureStateResponseType createSetFeatureStateResponseType() {
        return new SetFeatureStateResponseType();
    }

    /**
     * Create an instance of {@link CreateFeatureBundleRequestType }
     * 
     */
    public CreateFeatureBundleRequestType createCreateFeatureBundleRequestType() {
        return new CreateFeatureBundleRequestType();
    }

    /**
     * Create an instance of {@link CreateFeatureBundleResponseType }
     * 
     */
    public CreateFeatureBundleResponseType createCreateFeatureBundleResponseType() {
        return new CreateFeatureBundleResponseType();
    }

    /**
     * Create an instance of {@link UpdateFeatureBundleRequestType }
     * 
     */
    public UpdateFeatureBundleRequestType createUpdateFeatureBundleRequestType() {
        return new UpdateFeatureBundleRequestType();
    }

    /**
     * Create an instance of {@link UpdateFeatureBundleResponseType }
     * 
     */
    public UpdateFeatureBundleResponseType createUpdateFeatureBundleResponseType() {
        return new UpdateFeatureBundleResponseType();
    }

    /**
     * Create an instance of {@link DeleteFeatureBundleRequestType }
     * 
     */
    public DeleteFeatureBundleRequestType createDeleteFeatureBundleRequestType() {
        return new DeleteFeatureBundleRequestType();
    }

    /**
     * Create an instance of {@link DeleteFeatureBundleResponseType }
     * 
     */
    public DeleteFeatureBundleResponseType createDeleteFeatureBundleResponseType() {
        return new DeleteFeatureBundleResponseType();
    }

    /**
     * Create an instance of {@link GetFeatureBundleCountRequestType }
     * 
     */
    public GetFeatureBundleCountRequestType createGetFeatureBundleCountRequestType() {
        return new GetFeatureBundleCountRequestType();
    }

    /**
     * Create an instance of {@link GetFeatureBundleCountResponseType }
     * 
     */
    public GetFeatureBundleCountResponseType createGetFeatureBundleCountResponseType() {
        return new GetFeatureBundleCountResponseType();
    }

    /**
     * Create an instance of {@link GetFeatureBundlesQueryRequestType }
     * 
     */
    public GetFeatureBundlesQueryRequestType createGetFeatureBundlesQueryRequestType() {
        return new GetFeatureBundlesQueryRequestType();
    }

    /**
     * Create an instance of {@link GetFeatureBundlesQueryResponseType }
     * 
     */
    public GetFeatureBundlesQueryResponseType createGetFeatureBundlesQueryResponseType() {
        return new GetFeatureBundlesQueryResponseType();
    }

    /**
     * Create an instance of {@link SetFeatureBundleStateRequestType }
     * 
     */
    public SetFeatureBundleStateRequestType createSetFeatureBundleStateRequestType() {
        return new SetFeatureBundleStateRequestType();
    }

    /**
     * Create an instance of {@link SetFeatureBundleStateResponseType }
     * 
     */
    public SetFeatureBundleStateResponseType createSetFeatureBundleStateResponseType() {
        return new SetFeatureBundleStateResponseType();
    }

    /**
     * Create an instance of {@link CreateProductRequestType }
     * 
     */
    public CreateProductRequestType createCreateProductRequestType() {
        return new CreateProductRequestType();
    }

    /**
     * Create an instance of {@link CreateProductResponseType }
     * 
     */
    public CreateProductResponseType createCreateProductResponseType() {
        return new CreateProductResponseType();
    }

    /**
     * Create an instance of {@link UpdateProductRequestType }
     * 
     */
    public UpdateProductRequestType createUpdateProductRequestType() {
        return new UpdateProductRequestType();
    }

    /**
     * Create an instance of {@link UpdateProductResponseType }
     * 
     */
    public UpdateProductResponseType createUpdateProductResponseType() {
        return new UpdateProductResponseType();
    }

    /**
     * Create an instance of {@link DeleteProductRequestType }
     * 
     */
    public DeleteProductRequestType createDeleteProductRequestType() {
        return new DeleteProductRequestType();
    }

    /**
     * Create an instance of {@link DeleteProductResponseType }
     * 
     */
    public DeleteProductResponseType createDeleteProductResponseType() {
        return new DeleteProductResponseType();
    }

    /**
     * Create an instance of {@link GetProductCountRequestType }
     * 
     */
    public GetProductCountRequestType createGetProductCountRequestType() {
        return new GetProductCountRequestType();
    }

    /**
     * Create an instance of {@link GetProductCountResponseType }
     * 
     */
    public GetProductCountResponseType createGetProductCountResponseType() {
        return new GetProductCountResponseType();
    }

    /**
     * Create an instance of {@link GetProductsQueryRequestType }
     * 
     */
    public GetProductsQueryRequestType createGetProductsQueryRequestType() {
        return new GetProductsQueryRequestType();
    }

    /**
     * Create an instance of {@link GetProductsQueryResponseType }
     * 
     */
    public GetProductsQueryResponseType createGetProductsQueryResponseType() {
        return new GetProductsQueryResponseType();
    }

    /**
     * Create an instance of {@link CreateProductCategoryRequestType }
     * 
     */
    public CreateProductCategoryRequestType createCreateProductCategoryRequestType() {
        return new CreateProductCategoryRequestType();
    }

    /**
     * Create an instance of {@link CreateProductCategoryResponseType }
     * 
     */
    public CreateProductCategoryResponseType createCreateProductCategoryResponseType() {
        return new CreateProductCategoryResponseType();
    }

    /**
     * Create an instance of {@link GetProductCategoriesRequestType }
     * 
     */
    public GetProductCategoriesRequestType createGetProductCategoriesRequestType() {
        return new GetProductCategoriesRequestType();
    }

    /**
     * Create an instance of {@link GetProductCategoriesResponseType }
     * 
     */
    public GetProductCategoriesResponseType createGetProductCategoriesResponseType() {
        return new GetProductCategoriesResponseType();
    }

    /**
     * Create an instance of {@link HandleProductCategoriesToUserRequestType }
     * 
     */
    public HandleProductCategoriesToUserRequestType createHandleProductCategoriesToUserRequestType() {
        return new HandleProductCategoriesToUserRequestType();
    }

    /**
     * Create an instance of {@link HandleProductCategoriesToUserResponseType }
     * 
     */
    public HandleProductCategoriesToUserResponseType createHandleProductCategoriesToUserResponseType() {
        return new HandleProductCategoriesToUserResponseType();
    }

    /**
     * Create an instance of {@link GetUsersForProductCategoryRequestType }
     * 
     */
    public GetUsersForProductCategoryRequestType createGetUsersForProductCategoryRequestType() {
        return new GetUsersForProductCategoryRequestType();
    }

    /**
     * Create an instance of {@link GetUsersForProductCategoryResponseType }
     * 
     */
    public GetUsersForProductCategoryResponseType createGetUsersForProductCategoryResponseType() {
        return new GetUsersForProductCategoryResponseType();
    }

    /**
     * Create an instance of {@link HandleProductCategoryToOrgRequestType }
     * 
     */
    public HandleProductCategoryToOrgRequestType createHandleProductCategoryToOrgRequestType() {
        return new HandleProductCategoryToOrgRequestType();
    }

    /**
     * Create an instance of {@link HandleProductCategoryToOrgResponseType }
     * 
     */
    public HandleProductCategoryToOrgResponseType createHandleProductCategoryToOrgResponseType() {
        return new HandleProductCategoryToOrgResponseType();
    }

    /**
     * Create an instance of {@link IsEntitlementVisibleInTargetOrgRequestType }
     * 
     */
    public IsEntitlementVisibleInTargetOrgRequestType createIsEntitlementVisibleInTargetOrgRequestType() {
        return new IsEntitlementVisibleInTargetOrgRequestType();
    }

    /**
     * Create an instance of {@link IsEntitlementVisibleInTargetOrgResponseType }
     * 
     */
    public IsEntitlementVisibleInTargetOrgResponseType createIsEntitlementVisibleInTargetOrgResponseType() {
        return new IsEntitlementVisibleInTargetOrgResponseType();
    }

    /**
     * Create an instance of {@link IsFulfillmentVisibleInTargetOrgRequestType }
     * 
     */
    public IsFulfillmentVisibleInTargetOrgRequestType createIsFulfillmentVisibleInTargetOrgRequestType() {
        return new IsFulfillmentVisibleInTargetOrgRequestType();
    }

    /**
     * Create an instance of {@link IsFulfillmentVisibleInTargetOrgResponseType }
     * 
     */
    public IsFulfillmentVisibleInTargetOrgResponseType createIsFulfillmentVisibleInTargetOrgResponseType() {
        return new IsFulfillmentVisibleInTargetOrgResponseType();
    }

    /**
     * Create an instance of {@link SetProductStateRequestType }
     * 
     */
    public SetProductStateRequestType createSetProductStateRequestType() {
        return new SetProductStateRequestType();
    }

    /**
     * Create an instance of {@link SetProductStateResponseType }
     * 
     */
    public SetProductStateResponseType createSetProductStateResponseType() {
        return new SetProductStateResponseType();
    }

    /**
     * Create an instance of {@link CreateMaintenanceRequestType }
     * 
     */
    public CreateMaintenanceRequestType createCreateMaintenanceRequestType() {
        return new CreateMaintenanceRequestType();
    }

    /**
     * Create an instance of {@link CreateMaintenanceResponseType }
     * 
     */
    public CreateMaintenanceResponseType createCreateMaintenanceResponseType() {
        return new CreateMaintenanceResponseType();
    }

    /**
     * Create an instance of {@link UpdateMaintenanceRequestType }
     * 
     */
    public UpdateMaintenanceRequestType createUpdateMaintenanceRequestType() {
        return new UpdateMaintenanceRequestType();
    }

    /**
     * Create an instance of {@link UpdateMaintenanceResponseType }
     * 
     */
    public UpdateMaintenanceResponseType createUpdateMaintenanceResponseType() {
        return new UpdateMaintenanceResponseType();
    }

    /**
     * Create an instance of {@link DeleteMaintenanceRequestType }
     * 
     */
    public DeleteMaintenanceRequestType createDeleteMaintenanceRequestType() {
        return new DeleteMaintenanceRequestType();
    }

    /**
     * Create an instance of {@link DeleteMaintenanceResponseType }
     * 
     */
    public DeleteMaintenanceResponseType createDeleteMaintenanceResponseType() {
        return new DeleteMaintenanceResponseType();
    }

    /**
     * Create an instance of {@link GetMaintenanceCountRequestType }
     * 
     */
    public GetMaintenanceCountRequestType createGetMaintenanceCountRequestType() {
        return new GetMaintenanceCountRequestType();
    }

    /**
     * Create an instance of {@link GetMaintenanceCountResponseType }
     * 
     */
    public GetMaintenanceCountResponseType createGetMaintenanceCountResponseType() {
        return new GetMaintenanceCountResponseType();
    }

    /**
     * Create an instance of {@link GetMaintenanceQueryRequestType }
     * 
     */
    public GetMaintenanceQueryRequestType createGetMaintenanceQueryRequestType() {
        return new GetMaintenanceQueryRequestType();
    }

    /**
     * Create an instance of {@link GetMaintenanceQueryResponseType }
     * 
     */
    public GetMaintenanceQueryResponseType createGetMaintenanceQueryResponseType() {
        return new GetMaintenanceQueryResponseType();
    }

    /**
     * Create an instance of {@link SetMaintenanceStateRequestType }
     * 
     */
    public SetMaintenanceStateRequestType createSetMaintenanceStateRequestType() {
        return new SetMaintenanceStateRequestType();
    }

    /**
     * Create an instance of {@link SetMaintenanceStateResponseType }
     * 
     */
    public SetMaintenanceStateResponseType createSetMaintenanceStateResponseType() {
        return new SetMaintenanceStateResponseType();
    }

    /**
     * Create an instance of {@link CreateUniformSuiteRequestType }
     * 
     */
    public CreateUniformSuiteRequestType createCreateUniformSuiteRequestType() {
        return new CreateUniformSuiteRequestType();
    }

    /**
     * Create an instance of {@link CreateUniformSuiteResponseType }
     * 
     */
    public CreateUniformSuiteResponseType createCreateUniformSuiteResponseType() {
        return new CreateUniformSuiteResponseType();
    }

    /**
     * Create an instance of {@link UpdateUniformSuiteRequestType }
     * 
     */
    public UpdateUniformSuiteRequestType createUpdateUniformSuiteRequestType() {
        return new UpdateUniformSuiteRequestType();
    }

    /**
     * Create an instance of {@link UpdateUniformSuiteResponseType }
     * 
     */
    public UpdateUniformSuiteResponseType createUpdateUniformSuiteResponseType() {
        return new UpdateUniformSuiteResponseType();
    }

    /**
     * Create an instance of {@link DeleteUniformSuiteRequestType }
     * 
     */
    public DeleteUniformSuiteRequestType createDeleteUniformSuiteRequestType() {
        return new DeleteUniformSuiteRequestType();
    }

    /**
     * Create an instance of {@link DeleteUniformSuiteResponseType }
     * 
     */
    public DeleteUniformSuiteResponseType createDeleteUniformSuiteResponseType() {
        return new DeleteUniformSuiteResponseType();
    }

    /**
     * Create an instance of {@link GetUniformSuiteCountRequestType }
     * 
     */
    public GetUniformSuiteCountRequestType createGetUniformSuiteCountRequestType() {
        return new GetUniformSuiteCountRequestType();
    }

    /**
     * Create an instance of {@link GetUniformSuiteCountResponseType }
     * 
     */
    public GetUniformSuiteCountResponseType createGetUniformSuiteCountResponseType() {
        return new GetUniformSuiteCountResponseType();
    }

    /**
     * Create an instance of {@link GetUniformSuitesQueryRequestType }
     * 
     */
    public GetUniformSuitesQueryRequestType createGetUniformSuitesQueryRequestType() {
        return new GetUniformSuitesQueryRequestType();
    }

    /**
     * Create an instance of {@link GetUniformSuitesQueryResponseType }
     * 
     */
    public GetUniformSuitesQueryResponseType createGetUniformSuitesQueryResponseType() {
        return new GetUniformSuitesQueryResponseType();
    }

    /**
     * Create an instance of {@link SetUniformSuiteStateRequestType }
     * 
     */
    public SetUniformSuiteStateRequestType createSetUniformSuiteStateRequestType() {
        return new SetUniformSuiteStateRequestType();
    }

    /**
     * Create an instance of {@link SetUniformSuiteStateResponseType }
     * 
     */
    public SetUniformSuiteStateResponseType createSetUniformSuiteStateResponseType() {
        return new SetUniformSuiteStateResponseType();
    }

    /**
     * Create an instance of {@link CreatePartNumberRequestType }
     * 
     */
    public CreatePartNumberRequestType createCreatePartNumberRequestType() {
        return new CreatePartNumberRequestType();
    }

    /**
     * Create an instance of {@link CreatePartNumberResponseType }
     * 
     */
    public CreatePartNumberResponseType createCreatePartNumberResponseType() {
        return new CreatePartNumberResponseType();
    }

    /**
     * Create an instance of {@link DeletePartNumberRequestType }
     * 
     */
    public DeletePartNumberRequestType createDeletePartNumberRequestType() {
        return new DeletePartNumberRequestType();
    }

    /**
     * Create an instance of {@link DeletePartNumberResponseType }
     * 
     */
    public DeletePartNumberResponseType createDeletePartNumberResponseType() {
        return new DeletePartNumberResponseType();
    }

    /**
     * Create an instance of {@link GetPartNumberCountRequestType }
     * 
     */
    public GetPartNumberCountRequestType createGetPartNumberCountRequestType() {
        return new GetPartNumberCountRequestType();
    }

    /**
     * Create an instance of {@link GetPartNumberCountResponseType }
     * 
     */
    public GetPartNumberCountResponseType createGetPartNumberCountResponseType() {
        return new GetPartNumberCountResponseType();
    }

    /**
     * Create an instance of {@link GetPartNumbersQueryRequestType }
     * 
     */
    public GetPartNumbersQueryRequestType createGetPartNumbersQueryRequestType() {
        return new GetPartNumbersQueryRequestType();
    }

    /**
     * Create an instance of {@link GetPartNumbersQueryResponseType }
     * 
     */
    public GetPartNumbersQueryResponseType createGetPartNumbersQueryResponseType() {
        return new GetPartNumbersQueryResponseType();
    }

    /**
     * Create an instance of {@link CreateProductRelationshipRequestType }
     * 
     */
    public CreateProductRelationshipRequestType createCreateProductRelationshipRequestType() {
        return new CreateProductRelationshipRequestType();
    }

    /**
     * Create an instance of {@link CreateProductRelationshipResponseType }
     * 
     */
    public CreateProductRelationshipResponseType createCreateProductRelationshipResponseType() {
        return new CreateProductRelationshipResponseType();
    }

    /**
     * Create an instance of {@link UpdateProductRelationshipRequestType }
     * 
     */
    public UpdateProductRelationshipRequestType createUpdateProductRelationshipRequestType() {
        return new UpdateProductRelationshipRequestType();
    }

    /**
     * Create an instance of {@link UpdateProductRelationshipResponseType }
     * 
     */
    public UpdateProductRelationshipResponseType createUpdateProductRelationshipResponseType() {
        return new UpdateProductRelationshipResponseType();
    }

    /**
     * Create an instance of {@link DeleteProductRelationshipRequestType }
     * 
     */
    public DeleteProductRelationshipRequestType createDeleteProductRelationshipRequestType() {
        return new DeleteProductRelationshipRequestType();
    }

    /**
     * Create an instance of {@link DeleteProductRelationshipResponseType }
     * 
     */
    public DeleteProductRelationshipResponseType createDeleteProductRelationshipResponseType() {
        return new DeleteProductRelationshipResponseType();
    }

    /**
     * Create an instance of {@link GetProductRelationshipsRequestType }
     * 
     */
    public GetProductRelationshipsRequestType createGetProductRelationshipsRequestType() {
        return new GetProductRelationshipsRequestType();
    }

    /**
     * Create an instance of {@link GetProductRelationshipsResponseType }
     * 
     */
    public GetProductRelationshipsResponseType createGetProductRelationshipsResponseType() {
        return new GetProductRelationshipsResponseType();
    }

    /**
     * Create an instance of {@link GetModelIdentifiersRequestType }
     * 
     */
    public GetModelIdentifiersRequestType createGetModelIdentifiersRequestType() {
        return new GetModelIdentifiersRequestType();
    }

    /**
     * Create an instance of {@link GetModelIdentifiersResponseType }
     * 
     */
    public GetModelIdentifiersResponseType createGetModelIdentifiersResponseType() {
        return new GetModelIdentifiersResponseType();
    }

    /**
     * Create an instance of {@link GetTransactionKeyIdentifiersRequestType }
     * 
     */
    public GetTransactionKeyIdentifiersRequestType createGetTransactionKeyIdentifiersRequestType() {
        return new GetTransactionKeyIdentifiersRequestType();
    }

    /**
     * Create an instance of {@link GetTransactionKeyIdentifiersResponseType }
     * 
     */
    public GetTransactionKeyIdentifiersResponseType createGetTransactionKeyIdentifiersResponseType() {
        return new GetTransactionKeyIdentifiersResponseType();
    }

    /**
     * Create an instance of {@link CreateOrganizationRequestType }
     * 
     */
    public CreateOrganizationRequestType createCreateOrganizationRequestType() {
        return new CreateOrganizationRequestType();
    }

    /**
     * Create an instance of {@link CreateOrganizationResponseType }
     * 
     */
    public CreateOrganizationResponseType createCreateOrganizationResponseType() {
        return new CreateOrganizationResponseType();
    }

    /**
     * Create an instance of {@link GetLicenseTechnologyQueryRequestType }
     * 
     */
    public GetLicenseTechnologyQueryRequestType createGetLicenseTechnologyQueryRequestType() {
        return new GetLicenseTechnologyQueryRequestType();
    }

    /**
     * Create an instance of {@link GetLicenseTechnologyQueryResponseType }
     * 
     */
    public GetLicenseTechnologyQueryResponseType createGetLicenseTechnologyQueryResponseType() {
        return new GetLicenseTechnologyQueryResponseType();
    }

    /**
     * Create an instance of {@link GroupMaskDataType }
     * 
     */
    public GroupMaskDataType createGroupMaskDataType() {
        return new GroupMaskDataType();
    }

    /**
     * Create an instance of {@link DupGroupDataType }
     * 
     */
    public DupGroupDataType createDupGroupDataType() {
        return new DupGroupDataType();
    }

    /**
     * Create an instance of {@link FeatureOverrideParamsType }
     * 
     */
    public FeatureOverrideParamsType createFeatureOverrideParamsType() {
        return new FeatureOverrideParamsType();
    }

    /**
     * Create an instance of {@link FeatureDataType }
     * 
     */
    public FeatureDataType createFeatureDataType() {
        return new FeatureDataType();
    }

    /**
     * Create an instance of {@link StatusInfoType }
     * 
     */
    public StatusInfoType createStatusInfoType() {
        return new StatusInfoType();
    }

    /**
     * Create an instance of {@link FailedFeatureDataType }
     * 
     */
    public FailedFeatureDataType createFailedFeatureDataType() {
        return new FailedFeatureDataType();
    }

    /**
     * Create an instance of {@link FailedFeatureDataListType }
     * 
     */
    public FailedFeatureDataListType createFailedFeatureDataListType() {
        return new FailedFeatureDataListType();
    }

    /**
     * Create an instance of {@link CreatedFeatureDataType }
     * 
     */
    public CreatedFeatureDataType createCreatedFeatureDataType() {
        return new CreatedFeatureDataType();
    }

    /**
     * Create an instance of {@link CreatedFeatureDataListType }
     * 
     */
    public CreatedFeatureDataListType createCreatedFeatureDataListType() {
        return new CreatedFeatureDataListType();
    }

    /**
     * Create an instance of {@link FeaturePKType }
     * 
     */
    public FeaturePKType createFeaturePKType() {
        return new FeaturePKType();
    }

    /**
     * Create an instance of {@link FeatureIdentifierType }
     * 
     */
    public FeatureIdentifierType createFeatureIdentifierType() {
        return new FeatureIdentifierType();
    }

    /**
     * Create an instance of {@link UpdateFeatureDataType }
     * 
     */
    public UpdateFeatureDataType createUpdateFeatureDataType() {
        return new UpdateFeatureDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateFeatureDataType }
     * 
     */
    public FailedUpdateFeatureDataType createFailedUpdateFeatureDataType() {
        return new FailedUpdateFeatureDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateFeatureDataListType }
     * 
     */
    public FailedUpdateFeatureDataListType createFailedUpdateFeatureDataListType() {
        return new FailedUpdateFeatureDataListType();
    }

    /**
     * Create an instance of {@link DeleteFeatureDataType }
     * 
     */
    public DeleteFeatureDataType createDeleteFeatureDataType() {
        return new DeleteFeatureDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteFeatureDataType }
     * 
     */
    public FailedDeleteFeatureDataType createFailedDeleteFeatureDataType() {
        return new FailedDeleteFeatureDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteFeatureDataListType }
     * 
     */
    public FailedDeleteFeatureDataListType createFailedDeleteFeatureDataListType() {
        return new FailedDeleteFeatureDataListType();
    }

    /**
     * Create an instance of {@link SimpleQueryType }
     * 
     */
    public SimpleQueryType createSimpleQueryType() {
        return new SimpleQueryType();
    }

    /**
     * Create an instance of {@link VersionFormatQueryType }
     * 
     */
    public VersionFormatQueryType createVersionFormatQueryType() {
        return new VersionFormatQueryType();
    }

    /**
     * Create an instance of {@link StateQueryType }
     * 
     */
    public StateQueryType createStateQueryType() {
        return new StateQueryType();
    }

    /**
     * Create an instance of {@link DateQueryType }
     * 
     */
    public DateQueryType createDateQueryType() {
        return new DateQueryType();
    }

    /**
     * Create an instance of {@link FeatureQueryParametersType }
     * 
     */
    public FeatureQueryParametersType createFeatureQueryParametersType() {
        return new FeatureQueryParametersType();
    }

    /**
     * Create an instance of {@link GetCountResponseDataType }
     * 
     */
    public GetCountResponseDataType createGetCountResponseDataType() {
        return new GetCountResponseDataType();
    }

    /**
     * Create an instance of {@link FeatureQueryDataType }
     * 
     */
    public FeatureQueryDataType createFeatureQueryDataType() {
        return new FeatureQueryDataType();
    }

    /**
     * Create an instance of {@link GetFeaturesQueryResponseDataType }
     * 
     */
    public GetFeaturesQueryResponseDataType createGetFeaturesQueryResponseDataType() {
        return new GetFeaturesQueryResponseDataType();
    }

    /**
     * Create an instance of {@link FeatureStateDataType }
     * 
     */
    public FeatureStateDataType createFeatureStateDataType() {
        return new FeatureStateDataType();
    }

    /**
     * Create an instance of {@link FailedFeatureStateDataType }
     * 
     */
    public FailedFeatureStateDataType createFailedFeatureStateDataType() {
        return new FailedFeatureStateDataType();
    }

    /**
     * Create an instance of {@link FailedFeatureStateDataListType }
     * 
     */
    public FailedFeatureStateDataListType createFailedFeatureStateDataListType() {
        return new FailedFeatureStateDataListType();
    }

    /**
     * Create an instance of {@link FeatureIdentifierWithCountDataType }
     * 
     */
    public FeatureIdentifierWithCountDataType createFeatureIdentifierWithCountDataType() {
        return new FeatureIdentifierWithCountDataType();
    }

    /**
     * Create an instance of {@link FeaturesListType }
     * 
     */
    public FeaturesListType createFeaturesListType() {
        return new FeaturesListType();
    }

    /**
     * Create an instance of {@link CreateFeatureBundleDataType }
     * 
     */
    public CreateFeatureBundleDataType createCreateFeatureBundleDataType() {
        return new CreateFeatureBundleDataType();
    }

    /**
     * Create an instance of {@link FailedFeatureBundleDataType }
     * 
     */
    public FailedFeatureBundleDataType createFailedFeatureBundleDataType() {
        return new FailedFeatureBundleDataType();
    }

    /**
     * Create an instance of {@link FailedFeatureBundleDataListType }
     * 
     */
    public FailedFeatureBundleDataListType createFailedFeatureBundleDataListType() {
        return new FailedFeatureBundleDataListType();
    }

    /**
     * Create an instance of {@link CorrelationDataType }
     * 
     */
    public CorrelationDataType createCorrelationDataType() {
        return new CorrelationDataType();
    }

    /**
     * Create an instance of {@link CreatedFeatureBundleDataListType }
     * 
     */
    public CreatedFeatureBundleDataListType createCreatedFeatureBundleDataListType() {
        return new CreatedFeatureBundleDataListType();
    }

    /**
     * Create an instance of {@link FeatureBundlePKType }
     * 
     */
    public FeatureBundlePKType createFeatureBundlePKType() {
        return new FeatureBundlePKType();
    }

    /**
     * Create an instance of {@link FeatureBundleIdentifierType }
     * 
     */
    public FeatureBundleIdentifierType createFeatureBundleIdentifierType() {
        return new FeatureBundleIdentifierType();
    }

    /**
     * Create an instance of {@link UpdateFeaturesListType }
     * 
     */
    public UpdateFeaturesListType createUpdateFeaturesListType() {
        return new UpdateFeaturesListType();
    }

    /**
     * Create an instance of {@link UpdateFeatureBundleDataType }
     * 
     */
    public UpdateFeatureBundleDataType createUpdateFeatureBundleDataType() {
        return new UpdateFeatureBundleDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateFeatureBundleDataType }
     * 
     */
    public FailedUpdateFeatureBundleDataType createFailedUpdateFeatureBundleDataType() {
        return new FailedUpdateFeatureBundleDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateFeatureBundleDataListType }
     * 
     */
    public FailedUpdateFeatureBundleDataListType createFailedUpdateFeatureBundleDataListType() {
        return new FailedUpdateFeatureBundleDataListType();
    }

    /**
     * Create an instance of {@link DeleteFeatureBundleDataType }
     * 
     */
    public DeleteFeatureBundleDataType createDeleteFeatureBundleDataType() {
        return new DeleteFeatureBundleDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteFeatureBundleDataType }
     * 
     */
    public FailedDeleteFeatureBundleDataType createFailedDeleteFeatureBundleDataType() {
        return new FailedDeleteFeatureBundleDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteFeatureBundleDataListType }
     * 
     */
    public FailedDeleteFeatureBundleDataListType createFailedDeleteFeatureBundleDataListType() {
        return new FailedDeleteFeatureBundleDataListType();
    }

    /**
     * Create an instance of {@link FeatureBundleQueryParametersType }
     * 
     */
    public FeatureBundleQueryParametersType createFeatureBundleQueryParametersType() {
        return new FeatureBundleQueryParametersType();
    }

    /**
     * Create an instance of {@link FeatureBundleQueryDataType }
     * 
     */
    public FeatureBundleQueryDataType createFeatureBundleQueryDataType() {
        return new FeatureBundleQueryDataType();
    }

    /**
     * Create an instance of {@link GetFeatureBundlesQueryResponseDataType }
     * 
     */
    public GetFeatureBundlesQueryResponseDataType createGetFeatureBundlesQueryResponseDataType() {
        return new GetFeatureBundlesQueryResponseDataType();
    }

    /**
     * Create an instance of {@link FeatureBundleStateDataType }
     * 
     */
    public FeatureBundleStateDataType createFeatureBundleStateDataType() {
        return new FeatureBundleStateDataType();
    }

    /**
     * Create an instance of {@link FailedFeatureBundleStateDataType }
     * 
     */
    public FailedFeatureBundleStateDataType createFailedFeatureBundleStateDataType() {
        return new FailedFeatureBundleStateDataType();
    }

    /**
     * Create an instance of {@link FailedFeatureBundleStateDataListType }
     * 
     */
    public FailedFeatureBundleStateDataListType createFailedFeatureBundleStateDataListType() {
        return new FailedFeatureBundleStateDataListType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyPKType }
     * 
     */
    public LicenseTechnologyPKType createLicenseTechnologyPKType() {
        return new LicenseTechnologyPKType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyIdentifierType }
     * 
     */
    public LicenseTechnologyIdentifierType createLicenseTechnologyIdentifierType() {
        return new LicenseTechnologyIdentifierType();
    }

    /**
     * Create an instance of {@link LicenseGeneratorPKType }
     * 
     */
    public LicenseGeneratorPKType createLicenseGeneratorPKType() {
        return new LicenseGeneratorPKType();
    }

    /**
     * Create an instance of {@link LicenseGeneratorIdentifierType }
     * 
     */
    public LicenseGeneratorIdentifierType createLicenseGeneratorIdentifierType() {
        return new LicenseGeneratorIdentifierType();
    }

    /**
     * Create an instance of {@link PackagePropertiesDataType }
     * 
     */
    public PackagePropertiesDataType createPackagePropertiesDataType() {
        return new PackagePropertiesDataType();
    }

    /**
     * Create an instance of {@link FeatureBundleIdentifierWithCountDataType }
     * 
     */
    public FeatureBundleIdentifierWithCountDataType createFeatureBundleIdentifierWithCountDataType() {
        return new FeatureBundleIdentifierWithCountDataType();
    }

    /**
     * Create an instance of {@link FeatureBundlesListType }
     * 
     */
    public FeatureBundlesListType createFeatureBundlesListType() {
        return new FeatureBundlesListType();
    }

    /**
     * Create an instance of {@link LicenseModelPKType }
     * 
     */
    public LicenseModelPKType createLicenseModelPKType() {
        return new LicenseModelPKType();
    }

    /**
     * Create an instance of {@link LicenseModelIdentifierType }
     * 
     */
    public LicenseModelIdentifierType createLicenseModelIdentifierType() {
        return new LicenseModelIdentifierType();
    }

    /**
     * Create an instance of {@link LicenseModelsListType }
     * 
     */
    public LicenseModelsListType createLicenseModelsListType() {
        return new LicenseModelsListType();
    }

    /**
     * Create an instance of {@link TrustedKeyPKType }
     * 
     */
    public TrustedKeyPKType createTrustedKeyPKType() {
        return new TrustedKeyPKType();
    }

    /**
     * Create an instance of {@link TrustedKeyIdentifierType }
     * 
     */
    public TrustedKeyIdentifierType createTrustedKeyIdentifierType() {
        return new TrustedKeyIdentifierType();
    }

    /**
     * Create an instance of {@link PartNumberPKType }
     * 
     */
    public PartNumberPKType createPartNumberPKType() {
        return new PartNumberPKType();
    }

    /**
     * Create an instance of {@link PartNumberIdentifierWithModelType }
     * 
     */
    public PartNumberIdentifierWithModelType createPartNumberIdentifierWithModelType() {
        return new PartNumberIdentifierWithModelType();
    }

    /**
     * Create an instance of {@link PartNumbersListType }
     * 
     */
    public PartNumbersListType createPartNumbersListType() {
        return new PartNumbersListType();
    }

    /**
     * Create an instance of {@link HostTypePKType }
     * 
     */
    public HostTypePKType createHostTypePKType() {
        return new HostTypePKType();
    }

    /**
     * Create an instance of {@link HostTypeListType }
     * 
     */
    public HostTypeListType createHostTypeListType() {
        return new HostTypeListType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorType }
     * 
     */
    public AttributeDescriptorType createAttributeDescriptorType() {
        return new AttributeDescriptorType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorDataType }
     * 
     */
    public AttributeDescriptorDataType createAttributeDescriptorDataType() {
        return new AttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link CreateProductDataType }
     * 
     */
    public CreateProductDataType createCreateProductDataType() {
        return new CreateProductDataType();
    }

    /**
     * Create an instance of {@link FailedProductDataType }
     * 
     */
    public FailedProductDataType createFailedProductDataType() {
        return new FailedProductDataType();
    }

    /**
     * Create an instance of {@link FailedProductDataListType }
     * 
     */
    public FailedProductDataListType createFailedProductDataListType() {
        return new FailedProductDataListType();
    }

    /**
     * Create an instance of {@link CreatedProductDataType }
     * 
     */
    public CreatedProductDataType createCreatedProductDataType() {
        return new CreatedProductDataType();
    }

    /**
     * Create an instance of {@link CreatedProductDataListType }
     * 
     */
    public CreatedProductDataListType createCreatedProductDataListType() {
        return new CreatedProductDataListType();
    }

    /**
     * Create an instance of {@link ProductPKType }
     * 
     */
    public ProductPKType createProductPKType() {
        return new ProductPKType();
    }

    /**
     * Create an instance of {@link ProductIdentifierType }
     * 
     */
    public ProductIdentifierType createProductIdentifierType() {
        return new ProductIdentifierType();
    }

    /**
     * Create an instance of {@link UpdateFeatureBundlesListType }
     * 
     */
    public UpdateFeatureBundlesListType createUpdateFeatureBundlesListType() {
        return new UpdateFeatureBundlesListType();
    }

    /**
     * Create an instance of {@link UpdatePartNumbersListType }
     * 
     */
    public UpdatePartNumbersListType createUpdatePartNumbersListType() {
        return new UpdatePartNumbersListType();
    }

    /**
     * Create an instance of {@link UpdateLicenseModelsListType }
     * 
     */
    public UpdateLicenseModelsListType createUpdateLicenseModelsListType() {
        return new UpdateLicenseModelsListType();
    }

    /**
     * Create an instance of {@link UpdateHostTypeListType }
     * 
     */
    public UpdateHostTypeListType createUpdateHostTypeListType() {
        return new UpdateHostTypeListType();
    }

    /**
     * Create an instance of {@link UpdateProductDataType }
     * 
     */
    public UpdateProductDataType createUpdateProductDataType() {
        return new UpdateProductDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateProductDataType }
     * 
     */
    public FailedUpdateProductDataType createFailedUpdateProductDataType() {
        return new FailedUpdateProductDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateProductDataListType }
     * 
     */
    public FailedUpdateProductDataListType createFailedUpdateProductDataListType() {
        return new FailedUpdateProductDataListType();
    }

    /**
     * Create an instance of {@link DeleteProductDataType }
     * 
     */
    public DeleteProductDataType createDeleteProductDataType() {
        return new DeleteProductDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteProductDataType }
     * 
     */
    public FailedDeleteProductDataType createFailedDeleteProductDataType() {
        return new FailedDeleteProductDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteProductDataListType }
     * 
     */
    public FailedDeleteProductDataListType createFailedDeleteProductDataListType() {
        return new FailedDeleteProductDataListType();
    }

    /**
     * Create an instance of {@link ProdCustomAttributeQueryType }
     * 
     */
    public ProdCustomAttributeQueryType createProdCustomAttributeQueryType() {
        return new ProdCustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link ProdCustomAttributesQueryListType }
     * 
     */
    public ProdCustomAttributesQueryListType createProdCustomAttributesQueryListType() {
        return new ProdCustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link ProductQueryParametersType }
     * 
     */
    public ProductQueryParametersType createProductQueryParametersType() {
        return new ProductQueryParametersType();
    }

    /**
     * Create an instance of {@link ProductQueryDataType }
     * 
     */
    public ProductQueryDataType createProductQueryDataType() {
        return new ProductQueryDataType();
    }

    /**
     * Create an instance of {@link GetProductsQueryResponseDataType }
     * 
     */
    public GetProductsQueryResponseDataType createGetProductsQueryResponseDataType() {
        return new GetProductsQueryResponseDataType();
    }

    /**
     * Create an instance of {@link CreateProductCategoryDataType }
     * 
     */
    public CreateProductCategoryDataType createCreateProductCategoryDataType() {
        return new CreateProductCategoryDataType();
    }

    /**
     * Create an instance of {@link FailedProductCategoryDataType }
     * 
     */
    public FailedProductCategoryDataType createFailedProductCategoryDataType() {
        return new FailedProductCategoryDataType();
    }

    /**
     * Create an instance of {@link FailedProductCategoryDataListType }
     * 
     */
    public FailedProductCategoryDataListType createFailedProductCategoryDataListType() {
        return new FailedProductCategoryDataListType();
    }

    /**
     * Create an instance of {@link CreatedProductCategoryDataType }
     * 
     */
    public CreatedProductCategoryDataType createCreatedProductCategoryDataType() {
        return new CreatedProductCategoryDataType();
    }

    /**
     * Create an instance of {@link CreatedProductCategoryDataListType }
     * 
     */
    public CreatedProductCategoryDataListType createCreatedProductCategoryDataListType() {
        return new CreatedProductCategoryDataListType();
    }

    /**
     * Create an instance of {@link ValueType }
     * 
     */
    public ValueType createValueType() {
        return new ValueType();
    }

    /**
     * Create an instance of {@link CategoryAttributeDataType }
     * 
     */
    public CategoryAttributeDataType createCategoryAttributeDataType() {
        return new CategoryAttributeDataType();
    }

    /**
     * Create an instance of {@link CategoryAttributesDataType }
     * 
     */
    public CategoryAttributesDataType createCategoryAttributesDataType() {
        return new CategoryAttributesDataType();
    }

    /**
     * Create an instance of {@link ProductCategoryDataType }
     * 
     */
    public ProductCategoryDataType createProductCategoryDataType() {
        return new ProductCategoryDataType();
    }

    /**
     * Create an instance of {@link GetProductCategoriesResponseDataType }
     * 
     */
    public GetProductCategoriesResponseDataType createGetProductCategoriesResponseDataType() {
        return new GetProductCategoriesResponseDataType();
    }

    /**
     * Create an instance of {@link UserPKType }
     * 
     */
    public UserPKType createUserPKType() {
        return new UserPKType();
    }

    /**
     * Create an instance of {@link UserIdentifierType }
     * 
     */
    public UserIdentifierType createUserIdentifierType() {
        return new UserIdentifierType();
    }

    /**
     * Create an instance of {@link GetUsersForProductCategoryResponseDataType }
     * 
     */
    public GetUsersForProductCategoryResponseDataType createGetUsersForProductCategoryResponseDataType() {
        return new GetUsersForProductCategoryResponseDataType();
    }

    /**
     * Create an instance of {@link OrganizationPKType }
     * 
     */
    public OrganizationPKType createOrganizationPKType() {
        return new OrganizationPKType();
    }

    /**
     * Create an instance of {@link OrganizationIdentifierType }
     * 
     */
    public OrganizationIdentifierType createOrganizationIdentifierType() {
        return new OrganizationIdentifierType();
    }

    /**
     * Create an instance of {@link ProductStateDataType }
     * 
     */
    public ProductStateDataType createProductStateDataType() {
        return new ProductStateDataType();
    }

    /**
     * Create an instance of {@link FailedProductStateDataType }
     * 
     */
    public FailedProductStateDataType createFailedProductStateDataType() {
        return new FailedProductStateDataType();
    }

    /**
     * Create an instance of {@link FailedProductStateDataListType }
     * 
     */
    public FailedProductStateDataListType createFailedProductStateDataListType() {
        return new FailedProductStateDataListType();
    }

    /**
     * Create an instance of {@link PartNumberIdentifierType }
     * 
     */
    public PartNumberIdentifierType createPartNumberIdentifierType() {
        return new PartNumberIdentifierType();
    }

    /**
     * Create an instance of {@link PartNumbersSimpleListType }
     * 
     */
    public PartNumbersSimpleListType createPartNumbersSimpleListType() {
        return new PartNumbersSimpleListType();
    }

    /**
     * Create an instance of {@link CreateMaintenanceDataType }
     * 
     */
    public CreateMaintenanceDataType createCreateMaintenanceDataType() {
        return new CreateMaintenanceDataType();
    }

    /**
     * Create an instance of {@link FailedMaintenanceDataType }
     * 
     */
    public FailedMaintenanceDataType createFailedMaintenanceDataType() {
        return new FailedMaintenanceDataType();
    }

    /**
     * Create an instance of {@link FailedMaintenanceDataListType }
     * 
     */
    public FailedMaintenanceDataListType createFailedMaintenanceDataListType() {
        return new FailedMaintenanceDataListType();
    }

    /**
     * Create an instance of {@link CreatedMaintenaceDataType }
     * 
     */
    public CreatedMaintenaceDataType createCreatedMaintenaceDataType() {
        return new CreatedMaintenaceDataType();
    }

    /**
     * Create an instance of {@link CreatedMaintenanceDataListType }
     * 
     */
    public CreatedMaintenanceDataListType createCreatedMaintenanceDataListType() {
        return new CreatedMaintenanceDataListType();
    }

    /**
     * Create an instance of {@link MaintenancePKType }
     * 
     */
    public MaintenancePKType createMaintenancePKType() {
        return new MaintenancePKType();
    }

    /**
     * Create an instance of {@link MaintenanceIdentifierType }
     * 
     */
    public MaintenanceIdentifierType createMaintenanceIdentifierType() {
        return new MaintenanceIdentifierType();
    }

    /**
     * Create an instance of {@link UpdatePartNumbersSimpleListType }
     * 
     */
    public UpdatePartNumbersSimpleListType createUpdatePartNumbersSimpleListType() {
        return new UpdatePartNumbersSimpleListType();
    }

    /**
     * Create an instance of {@link UpdateMaintenanceDataType }
     * 
     */
    public UpdateMaintenanceDataType createUpdateMaintenanceDataType() {
        return new UpdateMaintenanceDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateMaintenanceDataType }
     * 
     */
    public FailedUpdateMaintenanceDataType createFailedUpdateMaintenanceDataType() {
        return new FailedUpdateMaintenanceDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateMaintenanceDataListType }
     * 
     */
    public FailedUpdateMaintenanceDataListType createFailedUpdateMaintenanceDataListType() {
        return new FailedUpdateMaintenanceDataListType();
    }

    /**
     * Create an instance of {@link DeleteMaintenanceDataType }
     * 
     */
    public DeleteMaintenanceDataType createDeleteMaintenanceDataType() {
        return new DeleteMaintenanceDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteMaintenanceDataType }
     * 
     */
    public FailedDeleteMaintenanceDataType createFailedDeleteMaintenanceDataType() {
        return new FailedDeleteMaintenanceDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteMaintenanceDataListType }
     * 
     */
    public FailedDeleteMaintenanceDataListType createFailedDeleteMaintenanceDataListType() {
        return new FailedDeleteMaintenanceDataListType();
    }

    /**
     * Create an instance of {@link MaintenanceQueryParametersType }
     * 
     */
    public MaintenanceQueryParametersType createMaintenanceQueryParametersType() {
        return new MaintenanceQueryParametersType();
    }

    /**
     * Create an instance of {@link MaintenanceQueryDataType }
     * 
     */
    public MaintenanceQueryDataType createMaintenanceQueryDataType() {
        return new MaintenanceQueryDataType();
    }

    /**
     * Create an instance of {@link GetMaintenanceQueryResponseDataType }
     * 
     */
    public GetMaintenanceQueryResponseDataType createGetMaintenanceQueryResponseDataType() {
        return new GetMaintenanceQueryResponseDataType();
    }

    /**
     * Create an instance of {@link MaintenanceStateDataType }
     * 
     */
    public MaintenanceStateDataType createMaintenanceStateDataType() {
        return new MaintenanceStateDataType();
    }

    /**
     * Create an instance of {@link FailedMaintenanceStateDataType }
     * 
     */
    public FailedMaintenanceStateDataType createFailedMaintenanceStateDataType() {
        return new FailedMaintenanceStateDataType();
    }

    /**
     * Create an instance of {@link FailedMaintenanceStateDataListType }
     * 
     */
    public FailedMaintenanceStateDataListType createFailedMaintenanceStateDataListType() {
        return new FailedMaintenanceStateDataListType();
    }

    /**
     * Create an instance of {@link ProductIdentifierWithCountDataType }
     * 
     */
    public ProductIdentifierWithCountDataType createProductIdentifierWithCountDataType() {
        return new ProductIdentifierWithCountDataType();
    }

    /**
     * Create an instance of {@link ProductsListType }
     * 
     */
    public ProductsListType createProductsListType() {
        return new ProductsListType();
    }

    /**
     * Create an instance of {@link CreateUniformSuiteDataType }
     * 
     */
    public CreateUniformSuiteDataType createCreateUniformSuiteDataType() {
        return new CreateUniformSuiteDataType();
    }

    /**
     * Create an instance of {@link FailedUniformSuiteDataType }
     * 
     */
    public FailedUniformSuiteDataType createFailedUniformSuiteDataType() {
        return new FailedUniformSuiteDataType();
    }

    /**
     * Create an instance of {@link FailedUniformSuiteDataListType }
     * 
     */
    public FailedUniformSuiteDataListType createFailedUniformSuiteDataListType() {
        return new FailedUniformSuiteDataListType();
    }

    /**
     * Create an instance of {@link CreatedUniformSuiteDataType }
     * 
     */
    public CreatedUniformSuiteDataType createCreatedUniformSuiteDataType() {
        return new CreatedUniformSuiteDataType();
    }

    /**
     * Create an instance of {@link CreatedUniformSuiteDataListType }
     * 
     */
    public CreatedUniformSuiteDataListType createCreatedUniformSuiteDataListType() {
        return new CreatedUniformSuiteDataListType();
    }

    /**
     * Create an instance of {@link SuitePKType }
     * 
     */
    public SuitePKType createSuitePKType() {
        return new SuitePKType();
    }

    /**
     * Create an instance of {@link SuiteIdentifierType }
     * 
     */
    public SuiteIdentifierType createSuiteIdentifierType() {
        return new SuiteIdentifierType();
    }

    /**
     * Create an instance of {@link UpdateProductsListType }
     * 
     */
    public UpdateProductsListType createUpdateProductsListType() {
        return new UpdateProductsListType();
    }

    /**
     * Create an instance of {@link UpdateUniformSuiteDataType }
     * 
     */
    public UpdateUniformSuiteDataType createUpdateUniformSuiteDataType() {
        return new UpdateUniformSuiteDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateUniformSuiteDataType }
     * 
     */
    public FailedUpdateUniformSuiteDataType createFailedUpdateUniformSuiteDataType() {
        return new FailedUpdateUniformSuiteDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateUniformSuiteDataListType }
     * 
     */
    public FailedUpdateUniformSuiteDataListType createFailedUpdateUniformSuiteDataListType() {
        return new FailedUpdateUniformSuiteDataListType();
    }

    /**
     * Create an instance of {@link DeleteUniformSuiteDataType }
     * 
     */
    public DeleteUniformSuiteDataType createDeleteUniformSuiteDataType() {
        return new DeleteUniformSuiteDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteUniformSuiteDataType }
     * 
     */
    public FailedDeleteUniformSuiteDataType createFailedDeleteUniformSuiteDataType() {
        return new FailedDeleteUniformSuiteDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteUniformSuiteDataListType }
     * 
     */
    public FailedDeleteUniformSuiteDataListType createFailedDeleteUniformSuiteDataListType() {
        return new FailedDeleteUniformSuiteDataListType();
    }

    /**
     * Create an instance of {@link SuiteCustomAttributeQueryType }
     * 
     */
    public SuiteCustomAttributeQueryType createSuiteCustomAttributeQueryType() {
        return new SuiteCustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link SuiteCustomAttributesQueryListType }
     * 
     */
    public SuiteCustomAttributesQueryListType createSuiteCustomAttributesQueryListType() {
        return new SuiteCustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link SuiteQueryParametersType }
     * 
     */
    public SuiteQueryParametersType createSuiteQueryParametersType() {
        return new SuiteQueryParametersType();
    }

    /**
     * Create an instance of {@link UniformSuiteQueryDataType }
     * 
     */
    public UniformSuiteQueryDataType createUniformSuiteQueryDataType() {
        return new UniformSuiteQueryDataType();
    }

    /**
     * Create an instance of {@link GetUniformSuitesQueryResponseDataType }
     * 
     */
    public GetUniformSuitesQueryResponseDataType createGetUniformSuitesQueryResponseDataType() {
        return new GetUniformSuitesQueryResponseDataType();
    }

    /**
     * Create an instance of {@link UniformSuiteStateDataType }
     * 
     */
    public UniformSuiteStateDataType createUniformSuiteStateDataType() {
        return new UniformSuiteStateDataType();
    }

    /**
     * Create an instance of {@link FailedUniformSuiteStateDataType }
     * 
     */
    public FailedUniformSuiteStateDataType createFailedUniformSuiteStateDataType() {
        return new FailedUniformSuiteStateDataType();
    }

    /**
     * Create an instance of {@link FailedUniformSuiteStateDataListType }
     * 
     */
    public FailedUniformSuiteStateDataListType createFailedUniformSuiteStateDataListType() {
        return new FailedUniformSuiteStateDataListType();
    }

    /**
     * Create an instance of {@link CreatePartNumberDataType }
     * 
     */
    public CreatePartNumberDataType createCreatePartNumberDataType() {
        return new CreatePartNumberDataType();
    }

    /**
     * Create an instance of {@link FailedPartNumberDataType }
     * 
     */
    public FailedPartNumberDataType createFailedPartNumberDataType() {
        return new FailedPartNumberDataType();
    }

    /**
     * Create an instance of {@link FailedPartNumberDataListType }
     * 
     */
    public FailedPartNumberDataListType createFailedPartNumberDataListType() {
        return new FailedPartNumberDataListType();
    }

    /**
     * Create an instance of {@link CreatedPartNumberDataType }
     * 
     */
    public CreatedPartNumberDataType createCreatedPartNumberDataType() {
        return new CreatedPartNumberDataType();
    }

    /**
     * Create an instance of {@link CreatedPartNumberDataListType }
     * 
     */
    public CreatedPartNumberDataListType createCreatedPartNumberDataListType() {
        return new CreatedPartNumberDataListType();
    }

    /**
     * Create an instance of {@link DeletePartNumberDataType }
     * 
     */
    public DeletePartNumberDataType createDeletePartNumberDataType() {
        return new DeletePartNumberDataType();
    }

    /**
     * Create an instance of {@link FailedDeletePartNumberDataType }
     * 
     */
    public FailedDeletePartNumberDataType createFailedDeletePartNumberDataType() {
        return new FailedDeletePartNumberDataType();
    }

    /**
     * Create an instance of {@link FailedDeletePartNumberDataListType }
     * 
     */
    public FailedDeletePartNumberDataListType createFailedDeletePartNumberDataListType() {
        return new FailedDeletePartNumberDataListType();
    }

    /**
     * Create an instance of {@link PartNumberQueryParametersType }
     * 
     */
    public PartNumberQueryParametersType createPartNumberQueryParametersType() {
        return new PartNumberQueryParametersType();
    }

    /**
     * Create an instance of {@link PartNumberCountDataType }
     * 
     */
    public PartNumberCountDataType createPartNumberCountDataType() {
        return new PartNumberCountDataType();
    }

    /**
     * Create an instance of {@link PartNumberDataType }
     * 
     */
    public PartNumberDataType createPartNumberDataType() {
        return new PartNumberDataType();
    }

    /**
     * Create an instance of {@link PartNumberDataListType }
     * 
     */
    public PartNumberDataListType createPartNumberDataListType() {
        return new PartNumberDataListType();
    }

    /**
     * Create an instance of {@link ProductRelationshipDataType }
     * 
     */
    public ProductRelationshipDataType createProductRelationshipDataType() {
        return new ProductRelationshipDataType();
    }

    /**
     * Create an instance of {@link UpdateProductRelationshipDataType }
     * 
     */
    public UpdateProductRelationshipDataType createUpdateProductRelationshipDataType() {
        return new UpdateProductRelationshipDataType();
    }

    /**
     * Create an instance of {@link FailedProductRelationshipDataType }
     * 
     */
    public FailedProductRelationshipDataType createFailedProductRelationshipDataType() {
        return new FailedProductRelationshipDataType();
    }

    /**
     * Create an instance of {@link FailedProductRelationshipDataListType }
     * 
     */
    public FailedProductRelationshipDataListType createFailedProductRelationshipDataListType() {
        return new FailedProductRelationshipDataListType();
    }

    /**
     * Create an instance of {@link FailedUpdateProductRelationshipDataType }
     * 
     */
    public FailedUpdateProductRelationshipDataType createFailedUpdateProductRelationshipDataType() {
        return new FailedUpdateProductRelationshipDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateProductRelationshipDataListType }
     * 
     */
    public FailedUpdateProductRelationshipDataListType createFailedUpdateProductRelationshipDataListType() {
        return new FailedUpdateProductRelationshipDataListType();
    }

    /**
     * Create an instance of {@link IdentifierQueryParametersType }
     * 
     */
    public IdentifierQueryParametersType createIdentifierQueryParametersType() {
        return new IdentifierQueryParametersType();
    }

    /**
     * Create an instance of {@link ModelAttributesType }
     * 
     */
    public ModelAttributesType createModelAttributesType() {
        return new ModelAttributesType();
    }

    /**
     * Create an instance of {@link LicenseModelDetailsType }
     * 
     */
    public LicenseModelDetailsType createLicenseModelDetailsType() {
        return new LicenseModelDetailsType();
    }

    /**
     * Create an instance of {@link ModelIdentifiersDataListType }
     * 
     */
    public ModelIdentifiersDataListType createModelIdentifiersDataListType() {
        return new ModelIdentifiersDataListType();
    }

    /**
     * Create an instance of {@link TransactionKeyIdentifiersDataListType }
     * 
     */
    public TransactionKeyIdentifiersDataListType createTransactionKeyIdentifiersDataListType() {
        return new TransactionKeyIdentifiersDataListType();
    }

    /**
     * Create an instance of {@link OrganizationBasicDataType }
     * 
     */
    public OrganizationBasicDataType createOrganizationBasicDataType() {
        return new OrganizationBasicDataType();
    }

    /**
     * Create an instance of {@link CreatedOrgDataType }
     * 
     */
    public CreatedOrgDataType createCreatedOrgDataType() {
        return new CreatedOrgDataType();
    }

    /**
     * Create an instance of {@link CreatedOrgDataListType }
     * 
     */
    public CreatedOrgDataListType createCreatedOrgDataListType() {
        return new CreatedOrgDataListType();
    }

    /**
     * Create an instance of {@link FailedOrgDataType }
     * 
     */
    public FailedOrgDataType createFailedOrgDataType() {
        return new FailedOrgDataType();
    }

    /**
     * Create an instance of {@link FailedOrgDataListType }
     * 
     */
    public FailedOrgDataListType createFailedOrgDataListType() {
        return new FailedOrgDataListType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyQueryParametersType }
     * 
     */
    public LicenseTechnologyQueryParametersType createLicenseTechnologyQueryParametersType() {
        return new LicenseTechnologyQueryParametersType();
    }

    /**
     * Create an instance of {@link LicenseGeneratorsDetailsType }
     * 
     */
    public LicenseGeneratorsDetailsType createLicenseGeneratorsDetailsType() {
        return new LicenseGeneratorsDetailsType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyDetailsType }
     * 
     */
    public LicenseTechnologyDetailsType createLicenseTechnologyDetailsType() {
        return new LicenseTechnologyDetailsType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyDataListType }
     * 
     */
    public LicenseTechnologyDataListType createLicenseTechnologyDataListType() {
        return new LicenseTechnologyDataListType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFeatureRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createFeatureRequest")
    public JAXBElement<CreateFeatureRequestType> createCreateFeatureRequest(CreateFeatureRequestType value) {
        return new JAXBElement<CreateFeatureRequestType>(_CreateFeatureRequest_QNAME, CreateFeatureRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFeatureResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createFeatureResponse")
    public JAXBElement<CreateFeatureResponseType> createCreateFeatureResponse(CreateFeatureResponseType value) {
        return new JAXBElement<CreateFeatureResponseType>(_CreateFeatureResponse_QNAME, CreateFeatureResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateFeatureRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateFeatureRequest")
    public JAXBElement<UpdateFeatureRequestType> createUpdateFeatureRequest(UpdateFeatureRequestType value) {
        return new JAXBElement<UpdateFeatureRequestType>(_UpdateFeatureRequest_QNAME, UpdateFeatureRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateFeatureResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateFeatureResponse")
    public JAXBElement<UpdateFeatureResponseType> createUpdateFeatureResponse(UpdateFeatureResponseType value) {
        return new JAXBElement<UpdateFeatureResponseType>(_UpdateFeatureResponse_QNAME, UpdateFeatureResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteFeatureRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteFeatureRequest")
    public JAXBElement<DeleteFeatureRequestType> createDeleteFeatureRequest(DeleteFeatureRequestType value) {
        return new JAXBElement<DeleteFeatureRequestType>(_DeleteFeatureRequest_QNAME, DeleteFeatureRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteFeatureResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteFeatureResponse")
    public JAXBElement<DeleteFeatureResponseType> createDeleteFeatureResponse(DeleteFeatureResponseType value) {
        return new JAXBElement<DeleteFeatureResponseType>(_DeleteFeatureResponse_QNAME, DeleteFeatureResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeatureCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeatureCountRequest")
    public JAXBElement<GetFeatureCountRequestType> createGetFeatureCountRequest(GetFeatureCountRequestType value) {
        return new JAXBElement<GetFeatureCountRequestType>(_GetFeatureCountRequest_QNAME, GetFeatureCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeatureCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeatureCountResponse")
    public JAXBElement<GetFeatureCountResponseType> createGetFeatureCountResponse(GetFeatureCountResponseType value) {
        return new JAXBElement<GetFeatureCountResponseType>(_GetFeatureCountResponse_QNAME, GetFeatureCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeaturesQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeaturesQueryRequest")
    public JAXBElement<GetFeaturesQueryRequestType> createGetFeaturesQueryRequest(GetFeaturesQueryRequestType value) {
        return new JAXBElement<GetFeaturesQueryRequestType>(_GetFeaturesQueryRequest_QNAME, GetFeaturesQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeaturesQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeaturesQueryResponse")
    public JAXBElement<GetFeaturesQueryResponseType> createGetFeaturesQueryResponse(GetFeaturesQueryResponseType value) {
        return new JAXBElement<GetFeaturesQueryResponseType>(_GetFeaturesQueryResponse_QNAME, GetFeaturesQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFeatureStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setFeatureStateRequest")
    public JAXBElement<SetFeatureStateRequestType> createSetFeatureStateRequest(SetFeatureStateRequestType value) {
        return new JAXBElement<SetFeatureStateRequestType>(_SetFeatureStateRequest_QNAME, SetFeatureStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFeatureStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setFeatureStateResponse")
    public JAXBElement<SetFeatureStateResponseType> createSetFeatureStateResponse(SetFeatureStateResponseType value) {
        return new JAXBElement<SetFeatureStateResponseType>(_SetFeatureStateResponse_QNAME, SetFeatureStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFeatureBundleRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createFeatureBundleRequest")
    public JAXBElement<CreateFeatureBundleRequestType> createCreateFeatureBundleRequest(CreateFeatureBundleRequestType value) {
        return new JAXBElement<CreateFeatureBundleRequestType>(_CreateFeatureBundleRequest_QNAME, CreateFeatureBundleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateFeatureBundleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createFeatureBundleResponse")
    public JAXBElement<CreateFeatureBundleResponseType> createCreateFeatureBundleResponse(CreateFeatureBundleResponseType value) {
        return new JAXBElement<CreateFeatureBundleResponseType>(_CreateFeatureBundleResponse_QNAME, CreateFeatureBundleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateFeatureBundleRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateFeatureBundleRequest")
    public JAXBElement<UpdateFeatureBundleRequestType> createUpdateFeatureBundleRequest(UpdateFeatureBundleRequestType value) {
        return new JAXBElement<UpdateFeatureBundleRequestType>(_UpdateFeatureBundleRequest_QNAME, UpdateFeatureBundleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateFeatureBundleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateFeatureBundleResponse")
    public JAXBElement<UpdateFeatureBundleResponseType> createUpdateFeatureBundleResponse(UpdateFeatureBundleResponseType value) {
        return new JAXBElement<UpdateFeatureBundleResponseType>(_UpdateFeatureBundleResponse_QNAME, UpdateFeatureBundleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteFeatureBundleRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteFeatureBundleRequest")
    public JAXBElement<DeleteFeatureBundleRequestType> createDeleteFeatureBundleRequest(DeleteFeatureBundleRequestType value) {
        return new JAXBElement<DeleteFeatureBundleRequestType>(_DeleteFeatureBundleRequest_QNAME, DeleteFeatureBundleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteFeatureBundleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteFeatureBundleResponse")
    public JAXBElement<DeleteFeatureBundleResponseType> createDeleteFeatureBundleResponse(DeleteFeatureBundleResponseType value) {
        return new JAXBElement<DeleteFeatureBundleResponseType>(_DeleteFeatureBundleResponse_QNAME, DeleteFeatureBundleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeatureBundleCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeatureBundleCountRequest")
    public JAXBElement<GetFeatureBundleCountRequestType> createGetFeatureBundleCountRequest(GetFeatureBundleCountRequestType value) {
        return new JAXBElement<GetFeatureBundleCountRequestType>(_GetFeatureBundleCountRequest_QNAME, GetFeatureBundleCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeatureBundleCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeatureBundleCountResponse")
    public JAXBElement<GetFeatureBundleCountResponseType> createGetFeatureBundleCountResponse(GetFeatureBundleCountResponseType value) {
        return new JAXBElement<GetFeatureBundleCountResponseType>(_GetFeatureBundleCountResponse_QNAME, GetFeatureBundleCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeatureBundlesQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeatureBundlesQueryRequest")
    public JAXBElement<GetFeatureBundlesQueryRequestType> createGetFeatureBundlesQueryRequest(GetFeatureBundlesQueryRequestType value) {
        return new JAXBElement<GetFeatureBundlesQueryRequestType>(_GetFeatureBundlesQueryRequest_QNAME, GetFeatureBundlesQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFeatureBundlesQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getFeatureBundlesQueryResponse")
    public JAXBElement<GetFeatureBundlesQueryResponseType> createGetFeatureBundlesQueryResponse(GetFeatureBundlesQueryResponseType value) {
        return new JAXBElement<GetFeatureBundlesQueryResponseType>(_GetFeatureBundlesQueryResponse_QNAME, GetFeatureBundlesQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFeatureBundleStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setFeatureBundleStateRequest")
    public JAXBElement<SetFeatureBundleStateRequestType> createSetFeatureBundleStateRequest(SetFeatureBundleStateRequestType value) {
        return new JAXBElement<SetFeatureBundleStateRequestType>(_SetFeatureBundleStateRequest_QNAME, SetFeatureBundleStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetFeatureBundleStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setFeatureBundleStateResponse")
    public JAXBElement<SetFeatureBundleStateResponseType> createSetFeatureBundleStateResponse(SetFeatureBundleStateResponseType value) {
        return new JAXBElement<SetFeatureBundleStateResponseType>(_SetFeatureBundleStateResponse_QNAME, SetFeatureBundleStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProductRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createProductRequest")
    public JAXBElement<CreateProductRequestType> createCreateProductRequest(CreateProductRequestType value) {
        return new JAXBElement<CreateProductRequestType>(_CreateProductRequest_QNAME, CreateProductRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProductResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createProductResponse")
    public JAXBElement<CreateProductResponseType> createCreateProductResponse(CreateProductResponseType value) {
        return new JAXBElement<CreateProductResponseType>(_CreateProductResponse_QNAME, CreateProductResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProductRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateProductRequest")
    public JAXBElement<UpdateProductRequestType> createUpdateProductRequest(UpdateProductRequestType value) {
        return new JAXBElement<UpdateProductRequestType>(_UpdateProductRequest_QNAME, UpdateProductRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProductResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateProductResponse")
    public JAXBElement<UpdateProductResponseType> createUpdateProductResponse(UpdateProductResponseType value) {
        return new JAXBElement<UpdateProductResponseType>(_UpdateProductResponse_QNAME, UpdateProductResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProductRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteProductRequest")
    public JAXBElement<DeleteProductRequestType> createDeleteProductRequest(DeleteProductRequestType value) {
        return new JAXBElement<DeleteProductRequestType>(_DeleteProductRequest_QNAME, DeleteProductRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProductResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteProductResponse")
    public JAXBElement<DeleteProductResponseType> createDeleteProductResponse(DeleteProductResponseType value) {
        return new JAXBElement<DeleteProductResponseType>(_DeleteProductResponse_QNAME, DeleteProductResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductCountRequest")
    public JAXBElement<GetProductCountRequestType> createGetProductCountRequest(GetProductCountRequestType value) {
        return new JAXBElement<GetProductCountRequestType>(_GetProductCountRequest_QNAME, GetProductCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductCountResponse")
    public JAXBElement<GetProductCountResponseType> createGetProductCountResponse(GetProductCountResponseType value) {
        return new JAXBElement<GetProductCountResponseType>(_GetProductCountResponse_QNAME, GetProductCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductsQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductsQueryRequest")
    public JAXBElement<GetProductsQueryRequestType> createGetProductsQueryRequest(GetProductsQueryRequestType value) {
        return new JAXBElement<GetProductsQueryRequestType>(_GetProductsQueryRequest_QNAME, GetProductsQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductsQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductsQueryResponse")
    public JAXBElement<GetProductsQueryResponseType> createGetProductsQueryResponse(GetProductsQueryResponseType value) {
        return new JAXBElement<GetProductsQueryResponseType>(_GetProductsQueryResponse_QNAME, GetProductsQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProductCategoryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createProductCategoryRequest")
    public JAXBElement<CreateProductCategoryRequestType> createCreateProductCategoryRequest(CreateProductCategoryRequestType value) {
        return new JAXBElement<CreateProductCategoryRequestType>(_CreateProductCategoryRequest_QNAME, CreateProductCategoryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProductCategoryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createProductCategoryResponse")
    public JAXBElement<CreateProductCategoryResponseType> createCreateProductCategoryResponse(CreateProductCategoryResponseType value) {
        return new JAXBElement<CreateProductCategoryResponseType>(_CreateProductCategoryResponse_QNAME, CreateProductCategoryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductCategoriesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductCategoriesRequest")
    public JAXBElement<GetProductCategoriesRequestType> createGetProductCategoriesRequest(GetProductCategoriesRequestType value) {
        return new JAXBElement<GetProductCategoriesRequestType>(_GetProductCategoriesRequest_QNAME, GetProductCategoriesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductCategoriesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductCategoriesResponse")
    public JAXBElement<GetProductCategoriesResponseType> createGetProductCategoriesResponse(GetProductCategoriesResponseType value) {
        return new JAXBElement<GetProductCategoriesResponseType>(_GetProductCategoriesResponse_QNAME, GetProductCategoriesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoriesToUserRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "assignProductCategoriesToUserRequest")
    public JAXBElement<HandleProductCategoriesToUserRequestType> createAssignProductCategoriesToUserRequest(HandleProductCategoriesToUserRequestType value) {
        return new JAXBElement<HandleProductCategoriesToUserRequestType>(_AssignProductCategoriesToUserRequest_QNAME, HandleProductCategoriesToUserRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoriesToUserResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "assignProductCategoriesToUserResponse")
    public JAXBElement<HandleProductCategoriesToUserResponseType> createAssignProductCategoriesToUserResponse(HandleProductCategoriesToUserResponseType value) {
        return new JAXBElement<HandleProductCategoriesToUserResponseType>(_AssignProductCategoriesToUserResponse_QNAME, HandleProductCategoriesToUserResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoriesToUserRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "removeProductCategoriesFromUserRequest")
    public JAXBElement<HandleProductCategoriesToUserRequestType> createRemoveProductCategoriesFromUserRequest(HandleProductCategoriesToUserRequestType value) {
        return new JAXBElement<HandleProductCategoriesToUserRequestType>(_RemoveProductCategoriesFromUserRequest_QNAME, HandleProductCategoriesToUserRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoriesToUserResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "removeProductCategoriesFromUserResponse")
    public JAXBElement<HandleProductCategoriesToUserResponseType> createRemoveProductCategoriesFromUserResponse(HandleProductCategoriesToUserResponseType value) {
        return new JAXBElement<HandleProductCategoriesToUserResponseType>(_RemoveProductCategoriesFromUserResponse_QNAME, HandleProductCategoriesToUserResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersForProductCategoryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getUsersForProductCategoryRequest")
    public JAXBElement<GetUsersForProductCategoryRequestType> createGetUsersForProductCategoryRequest(GetUsersForProductCategoryRequestType value) {
        return new JAXBElement<GetUsersForProductCategoryRequestType>(_GetUsersForProductCategoryRequest_QNAME, GetUsersForProductCategoryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersForProductCategoryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getUsersForProductCategoryResponse")
    public JAXBElement<GetUsersForProductCategoryResponseType> createGetUsersForProductCategoryResponse(GetUsersForProductCategoryResponseType value) {
        return new JAXBElement<GetUsersForProductCategoryResponseType>(_GetUsersForProductCategoryResponse_QNAME, GetUsersForProductCategoryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoryToOrgRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "assignProductCategoriesToOrgRequest")
    public JAXBElement<HandleProductCategoryToOrgRequestType> createAssignProductCategoriesToOrgRequest(HandleProductCategoryToOrgRequestType value) {
        return new JAXBElement<HandleProductCategoryToOrgRequestType>(_AssignProductCategoriesToOrgRequest_QNAME, HandleProductCategoryToOrgRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoryToOrgResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "assignProductCategoriesToOrgResponse")
    public JAXBElement<HandleProductCategoryToOrgResponseType> createAssignProductCategoriesToOrgResponse(HandleProductCategoryToOrgResponseType value) {
        return new JAXBElement<HandleProductCategoryToOrgResponseType>(_AssignProductCategoriesToOrgResponse_QNAME, HandleProductCategoryToOrgResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoryToOrgRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "removeProductCategoriesFromOrgRequest")
    public JAXBElement<HandleProductCategoryToOrgRequestType> createRemoveProductCategoriesFromOrgRequest(HandleProductCategoryToOrgRequestType value) {
        return new JAXBElement<HandleProductCategoryToOrgRequestType>(_RemoveProductCategoriesFromOrgRequest_QNAME, HandleProductCategoryToOrgRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link HandleProductCategoryToOrgResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "removeProductCategoriesFromOrgResponse")
    public JAXBElement<HandleProductCategoryToOrgResponseType> createRemoveProductCategoriesFromOrgResponse(HandleProductCategoryToOrgResponseType value) {
        return new JAXBElement<HandleProductCategoryToOrgResponseType>(_RemoveProductCategoriesFromOrgResponse_QNAME, HandleProductCategoryToOrgResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsEntitlementVisibleInTargetOrgRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "isEntitlementVisibleInTargetOrgRequest")
    public JAXBElement<IsEntitlementVisibleInTargetOrgRequestType> createIsEntitlementVisibleInTargetOrgRequest(IsEntitlementVisibleInTargetOrgRequestType value) {
        return new JAXBElement<IsEntitlementVisibleInTargetOrgRequestType>(_IsEntitlementVisibleInTargetOrgRequest_QNAME, IsEntitlementVisibleInTargetOrgRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsEntitlementVisibleInTargetOrgResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "isEntitlementVisibleInTargetOrgResponse")
    public JAXBElement<IsEntitlementVisibleInTargetOrgResponseType> createIsEntitlementVisibleInTargetOrgResponse(IsEntitlementVisibleInTargetOrgResponseType value) {
        return new JAXBElement<IsEntitlementVisibleInTargetOrgResponseType>(_IsEntitlementVisibleInTargetOrgResponse_QNAME, IsEntitlementVisibleInTargetOrgResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsFulfillmentVisibleInTargetOrgRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "isFulfillmentVisibleInTargetOrgRequest")
    public JAXBElement<IsFulfillmentVisibleInTargetOrgRequestType> createIsFulfillmentVisibleInTargetOrgRequest(IsFulfillmentVisibleInTargetOrgRequestType value) {
        return new JAXBElement<IsFulfillmentVisibleInTargetOrgRequestType>(_IsFulfillmentVisibleInTargetOrgRequest_QNAME, IsFulfillmentVisibleInTargetOrgRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsFulfillmentVisibleInTargetOrgResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "isFulfillmentVisibleInTargetOrgResponse")
    public JAXBElement<IsFulfillmentVisibleInTargetOrgResponseType> createIsFulfillmentVisibleInTargetOrgResponse(IsFulfillmentVisibleInTargetOrgResponseType value) {
        return new JAXBElement<IsFulfillmentVisibleInTargetOrgResponseType>(_IsFulfillmentVisibleInTargetOrgResponse_QNAME, IsFulfillmentVisibleInTargetOrgResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetProductStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setProductStateRequest")
    public JAXBElement<SetProductStateRequestType> createSetProductStateRequest(SetProductStateRequestType value) {
        return new JAXBElement<SetProductStateRequestType>(_SetProductStateRequest_QNAME, SetProductStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetProductStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setProductStateResponse")
    public JAXBElement<SetProductStateResponseType> createSetProductStateResponse(SetProductStateResponseType value) {
        return new JAXBElement<SetProductStateResponseType>(_SetProductStateResponse_QNAME, SetProductStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMaintenanceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createMaintenanceRequest")
    public JAXBElement<CreateMaintenanceRequestType> createCreateMaintenanceRequest(CreateMaintenanceRequestType value) {
        return new JAXBElement<CreateMaintenanceRequestType>(_CreateMaintenanceRequest_QNAME, CreateMaintenanceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateMaintenanceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createMaintenanceResponse")
    public JAXBElement<CreateMaintenanceResponseType> createCreateMaintenanceResponse(CreateMaintenanceResponseType value) {
        return new JAXBElement<CreateMaintenanceResponseType>(_CreateMaintenanceResponse_QNAME, CreateMaintenanceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMaintenanceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateMaintenanceRequest")
    public JAXBElement<UpdateMaintenanceRequestType> createUpdateMaintenanceRequest(UpdateMaintenanceRequestType value) {
        return new JAXBElement<UpdateMaintenanceRequestType>(_UpdateMaintenanceRequest_QNAME, UpdateMaintenanceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMaintenanceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateMaintenanceResponse")
    public JAXBElement<UpdateMaintenanceResponseType> createUpdateMaintenanceResponse(UpdateMaintenanceResponseType value) {
        return new JAXBElement<UpdateMaintenanceResponseType>(_UpdateMaintenanceResponse_QNAME, UpdateMaintenanceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteMaintenanceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteMaintenanceRequest")
    public JAXBElement<DeleteMaintenanceRequestType> createDeleteMaintenanceRequest(DeleteMaintenanceRequestType value) {
        return new JAXBElement<DeleteMaintenanceRequestType>(_DeleteMaintenanceRequest_QNAME, DeleteMaintenanceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteMaintenanceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteMaintenanceResponse")
    public JAXBElement<DeleteMaintenanceResponseType> createDeleteMaintenanceResponse(DeleteMaintenanceResponseType value) {
        return new JAXBElement<DeleteMaintenanceResponseType>(_DeleteMaintenanceResponse_QNAME, DeleteMaintenanceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMaintenanceCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getMaintenanceCountRequest")
    public JAXBElement<GetMaintenanceCountRequestType> createGetMaintenanceCountRequest(GetMaintenanceCountRequestType value) {
        return new JAXBElement<GetMaintenanceCountRequestType>(_GetMaintenanceCountRequest_QNAME, GetMaintenanceCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMaintenanceCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getMaintenanceCountResponse")
    public JAXBElement<GetMaintenanceCountResponseType> createGetMaintenanceCountResponse(GetMaintenanceCountResponseType value) {
        return new JAXBElement<GetMaintenanceCountResponseType>(_GetMaintenanceCountResponse_QNAME, GetMaintenanceCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMaintenanceQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getMaintenanceQueryRequest")
    public JAXBElement<GetMaintenanceQueryRequestType> createGetMaintenanceQueryRequest(GetMaintenanceQueryRequestType value) {
        return new JAXBElement<GetMaintenanceQueryRequestType>(_GetMaintenanceQueryRequest_QNAME, GetMaintenanceQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMaintenanceQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getMaintenanceQueryResponse")
    public JAXBElement<GetMaintenanceQueryResponseType> createGetMaintenanceQueryResponse(GetMaintenanceQueryResponseType value) {
        return new JAXBElement<GetMaintenanceQueryResponseType>(_GetMaintenanceQueryResponse_QNAME, GetMaintenanceQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetMaintenanceStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setMaintenanceStateRequest")
    public JAXBElement<SetMaintenanceStateRequestType> createSetMaintenanceStateRequest(SetMaintenanceStateRequestType value) {
        return new JAXBElement<SetMaintenanceStateRequestType>(_SetMaintenanceStateRequest_QNAME, SetMaintenanceStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetMaintenanceStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setMaintenanceStateResponse")
    public JAXBElement<SetMaintenanceStateResponseType> createSetMaintenanceStateResponse(SetMaintenanceStateResponseType value) {
        return new JAXBElement<SetMaintenanceStateResponseType>(_SetMaintenanceStateResponse_QNAME, SetMaintenanceStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUniformSuiteRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createUniformSuiteRequest")
    public JAXBElement<CreateUniformSuiteRequestType> createCreateUniformSuiteRequest(CreateUniformSuiteRequestType value) {
        return new JAXBElement<CreateUniformSuiteRequestType>(_CreateUniformSuiteRequest_QNAME, CreateUniformSuiteRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUniformSuiteResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createUniformSuiteResponse")
    public JAXBElement<CreateUniformSuiteResponseType> createCreateUniformSuiteResponse(CreateUniformSuiteResponseType value) {
        return new JAXBElement<CreateUniformSuiteResponseType>(_CreateUniformSuiteResponse_QNAME, CreateUniformSuiteResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUniformSuiteRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateUniformSuiteRequest")
    public JAXBElement<UpdateUniformSuiteRequestType> createUpdateUniformSuiteRequest(UpdateUniformSuiteRequestType value) {
        return new JAXBElement<UpdateUniformSuiteRequestType>(_UpdateUniformSuiteRequest_QNAME, UpdateUniformSuiteRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUniformSuiteResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateUniformSuiteResponse")
    public JAXBElement<UpdateUniformSuiteResponseType> createUpdateUniformSuiteResponse(UpdateUniformSuiteResponseType value) {
        return new JAXBElement<UpdateUniformSuiteResponseType>(_UpdateUniformSuiteResponse_QNAME, UpdateUniformSuiteResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUniformSuiteRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteUniformSuiteRequest")
    public JAXBElement<DeleteUniformSuiteRequestType> createDeleteUniformSuiteRequest(DeleteUniformSuiteRequestType value) {
        return new JAXBElement<DeleteUniformSuiteRequestType>(_DeleteUniformSuiteRequest_QNAME, DeleteUniformSuiteRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUniformSuiteResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteUniformSuiteResponse")
    public JAXBElement<DeleteUniformSuiteResponseType> createDeleteUniformSuiteResponse(DeleteUniformSuiteResponseType value) {
        return new JAXBElement<DeleteUniformSuiteResponseType>(_DeleteUniformSuiteResponse_QNAME, DeleteUniformSuiteResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUniformSuiteCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getUniformSuiteCountRequest")
    public JAXBElement<GetUniformSuiteCountRequestType> createGetUniformSuiteCountRequest(GetUniformSuiteCountRequestType value) {
        return new JAXBElement<GetUniformSuiteCountRequestType>(_GetUniformSuiteCountRequest_QNAME, GetUniformSuiteCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUniformSuiteCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getUniformSuiteCountResponse")
    public JAXBElement<GetUniformSuiteCountResponseType> createGetUniformSuiteCountResponse(GetUniformSuiteCountResponseType value) {
        return new JAXBElement<GetUniformSuiteCountResponseType>(_GetUniformSuiteCountResponse_QNAME, GetUniformSuiteCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUniformSuitesQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getUniformSuitesQueryRequest")
    public JAXBElement<GetUniformSuitesQueryRequestType> createGetUniformSuitesQueryRequest(GetUniformSuitesQueryRequestType value) {
        return new JAXBElement<GetUniformSuitesQueryRequestType>(_GetUniformSuitesQueryRequest_QNAME, GetUniformSuitesQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUniformSuitesQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getUniformSuitesQueryResponse")
    public JAXBElement<GetUniformSuitesQueryResponseType> createGetUniformSuitesQueryResponse(GetUniformSuitesQueryResponseType value) {
        return new JAXBElement<GetUniformSuitesQueryResponseType>(_GetUniformSuitesQueryResponse_QNAME, GetUniformSuitesQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUniformSuiteStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setUniformSuiteStateRequest")
    public JAXBElement<SetUniformSuiteStateRequestType> createSetUniformSuiteStateRequest(SetUniformSuiteStateRequestType value) {
        return new JAXBElement<SetUniformSuiteStateRequestType>(_SetUniformSuiteStateRequest_QNAME, SetUniformSuiteStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUniformSuiteStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "setUniformSuiteStateResponse")
    public JAXBElement<SetUniformSuiteStateResponseType> createSetUniformSuiteStateResponse(SetUniformSuiteStateResponseType value) {
        return new JAXBElement<SetUniformSuiteStateResponseType>(_SetUniformSuiteStateResponse_QNAME, SetUniformSuiteStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePartNumberRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createPartNumberRequest")
    public JAXBElement<CreatePartNumberRequestType> createCreatePartNumberRequest(CreatePartNumberRequestType value) {
        return new JAXBElement<CreatePartNumberRequestType>(_CreatePartNumberRequest_QNAME, CreatePartNumberRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePartNumberResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createPartNumberResponse")
    public JAXBElement<CreatePartNumberResponseType> createCreatePartNumberResponse(CreatePartNumberResponseType value) {
        return new JAXBElement<CreatePartNumberResponseType>(_CreatePartNumberResponse_QNAME, CreatePartNumberResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePartNumberRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deletePartNumberRequest")
    public JAXBElement<DeletePartNumberRequestType> createDeletePartNumberRequest(DeletePartNumberRequestType value) {
        return new JAXBElement<DeletePartNumberRequestType>(_DeletePartNumberRequest_QNAME, DeletePartNumberRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePartNumberResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deletePartNumberResponse")
    public JAXBElement<DeletePartNumberResponseType> createDeletePartNumberResponse(DeletePartNumberResponseType value) {
        return new JAXBElement<DeletePartNumberResponseType>(_DeletePartNumberResponse_QNAME, DeletePartNumberResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartNumberCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getPartNumberCountRequest")
    public JAXBElement<GetPartNumberCountRequestType> createGetPartNumberCountRequest(GetPartNumberCountRequestType value) {
        return new JAXBElement<GetPartNumberCountRequestType>(_GetPartNumberCountRequest_QNAME, GetPartNumberCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartNumberCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getPartNumberCountResponse")
    public JAXBElement<GetPartNumberCountResponseType> createGetPartNumberCountResponse(GetPartNumberCountResponseType value) {
        return new JAXBElement<GetPartNumberCountResponseType>(_GetPartNumberCountResponse_QNAME, GetPartNumberCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartNumbersQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getPartNumbersQueryRequest")
    public JAXBElement<GetPartNumbersQueryRequestType> createGetPartNumbersQueryRequest(GetPartNumbersQueryRequestType value) {
        return new JAXBElement<GetPartNumbersQueryRequestType>(_GetPartNumbersQueryRequest_QNAME, GetPartNumbersQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPartNumbersQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getPartNumbersQueryResponse")
    public JAXBElement<GetPartNumbersQueryResponseType> createGetPartNumbersQueryResponse(GetPartNumbersQueryResponseType value) {
        return new JAXBElement<GetPartNumbersQueryResponseType>(_GetPartNumbersQueryResponse_QNAME, GetPartNumbersQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProductRelationshipRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createProductRelationshipRequest")
    public JAXBElement<CreateProductRelationshipRequestType> createCreateProductRelationshipRequest(CreateProductRelationshipRequestType value) {
        return new JAXBElement<CreateProductRelationshipRequestType>(_CreateProductRelationshipRequest_QNAME, CreateProductRelationshipRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateProductRelationshipResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createProductRelationshipResponse")
    public JAXBElement<CreateProductRelationshipResponseType> createCreateProductRelationshipResponse(CreateProductRelationshipResponseType value) {
        return new JAXBElement<CreateProductRelationshipResponseType>(_CreateProductRelationshipResponse_QNAME, CreateProductRelationshipResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProductRelationshipRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateProductRelationshipRequest")
    public JAXBElement<UpdateProductRelationshipRequestType> createUpdateProductRelationshipRequest(UpdateProductRelationshipRequestType value) {
        return new JAXBElement<UpdateProductRelationshipRequestType>(_UpdateProductRelationshipRequest_QNAME, UpdateProductRelationshipRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateProductRelationshipResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "updateProductRelationshipResponse")
    public JAXBElement<UpdateProductRelationshipResponseType> createUpdateProductRelationshipResponse(UpdateProductRelationshipResponseType value) {
        return new JAXBElement<UpdateProductRelationshipResponseType>(_UpdateProductRelationshipResponse_QNAME, UpdateProductRelationshipResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProductRelationshipRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteProductRelationshipRequest")
    public JAXBElement<DeleteProductRelationshipRequestType> createDeleteProductRelationshipRequest(DeleteProductRelationshipRequestType value) {
        return new JAXBElement<DeleteProductRelationshipRequestType>(_DeleteProductRelationshipRequest_QNAME, DeleteProductRelationshipRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteProductRelationshipResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "deleteProductRelationshipResponse")
    public JAXBElement<DeleteProductRelationshipResponseType> createDeleteProductRelationshipResponse(DeleteProductRelationshipResponseType value) {
        return new JAXBElement<DeleteProductRelationshipResponseType>(_DeleteProductRelationshipResponse_QNAME, DeleteProductRelationshipResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductRelationshipsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductRelationshipsRequest")
    public JAXBElement<GetProductRelationshipsRequestType> createGetProductRelationshipsRequest(GetProductRelationshipsRequestType value) {
        return new JAXBElement<GetProductRelationshipsRequestType>(_GetProductRelationshipsRequest_QNAME, GetProductRelationshipsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetProductRelationshipsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getProductRelationshipsResponse")
    public JAXBElement<GetProductRelationshipsResponseType> createGetProductRelationshipsResponse(GetProductRelationshipsResponseType value) {
        return new JAXBElement<GetProductRelationshipsResponseType>(_GetProductRelationshipsResponse_QNAME, GetProductRelationshipsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetModelIdentifiersRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getModelIdentifiersRequest")
    public JAXBElement<GetModelIdentifiersRequestType> createGetModelIdentifiersRequest(GetModelIdentifiersRequestType value) {
        return new JAXBElement<GetModelIdentifiersRequestType>(_GetModelIdentifiersRequest_QNAME, GetModelIdentifiersRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetModelIdentifiersResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getModelIdentifiersResponse")
    public JAXBElement<GetModelIdentifiersResponseType> createGetModelIdentifiersResponse(GetModelIdentifiersResponseType value) {
        return new JAXBElement<GetModelIdentifiersResponseType>(_GetModelIdentifiersResponse_QNAME, GetModelIdentifiersResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransactionKeyIdentifiersRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getTransactionKeyIdentifiersRequest")
    public JAXBElement<GetTransactionKeyIdentifiersRequestType> createGetTransactionKeyIdentifiersRequest(GetTransactionKeyIdentifiersRequestType value) {
        return new JAXBElement<GetTransactionKeyIdentifiersRequestType>(_GetTransactionKeyIdentifiersRequest_QNAME, GetTransactionKeyIdentifiersRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetTransactionKeyIdentifiersResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getTransactionKeyIdentifiersResponse")
    public JAXBElement<GetTransactionKeyIdentifiersResponseType> createGetTransactionKeyIdentifiersResponse(GetTransactionKeyIdentifiersResponseType value) {
        return new JAXBElement<GetTransactionKeyIdentifiersResponseType>(_GetTransactionKeyIdentifiersResponse_QNAME, GetTransactionKeyIdentifiersResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrganizationRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createOrganizationRequest")
    public JAXBElement<CreateOrganizationRequestType> createCreateOrganizationRequest(CreateOrganizationRequestType value) {
        return new JAXBElement<CreateOrganizationRequestType>(_CreateOrganizationRequest_QNAME, CreateOrganizationRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateOrganizationResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "createOrganizationResponse")
    public JAXBElement<CreateOrganizationResponseType> createCreateOrganizationResponse(CreateOrganizationResponseType value) {
        return new JAXBElement<CreateOrganizationResponseType>(_CreateOrganizationResponse_QNAME, CreateOrganizationResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLicenseTechnologyQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getLicenseTechnologyQueryRequest")
    public JAXBElement<GetLicenseTechnologyQueryRequestType> createGetLicenseTechnologyQueryRequest(GetLicenseTechnologyQueryRequestType value) {
        return new JAXBElement<GetLicenseTechnologyQueryRequestType>(_GetLicenseTechnologyQueryRequest_QNAME, GetLicenseTechnologyQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLicenseTechnologyQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "getLicenseTechnologyQueryResponse")
    public JAXBElement<GetLicenseTechnologyQueryResponseType> createGetLicenseTechnologyQueryResponse(GetLicenseTechnologyQueryResponseType value) {
        return new JAXBElement<GetLicenseTechnologyQueryResponseType>(_GetLicenseTechnologyQueryResponse_QNAME, GetLicenseTechnologyQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "name", scope = LicenseTechnologyQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createLicenseTechnologyQueryParametersTypeName(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_LicenseTechnologyQueryParametersTypeName_QNAME, SimpleQueryType.class, LicenseTechnologyQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "name", scope = IdentifierQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createIdentifierQueryParametersTypeName(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_LicenseTechnologyQueryParametersTypeName_QNAME, SimpleQueryType.class, IdentifierQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "suiteName", scope = SuiteQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createSuiteQueryParametersTypeSuiteName(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeSuiteName_QNAME, SimpleQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "version", scope = SuiteQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createSuiteQueryParametersTypeVersion(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeVersion_QNAME, SimpleQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "description", scope = SuiteQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createSuiteQueryParametersTypeDescription(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeDescription_QNAME, SimpleQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "partNumber", scope = SuiteQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createSuiteQueryParametersTypePartNumber(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypePartNumber_QNAME, SimpleQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "state", scope = SuiteQueryParametersType.class)
    public JAXBElement<StateQueryType> createSuiteQueryParametersTypeState(StateQueryType value) {
        return new JAXBElement<StateQueryType>(_SuiteQueryParametersTypeState_QNAME, StateQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "creationDate", scope = SuiteQueryParametersType.class)
    public JAXBElement<DateQueryType> createSuiteQueryParametersTypeCreationDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeCreationDate_QNAME, DateQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "lastModifiedDate", scope = SuiteQueryParametersType.class)
    public JAXBElement<DateQueryType> createSuiteQueryParametersTypeLastModifiedDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeLastModifiedDate_QNAME, DateQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "hostType", scope = SuiteQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createSuiteQueryParametersTypeHostType(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeHostType_QNAME, SimpleQueryType.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "usedOnDevice", scope = SuiteQueryParametersType.class)
    public JAXBElement<Boolean> createSuiteQueryParametersTypeUsedOnDevice(Boolean value) {
        return new JAXBElement<Boolean>(_SuiteQueryParametersTypeUsedOnDevice_QNAME, Boolean.class, SuiteQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "maintenanceName", scope = MaintenanceQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createMaintenanceQueryParametersTypeMaintenanceName(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_MaintenanceQueryParametersTypeMaintenanceName_QNAME, SimpleQueryType.class, MaintenanceQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "version", scope = MaintenanceQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createMaintenanceQueryParametersTypeVersion(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeVersion_QNAME, SimpleQueryType.class, MaintenanceQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "description", scope = MaintenanceQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createMaintenanceQueryParametersTypeDescription(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeDescription_QNAME, SimpleQueryType.class, MaintenanceQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "partNumber", scope = MaintenanceQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createMaintenanceQueryParametersTypePartNumber(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypePartNumber_QNAME, SimpleQueryType.class, MaintenanceQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "state", scope = MaintenanceQueryParametersType.class)
    public JAXBElement<StateQueryType> createMaintenanceQueryParametersTypeState(StateQueryType value) {
        return new JAXBElement<StateQueryType>(_SuiteQueryParametersTypeState_QNAME, StateQueryType.class, MaintenanceQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "creationDate", scope = MaintenanceQueryParametersType.class)
    public JAXBElement<DateQueryType> createMaintenanceQueryParametersTypeCreationDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeCreationDate_QNAME, DateQueryType.class, MaintenanceQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "lastModifiedDate", scope = MaintenanceQueryParametersType.class)
    public JAXBElement<DateQueryType> createMaintenanceQueryParametersTypeLastModifiedDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeLastModifiedDate_QNAME, DateQueryType.class, MaintenanceQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "productName", scope = ProductQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createProductQueryParametersTypeProductName(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_ProductQueryParametersTypeProductName_QNAME, SimpleQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "version", scope = ProductQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createProductQueryParametersTypeVersion(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeVersion_QNAME, SimpleQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "description", scope = ProductQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createProductQueryParametersTypeDescription(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeDescription_QNAME, SimpleQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "partNumber", scope = ProductQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createProductQueryParametersTypePartNumber(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypePartNumber_QNAME, SimpleQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "state", scope = ProductQueryParametersType.class)
    public JAXBElement<StateQueryType> createProductQueryParametersTypeState(StateQueryType value) {
        return new JAXBElement<StateQueryType>(_SuiteQueryParametersTypeState_QNAME, StateQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "creationDate", scope = ProductQueryParametersType.class)
    public JAXBElement<DateQueryType> createProductQueryParametersTypeCreationDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeCreationDate_QNAME, DateQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "lastModifiedDate", scope = ProductQueryParametersType.class)
    public JAXBElement<DateQueryType> createProductQueryParametersTypeLastModifiedDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeLastModifiedDate_QNAME, DateQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "hostType", scope = ProductQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createProductQueryParametersTypeHostType(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeHostType_QNAME, SimpleQueryType.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "usedOnDevice", scope = ProductQueryParametersType.class)
    public JAXBElement<Boolean> createProductQueryParametersTypeUsedOnDevice(Boolean value) {
        return new JAXBElement<Boolean>(_SuiteQueryParametersTypeUsedOnDevice_QNAME, Boolean.class, ProductQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "name", scope = FeatureBundleQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createFeatureBundleQueryParametersTypeName(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_LicenseTechnologyQueryParametersTypeName_QNAME, SimpleQueryType.class, FeatureBundleQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "description", scope = FeatureBundleQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createFeatureBundleQueryParametersTypeDescription(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeDescription_QNAME, SimpleQueryType.class, FeatureBundleQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "state", scope = FeatureBundleQueryParametersType.class)
    public JAXBElement<StateQueryType> createFeatureBundleQueryParametersTypeState(StateQueryType value) {
        return new JAXBElement<StateQueryType>(_SuiteQueryParametersTypeState_QNAME, StateQueryType.class, FeatureBundleQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "creationDate", scope = FeatureBundleQueryParametersType.class)
    public JAXBElement<DateQueryType> createFeatureBundleQueryParametersTypeCreationDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeCreationDate_QNAME, DateQueryType.class, FeatureBundleQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "lastModifiedDate", scope = FeatureBundleQueryParametersType.class)
    public JAXBElement<DateQueryType> createFeatureBundleQueryParametersTypeLastModifiedDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeLastModifiedDate_QNAME, DateQueryType.class, FeatureBundleQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "featureName", scope = FeatureQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createFeatureQueryParametersTypeFeatureName(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_FeatureQueryParametersTypeFeatureName_QNAME, SimpleQueryType.class, FeatureQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "version", scope = FeatureQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createFeatureQueryParametersTypeVersion(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeVersion_QNAME, SimpleQueryType.class, FeatureQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionFormatQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "versionFormat", scope = FeatureQueryParametersType.class)
    public JAXBElement<VersionFormatQueryType> createFeatureQueryParametersTypeVersionFormat(VersionFormatQueryType value) {
        return new JAXBElement<VersionFormatQueryType>(_FeatureQueryParametersTypeVersionFormat_QNAME, VersionFormatQueryType.class, FeatureQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "description", scope = FeatureQueryParametersType.class)
    public JAXBElement<SimpleQueryType> createFeatureQueryParametersTypeDescription(SimpleQueryType value) {
        return new JAXBElement<SimpleQueryType>(_SuiteQueryParametersTypeDescription_QNAME, SimpleQueryType.class, FeatureQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "state", scope = FeatureQueryParametersType.class)
    public JAXBElement<StateQueryType> createFeatureQueryParametersTypeState(StateQueryType value) {
        return new JAXBElement<StateQueryType>(_SuiteQueryParametersTypeState_QNAME, StateQueryType.class, FeatureQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "creationDate", scope = FeatureQueryParametersType.class)
    public JAXBElement<DateQueryType> createFeatureQueryParametersTypeCreationDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeCreationDate_QNAME, DateQueryType.class, FeatureQueryParametersType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/operations", name = "lastModifiedDate", scope = FeatureQueryParametersType.class)
    public JAXBElement<DateQueryType> createFeatureQueryParametersTypeLastModifiedDate(DateQueryType value) {
        return new JAXBElement<DateQueryType>(_SuiteQueryParametersTypeLastModifiedDate_QNAME, DateQueryType.class, FeatureQueryParametersType.class, value);
    }

}
