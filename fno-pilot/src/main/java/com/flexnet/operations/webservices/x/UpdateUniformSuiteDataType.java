
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateUniformSuiteDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateUniformSuiteDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="suiteIdentifier" type="{urn:v2.webservices.operations.flexnet.com}suiteIdentifierType"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseGenerator" type="{urn:v2.webservices.operations.flexnet.com}licenseGeneratorIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="packageProperties" type="{urn:v2.webservices.operations.flexnet.com}packagePropertiesDataType" minOccurs="0"/&gt;
 *         &lt;element name="products" type="{urn:v2.webservices.operations.flexnet.com}updateProductsListType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModels" type="{urn:v2.webservices.operations.flexnet.com}updateLicenseModelsListType" minOccurs="0"/&gt;
 *         &lt;element name="trustedKey" type="{urn:v2.webservices.operations.flexnet.com}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="virtualTrustedKey" type="{urn:v2.webservices.operations.flexnet.com}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumbers" type="{urn:v2.webservices.operations.flexnet.com}updatePartNumbersListType" minOccurs="0"/&gt;
 *         &lt;element name="hostType" type="{urn:v2.webservices.operations.flexnet.com}hostTypePKType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypes" type="{urn:v2.webservices.operations.flexnet.com}updateHostTypeListType" minOccurs="0"/&gt;
 *         &lt;element name="usedOnDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v2.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInAdmin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productAttributes" type="{urn:v2.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateUniformSuiteDataType", propOrder = {
    "suiteIdentifier",
    "name",
    "version",
    "description",
    "licenseGenerator",
    "packageProperties",
    "products",
    "licenseModels",
    "trustedKey",
    "virtualTrustedKey",
    "partNumbers",
    "hostType",
    "hostTypes",
    "usedOnDevice",
    "customAttributes",
    "allowDownloadObsoleteFrInAdmin",
    "allowDownloadObsoleteFrInPortal",
    "productAttributes"
})
public class UpdateUniformSuiteDataType {

    @XmlElement(required = true)
    protected SuiteIdentifierType suiteIdentifier;
    protected String name;
    protected String version;
    protected String description;
    protected LicenseGeneratorIdentifierType licenseGenerator;
    protected PackagePropertiesDataType packageProperties;
    protected UpdateProductsListType products;
    protected UpdateLicenseModelsListType licenseModels;
    protected TrustedKeyIdentifierType trustedKey;
    protected TrustedKeyIdentifierType virtualTrustedKey;
    protected UpdatePartNumbersListType partNumbers;
    protected HostTypePKType hostType;
    protected UpdateHostTypeListType hostTypes;
    protected Boolean usedOnDevice;
    protected AttributeDescriptorDataType customAttributes;
    protected Boolean allowDownloadObsoleteFrInAdmin;
    protected Boolean allowDownloadObsoleteFrInPortal;
    protected AttributeDescriptorDataType productAttributes;

    /**
     * Gets the value of the suiteIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link SuiteIdentifierType }
     *     
     */
    public SuiteIdentifierType getSuiteIdentifier() {
        return suiteIdentifier;
    }

    /**
     * Sets the value of the suiteIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link SuiteIdentifierType }
     *     
     */
    public void setSuiteIdentifier(SuiteIdentifierType value) {
        this.suiteIdentifier = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the licenseGenerator property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public LicenseGeneratorIdentifierType getLicenseGenerator() {
        return licenseGenerator;
    }

    /**
     * Sets the value of the licenseGenerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public void setLicenseGenerator(LicenseGeneratorIdentifierType value) {
        this.licenseGenerator = value;
    }

    /**
     * Gets the value of the packageProperties property.
     * 
     * @return
     *     possible object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public PackagePropertiesDataType getPackageProperties() {
        return packageProperties;
    }

    /**
     * Sets the value of the packageProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public void setPackageProperties(PackagePropertiesDataType value) {
        this.packageProperties = value;
    }

    /**
     * Gets the value of the products property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateProductsListType }
     *     
     */
    public UpdateProductsListType getProducts() {
        return products;
    }

    /**
     * Sets the value of the products property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateProductsListType }
     *     
     */
    public void setProducts(UpdateProductsListType value) {
        this.products = value;
    }

    /**
     * Gets the value of the licenseModels property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateLicenseModelsListType }
     *     
     */
    public UpdateLicenseModelsListType getLicenseModels() {
        return licenseModels;
    }

    /**
     * Sets the value of the licenseModels property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateLicenseModelsListType }
     *     
     */
    public void setLicenseModels(UpdateLicenseModelsListType value) {
        this.licenseModels = value;
    }

    /**
     * Gets the value of the trustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getTrustedKey() {
        return trustedKey;
    }

    /**
     * Sets the value of the trustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setTrustedKey(TrustedKeyIdentifierType value) {
        this.trustedKey = value;
    }

    /**
     * Gets the value of the virtualTrustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getVirtualTrustedKey() {
        return virtualTrustedKey;
    }

    /**
     * Sets the value of the virtualTrustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setVirtualTrustedKey(TrustedKeyIdentifierType value) {
        this.virtualTrustedKey = value;
    }

    /**
     * Gets the value of the partNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link UpdatePartNumbersListType }
     *     
     */
    public UpdatePartNumbersListType getPartNumbers() {
        return partNumbers;
    }

    /**
     * Sets the value of the partNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdatePartNumbersListType }
     *     
     */
    public void setPartNumbers(UpdatePartNumbersListType value) {
        this.partNumbers = value;
    }

    /**
     * Gets the value of the hostType property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypePKType }
     *     
     */
    public HostTypePKType getHostType() {
        return hostType;
    }

    /**
     * Sets the value of the hostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypePKType }
     *     
     */
    public void setHostType(HostTypePKType value) {
        this.hostType = value;
    }

    /**
     * Gets the value of the hostTypes property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateHostTypeListType }
     *     
     */
    public UpdateHostTypeListType getHostTypes() {
        return hostTypes;
    }

    /**
     * Sets the value of the hostTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateHostTypeListType }
     *     
     */
    public void setHostTypes(UpdateHostTypeListType value) {
        this.hostTypes = value;
    }

    /**
     * Gets the value of the usedOnDevice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsedOnDevice() {
        return usedOnDevice;
    }

    /**
     * Sets the value of the usedOnDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsedOnDevice(Boolean value) {
        this.usedOnDevice = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInAdmin() {
        return allowDownloadObsoleteFrInAdmin;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInAdmin(Boolean value) {
        this.allowDownloadObsoleteFrInAdmin = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInPortal() {
        return allowDownloadObsoleteFrInPortal;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInPortal(Boolean value) {
        this.allowDownloadObsoleteFrInPortal = value;
    }

    /**
     * Gets the value of the productAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getProductAttributes() {
        return productAttributes;
    }

    /**
     * Sets the value of the productAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setProductAttributes(AttributeDescriptorDataType value) {
        this.productAttributes = value;
    }

}
