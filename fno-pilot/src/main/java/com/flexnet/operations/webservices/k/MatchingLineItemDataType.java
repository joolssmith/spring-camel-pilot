
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for matchingLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matchingLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="matchingLineItemIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementLineItemIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matchingLineItemDataType", propOrder = {
    "matchingLineItemIdentifier"
})
public class MatchingLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType matchingLineItemIdentifier;

    /**
     * Gets the value of the matchingLineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getMatchingLineItemIdentifier() {
        return matchingLineItemIdentifier;
    }

    /**
     * Sets the value of the matchingLineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setMatchingLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.matchingLineItemIdentifier = value;
    }

}
