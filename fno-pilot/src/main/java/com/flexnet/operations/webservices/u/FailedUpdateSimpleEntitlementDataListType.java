
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateSimpleEntitlementDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateSimpleEntitlementDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedSimpleEntitlement" type="{urn:v2.webservices.operations.flexnet.com}failedUpdateSimpleEntitlementDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateSimpleEntitlementDataListType", propOrder = {
    "failedSimpleEntitlement"
})
public class FailedUpdateSimpleEntitlementDataListType {

    protected List<FailedUpdateSimpleEntitlementDataType> failedSimpleEntitlement;

    /**
     * Gets the value of the failedSimpleEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedSimpleEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedSimpleEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUpdateSimpleEntitlementDataType }
     * 
     * 
     */
    public List<FailedUpdateSimpleEntitlementDataType> getFailedSimpleEntitlement() {
        if (failedSimpleEntitlement == null) {
            failedSimpleEntitlement = new ArrayList<FailedUpdateSimpleEntitlementDataType>();
        }
        return this.failedSimpleEntitlement;
    }

}
