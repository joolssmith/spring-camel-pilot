
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LineItemQuantityIncreaseNotAllowed complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LineItemQuantityIncreaseNotAllowed"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://producersuite.flexerasoftware.com/EntitlementService/}RightsIdProblem"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LineItemQuantityIncreaseNotAllowed")
public class LineItemQuantityIncreaseNotAllowed
    extends RightsIdProblem
{


}
