
package com.flexnet.operations.webservices.f;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateOrgDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateOrgDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedOrg" type="{urn:com.macrovision:flexnet/operations}failedUpdateOrgDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateOrgDataListType", propOrder = {
    "failedOrg"
})
public class FailedUpdateOrgDataListType {

    protected List<FailedUpdateOrgDataType> failedOrg;

    /**
     * Gets the value of the failedOrg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedOrg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedOrg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUpdateOrgDataType }
     * 
     * 
     */
    public List<FailedUpdateOrgDataType> getFailedOrg() {
        if (failedOrg == null) {
            failedOrg = new ArrayList<FailedUpdateOrgDataType>();
        }
        return this.failedOrg;
    }

}
