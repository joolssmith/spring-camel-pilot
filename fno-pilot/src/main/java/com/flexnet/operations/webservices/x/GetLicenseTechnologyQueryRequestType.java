
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getLicenseTechnologyQueryRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getLicenseTechnologyQueryRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v2.webservices.operations.flexnet.com}licenseTechnologyQueryParametersType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getLicenseTechnologyQueryRequestType", propOrder = {
    "queryParams"
})
public class GetLicenseTechnologyQueryRequestType {

    protected LicenseTechnologyQueryParametersType queryParams;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyQueryParametersType }
     *     
     */
    public LicenseTechnologyQueryParametersType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyQueryParametersType }
     *     
     */
    public void setQueryParams(LicenseTechnologyQueryParametersType value) {
        this.queryParams = value;
    }

}
