
package com.flexnet.operations.webservices.o;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for entitlementLineItemPropertiesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entitlementLineItemPropertiesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v1.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *         &lt;element name="activatableItemType" type="{urn:v1.webservices.operations.flexnet.com}ActivatableItemType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v1.webservices.operations.flexnet.com}organizationIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="soldToDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitlementState" type="{urn:v1.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *         &lt;element name="entitlementDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="allowPortalLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="parentBulkEntitlementId" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntSoldTo" type="{urn:v1.webservices.operations.flexnet.com}organizationIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntSoldToDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{urn:v1.webservices.operations.flexnet.com}productIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="productDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v1.webservices.operations.flexnet.com}partNumberIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumberDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v1.webservices.operations.flexnet.com}licenseTechnologyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModel" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel1" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel2" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="lineItemSupportAction" type="{urn:v1.webservices.operations.flexnet.com}LineItemType" minOccurs="0"/&gt;
 *         &lt;element name="parentLineItem" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="startDateOption" type="{urn:v1.webservices.operations.flexnet.com}StartDateOptionType" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="term" type="{urn:v1.webservices.operations.flexnet.com}DurationType" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDateAttributes" type="{urn:v1.webservices.operations.flexnet.com}versionDateAttributesType" minOccurs="0"/&gt;
 *         &lt;element name="numberOfCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="fulfilledAmount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="numberOfRemainingCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="isTrusted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="entitledProducts" type="{urn:v1.webservices.operations.flexnet.com}entitledProductDataListType" minOccurs="0"/&gt;
 *         &lt;element name="channelPartners" type="{urn:v1.webservices.operations.flexnet.com}channelPartnerDataListType" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItems" type="{urn:v1.webservices.operations.flexnet.com}maintenanceLineItemPropertiesType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="FNPTimeZoneValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="createdOnDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="lineItemAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entitlementLineItemPropertiesType", propOrder = {
    "activationId",
    "description",
    "state",
    "activatableItemType",
    "orderId",
    "orderLineNumber",
    "entitlementId",
    "soldTo",
    "soldToDisplayName",
    "entitlementState",
    "entitlementDescription",
    "allowPortalLogin",
    "shipToEmail",
    "shipToAddress",
    "parentBulkEntitlementId",
    "bulkEntSoldTo",
    "bulkEntSoldToDisplayName",
    "product",
    "productDescription",
    "partNumber",
    "partNumberDescription",
    "licenseTechnology",
    "licenseModel",
    "alternateLicenseModel1",
    "alternateLicenseModel2",
    "lineItemSupportAction",
    "parentLineItem",
    "startDate",
    "startDateOption",
    "isPermanent",
    "term",
    "expirationDate",
    "versionDate",
    "versionDateAttributes",
    "numberOfCopies",
    "fulfilledAmount",
    "numberOfRemainingCopies",
    "isTrusted",
    "customAttributes",
    "entitledProducts",
    "channelPartners",
    "maintenanceLineItems",
    "fnpTimeZoneValue",
    "createdOnDateTime",
    "lastModifiedDateTime",
    "lineItemAttributes"
})
public class EntitlementLineItemPropertiesType {

    protected EntitlementLineItemIdentifierType activationId;
    protected String description;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    @XmlSchemaType(name = "NMTOKEN")
    protected ActivatableItemType activatableItemType;
    protected String orderId;
    protected String orderLineNumber;
    protected EntitlementIdentifierType entitlementId;
    protected OrganizationIdentifierType soldTo;
    protected String soldToDisplayName;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType entitlementState;
    protected String entitlementDescription;
    protected Boolean allowPortalLogin;
    protected String shipToEmail;
    protected String shipToAddress;
    protected EntitlementIdentifierType parentBulkEntitlementId;
    protected OrganizationIdentifierType bulkEntSoldTo;
    protected String bulkEntSoldToDisplayName;
    protected ProductIdentifierType product;
    protected String productDescription;
    protected PartNumberIdentifierType partNumber;
    protected String partNumberDescription;
    protected LicenseTechnologyIdentifierType licenseTechnology;
    protected LicenseModelIdentifierType licenseModel;
    protected LicenseModelIdentifierType alternateLicenseModel1;
    protected LicenseModelIdentifierType alternateLicenseModel2;
    @XmlSchemaType(name = "NMTOKEN")
    protected LineItemType lineItemSupportAction;
    protected EntitlementLineItemIdentifierType parentLineItem;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "NMTOKEN")
    protected StartDateOptionType startDateOption;
    protected Boolean isPermanent;
    protected DurationType term;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar versionDate;
    protected VersionDateAttributesType versionDateAttributes;
    protected BigInteger numberOfCopies;
    protected BigInteger fulfilledAmount;
    protected BigInteger numberOfRemainingCopies;
    protected Boolean isTrusted;
    protected AttributeDescriptorDataType customAttributes;
    protected EntitledProductDataListType entitledProducts;
    protected ChannelPartnerDataListType channelPartners;
    protected List<MaintenanceLineItemPropertiesType> maintenanceLineItems;
    @XmlElement(name = "FNPTimeZoneValue")
    protected String fnpTimeZoneValue;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createdOnDateTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    protected AttributeDescriptorDataType lineItemAttributes;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setActivationId(EntitlementLineItemIdentifierType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the activatableItemType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivatableItemType }
     *     
     */
    public ActivatableItemType getActivatableItemType() {
        return activatableItemType;
    }

    /**
     * Sets the value of the activatableItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivatableItemType }
     *     
     */
    public void setActivatableItemType(ActivatableItemType value) {
        this.activatableItemType = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineNumber(String value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementId(EntitlementIdentifierType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setSoldTo(OrganizationIdentifierType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the soldToDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldToDisplayName() {
        return soldToDisplayName;
    }

    /**
     * Sets the value of the soldToDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldToDisplayName(String value) {
        this.soldToDisplayName = value;
    }

    /**
     * Gets the value of the entitlementState property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getEntitlementState() {
        return entitlementState;
    }

    /**
     * Sets the value of the entitlementState property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setEntitlementState(StateType value) {
        this.entitlementState = value;
    }

    /**
     * Gets the value of the entitlementDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementDescription() {
        return entitlementDescription;
    }

    /**
     * Sets the value of the entitlementDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementDescription(String value) {
        this.entitlementDescription = value;
    }

    /**
     * Gets the value of the allowPortalLogin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPortalLogin() {
        return allowPortalLogin;
    }

    /**
     * Sets the value of the allowPortalLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPortalLogin(Boolean value) {
        this.allowPortalLogin = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToEmail(String value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddress(String value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the parentBulkEntitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getParentBulkEntitlementId() {
        return parentBulkEntitlementId;
    }

    /**
     * Sets the value of the parentBulkEntitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setParentBulkEntitlementId(EntitlementIdentifierType value) {
        this.parentBulkEntitlementId = value;
    }

    /**
     * Gets the value of the bulkEntSoldTo property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getBulkEntSoldTo() {
        return bulkEntSoldTo;
    }

    /**
     * Sets the value of the bulkEntSoldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setBulkEntSoldTo(OrganizationIdentifierType value) {
        this.bulkEntSoldTo = value;
    }

    /**
     * Gets the value of the bulkEntSoldToDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBulkEntSoldToDisplayName() {
        return bulkEntSoldToDisplayName;
    }

    /**
     * Sets the value of the bulkEntSoldToDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBulkEntSoldToDisplayName(String value) {
        this.bulkEntSoldToDisplayName = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setProduct(ProductIdentifierType value) {
        this.product = value;
    }

    /**
     * Gets the value of the productDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * Sets the value of the productDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductDescription(String value) {
        this.productDescription = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setPartNumber(PartNumberIdentifierType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the partNumberDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartNumberDescription() {
        return partNumberDescription;
    }

    /**
     * Sets the value of the partNumberDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartNumberDescription(String value) {
        this.partNumberDescription = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the licenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModel() {
        return licenseModel;
    }

    /**
     * Sets the value of the licenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModel(LicenseModelIdentifierType value) {
        this.licenseModel = value;
    }

    /**
     * Gets the value of the alternateLicenseModel1 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel1() {
        return alternateLicenseModel1;
    }

    /**
     * Sets the value of the alternateLicenseModel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel1(LicenseModelIdentifierType value) {
        this.alternateLicenseModel1 = value;
    }

    /**
     * Gets the value of the alternateLicenseModel2 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel2() {
        return alternateLicenseModel2;
    }

    /**
     * Sets the value of the alternateLicenseModel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel2(LicenseModelIdentifierType value) {
        this.alternateLicenseModel2 = value;
    }

    /**
     * Gets the value of the lineItemSupportAction property.
     * 
     * @return
     *     possible object is
     *     {@link LineItemType }
     *     
     */
    public LineItemType getLineItemSupportAction() {
        return lineItemSupportAction;
    }

    /**
     * Sets the value of the lineItemSupportAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItemType }
     *     
     */
    public void setLineItemSupportAction(LineItemType value) {
        this.lineItemSupportAction = value;
    }

    /**
     * Gets the value of the parentLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getParentLineItem() {
        return parentLineItem;
    }

    /**
     * Sets the value of the parentLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setParentLineItem(EntitlementLineItemIdentifierType value) {
        this.parentLineItem = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the startDateOption property.
     * 
     * @return
     *     possible object is
     *     {@link StartDateOptionType }
     *     
     */
    public StartDateOptionType getStartDateOption() {
        return startDateOption;
    }

    /**
     * Sets the value of the startDateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link StartDateOptionType }
     *     
     */
    public void setStartDateOption(StartDateOptionType value) {
        this.startDateOption = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link DurationType }
     *     
     */
    public DurationType getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationType }
     *     
     */
    public void setTerm(DurationType value) {
        this.term = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersionDate(XMLGregorianCalendar value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the versionDateAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public VersionDateAttributesType getVersionDateAttributes() {
        return versionDateAttributes;
    }

    /**
     * Sets the value of the versionDateAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public void setVersionDateAttributes(VersionDateAttributesType value) {
        this.versionDateAttributes = value;
    }

    /**
     * Gets the value of the numberOfCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * Sets the value of the numberOfCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCopies(BigInteger value) {
        this.numberOfCopies = value;
    }

    /**
     * Gets the value of the fulfilledAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFulfilledAmount() {
        return fulfilledAmount;
    }

    /**
     * Sets the value of the fulfilledAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFulfilledAmount(BigInteger value) {
        this.fulfilledAmount = value;
    }

    /**
     * Gets the value of the numberOfRemainingCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfRemainingCopies() {
        return numberOfRemainingCopies;
    }

    /**
     * Sets the value of the numberOfRemainingCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfRemainingCopies(BigInteger value) {
        this.numberOfRemainingCopies = value;
    }

    /**
     * Gets the value of the isTrusted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTrusted() {
        return isTrusted;
    }

    /**
     * Sets the value of the isTrusted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTrusted(Boolean value) {
        this.isTrusted = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the entitledProducts property.
     * 
     * @return
     *     possible object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public EntitledProductDataListType getEntitledProducts() {
        return entitledProducts;
    }

    /**
     * Sets the value of the entitledProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public void setEntitledProducts(EntitledProductDataListType value) {
        this.entitledProducts = value;
    }

    /**
     * Gets the value of the channelPartners property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public ChannelPartnerDataListType getChannelPartners() {
        return channelPartners;
    }

    /**
     * Sets the value of the channelPartners property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public void setChannelPartners(ChannelPartnerDataListType value) {
        this.channelPartners = value;
    }

    /**
     * Gets the value of the maintenanceLineItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maintenanceLineItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaintenanceLineItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MaintenanceLineItemPropertiesType }
     * 
     * 
     */
    public List<MaintenanceLineItemPropertiesType> getMaintenanceLineItems() {
        if (maintenanceLineItems == null) {
            maintenanceLineItems = new ArrayList<MaintenanceLineItemPropertiesType>();
        }
        return this.maintenanceLineItems;
    }

    /**
     * Gets the value of the fnpTimeZoneValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNPTimeZoneValue() {
        return fnpTimeZoneValue;
    }

    /**
     * Sets the value of the fnpTimeZoneValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNPTimeZoneValue(String value) {
        this.fnpTimeZoneValue = value;
    }

    /**
     * Gets the value of the createdOnDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreatedOnDateTime() {
        return createdOnDateTime;
    }

    /**
     * Sets the value of the createdOnDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreatedOnDateTime(XMLGregorianCalendar value) {
        this.createdOnDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the lineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLineItemAttributes() {
        return lineItemAttributes;
    }

    /**
     * Sets the value of the lineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLineItemAttributes(AttributeDescriptorDataType value) {
        this.lineItemAttributes = value;
    }

}
