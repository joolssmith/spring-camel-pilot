
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getHostAttributesDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getHostAttributesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hostAttributes" type="{urn:com.macrovision:flexnet/operations}attributeMetaDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getHostAttributesDataType", propOrder = {
    "hostAttributes"
})
public class GetHostAttributesDataType {

    protected AttributeMetaDescriptorDataType hostAttributes;

    /**
     * Gets the value of the hostAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public AttributeMetaDescriptorDataType getHostAttributes() {
        return hostAttributes;
    }

    /**
     * Sets the value of the hostAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public void setHostAttributes(AttributeMetaDescriptorDataType value) {
        this.hostAttributes = value;
    }

}
