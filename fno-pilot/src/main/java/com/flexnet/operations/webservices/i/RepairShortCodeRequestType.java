
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for repairShortCodeRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="repairShortCodeRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shortCodeData" type="{urn:com.macrovision:flexnet/operations}repairShortCodeDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "repairShortCodeRequestType", propOrder = {
    "shortCodeData"
})
public class RepairShortCodeRequestType {

    protected RepairShortCodeDataType shortCodeData;

    /**
     * Gets the value of the shortCodeData property.
     * 
     * @return
     *     possible object is
     *     {@link RepairShortCodeDataType }
     *     
     */
    public RepairShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }

    /**
     * Sets the value of the shortCodeData property.
     * 
     * @param value
     *     allowed object is
     *     {@link RepairShortCodeDataType }
     *     
     */
    public void setShortCodeData(RepairShortCodeDataType value) {
        this.shortCodeData = value;
    }

}
