
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for entitlementStateDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entitlementStateDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementIdentifier" type="{urn:v2.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="stateToSet" type="{urn:v2.webservices.operations.flexnet.com}StateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entitlementStateDataType", propOrder = {
    "entitlementIdentifier",
    "stateToSet"
})
public class EntitlementStateDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementIdentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType stateToSet;

    /**
     * Gets the value of the entitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }

    /**
     * Sets the value of the entitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementIdentifier(EntitlementIdentifierType value) {
        this.entitlementIdentifier = value;
    }

    /**
     * Gets the value of the stateToSet property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getStateToSet() {
        return stateToSet;
    }

    /**
     * Sets the value of the stateToSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setStateToSet(StateType value) {
        this.stateToSet = value;
    }

}
