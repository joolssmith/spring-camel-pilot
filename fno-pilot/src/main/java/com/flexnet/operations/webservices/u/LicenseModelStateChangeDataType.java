
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for licenseModelStateChangeDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="licenseModelStateChangeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="licenseModelIdentifier" type="{urn:v2.webservices.operations.flexnet.com}licenseModelIdentifierType"/&gt;
 *         &lt;element name="stateChangeRecord" type="{urn:v2.webservices.operations.flexnet.com}stateChangeDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "licenseModelStateChangeDataType", propOrder = {
    "licenseModelIdentifier",
    "stateChangeRecord"
})
public class LicenseModelStateChangeDataType {

    @XmlElement(required = true)
    protected LicenseModelIdentifierType licenseModelIdentifier;
    protected List<StateChangeDataType> stateChangeRecord;

    /**
     * Gets the value of the licenseModelIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModelIdentifier() {
        return licenseModelIdentifier;
    }

    /**
     * Sets the value of the licenseModelIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModelIdentifier(LicenseModelIdentifierType value) {
        this.licenseModelIdentifier = value;
    }

    /**
     * Gets the value of the stateChangeRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stateChangeRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStateChangeRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StateChangeDataType }
     * 
     * 
     */
    public List<StateChangeDataType> getStateChangeRecord() {
        if (stateChangeRecord == null) {
            stateChangeRecord = new ArrayList<StateChangeDataType>();
        }
        return this.stateChangeRecord;
    }

}
