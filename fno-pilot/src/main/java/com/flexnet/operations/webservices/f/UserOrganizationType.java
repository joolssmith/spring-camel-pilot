
package com.flexnet.operations.webservices.f;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userOrganizationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userOrganizationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="organization" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType"/&gt;
 *         &lt;element name="roles" type="{urn:com.macrovision:flexnet/operations}userOrganizationRolesListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userOrganizationType", propOrder = {
    "organization",
    "roles"
})
public class UserOrganizationType {

    @XmlElement(required = true)
    protected OrganizationIdentifierType organization;
    protected UserOrganizationRolesListType roles;

    /**
     * Gets the value of the organization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getOrganization() {
        return organization;
    }

    /**
     * Sets the value of the organization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setOrganization(OrganizationIdentifierType value) {
        this.organization = value;
    }

    /**
     * Gets the value of the roles property.
     * 
     * @return
     *     possible object is
     *     {@link UserOrganizationRolesListType }
     *     
     */
    public UserOrganizationRolesListType getRoles() {
        return roles;
    }

    /**
     * Sets the value of the roles property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserOrganizationRolesListType }
     *     
     */
    public void setRoles(UserOrganizationRolesListType value) {
        this.roles = value;
    }

}
