
package com.flexnet.operations.webservices.g;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for handleProductCategoryToOrgRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="handleProductCategoryToOrgRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="org" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType"/&gt;
 *         &lt;element name="productCategory" type="{urn:com.macrovision:flexnet/operations}productCategoryDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "handleProductCategoryToOrgRequestType", propOrder = {
    "org",
    "productCategory"
})
public class HandleProductCategoryToOrgRequestType {

    @XmlElement(required = true)
    protected OrganizationIdentifierType org;
    @XmlElement(required = true)
    protected List<ProductCategoryDataType> productCategory;

    /**
     * Gets the value of the org property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getOrg() {
        return org;
    }

    /**
     * Sets the value of the org property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setOrg(OrganizationIdentifierType value) {
        this.org = value;
    }

    /**
     * Gets the value of the productCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProductCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductCategoryDataType }
     * 
     * 
     */
    public List<ProductCategoryDataType> getProductCategory() {
        if (productCategory == null) {
            productCategory = new ArrayList<ProductCategoryDataType>();
        }
        return this.productCategory;
    }

}
