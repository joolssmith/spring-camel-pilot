
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for uniformSuiteStateDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="uniformSuiteStateDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="suiteIdentifier" type="{urn:v1.webservices.operations.flexnet.com}suiteIdentifierType"/&gt;
 *         &lt;element name="stateToSet" type="{urn:v1.webservices.operations.flexnet.com}StateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "uniformSuiteStateDataType", propOrder = {
    "suiteIdentifier",
    "stateToSet"
})
public class UniformSuiteStateDataType {

    @XmlElement(required = true)
    protected SuiteIdentifierType suiteIdentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType stateToSet;

    /**
     * Gets the value of the suiteIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link SuiteIdentifierType }
     *     
     */
    public SuiteIdentifierType getSuiteIdentifier() {
        return suiteIdentifier;
    }

    /**
     * Sets the value of the suiteIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link SuiteIdentifierType }
     *     
     */
    public void setSuiteIdentifier(SuiteIdentifierType value) {
        this.suiteIdentifier = value;
    }

    /**
     * Gets the value of the stateToSet property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getStateToSet() {
        return stateToSet;
    }

    /**
     * Sets the value of the stateToSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setStateToSet(StateType value) {
        this.stateToSet = value;
    }

}
