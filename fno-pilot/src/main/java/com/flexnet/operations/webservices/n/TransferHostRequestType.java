
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transferHostRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferHostRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sourceHosts" type="{urn:v1.webservices.operations.flexnet.com}transferHostList"/&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="poolEntitlements" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferHostRequestType", propOrder = {
    "sourceHosts",
    "soldTo",
    "poolEntitlements"
})
public class TransferHostRequestType {

    @XmlElement(required = true)
    protected TransferHostList sourceHosts;
    protected String soldTo;
    protected Boolean poolEntitlements;

    /**
     * Gets the value of the sourceHosts property.
     * 
     * @return
     *     possible object is
     *     {@link TransferHostList }
     *     
     */
    public TransferHostList getSourceHosts() {
        return sourceHosts;
    }

    /**
     * Sets the value of the sourceHosts property.
     * 
     * @param value
     *     allowed object is
     *     {@link TransferHostList }
     *     
     */
    public void setSourceHosts(TransferHostList value) {
        this.sourceHosts = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldTo(String value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the poolEntitlements property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPoolEntitlements() {
        return poolEntitlements;
    }

    /**
     * Sets the value of the poolEntitlements property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPoolEntitlements(Boolean value) {
        this.poolEntitlements = value;
    }

}
