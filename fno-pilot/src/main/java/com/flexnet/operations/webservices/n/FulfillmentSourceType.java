
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FulfillmentSourceType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FulfillmentSourceType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="ONLINE"/&gt;
 *     &lt;enumeration value="APPLICATION"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "FulfillmentSourceType")
@XmlEnum
public enum FulfillmentSourceType {

    ONLINE,
    APPLICATION;

    public String value() {
        return name();
    }

    public static FulfillmentSourceType fromValue(String v) {
        return valueOf(v);
    }

}
