
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateUniformSuiteDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateUniformSuiteDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uniformSuite" type="{urn:com.macrovision:flexnet/operations}updateUniformSuiteDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateUniformSuiteDataType", propOrder = {
    "uniformSuite",
    "reason"
})
public class FailedUpdateUniformSuiteDataType {

    @XmlElement(required = true)
    protected UpdateUniformSuiteDataType uniformSuite;
    protected String reason;

    /**
     * Gets the value of the uniformSuite property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateUniformSuiteDataType }
     *     
     */
    public UpdateUniformSuiteDataType getUniformSuite() {
        return uniformSuite;
    }

    /**
     * Sets the value of the uniformSuite property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateUniformSuiteDataType }
     *     
     */
    public void setUniformSuite(UpdateUniformSuiteDataType value) {
        this.uniformSuite = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
