
package com.flexnet.operations.webservices.w;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedLinkAcctDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedLinkAcctDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedAcctData" type="{urn:v2.webservices.operations.flexnet.com}failedLinkAcctDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedLinkAcctDataListType", propOrder = {
    "failedAcctData"
})
public class FailedLinkAcctDataListType {

    protected List<FailedLinkAcctDataType> failedAcctData;

    /**
     * Gets the value of the failedAcctData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedAcctData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedAcctData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedLinkAcctDataType }
     * 
     * 
     */
    public List<FailedLinkAcctDataType> getFailedAcctData() {
        if (failedAcctData == null) {
            failedAcctData = new ArrayList<FailedLinkAcctDataType>();
        }
        return this.failedAcctData;
    }

}
