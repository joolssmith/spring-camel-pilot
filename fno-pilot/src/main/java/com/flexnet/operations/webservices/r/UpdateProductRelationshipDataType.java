
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateProductRelationshipDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateProductRelationshipDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:v1.webservices.operations.flexnet.com}productRelationshipDataType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="newRelation" type="{urn:v1.webservices.operations.flexnet.com}relationshipType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateProductRelationshipDataType", propOrder = {
    "newRelation"
})
public class UpdateProductRelationshipDataType
    extends ProductRelationshipDataType
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected RelationshipType newRelation;

    /**
     * Gets the value of the newRelation property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipType }
     *     
     */
    public RelationshipType getNewRelation() {
        return newRelation;
    }

    /**
     * Sets the value of the newRelation property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipType }
     *     
     */
    public void setNewRelation(RelationshipType value) {
        this.newRelation = value;
    }

}
