
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for licenseModelDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="licenseModelDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="licenseModelIdentifier" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v1.webservices.operations.flexnet.com}licenseTechnologyIdentifierType"/&gt;
 *         &lt;element name="attributeDetails" type="{urn:v1.webservices.operations.flexnet.com}modelAttributesType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "licenseModelDetailsType", propOrder = {
    "licenseModelIdentifier",
    "licenseTechnology",
    "attributeDetails"
})
public class LicenseModelDetailsType {

    @XmlElement(required = true)
    protected LicenseModelIdentifierType licenseModelIdentifier;
    @XmlElement(required = true)
    protected LicenseTechnologyIdentifierType licenseTechnology;
    @XmlElement(required = true)
    protected ModelAttributesType attributeDetails;

    /**
     * Gets the value of the licenseModelIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModelIdentifier() {
        return licenseModelIdentifier;
    }

    /**
     * Sets the value of the licenseModelIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModelIdentifier(LicenseModelIdentifierType value) {
        this.licenseModelIdentifier = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the attributeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ModelAttributesType }
     *     
     */
    public ModelAttributesType getAttributeDetails() {
        return attributeDetails;
    }

    /**
     * Sets the value of the attributeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ModelAttributesType }
     *     
     */
    public void setAttributeDetails(ModelAttributesType value) {
        this.attributeDetails = value;
    }

}
