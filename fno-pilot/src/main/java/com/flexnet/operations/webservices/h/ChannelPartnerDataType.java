
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for channelPartnerDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="channelPartnerDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="tierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="organizationUnit" type="{urn:com.macrovision:flexnet/opsembedded}organizationIdentifierType"/&gt;
 *         &lt;element name="contact" type="{urn:com.macrovision:flexnet/opsembedded}userIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="currentOwner" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "channelPartnerDataType", propOrder = {
    "tierName",
    "organizationUnit",
    "contact",
    "currentOwner"
})
public class ChannelPartnerDataType {

    @XmlElement(required = true)
    protected String tierName;
    @XmlElement(required = true)
    protected OrganizationIdentifierType organizationUnit;
    protected UserIdentifierType contact;
    protected Boolean currentOwner;

    /**
     * Gets the value of the tierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTierName() {
        return tierName;
    }

    /**
     * Sets the value of the tierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTierName(String value) {
        this.tierName = value;
    }

    /**
     * Gets the value of the organizationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getOrganizationUnit() {
        return organizationUnit;
    }

    /**
     * Sets the value of the organizationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setOrganizationUnit(OrganizationIdentifierType value) {
        this.organizationUnit = value;
    }

    /**
     * Gets the value of the contact property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifierType }
     *     
     */
    public UserIdentifierType getContact() {
        return contact;
    }

    /**
     * Sets the value of the contact property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifierType }
     *     
     */
    public void setContact(UserIdentifierType value) {
        this.contact = value;
    }

    /**
     * Gets the value of the currentOwner property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCurrentOwner() {
        return currentOwner;
    }

    /**
     * Sets the value of the currentOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCurrentOwner(Boolean value) {
        this.currentOwner = value;
    }

}
