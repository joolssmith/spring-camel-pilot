
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for entitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="simpleEntitlement" type="{urn:v3.webservices.operations.flexnet.com}simpleEntitlementDataType" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntitlement" type="{urn:v3.webservices.operations.flexnet.com}bulkEntitlementDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entitlementDataType", propOrder = {
    "simpleEntitlement",
    "bulkEntitlement"
})
public class EntitlementDataType {

    protected SimpleEntitlementDataType simpleEntitlement;
    protected BulkEntitlementDataType bulkEntitlement;

    /**
     * Gets the value of the simpleEntitlement property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleEntitlementDataType }
     *     
     */
    public SimpleEntitlementDataType getSimpleEntitlement() {
        return simpleEntitlement;
    }

    /**
     * Sets the value of the simpleEntitlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleEntitlementDataType }
     *     
     */
    public void setSimpleEntitlement(SimpleEntitlementDataType value) {
        this.simpleEntitlement = value;
    }

    /**
     * Gets the value of the bulkEntitlement property.
     * 
     * @return
     *     possible object is
     *     {@link BulkEntitlementDataType }
     *     
     */
    public BulkEntitlementDataType getBulkEntitlement() {
        return bulkEntitlement;
    }

    /**
     * Sets the value of the bulkEntitlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link BulkEntitlementDataType }
     *     
     */
    public void setBulkEntitlement(BulkEntitlementDataType value) {
        this.bulkEntitlement = value;
    }

}
