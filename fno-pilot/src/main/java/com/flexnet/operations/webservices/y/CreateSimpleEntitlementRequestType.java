
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createSimpleEntitlementRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createSimpleEntitlementRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="simpleEntitlement" type="{urn:v3.webservices.operations.flexnet.com}createSimpleEntitlementDataType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="opType" type="{urn:v3.webservices.operations.flexnet.com}CreateOrUpdateOperationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createSimpleEntitlementRequestType", propOrder = {
    "simpleEntitlement",
    "opType"
})
public class CreateSimpleEntitlementRequestType {

    @XmlElement(required = true)
    protected List<CreateSimpleEntitlementDataType> simpleEntitlement;
    @XmlSchemaType(name = "NMTOKEN")
    protected CreateOrUpdateOperationType opType;

    /**
     * Gets the value of the simpleEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the simpleEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimpleEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateSimpleEntitlementDataType }
     * 
     * 
     */
    public List<CreateSimpleEntitlementDataType> getSimpleEntitlement() {
        if (simpleEntitlement == null) {
            simpleEntitlement = new ArrayList<CreateSimpleEntitlementDataType>();
        }
        return this.simpleEntitlement;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrUpdateOperationType }
     *     
     */
    public CreateOrUpdateOperationType getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrUpdateOperationType }
     *     
     */
    public void setOpType(CreateOrUpdateOperationType value) {
        this.opType = value;
    }

}
