
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteWebRegKeyRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteWebRegKeyRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="webRegKeyList" type="{urn:v1.webservices.operations.flexnet.com}webRegKeysListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteWebRegKeyRequestType", propOrder = {
    "webRegKeyList"
})
public class DeleteWebRegKeyRequestType {

    @XmlElement(required = true)
    protected WebRegKeysListType webRegKeyList;

    /**
     * Gets the value of the webRegKeyList property.
     * 
     * @return
     *     possible object is
     *     {@link WebRegKeysListType }
     *     
     */
    public WebRegKeysListType getWebRegKeyList() {
        return webRegKeyList;
    }

    /**
     * Sets the value of the webRegKeyList property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebRegKeysListType }
     *     
     */
    public void setWebRegKeyList(WebRegKeysListType value) {
        this.webRegKeyList = value;
    }

}
