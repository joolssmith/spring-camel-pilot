
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setLicenseRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setLicenseRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="onholdFulfillmentList" type="{urn:com.macrovision:flexnet/operations}onholdFulfillmentListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setLicenseRequestType", propOrder = {
    "onholdFulfillmentList"
})
public class SetLicenseRequestType {

    @XmlElement(required = true)
    protected OnholdFulfillmentListType onholdFulfillmentList;

    /**
     * Gets the value of the onholdFulfillmentList property.
     * 
     * @return
     *     possible object is
     *     {@link OnholdFulfillmentListType }
     *     
     */
    public OnholdFulfillmentListType getOnholdFulfillmentList() {
        return onholdFulfillmentList;
    }

    /**
     * Sets the value of the onholdFulfillmentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link OnholdFulfillmentListType }
     *     
     */
    public void setOnholdFulfillmentList(OnholdFulfillmentListType value) {
        this.onholdFulfillmentList = value;
    }

}
