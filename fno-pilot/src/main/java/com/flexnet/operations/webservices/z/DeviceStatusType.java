
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="deviceStatusType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="ACTIVE"/&gt;
 *     &lt;enumeration value="OBSOLETE"/&gt;
 *     &lt;enumeration value="TEST"/&gt;
 *     &lt;enumeration value="RETURNED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "deviceStatusType")
@XmlEnum
public enum DeviceStatusType {

    ACTIVE,
    OBSOLETE,
    TEST,
    RETURNED;

    public String value() {
        return name();
    }

    public static DeviceStatusType fromValue(String v) {
        return valueOf(v);
    }

}
