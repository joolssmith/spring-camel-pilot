
package com.flexnet.operations.webservices.z;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for getDeviceResponseData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDeviceResponseData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceId" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceId" minOccurs="0"/&gt;
 *         &lt;element name="backupDeviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceStatusType" minOccurs="0"/&gt;
 *         &lt;element name="servedStatus" type="{urn:v3.fne.webservices.operations.flexnet.com}servedStatusType" minOccurs="0"/&gt;
 *         &lt;element name="hosted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v3.fne.webservices.operations.flexnet.com}soldToType" minOccurs="0"/&gt;
 *         &lt;element name="baseProduct" type="{urn:v3.fne.webservices.operations.flexnet.com}productPKType" minOccurs="0"/&gt;
 *         &lt;element name="hasBufferLicense" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="bufferLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="hasAddonLicense" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="addonLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="publisherIdName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="addon" type="{urn:v3.fne.webservices.operations.flexnet.com}addonData" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="feature" type="{urn:v3.fne.webservices.operations.flexnet.com}featureDataDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v3.fne.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="parentIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceId" minOccurs="0"/&gt;
 *         &lt;element name="machineType" type="{urn:v3.fne.webservices.operations.flexnet.com}machineTypeType" minOccurs="0"/&gt;
 *         &lt;element name="vmName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vmInfo" type="{urn:v3.fne.webservices.operations.flexnet.com}dictionaryType" minOccurs="0"/&gt;
 *         &lt;element name="vendorDictionary" type="{urn:v3.fne.webservices.operations.flexnet.com}dictionaryType" minOccurs="0"/&gt;
 *         &lt;element name="update" type="{urn:v3.fne.webservices.operations.flexnet.com}updatesType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="userString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="userIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}userIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="lastModified" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="lastSyncTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="lastRequestTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDeviceResponseData", propOrder = {
    "deviceId",
    "backupDeviceId",
    "name",
    "description",
    "hostTypeName",
    "status",
    "servedStatus",
    "hosted",
    "soldTo",
    "baseProduct",
    "hasBufferLicense",
    "bufferLicense",
    "hasAddonLicense",
    "addonLicense",
    "publisherIdName",
    "addon",
    "feature",
    "customAttributes",
    "parentIdentifier",
    "machineType",
    "vmName",
    "vmInfo",
    "vendorDictionary",
    "update",
    "userString",
    "userIdentifier",
    "lastModified",
    "lastSyncTime",
    "lastRequestTime"
})
public class GetDeviceResponseData {

    protected DeviceId deviceId;
    protected String backupDeviceId;
    protected String name;
    protected String description;
    protected String hostTypeName;
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceStatusType status;
    @XmlSchemaType(name = "NMTOKEN")
    protected ServedStatusType servedStatus;
    protected Boolean hosted;
    protected SoldToType soldTo;
    protected ProductPKType baseProduct;
    protected Boolean hasBufferLicense;
    protected byte[] bufferLicense;
    protected Boolean hasAddonLicense;
    protected byte[] addonLicense;
    protected String publisherIdName;
    protected List<AddonData> addon;
    protected List<FeatureDataDataType> feature;
    protected AttributeDescriptorDataType customAttributes;
    protected DeviceId parentIdentifier;
    @XmlSchemaType(name = "NMTOKEN")
    protected MachineTypeType machineType;
    protected String vmName;
    protected DictionaryType vmInfo;
    protected DictionaryType vendorDictionary;
    protected List<UpdatesType> update;
    protected String userString;
    protected UserIdentifierType userIdentifier;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModified;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastSyncTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastRequestTime;

    /**
     * Gets the value of the deviceId property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceId }
     *     
     */
    public DeviceId getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceId }
     *     
     */
    public void setDeviceId(DeviceId value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the backupDeviceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBackupDeviceId() {
        return backupDeviceId;
    }

    /**
     * Sets the value of the backupDeviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBackupDeviceId(String value) {
        this.backupDeviceId = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostTypeName(String value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceStatusType }
     *     
     */
    public DeviceStatusType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceStatusType }
     *     
     */
    public void setStatus(DeviceStatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the servedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ServedStatusType }
     *     
     */
    public ServedStatusType getServedStatus() {
        return servedStatus;
    }

    /**
     * Sets the value of the servedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServedStatusType }
     *     
     */
    public void setServedStatus(ServedStatusType value) {
        this.servedStatus = value;
    }

    /**
     * Gets the value of the hosted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHosted() {
        return hosted;
    }

    /**
     * Sets the value of the hosted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHosted(Boolean value) {
        this.hosted = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SoldToType }
     *     
     */
    public SoldToType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoldToType }
     *     
     */
    public void setSoldTo(SoldToType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the baseProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ProductPKType }
     *     
     */
    public ProductPKType getBaseProduct() {
        return baseProduct;
    }

    /**
     * Sets the value of the baseProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductPKType }
     *     
     */
    public void setBaseProduct(ProductPKType value) {
        this.baseProduct = value;
    }

    /**
     * Gets the value of the hasBufferLicense property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasBufferLicense() {
        return hasBufferLicense;
    }

    /**
     * Sets the value of the hasBufferLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasBufferLicense(Boolean value) {
        this.hasBufferLicense = value;
    }

    /**
     * Gets the value of the bufferLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBufferLicense() {
        return bufferLicense;
    }

    /**
     * Sets the value of the bufferLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBufferLicense(byte[] value) {
        this.bufferLicense = value;
    }

    /**
     * Gets the value of the hasAddonLicense property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasAddonLicense() {
        return hasAddonLicense;
    }

    /**
     * Sets the value of the hasAddonLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasAddonLicense(Boolean value) {
        this.hasAddonLicense = value;
    }

    /**
     * Gets the value of the addonLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAddonLicense() {
        return addonLicense;
    }

    /**
     * Sets the value of the addonLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAddonLicense(byte[] value) {
        this.addonLicense = value;
    }

    /**
     * Gets the value of the publisherIdName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPublisherIdName() {
        return publisherIdName;
    }

    /**
     * Sets the value of the publisherIdName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPublisherIdName(String value) {
        this.publisherIdName = value;
    }

    /**
     * Gets the value of the addon property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addon property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddon().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddonData }
     * 
     * 
     */
    public List<AddonData> getAddon() {
        if (addon == null) {
            addon = new ArrayList<AddonData>();
        }
        return this.addon;
    }

    /**
     * Gets the value of the feature property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the feature property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeature().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureDataDataType }
     * 
     * 
     */
    public List<FeatureDataDataType> getFeature() {
        if (feature == null) {
            feature = new ArrayList<FeatureDataDataType>();
        }
        return this.feature;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceId }
     *     
     */
    public DeviceId getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceId }
     *     
     */
    public void setParentIdentifier(DeviceId value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the machineType property.
     * 
     * @return
     *     possible object is
     *     {@link MachineTypeType }
     *     
     */
    public MachineTypeType getMachineType() {
        return machineType;
    }

    /**
     * Sets the value of the machineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MachineTypeType }
     *     
     */
    public void setMachineType(MachineTypeType value) {
        this.machineType = value;
    }

    /**
     * Gets the value of the vmName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVmName() {
        return vmName;
    }

    /**
     * Sets the value of the vmName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVmName(String value) {
        this.vmName = value;
    }

    /**
     * Gets the value of the vmInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DictionaryType }
     *     
     */
    public DictionaryType getVmInfo() {
        return vmInfo;
    }

    /**
     * Sets the value of the vmInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DictionaryType }
     *     
     */
    public void setVmInfo(DictionaryType value) {
        this.vmInfo = value;
    }

    /**
     * Gets the value of the vendorDictionary property.
     * 
     * @return
     *     possible object is
     *     {@link DictionaryType }
     *     
     */
    public DictionaryType getVendorDictionary() {
        return vendorDictionary;
    }

    /**
     * Sets the value of the vendorDictionary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DictionaryType }
     *     
     */
    public void setVendorDictionary(DictionaryType value) {
        this.vendorDictionary = value;
    }

    /**
     * Gets the value of the update property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the update property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpdate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdatesType }
     * 
     * 
     */
    public List<UpdatesType> getUpdate() {
        if (update == null) {
            update = new ArrayList<UpdatesType>();
        }
        return this.update;
    }

    /**
     * Gets the value of the userString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserString() {
        return userString;
    }

    /**
     * Sets the value of the userString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserString(String value) {
        this.userString = value;
    }

    /**
     * Gets the value of the userIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifierType }
     *     
     */
    public UserIdentifierType getUserIdentifier() {
        return userIdentifier;
    }

    /**
     * Sets the value of the userIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifierType }
     *     
     */
    public void setUserIdentifier(UserIdentifierType value) {
        this.userIdentifier = value;
    }

    /**
     * Gets the value of the lastModified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModified() {
        return lastModified;
    }

    /**
     * Sets the value of the lastModified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModified(XMLGregorianCalendar value) {
        this.lastModified = value;
    }

    /**
     * Gets the value of the lastSyncTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastSyncTime() {
        return lastSyncTime;
    }

    /**
     * Sets the value of the lastSyncTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastSyncTime(XMLGregorianCalendar value) {
        this.lastSyncTime = value;
    }

    /**
     * Gets the value of the lastRequestTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastRequestTime() {
        return lastRequestTime;
    }

    /**
     * Sets the value of the lastRequestTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastRequestTime(XMLGregorianCalendar value) {
        this.lastRequestTime = value;
    }

}
