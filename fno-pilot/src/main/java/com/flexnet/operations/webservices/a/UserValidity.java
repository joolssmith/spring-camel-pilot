
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UserValidity.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="UserValidity"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="IGNORE"/&gt;
 *     &lt;enumeration value="NOT_VALIDATING"/&gt;
 *     &lt;enumeration value="INVALID"/&gt;
 *     &lt;enumeration value="VALID"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "UserValidity")
@XmlEnum
public enum UserValidity {

    IGNORE,
    NOT_VALIDATING,
    INVALID,
    VALID;

    public String value() {
        return name();
    }

    public static UserValidity fromValue(String v) {
        return valueOf(v);
    }

}
