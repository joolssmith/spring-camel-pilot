
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addedEntitlementLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addedEntitlementLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementIdentifier" type="{urn:v2.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="lineItemUniqueIds" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItemUniqueIds" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="lineItemIdentifiers" type="{urn:v2.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItemIdentifiers" type="{urn:v2.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addedEntitlementLineItemDataType", propOrder = {
    "entitlementIdentifier",
    "lineItemUniqueIds",
    "maintenanceLineItemUniqueIds",
    "lineItemIdentifiers",
    "maintenanceLineItemIdentifiers"
})
public class AddedEntitlementLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementIdentifier;
    protected List<String> lineItemUniqueIds;
    protected List<String> maintenanceLineItemUniqueIds;
    protected List<EntitlementLineItemIdentifierType> lineItemIdentifiers;
    protected List<EntitlementLineItemIdentifierType> maintenanceLineItemIdentifiers;

    /**
     * Gets the value of the entitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }

    /**
     * Sets the value of the entitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementIdentifier(EntitlementIdentifierType value) {
        this.entitlementIdentifier = value;
    }

    /**
     * Gets the value of the lineItemUniqueIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemUniqueIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemUniqueIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLineItemUniqueIds() {
        if (lineItemUniqueIds == null) {
            lineItemUniqueIds = new ArrayList<String>();
        }
        return this.lineItemUniqueIds;
    }

    /**
     * Gets the value of the maintenanceLineItemUniqueIds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maintenanceLineItemUniqueIds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaintenanceLineItemUniqueIds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMaintenanceLineItemUniqueIds() {
        if (maintenanceLineItemUniqueIds == null) {
            maintenanceLineItemUniqueIds = new ArrayList<String>();
        }
        return this.maintenanceLineItemUniqueIds;
    }

    /**
     * Gets the value of the lineItemIdentifiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemIdentifiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemIdentifiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntitlementLineItemIdentifierType }
     * 
     * 
     */
    public List<EntitlementLineItemIdentifierType> getLineItemIdentifiers() {
        if (lineItemIdentifiers == null) {
            lineItemIdentifiers = new ArrayList<EntitlementLineItemIdentifierType>();
        }
        return this.lineItemIdentifiers;
    }

    /**
     * Gets the value of the maintenanceLineItemIdentifiers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maintenanceLineItemIdentifiers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaintenanceLineItemIdentifiers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntitlementLineItemIdentifierType }
     * 
     * 
     */
    public List<EntitlementLineItemIdentifierType> getMaintenanceLineItemIdentifiers() {
        if (maintenanceLineItemIdentifiers == null) {
            maintenanceLineItemIdentifiers = new ArrayList<EntitlementLineItemIdentifierType>();
        }
        return this.maintenanceLineItemIdentifiers;
    }

}
