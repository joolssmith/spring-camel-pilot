
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="BaseProductId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="HostData" type="{http://producersuite.flexerasoftware.com/EntitlementService/}HostData"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "producerId",
    "baseProductId",
    "hostData"
})
@XmlRootElement(name = "getFlexNetFeaturesForBaseProduct")
public class GetFlexNetFeaturesForBaseProduct {

    @XmlElement(name = "ProducerId", required = true)
    protected String producerId;
    @XmlElement(name = "BaseProductId", required = true)
    protected String baseProductId;
    @XmlElement(name = "HostData", required = true)
    protected HostData hostData;

    /**
     * Gets the value of the producerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerId() {
        return producerId;
    }

    /**
     * Sets the value of the producerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerId(String value) {
        this.producerId = value;
    }

    /**
     * Gets the value of the baseProductId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseProductId() {
        return baseProductId;
    }

    /**
     * Sets the value of the baseProductId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseProductId(String value) {
        this.baseProductId = value;
    }

    /**
     * Gets the value of the hostData property.
     * 
     * @return
     *     possible object is
     *     {@link HostData }
     *     
     */
    public HostData getHostData() {
        return hostData;
    }

    /**
     * Sets the value of the hostData property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostData }
     *     
     */
    public void setHostData(HostData value) {
        this.hostData = value;
    }

}
