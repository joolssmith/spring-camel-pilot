
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for usageSummaryGroupByType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="usageSummaryGroupByType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="LINE_ITEM"/&gt;
 *     &lt;enumeration value="SERVER"/&gt;
 *     &lt;enumeration value="FEATURE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "usageSummaryGroupByType")
@XmlEnum
public enum UsageSummaryGroupByType {

    LINE_ITEM,
    SERVER,
    FEATURE;

    public String value() {
        return name();
    }

    public static UsageSummaryGroupByType fromValue(String v) {
        return valueOf(v);
    }

}
