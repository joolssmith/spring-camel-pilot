
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VersionFormatType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VersionFormatType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="FIXED"/&gt;
 *     &lt;enumeration value="DATE_BASED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VersionFormatType")
@XmlEnum
public enum VersionFormatType {

    FIXED,
    DATE_BASED;

    public String value() {
        return name();
    }

    public static VersionFormatType fromValue(String v) {
        return valueOf(v);
    }

}
