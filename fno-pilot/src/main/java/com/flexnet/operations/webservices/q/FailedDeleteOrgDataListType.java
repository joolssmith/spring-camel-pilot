
package com.flexnet.operations.webservices.q;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedDeleteOrgDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedDeleteOrgDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedOrg" type="{urn:v1.webservices.operations.flexnet.com}failedDeleteOrgDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedDeleteOrgDataListType", propOrder = {
    "failedOrg"
})
public class FailedDeleteOrgDataListType {

    protected List<FailedDeleteOrgDataType> failedOrg;

    /**
     * Gets the value of the failedOrg property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedOrg property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedOrg().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedDeleteOrgDataType }
     * 
     * 
     */
    public List<FailedDeleteOrgDataType> getFailedOrg() {
        if (failedOrg == null) {
            failedOrg = new ArrayList<FailedDeleteOrgDataType>();
        }
        return this.failedOrg;
    }

}
