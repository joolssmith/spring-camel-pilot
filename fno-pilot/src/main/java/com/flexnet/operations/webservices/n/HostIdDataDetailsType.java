
package com.flexnet.operations.webservices.n;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hostIdDataDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hostIdDataDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="serverHost" type="{urn:v1.webservices.operations.flexnet.com}ServerIDsType" minOccurs="0"/&gt;
 *         &lt;element name="nodeLockHost" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countedNodeLockHostIds" type="{urn:v1.webservices.operations.flexnet.com}NodeIDsType" minOccurs="0"/&gt;
 *         &lt;element name="customHost" type="{urn:v1.webservices.operations.flexnet.com}CustomHostIDType" minOccurs="0"/&gt;
 *         &lt;element name="countFromParent" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="countFromOwn" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="parentActivationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hostIdDataDetailsType", propOrder = {
    "serverHost",
    "nodeLockHost",
    "countedNodeLockHostIds",
    "customHost",
    "countFromParent",
    "countFromOwn",
    "parentActivationId"
})
public class HostIdDataDetailsType {

    protected ServerIDsType serverHost;
    protected String nodeLockHost;
    protected NodeIDsType countedNodeLockHostIds;
    protected CustomHostIDType customHost;
    protected BigInteger countFromParent;
    protected BigInteger countFromOwn;
    protected String parentActivationId;

    /**
     * Gets the value of the serverHost property.
     * 
     * @return
     *     possible object is
     *     {@link ServerIDsType }
     *     
     */
    public ServerIDsType getServerHost() {
        return serverHost;
    }

    /**
     * Sets the value of the serverHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerIDsType }
     *     
     */
    public void setServerHost(ServerIDsType value) {
        this.serverHost = value;
    }

    /**
     * Gets the value of the nodeLockHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNodeLockHost() {
        return nodeLockHost;
    }

    /**
     * Sets the value of the nodeLockHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNodeLockHost(String value) {
        this.nodeLockHost = value;
    }

    /**
     * Gets the value of the countedNodeLockHostIds property.
     * 
     * @return
     *     possible object is
     *     {@link NodeIDsType }
     *     
     */
    public NodeIDsType getCountedNodeLockHostIds() {
        return countedNodeLockHostIds;
    }

    /**
     * Sets the value of the countedNodeLockHostIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeIDsType }
     *     
     */
    public void setCountedNodeLockHostIds(NodeIDsType value) {
        this.countedNodeLockHostIds = value;
    }

    /**
     * Gets the value of the customHost property.
     * 
     * @return
     *     possible object is
     *     {@link CustomHostIDType }
     *     
     */
    public CustomHostIDType getCustomHost() {
        return customHost;
    }

    /**
     * Sets the value of the customHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomHostIDType }
     *     
     */
    public void setCustomHost(CustomHostIDType value) {
        this.customHost = value;
    }

    /**
     * Gets the value of the countFromParent property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCountFromParent() {
        return countFromParent;
    }

    /**
     * Sets the value of the countFromParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCountFromParent(BigInteger value) {
        this.countFromParent = value;
    }

    /**
     * Gets the value of the countFromOwn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCountFromOwn() {
        return countFromOwn;
    }

    /**
     * Sets the value of the countFromOwn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCountFromOwn(BigInteger value) {
        this.countFromOwn = value;
    }

    /**
     * Gets the value of the parentActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentActivationId() {
        return parentActivationId;
    }

    /**
     * Sets the value of the parentActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentActivationId(String value) {
        this.parentActivationId = value;
    }

}
