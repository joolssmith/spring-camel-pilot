
package com.flexnet.operations.webservices.a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ActivationId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *         &lt;element name="EnterpriseId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AutoRedeem" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="Actor" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UserId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ParentDetails" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "producerId",
    "activationId",
    "enterpriseId",
    "autoRedeem",
    "actor",
    "userId",
    "parentDetails"
})
@XmlRootElement(name = "getActivationIdDetails")
public class GetActivationIdDetails {

    @XmlElement(name = "ProducerId", required = true)
    protected String producerId;
    @XmlElement(name = "ActivationId", required = true)
    protected List<String> activationId;
    @XmlElement(name = "EnterpriseId", required = true)
    protected String enterpriseId;
    @XmlElement(name = "AutoRedeem")
    protected boolean autoRedeem;
    @XmlElement(name = "Actor", required = true)
    protected String actor;
    @XmlElement(name = "UserId", required = true)
    protected String userId;
    @XmlElement(name = "ParentDetails")
    protected boolean parentDetails;

    /**
     * Gets the value of the producerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerId() {
        return producerId;
    }

    /**
     * Sets the value of the producerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerId(String value) {
        this.producerId = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activationId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivationId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getActivationId() {
        if (activationId == null) {
            activationId = new ArrayList<String>();
        }
        return this.activationId;
    }

    /**
     * Gets the value of the enterpriseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnterpriseId() {
        return enterpriseId;
    }

    /**
     * Sets the value of the enterpriseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnterpriseId(String value) {
        this.enterpriseId = value;
    }

    /**
     * Gets the value of the autoRedeem property.
     * 
     */
    public boolean isAutoRedeem() {
        return autoRedeem;
    }

    /**
     * Sets the value of the autoRedeem property.
     * 
     */
    public void setAutoRedeem(boolean value) {
        this.autoRedeem = value;
    }

    /**
     * Gets the value of the actor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActor() {
        return actor;
    }

    /**
     * Sets the value of the actor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActor(String value) {
        this.actor = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the parentDetails property.
     * 
     */
    public boolean isParentDetails() {
        return parentDetails;
    }

    /**
     * Sets the value of the parentDetails property.
     * 
     */
    public void setParentDetails(boolean value) {
        this.parentDetails = value;
    }

}
