
package com.flexnet.operations.webservices.n;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for overDraftDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="overDraftDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationIdMap" type="{urn:v1.webservices.operations.flexnet.com}activationIdOverDraftMapType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "overDraftDataListType", propOrder = {
    "activationIdMap"
})
public class OverDraftDataListType {

    @XmlElement(required = true)
    protected List<ActivationIdOverDraftMapType> activationIdMap;

    /**
     * Gets the value of the activationIdMap property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the activationIdMap property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActivationIdMap().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActivationIdOverDraftMapType }
     * 
     * 
     */
    public List<ActivationIdOverDraftMapType> getActivationIdMap() {
        if (activationIdMap == null) {
            activationIdMap = new ArrayList<ActivationIdOverDraftMapType>();
        }
        return this.activationIdMap;
    }

}
