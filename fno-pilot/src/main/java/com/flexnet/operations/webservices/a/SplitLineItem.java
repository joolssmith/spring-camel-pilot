
package com.flexnet.operations.webservices.a;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SplitLineItem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SplitLineItem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RightsId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SplitCountRequested" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/&gt;
 *         &lt;element name="SplitCountConsumed" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SplitLineItem", propOrder = {
    "rightsId",
    "splitCountRequested",
    "splitCountConsumed"
})
public class SplitLineItem {

    @XmlElement(name = "RightsId", required = true, nillable = true)
    protected String rightsId;
    @XmlElement(name = "SplitCountRequested", required = true, nillable = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger splitCountRequested;
    @XmlElement(name = "SplitCountConsumed", required = true, nillable = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger splitCountConsumed;

    /**
     * Gets the value of the rightsId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRightsId() {
        return rightsId;
    }

    /**
     * Sets the value of the rightsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRightsId(String value) {
        this.rightsId = value;
    }

    /**
     * Gets the value of the splitCountRequested property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSplitCountRequested() {
        return splitCountRequested;
    }

    /**
     * Sets the value of the splitCountRequested property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSplitCountRequested(BigInteger value) {
        this.splitCountRequested = value;
    }

    /**
     * Gets the value of the splitCountConsumed property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSplitCountConsumed() {
        return splitCountConsumed;
    }

    /**
     * Sets the value of the splitCountConsumed property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSplitCountConsumed(BigInteger value) {
        this.splitCountConsumed = value;
    }

}
