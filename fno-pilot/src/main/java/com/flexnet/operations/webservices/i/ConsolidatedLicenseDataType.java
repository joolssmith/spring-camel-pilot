
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for consolidatedLicenseDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="consolidatedLicenseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="consolidatedLicenseId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="licenseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="binaryLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="criteria" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="state" type="{urn:com.macrovision:flexnet/operations}StateType" minOccurs="0"/&gt;
 *         &lt;element name="consolidatedFulfillments" type="{urn:com.macrovision:flexnet/operations}fulfillmentIdentifierListType"/&gt;
 *         &lt;element name="licenseFiles" type="{urn:com.macrovision:flexnet/operations}licenseFileDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "consolidatedLicenseDataType", propOrder = {
    "consolidatedLicenseId",
    "licenseText",
    "binaryLicense",
    "soldTo",
    "criteria",
    "state",
    "consolidatedFulfillments",
    "licenseFiles"
})
public class ConsolidatedLicenseDataType {

    @XmlElement(required = true)
    protected String consolidatedLicenseId;
    protected String licenseText;
    protected byte[] binaryLicense;
    @XmlElement(required = true)
    protected String soldTo;
    @XmlElement(required = true)
    protected String criteria;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    @XmlElement(required = true)
    protected FulfillmentIdentifierListType consolidatedFulfillments;
    protected LicenseFileDataListType licenseFiles;

    /**
     * Gets the value of the consolidatedLicenseId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsolidatedLicenseId() {
        return consolidatedLicenseId;
    }

    /**
     * Sets the value of the consolidatedLicenseId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsolidatedLicenseId(String value) {
        this.consolidatedLicenseId = value;
    }

    /**
     * Gets the value of the licenseText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseText() {
        return licenseText;
    }

    /**
     * Sets the value of the licenseText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseText(String value) {
        this.licenseText = value;
    }

    /**
     * Gets the value of the binaryLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }

    /**
     * Sets the value of the binaryLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBinaryLicense(byte[] value) {
        this.binaryLicense = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldTo(String value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the criteria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCriteria() {
        return criteria;
    }

    /**
     * Sets the value of the criteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCriteria(String value) {
        this.criteria = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the consolidatedFulfillments property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierListType }
     *     
     */
    public FulfillmentIdentifierListType getConsolidatedFulfillments() {
        return consolidatedFulfillments;
    }

    /**
     * Sets the value of the consolidatedFulfillments property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierListType }
     *     
     */
    public void setConsolidatedFulfillments(FulfillmentIdentifierListType value) {
        this.consolidatedFulfillments = value;
    }

    /**
     * Gets the value of the licenseFiles property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public LicenseFileDataListType getLicenseFiles() {
        return licenseFiles;
    }

    /**
     * Sets the value of the licenseFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public void setLicenseFiles(LicenseFileDataListType value) {
        this.licenseFiles = value;
    }

}
