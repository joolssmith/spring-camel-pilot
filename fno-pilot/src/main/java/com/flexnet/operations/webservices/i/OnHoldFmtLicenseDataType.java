
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for onHoldFmtLicenseDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="onHoldFmtLicenseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="textLicense" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="binaryLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="licenseFiles" type="{urn:com.macrovision:flexnet/operations}licenseFileDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "onHoldFmtLicenseDataType", propOrder = {
    "fulfillmentId",
    "textLicense",
    "binaryLicense",
    "licenseFiles"
})
public class OnHoldFmtLicenseDataType {

    @XmlElement(required = true)
    protected String fulfillmentId;
    protected String textLicense;
    protected byte[] binaryLicense;
    protected LicenseFileDataListType licenseFiles;

    /**
     * Gets the value of the fulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentId() {
        return fulfillmentId;
    }

    /**
     * Sets the value of the fulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentId(String value) {
        this.fulfillmentId = value;
    }

    /**
     * Gets the value of the textLicense property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextLicense() {
        return textLicense;
    }

    /**
     * Sets the value of the textLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextLicense(String value) {
        this.textLicense = value;
    }

    /**
     * Gets the value of the binaryLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }

    /**
     * Sets the value of the binaryLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBinaryLicense(byte[] value) {
        this.binaryLicense = value;
    }

    /**
     * Gets the value of the licenseFiles property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public LicenseFileDataListType getLicenseFiles() {
        return licenseFiles;
    }

    /**
     * Sets the value of the licenseFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public void setLicenseFiles(LicenseFileDataListType value) {
        this.licenseFiles = value;
    }

}
