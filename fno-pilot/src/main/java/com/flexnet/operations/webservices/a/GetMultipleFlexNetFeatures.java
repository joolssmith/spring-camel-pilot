
package com.flexnet.operations.webservices.a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OwnerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FeatureLimits" type="{http://producersuite.flexerasoftware.com/EntitlementService/}FeatureLimit" maxOccurs="unbounded"/&gt;
 *         &lt;element name="RightsId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "producerId",
    "ownerId",
    "featureLimits",
    "rightsId"
})
@XmlRootElement(name = "getMultipleFlexNetFeatures")
public class GetMultipleFlexNetFeatures {

    @XmlElement(name = "ProducerId", required = true)
    protected String producerId;
    @XmlElement(name = "OwnerId", required = true)
    protected String ownerId;
    @XmlElement(name = "FeatureLimits", required = true)
    protected List<FeatureLimit> featureLimits;
    @XmlElement(name = "RightsId", required = true)
    protected List<String> rightsId;

    /**
     * Gets the value of the producerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerId() {
        return producerId;
    }

    /**
     * Sets the value of the producerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerId(String value) {
        this.producerId = value;
    }

    /**
     * Gets the value of the ownerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * Sets the value of the ownerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerId(String value) {
        this.ownerId = value;
    }

    /**
     * Gets the value of the featureLimits property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featureLimits property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureLimits().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureLimit }
     * 
     * 
     */
    public List<FeatureLimit> getFeatureLimits() {
        if (featureLimits == null) {
            featureLimits = new ArrayList<FeatureLimit>();
        }
        return this.featureLimits;
    }

    /**
     * Gets the value of the rightsId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rightsId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRightsId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getRightsId() {
        if (rightsId == null) {
            rightsId = new ArrayList<String>();
        }
        return this.rightsId;
    }

}
