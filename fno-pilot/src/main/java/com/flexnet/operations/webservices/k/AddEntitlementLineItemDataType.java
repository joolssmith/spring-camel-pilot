
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addEntitlementLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addEntitlementLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementIdentifierType"/&gt;
 *         &lt;element name="lineItems" type="{urn:com.macrovision:flexnet/operations}createEntitlementLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItems" type="{urn:com.macrovision:flexnet/operations}createMaintenanceLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="autoDeploy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEntitlementLineItemDataType", propOrder = {
    "entitlementIdentifier",
    "lineItems",
    "maintenanceLineItems",
    "autoDeploy"
})
public class AddEntitlementLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementIdentifier;
    protected List<CreateEntitlementLineItemDataType> lineItems;
    protected List<CreateMaintenanceLineItemDataType> maintenanceLineItems;
    protected Boolean autoDeploy;

    /**
     * Gets the value of the entitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }

    /**
     * Sets the value of the entitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementIdentifier(EntitlementIdentifierType value) {
        this.entitlementIdentifier = value;
    }

    /**
     * Gets the value of the lineItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateEntitlementLineItemDataType }
     * 
     * 
     */
    public List<CreateEntitlementLineItemDataType> getLineItems() {
        if (lineItems == null) {
            lineItems = new ArrayList<CreateEntitlementLineItemDataType>();
        }
        return this.lineItems;
    }

    /**
     * Gets the value of the maintenanceLineItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maintenanceLineItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaintenanceLineItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreateMaintenanceLineItemDataType }
     * 
     * 
     */
    public List<CreateMaintenanceLineItemDataType> getMaintenanceLineItems() {
        if (maintenanceLineItems == null) {
            maintenanceLineItems = new ArrayList<CreateMaintenanceLineItemDataType>();
        }
        return this.maintenanceLineItems;
    }

    /**
     * Gets the value of the autoDeploy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoDeploy() {
        return autoDeploy;
    }

    /**
     * Sets the value of the autoDeploy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoDeploy(Boolean value) {
        this.autoDeploy = value;
    }

}
