
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LineItemsAndProblems" type="{http://producersuite.flexerasoftware.com/EntitlementService/}LineItemsAndProblems"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "lineItemsAndProblems"
})
@XmlRootElement(name = "findLineItemsForOwnerResponse")
public class FindLineItemsForOwnerResponse {

    @XmlElement(name = "LineItemsAndProblems", required = true)
    protected LineItemsAndProblems lineItemsAndProblems;

    /**
     * Gets the value of the lineItemsAndProblems property.
     * 
     * @return
     *     possible object is
     *     {@link LineItemsAndProblems }
     *     
     */
    public LineItemsAndProblems getLineItemsAndProblems() {
        return lineItemsAndProblems;
    }

    /**
     * Sets the value of the lineItemsAndProblems property.
     * 
     * @param value
     *     allowed object is
     *     {@link LineItemsAndProblems }
     *     
     */
    public void setLineItemsAndProblems(LineItemsAndProblems value) {
        this.lineItemsAndProblems = value;
    }

}
