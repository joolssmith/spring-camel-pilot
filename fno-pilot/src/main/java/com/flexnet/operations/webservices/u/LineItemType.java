
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LineItemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="LineItemType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="UPGRADE"/&gt;
 *     &lt;enumeration value="UPSELL"/&gt;
 *     &lt;enumeration value="RENEWAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "LineItemType")
@XmlEnum
public enum LineItemType {

    UPGRADE,
    UPSELL,
    RENEWAL;

    public String value() {
        return name();
    }

    public static LineItemType fromValue(String v) {
        return valueOf(v);
    }

}
