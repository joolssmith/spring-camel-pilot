
package com.flexnet.operations.webservices.n;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for fulfillmentPropertiesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fulfillmentPropertiesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentId" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillmentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v1.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="lineitemId" type="{urn:v1.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{urn:v1.webservices.operations.flexnet.com}productIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="productDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v1.webservices.operations.flexnet.com}partNumberIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumberDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v1.webservices.operations.flexnet.com}licenseTechnologyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModel" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v1.webservices.operations.flexnet.com}accountIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="soldToDisplayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseHost" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fulfilledCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="overDraftCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="fulfillDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="fulfillDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="licenseFileType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="binaryLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="supportAction" type="{urn:v1.webservices.operations.flexnet.com}SupportLicenseType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="parentFulfillmentId" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillmentSource" type="{urn:v1.webservices.operations.flexnet.com}FulfillmentSourceType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lineitemDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="totalCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="numberOfRemainingCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="isTrusted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="customHostAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="migrationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vendorDaemonName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseFiles" type="{urn:v1.webservices.operations.flexnet.com}licenseFileDataListType" minOccurs="0"/&gt;
 *         &lt;element name="entitledProducts" type="{urn:v1.webservices.operations.flexnet.com}entitledProductDataListType" minOccurs="0"/&gt;
 *         &lt;element name="FNPTimeZoneValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="activationType" type="{urn:v1.webservices.operations.flexnet.com}ActivationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fulfillmentPropertiesType", propOrder = {
    "fulfillmentId",
    "fulfillmentType",
    "state",
    "entitlementId",
    "lineitemId",
    "product",
    "productDescription",
    "partNumber",
    "partNumberDescription",
    "licenseTechnology",
    "licenseModel",
    "soldTo",
    "soldToDisplayName",
    "shipToEmail",
    "shipToAddress",
    "licenseHost",
    "fulfilledCount",
    "overDraftCount",
    "fulfillDate",
    "fulfillDateTime",
    "isPermanent",
    "startDate",
    "expirationDate",
    "versionDate",
    "licenseFileType",
    "licenseText",
    "binaryLicense",
    "supportAction",
    "lastModifiedDateTime",
    "parentFulfillmentId",
    "fulfillmentSource",
    "orderId",
    "orderLineNumber",
    "lineitemDescription",
    "totalCopies",
    "numberOfRemainingCopies",
    "isTrusted",
    "customAttributes",
    "customHostAttributes",
    "migrationId",
    "vendorDaemonName",
    "licenseFiles",
    "entitledProducts",
    "fnpTimeZoneValue",
    "activationType"
})
public class FulfillmentPropertiesType {

    protected FulfillmentIdentifierType fulfillmentId;
    protected String fulfillmentType;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    protected EntitlementIdentifierType entitlementId;
    protected EntitlementLineItemIdentifierType lineitemId;
    protected ProductIdentifierType product;
    protected String productDescription;
    protected PartNumberIdentifierType partNumber;
    protected String partNumberDescription;
    protected LicenseTechnologyIdentifierType licenseTechnology;
    protected LicenseModelIdentifierType licenseModel;
    protected AccountIdentifierType soldTo;
    protected String soldToDisplayName;
    protected String shipToEmail;
    protected String shipToAddress;
    protected String licenseHost;
    protected BigInteger fulfilledCount;
    protected BigInteger overDraftCount;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fulfillDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fulfillDateTime;
    protected Boolean isPermanent;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar versionDate;
    protected String licenseFileType;
    protected String licenseText;
    protected byte[] binaryLicense;
    @XmlSchemaType(name = "NMTOKEN")
    protected SupportLicenseType supportAction;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    protected FulfillmentIdentifierType parentFulfillmentId;
    @XmlSchemaType(name = "NMTOKEN")
    protected FulfillmentSourceType fulfillmentSource;
    protected String orderId;
    protected String orderLineNumber;
    protected String lineitemDescription;
    protected BigInteger totalCopies;
    protected BigInteger numberOfRemainingCopies;
    protected Boolean isTrusted;
    protected AttributeDescriptorDataType customAttributes;
    protected AttributeDescriptorDataType customHostAttributes;
    protected String migrationId;
    protected String vendorDaemonName;
    protected LicenseFileDataListType licenseFiles;
    protected EntitledProductDataListType entitledProducts;
    @XmlElement(name = "FNPTimeZoneValue")
    protected String fnpTimeZoneValue;
    @XmlSchemaType(name = "NMTOKEN")
    protected ActivationType activationType;

    /**
     * Gets the value of the fulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getFulfillmentId() {
        return fulfillmentId;
    }

    /**
     * Sets the value of the fulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setFulfillmentId(FulfillmentIdentifierType value) {
        this.fulfillmentId = value;
    }

    /**
     * Gets the value of the fulfillmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * Sets the value of the fulfillmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentType(String value) {
        this.fulfillmentType = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementId(EntitlementIdentifierType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the lineitemId property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getLineitemId() {
        return lineitemId;
    }

    /**
     * Sets the value of the lineitemId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setLineitemId(EntitlementLineItemIdentifierType value) {
        this.lineitemId = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setProduct(ProductIdentifierType value) {
        this.product = value;
    }

    /**
     * Gets the value of the productDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * Sets the value of the productDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductDescription(String value) {
        this.productDescription = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setPartNumber(PartNumberIdentifierType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the partNumberDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartNumberDescription() {
        return partNumberDescription;
    }

    /**
     * Sets the value of the partNumberDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartNumberDescription(String value) {
        this.partNumberDescription = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the licenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModel() {
        return licenseModel;
    }

    /**
     * Sets the value of the licenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModel(LicenseModelIdentifierType value) {
        this.licenseModel = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifierType }
     *     
     */
    public AccountIdentifierType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifierType }
     *     
     */
    public void setSoldTo(AccountIdentifierType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the soldToDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldToDisplayName() {
        return soldToDisplayName;
    }

    /**
     * Sets the value of the soldToDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldToDisplayName(String value) {
        this.soldToDisplayName = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToEmail(String value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddress(String value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the licenseHost property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseHost() {
        return licenseHost;
    }

    /**
     * Sets the value of the licenseHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseHost(String value) {
        this.licenseHost = value;
    }

    /**
     * Gets the value of the fulfilledCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFulfilledCount() {
        return fulfilledCount;
    }

    /**
     * Sets the value of the fulfilledCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFulfilledCount(BigInteger value) {
        this.fulfilledCount = value;
    }

    /**
     * Gets the value of the overDraftCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOverDraftCount() {
        return overDraftCount;
    }

    /**
     * Sets the value of the overDraftCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOverDraftCount(BigInteger value) {
        this.overDraftCount = value;
    }

    /**
     * Gets the value of the fulfillDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFulfillDate() {
        return fulfillDate;
    }

    /**
     * Sets the value of the fulfillDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFulfillDate(XMLGregorianCalendar value) {
        this.fulfillDate = value;
    }

    /**
     * Gets the value of the fulfillDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFulfillDateTime() {
        return fulfillDateTime;
    }

    /**
     * Sets the value of the fulfillDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFulfillDateTime(XMLGregorianCalendar value) {
        this.fulfillDateTime = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersionDate(XMLGregorianCalendar value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the licenseFileType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseFileType() {
        return licenseFileType;
    }

    /**
     * Sets the value of the licenseFileType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseFileType(String value) {
        this.licenseFileType = value;
    }

    /**
     * Gets the value of the licenseText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseText() {
        return licenseText;
    }

    /**
     * Sets the value of the licenseText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseText(String value) {
        this.licenseText = value;
    }

    /**
     * Gets the value of the binaryLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }

    /**
     * Sets the value of the binaryLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBinaryLicense(byte[] value) {
        this.binaryLicense = value;
    }

    /**
     * Gets the value of the supportAction property.
     * 
     * @return
     *     possible object is
     *     {@link SupportLicenseType }
     *     
     */
    public SupportLicenseType getSupportAction() {
        return supportAction;
    }

    /**
     * Sets the value of the supportAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupportLicenseType }
     *     
     */
    public void setSupportAction(SupportLicenseType value) {
        this.supportAction = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the parentFulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getParentFulfillmentId() {
        return parentFulfillmentId;
    }

    /**
     * Sets the value of the parentFulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setParentFulfillmentId(FulfillmentIdentifierType value) {
        this.parentFulfillmentId = value;
    }

    /**
     * Gets the value of the fulfillmentSource property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }

    /**
     * Sets the value of the fulfillmentSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public void setFulfillmentSource(FulfillmentSourceType value) {
        this.fulfillmentSource = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineNumber(String value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the lineitemDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineitemDescription() {
        return lineitemDescription;
    }

    /**
     * Sets the value of the lineitemDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineitemDescription(String value) {
        this.lineitemDescription = value;
    }

    /**
     * Gets the value of the totalCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalCopies() {
        return totalCopies;
    }

    /**
     * Sets the value of the totalCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalCopies(BigInteger value) {
        this.totalCopies = value;
    }

    /**
     * Gets the value of the numberOfRemainingCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfRemainingCopies() {
        return numberOfRemainingCopies;
    }

    /**
     * Sets the value of the numberOfRemainingCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfRemainingCopies(BigInteger value) {
        this.numberOfRemainingCopies = value;
    }

    /**
     * Gets the value of the isTrusted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTrusted() {
        return isTrusted;
    }

    /**
     * Sets the value of the isTrusted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTrusted(Boolean value) {
        this.isTrusted = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the customHostAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomHostAttributes() {
        return customHostAttributes;
    }

    /**
     * Sets the value of the customHostAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomHostAttributes(AttributeDescriptorDataType value) {
        this.customHostAttributes = value;
    }

    /**
     * Gets the value of the migrationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMigrationId() {
        return migrationId;
    }

    /**
     * Sets the value of the migrationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMigrationId(String value) {
        this.migrationId = value;
    }

    /**
     * Gets the value of the vendorDaemonName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorDaemonName() {
        return vendorDaemonName;
    }

    /**
     * Sets the value of the vendorDaemonName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorDaemonName(String value) {
        this.vendorDaemonName = value;
    }

    /**
     * Gets the value of the licenseFiles property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public LicenseFileDataListType getLicenseFiles() {
        return licenseFiles;
    }

    /**
     * Sets the value of the licenseFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public void setLicenseFiles(LicenseFileDataListType value) {
        this.licenseFiles = value;
    }

    /**
     * Gets the value of the entitledProducts property.
     * 
     * @return
     *     possible object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public EntitledProductDataListType getEntitledProducts() {
        return entitledProducts;
    }

    /**
     * Sets the value of the entitledProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public void setEntitledProducts(EntitledProductDataListType value) {
        this.entitledProducts = value;
    }

    /**
     * Gets the value of the fnpTimeZoneValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNPTimeZoneValue() {
        return fnpTimeZoneValue;
    }

    /**
     * Sets the value of the fnpTimeZoneValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNPTimeZoneValue(String value) {
        this.fnpTimeZoneValue = value;
    }

    /**
     * Gets the value of the activationType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivationType }
     *     
     */
    public ActivationType getActivationType() {
        return activationType;
    }

    /**
     * Sets the value of the activationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivationType }
     *     
     */
    public void setActivationType(ActivationType value) {
        this.activationType = value;
    }

}
