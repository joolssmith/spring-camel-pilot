
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedDeleteMaintenanceDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedDeleteMaintenanceDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="maintenanceIdentifier" type="{urn:v1.webservices.operations.flexnet.com}maintenanceIdentifierType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedDeleteMaintenanceDataType", propOrder = {
    "maintenanceIdentifier",
    "reason"
})
public class FailedDeleteMaintenanceDataType {

    @XmlElement(required = true)
    protected MaintenanceIdentifierType maintenanceIdentifier;
    protected String reason;

    /**
     * Gets the value of the maintenanceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public MaintenanceIdentifierType getMaintenanceIdentifier() {
        return maintenanceIdentifier;
    }

    /**
     * Sets the value of the maintenanceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public void setMaintenanceIdentifier(MaintenanceIdentifierType value) {
        this.maintenanceIdentifier = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
