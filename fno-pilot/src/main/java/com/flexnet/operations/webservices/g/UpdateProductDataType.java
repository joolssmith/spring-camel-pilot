
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateProductDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateProductDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productIdentifier" type="{urn:com.macrovision:flexnet/operations}productIdentifierType"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseGenerator" type="{urn:com.macrovision:flexnet/operations}licenseGeneratorIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="packageProperties" type="{urn:com.macrovision:flexnet/operations}packagePropertiesDataType" minOccurs="0"/&gt;
 *         &lt;element name="features" type="{urn:com.macrovision:flexnet/operations}updateFeaturesListType" minOccurs="0"/&gt;
 *         &lt;element name="featureBundles" type="{urn:com.macrovision:flexnet/operations}updateFeatureBundlesListType" minOccurs="0"/&gt;
 *         &lt;element name="partNumbers" type="{urn:com.macrovision:flexnet/operations}updatePartNumbersListType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModels" type="{urn:com.macrovision:flexnet/operations}updateLicenseModelsListType" minOccurs="0"/&gt;
 *         &lt;element name="trustedKey" type="{urn:com.macrovision:flexnet/operations}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="virtualTrustedKey" type="{urn:com.macrovision:flexnet/operations}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="hostType" type="{urn:com.macrovision:flexnet/operations}hostTypePKType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypes" type="{urn:com.macrovision:flexnet/operations}updateHostTypeListType" minOccurs="0"/&gt;
 *         &lt;element name="usedOnDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:com.macrovision:flexnet/operations}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInAdmin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productAttributes" type="{urn:com.macrovision:flexnet/operations}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateProductDataType", propOrder = {
    "productIdentifier",
    "name",
    "version",
    "description",
    "licenseGenerator",
    "packageProperties",
    "features",
    "featureBundles",
    "partNumbers",
    "licenseModels",
    "trustedKey",
    "virtualTrustedKey",
    "hostType",
    "hostTypes",
    "usedOnDevice",
    "productCategory",
    "customAttributes",
    "allowDownloadObsoleteFrInAdmin",
    "allowDownloadObsoleteFrInPortal",
    "productAttributes"
})
public class UpdateProductDataType {

    @XmlElement(required = true)
    protected ProductIdentifierType productIdentifier;
    protected String name;
    protected String version;
    protected String description;
    protected LicenseGeneratorIdentifierType licenseGenerator;
    protected PackagePropertiesDataType packageProperties;
    protected UpdateFeaturesListType features;
    protected UpdateFeatureBundlesListType featureBundles;
    protected UpdatePartNumbersListType partNumbers;
    protected UpdateLicenseModelsListType licenseModels;
    protected TrustedKeyIdentifierType trustedKey;
    protected TrustedKeyIdentifierType virtualTrustedKey;
    protected HostTypePKType hostType;
    protected UpdateHostTypeListType hostTypes;
    protected Boolean usedOnDevice;
    protected String productCategory;
    protected AttributeDescriptorDataType customAttributes;
    protected Boolean allowDownloadObsoleteFrInAdmin;
    protected Boolean allowDownloadObsoleteFrInPortal;
    protected AttributeDescriptorDataType productAttributes;

    /**
     * Gets the value of the productIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getProductIdentifier() {
        return productIdentifier;
    }

    /**
     * Sets the value of the productIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setProductIdentifier(ProductIdentifierType value) {
        this.productIdentifier = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the licenseGenerator property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public LicenseGeneratorIdentifierType getLicenseGenerator() {
        return licenseGenerator;
    }

    /**
     * Sets the value of the licenseGenerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public void setLicenseGenerator(LicenseGeneratorIdentifierType value) {
        this.licenseGenerator = value;
    }

    /**
     * Gets the value of the packageProperties property.
     * 
     * @return
     *     possible object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public PackagePropertiesDataType getPackageProperties() {
        return packageProperties;
    }

    /**
     * Sets the value of the packageProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public void setPackageProperties(PackagePropertiesDataType value) {
        this.packageProperties = value;
    }

    /**
     * Gets the value of the features property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateFeaturesListType }
     *     
     */
    public UpdateFeaturesListType getFeatures() {
        return features;
    }

    /**
     * Sets the value of the features property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateFeaturesListType }
     *     
     */
    public void setFeatures(UpdateFeaturesListType value) {
        this.features = value;
    }

    /**
     * Gets the value of the featureBundles property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateFeatureBundlesListType }
     *     
     */
    public UpdateFeatureBundlesListType getFeatureBundles() {
        return featureBundles;
    }

    /**
     * Sets the value of the featureBundles property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateFeatureBundlesListType }
     *     
     */
    public void setFeatureBundles(UpdateFeatureBundlesListType value) {
        this.featureBundles = value;
    }

    /**
     * Gets the value of the partNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link UpdatePartNumbersListType }
     *     
     */
    public UpdatePartNumbersListType getPartNumbers() {
        return partNumbers;
    }

    /**
     * Sets the value of the partNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdatePartNumbersListType }
     *     
     */
    public void setPartNumbers(UpdatePartNumbersListType value) {
        this.partNumbers = value;
    }

    /**
     * Gets the value of the licenseModels property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateLicenseModelsListType }
     *     
     */
    public UpdateLicenseModelsListType getLicenseModels() {
        return licenseModels;
    }

    /**
     * Sets the value of the licenseModels property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateLicenseModelsListType }
     *     
     */
    public void setLicenseModels(UpdateLicenseModelsListType value) {
        this.licenseModels = value;
    }

    /**
     * Gets the value of the trustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getTrustedKey() {
        return trustedKey;
    }

    /**
     * Sets the value of the trustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setTrustedKey(TrustedKeyIdentifierType value) {
        this.trustedKey = value;
    }

    /**
     * Gets the value of the virtualTrustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getVirtualTrustedKey() {
        return virtualTrustedKey;
    }

    /**
     * Sets the value of the virtualTrustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setVirtualTrustedKey(TrustedKeyIdentifierType value) {
        this.virtualTrustedKey = value;
    }

    /**
     * Gets the value of the hostType property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypePKType }
     *     
     */
    public HostTypePKType getHostType() {
        return hostType;
    }

    /**
     * Sets the value of the hostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypePKType }
     *     
     */
    public void setHostType(HostTypePKType value) {
        this.hostType = value;
    }

    /**
     * Gets the value of the hostTypes property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateHostTypeListType }
     *     
     */
    public UpdateHostTypeListType getHostTypes() {
        return hostTypes;
    }

    /**
     * Sets the value of the hostTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateHostTypeListType }
     *     
     */
    public void setHostTypes(UpdateHostTypeListType value) {
        this.hostTypes = value;
    }

    /**
     * Gets the value of the usedOnDevice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsedOnDevice() {
        return usedOnDevice;
    }

    /**
     * Sets the value of the usedOnDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsedOnDevice(Boolean value) {
        this.usedOnDevice = value;
    }

    /**
     * Gets the value of the productCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategory() {
        return productCategory;
    }

    /**
     * Sets the value of the productCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategory(String value) {
        this.productCategory = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInAdmin() {
        return allowDownloadObsoleteFrInAdmin;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInAdmin(Boolean value) {
        this.allowDownloadObsoleteFrInAdmin = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInPortal() {
        return allowDownloadObsoleteFrInPortal;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInPortal(Boolean value) {
        this.allowDownloadObsoleteFrInPortal = value;
    }

    /**
     * Gets the value of the productAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getProductAttributes() {
        return productAttributes;
    }

    /**
     * Sets the value of the productAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setProductAttributes(AttributeDescriptorDataType value) {
        this.productAttributes = value;
    }

}
