
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for entitlementMaintenanceLineItemPropertiesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entitlementMaintenanceLineItemPropertiesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v3.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{urn:v3.webservices.operations.flexnet.com}entitlementIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceProduct" type="{urn:v3.webservices.operations.flexnet.com}productIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceProductDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="maintenancePartNumber" type="{urn:v3.webservices.operations.flexnet.com}partNumberIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="maintenancePartNumberDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="createdOnDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="linkedLineItemActivationId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItemAttributes" type="{urn:v3.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entitlementMaintenanceLineItemPropertiesType", propOrder = {
    "activationId",
    "state",
    "orderId",
    "orderLineNumber",
    "entitlementId",
    "maintenanceProduct",
    "maintenanceProductDescription",
    "maintenancePartNumber",
    "maintenancePartNumberDescription",
    "startDate",
    "isPermanent",
    "expirationDate",
    "createdOnDateTime",
    "lastModifiedDateTime",
    "linkedLineItemActivationId",
    "maintenanceLineItemAttributes"
})
public class EntitlementMaintenanceLineItemPropertiesType {

    protected EntitlementLineItemIdentifierType activationId;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    protected String orderId;
    protected String orderLineNumber;
    protected EntitlementIdentifierType entitlementId;
    protected ProductIdentifierType maintenanceProduct;
    protected String maintenanceProductDescription;
    protected PartNumberIdentifierType maintenancePartNumber;
    protected String maintenancePartNumberDescription;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    protected Boolean isPermanent;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar createdOnDateTime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    protected List<String> linkedLineItemActivationId;
    protected AttributeDescriptorDataType maintenanceLineItemAttributes;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setActivationId(EntitlementLineItemIdentifierType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineNumber(String value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementId(EntitlementIdentifierType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the maintenanceProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getMaintenanceProduct() {
        return maintenanceProduct;
    }

    /**
     * Sets the value of the maintenanceProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setMaintenanceProduct(ProductIdentifierType value) {
        this.maintenanceProduct = value;
    }

    /**
     * Gets the value of the maintenanceProductDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaintenanceProductDescription() {
        return maintenanceProductDescription;
    }

    /**
     * Sets the value of the maintenanceProductDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaintenanceProductDescription(String value) {
        this.maintenanceProductDescription = value;
    }

    /**
     * Gets the value of the maintenancePartNumber property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getMaintenancePartNumber() {
        return maintenancePartNumber;
    }

    /**
     * Sets the value of the maintenancePartNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setMaintenancePartNumber(PartNumberIdentifierType value) {
        this.maintenancePartNumber = value;
    }

    /**
     * Gets the value of the maintenancePartNumberDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaintenancePartNumberDescription() {
        return maintenancePartNumberDescription;
    }

    /**
     * Sets the value of the maintenancePartNumberDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaintenancePartNumberDescription(String value) {
        this.maintenancePartNumberDescription = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the createdOnDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreatedOnDateTime() {
        return createdOnDateTime;
    }

    /**
     * Sets the value of the createdOnDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreatedOnDateTime(XMLGregorianCalendar value) {
        this.createdOnDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the linkedLineItemActivationId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkedLineItemActivationId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkedLineItemActivationId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getLinkedLineItemActivationId() {
        if (linkedLineItemActivationId == null) {
            linkedLineItemActivationId = new ArrayList<String>();
        }
        return this.linkedLineItemActivationId;
    }

    /**
     * Gets the value of the maintenanceLineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getMaintenanceLineItemAttributes() {
        return maintenanceLineItemAttributes;
    }

    /**
     * Sets the value of the maintenanceLineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setMaintenanceLineItemAttributes(AttributeDescriptorDataType value) {
        this.maintenanceLineItemAttributes = value;
    }

}
