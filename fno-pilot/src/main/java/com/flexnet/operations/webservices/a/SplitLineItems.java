
package com.flexnet.operations.webservices.a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="SplitLineItem" type="{http://producersuite.flexerasoftware.com/EntitlementService/}SplitLineItem" maxOccurs="unbounded"/&gt;
 *         &lt;element name="SourceEnterpriseID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="TargetEnterpriseID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "producerId",
    "splitLineItem",
    "sourceEnterpriseID",
    "targetEnterpriseID"
})
@XmlRootElement(name = "splitLineItems")
public class SplitLineItems {

    @XmlElement(name = "ProducerId", required = true)
    protected String producerId;
    @XmlElement(name = "SplitLineItem", required = true)
    protected List<SplitLineItem> splitLineItem;
    @XmlElement(name = "SourceEnterpriseID", required = true)
    protected String sourceEnterpriseID;
    @XmlElement(name = "TargetEnterpriseID", required = true)
    protected String targetEnterpriseID;

    /**
     * Gets the value of the producerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerId() {
        return producerId;
    }

    /**
     * Sets the value of the producerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerId(String value) {
        this.producerId = value;
    }

    /**
     * Gets the value of the splitLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the splitLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSplitLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SplitLineItem }
     * 
     * 
     */
    public List<SplitLineItem> getSplitLineItem() {
        if (splitLineItem == null) {
            splitLineItem = new ArrayList<SplitLineItem>();
        }
        return this.splitLineItem;
    }

    /**
     * Gets the value of the sourceEnterpriseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceEnterpriseID() {
        return sourceEnterpriseID;
    }

    /**
     * Sets the value of the sourceEnterpriseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceEnterpriseID(String value) {
        this.sourceEnterpriseID = value;
    }

    /**
     * Gets the value of the targetEnterpriseID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetEnterpriseID() {
        return targetEnterpriseID;
    }

    /**
     * Sets the value of the targetEnterpriseID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetEnterpriseID(String value) {
        this.targetEnterpriseID = value;
    }

}
