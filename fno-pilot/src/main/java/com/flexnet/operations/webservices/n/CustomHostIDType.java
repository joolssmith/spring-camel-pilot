
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomHostIDType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomHostIDType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hostId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hostAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="hostType" type="{urn:v1.webservices.operations.flexnet.com}hostTypePKType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomHostIDType", propOrder = {
    "hostId",
    "hostAttributes",
    "hostType"
})
public class CustomHostIDType {

    protected String hostId;
    protected AttributeDescriptorDataType hostAttributes;
    protected HostTypePKType hostType;

    /**
     * Gets the value of the hostId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostId() {
        return hostId;
    }

    /**
     * Sets the value of the hostId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostId(String value) {
        this.hostId = value;
    }

    /**
     * Gets the value of the hostAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getHostAttributes() {
        return hostAttributes;
    }

    /**
     * Sets the value of the hostAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setHostAttributes(AttributeDescriptorDataType value) {
        this.hostAttributes = value;
    }

    /**
     * Gets the value of the hostType property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypePKType }
     *     
     */
    public HostTypePKType getHostType() {
        return hostType;
    }

    /**
     * Sets the value of the hostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypePKType }
     *     
     */
    public void setHostType(HostTypePKType value) {
        this.hostType = value;
    }

}
