
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceSortBy complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deviceSortBy"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sortKey" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceSortKey"/&gt;
 *         &lt;element name="ascending" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deviceSortBy", propOrder = {
    "sortKey",
    "ascending"
})
public class DeviceSortBy {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceSortKey sortKey;
    protected boolean ascending;

    /**
     * Gets the value of the sortKey property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceSortKey }
     *     
     */
    public DeviceSortKey getSortKey() {
        return sortKey;
    }

    /**
     * Sets the value of the sortKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceSortKey }
     *     
     */
    public void setSortKey(DeviceSortKey value) {
        this.sortKey = value;
    }

    /**
     * Gets the value of the ascending property.
     * 
     */
    public boolean isAscending() {
        return ascending;
    }

    /**
     * Sets the value of the ascending property.
     * 
     */
    public void setAscending(boolean value) {
        this.ascending = value;
    }

}
