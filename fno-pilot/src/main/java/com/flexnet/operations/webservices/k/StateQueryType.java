
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StateQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="StateQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="value" type="{urn:com.macrovision:flexnet/operations}StateType"/&gt;
 *         &lt;element name="searchType" type="{urn:com.macrovision:flexnet/operations}simpleSearchType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "StateQueryType", propOrder = {
    "value",
    "searchType"
})
public class StateQueryType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType value;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected SimpleSearchType searchType;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setValue(StateType value) {
        this.value = value;
    }

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleSearchType }
     *     
     */
    public SimpleSearchType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleSearchType }
     *     
     */
    public void setSearchType(SimpleSearchType value) {
        this.searchType = value;
    }

}
