
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for relationshipType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="relationshipType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="RELATION_UNDEFINED"/&gt;
 *     &lt;enumeration value="PRODUCTION_VERSION_OF"/&gt;
 *     &lt;enumeration value="DEMO_OF"/&gt;
 *     &lt;enumeration value="UPGRADE_FROM"/&gt;
 *     &lt;enumeration value="UPGRADE_TO"/&gt;
 *     &lt;enumeration value="UPSELL_FROM"/&gt;
 *     &lt;enumeration value="UPSELL_TO"/&gt;
 *     &lt;enumeration value="IS_MAINTENANCE"/&gt;
 *     &lt;enumeration value="HAS_MAINTENANCE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "relationshipType")
@XmlEnum
public enum RelationshipType {

    RELATION_UNDEFINED,
    PRODUCTION_VERSION_OF,
    DEMO_OF,
    UPGRADE_FROM,
    UPGRADE_TO,
    UPSELL_FROM,
    UPSELL_TO,
    IS_MAINTENANCE,
    HAS_MAINTENANCE;

    public String value() {
        return name();
    }

    public static RelationshipType fromValue(String v) {
        return valueOf(v);
    }

}
