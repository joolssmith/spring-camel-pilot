
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for renewedEntitlementLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="renewedEntitlementLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemRecordRefNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="parentLineItemIdentifier" type="{urn:v2.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="newLineItem" type="{urn:v2.webservices.operations.flexnet.com}newEntitlementLineItemDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "renewedEntitlementLineItemDataType", propOrder = {
    "lineItemRecordRefNo",
    "parentLineItemIdentifier",
    "newLineItem"
})
public class RenewedEntitlementLineItemDataType {

    @XmlElement(required = true)
    protected String lineItemRecordRefNo;
    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType parentLineItemIdentifier;
    @XmlElement(required = true)
    protected NewEntitlementLineItemDataType newLineItem;

    /**
     * Gets the value of the lineItemRecordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLineItemRecordRefNo() {
        return lineItemRecordRefNo;
    }

    /**
     * Sets the value of the lineItemRecordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLineItemRecordRefNo(String value) {
        this.lineItemRecordRefNo = value;
    }

    /**
     * Gets the value of the parentLineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getParentLineItemIdentifier() {
        return parentLineItemIdentifier;
    }

    /**
     * Sets the value of the parentLineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setParentLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.parentLineItemIdentifier = value;
    }

    /**
     * Gets the value of the newLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link NewEntitlementLineItemDataType }
     *     
     */
    public NewEntitlementLineItemDataType getNewLineItem() {
        return newLineItem;
    }

    /**
     * Sets the value of the newLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link NewEntitlementLineItemDataType }
     *     
     */
    public void setNewLineItem(NewEntitlementLineItemDataType value) {
        this.newLineItem = value;
    }

}
