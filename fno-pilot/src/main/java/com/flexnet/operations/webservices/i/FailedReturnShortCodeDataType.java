
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedReturnShortCodeDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedReturnShortCodeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shortCodeData" type="{urn:com.macrovision:flexnet/operations}returnShortCodeDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedReturnShortCodeDataType", propOrder = {
    "shortCodeData",
    "reason"
})
public class FailedReturnShortCodeDataType {

    @XmlElement(required = true)
    protected ReturnShortCodeDataType shortCodeData;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the shortCodeData property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnShortCodeDataType }
     *     
     */
    public ReturnShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }

    /**
     * Sets the value of the shortCodeData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnShortCodeDataType }
     *     
     */
    public void setShortCodeData(ReturnShortCodeDataType value) {
        this.shortCodeData = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
