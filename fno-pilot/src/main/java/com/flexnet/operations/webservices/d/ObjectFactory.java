
package com.flexnet.operations.webservices.d;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.d package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _UserTokenInput_QNAME = new QName("urn:com.macrovision:flexnet/platform", "UserTokenInput");
    private final static QName _UserTokenReturn_QNAME = new QName("urn:com.macrovision:flexnet/platform", "UserTokenReturn");
    private final static QName _SecureTokenRequest_QNAME = new QName("urn:com.macrovision:flexnet/platform", "secureTokenRequest");
    private final static QName _SecureTokenResponse_QNAME = new QName("urn:com.macrovision:flexnet/platform", "secureTokenResponse");
    private final static QName _AuthenticateUserInput_QNAME = new QName("urn:com.macrovision:flexnet/platform", "AuthenticateUserInput");
    private final static QName _AuthenticateUserReturn_QNAME = new QName("urn:com.macrovision:flexnet/platform", "AuthenticateUserReturn");
    private final static QName _ValidateTokenRequest_QNAME = new QName("urn:com.macrovision:flexnet/platform", "validateTokenRequest");
    private final static QName _ValidateTokenResponse_QNAME = new QName("urn:com.macrovision:flexnet/platform", "validateTokenResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.d
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UserTokenReturnType }
     * 
     */
    public UserTokenReturnType createUserTokenReturnType() {
        return new UserTokenReturnType();
    }

    /**
     * Create an instance of {@link IdentityType }
     * 
     */
    public IdentityType createIdentityType() {
        return new IdentityType();
    }

    /**
     * Create an instance of {@link TokenResponseType }
     * 
     */
    public TokenResponseType createTokenResponseType() {
        return new TokenResponseType();
    }

    /**
     * Create an instance of {@link AuthenticateUserInputType }
     * 
     */
    public AuthenticateUserInputType createAuthenticateUserInputType() {
        return new AuthenticateUserInputType();
    }

    /**
     * Create an instance of {@link AuthenticateUserReturnType }
     * 
     */
    public AuthenticateUserReturnType createAuthenticateUserReturnType() {
        return new AuthenticateUserReturnType();
    }

    /**
     * Create an instance of {@link TokenType }
     * 
     */
    public TokenType createTokenType() {
        return new TokenType();
    }

    /**
     * Create an instance of {@link StatusResponse }
     * 
     */
    public StatusResponse createStatusResponse() {
        return new StatusResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "UserTokenInput")
    public JAXBElement<String> createUserTokenInput(String value) {
        return new JAXBElement<String>(_UserTokenInput_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserTokenReturnType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "UserTokenReturn")
    public JAXBElement<UserTokenReturnType> createUserTokenReturn(UserTokenReturnType value) {
        return new JAXBElement<UserTokenReturnType>(_UserTokenReturn_QNAME, UserTokenReturnType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IdentityType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "secureTokenRequest")
    public JAXBElement<IdentityType> createSecureTokenRequest(IdentityType value) {
        return new JAXBElement<IdentityType>(_SecureTokenRequest_QNAME, IdentityType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "secureTokenResponse")
    public JAXBElement<TokenResponseType> createSecureTokenResponse(TokenResponseType value) {
        return new JAXBElement<TokenResponseType>(_SecureTokenResponse_QNAME, TokenResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticateUserInputType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "AuthenticateUserInput")
    public JAXBElement<AuthenticateUserInputType> createAuthenticateUserInput(AuthenticateUserInputType value) {
        return new JAXBElement<AuthenticateUserInputType>(_AuthenticateUserInput_QNAME, AuthenticateUserInputType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AuthenticateUserReturnType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "AuthenticateUserReturn")
    public JAXBElement<AuthenticateUserReturnType> createAuthenticateUserReturn(AuthenticateUserReturnType value) {
        return new JAXBElement<AuthenticateUserReturnType>(_AuthenticateUserReturn_QNAME, AuthenticateUserReturnType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TokenType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "validateTokenRequest")
    public JAXBElement<TokenType> createValidateTokenRequest(TokenType value) {
        return new JAXBElement<TokenType>(_ValidateTokenRequest_QNAME, TokenType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/platform", name = "validateTokenResponse")
    public JAXBElement<StatusResponse> createValidateTokenResponse(StatusResponse value) {
        return new JAXBElement<StatusResponse>(_ValidateTokenResponse_QNAME, StatusResponse.class, null, value);
    }

}
