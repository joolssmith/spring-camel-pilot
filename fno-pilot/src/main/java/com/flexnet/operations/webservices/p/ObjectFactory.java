
package com.flexnet.operations.webservices.p;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.p package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateDeviceRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "createDeviceRequest");
    private final static QName _CreateDeviceResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "createDeviceResponse");
    private final static QName _DeleteDeviceRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "deleteDeviceRequest");
    private final static QName _DeleteDeviceResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "deleteDeviceResponse");
    private final static QName _UpdateDeviceRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "updateDeviceRequest");
    private final static QName _UpdateDeviceResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "updateDeviceResponse");
    private final static QName _GeneratePrebuiltLicenseRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "generatePrebuiltLicenseRequest");
    private final static QName _GeneratePrebuiltLicenseResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "generatePrebuiltLicenseResponse");
    private final static QName _GetDevicesRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "getDevicesRequest");
    private final static QName _GetDevicesResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "getDevicesResponse");
    private final static QName _GetDeviceCountRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "getDeviceCountRequest");
    private final static QName _GetDeviceCountResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "getDeviceCountResponse");
    private final static QName _LinkAddonLineItemsRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "linkAddonLineItemsRequest");
    private final static QName _LinkAddonLineItemsResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "linkAddonLineItemsResponse");
    private final static QName _DeleteAddonLineItemsRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "deleteAddonLineItemsRequest");
    private final static QName _DeleteAddonLineItemsResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "deleteAddonLineItemsResponse");
    private final static QName _IncrementAddonLineItemsRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "incrementAddonLineItemsRequest");
    private final static QName _IncrementAddonLineItemsResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "incrementAddonLineItemsResponse");
    private final static QName _DecrementAddonLineItemsRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "decrementAddonLineItemsRequest");
    private final static QName _DecrementAddonLineItemsResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "decrementAddonLineItemsResponse");
    private final static QName _GenerateCapabilityResponseRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "generateCapabilityResponseRequest");
    private final static QName _GenerateCapabilityResponseResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "generateCapabilityResponseResponse");
    private final static QName _MoveDeviceRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "moveDeviceRequest");
    private final static QName _MoveDeviceResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "moveDeviceResponse");
    private final static QName _ReturnHostRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "returnHostRequest");
    private final static QName _ReturnHostResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "returnHostResponse");
    private final static QName _ObsoleteHostRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "obsoleteHostRequest");
    private final static QName _ObsoleteHostResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "obsoleteHostResponse");
    private final static QName _GetAutoProvisionedServerRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "getAutoProvisionedServerRequest");
    private final static QName _GetAutoProvisionedServerResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "getAutoProvisionedServerResponse");
    private final static QName _GenerateCloneDetectionReportRequest_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "generateCloneDetectionReportRequest");
    private final static QName _GenerateCloneDetectionReportResponse_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "generateCloneDetectionReportResponse");
    private final static QName _GenerateCloneDetectionReportRequestEnterpriseIds_QNAME = new QName("urn:v1.fne.webservices.operations.flexnet.com", "enterpriseIds");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.p
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CloneSuspect }
     * 
     */
    public CloneSuspect createCloneSuspect() {
        return new CloneSuspect();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportResponse }
     * 
     */
    public GenerateCloneDetectionReportResponse createGenerateCloneDetectionReportResponse() {
        return new GenerateCloneDetectionReportResponse();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportRequest }
     * 
     */
    public GenerateCloneDetectionReportRequest createGenerateCloneDetectionReportRequest() {
        return new GenerateCloneDetectionReportRequest();
    }

    /**
     * Create an instance of {@link CreateDevRequestType }
     * 
     */
    public CreateDevRequestType createCreateDevRequestType() {
        return new CreateDevRequestType();
    }

    /**
     * Create an instance of {@link CreateDevResponseType }
     * 
     */
    public CreateDevResponseType createCreateDevResponseType() {
        return new CreateDevResponseType();
    }

    /**
     * Create an instance of {@link DeleteDeviceRequestType }
     * 
     */
    public DeleteDeviceRequestType createDeleteDeviceRequestType() {
        return new DeleteDeviceRequestType();
    }

    /**
     * Create an instance of {@link DeleteDeviceResponseType }
     * 
     */
    public DeleteDeviceResponseType createDeleteDeviceResponseType() {
        return new DeleteDeviceResponseType();
    }

    /**
     * Create an instance of {@link UpdateDevRequestType }
     * 
     */
    public UpdateDevRequestType createUpdateDevRequestType() {
        return new UpdateDevRequestType();
    }

    /**
     * Create an instance of {@link UpdateDevResponseType }
     * 
     */
    public UpdateDevResponseType createUpdateDevResponseType() {
        return new UpdateDevResponseType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseRequestType }
     * 
     */
    public GeneratePrebuiltLicenseRequestType createGeneratePrebuiltLicenseRequestType() {
        return new GeneratePrebuiltLicenseRequestType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseResponseType }
     * 
     */
    public GeneratePrebuiltLicenseResponseType createGeneratePrebuiltLicenseResponseType() {
        return new GeneratePrebuiltLicenseResponseType();
    }

    /**
     * Create an instance of {@link GetDevicesRequestType }
     * 
     */
    public GetDevicesRequestType createGetDevicesRequestType() {
        return new GetDevicesRequestType();
    }

    /**
     * Create an instance of {@link GetDevicesResponseType }
     * 
     */
    public GetDevicesResponseType createGetDevicesResponseType() {
        return new GetDevicesResponseType();
    }

    /**
     * Create an instance of {@link GetDevicesCountRequestType }
     * 
     */
    public GetDevicesCountRequestType createGetDevicesCountRequestType() {
        return new GetDevicesCountRequestType();
    }

    /**
     * Create an instance of {@link GetDeviceCountResponseType }
     * 
     */
    public GetDeviceCountResponseType createGetDeviceCountResponseType() {
        return new GetDeviceCountResponseType();
    }

    /**
     * Create an instance of {@link LinkAddonLineItemsRequestType }
     * 
     */
    public LinkAddonLineItemsRequestType createLinkAddonLineItemsRequestType() {
        return new LinkAddonLineItemsRequestType();
    }

    /**
     * Create an instance of {@link LinkAddonLineItemsResponseType }
     * 
     */
    public LinkAddonLineItemsResponseType createLinkAddonLineItemsResponseType() {
        return new LinkAddonLineItemsResponseType();
    }

    /**
     * Create an instance of {@link DeleteAddonLineItemsRequestType }
     * 
     */
    public DeleteAddonLineItemsRequestType createDeleteAddonLineItemsRequestType() {
        return new DeleteAddonLineItemsRequestType();
    }

    /**
     * Create an instance of {@link DeleteAddonLineItemsResponseType }
     * 
     */
    public DeleteAddonLineItemsResponseType createDeleteAddonLineItemsResponseType() {
        return new DeleteAddonLineItemsResponseType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseRequestType }
     * 
     */
    public GenerateCapabilityResponseRequestType createGenerateCapabilityResponseRequestType() {
        return new GenerateCapabilityResponseRequestType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseResponseType }
     * 
     */
    public GenerateCapabilityResponseResponseType createGenerateCapabilityResponseResponseType() {
        return new GenerateCapabilityResponseResponseType();
    }

    /**
     * Create an instance of {@link MoveDeviceRequestType }
     * 
     */
    public MoveDeviceRequestType createMoveDeviceRequestType() {
        return new MoveDeviceRequestType();
    }

    /**
     * Create an instance of {@link MoveDeviceResponseType }
     * 
     */
    public MoveDeviceResponseType createMoveDeviceResponseType() {
        return new MoveDeviceResponseType();
    }

    /**
     * Create an instance of {@link ReturnHostRequestListType }
     * 
     */
    public ReturnHostRequestListType createReturnHostRequestListType() {
        return new ReturnHostRequestListType();
    }

    /**
     * Create an instance of {@link ReturnHostResponseType }
     * 
     */
    public ReturnHostResponseType createReturnHostResponseType() {
        return new ReturnHostResponseType();
    }

    /**
     * Create an instance of {@link ObsoleteHostRequestListType }
     * 
     */
    public ObsoleteHostRequestListType createObsoleteHostRequestListType() {
        return new ObsoleteHostRequestListType();
    }

    /**
     * Create an instance of {@link ObsoleteHostResponseType }
     * 
     */
    public ObsoleteHostResponseType createObsoleteHostResponseType() {
        return new ObsoleteHostResponseType();
    }

    /**
     * Create an instance of {@link GetAutoProvisionedServerRequest }
     * 
     */
    public GetAutoProvisionedServerRequest createGetAutoProvisionedServerRequest() {
        return new GetAutoProvisionedServerRequest();
    }

    /**
     * Create an instance of {@link GetAutoProvisionedServerResponse }
     * 
     */
    public GetAutoProvisionedServerResponse createGetAutoProvisionedServerResponse() {
        return new GetAutoProvisionedServerResponse();
    }

    /**
     * Create an instance of {@link ServerIdsType }
     * 
     */
    public ServerIdsType createServerIdsType() {
        return new ServerIdsType();
    }

    /**
     * Create an instance of {@link CreateDeviceIdentifier }
     * 
     */
    public CreateDeviceIdentifier createCreateDeviceIdentifier() {
        return new CreateDeviceIdentifier();
    }

    /**
     * Create an instance of {@link HostTypeIdentifier }
     * 
     */
    public HostTypeIdentifier createHostTypeIdentifier() {
        return new HostTypeIdentifier();
    }

    /**
     * Create an instance of {@link PublisherIdentifier }
     * 
     */
    public PublisherIdentifier createPublisherIdentifier() {
        return new PublisherIdentifier();
    }

    /**
     * Create an instance of {@link OrganizationPKType }
     * 
     */
    public OrganizationPKType createOrganizationPKType() {
        return new OrganizationPKType();
    }

    /**
     * Create an instance of {@link OrganizationIdentifierType }
     * 
     */
    public OrganizationIdentifierType createOrganizationIdentifierType() {
        return new OrganizationIdentifierType();
    }

    /**
     * Create an instance of {@link UserPKType }
     * 
     */
    public UserPKType createUserPKType() {
        return new UserPKType();
    }

    /**
     * Create an instance of {@link UserIdentifierType }
     * 
     */
    public UserIdentifierType createUserIdentifierType() {
        return new UserIdentifierType();
    }

    /**
     * Create an instance of {@link ChannelPartnerDataType }
     * 
     */
    public ChannelPartnerDataType createChannelPartnerDataType() {
        return new ChannelPartnerDataType();
    }

    /**
     * Create an instance of {@link ChannelPartnerDataListType }
     * 
     */
    public ChannelPartnerDataListType createChannelPartnerDataListType() {
        return new ChannelPartnerDataListType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorType }
     * 
     */
    public AttributeDescriptorType createAttributeDescriptorType() {
        return new AttributeDescriptorType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorDataType }
     * 
     */
    public AttributeDescriptorDataType createAttributeDescriptorDataType() {
        return new AttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link DeviceDataType }
     * 
     */
    public DeviceDataType createDeviceDataType() {
        return new DeviceDataType();
    }

    /**
     * Create an instance of {@link OpsEmbeddedStatusInfoType }
     * 
     */
    public OpsEmbeddedStatusInfoType createOpsEmbeddedStatusInfoType() {
        return new OpsEmbeddedStatusInfoType();
    }

    /**
     * Create an instance of {@link FailedCreateDeviceDataType }
     * 
     */
    public FailedCreateDeviceDataType createFailedCreateDeviceDataType() {
        return new FailedCreateDeviceDataType();
    }

    /**
     * Create an instance of {@link FailedCreateDevDataListType }
     * 
     */
    public FailedCreateDevDataListType createFailedCreateDevDataListType() {
        return new FailedCreateDevDataListType();
    }

    /**
     * Create an instance of {@link DeviceIdentifier }
     * 
     */
    public DeviceIdentifier createDeviceIdentifier() {
        return new DeviceIdentifier();
    }

    /**
     * Create an instance of {@link CreatedDeviceDataListType }
     * 
     */
    public CreatedDeviceDataListType createCreatedDeviceDataListType() {
        return new CreatedDeviceDataListType();
    }

    /**
     * Create an instance of {@link FailedDeleteDevDataType }
     * 
     */
    public FailedDeleteDevDataType createFailedDeleteDevDataType() {
        return new FailedDeleteDevDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteDevDataListType }
     * 
     */
    public FailedDeleteDevDataListType createFailedDeleteDevDataListType() {
        return new FailedDeleteDevDataListType();
    }

    /**
     * Create an instance of {@link UpdateChannelPartnerDataListType }
     * 
     */
    public UpdateChannelPartnerDataListType createUpdateChannelPartnerDataListType() {
        return new UpdateChannelPartnerDataListType();
    }

    /**
     * Create an instance of {@link UpdateDevDataType }
     * 
     */
    public UpdateDevDataType createUpdateDevDataType() {
        return new UpdateDevDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateDeviceDataType }
     * 
     */
    public FailedUpdateDeviceDataType createFailedUpdateDeviceDataType() {
        return new FailedUpdateDeviceDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateDevDataListType }
     * 
     */
    public FailedUpdateDevDataListType createFailedUpdateDevDataListType() {
        return new FailedUpdateDevDataListType();
    }

    /**
     * Create an instance of {@link UpdatedDeviceDataListType }
     * 
     */
    public UpdatedDeviceDataListType createUpdatedDeviceDataListType() {
        return new UpdatedDeviceDataListType();
    }

    /**
     * Create an instance of {@link ProductPKType }
     * 
     */
    public ProductPKType createProductPKType() {
        return new ProductPKType();
    }

    /**
     * Create an instance of {@link ProductIdentifierType }
     * 
     */
    public ProductIdentifierType createProductIdentifierType() {
        return new ProductIdentifierType();
    }

    /**
     * Create an instance of {@link LicenseModelPKType }
     * 
     */
    public LicenseModelPKType createLicenseModelPKType() {
        return new LicenseModelPKType();
    }

    /**
     * Create an instance of {@link LicenseModelIdentifierType }
     * 
     */
    public LicenseModelIdentifierType createLicenseModelIdentifierType() {
        return new LicenseModelIdentifierType();
    }

    /**
     * Create an instance of {@link DurationType }
     * 
     */
    public DurationType createDurationType() {
        return new DurationType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseDataType }
     * 
     */
    public GeneratePrebuiltLicenseDataType createGeneratePrebuiltLicenseDataType() {
        return new GeneratePrebuiltLicenseDataType();
    }

    /**
     * Create an instance of {@link FailedGeneratePrebuiltLicenseDataType }
     * 
     */
    public FailedGeneratePrebuiltLicenseDataType createFailedGeneratePrebuiltLicenseDataType() {
        return new FailedGeneratePrebuiltLicenseDataType();
    }

    /**
     * Create an instance of {@link FailedGeneratePrebuiltLicenseDataListType }
     * 
     */
    public FailedGeneratePrebuiltLicenseDataListType createFailedGeneratePrebuiltLicenseDataListType() {
        return new FailedGeneratePrebuiltLicenseDataListType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseDataListType }
     * 
     */
    public GeneratePrebuiltLicenseDataListType createGeneratePrebuiltLicenseDataListType() {
        return new GeneratePrebuiltLicenseDataListType();
    }

    /**
     * Create an instance of {@link SimpleQueryType }
     * 
     */
    public SimpleQueryType createSimpleQueryType() {
        return new SimpleQueryType();
    }

    /**
     * Create an instance of {@link DeviceIdTypeQueryType }
     * 
     */
    public DeviceIdTypeQueryType createDeviceIdTypeQueryType() {
        return new DeviceIdTypeQueryType();
    }

    /**
     * Create an instance of {@link PartnerTierQueryType }
     * 
     */
    public PartnerTierQueryType createPartnerTierQueryType() {
        return new PartnerTierQueryType();
    }

    /**
     * Create an instance of {@link DeviceStateQueryType }
     * 
     */
    public DeviceStateQueryType createDeviceStateQueryType() {
        return new DeviceStateQueryType();
    }

    /**
     * Create an instance of {@link DeviceServedStateQueryType }
     * 
     */
    public DeviceServedStateQueryType createDeviceServedStateQueryType() {
        return new DeviceServedStateQueryType();
    }

    /**
     * Create an instance of {@link NumberQueryType }
     * 
     */
    public NumberQueryType createNumberQueryType() {
        return new NumberQueryType();
    }

    /**
     * Create an instance of {@link DeviceTypeList }
     * 
     */
    public DeviceTypeList createDeviceTypeList() {
        return new DeviceTypeList();
    }

    /**
     * Create an instance of {@link DateQueryType }
     * 
     */
    public DateQueryType createDateQueryType() {
        return new DateQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributeQueryType }
     * 
     */
    public CustomAttributeQueryType createCustomAttributeQueryType() {
        return new CustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributesQueryListType }
     * 
     */
    public CustomAttributesQueryListType createCustomAttributesQueryListType() {
        return new CustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link DeviceMachineTypeQueryType }
     * 
     */
    public DeviceMachineTypeQueryType createDeviceMachineTypeQueryType() {
        return new DeviceMachineTypeQueryType();
    }

    /**
     * Create an instance of {@link GetDevicesParametersType }
     * 
     */
    public GetDevicesParametersType createGetDevicesParametersType() {
        return new GetDevicesParametersType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorType }
     * 
     */
    public CustomAttributeDescriptorType createCustomAttributeDescriptorType() {
        return new CustomAttributeDescriptorType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorDataType }
     * 
     */
    public CustomAttributeDescriptorDataType createCustomAttributeDescriptorDataType() {
        return new CustomAttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link DeviceResponseConfigRequestType }
     * 
     */
    public DeviceResponseConfigRequestType createDeviceResponseConfigRequestType() {
        return new DeviceResponseConfigRequestType();
    }

    /**
     * Create an instance of {@link FailedGetDevicesDataType }
     * 
     */
    public FailedGetDevicesDataType createFailedGetDevicesDataType() {
        return new FailedGetDevicesDataType();
    }

    /**
     * Create an instance of {@link SoldToType }
     * 
     */
    public SoldToType createSoldToType() {
        return new SoldToType();
    }

    /**
     * Create an instance of {@link EntitledProductDataType }
     * 
     */
    public EntitledProductDataType createEntitledProductDataType() {
        return new EntitledProductDataType();
    }

    /**
     * Create an instance of {@link EntitledProductDataListType }
     * 
     */
    public EntitledProductDataListType createEntitledProductDataListType() {
        return new EntitledProductDataListType();
    }

    /**
     * Create an instance of {@link AddonLineItemDataDataType }
     * 
     */
    public AddonLineItemDataDataType createAddonLineItemDataDataType() {
        return new AddonLineItemDataDataType();
    }

    /**
     * Create an instance of {@link FeatureDataDataType }
     * 
     */
    public FeatureDataDataType createFeatureDataDataType() {
        return new FeatureDataDataType();
    }

    /**
     * Create an instance of {@link DictionaryEntry }
     * 
     */
    public DictionaryEntry createDictionaryEntry() {
        return new DictionaryEntry();
    }

    /**
     * Create an instance of {@link DictionaryType }
     * 
     */
    public DictionaryType createDictionaryType() {
        return new DictionaryType();
    }

    /**
     * Create an instance of {@link DeviceQueryDataType }
     * 
     */
    public DeviceQueryDataType createDeviceQueryDataType() {
        return new DeviceQueryDataType();
    }

    /**
     * Create an instance of {@link GetDevicesResponseDataType }
     * 
     */
    public GetDevicesResponseDataType createGetDevicesResponseDataType() {
        return new GetDevicesResponseDataType();
    }

    /**
     * Create an instance of {@link GetDeviceCountResponseDataType }
     * 
     */
    public GetDeviceCountResponseDataType createGetDeviceCountResponseDataType() {
        return new GetDeviceCountResponseDataType();
    }

    /**
     * Create an instance of {@link LinkLineItemIdentifier }
     * 
     */
    public LinkLineItemIdentifier createLinkLineItemIdentifier() {
        return new LinkLineItemIdentifier();
    }

    /**
     * Create an instance of {@link LinkLineItemDataType }
     * 
     */
    public LinkLineItemDataType createLinkLineItemDataType() {
        return new LinkLineItemDataType();
    }

    /**
     * Create an instance of {@link LinkAddonLineItemDataType }
     * 
     */
    public LinkAddonLineItemDataType createLinkAddonLineItemDataType() {
        return new LinkAddonLineItemDataType();
    }

    /**
     * Create an instance of {@link LinkFailAddonDataType }
     * 
     */
    public LinkFailAddonDataType createLinkFailAddonDataType() {
        return new LinkFailAddonDataType();
    }

    /**
     * Create an instance of {@link LinkFailAddonDataListType }
     * 
     */
    public LinkFailAddonDataListType createLinkFailAddonDataListType() {
        return new LinkFailAddonDataListType();
    }

    /**
     * Create an instance of {@link SuccessAddonDataListType }
     * 
     */
    public SuccessAddonDataListType createSuccessAddonDataListType() {
        return new SuccessAddonDataListType();
    }

    /**
     * Create an instance of {@link DeleteLineItemIdentifier }
     * 
     */
    public DeleteLineItemIdentifier createDeleteLineItemIdentifier() {
        return new DeleteLineItemIdentifier();
    }

    /**
     * Create an instance of {@link DeleteLineItemDataType }
     * 
     */
    public DeleteLineItemDataType createDeleteLineItemDataType() {
        return new DeleteLineItemDataType();
    }

    /**
     * Create an instance of {@link DeleteAddonLineItemDataType }
     * 
     */
    public DeleteAddonLineItemDataType createDeleteAddonLineItemDataType() {
        return new DeleteAddonLineItemDataType();
    }

    /**
     * Create an instance of {@link DeleteFailAddonDataType }
     * 
     */
    public DeleteFailAddonDataType createDeleteFailAddonDataType() {
        return new DeleteFailAddonDataType();
    }

    /**
     * Create an instance of {@link DeleteFailAddonDataListType }
     * 
     */
    public DeleteFailAddonDataListType createDeleteFailAddonDataListType() {
        return new DeleteFailAddonDataListType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseDictionaryEntry }
     * 
     */
    public GenerateCapabilityResponseDictionaryEntry createGenerateCapabilityResponseDictionaryEntry() {
        return new GenerateCapabilityResponseDictionaryEntry();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseDictionary }
     * 
     */
    public GenerateCapabilityResponseDictionary createGenerateCapabilityResponseDictionary() {
        return new GenerateCapabilityResponseDictionary();
    }

    /**
     * Create an instance of {@link CapabilityRequestType }
     * 
     */
    public CapabilityRequestType createCapabilityRequestType() {
        return new CapabilityRequestType();
    }

    /**
     * Create an instance of {@link FailedCapabilityResponseDataType }
     * 
     */
    public FailedCapabilityResponseDataType createFailedCapabilityResponseDataType() {
        return new FailedCapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link FailedGenerateCapabilityResponseDataType }
     * 
     */
    public FailedGenerateCapabilityResponseDataType createFailedGenerateCapabilityResponseDataType() {
        return new FailedGenerateCapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link CapabilityResponseDataType }
     * 
     */
    public CapabilityResponseDataType createCapabilityResponseDataType() {
        return new CapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseDataType }
     * 
     */
    public GenerateCapabilityResponseDataType createGenerateCapabilityResponseDataType() {
        return new GenerateCapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link MoveDeviceList }
     * 
     */
    public MoveDeviceList createMoveDeviceList() {
        return new MoveDeviceList();
    }

    /**
     * Create an instance of {@link FailedMoveDeviceDataType }
     * 
     */
    public FailedMoveDeviceDataType createFailedMoveDeviceDataType() {
        return new FailedMoveDeviceDataType();
    }

    /**
     * Create an instance of {@link FailedMoveDeviceListDataType }
     * 
     */
    public FailedMoveDeviceListDataType createFailedMoveDeviceListDataType() {
        return new FailedMoveDeviceListDataType();
    }

    /**
     * Create an instance of {@link ReturnHostType }
     * 
     */
    public ReturnHostType createReturnHostType() {
        return new ReturnHostType();
    }

    /**
     * Create an instance of {@link FailedReturnHostDataType }
     * 
     */
    public FailedReturnHostDataType createFailedReturnHostDataType() {
        return new FailedReturnHostDataType();
    }

    /**
     * Create an instance of {@link FailedReturnHostListDataType }
     * 
     */
    public FailedReturnHostListDataType createFailedReturnHostListDataType() {
        return new FailedReturnHostListDataType();
    }

    /**
     * Create an instance of {@link ObsoleteHostType }
     * 
     */
    public ObsoleteHostType createObsoleteHostType() {
        return new ObsoleteHostType();
    }

    /**
     * Create an instance of {@link FailedObsoleteHostDataType }
     * 
     */
    public FailedObsoleteHostDataType createFailedObsoleteHostDataType() {
        return new FailedObsoleteHostDataType();
    }

    /**
     * Create an instance of {@link FailedObsoleteHostListDataType }
     * 
     */
    public FailedObsoleteHostListDataType createFailedObsoleteHostListDataType() {
        return new FailedObsoleteHostListDataType();
    }

    /**
     * Create an instance of {@link FeatureIds }
     * 
     */
    public FeatureIds createFeatureIds() {
        return new FeatureIds();
    }

    /**
     * Create an instance of {@link CloneSuspect.LineItemActivationIds }
     * 
     */
    public CloneSuspect.LineItemActivationIds createCloneSuspectLineItemActivationIds() {
        return new CloneSuspect.LineItemActivationIds();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportResponse.CloneSuspects }
     * 
     */
    public GenerateCloneDetectionReportResponse.CloneSuspects createGenerateCloneDetectionReportResponseCloneSuspects() {
        return new GenerateCloneDetectionReportResponse.CloneSuspects();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportRequest.EnterpriseIds }
     * 
     */
    public GenerateCloneDetectionReportRequest.EnterpriseIds createGenerateCloneDetectionReportRequestEnterpriseIds() {
        return new GenerateCloneDetectionReportRequest.EnterpriseIds();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDevRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "createDeviceRequest")
    public JAXBElement<CreateDevRequestType> createCreateDeviceRequest(CreateDevRequestType value) {
        return new JAXBElement<CreateDevRequestType>(_CreateDeviceRequest_QNAME, CreateDevRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDevResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "createDeviceResponse")
    public JAXBElement<CreateDevResponseType> createCreateDeviceResponse(CreateDevResponseType value) {
        return new JAXBElement<CreateDevResponseType>(_CreateDeviceResponse_QNAME, CreateDevResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteDeviceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "deleteDeviceRequest")
    public JAXBElement<DeleteDeviceRequestType> createDeleteDeviceRequest(DeleteDeviceRequestType value) {
        return new JAXBElement<DeleteDeviceRequestType>(_DeleteDeviceRequest_QNAME, DeleteDeviceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteDeviceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "deleteDeviceResponse")
    public JAXBElement<DeleteDeviceResponseType> createDeleteDeviceResponse(DeleteDeviceResponseType value) {
        return new JAXBElement<DeleteDeviceResponseType>(_DeleteDeviceResponse_QNAME, DeleteDeviceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDevRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "updateDeviceRequest")
    public JAXBElement<UpdateDevRequestType> createUpdateDeviceRequest(UpdateDevRequestType value) {
        return new JAXBElement<UpdateDevRequestType>(_UpdateDeviceRequest_QNAME, UpdateDevRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDevResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "updateDeviceResponse")
    public JAXBElement<UpdateDevResponseType> createUpdateDeviceResponse(UpdateDevResponseType value) {
        return new JAXBElement<UpdateDevResponseType>(_UpdateDeviceResponse_QNAME, UpdateDevResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneratePrebuiltLicenseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "generatePrebuiltLicenseRequest")
    public JAXBElement<GeneratePrebuiltLicenseRequestType> createGeneratePrebuiltLicenseRequest(GeneratePrebuiltLicenseRequestType value) {
        return new JAXBElement<GeneratePrebuiltLicenseRequestType>(_GeneratePrebuiltLicenseRequest_QNAME, GeneratePrebuiltLicenseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneratePrebuiltLicenseResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "generatePrebuiltLicenseResponse")
    public JAXBElement<GeneratePrebuiltLicenseResponseType> createGeneratePrebuiltLicenseResponse(GeneratePrebuiltLicenseResponseType value) {
        return new JAXBElement<GeneratePrebuiltLicenseResponseType>(_GeneratePrebuiltLicenseResponse_QNAME, GeneratePrebuiltLicenseResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDevicesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "getDevicesRequest")
    public JAXBElement<GetDevicesRequestType> createGetDevicesRequest(GetDevicesRequestType value) {
        return new JAXBElement<GetDevicesRequestType>(_GetDevicesRequest_QNAME, GetDevicesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDevicesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "getDevicesResponse")
    public JAXBElement<GetDevicesResponseType> createGetDevicesResponse(GetDevicesResponseType value) {
        return new JAXBElement<GetDevicesResponseType>(_GetDevicesResponse_QNAME, GetDevicesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDevicesCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "getDeviceCountRequest")
    public JAXBElement<GetDevicesCountRequestType> createGetDeviceCountRequest(GetDevicesCountRequestType value) {
        return new JAXBElement<GetDevicesCountRequestType>(_GetDeviceCountRequest_QNAME, GetDevicesCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "getDeviceCountResponse")
    public JAXBElement<GetDeviceCountResponseType> createGetDeviceCountResponse(GetDeviceCountResponseType value) {
        return new JAXBElement<GetDeviceCountResponseType>(_GetDeviceCountResponse_QNAME, GetDeviceCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "linkAddonLineItemsRequest")
    public JAXBElement<LinkAddonLineItemsRequestType> createLinkAddonLineItemsRequest(LinkAddonLineItemsRequestType value) {
        return new JAXBElement<LinkAddonLineItemsRequestType>(_LinkAddonLineItemsRequest_QNAME, LinkAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "linkAddonLineItemsResponse")
    public JAXBElement<LinkAddonLineItemsResponseType> createLinkAddonLineItemsResponse(LinkAddonLineItemsResponseType value) {
        return new JAXBElement<LinkAddonLineItemsResponseType>(_LinkAddonLineItemsResponse_QNAME, LinkAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "deleteAddonLineItemsRequest")
    public JAXBElement<DeleteAddonLineItemsRequestType> createDeleteAddonLineItemsRequest(DeleteAddonLineItemsRequestType value) {
        return new JAXBElement<DeleteAddonLineItemsRequestType>(_DeleteAddonLineItemsRequest_QNAME, DeleteAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "deleteAddonLineItemsResponse")
    public JAXBElement<DeleteAddonLineItemsResponseType> createDeleteAddonLineItemsResponse(DeleteAddonLineItemsResponseType value) {
        return new JAXBElement<DeleteAddonLineItemsResponseType>(_DeleteAddonLineItemsResponse_QNAME, DeleteAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "incrementAddonLineItemsRequest")
    public JAXBElement<LinkAddonLineItemsRequestType> createIncrementAddonLineItemsRequest(LinkAddonLineItemsRequestType value) {
        return new JAXBElement<LinkAddonLineItemsRequestType>(_IncrementAddonLineItemsRequest_QNAME, LinkAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "incrementAddonLineItemsResponse")
    public JAXBElement<LinkAddonLineItemsResponseType> createIncrementAddonLineItemsResponse(LinkAddonLineItemsResponseType value) {
        return new JAXBElement<LinkAddonLineItemsResponseType>(_IncrementAddonLineItemsResponse_QNAME, LinkAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "decrementAddonLineItemsRequest")
    public JAXBElement<LinkAddonLineItemsRequestType> createDecrementAddonLineItemsRequest(LinkAddonLineItemsRequestType value) {
        return new JAXBElement<LinkAddonLineItemsRequestType>(_DecrementAddonLineItemsRequest_QNAME, LinkAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "decrementAddonLineItemsResponse")
    public JAXBElement<LinkAddonLineItemsResponseType> createDecrementAddonLineItemsResponse(LinkAddonLineItemsResponseType value) {
        return new JAXBElement<LinkAddonLineItemsResponseType>(_DecrementAddonLineItemsResponse_QNAME, LinkAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCapabilityResponseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "generateCapabilityResponseRequest")
    public JAXBElement<GenerateCapabilityResponseRequestType> createGenerateCapabilityResponseRequest(GenerateCapabilityResponseRequestType value) {
        return new JAXBElement<GenerateCapabilityResponseRequestType>(_GenerateCapabilityResponseRequest_QNAME, GenerateCapabilityResponseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCapabilityResponseResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "generateCapabilityResponseResponse")
    public JAXBElement<GenerateCapabilityResponseResponseType> createGenerateCapabilityResponseResponse(GenerateCapabilityResponseResponseType value) {
        return new JAXBElement<GenerateCapabilityResponseResponseType>(_GenerateCapabilityResponseResponse_QNAME, GenerateCapabilityResponseResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveDeviceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "moveDeviceRequest")
    public JAXBElement<MoveDeviceRequestType> createMoveDeviceRequest(MoveDeviceRequestType value) {
        return new JAXBElement<MoveDeviceRequestType>(_MoveDeviceRequest_QNAME, MoveDeviceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveDeviceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "moveDeviceResponse")
    public JAXBElement<MoveDeviceResponseType> createMoveDeviceResponse(MoveDeviceResponseType value) {
        return new JAXBElement<MoveDeviceResponseType>(_MoveDeviceResponse_QNAME, MoveDeviceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnHostRequestListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "returnHostRequest")
    public JAXBElement<ReturnHostRequestListType> createReturnHostRequest(ReturnHostRequestListType value) {
        return new JAXBElement<ReturnHostRequestListType>(_ReturnHostRequest_QNAME, ReturnHostRequestListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnHostResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "returnHostResponse")
    public JAXBElement<ReturnHostResponseType> createReturnHostResponse(ReturnHostResponseType value) {
        return new JAXBElement<ReturnHostResponseType>(_ReturnHostResponse_QNAME, ReturnHostResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObsoleteHostRequestListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "obsoleteHostRequest")
    public JAXBElement<ObsoleteHostRequestListType> createObsoleteHostRequest(ObsoleteHostRequestListType value) {
        return new JAXBElement<ObsoleteHostRequestListType>(_ObsoleteHostRequest_QNAME, ObsoleteHostRequestListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObsoleteHostResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "obsoleteHostResponse")
    public JAXBElement<ObsoleteHostResponseType> createObsoleteHostResponse(ObsoleteHostResponseType value) {
        return new JAXBElement<ObsoleteHostResponseType>(_ObsoleteHostResponse_QNAME, ObsoleteHostResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAutoProvisionedServerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "getAutoProvisionedServerRequest")
    public JAXBElement<GetAutoProvisionedServerRequest> createGetAutoProvisionedServerRequest(GetAutoProvisionedServerRequest value) {
        return new JAXBElement<GetAutoProvisionedServerRequest>(_GetAutoProvisionedServerRequest_QNAME, GetAutoProvisionedServerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAutoProvisionedServerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "getAutoProvisionedServerResponse")
    public JAXBElement<GetAutoProvisionedServerResponse> createGetAutoProvisionedServerResponse(GetAutoProvisionedServerResponse value) {
        return new JAXBElement<GetAutoProvisionedServerResponse>(_GetAutoProvisionedServerResponse_QNAME, GetAutoProvisionedServerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCloneDetectionReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "generateCloneDetectionReportRequest")
    public JAXBElement<GenerateCloneDetectionReportRequest> createGenerateCloneDetectionReportRequest(GenerateCloneDetectionReportRequest value) {
        return new JAXBElement<GenerateCloneDetectionReportRequest>(_GenerateCloneDetectionReportRequest_QNAME, GenerateCloneDetectionReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCloneDetectionReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "generateCloneDetectionReportResponse")
    public JAXBElement<GenerateCloneDetectionReportResponse> createGenerateCloneDetectionReportResponse(GenerateCloneDetectionReportResponse value) {
        return new JAXBElement<GenerateCloneDetectionReportResponse>(_GenerateCloneDetectionReportResponse_QNAME, GenerateCloneDetectionReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCloneDetectionReportRequest.EnterpriseIds }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.fne.webservices.operations.flexnet.com", name = "enterpriseIds", scope = GenerateCloneDetectionReportRequest.class)
    public JAXBElement<GenerateCloneDetectionReportRequest.EnterpriseIds> createGenerateCloneDetectionReportRequestEnterpriseIds(GenerateCloneDetectionReportRequest.EnterpriseIds value) {
        return new JAXBElement<GenerateCloneDetectionReportRequest.EnterpriseIds>(_GenerateCloneDetectionReportRequestEnterpriseIds_QNAME, GenerateCloneDetectionReportRequest.EnterpriseIds.class, GenerateCloneDetectionReportRequest.class, value);
    }

}
