
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for matchingBulkEntDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="matchingBulkEntDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="matchingBulkEntIdentifier" type="{urn:v2.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "matchingBulkEntDataType", propOrder = {
    "matchingBulkEntIdentifier"
})
public class MatchingBulkEntDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType matchingBulkEntIdentifier;

    /**
     * Gets the value of the matchingBulkEntIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getMatchingBulkEntIdentifier() {
        return matchingBulkEntIdentifier;
    }

    /**
     * Sets the value of the matchingBulkEntIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setMatchingBulkEntIdentifier(EntitlementIdentifierType value) {
        this.matchingBulkEntIdentifier = value;
    }

}
