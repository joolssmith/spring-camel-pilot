
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for attributeMetaDescriptorType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="attributeMetaDescriptorType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="attributeName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="attributeDataType" type="{urn:com.macrovision:flexnet/operations}AttributeDataType"/&gt;
 *         &lt;element name="namespace" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="validValues" type="{urn:com.macrovision:flexnet/operations}valueType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "attributeMetaDescriptorType", propOrder = {
    "attributeName",
    "attributeDataType",
    "namespace",
    "validValues"
})
public class AttributeMetaDescriptorType {

    @XmlElement(required = true)
    protected String attributeName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected AttributeDataType attributeDataType;
    @XmlElement(required = true)
    protected String namespace;
    protected ValueType validValues;

    /**
     * Gets the value of the attributeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets the value of the attributeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttributeName(String value) {
        this.attributeName = value;
    }

    /**
     * Gets the value of the attributeDataType property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDataType }
     *     
     */
    public AttributeDataType getAttributeDataType() {
        return attributeDataType;
    }

    /**
     * Sets the value of the attributeDataType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDataType }
     *     
     */
    public void setAttributeDataType(AttributeDataType value) {
        this.attributeDataType = value;
    }

    /**
     * Gets the value of the namespace property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNamespace() {
        return namespace;
    }

    /**
     * Sets the value of the namespace property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNamespace(String value) {
        this.namespace = value;
    }

    /**
     * Gets the value of the validValues property.
     * 
     * @return
     *     possible object is
     *     {@link ValueType }
     *     
     */
    public ValueType getValidValues() {
        return validValues;
    }

    /**
     * Sets the value of the validValues property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueType }
     *     
     */
    public void setValidValues(ValueType value) {
        this.validValues = value;
    }

}
