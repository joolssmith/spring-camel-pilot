
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for generatePrebuiltLicenseResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generatePrebuiltLicenseResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:com.macrovision:flexnet/opsembedded}OpsEmbeddedStatusInfoType"/&gt;
 *         &lt;element name="failedData" type="{urn:com.macrovision:flexnet/opsembedded}failedGeneratePrebuiltLicenseDataListType" minOccurs="0"/&gt;
 *         &lt;element name="responseData" type="{urn:com.macrovision:flexnet/opsembedded}generatePrebuiltLicenseDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generatePrebuiltLicenseResponseType", propOrder = {
    "statusInfo",
    "failedData",
    "responseData"
})
public class GeneratePrebuiltLicenseResponseType {

    @XmlElement(required = true)
    protected OpsEmbeddedStatusInfoType statusInfo;
    protected FailedGeneratePrebuiltLicenseDataListType failedData;
    protected GeneratePrebuiltLicenseDataListType responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public OpsEmbeddedStatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public void setStatusInfo(OpsEmbeddedStatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedGeneratePrebuiltLicenseDataListType }
     *     
     */
    public FailedGeneratePrebuiltLicenseDataListType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedGeneratePrebuiltLicenseDataListType }
     *     
     */
    public void setFailedData(FailedGeneratePrebuiltLicenseDataListType value) {
        this.failedData = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link GeneratePrebuiltLicenseDataListType }
     *     
     */
    public GeneratePrebuiltLicenseDataListType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneratePrebuiltLicenseDataListType }
     *     
     */
    public void setResponseData(GeneratePrebuiltLicenseDataListType value) {
        this.responseData = value;
    }

}
