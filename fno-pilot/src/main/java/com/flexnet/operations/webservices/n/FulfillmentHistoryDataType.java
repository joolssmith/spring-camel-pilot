
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fulfillmentHistoryDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fulfillmentHistoryDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="historyDetails" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentHistoryDetailsType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fulfillmentHistoryDataType", propOrder = {
    "activationId",
    "historyDetails"
})
public class FulfillmentHistoryDataType {

    @XmlElement(required = true)
    protected String activationId;
    @XmlElement(required = true)
    protected FulfillmentHistoryDetailsType historyDetails;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationId(String value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the historyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentHistoryDetailsType }
     *     
     */
    public FulfillmentHistoryDetailsType getHistoryDetails() {
        return historyDetails;
    }

    /**
     * Sets the value of the historyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentHistoryDetailsType }
     *     
     */
    public void setHistoryDetails(FulfillmentHistoryDetailsType value) {
        this.historyDetails = value;
    }

}
