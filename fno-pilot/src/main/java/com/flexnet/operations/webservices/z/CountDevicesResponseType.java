
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for countDevicesResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="countDevicesResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v3.fne.webservices.operations.flexnet.com}OpsEmbeddedStatusInfoType"/&gt;
 *         &lt;element name="failedData" type="{urn:v3.fne.webservices.operations.flexnet.com}failedCountDevicesData" minOccurs="0"/&gt;
 *         &lt;element name="responseData" type="{urn:v3.fne.webservices.operations.flexnet.com}countDevicesResponseData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "countDevicesResponseType", propOrder = {
    "statusInfo",
    "failedData",
    "responseData"
})
public class CountDevicesResponseType {

    @XmlElement(required = true)
    protected OpsEmbeddedStatusInfoType statusInfo;
    protected FailedCountDevicesData failedData;
    protected CountDevicesResponseData responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public OpsEmbeddedStatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public void setStatusInfo(OpsEmbeddedStatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedCountDevicesData }
     *     
     */
    public FailedCountDevicesData getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedCountDevicesData }
     *     
     */
    public void setFailedData(FailedCountDevicesData value) {
        this.failedData = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link CountDevicesResponseData }
     *     
     */
    public CountDevicesResponseData getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CountDevicesResponseData }
     *     
     */
    public void setResponseData(CountDevicesResponseData value) {
        this.responseData = value;
    }

}
