
package com.flexnet.operations.webservices.i;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for countForHostsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="countForHostsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hostDataRefId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fulfillCount" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="overDraftCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "countForHostsType", propOrder = {
    "hostDataRefId",
    "fulfillCount",
    "overDraftCount"
})
public class CountForHostsType {

    @XmlElement(required = true)
    protected String hostDataRefId;
    @XmlElement(required = true)
    protected BigInteger fulfillCount;
    protected BigInteger overDraftCount;

    /**
     * Gets the value of the hostDataRefId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHostDataRefId() {
        return hostDataRefId;
    }

    /**
     * Sets the value of the hostDataRefId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHostDataRefId(String value) {
        this.hostDataRefId = value;
    }

    /**
     * Gets the value of the fulfillCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFulfillCount() {
        return fulfillCount;
    }

    /**
     * Sets the value of the fulfillCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFulfillCount(BigInteger value) {
        this.fulfillCount = value;
    }

    /**
     * Gets the value of the overDraftCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOverDraftCount() {
        return overDraftCount;
    }

    /**
     * Sets the value of the overDraftCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOverDraftCount(BigInteger value) {
        this.overDraftCount = value;
    }

}
