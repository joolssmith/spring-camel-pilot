
package com.flexnet.operations.webservices.o;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addWebRegKeyRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addWebRegKeyRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="webRegKeyData" type="{urn:v1.webservices.operations.flexnet.com}addWebRegKeyDataType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="processSync" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addWebRegKeyRequestType", propOrder = {
    "webRegKeyData",
    "processSync"
})
public class AddWebRegKeyRequestType {

    @XmlElement(required = true)
    protected List<AddWebRegKeyDataType> webRegKeyData;
    protected Boolean processSync;

    /**
     * Gets the value of the webRegKeyData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the webRegKeyData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWebRegKeyData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddWebRegKeyDataType }
     * 
     * 
     */
    public List<AddWebRegKeyDataType> getWebRegKeyData() {
        if (webRegKeyData == null) {
            webRegKeyData = new ArrayList<AddWebRegKeyDataType>();
        }
        return this.webRegKeyData;
    }

    /**
     * Gets the value of the processSync property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProcessSync() {
        return processSync;
    }

    /**
     * Sets the value of the processSync property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProcessSync(Boolean value) {
        this.processSync = value;
    }

}
