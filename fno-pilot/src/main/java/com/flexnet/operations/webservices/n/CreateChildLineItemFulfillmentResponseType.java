
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createChildLineItemFulfillmentResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createChildLineItemFulfillmentResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="responseData" type="{urn:v1.webservices.operations.flexnet.com}createdChildLIFmtResponseDataType" minOccurs="0"/&gt;
 *         &lt;element name="failedData" type="{urn:v1.webservices.operations.flexnet.com}failedChildLIFmtResponseDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createChildLineItemFulfillmentResponseType", propOrder = {
    "statusInfo",
    "responseData",
    "failedData"
})
public class CreateChildLineItemFulfillmentResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected CreatedChildLIFmtResponseDataType responseData;
    protected FailedChildLIFmtResponseDataType failedData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link CreatedChildLIFmtResponseDataType }
     *     
     */
    public CreatedChildLIFmtResponseDataType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatedChildLIFmtResponseDataType }
     *     
     */
    public void setResponseData(CreatedChildLIFmtResponseDataType value) {
        this.responseData = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedChildLIFmtResponseDataType }
     *     
     */
    public FailedChildLIFmtResponseDataType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedChildLIFmtResponseDataType }
     *     
     */
    public void setFailedData(FailedChildLIFmtResponseDataType value) {
        this.failedData = value;
    }

}
