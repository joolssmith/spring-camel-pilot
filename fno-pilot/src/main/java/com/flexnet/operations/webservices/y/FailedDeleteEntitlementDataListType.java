
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedDeleteEntitlementDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedDeleteEntitlementDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedEntitlement" type="{urn:v3.webservices.operations.flexnet.com}failedDeleteEntitlementDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedDeleteEntitlementDataListType", propOrder = {
    "failedEntitlement"
})
public class FailedDeleteEntitlementDataListType {

    protected List<FailedDeleteEntitlementDataType> failedEntitlement;

    /**
     * Gets the value of the failedEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedDeleteEntitlementDataType }
     * 
     * 
     */
    public List<FailedDeleteEntitlementDataType> getFailedEntitlement() {
        if (failedEntitlement == null) {
            failedEntitlement = new ArrayList<FailedDeleteEntitlementDataType>();
        }
        return this.failedEntitlement;
    }

}
