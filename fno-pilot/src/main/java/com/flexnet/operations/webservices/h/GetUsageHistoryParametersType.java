
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUsageHistoryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getUsageHistoryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="updateTime" type="{urn:com.macrovision:flexnet/opsembedded}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="serverUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="serverId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="alias" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureVersion" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureCount" type="{urn:com.macrovision:flexnet/opsembedded}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureOverage" type="{urn:com.macrovision:flexnet/opsembedded}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="machineType" type="{urn:com.macrovision:flexnet/opsembedded}DeviceMachineTypeQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUsageHistoryParametersType", propOrder = {
    "updateTime",
    "serverUniqueId",
    "serverId",
    "deviceUniqueId",
    "deviceId",
    "alias",
    "hostTypeName",
    "featureName",
    "featureVersion",
    "featureCount",
    "featureOverage",
    "machineType"
})
public class GetUsageHistoryParametersType {

    protected DateTimeQueryType updateTime;
    protected ExternalIdQueryType serverUniqueId;
    protected SimpleQueryType serverId;
    protected ExternalIdQueryType deviceUniqueId;
    protected SimpleQueryType deviceId;
    protected SimpleQueryType alias;
    protected SimpleQueryType hostTypeName;
    protected SimpleQueryType featureName;
    protected SimpleQueryType featureVersion;
    protected NumberQueryType featureCount;
    protected NumberQueryType featureOverage;
    protected DeviceMachineTypeQueryType machineType;

    /**
     * Gets the value of the updateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setUpdateTime(DateTimeQueryType value) {
        this.updateTime = value;
    }

    /**
     * Gets the value of the serverUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getServerUniqueId() {
        return serverUniqueId;
    }

    /**
     * Sets the value of the serverUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setServerUniqueId(ExternalIdQueryType value) {
        this.serverUniqueId = value;
    }

    /**
     * Gets the value of the serverId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getServerId() {
        return serverId;
    }

    /**
     * Sets the value of the serverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setServerId(SimpleQueryType value) {
        this.serverId = value;
    }

    /**
     * Gets the value of the deviceUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getDeviceUniqueId() {
        return deviceUniqueId;
    }

    /**
     * Sets the value of the deviceUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setDeviceUniqueId(ExternalIdQueryType value) {
        this.deviceUniqueId = value;
    }

    /**
     * Gets the value of the deviceId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDeviceId(SimpleQueryType value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAlias(SimpleQueryType value) {
        this.alias = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setHostTypeName(SimpleQueryType value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFeatureName(SimpleQueryType value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the featureVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFeatureVersion() {
        return featureVersion;
    }

    /**
     * Sets the value of the featureVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFeatureVersion(SimpleQueryType value) {
        this.featureVersion = value;
    }

    /**
     * Gets the value of the featureCount property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getFeatureCount() {
        return featureCount;
    }

    /**
     * Sets the value of the featureCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setFeatureCount(NumberQueryType value) {
        this.featureCount = value;
    }

    /**
     * Gets the value of the featureOverage property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getFeatureOverage() {
        return featureOverage;
    }

    /**
     * Sets the value of the featureOverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setFeatureOverage(NumberQueryType value) {
        this.featureOverage = value;
    }

    /**
     * Gets the value of the machineType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceMachineTypeQueryType }
     *     
     */
    public DeviceMachineTypeQueryType getMachineType() {
        return machineType;
    }

    /**
     * Sets the value of the machineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceMachineTypeQueryType }
     *     
     */
    public void setMachineType(DeviceMachineTypeQueryType value) {
        this.machineType = value;
    }

}
