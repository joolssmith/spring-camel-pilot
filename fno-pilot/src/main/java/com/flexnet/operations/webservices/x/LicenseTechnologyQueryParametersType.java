
package com.flexnet.operations.webservices.x;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for licenseTechnologyQueryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="licenseTechnologyQueryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="name" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "licenseTechnologyQueryParametersType", propOrder = {
    "name"
})
public class LicenseTechnologyQueryParametersType {

    @XmlElementRef(name = "name", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> name;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setName(JAXBElement<SimpleQueryType> value) {
        this.name = value;
    }

}
