
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchMaintenanceLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchMaintenanceLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="activationId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceProductName" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceProductVersion" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{urn:v2.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{urn:v2.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="withNoOrderId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItemAttributes" type="{urn:v2.webservices.operations.flexnet.com}customAttributesQueryListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchMaintenanceLineItemDataType", propOrder = {
    "entitlementId",
    "activationId",
    "maintenanceProductName",
    "maintenanceProductVersion",
    "partNumber",
    "startDate",
    "isPermanent",
    "expirationDate",
    "orderId",
    "orderLineNumber",
    "withNoOrderId",
    "maintenanceLineItemAttributes"
})
public class SearchMaintenanceLineItemDataType {

    protected SimpleQueryType entitlementId;
    protected SimpleQueryType activationId;
    protected SimpleQueryType maintenanceProductName;
    protected SimpleQueryType maintenanceProductVersion;
    protected SimpleQueryType partNumber;
    protected DateQueryType startDate;
    protected Boolean isPermanent;
    protected DateQueryType expirationDate;
    protected SimpleQueryType orderId;
    protected SimpleQueryType orderLineNumber;
    protected Boolean withNoOrderId;
    protected CustomAttributesQueryListType maintenanceLineItemAttributes;

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setEntitlementId(SimpleQueryType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setActivationId(SimpleQueryType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the maintenanceProductName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getMaintenanceProductName() {
        return maintenanceProductName;
    }

    /**
     * Sets the value of the maintenanceProductName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setMaintenanceProductName(SimpleQueryType value) {
        this.maintenanceProductName = value;
    }

    /**
     * Gets the value of the maintenanceProductVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getMaintenanceProductVersion() {
        return maintenanceProductVersion;
    }

    /**
     * Sets the value of the maintenanceProductVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setMaintenanceProductVersion(SimpleQueryType value) {
        this.maintenanceProductVersion = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setPartNumber(SimpleQueryType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setStartDate(DateQueryType value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setExpirationDate(DateQueryType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setOrderId(SimpleQueryType value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setOrderLineNumber(SimpleQueryType value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the withNoOrderId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isWithNoOrderId() {
        return withNoOrderId;
    }

    /**
     * Sets the value of the withNoOrderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setWithNoOrderId(Boolean value) {
        this.withNoOrderId = value;
    }

    /**
     * Gets the value of the maintenanceLineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public CustomAttributesQueryListType getMaintenanceLineItemAttributes() {
        return maintenanceLineItemAttributes;
    }

    /**
     * Sets the value of the maintenanceLineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public void setMaintenanceLineItemAttributes(CustomAttributesQueryListType value) {
        this.maintenanceLineItemAttributes = value;
    }

}
