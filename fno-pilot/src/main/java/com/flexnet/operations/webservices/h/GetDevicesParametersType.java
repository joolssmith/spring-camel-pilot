
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDevicesParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDevicesParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="alias" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceIdType" type="{urn:com.macrovision:flexnet/opsembedded}DeviceIdTypeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="parentId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="parentIdType" type="{urn:com.macrovision:flexnet/opsembedded}DeviceIdTypeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="currentOwnerName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="organizationUnitName" type="{urn:com.macrovision:flexnet/opsembedded}PartnerTierQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{urn:com.macrovision:flexnet/opsembedded}DeviceStateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="servedStatus" type="{urn:com.macrovision:flexnet/opsembedded}DeviceServedStateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="preBuiltProductName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="preBuiltProductVersion" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnActivationId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnProductName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnProductVersion" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureVersion" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureCount" type="{urn:com.macrovision:flexnet/opsembedded}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureOverage" type="{urn:com.macrovision:flexnet/opsembedded}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="isServer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deviceTypes" type="{urn:com.macrovision:flexnet/opsembedded}deviceTypeList" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:com.macrovision:flexnet/opsembedded}customAttributesQueryListType" minOccurs="0"/&gt;
 *         &lt;element name="machineType" type="{urn:com.macrovision:flexnet/opsembedded}DeviceMachineTypeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="vmName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDevicesParametersType", propOrder = {
    "alias",
    "deviceId",
    "deviceIdType",
    "parentId",
    "parentIdType",
    "hostTypeName",
    "soldTo",
    "currentOwnerName",
    "organizationUnitName",
    "description",
    "status",
    "servedStatus",
    "preBuiltProductName",
    "preBuiltProductVersion",
    "addOnActivationId",
    "addOnProductName",
    "addOnProductVersion",
    "featureName",
    "featureVersion",
    "featureCount",
    "featureOverage",
    "isServer",
    "deviceTypes",
    "customAttributes",
    "machineType",
    "vmName"
})
public class GetDevicesParametersType {

    protected SimpleQueryType alias;
    protected SimpleQueryType deviceId;
    protected DeviceIdTypeQueryType deviceIdType;
    protected SimpleQueryType parentId;
    protected DeviceIdTypeQueryType parentIdType;
    protected SimpleQueryType hostTypeName;
    protected SimpleQueryType soldTo;
    protected SimpleQueryType currentOwnerName;
    protected PartnerTierQueryType organizationUnitName;
    protected SimpleQueryType description;
    protected DeviceStateQueryType status;
    protected DeviceServedStateQueryType servedStatus;
    protected SimpleQueryType preBuiltProductName;
    protected SimpleQueryType preBuiltProductVersion;
    protected SimpleQueryType addOnActivationId;
    protected SimpleQueryType addOnProductName;
    protected SimpleQueryType addOnProductVersion;
    protected SimpleQueryType featureName;
    protected SimpleQueryType featureVersion;
    protected NumberQueryType featureCount;
    protected NumberQueryType featureOverage;
    protected Boolean isServer;
    protected DeviceTypeList deviceTypes;
    protected CustomAttributesQueryListType customAttributes;
    protected DeviceMachineTypeQueryType machineType;
    protected SimpleQueryType vmName;

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAlias(SimpleQueryType value) {
        this.alias = value;
    }

    /**
     * Gets the value of the deviceId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDeviceId(SimpleQueryType value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the deviceIdType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public DeviceIdTypeQueryType getDeviceIdType() {
        return deviceIdType;
    }

    /**
     * Sets the value of the deviceIdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public void setDeviceIdType(DeviceIdTypeQueryType value) {
        this.deviceIdType = value;
    }

    /**
     * Gets the value of the parentId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getParentId() {
        return parentId;
    }

    /**
     * Sets the value of the parentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setParentId(SimpleQueryType value) {
        this.parentId = value;
    }

    /**
     * Gets the value of the parentIdType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public DeviceIdTypeQueryType getParentIdType() {
        return parentIdType;
    }

    /**
     * Sets the value of the parentIdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public void setParentIdType(DeviceIdTypeQueryType value) {
        this.parentIdType = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setHostTypeName(SimpleQueryType value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldTo(SimpleQueryType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the currentOwnerName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCurrentOwnerName() {
        return currentOwnerName;
    }

    /**
     * Sets the value of the currentOwnerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCurrentOwnerName(SimpleQueryType value) {
        this.currentOwnerName = value;
    }

    /**
     * Gets the value of the organizationUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public PartnerTierQueryType getOrganizationUnitName() {
        return organizationUnitName;
    }

    /**
     * Sets the value of the organizationUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public void setOrganizationUnitName(PartnerTierQueryType value) {
        this.organizationUnitName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDescription(SimpleQueryType value) {
        this.description = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceStateQueryType }
     *     
     */
    public DeviceStateQueryType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceStateQueryType }
     *     
     */
    public void setStatus(DeviceStateQueryType value) {
        this.status = value;
    }

    /**
     * Gets the value of the servedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceServedStateQueryType }
     *     
     */
    public DeviceServedStateQueryType getServedStatus() {
        return servedStatus;
    }

    /**
     * Sets the value of the servedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceServedStateQueryType }
     *     
     */
    public void setServedStatus(DeviceServedStateQueryType value) {
        this.servedStatus = value;
    }

    /**
     * Gets the value of the preBuiltProductName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getPreBuiltProductName() {
        return preBuiltProductName;
    }

    /**
     * Sets the value of the preBuiltProductName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setPreBuiltProductName(SimpleQueryType value) {
        this.preBuiltProductName = value;
    }

    /**
     * Gets the value of the preBuiltProductVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getPreBuiltProductVersion() {
        return preBuiltProductVersion;
    }

    /**
     * Sets the value of the preBuiltProductVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setPreBuiltProductVersion(SimpleQueryType value) {
        this.preBuiltProductVersion = value;
    }

    /**
     * Gets the value of the addOnActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnActivationId() {
        return addOnActivationId;
    }

    /**
     * Sets the value of the addOnActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnActivationId(SimpleQueryType value) {
        this.addOnActivationId = value;
    }

    /**
     * Gets the value of the addOnProductName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnProductName() {
        return addOnProductName;
    }

    /**
     * Sets the value of the addOnProductName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnProductName(SimpleQueryType value) {
        this.addOnProductName = value;
    }

    /**
     * Gets the value of the addOnProductVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnProductVersion() {
        return addOnProductVersion;
    }

    /**
     * Sets the value of the addOnProductVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnProductVersion(SimpleQueryType value) {
        this.addOnProductVersion = value;
    }

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFeatureName(SimpleQueryType value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the featureVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFeatureVersion() {
        return featureVersion;
    }

    /**
     * Sets the value of the featureVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFeatureVersion(SimpleQueryType value) {
        this.featureVersion = value;
    }

    /**
     * Gets the value of the featureCount property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getFeatureCount() {
        return featureCount;
    }

    /**
     * Sets the value of the featureCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setFeatureCount(NumberQueryType value) {
        this.featureCount = value;
    }

    /**
     * Gets the value of the featureOverage property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getFeatureOverage() {
        return featureOverage;
    }

    /**
     * Sets the value of the featureOverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setFeatureOverage(NumberQueryType value) {
        this.featureOverage = value;
    }

    /**
     * Gets the value of the isServer property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsServer() {
        return isServer;
    }

    /**
     * Sets the value of the isServer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsServer(Boolean value) {
        this.isServer = value;
    }

    /**
     * Gets the value of the deviceTypes property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeList }
     *     
     */
    public DeviceTypeList getDeviceTypes() {
        return deviceTypes;
    }

    /**
     * Sets the value of the deviceTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeList }
     *     
     */
    public void setDeviceTypes(DeviceTypeList value) {
        this.deviceTypes = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public CustomAttributesQueryListType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public void setCustomAttributes(CustomAttributesQueryListType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the machineType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceMachineTypeQueryType }
     *     
     */
    public DeviceMachineTypeQueryType getMachineType() {
        return machineType;
    }

    /**
     * Sets the value of the machineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceMachineTypeQueryType }
     *     
     */
    public void setMachineType(DeviceMachineTypeQueryType value) {
        this.machineType = value;
    }

    /**
     * Gets the value of the vmName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getVmName() {
        return vmName;
    }

    /**
     * Sets the value of the vmName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setVmName(SimpleQueryType value) {
        this.vmName = value;
    }

}
