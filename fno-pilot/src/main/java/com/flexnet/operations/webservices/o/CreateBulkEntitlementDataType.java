
package com.flexnet.operations.webservices.o;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for createBulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createBulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementId" type="{urn:v1.webservices.operations.flexnet.com}idType"/&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{urn:v1.webservices.operations.flexnet.com}productIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v1.webservices.operations.flexnet.com}partNumberIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModel" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel1" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel2" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModelAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="FNPTimeZoneValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="policyAttributes" type="{urn:v1.webservices.operations.flexnet.com}policyAttributesListType" minOccurs="0"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="startDateOption" type="{urn:v1.webservices.operations.flexnet.com}StartDateOptionType" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="term" type="{urn:v1.webservices.operations.flexnet.com}DurationType" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDateAttributes" type="{urn:v1.webservices.operations.flexnet.com}versionDateAttributesType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numberOfCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntitlementType" type="{urn:v1.webservices.operations.flexnet.com}BulkEntitlementType" minOccurs="0"/&gt;
 *         &lt;element name="autoDeploy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="entitledProducts" type="{urn:v1.webservices.operations.flexnet.com}entitledProductDataListType" minOccurs="0"/&gt;
 *         &lt;element name="channelPartners" type="{urn:v1.webservices.operations.flexnet.com}channelPartnerDataListType" minOccurs="0"/&gt;
 *         &lt;element name="allowPortalLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createBulkEntitlementDataType", propOrder = {
    "entitlementId",
    "soldTo",
    "product",
    "partNumber",
    "licenseModel",
    "alternateLicenseModel1",
    "alternateLicenseModel2",
    "licenseModelAttributes",
    "fnpTimeZoneValue",
    "policyAttributes",
    "shipToEmail",
    "shipToAddress",
    "orderId",
    "orderLineNumber",
    "startDateOption",
    "isPermanent",
    "term",
    "expirationDate",
    "versionDate",
    "versionDateAttributes",
    "description",
    "numberOfCopies",
    "bulkEntitlementType",
    "autoDeploy",
    "entitledProducts",
    "channelPartners",
    "allowPortalLogin"
})
@XmlSeeAlso({
    BulkEntitlementDataType.class
})
public class CreateBulkEntitlementDataType {

    @XmlElement(required = true)
    protected IdType entitlementId;
    protected String soldTo;
    protected ProductIdentifierType product;
    protected PartNumberIdentifierType partNumber;
    protected LicenseModelIdentifierType licenseModel;
    protected LicenseModelIdentifierType alternateLicenseModel1;
    protected LicenseModelIdentifierType alternateLicenseModel2;
    protected AttributeDescriptorDataType licenseModelAttributes;
    @XmlElement(name = "FNPTimeZoneValue")
    protected String fnpTimeZoneValue;
    protected PolicyAttributesListType policyAttributes;
    protected String shipToEmail;
    protected String shipToAddress;
    protected String orderId;
    protected String orderLineNumber;
    @XmlSchemaType(name = "NMTOKEN")
    protected StartDateOptionType startDateOption;
    protected Boolean isPermanent;
    protected DurationType term;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar versionDate;
    protected VersionDateAttributesType versionDateAttributes;
    protected String description;
    protected BigInteger numberOfCopies;
    @XmlSchemaType(name = "NMTOKEN")
    protected BulkEntitlementType bulkEntitlementType;
    protected Boolean autoDeploy;
    protected EntitledProductDataListType entitledProducts;
    protected ChannelPartnerDataListType channelPartners;
    protected Boolean allowPortalLogin;

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link IdType }
     *     
     */
    public IdType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdType }
     *     
     */
    public void setEntitlementId(IdType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldTo(String value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setProduct(ProductIdentifierType value) {
        this.product = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setPartNumber(PartNumberIdentifierType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the licenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModel() {
        return licenseModel;
    }

    /**
     * Sets the value of the licenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModel(LicenseModelIdentifierType value) {
        this.licenseModel = value;
    }

    /**
     * Gets the value of the alternateLicenseModel1 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel1() {
        return alternateLicenseModel1;
    }

    /**
     * Sets the value of the alternateLicenseModel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel1(LicenseModelIdentifierType value) {
        this.alternateLicenseModel1 = value;
    }

    /**
     * Gets the value of the alternateLicenseModel2 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel2() {
        return alternateLicenseModel2;
    }

    /**
     * Sets the value of the alternateLicenseModel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel2(LicenseModelIdentifierType value) {
        this.alternateLicenseModel2 = value;
    }

    /**
     * Gets the value of the licenseModelAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLicenseModelAttributes() {
        return licenseModelAttributes;
    }

    /**
     * Sets the value of the licenseModelAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLicenseModelAttributes(AttributeDescriptorDataType value) {
        this.licenseModelAttributes = value;
    }

    /**
     * Gets the value of the fnpTimeZoneValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNPTimeZoneValue() {
        return fnpTimeZoneValue;
    }

    /**
     * Sets the value of the fnpTimeZoneValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNPTimeZoneValue(String value) {
        this.fnpTimeZoneValue = value;
    }

    /**
     * Gets the value of the policyAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyAttributesListType }
     *     
     */
    public PolicyAttributesListType getPolicyAttributes() {
        return policyAttributes;
    }

    /**
     * Sets the value of the policyAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyAttributesListType }
     *     
     */
    public void setPolicyAttributes(PolicyAttributesListType value) {
        this.policyAttributes = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToEmail(String value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddress(String value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineNumber(String value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the startDateOption property.
     * 
     * @return
     *     possible object is
     *     {@link StartDateOptionType }
     *     
     */
    public StartDateOptionType getStartDateOption() {
        return startDateOption;
    }

    /**
     * Sets the value of the startDateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link StartDateOptionType }
     *     
     */
    public void setStartDateOption(StartDateOptionType value) {
        this.startDateOption = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link DurationType }
     *     
     */
    public DurationType getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationType }
     *     
     */
    public void setTerm(DurationType value) {
        this.term = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersionDate(XMLGregorianCalendar value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the versionDateAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public VersionDateAttributesType getVersionDateAttributes() {
        return versionDateAttributes;
    }

    /**
     * Sets the value of the versionDateAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public void setVersionDateAttributes(VersionDateAttributesType value) {
        this.versionDateAttributes = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the numberOfCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * Sets the value of the numberOfCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCopies(BigInteger value) {
        this.numberOfCopies = value;
    }

    /**
     * Gets the value of the bulkEntitlementType property.
     * 
     * @return
     *     possible object is
     *     {@link BulkEntitlementType }
     *     
     */
    public BulkEntitlementType getBulkEntitlementType() {
        return bulkEntitlementType;
    }

    /**
     * Sets the value of the bulkEntitlementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BulkEntitlementType }
     *     
     */
    public void setBulkEntitlementType(BulkEntitlementType value) {
        this.bulkEntitlementType = value;
    }

    /**
     * Gets the value of the autoDeploy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoDeploy() {
        return autoDeploy;
    }

    /**
     * Sets the value of the autoDeploy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoDeploy(Boolean value) {
        this.autoDeploy = value;
    }

    /**
     * Gets the value of the entitledProducts property.
     * 
     * @return
     *     possible object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public EntitledProductDataListType getEntitledProducts() {
        return entitledProducts;
    }

    /**
     * Sets the value of the entitledProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public void setEntitledProducts(EntitledProductDataListType value) {
        this.entitledProducts = value;
    }

    /**
     * Gets the value of the channelPartners property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public ChannelPartnerDataListType getChannelPartners() {
        return channelPartners;
    }

    /**
     * Sets the value of the channelPartners property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public void setChannelPartners(ChannelPartnerDataListType value) {
        this.channelPartners = value;
    }

    /**
     * Gets the value of the allowPortalLogin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPortalLogin() {
        return allowPortalLogin;
    }

    /**
     * Sets the value of the allowPortalLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPortalLogin(Boolean value) {
        this.allowPortalLogin = value;
    }

}
