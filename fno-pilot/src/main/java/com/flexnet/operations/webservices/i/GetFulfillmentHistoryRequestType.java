
package com.flexnet.operations.webservices.i;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFulfillmentHistoryRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFulfillmentHistoryRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fulfillmentId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="userId" type="{urn:com.macrovision:flexnet/operations}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="count" type="{urn:com.macrovision:flexnet/operations}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="policyOverridden" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="actionDateTime" type="{urn:com.macrovision:flexnet/operations}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lifeCycleAction" type="{urn:com.macrovision:flexnet/operations}SupportLicenseType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillmentSource" type="{urn:com.macrovision:flexnet/operations}FulfillmentSourceType" minOccurs="0"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFulfillmentHistoryRequestType", propOrder = {
    "activationId",
    "fulfillmentId",
    "userId",
    "count",
    "policyOverridden",
    "actionDateTime",
    "lifeCycleAction",
    "fulfillmentSource",
    "pageNumber",
    "batchSize"
})
public class GetFulfillmentHistoryRequestType {

    @XmlElement(required = true)
    protected String activationId;
    protected SimpleQueryType fulfillmentId;
    protected SimpleQueryType userId;
    protected NumberQueryType count;
    protected Boolean policyOverridden;
    protected DateTimeQueryType actionDateTime;
    @XmlSchemaType(name = "NMTOKEN")
    protected SupportLicenseType lifeCycleAction;
    @XmlSchemaType(name = "NMTOKEN")
    protected FulfillmentSourceType fulfillmentSource;
    protected BigInteger pageNumber;
    protected BigInteger batchSize;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationId(String value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the fulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFulfillmentId() {
        return fulfillmentId;
    }

    /**
     * Sets the value of the fulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFulfillmentId(SimpleQueryType value) {
        this.fulfillmentId = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setUserId(SimpleQueryType value) {
        this.userId = value;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setCount(NumberQueryType value) {
        this.count = value;
    }

    /**
     * Gets the value of the policyOverridden property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPolicyOverridden() {
        return policyOverridden;
    }

    /**
     * Sets the value of the policyOverridden property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPolicyOverridden(Boolean value) {
        this.policyOverridden = value;
    }

    /**
     * Gets the value of the actionDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getActionDateTime() {
        return actionDateTime;
    }

    /**
     * Sets the value of the actionDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setActionDateTime(DateTimeQueryType value) {
        this.actionDateTime = value;
    }

    /**
     * Gets the value of the lifeCycleAction property.
     * 
     * @return
     *     possible object is
     *     {@link SupportLicenseType }
     *     
     */
    public SupportLicenseType getLifeCycleAction() {
        return lifeCycleAction;
    }

    /**
     * Sets the value of the lifeCycleAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupportLicenseType }
     *     
     */
    public void setLifeCycleAction(SupportLicenseType value) {
        this.lifeCycleAction = value;
    }

    /**
     * Gets the value of the fulfillmentSource property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }

    /**
     * Sets the value of the fulfillmentSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public void setFulfillmentSource(FulfillmentSourceType value) {
        this.fulfillmentSource = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

}
