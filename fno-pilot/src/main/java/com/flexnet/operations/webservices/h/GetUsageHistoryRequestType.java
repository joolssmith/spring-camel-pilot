
package com.flexnet.operations.webservices.h;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUsageHistoryRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getUsageHistoryRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:com.macrovision:flexnet/opsembedded}getUsageHistoryParametersType" minOccurs="0"/&gt;
 *         &lt;element name="queryConfig" type="{urn:com.macrovision:flexnet/opsembedded}getUsageHistoryConfigType" minOccurs="0"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUsageHistoryRequestType", propOrder = {
    "queryParams",
    "queryConfig",
    "pageNumber",
    "batchSize"
})
public class GetUsageHistoryRequestType {

    protected GetUsageHistoryParametersType queryParams;
    protected GetUsageHistoryConfigType queryConfig;
    @XmlElement(required = true, nillable = true)
    protected BigInteger pageNumber;
    @XmlElement(required = true, nillable = true)
    protected BigInteger batchSize;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link GetUsageHistoryParametersType }
     *     
     */
    public GetUsageHistoryParametersType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetUsageHistoryParametersType }
     *     
     */
    public void setQueryParams(GetUsageHistoryParametersType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the queryConfig property.
     * 
     * @return
     *     possible object is
     *     {@link GetUsageHistoryConfigType }
     *     
     */
    public GetUsageHistoryConfigType getQueryConfig() {
        return queryConfig;
    }

    /**
     * Sets the value of the queryConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetUsageHistoryConfigType }
     *     
     */
    public void setQueryConfig(GetUsageHistoryConfigType value) {
        this.queryConfig = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

}
