
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFulfillmentAttributesDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFulfillmentAttributesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentAttributes" type="{urn:com.macrovision:flexnet/operations}attributeMetaDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="needTimeZone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFulfillmentAttributesDataType", propOrder = {
    "fulfillmentAttributes",
    "needTimeZone"
})
public class GetFulfillmentAttributesDataType {

    protected AttributeMetaDescriptorDataType fulfillmentAttributes;
    protected Boolean needTimeZone;

    /**
     * Gets the value of the fulfillmentAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public AttributeMetaDescriptorDataType getFulfillmentAttributes() {
        return fulfillmentAttributes;
    }

    /**
     * Sets the value of the fulfillmentAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public void setFulfillmentAttributes(AttributeMetaDescriptorDataType value) {
        this.fulfillmentAttributes = value;
    }

    /**
     * Gets the value of the needTimeZone property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNeedTimeZone() {
        return needTimeZone;
    }

    /**
     * Sets the value of the needTimeZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNeedTimeZone(Boolean value) {
        this.needTimeZone = value;
    }

}
