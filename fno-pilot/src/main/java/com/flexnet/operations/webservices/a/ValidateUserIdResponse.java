
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ValidationInfo" type="{http://producersuite.flexerasoftware.com/EntitlementService/}UserValidationInfo"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "validationInfo"
})
@XmlRootElement(name = "validateUserIdResponse")
public class ValidateUserIdResponse {

    @XmlElement(name = "ValidationInfo", required = true)
    protected UserValidationInfo validationInfo;

    /**
     * Gets the value of the validationInfo property.
     * 
     * @return
     *     possible object is
     *     {@link UserValidationInfo }
     *     
     */
    public UserValidationInfo getValidationInfo() {
        return validationInfo;
    }

    /**
     * Sets the value of the validationInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserValidationInfo }
     *     
     */
    public void setValidationInfo(UserValidationInfo value) {
        this.validationInfo = value;
    }

}
