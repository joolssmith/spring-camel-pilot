
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maintenanceLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maintenanceLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:v2.webservices.operations.flexnet.com}createMaintenanceLineItemDataType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="state" type="{urn:v2.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maintenanceLineItemDataType", propOrder = {
    "state"
})
public class MaintenanceLineItemDataType
    extends CreateMaintenanceLineItemDataType
{

    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

}
