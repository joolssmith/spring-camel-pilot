
package com.flexnet.operations.webservices.q;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOrganizationCountRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getOrganizationCountRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v1.webservices.operations.flexnet.com}organizationQueryParametersType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getOrganizationCountRequestType", propOrder = {
    "queryParams"
})
public class GetOrganizationCountRequestType {

    protected OrganizationQueryParametersType queryParams;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationQueryParametersType }
     *     
     */
    public OrganizationQueryParametersType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationQueryParametersType }
     *     
     */
    public void setQueryParams(OrganizationQueryParametersType value) {
        this.queryParams = value;
    }

}
