
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for linkMaintenanceLineItemRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="linkMaintenanceLineItemRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="linkMaintenanceLineItemList" type="{urn:v3.webservices.operations.flexnet.com}linkMaintenanceLineItemListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linkMaintenanceLineItemRequestType", propOrder = {
    "linkMaintenanceLineItemList"
})
public class LinkMaintenanceLineItemRequestType {

    @XmlElement(required = true)
    protected LinkMaintenanceLineItemListType linkMaintenanceLineItemList;

    /**
     * Gets the value of the linkMaintenanceLineItemList property.
     * 
     * @return
     *     possible object is
     *     {@link LinkMaintenanceLineItemListType }
     *     
     */
    public LinkMaintenanceLineItemListType getLinkMaintenanceLineItemList() {
        return linkMaintenanceLineItemList;
    }

    /**
     * Sets the value of the linkMaintenanceLineItemList property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkMaintenanceLineItemListType }
     *     
     */
    public void setLinkMaintenanceLineItemList(LinkMaintenanceLineItemListType value) {
        this.linkMaintenanceLineItemList = value;
    }

}
