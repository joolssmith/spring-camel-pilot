
package com.flexnet.operations.webservices.o;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedDeleteWebRegKeyListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedDeleteWebRegKeyListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedWebRegKey" type="{urn:v1.webservices.operations.flexnet.com}failedDeleteWebRegKeyDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedDeleteWebRegKeyListType", propOrder = {
    "failedWebRegKey"
})
public class FailedDeleteWebRegKeyListType {

    protected List<FailedDeleteWebRegKeyDataType> failedWebRegKey;

    /**
     * Gets the value of the failedWebRegKey property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedWebRegKey property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedWebRegKey().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedDeleteWebRegKeyDataType }
     * 
     * 
     */
    public List<FailedDeleteWebRegKeyDataType> getFailedWebRegKey() {
        if (failedWebRegKey == null) {
            failedWebRegKey = new ArrayList<FailedDeleteWebRegKeyDataType>();
        }
        return this.failedWebRegKey;
    }

}
