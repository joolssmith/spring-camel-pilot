
package com.flexnet.operations.webservices.n;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for fulfillmentHistoryRecordType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fulfillmentHistoryRecordType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="action" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="actionDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="actionDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="actionPerformedBy" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="isPolicyOverridden" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="count" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="fulfillmentSource" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fulfillmentHistoryRecordType", propOrder = {
    "fulfillmentId",
    "action",
    "actionDate",
    "actionDateTime",
    "actionPerformedBy",
    "isPolicyOverridden",
    "count",
    "fulfillmentSource"
})
public class FulfillmentHistoryRecordType {

    @XmlElement(required = true)
    protected String fulfillmentId;
    @XmlElement(required = true)
    protected String action;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar actionDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar actionDateTime;
    @XmlElement(required = true)
    protected String actionPerformedBy;
    protected boolean isPolicyOverridden;
    @XmlElement(required = true)
    protected BigInteger count;
    @XmlElement(required = true)
    protected String fulfillmentSource;

    /**
     * Gets the value of the fulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentId() {
        return fulfillmentId;
    }

    /**
     * Sets the value of the fulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentId(String value) {
        this.fulfillmentId = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the actionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActionDate() {
        return actionDate;
    }

    /**
     * Sets the value of the actionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActionDate(XMLGregorianCalendar value) {
        this.actionDate = value;
    }

    /**
     * Gets the value of the actionDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getActionDateTime() {
        return actionDateTime;
    }

    /**
     * Sets the value of the actionDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setActionDateTime(XMLGregorianCalendar value) {
        this.actionDateTime = value;
    }

    /**
     * Gets the value of the actionPerformedBy property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionPerformedBy() {
        return actionPerformedBy;
    }

    /**
     * Sets the value of the actionPerformedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionPerformedBy(String value) {
        this.actionPerformedBy = value;
    }

    /**
     * Gets the value of the isPolicyOverridden property.
     * 
     */
    public boolean isIsPolicyOverridden() {
        return isPolicyOverridden;
    }

    /**
     * Sets the value of the isPolicyOverridden property.
     * 
     */
    public void setIsPolicyOverridden(boolean value) {
        this.isPolicyOverridden = value;
    }

    /**
     * Gets the value of the count property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCount(BigInteger value) {
        this.count = value;
    }

    /**
     * Gets the value of the fulfillmentSource property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentSource() {
        return fulfillmentSource;
    }

    /**
     * Sets the value of the fulfillmentSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentSource(String value) {
        this.fulfillmentSource = value;
    }

}
