
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyAttributesListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyAttributesListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rehostsPolicy" type="{urn:v3.webservices.operations.flexnet.com}policyDataType" minOccurs="0"/&gt;
 *         &lt;element name="returnsPolicy" type="{urn:v3.webservices.operations.flexnet.com}policyDataType" minOccurs="0"/&gt;
 *         &lt;element name="repairsPolicy" type="{urn:v3.webservices.operations.flexnet.com}policyDataType" minOccurs="0"/&gt;
 *         &lt;element name="extraActivationsPolicy" type="{urn:v3.webservices.operations.flexnet.com}extraActivationDataType" minOccurs="0"/&gt;
 *         &lt;element name="cancelLicensePolicy" type="{urn:v3.webservices.operations.flexnet.com}cancelLicensePolicyDataType" minOccurs="0"/&gt;
 *         &lt;element name="virtualLicensePolicy" type="{urn:v3.webservices.operations.flexnet.com}virtualLicensePolicyDataType" minOccurs="0"/&gt;
 *         &lt;element name="reinstallPolicy" type="{urn:v3.webservices.operations.flexnet.com}reinstallPolicyDataType" minOccurs="0"/&gt;
 *         &lt;element name="acpiGenerationIdLicensePolicy" type="{urn:v3.webservices.operations.flexnet.com}acpiGenerationIdLicensePolicyDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyAttributesListType", propOrder = {
    "rehostsPolicy",
    "returnsPolicy",
    "repairsPolicy",
    "extraActivationsPolicy",
    "cancelLicensePolicy",
    "virtualLicensePolicy",
    "reinstallPolicy",
    "acpiGenerationIdLicensePolicy"
})
public class PolicyAttributesListType {

    protected PolicyDataType rehostsPolicy;
    protected PolicyDataType returnsPolicy;
    protected PolicyDataType repairsPolicy;
    protected ExtraActivationDataType extraActivationsPolicy;
    protected CancelLicensePolicyDataType cancelLicensePolicy;
    protected VirtualLicensePolicyDataType virtualLicensePolicy;
    protected ReinstallPolicyDataType reinstallPolicy;
    protected AcpiGenerationIdLicensePolicyDataType acpiGenerationIdLicensePolicy;

    /**
     * Gets the value of the rehostsPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getRehostsPolicy() {
        return rehostsPolicy;
    }

    /**
     * Sets the value of the rehostsPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setRehostsPolicy(PolicyDataType value) {
        this.rehostsPolicy = value;
    }

    /**
     * Gets the value of the returnsPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getReturnsPolicy() {
        return returnsPolicy;
    }

    /**
     * Sets the value of the returnsPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setReturnsPolicy(PolicyDataType value) {
        this.returnsPolicy = value;
    }

    /**
     * Gets the value of the repairsPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getRepairsPolicy() {
        return repairsPolicy;
    }

    /**
     * Sets the value of the repairsPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setRepairsPolicy(PolicyDataType value) {
        this.repairsPolicy = value;
    }

    /**
     * Gets the value of the extraActivationsPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link ExtraActivationDataType }
     *     
     */
    public ExtraActivationDataType getExtraActivationsPolicy() {
        return extraActivationsPolicy;
    }

    /**
     * Sets the value of the extraActivationsPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtraActivationDataType }
     *     
     */
    public void setExtraActivationsPolicy(ExtraActivationDataType value) {
        this.extraActivationsPolicy = value;
    }

    /**
     * Gets the value of the cancelLicensePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link CancelLicensePolicyDataType }
     *     
     */
    public CancelLicensePolicyDataType getCancelLicensePolicy() {
        return cancelLicensePolicy;
    }

    /**
     * Sets the value of the cancelLicensePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link CancelLicensePolicyDataType }
     *     
     */
    public void setCancelLicensePolicy(CancelLicensePolicyDataType value) {
        this.cancelLicensePolicy = value;
    }

    /**
     * Gets the value of the virtualLicensePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link VirtualLicensePolicyDataType }
     *     
     */
    public VirtualLicensePolicyDataType getVirtualLicensePolicy() {
        return virtualLicensePolicy;
    }

    /**
     * Sets the value of the virtualLicensePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link VirtualLicensePolicyDataType }
     *     
     */
    public void setVirtualLicensePolicy(VirtualLicensePolicyDataType value) {
        this.virtualLicensePolicy = value;
    }

    /**
     * Gets the value of the reinstallPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link ReinstallPolicyDataType }
     *     
     */
    public ReinstallPolicyDataType getReinstallPolicy() {
        return reinstallPolicy;
    }

    /**
     * Sets the value of the reinstallPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReinstallPolicyDataType }
     *     
     */
    public void setReinstallPolicy(ReinstallPolicyDataType value) {
        this.reinstallPolicy = value;
    }

    /**
     * Gets the value of the acpiGenerationIdLicensePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link AcpiGenerationIdLicensePolicyDataType }
     *     
     */
    public AcpiGenerationIdLicensePolicyDataType getAcpiGenerationIdLicensePolicy() {
        return acpiGenerationIdLicensePolicy;
    }

    /**
     * Sets the value of the acpiGenerationIdLicensePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcpiGenerationIdLicensePolicyDataType }
     *     
     */
    public void setAcpiGenerationIdLicensePolicy(AcpiGenerationIdLicensePolicyDataType value) {
        this.acpiGenerationIdLicensePolicy = value;
    }

}
