
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cancelLicensePolicyDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cancelLicensePolicyDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isCancelLicense" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cancelLicensePolicyDataType", propOrder = {
    "isCancelLicense"
})
public class CancelLicensePolicyDataType {

    protected boolean isCancelLicense;

    /**
     * Gets the value of the isCancelLicense property.
     * 
     */
    public boolean isIsCancelLicense() {
        return isCancelLicense;
    }

    /**
     * Sets the value of the isCancelLicense property.
     * 
     */
    public void setIsCancelLicense(boolean value) {
        this.isCancelLicense = value;
    }

}
