
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for partNumberDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="partNumberDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="partNumberIdentifier" type="{urn:v2.webservices.operations.flexnet.com}partNumberIdentifierType"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="creationDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="lastModifiedDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="mappedProduct" type="{urn:v2.webservices.operations.flexnet.com}productIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="mappedLicenseModel" type="{urn:v2.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "partNumberDataType", propOrder = {
    "partNumberIdentifier",
    "description",
    "creationDate",
    "lastModifiedDate",
    "mappedProduct",
    "mappedLicenseModel"
})
public class PartNumberDataType {

    @XmlElement(required = true)
    protected PartNumberIdentifierType partNumberIdentifier;
    @XmlElement(required = true, nillable = true)
    protected String description;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creationDate;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar lastModifiedDate;
    protected ProductIdentifierType mappedProduct;
    protected LicenseModelIdentifierType mappedLicenseModel;

    /**
     * Gets the value of the partNumberIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getPartNumberIdentifier() {
        return partNumberIdentifier;
    }

    /**
     * Sets the value of the partNumberIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setPartNumberIdentifier(PartNumberIdentifierType value) {
        this.partNumberIdentifier = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationDate(XMLGregorianCalendar value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the lastModifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets the value of the lastModifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDate(XMLGregorianCalendar value) {
        this.lastModifiedDate = value;
    }

    /**
     * Gets the value of the mappedProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getMappedProduct() {
        return mappedProduct;
    }

    /**
     * Sets the value of the mappedProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setMappedProduct(ProductIdentifierType value) {
        this.mappedProduct = value;
    }

    /**
     * Gets the value of the mappedLicenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getMappedLicenseModel() {
        return mappedLicenseModel;
    }

    /**
     * Sets the value of the mappedLicenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setMappedLicenseModel(LicenseModelIdentifierType value) {
        this.mappedLicenseModel = value;
    }

}
