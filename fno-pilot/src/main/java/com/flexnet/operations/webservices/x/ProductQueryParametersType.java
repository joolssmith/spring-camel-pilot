
package com.flexnet.operations.webservices.x;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for productQueryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="productQueryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productName" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="version" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v2.webservices.operations.flexnet.com}StateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="creationDate" type="{urn:v2.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDate" type="{urn:v2.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="hostType" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="usedOnDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productAttributes" type="{urn:v2.webservices.operations.flexnet.com}prodCustomAttributesQueryListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "productQueryParametersType", propOrder = {
    "productName",
    "version",
    "description",
    "partNumber",
    "state",
    "creationDate",
    "lastModifiedDate",
    "licenseTechnology",
    "hostType",
    "usedOnDevice",
    "productAttributes"
})
public class ProductQueryParametersType {

    @XmlElementRef(name = "productName", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> productName;
    @XmlElementRef(name = "version", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> version;
    @XmlElementRef(name = "description", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> description;
    @XmlElementRef(name = "partNumber", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> partNumber;
    @XmlElementRef(name = "state", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<StateQueryType> state;
    @XmlElementRef(name = "creationDate", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<DateQueryType> creationDate;
    @XmlElementRef(name = "lastModifiedDate", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<DateQueryType> lastModifiedDate;
    protected SimpleQueryType licenseTechnology;
    @XmlElementRef(name = "hostType", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<SimpleQueryType> hostType;
    @XmlElementRef(name = "usedOnDevice", namespace = "urn:v2.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> usedOnDevice;
    protected ProdCustomAttributesQueryListType productAttributes;

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setProductName(JAXBElement<SimpleQueryType> value) {
        this.productName = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setVersion(JAXBElement<SimpleQueryType> value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setDescription(JAXBElement<SimpleQueryType> value) {
        this.description = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setPartNumber(JAXBElement<SimpleQueryType> value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}
     *     
     */
    public JAXBElement<StateQueryType> getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StateQueryType }{@code >}
     *     
     */
    public void setState(JAXBElement<StateQueryType> value) {
        this.state = value;
    }

    /**
     * Gets the value of the creationDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public JAXBElement<DateQueryType> getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the value of the creationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public void setCreationDate(JAXBElement<DateQueryType> value) {
        this.creationDate = value;
    }

    /**
     * Gets the value of the lastModifiedDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public JAXBElement<DateQueryType> getLastModifiedDate() {
        return lastModifiedDate;
    }

    /**
     * Sets the value of the lastModifiedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DateQueryType }{@code >}
     *     
     */
    public void setLastModifiedDate(JAXBElement<DateQueryType> value) {
        this.lastModifiedDate = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setLicenseTechnology(SimpleQueryType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the hostType property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public JAXBElement<SimpleQueryType> getHostType() {
        return hostType;
    }

    /**
     * Sets the value of the hostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link SimpleQueryType }{@code >}
     *     
     */
    public void setHostType(JAXBElement<SimpleQueryType> value) {
        this.hostType = value;
    }

    /**
     * Gets the value of the usedOnDevice property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getUsedOnDevice() {
        return usedOnDevice;
    }

    /**
     * Sets the value of the usedOnDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setUsedOnDevice(JAXBElement<Boolean> value) {
        this.usedOnDevice = value;
    }

    /**
     * Gets the value of the productAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link ProdCustomAttributesQueryListType }
     *     
     */
    public ProdCustomAttributesQueryListType getProductAttributes() {
        return productAttributes;
    }

    /**
     * Sets the value of the productAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProdCustomAttributesQueryListType }
     *     
     */
    public void setProductAttributes(ProdCustomAttributesQueryListType value) {
        this.productAttributes = value;
    }

}
