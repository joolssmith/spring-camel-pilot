
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="PROGRAMMATIC"/&gt;
 *     &lt;enumeration value="MANUAL"/&gt;
 *     &lt;enumeration value="SHORT_CODE"/&gt;
 *     &lt;enumeration value="DEFAULT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActivationType")
@XmlEnum
public enum ActivationType {

    PROGRAMMATIC,
    MANUAL,
    SHORT_CODE,
    DEFAULT;

    public String value() {
        return name();
    }

    public static ActivationType fromValue(String v) {
        return valueOf(v);
    }

}
