
package com.flexnet.operations.webservices.n;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for publisherErrorFulfillmentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="publisherErrorFulfillmentDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentIdentifier" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentIdentifierType"/&gt;
 *         &lt;element name="licenseModelAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="FNPTimeZoneValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serverIds" type="{urn:v1.webservices.operations.flexnet.com}ServerIDsType" minOccurs="0"/&gt;
 *         &lt;element name="nodeIds" type="{urn:v1.webservices.operations.flexnet.com}NodeIDsType" minOccurs="0"/&gt;
 *         &lt;element name="customHost" type="{urn:v1.webservices.operations.flexnet.com}CustomHostIDType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="overridePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "publisherErrorFulfillmentDataType", propOrder = {
    "fulfillmentIdentifier",
    "licenseModelAttributes",
    "fnpTimeZoneValue",
    "shipToEmail",
    "shipToAddress",
    "serverIds",
    "nodeIds",
    "customHost",
    "fulfillCount",
    "overridePolicy"
})
public class PublisherErrorFulfillmentDataType {

    @XmlElement(required = true)
    protected FulfillmentIdentifierType fulfillmentIdentifier;
    protected AttributeDescriptorDataType licenseModelAttributes;
    @XmlElement(name = "FNPTimeZoneValue")
    protected String fnpTimeZoneValue;
    protected String shipToEmail;
    protected String shipToAddress;
    protected ServerIDsType serverIds;
    protected NodeIDsType nodeIds;
    protected CustomHostIDType customHost;
    protected BigInteger fulfillCount;
    protected Boolean overridePolicy;

    /**
     * Gets the value of the fulfillmentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }

    /**
     * Sets the value of the fulfillmentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setFulfillmentIdentifier(FulfillmentIdentifierType value) {
        this.fulfillmentIdentifier = value;
    }

    /**
     * Gets the value of the licenseModelAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLicenseModelAttributes() {
        return licenseModelAttributes;
    }

    /**
     * Sets the value of the licenseModelAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLicenseModelAttributes(AttributeDescriptorDataType value) {
        this.licenseModelAttributes = value;
    }

    /**
     * Gets the value of the fnpTimeZoneValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNPTimeZoneValue() {
        return fnpTimeZoneValue;
    }

    /**
     * Sets the value of the fnpTimeZoneValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNPTimeZoneValue(String value) {
        this.fnpTimeZoneValue = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToEmail(String value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddress(String value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the serverIds property.
     * 
     * @return
     *     possible object is
     *     {@link ServerIDsType }
     *     
     */
    public ServerIDsType getServerIds() {
        return serverIds;
    }

    /**
     * Sets the value of the serverIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerIDsType }
     *     
     */
    public void setServerIds(ServerIDsType value) {
        this.serverIds = value;
    }

    /**
     * Gets the value of the nodeIds property.
     * 
     * @return
     *     possible object is
     *     {@link NodeIDsType }
     *     
     */
    public NodeIDsType getNodeIds() {
        return nodeIds;
    }

    /**
     * Sets the value of the nodeIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeIDsType }
     *     
     */
    public void setNodeIds(NodeIDsType value) {
        this.nodeIds = value;
    }

    /**
     * Gets the value of the customHost property.
     * 
     * @return
     *     possible object is
     *     {@link CustomHostIDType }
     *     
     */
    public CustomHostIDType getCustomHost() {
        return customHost;
    }

    /**
     * Sets the value of the customHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomHostIDType }
     *     
     */
    public void setCustomHost(CustomHostIDType value) {
        this.customHost = value;
    }

    /**
     * Gets the value of the fulfillCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getFulfillCount() {
        return fulfillCount;
    }

    /**
     * Sets the value of the fulfillCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setFulfillCount(BigInteger value) {
        this.fulfillCount = value;
    }

    /**
     * Gets the value of the overridePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverridePolicy() {
        return overridePolicy;
    }

    /**
     * Sets the value of the overridePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverridePolicy(Boolean value) {
        this.overridePolicy = value;
    }

}
