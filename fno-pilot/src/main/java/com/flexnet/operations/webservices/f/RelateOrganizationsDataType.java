
package com.flexnet.operations.webservices.f;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for relateOrganizationsDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="relateOrganizationsDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="organizationToRelate" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType"/&gt;
 *         &lt;element name="relatedOrganization" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "relateOrganizationsDataType", propOrder = {
    "organizationToRelate",
    "relatedOrganization"
})
public class RelateOrganizationsDataType {

    @XmlElement(required = true)
    protected OrganizationIdentifierType organizationToRelate;
    @XmlElement(required = true)
    protected OrganizationIdentifierType relatedOrganization;

    /**
     * Gets the value of the organizationToRelate property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getOrganizationToRelate() {
        return organizationToRelate;
    }

    /**
     * Sets the value of the organizationToRelate property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setOrganizationToRelate(OrganizationIdentifierType value) {
        this.organizationToRelate = value;
    }

    /**
     * Gets the value of the relatedOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getRelatedOrganization() {
        return relatedOrganization;
    }

    /**
     * Sets the value of the relatedOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setRelatedOrganization(OrganizationIdentifierType value) {
        this.relatedOrganization = value;
    }

}
