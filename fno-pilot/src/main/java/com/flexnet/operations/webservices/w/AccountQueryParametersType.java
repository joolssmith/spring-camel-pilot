
package com.flexnet.operations.webservices.w;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for accountQueryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="accountQueryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountID" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="accountName" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="address1" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="address2" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="city" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="zipcode" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="country" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="region" type="{urn:v2.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="onlyRootAccounts" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="accountType" type="{urn:v2.webservices.operations.flexnet.com}accountType" minOccurs="0"/&gt;
 *         &lt;element name="accountTypeList" type="{urn:v2.webservices.operations.flexnet.com}accountTypeList" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v2.webservices.operations.flexnet.com}acctCustomAttributesQueryListType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{urn:v2.webservices.operations.flexnet.com}DateTimeQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "accountQueryParametersType", propOrder = {
    "accountID",
    "accountName",
    "description",
    "address1",
    "address2",
    "city",
    "state",
    "zipcode",
    "country",
    "region",
    "onlyRootAccounts",
    "accountType",
    "accountTypeList",
    "customAttributes",
    "lastModifiedDateTime"
})
public class AccountQueryParametersType {

    protected SimpleQueryType accountID;
    protected SimpleQueryType accountName;
    protected SimpleQueryType description;
    protected SimpleQueryType address1;
    protected SimpleQueryType address2;
    protected SimpleQueryType city;
    protected SimpleQueryType state;
    protected SimpleQueryType zipcode;
    protected SimpleQueryType country;
    protected SimpleQueryType region;
    protected Boolean onlyRootAccounts;
    @XmlSchemaType(name = "NMTOKEN")
    protected AccountType accountType;
    protected AccountTypeList accountTypeList;
    protected AcctCustomAttributesQueryListType customAttributes;
    protected DateTimeQueryType lastModifiedDateTime;

    /**
     * Gets the value of the accountID property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAccountID() {
        return accountID;
    }

    /**
     * Sets the value of the accountID property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAccountID(SimpleQueryType value) {
        this.accountID = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAccountName(SimpleQueryType value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDescription(SimpleQueryType value) {
        this.description = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddress1(SimpleQueryType value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddress2(SimpleQueryType value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCity(SimpleQueryType value) {
        this.city = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setState(SimpleQueryType value) {
        this.state = value;
    }

    /**
     * Gets the value of the zipcode property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getZipcode() {
        return zipcode;
    }

    /**
     * Sets the value of the zipcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setZipcode(SimpleQueryType value) {
        this.zipcode = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setCountry(SimpleQueryType value) {
        this.country = value;
    }

    /**
     * Gets the value of the region property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setRegion(SimpleQueryType value) {
        this.region = value;
    }

    /**
     * Gets the value of the onlyRootAccounts property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOnlyRootAccounts() {
        return onlyRootAccounts;
    }

    /**
     * Sets the value of the onlyRootAccounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOnlyRootAccounts(Boolean value) {
        this.onlyRootAccounts = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link AccountType }
     *     
     */
    public AccountType getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountType }
     *     
     */
    public void setAccountType(AccountType value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the accountTypeList property.
     * 
     * @return
     *     possible object is
     *     {@link AccountTypeList }
     *     
     */
    public AccountTypeList getAccountTypeList() {
        return accountTypeList;
    }

    /**
     * Sets the value of the accountTypeList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountTypeList }
     *     
     */
    public void setAccountTypeList(AccountTypeList value) {
        this.accountTypeList = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AcctCustomAttributesQueryListType }
     *     
     */
    public AcctCustomAttributesQueryListType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AcctCustomAttributesQueryListType }
     *     
     */
    public void setCustomAttributes(AcctCustomAttributesQueryListType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setLastModifiedDateTime(DateTimeQueryType value) {
        this.lastModifiedDateTime = value;
    }

}
