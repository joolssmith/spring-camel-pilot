
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMatchingBulkEntsListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMatchingBulkEntsListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntInfo" type="{urn:v2.webservices.operations.flexnet.com}getMatchingBulkEntInfoType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMatchingBulkEntsListType", propOrder = {
    "bulkEntInfo"
})
public class GetMatchingBulkEntsListType {

    @XmlElement(required = true)
    protected List<GetMatchingBulkEntInfoType> bulkEntInfo;

    /**
     * Gets the value of the bulkEntInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the bulkEntInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBulkEntInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GetMatchingBulkEntInfoType }
     * 
     * 
     */
    public List<GetMatchingBulkEntInfoType> getBulkEntInfo() {
        if (bulkEntInfo == null) {
            bulkEntInfo = new ArrayList<GetMatchingBulkEntInfoType>();
        }
        return this.bulkEntInfo;
    }

}
