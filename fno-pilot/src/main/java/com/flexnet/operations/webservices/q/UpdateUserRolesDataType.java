
package com.flexnet.operations.webservices.q;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateUserRolesDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateUserRolesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="user" type="{urn:v1.webservices.operations.flexnet.com}userIdentifierType"/&gt;
 *         &lt;element name="orgRoles" type="{urn:v1.webservices.operations.flexnet.com}updateUserRolesOrganizationDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateUserRolesDataType", propOrder = {
    "user",
    "orgRoles"
})
public class UpdateUserRolesDataType {

    @XmlElement(required = true)
    protected UserIdentifierType user;
    @XmlElement(required = true)
    protected UpdateUserRolesOrganizationDataType orgRoles;

    /**
     * Gets the value of the user property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifierType }
     *     
     */
    public UserIdentifierType getUser() {
        return user;
    }

    /**
     * Sets the value of the user property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifierType }
     *     
     */
    public void setUser(UserIdentifierType value) {
        this.user = value;
    }

    /**
     * Gets the value of the orgRoles property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateUserRolesOrganizationDataType }
     *     
     */
    public UpdateUserRolesOrganizationDataType getOrgRoles() {
        return orgRoles;
    }

    /**
     * Sets the value of the orgRoles property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateUserRolesOrganizationDataType }
     *     
     */
    public void setOrgRoles(UpdateUserRolesOrganizationDataType value) {
        this.orgRoles = value;
    }

}
