package com.flexnet.operations.webservices.d;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 3.2.4
 * 2018-05-09T18:59:00.799+01:00
 * Generated source version: 3.2.4
 *
 */
@WebService(targetNamespace = "urn:com.macrovision:flexnet/platform", name = "FlexnetAuthentication")
@XmlSeeAlso({ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface FlexnetAuthentication {

    @WebMethod
    @WebResult(name = "AuthenticateUserReturn", targetNamespace = "urn:com.macrovision:flexnet/platform", partName = "AuthenticateUserReturn")
    public AuthenticateUserReturnType authenticateUser(
        @WebParam(partName = "AuthenticateUserInput", name = "AuthenticateUserInput", targetNamespace = "urn:com.macrovision:flexnet/platform")
        AuthenticateUserInputType authenticateUserInput
    );

    @WebMethod
    @WebResult(name = "UserTokenReturn", targetNamespace = "urn:com.macrovision:flexnet/platform", partName = "UserTokenReturn")
    public UserTokenReturnType getUserToken(
        @WebParam(partName = "UserTokenInput", name = "UserTokenInput", targetNamespace = "urn:com.macrovision:flexnet/platform")
        java.lang.String userTokenInput
    );

    @WebMethod
    @WebResult(name = "secureTokenResponse", targetNamespace = "urn:com.macrovision:flexnet/platform", partName = "secureTokenResponse")
    public TokenResponseType getSecureToken(
        @WebParam(partName = "secureTokenRequest", name = "secureTokenRequest", targetNamespace = "urn:com.macrovision:flexnet/platform")
        IdentityType secureTokenRequest
    );

    @WebMethod
    @WebResult(name = "validateTokenResponse", targetNamespace = "urn:com.macrovision:flexnet/platform", partName = "validateTokenResponse")
    public StatusResponse validateToken(
        @WebParam(partName = "validateTokenRequest", name = "validateTokenRequest", targetNamespace = "urn:com.macrovision:flexnet/platform")
        TokenType validateTokenRequest
    );
}
