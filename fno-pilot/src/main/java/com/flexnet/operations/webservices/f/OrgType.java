
package com.flexnet.operations.webservices.f;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrgType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrgType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="CUSTOMER"/&gt;
 *     &lt;enumeration value="PUBLISHER"/&gt;
 *     &lt;enumeration value="CHANNEL_PARTNER"/&gt;
 *     &lt;enumeration value="SELF_REGISTERED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "OrgType")
@XmlEnum
public enum OrgType {

    CUSTOMER,
    PUBLISHER,
    CHANNEL_PARTNER,
    SELF_REGISTERED;

    public String value() {
        return name();
    }

    public static OrgType fromValue(String v) {
        return valueOf(v);
    }

}
