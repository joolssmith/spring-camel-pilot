
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for versionDateAttributesType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="versionDateAttributesType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="versionOption" type="{urn:v2.webservices.operations.flexnet.com}VersionDateOptionType"/&gt;
 *         &lt;element name="duration" type="{urn:v2.webservices.operations.flexnet.com}DurationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "versionDateAttributesType", propOrder = {
    "versionOption",
    "duration"
})
public class VersionDateAttributesType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected VersionDateOptionType versionOption;
    protected DurationType duration;

    /**
     * Gets the value of the versionOption property.
     * 
     * @return
     *     possible object is
     *     {@link VersionDateOptionType }
     *     
     */
    public VersionDateOptionType getVersionOption() {
        return versionOption;
    }

    /**
     * Sets the value of the versionOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionDateOptionType }
     *     
     */
    public void setVersionOption(VersionDateOptionType value) {
        this.versionOption = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link DurationType }
     *     
     */
    public DurationType getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationType }
     *     
     */
    public void setDuration(DurationType value) {
        this.duration = value;
    }

}
