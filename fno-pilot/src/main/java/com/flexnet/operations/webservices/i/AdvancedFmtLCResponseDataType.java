
package com.flexnet.operations.webservices.i;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for advancedFmtLCResponseDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="advancedFmtLCResponseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="recordRefNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="upgradedFulfillmentInfo" type="{urn:com.macrovision:flexnet/operations}advancedFulfillmentLCInfoType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "advancedFmtLCResponseDataType", propOrder = {
    "recordRefNo",
    "upgradedFulfillmentInfo"
})
public class AdvancedFmtLCResponseDataType {

    @XmlElement(required = true)
    protected BigInteger recordRefNo;
    @XmlElement(required = true)
    protected List<AdvancedFulfillmentLCInfoType> upgradedFulfillmentInfo;

    /**
     * Gets the value of the recordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRecordRefNo() {
        return recordRefNo;
    }

    /**
     * Sets the value of the recordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRecordRefNo(BigInteger value) {
        this.recordRefNo = value;
    }

    /**
     * Gets the value of the upgradedFulfillmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the upgradedFulfillmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUpgradedFulfillmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AdvancedFulfillmentLCInfoType }
     * 
     * 
     */
    public List<AdvancedFulfillmentLCInfoType> getUpgradedFulfillmentInfo() {
        if (upgradedFulfillmentInfo == null) {
            upgradedFulfillmentInfo = new ArrayList<AdvancedFulfillmentLCInfoType>();
        }
        return this.upgradedFulfillmentInfo;
    }

}
