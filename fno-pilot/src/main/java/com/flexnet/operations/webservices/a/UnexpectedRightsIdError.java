
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnexpectedRightsIdError complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnexpectedRightsIdError"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://producersuite.flexerasoftware.com/EntitlementService/}RightsIdProblem"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UnexpectedError" type="{http://producersuite.flexerasoftware.com/EntitlementService/}UnexpectedError"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnexpectedRightsIdError", propOrder = {
    "unexpectedError"
})
public class UnexpectedRightsIdError
    extends RightsIdProblem
{

    @XmlElement(name = "UnexpectedError", required = true, nillable = true)
    protected UnexpectedError unexpectedError;

    /**
     * Gets the value of the unexpectedError property.
     * 
     * @return
     *     possible object is
     *     {@link UnexpectedError }
     *     
     */
    public UnexpectedError getUnexpectedError() {
        return unexpectedError;
    }

    /**
     * Sets the value of the unexpectedError property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnexpectedError }
     *     
     */
    public void setUnexpectedError(UnexpectedError value) {
        this.unexpectedError = value;
    }

}
