
package com.flexnet.operations.webservices.w;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateUserAccountsListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateUserAccountsListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="acctRoles" type="{urn:v2.webservices.operations.flexnet.com}updateUserAccountType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="opType" type="{urn:v2.webservices.operations.flexnet.com}CollectionOperationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateUserAccountsListType", propOrder = {
    "acctRoles",
    "opType"
})
public class UpdateUserAccountsListType {

    @XmlElement(required = true)
    protected List<UpdateUserAccountType> acctRoles;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected CollectionOperationType opType;

    /**
     * Gets the value of the acctRoles property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acctRoles property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcctRoles().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateUserAccountType }
     * 
     * 
     */
    public List<UpdateUserAccountType> getAcctRoles() {
        if (acctRoles == null) {
            acctRoles = new ArrayList<UpdateUserAccountType>();
        }
        return this.acctRoles;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link CollectionOperationType }
     *     
     */
    public CollectionOperationType getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionOperationType }
     *     
     */
    public void setOpType(CollectionOperationType value) {
        this.opType = value;
    }

}
