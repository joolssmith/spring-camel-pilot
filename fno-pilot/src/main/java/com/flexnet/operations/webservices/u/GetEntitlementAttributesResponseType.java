
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getEntitlementAttributesResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getEntitlementAttributesResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v2.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="entitlementAttributes" type="{urn:v2.webservices.operations.flexnet.com}attributeMetaDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="policyAttributes" type="{urn:v2.webservices.operations.flexnet.com}policyAttributesDataType" minOccurs="0"/&gt;
 *         &lt;element name="needTimeZone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEntitlementAttributesResponseType", propOrder = {
    "statusInfo",
    "entitlementAttributes",
    "policyAttributes",
    "needTimeZone"
})
public class GetEntitlementAttributesResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected AttributeMetaDescriptorDataType entitlementAttributes;
    protected PolicyAttributesDataType policyAttributes;
    protected Boolean needTimeZone;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the entitlementAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public AttributeMetaDescriptorDataType getEntitlementAttributes() {
        return entitlementAttributes;
    }

    /**
     * Sets the value of the entitlementAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public void setEntitlementAttributes(AttributeMetaDescriptorDataType value) {
        this.entitlementAttributes = value;
    }

    /**
     * Gets the value of the policyAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyAttributesDataType }
     *     
     */
    public PolicyAttributesDataType getPolicyAttributes() {
        return policyAttributes;
    }

    /**
     * Sets the value of the policyAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyAttributesDataType }
     *     
     */
    public void setPolicyAttributes(PolicyAttributesDataType value) {
        this.policyAttributes = value;
    }

    /**
     * Gets the value of the needTimeZone property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNeedTimeZone() {
        return needTimeZone;
    }

    /**
     * Sets the value of the needTimeZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNeedTimeZone(Boolean value) {
        this.needTimeZone = value;
    }

}
