
package com.flexnet.operations.webservices.n;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rehostFulfillmentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rehostFulfillmentDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentIdentifier" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentIdentifierType"/&gt;
 *         &lt;element name="serverIds" type="{urn:v1.webservices.operations.flexnet.com}ServerIDsType" minOccurs="0"/&gt;
 *         &lt;element name="nodeIds" type="{urn:v1.webservices.operations.flexnet.com}NodeIDsType" minOccurs="0"/&gt;
 *         &lt;element name="customHost" type="{urn:v1.webservices.operations.flexnet.com}CustomHostIDType" minOccurs="0"/&gt;
 *         &lt;element name="partialCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="overDraftCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="overridePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rehostFulfillmentDataType", propOrder = {
    "fulfillmentIdentifier",
    "serverIds",
    "nodeIds",
    "customHost",
    "partialCount",
    "overDraftCount",
    "overridePolicy"
})
public class RehostFulfillmentDataType {

    @XmlElement(required = true)
    protected FulfillmentIdentifierType fulfillmentIdentifier;
    protected ServerIDsType serverIds;
    protected NodeIDsType nodeIds;
    protected CustomHostIDType customHost;
    protected BigInteger partialCount;
    protected BigInteger overDraftCount;
    protected Boolean overridePolicy;

    /**
     * Gets the value of the fulfillmentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }

    /**
     * Sets the value of the fulfillmentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setFulfillmentIdentifier(FulfillmentIdentifierType value) {
        this.fulfillmentIdentifier = value;
    }

    /**
     * Gets the value of the serverIds property.
     * 
     * @return
     *     possible object is
     *     {@link ServerIDsType }
     *     
     */
    public ServerIDsType getServerIds() {
        return serverIds;
    }

    /**
     * Sets the value of the serverIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerIDsType }
     *     
     */
    public void setServerIds(ServerIDsType value) {
        this.serverIds = value;
    }

    /**
     * Gets the value of the nodeIds property.
     * 
     * @return
     *     possible object is
     *     {@link NodeIDsType }
     *     
     */
    public NodeIDsType getNodeIds() {
        return nodeIds;
    }

    /**
     * Sets the value of the nodeIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeIDsType }
     *     
     */
    public void setNodeIds(NodeIDsType value) {
        this.nodeIds = value;
    }

    /**
     * Gets the value of the customHost property.
     * 
     * @return
     *     possible object is
     *     {@link CustomHostIDType }
     *     
     */
    public CustomHostIDType getCustomHost() {
        return customHost;
    }

    /**
     * Sets the value of the customHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomHostIDType }
     *     
     */
    public void setCustomHost(CustomHostIDType value) {
        this.customHost = value;
    }

    /**
     * Gets the value of the partialCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPartialCount() {
        return partialCount;
    }

    /**
     * Sets the value of the partialCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPartialCount(BigInteger value) {
        this.partialCount = value;
    }

    /**
     * Gets the value of the overDraftCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOverDraftCount() {
        return overDraftCount;
    }

    /**
     * Sets the value of the overDraftCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOverDraftCount(BigInteger value) {
        this.overDraftCount = value;
    }

    /**
     * Gets the value of the overridePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverridePolicy() {
        return overridePolicy;
    }

    /**
     * Sets the value of the overridePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverridePolicy(Boolean value) {
        this.overridePolicy = value;
    }

}
