
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDeviceRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDeviceRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceId" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceId"/&gt;
 *         &lt;element name="responseConfig" type="{urn:v3.fne.webservices.operations.flexnet.com}getDeviceResponseConfigType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDeviceRequestType", propOrder = {
    "deviceId",
    "responseConfig"
})
public class GetDeviceRequestType {

    @XmlElement(required = true)
    protected DeviceId deviceId;
    protected GetDeviceResponseConfigType responseConfig;

    /**
     * Gets the value of the deviceId property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceId }
     *     
     */
    public DeviceId getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceId }
     *     
     */
    public void setDeviceId(DeviceId value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the responseConfig property.
     * 
     * @return
     *     possible object is
     *     {@link GetDeviceResponseConfigType }
     *     
     */
    public GetDeviceResponseConfigType getResponseConfig() {
        return responseConfig;
    }

    /**
     * Sets the value of the responseConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDeviceResponseConfigType }
     *     
     */
    public void setResponseConfig(GetDeviceResponseConfigType value) {
        this.responseConfig = value;
    }

}
