
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUsageSummaryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getUsageSummaryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="activationId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="entitlementUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="usedCount" type="{urn:com.macrovision:flexnet/opsembedded}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="usedExceedsOrderedBy" type="{urn:com.macrovision:flexnet/opsembedded}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="usedExceedsProvisionedBy" type="{urn:com.macrovision:flexnet/opsembedded}NumberQueryType" minOccurs="0"/&gt;
 *         &lt;element name="organizationUnitName" type="{urn:com.macrovision:flexnet/opsembedded}PartnerTierQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldToUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="orderableUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="orderableName" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="orderableVersion" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="serverUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="serverId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="serverAlias" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="summaryTimeUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="summaryTime" type="{urn:com.macrovision:flexnet/opsembedded}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="groupBy" type="{urn:com.macrovision:flexnet/opsembedded}usageSummaryGroupByType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUsageSummaryParametersType", propOrder = {
    "activationUniqueId",
    "activationId",
    "entitlementUniqueId",
    "entitlementId",
    "usedCount",
    "usedExceedsOrderedBy",
    "usedExceedsProvisionedBy",
    "organizationUnitName",
    "soldToUniqueId",
    "orderableUniqueId",
    "orderableName",
    "orderableVersion",
    "serverUniqueId",
    "serverId",
    "serverAlias",
    "summaryTimeUniqueId",
    "summaryTime",
    "groupBy"
})
public class GetUsageSummaryParametersType {

    protected ExternalIdQueryType activationUniqueId;
    protected SimpleQueryType activationId;
    protected ExternalIdQueryType entitlementUniqueId;
    protected SimpleQueryType entitlementId;
    protected NumberQueryType usedCount;
    protected NumberQueryType usedExceedsOrderedBy;
    protected NumberQueryType usedExceedsProvisionedBy;
    protected PartnerTierQueryType organizationUnitName;
    protected ExternalIdQueryType soldToUniqueId;
    protected ExternalIdQueryType orderableUniqueId;
    protected SimpleQueryType orderableName;
    protected SimpleQueryType orderableVersion;
    protected ExternalIdQueryType serverUniqueId;
    protected SimpleQueryType serverId;
    protected SimpleQueryType serverAlias;
    protected ExternalIdQueryType summaryTimeUniqueId;
    protected DateTimeQueryType summaryTime;
    @XmlSchemaType(name = "NMTOKEN")
    protected UsageSummaryGroupByType groupBy;

    /**
     * Gets the value of the activationUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getActivationUniqueId() {
        return activationUniqueId;
    }

    /**
     * Sets the value of the activationUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setActivationUniqueId(ExternalIdQueryType value) {
        this.activationUniqueId = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setActivationId(SimpleQueryType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the entitlementUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getEntitlementUniqueId() {
        return entitlementUniqueId;
    }

    /**
     * Sets the value of the entitlementUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setEntitlementUniqueId(ExternalIdQueryType value) {
        this.entitlementUniqueId = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setEntitlementId(SimpleQueryType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the usedCount property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getUsedCount() {
        return usedCount;
    }

    /**
     * Sets the value of the usedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setUsedCount(NumberQueryType value) {
        this.usedCount = value;
    }

    /**
     * Gets the value of the usedExceedsOrderedBy property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getUsedExceedsOrderedBy() {
        return usedExceedsOrderedBy;
    }

    /**
     * Sets the value of the usedExceedsOrderedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setUsedExceedsOrderedBy(NumberQueryType value) {
        this.usedExceedsOrderedBy = value;
    }

    /**
     * Gets the value of the usedExceedsProvisionedBy property.
     * 
     * @return
     *     possible object is
     *     {@link NumberQueryType }
     *     
     */
    public NumberQueryType getUsedExceedsProvisionedBy() {
        return usedExceedsProvisionedBy;
    }

    /**
     * Sets the value of the usedExceedsProvisionedBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumberQueryType }
     *     
     */
    public void setUsedExceedsProvisionedBy(NumberQueryType value) {
        this.usedExceedsProvisionedBy = value;
    }

    /**
     * Gets the value of the organizationUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public PartnerTierQueryType getOrganizationUnitName() {
        return organizationUnitName;
    }

    /**
     * Sets the value of the organizationUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartnerTierQueryType }
     *     
     */
    public void setOrganizationUnitName(PartnerTierQueryType value) {
        this.organizationUnitName = value;
    }

    /**
     * Gets the value of the soldToUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getSoldToUniqueId() {
        return soldToUniqueId;
    }

    /**
     * Sets the value of the soldToUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setSoldToUniqueId(ExternalIdQueryType value) {
        this.soldToUniqueId = value;
    }

    /**
     * Gets the value of the orderableUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getOrderableUniqueId() {
        return orderableUniqueId;
    }

    /**
     * Sets the value of the orderableUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setOrderableUniqueId(ExternalIdQueryType value) {
        this.orderableUniqueId = value;
    }

    /**
     * Gets the value of the orderableName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getOrderableName() {
        return orderableName;
    }

    /**
     * Sets the value of the orderableName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setOrderableName(SimpleQueryType value) {
        this.orderableName = value;
    }

    /**
     * Gets the value of the orderableVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getOrderableVersion() {
        return orderableVersion;
    }

    /**
     * Sets the value of the orderableVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setOrderableVersion(SimpleQueryType value) {
        this.orderableVersion = value;
    }

    /**
     * Gets the value of the serverUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getServerUniqueId() {
        return serverUniqueId;
    }

    /**
     * Sets the value of the serverUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setServerUniqueId(ExternalIdQueryType value) {
        this.serverUniqueId = value;
    }

    /**
     * Gets the value of the serverId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getServerId() {
        return serverId;
    }

    /**
     * Sets the value of the serverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setServerId(SimpleQueryType value) {
        this.serverId = value;
    }

    /**
     * Gets the value of the serverAlias property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getServerAlias() {
        return serverAlias;
    }

    /**
     * Sets the value of the serverAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setServerAlias(SimpleQueryType value) {
        this.serverAlias = value;
    }

    /**
     * Gets the value of the summaryTimeUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getSummaryTimeUniqueId() {
        return summaryTimeUniqueId;
    }

    /**
     * Sets the value of the summaryTimeUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setSummaryTimeUniqueId(ExternalIdQueryType value) {
        this.summaryTimeUniqueId = value;
    }

    /**
     * Gets the value of the summaryTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getSummaryTime() {
        return summaryTime;
    }

    /**
     * Sets the value of the summaryTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setSummaryTime(DateTimeQueryType value) {
        this.summaryTime = value;
    }

    /**
     * Gets the value of the groupBy property.
     * 
     * @return
     *     possible object is
     *     {@link UsageSummaryGroupByType }
     *     
     */
    public UsageSummaryGroupByType getGroupBy() {
        return groupBy;
    }

    /**
     * Sets the value of the groupBy property.
     * 
     * @param value
     *     allowed object is
     *     {@link UsageSummaryGroupByType }
     *     
     */
    public void setGroupBy(UsageSummaryGroupByType value) {
        this.groupBy = value;
    }

}
