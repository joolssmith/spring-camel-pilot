
package com.flexnet.operations.webservices.k;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for splitBulkEntitlementInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="splitBulkEntitlementInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementIdentifierType"/&gt;
 *         &lt;element name="numberOfWRKs" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="targetTierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="targetOrganizationUnit" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType"/&gt;
 *         &lt;element name="targetContact" type="{urn:com.macrovision:flexnet/operations}userIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="matchingBulkEntIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="matchingLineItemIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "splitBulkEntitlementInfoType", propOrder = {
    "bulkEntIdentifier",
    "numberOfWRKs",
    "targetTierName",
    "targetOrganizationUnit",
    "targetContact",
    "matchingBulkEntIdentifier",
    "matchingLineItemIdentifier"
})
public class SplitBulkEntitlementInfoType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType bulkEntIdentifier;
    @XmlElement(required = true)
    protected BigInteger numberOfWRKs;
    @XmlElement(required = true)
    protected String targetTierName;
    @XmlElement(required = true)
    protected OrganizationIdentifierType targetOrganizationUnit;
    protected UserIdentifierType targetContact;
    protected EntitlementIdentifierType matchingBulkEntIdentifier;
    protected EntitlementLineItemIdentifierType matchingLineItemIdentifier;

    /**
     * Gets the value of the bulkEntIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getBulkEntIdentifier() {
        return bulkEntIdentifier;
    }

    /**
     * Sets the value of the bulkEntIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setBulkEntIdentifier(EntitlementIdentifierType value) {
        this.bulkEntIdentifier = value;
    }

    /**
     * Gets the value of the numberOfWRKs property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfWRKs() {
        return numberOfWRKs;
    }

    /**
     * Sets the value of the numberOfWRKs property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfWRKs(BigInteger value) {
        this.numberOfWRKs = value;
    }

    /**
     * Gets the value of the targetTierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetTierName() {
        return targetTierName;
    }

    /**
     * Sets the value of the targetTierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetTierName(String value) {
        this.targetTierName = value;
    }

    /**
     * Gets the value of the targetOrganizationUnit property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getTargetOrganizationUnit() {
        return targetOrganizationUnit;
    }

    /**
     * Sets the value of the targetOrganizationUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setTargetOrganizationUnit(OrganizationIdentifierType value) {
        this.targetOrganizationUnit = value;
    }

    /**
     * Gets the value of the targetContact property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifierType }
     *     
     */
    public UserIdentifierType getTargetContact() {
        return targetContact;
    }

    /**
     * Sets the value of the targetContact property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifierType }
     *     
     */
    public void setTargetContact(UserIdentifierType value) {
        this.targetContact = value;
    }

    /**
     * Gets the value of the matchingBulkEntIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getMatchingBulkEntIdentifier() {
        return matchingBulkEntIdentifier;
    }

    /**
     * Sets the value of the matchingBulkEntIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setMatchingBulkEntIdentifier(EntitlementIdentifierType value) {
        this.matchingBulkEntIdentifier = value;
    }

    /**
     * Gets the value of the matchingLineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getMatchingLineItemIdentifier() {
        return matchingLineItemIdentifier;
    }

    /**
     * Sets the value of the matchingLineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setMatchingLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.matchingLineItemIdentifier = value;
    }

}
