
package com.flexnet.operations.webservices.i;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for publisherErrorFulfillmentResponseDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="publisherErrorFulfillmentResponseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="recordRefNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="fulfillment" type="{urn:com.macrovision:flexnet/operations}fulfillmentDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "publisherErrorFulfillmentResponseDataType", propOrder = {
    "recordRefNo",
    "fulfillment"
})
public class PublisherErrorFulfillmentResponseDataType {

    @XmlElement(required = true)
    protected BigInteger recordRefNo;
    protected FulfillmentDataType fulfillment;

    /**
     * Gets the value of the recordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRecordRefNo() {
        return recordRefNo;
    }

    /**
     * Sets the value of the recordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRecordRefNo(BigInteger value) {
        this.recordRefNo = value;
    }

    /**
     * Gets the value of the fulfillment property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentDataType }
     *     
     */
    public FulfillmentDataType getFulfillment() {
        return fulfillment;
    }

    /**
     * Sets the value of the fulfillment property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentDataType }
     *     
     */
    public void setFulfillment(FulfillmentDataType value) {
        this.fulfillment = value;
    }

}
