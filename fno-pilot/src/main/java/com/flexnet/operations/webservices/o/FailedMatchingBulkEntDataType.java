
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedMatchingBulkEntDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedMatchingBulkEntDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntInfo" type="{urn:v1.webservices.operations.flexnet.com}getMatchingBulkEntInfoType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedMatchingBulkEntDataType", propOrder = {
    "bulkEntInfo",
    "reason"
})
public class FailedMatchingBulkEntDataType {

    @XmlElement(required = true)
    protected GetMatchingBulkEntInfoType bulkEntInfo;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the bulkEntInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GetMatchingBulkEntInfoType }
     *     
     */
    public GetMatchingBulkEntInfoType getBulkEntInfo() {
        return bulkEntInfo;
    }

    /**
     * Sets the value of the bulkEntInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMatchingBulkEntInfoType }
     *     
     */
    public void setBulkEntInfo(GetMatchingBulkEntInfoType value) {
        this.bulkEntInfo = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
