
package com.flexnet.operations.webservices.a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ProducerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="OwnerId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FeatureLimits" type="{http://producersuite.flexerasoftware.com/EntitlementService/}FeatureLimit" maxOccurs="unbounded"/&gt;
 *         &lt;element name="ActivationId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EntitlementId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="FeatureName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustomAttributeCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="CustomAttributeValue" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ProductName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "producerId",
    "ownerId",
    "featureLimits",
    "activationId",
    "entitlementId",
    "featureName",
    "customAttributeCode",
    "customAttributeValue",
    "productName"
})
@XmlRootElement(name = "findLineItemsForOwner")
public class FindLineItemsForOwner {

    @XmlElement(name = "ProducerId", required = true)
    protected String producerId;
    @XmlElement(name = "OwnerId", required = true)
    protected String ownerId;
    @XmlElement(name = "FeatureLimits", required = true)
    protected List<FeatureLimit> featureLimits;
    @XmlElement(name = "ActivationId", required = true)
    protected String activationId;
    @XmlElement(name = "EntitlementId", required = true)
    protected String entitlementId;
    @XmlElement(name = "FeatureName", required = true)
    protected String featureName;
    @XmlElement(name = "CustomAttributeCode", required = true)
    protected String customAttributeCode;
    @XmlElement(name = "CustomAttributeValue", required = true)
    protected String customAttributeValue;
    @XmlElement(name = "ProductName", required = true)
    protected String productName;

    /**
     * Gets the value of the producerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducerId() {
        return producerId;
    }

    /**
     * Sets the value of the producerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducerId(String value) {
        this.producerId = value;
    }

    /**
     * Gets the value of the ownerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwnerId() {
        return ownerId;
    }

    /**
     * Sets the value of the ownerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwnerId(String value) {
        this.ownerId = value;
    }

    /**
     * Gets the value of the featureLimits property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featureLimits property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureLimits().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureLimit }
     * 
     * 
     */
    public List<FeatureLimit> getFeatureLimits() {
        if (featureLimits == null) {
            featureLimits = new ArrayList<FeatureLimit>();
        }
        return this.featureLimits;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationId(String value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementId(String value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureName(String value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the customAttributeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomAttributeCode() {
        return customAttributeCode;
    }

    /**
     * Sets the value of the customAttributeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomAttributeCode(String value) {
        this.customAttributeCode = value;
    }

    /**
     * Gets the value of the customAttributeValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomAttributeValue() {
        return customAttributeValue;
    }

    /**
     * Sets the value of the customAttributeValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomAttributeValue(String value) {
        this.customAttributeValue = value;
    }

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductName(String value) {
        this.productName = value;
    }

}
