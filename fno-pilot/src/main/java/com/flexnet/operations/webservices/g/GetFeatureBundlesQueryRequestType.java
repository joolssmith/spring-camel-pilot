
package com.flexnet.operations.webservices.g;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFeatureBundlesQueryRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFeatureBundlesQueryRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:com.macrovision:flexnet/operations}featureBundleQueryParametersType" minOccurs="0"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="returnContainedObjects" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFeatureBundlesQueryRequestType", propOrder = {
    "queryParams",
    "pageNumber",
    "batchSize",
    "returnContainedObjects"
})
public class GetFeatureBundlesQueryRequestType {

    protected FeatureBundleQueryParametersType queryParams;
    @XmlElement(required = true)
    protected BigInteger pageNumber;
    @XmlElement(required = true)
    protected BigInteger batchSize;
    protected boolean returnContainedObjects;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleQueryParametersType }
     *     
     */
    public FeatureBundleQueryParametersType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleQueryParametersType }
     *     
     */
    public void setQueryParams(FeatureBundleQueryParametersType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

    /**
     * Gets the value of the returnContainedObjects property.
     * 
     */
    public boolean isReturnContainedObjects() {
        return returnContainedObjects;
    }

    /**
     * Sets the value of the returnContainedObjects property.
     * 
     */
    public void setReturnContainedObjects(boolean value) {
        this.returnContainedObjects = value;
    }

}
