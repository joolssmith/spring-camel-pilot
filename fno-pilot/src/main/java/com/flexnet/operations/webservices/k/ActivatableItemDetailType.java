
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for activatableItemDetailType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="activatableItemDetailType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activatableItemType" type="{urn:com.macrovision:flexnet/operations}ActivatableItemType"/&gt;
 *         &lt;element name="parentBulkEntitlementId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitlementState" type="{urn:com.macrovision:flexnet/operations}StateType" minOccurs="0"/&gt;
 *         &lt;element name="activatableItemData" type="{urn:com.macrovision:flexnet/operations}entitlementLineItemDataType"/&gt;
 *         &lt;element name="channelPartners" type="{urn:com.macrovision:flexnet/operations}channelPartnerDataListType" minOccurs="0"/&gt;
 *         &lt;element name="entitlementAttributes" type="{urn:com.macrovision:flexnet/operations}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "activatableItemDetailType", propOrder = {
    "activatableItemType",
    "parentBulkEntitlementId",
    "entitlementId",
    "soldTo",
    "shipToEmail",
    "shipToAddress",
    "entitlementState",
    "activatableItemData",
    "channelPartners",
    "entitlementAttributes"
})
public class ActivatableItemDetailType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected ActivatableItemType activatableItemType;
    protected String parentBulkEntitlementId;
    @XmlElement(required = true)
    protected String entitlementId;
    @XmlElement(required = true)
    protected String soldTo;
    protected String shipToEmail;
    protected String shipToAddress;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType entitlementState;
    @XmlElement(required = true)
    protected EntitlementLineItemDataType activatableItemData;
    protected ChannelPartnerDataListType channelPartners;
    protected AttributeDescriptorDataType entitlementAttributes;

    /**
     * Gets the value of the activatableItemType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivatableItemType }
     *     
     */
    public ActivatableItemType getActivatableItemType() {
        return activatableItemType;
    }

    /**
     * Sets the value of the activatableItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivatableItemType }
     *     
     */
    public void setActivatableItemType(ActivatableItemType value) {
        this.activatableItemType = value;
    }

    /**
     * Gets the value of the parentBulkEntitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentBulkEntitlementId() {
        return parentBulkEntitlementId;
    }

    /**
     * Sets the value of the parentBulkEntitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentBulkEntitlementId(String value) {
        this.parentBulkEntitlementId = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementId(String value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldTo(String value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToEmail(String value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddress(String value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the entitlementState property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getEntitlementState() {
        return entitlementState;
    }

    /**
     * Sets the value of the entitlementState property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setEntitlementState(StateType value) {
        this.entitlementState = value;
    }

    /**
     * Gets the value of the activatableItemData property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemDataType }
     *     
     */
    public EntitlementLineItemDataType getActivatableItemData() {
        return activatableItemData;
    }

    /**
     * Sets the value of the activatableItemData property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemDataType }
     *     
     */
    public void setActivatableItemData(EntitlementLineItemDataType value) {
        this.activatableItemData = value;
    }

    /**
     * Gets the value of the channelPartners property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public ChannelPartnerDataListType getChannelPartners() {
        return channelPartners;
    }

    /**
     * Sets the value of the channelPartners property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public void setChannelPartners(ChannelPartnerDataListType value) {
        this.channelPartners = value;
    }

    /**
     * Gets the value of the entitlementAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getEntitlementAttributes() {
        return entitlementAttributes;
    }

    /**
     * Sets the value of the entitlementAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setEntitlementAttributes(AttributeDescriptorDataType value) {
        this.entitlementAttributes = value;
    }

}
