
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deviceDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceIdentifier" type="{urn:com.macrovision:flexnet/opsembedded}createDeviceIdentifier"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:com.macrovision:flexnet/opsembedded}hostTypeIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="publisherIdName" type="{urn:com.macrovision:flexnet/opsembedded}publisherIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="channelPartners" type="{urn:com.macrovision:flexnet/opsembedded}channelPartnerDataListType" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:com.macrovision:flexnet/opsembedded}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="alias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deviceDataType", propOrder = {
    "deviceIdentifier",
    "hostTypeName",
    "publisherIdName",
    "description",
    "channelPartners",
    "customAttributes",
    "alias"
})
public class DeviceDataType {

    @XmlElement(required = true)
    protected CreateDeviceIdentifier deviceIdentifier;
    protected HostTypeIdentifier hostTypeName;
    protected PublisherIdentifier publisherIdName;
    protected String description;
    protected ChannelPartnerDataListType channelPartners;
    protected AttributeDescriptorDataType customAttributes;
    protected String alias;

    /**
     * Gets the value of the deviceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link CreateDeviceIdentifier }
     *     
     */
    public CreateDeviceIdentifier getDeviceIdentifier() {
        return deviceIdentifier;
    }

    /**
     * Sets the value of the deviceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateDeviceIdentifier }
     *     
     */
    public void setDeviceIdentifier(CreateDeviceIdentifier value) {
        this.deviceIdentifier = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public HostTypeIdentifier getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public void setHostTypeName(HostTypeIdentifier value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the publisherIdName property.
     * 
     * @return
     *     possible object is
     *     {@link PublisherIdentifier }
     *     
     */
    public PublisherIdentifier getPublisherIdName() {
        return publisherIdName;
    }

    /**
     * Sets the value of the publisherIdName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublisherIdentifier }
     *     
     */
    public void setPublisherIdName(PublisherIdentifier value) {
        this.publisherIdName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the channelPartners property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public ChannelPartnerDataListType getChannelPartners() {
        return channelPartners;
    }

    /**
     * Sets the value of the channelPartners property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public void setChannelPartners(ChannelPartnerDataListType value) {
        this.channelPartners = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

}
