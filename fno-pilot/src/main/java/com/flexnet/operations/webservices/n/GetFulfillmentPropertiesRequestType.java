
package com.flexnet.operations.webservices.n;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFulfillmentPropertiesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFulfillmentPropertiesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentsQueryParametersType"/&gt;
 *         &lt;element name="fulfillmentResponseConfig" type="{urn:v1.webservices.operations.flexnet.com}fulfillmentResponseConfigRequestType"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFulfillmentPropertiesRequestType", propOrder = {
    "queryParams",
    "fulfillmentResponseConfig",
    "batchSize",
    "pageNumber"
})
public class GetFulfillmentPropertiesRequestType {

    @XmlElement(required = true)
    protected FulfillmentsQueryParametersType queryParams;
    @XmlElement(required = true)
    protected FulfillmentResponseConfigRequestType fulfillmentResponseConfig;
    @XmlElement(required = true)
    protected BigInteger batchSize;
    protected BigInteger pageNumber;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentsQueryParametersType }
     *     
     */
    public FulfillmentsQueryParametersType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentsQueryParametersType }
     *     
     */
    public void setQueryParams(FulfillmentsQueryParametersType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the fulfillmentResponseConfig property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentResponseConfigRequestType }
     *     
     */
    public FulfillmentResponseConfigRequestType getFulfillmentResponseConfig() {
        return fulfillmentResponseConfig;
    }

    /**
     * Sets the value of the fulfillmentResponseConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentResponseConfigRequestType }
     *     
     */
    public void setFulfillmentResponseConfig(FulfillmentResponseConfigRequestType value) {
        this.fulfillmentResponseConfig = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

}
