
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for advancedReinstallPolicyType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="advancedReinstallPolicyType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="umn1Policy" type="{urn:com.macrovision:flexnet/operations}policyDataType"/&gt;
 *         &lt;element name="umn2Policy" type="{urn:com.macrovision:flexnet/operations}policyDataType"/&gt;
 *         &lt;element name="umn3Policy" type="{urn:com.macrovision:flexnet/operations}policyDataType"/&gt;
 *         &lt;element name="midPolicy" type="{urn:com.macrovision:flexnet/operations}policyDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "advancedReinstallPolicyType", propOrder = {
    "umn1Policy",
    "umn2Policy",
    "umn3Policy",
    "midPolicy"
})
public class AdvancedReinstallPolicyType {

    @XmlElement(required = true)
    protected PolicyDataType umn1Policy;
    @XmlElement(required = true)
    protected PolicyDataType umn2Policy;
    @XmlElement(required = true)
    protected PolicyDataType umn3Policy;
    @XmlElement(required = true)
    protected PolicyDataType midPolicy;

    /**
     * Gets the value of the umn1Policy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getUmn1Policy() {
        return umn1Policy;
    }

    /**
     * Sets the value of the umn1Policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setUmn1Policy(PolicyDataType value) {
        this.umn1Policy = value;
    }

    /**
     * Gets the value of the umn2Policy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getUmn2Policy() {
        return umn2Policy;
    }

    /**
     * Sets the value of the umn2Policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setUmn2Policy(PolicyDataType value) {
        this.umn2Policy = value;
    }

    /**
     * Gets the value of the umn3Policy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getUmn3Policy() {
        return umn3Policy;
    }

    /**
     * Sets the value of the umn3Policy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setUmn3Policy(PolicyDataType value) {
        this.umn3Policy = value;
    }

    /**
     * Gets the value of the midPolicy property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDataType }
     *     
     */
    public PolicyDataType getMidPolicy() {
        return midPolicy;
    }

    /**
     * Sets the value of the midPolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDataType }
     *     
     */
    public void setMidPolicy(PolicyDataType value) {
        this.midPolicy = value;
    }

}
