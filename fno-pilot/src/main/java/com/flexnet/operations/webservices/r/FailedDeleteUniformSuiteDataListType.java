
package com.flexnet.operations.webservices.r;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedDeleteUniformSuiteDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedDeleteUniformSuiteDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedUniformSuite" type="{urn:v1.webservices.operations.flexnet.com}failedDeleteUniformSuiteDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedDeleteUniformSuiteDataListType", propOrder = {
    "failedUniformSuite"
})
public class FailedDeleteUniformSuiteDataListType {

    protected List<FailedDeleteUniformSuiteDataType> failedUniformSuite;

    /**
     * Gets the value of the failedUniformSuite property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedUniformSuite property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedUniformSuite().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedDeleteUniformSuiteDataType }
     * 
     * 
     */
    public List<FailedDeleteUniformSuiteDataType> getFailedUniformSuite() {
        if (failedUniformSuite == null) {
            failedUniformSuite = new ArrayList<FailedDeleteUniformSuiteDataType>();
        }
        return this.failedUniformSuite;
    }

}
