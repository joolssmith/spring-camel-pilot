
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMaintenanceQueryResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMaintenanceQueryResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:com.macrovision:flexnet/operations}StatusInfoType"/&gt;
 *         &lt;element name="responseData" type="{urn:com.macrovision:flexnet/operations}getMaintenanceQueryResponseDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMaintenanceQueryResponseType", propOrder = {
    "statusInfo",
    "responseData"
})
public class GetMaintenanceQueryResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected GetMaintenanceQueryResponseDataType responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link GetMaintenanceQueryResponseDataType }
     *     
     */
    public GetMaintenanceQueryResponseDataType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMaintenanceQueryResponseDataType }
     *     
     */
    public void setResponseData(GetMaintenanceQueryResponseDataType value) {
        this.responseData = value;
    }

}
