
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fulfillmentsQueryParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fulfillmentsQueryParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementId" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="activationId" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillmentId" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="hostId" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="nodeLockHostId" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillDate" type="{urn:v1.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillDateTime" type="{urn:v1.webservices.operations.flexnet.com}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{urn:v1.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{urn:v1.webservices.operations.flexnet.com}DateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{urn:v1.webservices.operations.flexnet.com}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v1.webservices.operations.flexnet.com}StateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v1.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fulfillmentSource" type="{urn:v1.webservices.operations.flexnet.com}FulfillmentSourceType" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v1.webservices.operations.flexnet.com}customAttributesQueryListType" minOccurs="0"/&gt;
 *         &lt;element name="customHostAttributes" type="{urn:v1.webservices.operations.flexnet.com}customAttributesQueryListType" minOccurs="0"/&gt;
 *         &lt;element name="activationType" type="{urn:v1.webservices.operations.flexnet.com}ActivationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fulfillmentsQueryParametersType", propOrder = {
    "entitlementId",
    "activationId",
    "fulfillmentId",
    "product",
    "hostId",
    "nodeLockHostId",
    "soldTo",
    "fulfillDate",
    "fulfillDateTime",
    "startDate",
    "expirationDate",
    "lastModifiedDateTime",
    "state",
    "licenseTechnology",
    "userId",
    "fulfillmentSource",
    "customAttributes",
    "customHostAttributes",
    "activationType"
})
public class FulfillmentsQueryParametersType {

    protected SimpleQueryType entitlementId;
    protected SimpleQueryType activationId;
    protected SimpleQueryType fulfillmentId;
    protected SimpleQueryType product;
    protected SimpleQueryType hostId;
    protected SimpleQueryType nodeLockHostId;
    protected SimpleQueryType soldTo;
    protected DateQueryType fulfillDate;
    protected DateTimeQueryType fulfillDateTime;
    protected DateQueryType startDate;
    protected DateQueryType expirationDate;
    protected DateTimeQueryType lastModifiedDateTime;
    protected StateQueryType state;
    protected SimpleQueryType licenseTechnology;
    protected String userId;
    @XmlSchemaType(name = "NMTOKEN")
    protected FulfillmentSourceType fulfillmentSource;
    protected CustomAttributesQueryListType customAttributes;
    protected CustomAttributesQueryListType customHostAttributes;
    @XmlSchemaType(name = "NMTOKEN")
    protected ActivationType activationType;

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setEntitlementId(SimpleQueryType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setActivationId(SimpleQueryType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the fulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFulfillmentId() {
        return fulfillmentId;
    }

    /**
     * Sets the value of the fulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFulfillmentId(SimpleQueryType value) {
        this.fulfillmentId = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setProduct(SimpleQueryType value) {
        this.product = value;
    }

    /**
     * Gets the value of the hostId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getHostId() {
        return hostId;
    }

    /**
     * Sets the value of the hostId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setHostId(SimpleQueryType value) {
        this.hostId = value;
    }

    /**
     * Gets the value of the nodeLockHostId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getNodeLockHostId() {
        return nodeLockHostId;
    }

    /**
     * Sets the value of the nodeLockHostId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setNodeLockHostId(SimpleQueryType value) {
        this.nodeLockHostId = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldTo(SimpleQueryType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the fulfillDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getFulfillDate() {
        return fulfillDate;
    }

    /**
     * Sets the value of the fulfillDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setFulfillDate(DateQueryType value) {
        this.fulfillDate = value;
    }

    /**
     * Gets the value of the fulfillDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getFulfillDateTime() {
        return fulfillDateTime;
    }

    /**
     * Sets the value of the fulfillDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setFulfillDateTime(DateTimeQueryType value) {
        this.fulfillDateTime = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setStartDate(DateQueryType value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateQueryType }
     *     
     */
    public DateQueryType getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateQueryType }
     *     
     */
    public void setExpirationDate(DateQueryType value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setLastModifiedDateTime(DateTimeQueryType value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateQueryType }
     *     
     */
    public StateQueryType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateQueryType }
     *     
     */
    public void setState(StateQueryType value) {
        this.state = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setLicenseTechnology(SimpleQueryType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Gets the value of the fulfillmentSource property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }

    /**
     * Sets the value of the fulfillmentSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public void setFulfillmentSource(FulfillmentSourceType value) {
        this.fulfillmentSource = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public CustomAttributesQueryListType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public void setCustomAttributes(CustomAttributesQueryListType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the customHostAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public CustomAttributesQueryListType getCustomHostAttributes() {
        return customHostAttributes;
    }

    /**
     * Sets the value of the customHostAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributesQueryListType }
     *     
     */
    public void setCustomHostAttributes(CustomAttributesQueryListType value) {
        this.customHostAttributes = value;
    }

    /**
     * Gets the value of the activationType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivationType }
     *     
     */
    public ActivationType getActivationType() {
        return activationType;
    }

    /**
     * Sets the value of the activationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivationType }
     *     
     */
    public void setActivationType(ActivationType value) {
        this.activationType = value;
    }

}
