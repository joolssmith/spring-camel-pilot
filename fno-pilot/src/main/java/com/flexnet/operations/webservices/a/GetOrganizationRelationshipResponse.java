
package com.flexnet.operations.webservices.a;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Relationship" type="{http://producersuite.flexerasoftware.com/EntitlementService/}OrganizationRelation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "relationship"
})
@XmlRootElement(name = "getOrganizationRelationshipResponse")
public class GetOrganizationRelationshipResponse {

    @XmlElement(name = "Relationship", required = true)
    @XmlSchemaType(name = "string")
    protected OrganizationRelation relationship;

    /**
     * Gets the value of the relationship property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationRelation }
     *     
     */
    public OrganizationRelation getRelationship() {
        return relationship;
    }

    /**
     * Sets the value of the relationship property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationRelation }
     *     
     */
    public void setRelationship(OrganizationRelation value) {
        this.relationship = value;
    }

}
