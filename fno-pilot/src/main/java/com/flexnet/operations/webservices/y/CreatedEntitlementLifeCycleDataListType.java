
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdEntitlementLifeCycleDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdEntitlementLifeCycleDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementData" type="{urn:v3.webservices.operations.flexnet.com}createdEntitlementLifeCycleDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdEntitlementLifeCycleDataListType", propOrder = {
    "entitlementData"
})
public class CreatedEntitlementLifeCycleDataListType {

    protected List<CreatedEntitlementLifeCycleDataType> entitlementData;

    /**
     * Gets the value of the entitlementData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entitlementData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntitlementData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatedEntitlementLifeCycleDataType }
     * 
     * 
     */
    public List<CreatedEntitlementLifeCycleDataType> getEntitlementData() {
        if (entitlementData == null) {
            entitlementData = new ArrayList<CreatedEntitlementLifeCycleDataType>();
        }
        return this.entitlementData;
    }

}
