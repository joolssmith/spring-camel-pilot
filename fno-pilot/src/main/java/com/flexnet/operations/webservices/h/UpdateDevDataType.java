
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateDevDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateDevDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceIdentifier" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdentifier"/&gt;
 *         &lt;element name="deviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serverIds" type="{urn:com.macrovision:flexnet/opsembedded}ServerIdsType" minOccurs="0"/&gt;
 *         &lt;element name="deviceIdType" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdTypeType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:com.macrovision:flexnet/opsembedded}hostTypeIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="publisherIdName" type="{urn:com.macrovision:flexnet/opsembedded}publisherIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="updateChannelPartners" type="{urn:com.macrovision:flexnet/opsembedded}updateChannelPartnerDataListType" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:com.macrovision:flexnet/opsembedded}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="alias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateDevDataType", propOrder = {
    "deviceIdentifier",
    "deviceId",
    "serverIds",
    "deviceIdType",
    "hostTypeName",
    "publisherIdName",
    "description",
    "updateChannelPartners",
    "customAttributes",
    "alias"
})
public class UpdateDevDataType {

    @XmlElement(required = true)
    protected DeviceIdentifier deviceIdentifier;
    protected String deviceId;
    protected ServerIdsType serverIds;
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceIdTypeType deviceIdType;
    protected HostTypeIdentifier hostTypeName;
    protected PublisherIdentifier publisherIdName;
    protected String description;
    protected UpdateChannelPartnerDataListType updateChannelPartners;
    protected AttributeDescriptorDataType customAttributes;
    protected String alias;

    /**
     * Gets the value of the deviceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getDeviceIdentifier() {
        return deviceIdentifier;
    }

    /**
     * Sets the value of the deviceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setDeviceIdentifier(DeviceIdentifier value) {
        this.deviceIdentifier = value;
    }

    /**
     * Gets the value of the deviceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceId(String value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the serverIds property.
     * 
     * @return
     *     possible object is
     *     {@link ServerIdsType }
     *     
     */
    public ServerIdsType getServerIds() {
        return serverIds;
    }

    /**
     * Sets the value of the serverIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerIdsType }
     *     
     */
    public void setServerIds(ServerIdsType value) {
        this.serverIds = value;
    }

    /**
     * Gets the value of the deviceIdType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdTypeType }
     *     
     */
    public DeviceIdTypeType getDeviceIdType() {
        return deviceIdType;
    }

    /**
     * Sets the value of the deviceIdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdTypeType }
     *     
     */
    public void setDeviceIdType(DeviceIdTypeType value) {
        this.deviceIdType = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public HostTypeIdentifier getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public void setHostTypeName(HostTypeIdentifier value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the publisherIdName property.
     * 
     * @return
     *     possible object is
     *     {@link PublisherIdentifier }
     *     
     */
    public PublisherIdentifier getPublisherIdName() {
        return publisherIdName;
    }

    /**
     * Sets the value of the publisherIdName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublisherIdentifier }
     *     
     */
    public void setPublisherIdName(PublisherIdentifier value) {
        this.publisherIdName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the updateChannelPartners property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateChannelPartnerDataListType }
     *     
     */
    public UpdateChannelPartnerDataListType getUpdateChannelPartners() {
        return updateChannelPartners;
    }

    /**
     * Sets the value of the updateChannelPartners property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateChannelPartnerDataListType }
     *     
     */
    public void setUpdateChannelPartners(UpdateChannelPartnerDataListType value) {
        this.updateChannelPartners = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

}
