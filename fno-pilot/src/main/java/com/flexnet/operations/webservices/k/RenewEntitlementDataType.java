
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for renewEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="renewEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentEntitlementIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="renewLineItemData" type="{urn:com.macrovision:flexnet/operations}renewLineItemDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "renewEntitlementDataType", propOrder = {
    "parentEntitlementIdentifier",
    "renewLineItemData"
})
public class RenewEntitlementDataType {

    protected EntitlementIdentifierType parentEntitlementIdentifier;
    @XmlElement(required = true)
    protected List<RenewLineItemDataType> renewLineItemData;

    /**
     * Gets the value of the parentEntitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getParentEntitlementIdentifier() {
        return parentEntitlementIdentifier;
    }

    /**
     * Sets the value of the parentEntitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setParentEntitlementIdentifier(EntitlementIdentifierType value) {
        this.parentEntitlementIdentifier = value;
    }

    /**
     * Gets the value of the renewLineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the renewLineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRenewLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RenewLineItemDataType }
     * 
     * 
     */
    public List<RenewLineItemDataType> getRenewLineItemData() {
        if (renewLineItemData == null) {
            renewLineItemData = new ArrayList<RenewLineItemDataType>();
        }
        return this.renewLineItemData;
    }

}
