
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for updateMaintenanceLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMaintenanceLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemIdentifier" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="activationId" type="{urn:v3.webservices.operations.flexnet.com}idType" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceProduct" type="{urn:v3.webservices.operations.flexnet.com}productIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v3.webservices.operations.flexnet.com}partNumberIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="parentLineItem" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItemAttributes" type="{urn:v3.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMaintenanceLineItemDataType", propOrder = {
    "lineItemIdentifier",
    "activationId",
    "maintenanceProduct",
    "partNumber",
    "orderId",
    "orderLineNumber",
    "startDate",
    "expirationDate",
    "isPermanent",
    "parentLineItem",
    "maintenanceLineItemAttributes"
})
public class UpdateMaintenanceLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType lineItemIdentifier;
    protected IdType activationId;
    protected ProductIdentifierType maintenanceProduct;
    protected PartNumberIdentifierType partNumber;
    protected String orderId;
    protected String orderLineNumber;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    protected Boolean isPermanent;
    protected EntitlementLineItemIdentifierType parentLineItem;
    protected AttributeDescriptorDataType maintenanceLineItemAttributes;

    /**
     * Gets the value of the lineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getLineItemIdentifier() {
        return lineItemIdentifier;
    }

    /**
     * Sets the value of the lineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.lineItemIdentifier = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link IdType }
     *     
     */
    public IdType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdType }
     *     
     */
    public void setActivationId(IdType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the maintenanceProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getMaintenanceProduct() {
        return maintenanceProduct;
    }

    /**
     * Sets the value of the maintenanceProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setMaintenanceProduct(ProductIdentifierType value) {
        this.maintenanceProduct = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setPartNumber(PartNumberIdentifierType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineNumber(String value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the parentLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getParentLineItem() {
        return parentLineItem;
    }

    /**
     * Sets the value of the parentLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setParentLineItem(EntitlementLineItemIdentifierType value) {
        this.parentLineItem = value;
    }

    /**
     * Gets the value of the maintenanceLineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getMaintenanceLineItemAttributes() {
        return maintenanceLineItemAttributes;
    }

    /**
     * Sets the value of the maintenanceLineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setMaintenanceLineItemAttributes(AttributeDescriptorDataType value) {
        this.maintenanceLineItemAttributes = value;
    }

}
