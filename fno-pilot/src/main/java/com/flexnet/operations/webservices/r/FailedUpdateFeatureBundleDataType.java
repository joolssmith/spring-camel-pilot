
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateFeatureBundleDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateFeatureBundleDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundle" type="{urn:v1.webservices.operations.flexnet.com}updateFeatureBundleDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateFeatureBundleDataType", propOrder = {
    "featureBundle",
    "reason"
})
public class FailedUpdateFeatureBundleDataType {

    @XmlElement(required = true)
    protected UpdateFeatureBundleDataType featureBundle;
    protected String reason;

    /**
     * Gets the value of the featureBundle property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateFeatureBundleDataType }
     *     
     */
    public UpdateFeatureBundleDataType getFeatureBundle() {
        return featureBundle;
    }

    /**
     * Sets the value of the featureBundle property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateFeatureBundleDataType }
     *     
     */
    public void setFeatureBundle(UpdateFeatureBundleDataType value) {
        this.featureBundle = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
