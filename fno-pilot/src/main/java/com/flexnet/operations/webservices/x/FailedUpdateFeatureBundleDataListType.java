
package com.flexnet.operations.webservices.x;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateFeatureBundleDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateFeatureBundleDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedFeatureBundle" type="{urn:v2.webservices.operations.flexnet.com}failedUpdateFeatureBundleDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateFeatureBundleDataListType", propOrder = {
    "failedFeatureBundle"
})
public class FailedUpdateFeatureBundleDataListType {

    protected List<FailedUpdateFeatureBundleDataType> failedFeatureBundle;

    /**
     * Gets the value of the failedFeatureBundle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedFeatureBundle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedFeatureBundle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUpdateFeatureBundleDataType }
     * 
     * 
     */
    public List<FailedUpdateFeatureBundleDataType> getFailedFeatureBundle() {
        if (failedFeatureBundle == null) {
            failedFeatureBundle = new ArrayList<FailedUpdateFeatureBundleDataType>();
        }
        return this.failedFeatureBundle;
    }

}
