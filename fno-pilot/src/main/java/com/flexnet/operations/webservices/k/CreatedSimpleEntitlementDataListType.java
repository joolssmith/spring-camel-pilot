
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdSimpleEntitlementDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdSimpleEntitlementDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="createdSimpleEntitlement" type="{urn:com.macrovision:flexnet/operations}createdSimpleEntitlementDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdSimpleEntitlementDataListType", propOrder = {
    "createdSimpleEntitlement"
})
public class CreatedSimpleEntitlementDataListType {

    protected List<CreatedSimpleEntitlementDataType> createdSimpleEntitlement;

    /**
     * Gets the value of the createdSimpleEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdSimpleEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedSimpleEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatedSimpleEntitlementDataType }
     * 
     * 
     */
    public List<CreatedSimpleEntitlementDataType> getCreatedSimpleEntitlement() {
        if (createdSimpleEntitlement == null) {
            createdSimpleEntitlement = new ArrayList<CreatedSimpleEntitlementDataType>();
        }
        return this.createdSimpleEntitlement;
    }

}
