
package com.flexnet.operations.webservices.h;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getUsageSummaryTimesDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getUsageSummaryTimesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="usageSummaryTime" type="{urn:com.macrovision:flexnet/opsembedded}usageSummaryTimesDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getUsageSummaryTimesDataType", propOrder = {
    "usageSummaryTime"
})
public class GetUsageSummaryTimesDataType {

    protected List<UsageSummaryTimesDataType> usageSummaryTime;

    /**
     * Gets the value of the usageSummaryTime property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usageSummaryTime property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsageSummaryTime().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UsageSummaryTimesDataType }
     * 
     * 
     */
    public List<UsageSummaryTimesDataType> getUsageSummaryTime() {
        if (usageSummaryTime == null) {
            usageSummaryTime = new ArrayList<UsageSummaryTimesDataType>();
        }
        return this.usageSummaryTime;
    }

}
