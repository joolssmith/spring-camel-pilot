
package com.flexnet.operations.webservices.x;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for licenseGeneratorsDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="licenseGeneratorsDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="licenseGeneratorIdentifier" type="{urn:v2.webservices.operations.flexnet.com}licenseGeneratorIdentifierType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "licenseGeneratorsDetailsType", propOrder = {
    "licenseGeneratorIdentifier"
})
public class LicenseGeneratorsDetailsType {

    protected List<LicenseGeneratorIdentifierType> licenseGeneratorIdentifier;

    /**
     * Gets the value of the licenseGeneratorIdentifier property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the licenseGeneratorIdentifier property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLicenseGeneratorIdentifier().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LicenseGeneratorIdentifierType }
     * 
     * 
     */
    public List<LicenseGeneratorIdentifierType> getLicenseGeneratorIdentifier() {
        if (licenseGeneratorIdentifier == null) {
            licenseGeneratorIdentifier = new ArrayList<LicenseGeneratorIdentifierType>();
        }
        return this.licenseGeneratorIdentifier;
    }

}
