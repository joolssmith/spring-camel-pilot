
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedMatchingLineItemsListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedMatchingLineItemsListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedLineItem" type="{urn:v3.webservices.operations.flexnet.com}failedMatchingLineItemDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedMatchingLineItemsListType", propOrder = {
    "failedLineItem"
})
public class FailedMatchingLineItemsListType {

    @XmlElement(required = true)
    protected List<FailedMatchingLineItemDataType> failedLineItem;

    /**
     * Gets the value of the failedLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedMatchingLineItemDataType }
     * 
     * 
     */
    public List<FailedMatchingLineItemDataType> getFailedLineItem() {
        if (failedLineItem == null) {
            failedLineItem = new ArrayList<FailedMatchingLineItemDataType>();
        }
        return this.failedLineItem;
    }

}
