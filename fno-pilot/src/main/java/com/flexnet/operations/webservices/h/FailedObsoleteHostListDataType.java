
package com.flexnet.operations.webservices.h;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedObsoleteHostListDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedObsoleteHostListDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedHost" type="{urn:com.macrovision:flexnet/opsembedded}failedObsoleteHostDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedObsoleteHostListDataType", propOrder = {
    "failedHost"
})
public class FailedObsoleteHostListDataType {

    protected List<FailedObsoleteHostDataType> failedHost;

    /**
     * Gets the value of the failedHost property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedHost property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedHost().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedObsoleteHostDataType }
     * 
     * 
     */
    public List<FailedObsoleteHostDataType> getFailedHost() {
        if (failedHost == null) {
            failedHost = new ArrayList<FailedObsoleteHostDataType>();
        }
        return this.failedHost;
    }

}
