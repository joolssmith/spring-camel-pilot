
package com.flexnet.operations.webservices.v;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for generateCapabilityResponseResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generateCapabilityResponseResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v2.fne.webservices.operations.flexnet.com}OpsEmbeddedStatusInfoType"/&gt;
 *         &lt;element name="failedData" type="{urn:v2.fne.webservices.operations.flexnet.com}failedGenerateCapabilityResponseDataType" minOccurs="0"/&gt;
 *         &lt;element name="responseData" type="{urn:v2.fne.webservices.operations.flexnet.com}generateCapabilityResponseDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generateCapabilityResponseResponseType", propOrder = {
    "statusInfo",
    "failedData",
    "responseData"
})
public class GenerateCapabilityResponseResponseType {

    @XmlElement(required = true)
    protected OpsEmbeddedStatusInfoType statusInfo;
    protected FailedGenerateCapabilityResponseDataType failedData;
    protected GenerateCapabilityResponseDataType responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public OpsEmbeddedStatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public void setStatusInfo(OpsEmbeddedStatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedGenerateCapabilityResponseDataType }
     *     
     */
    public FailedGenerateCapabilityResponseDataType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedGenerateCapabilityResponseDataType }
     *     
     */
    public void setFailedData(FailedGenerateCapabilityResponseDataType value) {
        this.failedData = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateCapabilityResponseDataType }
     *     
     */
    public GenerateCapabilityResponseDataType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateCapabilityResponseDataType }
     *     
     */
    public void setResponseData(GenerateCapabilityResponseDataType value) {
        this.responseData = value;
    }

}
