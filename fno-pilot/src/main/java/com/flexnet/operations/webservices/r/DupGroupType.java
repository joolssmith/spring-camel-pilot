
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DupGroupType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="DupGroupType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="NO_OVERRIDE"/&gt;
 *     &lt;enumeration value="GROUPMASK"/&gt;
 *     &lt;enumeration value="NONE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "DupGroupType")
@XmlEnum
public enum DupGroupType {

    NO_OVERRIDE,
    GROUPMASK,
    NONE;

    public String value() {
        return name();
    }

    public static DupGroupType fromValue(String v) {
        return valueOf(v);
    }

}
