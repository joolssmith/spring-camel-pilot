
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for createProductDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createProductDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v1.webservices.operations.flexnet.com}licenseTechnologyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseGenerator" type="{urn:v1.webservices.operations.flexnet.com}licenseGeneratorIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="packageProperties" type="{urn:v1.webservices.operations.flexnet.com}packagePropertiesDataType" minOccurs="0"/&gt;
 *         &lt;element name="features" type="{urn:v1.webservices.operations.flexnet.com}featuresListType" minOccurs="0"/&gt;
 *         &lt;element name="featureBundles" type="{urn:v1.webservices.operations.flexnet.com}featureBundlesListType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModels" type="{urn:v1.webservices.operations.flexnet.com}licenseModelsListType" minOccurs="0"/&gt;
 *         &lt;element name="trustedKey" type="{urn:v1.webservices.operations.flexnet.com}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="virtualTrustedKey" type="{urn:v1.webservices.operations.flexnet.com}trustedKeyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumbers" type="{urn:v1.webservices.operations.flexnet.com}partNumbersListType" minOccurs="0"/&gt;
 *         &lt;element name="hostType" type="{urn:v1.webservices.operations.flexnet.com}hostTypePKType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypes" type="{urn:v1.webservices.operations.flexnet.com}hostTypeListType" minOccurs="0"/&gt;
 *         &lt;element name="usedOnDevice" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInAdmin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowDownloadObsoleteFrInPortal" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="endDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="upgradeEmailTemplateVarName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createProductDataType", propOrder = {
    "productName",
    "version",
    "description",
    "licenseTechnology",
    "licenseGenerator",
    "packageProperties",
    "features",
    "featureBundles",
    "licenseModels",
    "trustedKey",
    "virtualTrustedKey",
    "partNumbers",
    "hostType",
    "hostTypes",
    "usedOnDevice",
    "productCategory",
    "customAttributes",
    "allowDownloadObsoleteFrInAdmin",
    "allowDownloadObsoleteFrInPortal",
    "productAttributes",
    "startDate",
    "endDate",
    "upgradeEmailTemplateVarName"
})
public class CreateProductDataType {

    @XmlElement(required = true)
    protected String productName;
    protected String version;
    protected String description;
    protected LicenseTechnologyIdentifierType licenseTechnology;
    protected LicenseGeneratorIdentifierType licenseGenerator;
    protected PackagePropertiesDataType packageProperties;
    protected FeaturesListType features;
    protected FeatureBundlesListType featureBundles;
    protected LicenseModelsListType licenseModels;
    protected TrustedKeyIdentifierType trustedKey;
    protected TrustedKeyIdentifierType virtualTrustedKey;
    protected PartNumbersListType partNumbers;
    protected HostTypePKType hostType;
    protected HostTypeListType hostTypes;
    protected Boolean usedOnDevice;
    protected String productCategory;
    protected AttributeDescriptorDataType customAttributes;
    protected Boolean allowDownloadObsoleteFrInAdmin;
    protected Boolean allowDownloadObsoleteFrInPortal;
    protected AttributeDescriptorDataType productAttributes;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endDate;
    protected String upgradeEmailTemplateVarName;

    /**
     * Gets the value of the productName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets the value of the productName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductName(String value) {
        this.productName = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the licenseGenerator property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public LicenseGeneratorIdentifierType getLicenseGenerator() {
        return licenseGenerator;
    }

    /**
     * Sets the value of the licenseGenerator property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseGeneratorIdentifierType }
     *     
     */
    public void setLicenseGenerator(LicenseGeneratorIdentifierType value) {
        this.licenseGenerator = value;
    }

    /**
     * Gets the value of the packageProperties property.
     * 
     * @return
     *     possible object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public PackagePropertiesDataType getPackageProperties() {
        return packageProperties;
    }

    /**
     * Sets the value of the packageProperties property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackagePropertiesDataType }
     *     
     */
    public void setPackageProperties(PackagePropertiesDataType value) {
        this.packageProperties = value;
    }

    /**
     * Gets the value of the features property.
     * 
     * @return
     *     possible object is
     *     {@link FeaturesListType }
     *     
     */
    public FeaturesListType getFeatures() {
        return features;
    }

    /**
     * Sets the value of the features property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeaturesListType }
     *     
     */
    public void setFeatures(FeaturesListType value) {
        this.features = value;
    }

    /**
     * Gets the value of the featureBundles property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundlesListType }
     *     
     */
    public FeatureBundlesListType getFeatureBundles() {
        return featureBundles;
    }

    /**
     * Sets the value of the featureBundles property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundlesListType }
     *     
     */
    public void setFeatureBundles(FeatureBundlesListType value) {
        this.featureBundles = value;
    }

    /**
     * Gets the value of the licenseModels property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelsListType }
     *     
     */
    public LicenseModelsListType getLicenseModels() {
        return licenseModels;
    }

    /**
     * Sets the value of the licenseModels property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelsListType }
     *     
     */
    public void setLicenseModels(LicenseModelsListType value) {
        this.licenseModels = value;
    }

    /**
     * Gets the value of the trustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getTrustedKey() {
        return trustedKey;
    }

    /**
     * Sets the value of the trustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setTrustedKey(TrustedKeyIdentifierType value) {
        this.trustedKey = value;
    }

    /**
     * Gets the value of the virtualTrustedKey property.
     * 
     * @return
     *     possible object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public TrustedKeyIdentifierType getVirtualTrustedKey() {
        return virtualTrustedKey;
    }

    /**
     * Sets the value of the virtualTrustedKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link TrustedKeyIdentifierType }
     *     
     */
    public void setVirtualTrustedKey(TrustedKeyIdentifierType value) {
        this.virtualTrustedKey = value;
    }

    /**
     * Gets the value of the partNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumbersListType }
     *     
     */
    public PartNumbersListType getPartNumbers() {
        return partNumbers;
    }

    /**
     * Sets the value of the partNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumbersListType }
     *     
     */
    public void setPartNumbers(PartNumbersListType value) {
        this.partNumbers = value;
    }

    /**
     * Gets the value of the hostType property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypePKType }
     *     
     */
    public HostTypePKType getHostType() {
        return hostType;
    }

    /**
     * Sets the value of the hostType property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypePKType }
     *     
     */
    public void setHostType(HostTypePKType value) {
        this.hostType = value;
    }

    /**
     * Gets the value of the hostTypes property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypeListType }
     *     
     */
    public HostTypeListType getHostTypes() {
        return hostTypes;
    }

    /**
     * Sets the value of the hostTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypeListType }
     *     
     */
    public void setHostTypes(HostTypeListType value) {
        this.hostTypes = value;
    }

    /**
     * Gets the value of the usedOnDevice property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isUsedOnDevice() {
        return usedOnDevice;
    }

    /**
     * Sets the value of the usedOnDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setUsedOnDevice(Boolean value) {
        this.usedOnDevice = value;
    }

    /**
     * Gets the value of the productCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCategory() {
        return productCategory;
    }

    /**
     * Sets the value of the productCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCategory(String value) {
        this.productCategory = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInAdmin() {
        return allowDownloadObsoleteFrInAdmin;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInAdmin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInAdmin(Boolean value) {
        this.allowDownloadObsoleteFrInAdmin = value;
    }

    /**
     * Gets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowDownloadObsoleteFrInPortal() {
        return allowDownloadObsoleteFrInPortal;
    }

    /**
     * Sets the value of the allowDownloadObsoleteFrInPortal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowDownloadObsoleteFrInPortal(Boolean value) {
        this.allowDownloadObsoleteFrInPortal = value;
    }

    /**
     * Gets the value of the productAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getProductAttributes() {
        return productAttributes;
    }

    /**
     * Sets the value of the productAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setProductAttributes(AttributeDescriptorDataType value) {
        this.productAttributes = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the endDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndDate() {
        return endDate;
    }

    /**
     * Sets the value of the endDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndDate(XMLGregorianCalendar value) {
        this.endDate = value;
    }

    /**
     * Gets the value of the upgradeEmailTemplateVarName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUpgradeEmailTemplateVarName() {
        return upgradeEmailTemplateVarName;
    }

    /**
     * Sets the value of the upgradeEmailTemplateVarName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUpgradeEmailTemplateVarName(String value) {
        this.upgradeEmailTemplateVarName = value;
    }

}
