
package com.flexnet.operations.webservices.o;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateSimpleEntitlementRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateSimpleEntitlementRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="simpleEntitlement" type="{urn:v1.webservices.operations.flexnet.com}updateSimpleEntitlementDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateSimpleEntitlementRequestType", propOrder = {
    "simpleEntitlement"
})
public class UpdateSimpleEntitlementRequestType {

    @XmlElement(required = true)
    protected List<UpdateSimpleEntitlementDataType> simpleEntitlement;

    /**
     * Gets the value of the simpleEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the simpleEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSimpleEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateSimpleEntitlementDataType }
     * 
     * 
     */
    public List<UpdateSimpleEntitlementDataType> getSimpleEntitlement() {
        if (simpleEntitlement == null) {
            simpleEntitlement = new ArrayList<UpdateSimpleEntitlementDataType>();
        }
        return this.simpleEntitlement;
    }

}
