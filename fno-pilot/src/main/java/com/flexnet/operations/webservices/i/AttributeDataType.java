
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AttributeDataType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AttributeDataType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="TEXT"/&gt;
 *     &lt;enumeration value="BOOLEAN"/&gt;
 *     &lt;enumeration value="NUMBER"/&gt;
 *     &lt;enumeration value="DATE"/&gt;
 *     &lt;enumeration value="MULTI_VALUED_TEXT"/&gt;
 *     &lt;enumeration value="LONGTEXT"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "AttributeDataType")
@XmlEnum
public enum AttributeDataType {

    TEXT,
    BOOLEAN,
    NUMBER,
    DATE,
    MULTI_VALUED_TEXT,
    LONGTEXT;

    public String value() {
        return name();
    }

    public static AttributeDataType fromValue(String v) {
        return valueOf(v);
    }

}
