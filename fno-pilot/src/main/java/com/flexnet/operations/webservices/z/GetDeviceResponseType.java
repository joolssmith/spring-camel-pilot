
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDeviceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDeviceResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v3.fne.webservices.operations.flexnet.com}OpsEmbeddedStatusInfoType"/&gt;
 *         &lt;element name="failedDevice" type="{urn:v3.fne.webservices.operations.flexnet.com}failedGetDeviceDataType" minOccurs="0"/&gt;
 *         &lt;element name="device" type="{urn:v3.fne.webservices.operations.flexnet.com}getDeviceResponseData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDeviceResponseType", propOrder = {
    "statusInfo",
    "failedDevice",
    "device"
})
public class GetDeviceResponseType {

    @XmlElement(required = true)
    protected OpsEmbeddedStatusInfoType statusInfo;
    protected FailedGetDeviceDataType failedDevice;
    protected GetDeviceResponseData device;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public OpsEmbeddedStatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public void setStatusInfo(OpsEmbeddedStatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedDevice property.
     * 
     * @return
     *     possible object is
     *     {@link FailedGetDeviceDataType }
     *     
     */
    public FailedGetDeviceDataType getFailedDevice() {
        return failedDevice;
    }

    /**
     * Sets the value of the failedDevice property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedGetDeviceDataType }
     *     
     */
    public void setFailedDevice(FailedGetDeviceDataType value) {
        this.failedDevice = value;
    }

    /**
     * Gets the value of the device property.
     * 
     * @return
     *     possible object is
     *     {@link GetDeviceResponseData }
     *     
     */
    public GetDeviceResponseData getDevice() {
        return device;
    }

    /**
     * Sets the value of the device property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetDeviceResponseData }
     *     
     */
    public void setDevice(GetDeviceResponseData value) {
        this.device = value;
    }

}
