
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceStatusSettableType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="deviceStatusSettableType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="ACTIVE"/&gt;
 *     &lt;enumeration value="OBSOLETE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "deviceStatusSettableType")
@XmlEnum
public enum DeviceStatusSettableType {

    ACTIVE,
    OBSOLETE;

    public String value() {
        return name();
    }

    public static DeviceStatusSettableType fromValue(String v) {
        return valueOf(v);
    }

}
