
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedSimpleEntitlementDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedSimpleEntitlementDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedSimpleEntitlement" type="{urn:com.macrovision:flexnet/operations}failedSimpleEntitlementDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedSimpleEntitlementDataListType", propOrder = {
    "failedSimpleEntitlement"
})
public class FailedSimpleEntitlementDataListType {

    protected List<FailedSimpleEntitlementDataType> failedSimpleEntitlement;

    /**
     * Gets the value of the failedSimpleEntitlement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedSimpleEntitlement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedSimpleEntitlement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedSimpleEntitlementDataType }
     * 
     * 
     */
    public List<FailedSimpleEntitlementDataType> getFailedSimpleEntitlement() {
        if (failedSimpleEntitlement == null) {
            failedSimpleEntitlement = new ArrayList<FailedSimpleEntitlementDataType>();
        }
        return this.failedSimpleEntitlement;
    }

}
