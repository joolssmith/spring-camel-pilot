
package com.flexnet.operations.webservices.g;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedProductCategoryDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedProductCategoryDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedProductCategory" type="{urn:com.macrovision:flexnet/operations}failedProductCategoryDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedProductCategoryDataListType", propOrder = {
    "failedProductCategory"
})
public class FailedProductCategoryDataListType {

    protected List<FailedProductCategoryDataType> failedProductCategory;

    /**
     * Gets the value of the failedProductCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedProductCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedProductCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedProductCategoryDataType }
     * 
     * 
     */
    public List<FailedProductCategoryDataType> getFailedProductCategory() {
        if (failedProductCategory == null) {
            failedProductCategory = new ArrayList<FailedProductCategoryDataType>();
        }
        return this.failedProductCategory;
    }

}
