
package com.flexnet.operations.webservices.p;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeviceServedStateQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeviceServedStateQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="value" type="{urn:v1.fne.webservices.operations.flexnet.com}deviceServedStatusType"/&gt;
 *         &lt;element name="searchType" type="{urn:v1.fne.webservices.operations.flexnet.com}simpleSearchType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceServedStateQueryType", propOrder = {
    "value",
    "searchType"
})
public class DeviceServedStateQueryType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceServedStatusType value;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected SimpleSearchType searchType;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceServedStatusType }
     *     
     */
    public DeviceServedStatusType getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceServedStatusType }
     *     
     */
    public void setValue(DeviceServedStatusType value) {
        this.value = value;
    }

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleSearchType }
     *     
     */
    public SimpleSearchType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleSearchType }
     *     
     */
    public void setSearchType(SimpleSearchType value) {
        this.searchType = value;
    }

}
