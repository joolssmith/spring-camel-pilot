
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for isEntitlementVisibleInTargetOrgRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="isEntitlementVisibleInTargetOrgRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="targetOrgID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "isEntitlementVisibleInTargetOrgRequestType", propOrder = {
    "entitlementID",
    "targetOrgID"
})
public class IsEntitlementVisibleInTargetOrgRequestType {

    @XmlElement(required = true)
    protected String entitlementID;
    @XmlElement(required = true)
    protected String targetOrgID;

    /**
     * Gets the value of the entitlementID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementID() {
        return entitlementID;
    }

    /**
     * Sets the value of the entitlementID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementID(String value) {
        this.entitlementID = value;
    }

    /**
     * Gets the value of the targetOrgID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetOrgID() {
        return targetOrgID;
    }

    /**
     * Sets the value of the targetOrgID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetOrgID(String value) {
        this.targetOrgID = value;
    }

}
