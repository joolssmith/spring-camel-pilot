
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedMaintenanceLineItemStateDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedMaintenanceLineItemStateDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedMaintenanceLineItem" type="{urn:v3.webservices.operations.flexnet.com}failedMaintenanceLineItemStateDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedMaintenanceLineItemStateDataListType", propOrder = {
    "failedMaintenanceLineItem"
})
public class FailedMaintenanceLineItemStateDataListType {

    protected List<FailedMaintenanceLineItemStateDataType> failedMaintenanceLineItem;

    /**
     * Gets the value of the failedMaintenanceLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedMaintenanceLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedMaintenanceLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedMaintenanceLineItemStateDataType }
     * 
     * 
     */
    public List<FailedMaintenanceLineItemStateDataType> getFailedMaintenanceLineItem() {
        if (failedMaintenanceLineItem == null) {
            failedMaintenanceLineItem = new ArrayList<FailedMaintenanceLineItemStateDataType>();
        }
        return this.failedMaintenanceLineItem;
    }

}
