package com.flexnet.operations.webservices.a;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.4
 * 2018-05-09T18:58:50.935+01:00
 * Generated source version: 3.2.4
 *
 */
@WebServiceClient(name = "EntitlementService",
                  wsdlLocation = "https://flex1113-uat.flexnetoperations.com/flexnet/services/EntitlementServiceSOAP?wsdl",
                  targetNamespace = "http://producersuite.flexerasoftware.com/EntitlementService/")
public class EntitlementService_Service extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "EntitlementService");
    public final static QName EntitlementServiceSOAP = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "EntitlementServiceSOAP");
    static {
        URL url = null;
        try {
            url = new URL("https://flex1113-uat.flexnetoperations.com/flexnet/services/EntitlementServiceSOAP?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(EntitlementService_Service.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "https://flex1113-uat.flexnetoperations.com/flexnet/services/EntitlementServiceSOAP?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public EntitlementService_Service(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public EntitlementService_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public EntitlementService_Service() {
        super(WSDL_LOCATION, SERVICE);
    }

    public EntitlementService_Service(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public EntitlementService_Service(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public EntitlementService_Service(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns EntitlementService
     */
    @WebEndpoint(name = "EntitlementServiceSOAP")
    public EntitlementService getEntitlementServiceSOAP() {
        return super.getPort(EntitlementServiceSOAP, EntitlementService.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns EntitlementService
     */
    @WebEndpoint(name = "EntitlementServiceSOAP")
    public EntitlementService getEntitlementServiceSOAP(WebServiceFeature... features) {
        return super.getPort(EntitlementServiceSOAP, EntitlementService.class, features);
    }

}
