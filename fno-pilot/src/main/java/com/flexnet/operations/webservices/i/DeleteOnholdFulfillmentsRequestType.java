
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteOnholdFulfillmentsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteOnholdFulfillmentsRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentIdList" type="{urn:com.macrovision:flexnet/operations}fulfillmentIdListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteOnholdFulfillmentsRequestType", propOrder = {
    "fulfillmentIdList"
})
public class DeleteOnholdFulfillmentsRequestType {

    @XmlElement(required = true)
    protected FulfillmentIdListType fulfillmentIdList;

    /**
     * Gets the value of the fulfillmentIdList property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdListType }
     *     
     */
    public FulfillmentIdListType getFulfillmentIdList() {
        return fulfillmentIdList;
    }

    /**
     * Sets the value of the fulfillmentIdList property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdListType }
     *     
     */
    public void setFulfillmentIdList(FulfillmentIdListType value) {
        this.fulfillmentIdList = value;
    }

}
