
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedLinkMaintenanceLineItemListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedLinkMaintenanceLineItemListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedLinkMaintenanceLineItem" type="{urn:com.macrovision:flexnet/operations}failedLinkMaintenanceLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedLinkMaintenanceLineItemListType", propOrder = {
    "failedLinkMaintenanceLineItem"
})
public class FailedLinkMaintenanceLineItemListType {

    protected List<FailedLinkMaintenanceLineItemDataType> failedLinkMaintenanceLineItem;

    /**
     * Gets the value of the failedLinkMaintenanceLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedLinkMaintenanceLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedLinkMaintenanceLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedLinkMaintenanceLineItemDataType }
     * 
     * 
     */
    public List<FailedLinkMaintenanceLineItemDataType> getFailedLinkMaintenanceLineItem() {
        if (failedLinkMaintenanceLineItem == null) {
            failedLinkMaintenanceLineItem = new ArrayList<FailedLinkMaintenanceLineItemDataType>();
        }
        return this.failedLinkMaintenanceLineItem;
    }

}
