
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateProductRelationshipDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateProductRelationshipDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="relationship" type="{urn:com.macrovision:flexnet/operations}updateProductRelationshipDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateProductRelationshipDataType", propOrder = {
    "relationship",
    "reason"
})
public class FailedUpdateProductRelationshipDataType {

    @XmlElement(required = true)
    protected UpdateProductRelationshipDataType relationship;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the relationship property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateProductRelationshipDataType }
     *     
     */
    public UpdateProductRelationshipDataType getRelationship() {
        return relationship;
    }

    /**
     * Sets the value of the relationship property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateProductRelationshipDataType }
     *     
     */
    public void setRelationship(UpdateProductRelationshipDataType value) {
        this.relationship = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
