
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for featureDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="featureDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="versionFormat" type="{urn:com.macrovision:flexnet/operations}VersionFormatType"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="featureOverrideParams" type="{urn:com.macrovision:flexnet/operations}featureOverrideParamsType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "featureDataType", propOrder = {
    "featureName",
    "versionFormat",
    "version",
    "description",
    "featureOverrideParams"
})
public class FeatureDataType {

    @XmlElement(required = true)
    protected String featureName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected VersionFormatType versionFormat;
    protected String version;
    protected String description;
    protected FeatureOverrideParamsType featureOverrideParams;

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureName(String value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the versionFormat property.
     * 
     * @return
     *     possible object is
     *     {@link VersionFormatType }
     *     
     */
    public VersionFormatType getVersionFormat() {
        return versionFormat;
    }

    /**
     * Sets the value of the versionFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionFormatType }
     *     
     */
    public void setVersionFormat(VersionFormatType value) {
        this.versionFormat = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the featureOverrideParams property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureOverrideParamsType }
     *     
     */
    public FeatureOverrideParamsType getFeatureOverrideParams() {
        return featureOverrideParams;
    }

    /**
     * Sets the value of the featureOverrideParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureOverrideParamsType }
     *     
     */
    public void setFeatureOverrideParams(FeatureOverrideParamsType value) {
        this.featureOverrideParams = value;
    }

}
