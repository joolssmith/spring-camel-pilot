
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getActivatableItemCountRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getActivatableItemCountRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v2.webservices.operations.flexnet.com}searchActivatableItemDataType"/&gt;
 *         &lt;element name="restrictToItemsReadyToActivate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getActivatableItemCountRequestType", propOrder = {
    "queryParams",
    "restrictToItemsReadyToActivate"
})
public class GetActivatableItemCountRequestType {

    @XmlElement(required = true)
    protected SearchActivatableItemDataType queryParams;
    protected Boolean restrictToItemsReadyToActivate;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link SearchActivatableItemDataType }
     *     
     */
    public SearchActivatableItemDataType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchActivatableItemDataType }
     *     
     */
    public void setQueryParams(SearchActivatableItemDataType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the restrictToItemsReadyToActivate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRestrictToItemsReadyToActivate() {
        return restrictToItemsReadyToActivate;
    }

    /**
     * Sets the value of the restrictToItemsReadyToActivate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRestrictToItemsReadyToActivate(Boolean value) {
        this.restrictToItemsReadyToActivate = value;
    }

}
