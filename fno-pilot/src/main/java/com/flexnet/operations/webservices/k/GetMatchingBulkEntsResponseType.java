
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMatchingBulkEntsResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMatchingBulkEntsResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:com.macrovision:flexnet/operations}StatusInfoType"/&gt;
 *         &lt;element name="responseData" type="{urn:com.macrovision:flexnet/operations}getMatchingBulkEntsResponseListType" minOccurs="0"/&gt;
 *         &lt;element name="failedData" type="{urn:com.macrovision:flexnet/operations}failedMatchingBulkEntsListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMatchingBulkEntsResponseType", propOrder = {
    "statusInfo",
    "responseData",
    "failedData"
})
public class GetMatchingBulkEntsResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected GetMatchingBulkEntsResponseListType responseData;
    protected FailedMatchingBulkEntsListType failedData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link GetMatchingBulkEntsResponseListType }
     *     
     */
    public GetMatchingBulkEntsResponseListType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMatchingBulkEntsResponseListType }
     *     
     */
    public void setResponseData(GetMatchingBulkEntsResponseListType value) {
        this.responseData = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedMatchingBulkEntsListType }
     *     
     */
    public FailedMatchingBulkEntsListType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedMatchingBulkEntsListType }
     *     
     */
    public void setFailedData(FailedMatchingBulkEntsListType value) {
        this.failedData = value;
    }

}
