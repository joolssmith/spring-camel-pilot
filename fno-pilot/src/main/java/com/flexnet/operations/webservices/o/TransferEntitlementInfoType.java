
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for transferEntitlementInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="transferEntitlementInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementIdentifier" type="{urn:v1.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="organizationTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="retainExistingIds" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="returnActiveFulfillments" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="forceTransferEvenIfNoTargetUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="forceTransferEvenIfParentAndChildSeparated" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transferEntitlementInfoType", propOrder = {
    "entitlementIdentifier",
    "organizationTo",
    "retainExistingIds",
    "returnActiveFulfillments",
    "forceTransferEvenIfNoTargetUsers",
    "forceTransferEvenIfParentAndChildSeparated"
})
public class TransferEntitlementInfoType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementIdentifier;
    @XmlElement(required = true)
    protected String organizationTo;
    protected Boolean retainExistingIds;
    protected Boolean returnActiveFulfillments;
    protected Boolean forceTransferEvenIfNoTargetUsers;
    protected Boolean forceTransferEvenIfParentAndChildSeparated;

    /**
     * Gets the value of the entitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }

    /**
     * Sets the value of the entitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementIdentifier(EntitlementIdentifierType value) {
        this.entitlementIdentifier = value;
    }

    /**
     * Gets the value of the organizationTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganizationTo() {
        return organizationTo;
    }

    /**
     * Sets the value of the organizationTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganizationTo(String value) {
        this.organizationTo = value;
    }

    /**
     * Gets the value of the retainExistingIds property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRetainExistingIds() {
        return retainExistingIds;
    }

    /**
     * Sets the value of the retainExistingIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRetainExistingIds(Boolean value) {
        this.retainExistingIds = value;
    }

    /**
     * Gets the value of the returnActiveFulfillments property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isReturnActiveFulfillments() {
        return returnActiveFulfillments;
    }

    /**
     * Sets the value of the returnActiveFulfillments property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setReturnActiveFulfillments(Boolean value) {
        this.returnActiveFulfillments = value;
    }

    /**
     * Gets the value of the forceTransferEvenIfNoTargetUsers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceTransferEvenIfNoTargetUsers() {
        return forceTransferEvenIfNoTargetUsers;
    }

    /**
     * Sets the value of the forceTransferEvenIfNoTargetUsers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceTransferEvenIfNoTargetUsers(Boolean value) {
        this.forceTransferEvenIfNoTargetUsers = value;
    }

    /**
     * Gets the value of the forceTransferEvenIfParentAndChildSeparated property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isForceTransferEvenIfParentAndChildSeparated() {
        return forceTransferEvenIfParentAndChildSeparated;
    }

    /**
     * Sets the value of the forceTransferEvenIfParentAndChildSeparated property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setForceTransferEvenIfParentAndChildSeparated(Boolean value) {
        this.forceTransferEvenIfParentAndChildSeparated = value;
    }

}
