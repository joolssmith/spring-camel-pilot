
package com.flexnet.operations.webservices.e;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.e package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateTestRequest_QNAME = new QName("urn:com.flexerasoftware:operations/test", "createTestRequest");
    private final static QName _CreateTestResponse_QNAME = new QName("urn:com.flexerasoftware:operations/test", "createTestResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.e
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateTestRequestType }
     * 
     */
    public CreateTestRequestType createCreateTestRequestType() {
        return new CreateTestRequestType();
    }

    /**
     * Create an instance of {@link CreateTestResponseType }
     * 
     */
    public CreateTestResponseType createCreateTestResponseType() {
        return new CreateTestResponseType();
    }

    /**
     * Create an instance of {@link WeekDays }
     * 
     */
    public WeekDays createWeekDays() {
        return new WeekDays();
    }

    /**
     * Create an instance of {@link TestStatusInfoType }
     * 
     */
    public TestStatusInfoType createTestStatusInfoType() {
        return new TestStatusInfoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTestRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.flexerasoftware:operations/test", name = "createTestRequest")
    public JAXBElement<CreateTestRequestType> createCreateTestRequest(CreateTestRequestType value) {
        return new JAXBElement<CreateTestRequestType>(_CreateTestRequest_QNAME, CreateTestRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateTestResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.flexerasoftware:operations/test", name = "createTestResponse")
    public JAXBElement<CreateTestResponseType> createCreateTestResponse(CreateTestResponseType value) {
        return new JAXBElement<CreateTestResponseType>(_CreateTestResponse_QNAME, CreateTestResponseType.class, null, value);
    }

}
