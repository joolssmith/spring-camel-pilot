
package com.flexnet.operations.webservices.h;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for usageSummaryDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="usageSummaryDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="summaryTimeUniqueId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="summaryTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="server" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="serverAlias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serverLastSyncTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="entitlementUniqueId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="activationUniqueId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="activationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="featureUniqueId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="featureName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="featureVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderedCount" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="usedCount" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="provisionedCount" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "usageSummaryDataType", propOrder = {
    "summaryTimeUniqueId",
    "summaryTime",
    "server",
    "serverAlias",
    "serverLastSyncTime",
    "entitlementUniqueId",
    "entitlementId",
    "activationUniqueId",
    "activationId",
    "featureUniqueId",
    "featureName",
    "featureVersion",
    "orderedCount",
    "usedCount",
    "provisionedCount"
})
public class UsageSummaryDataType {

    protected String summaryTimeUniqueId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar summaryTime;
    protected DeviceIdentifier server;
    protected String serverAlias;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar serverLastSyncTime;
    protected String entitlementUniqueId;
    protected String entitlementId;
    protected String activationUniqueId;
    protected String activationId;
    protected String featureUniqueId;
    protected String featureName;
    protected String featureVersion;
    protected BigInteger orderedCount;
    @XmlElement(required = true)
    protected BigInteger usedCount;
    @XmlElement(required = true)
    protected BigInteger provisionedCount;

    /**
     * Gets the value of the summaryTimeUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummaryTimeUniqueId() {
        return summaryTimeUniqueId;
    }

    /**
     * Sets the value of the summaryTimeUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummaryTimeUniqueId(String value) {
        this.summaryTimeUniqueId = value;
    }

    /**
     * Gets the value of the summaryTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSummaryTime() {
        return summaryTime;
    }

    /**
     * Sets the value of the summaryTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSummaryTime(XMLGregorianCalendar value) {
        this.summaryTime = value;
    }

    /**
     * Gets the value of the server property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getServer() {
        return server;
    }

    /**
     * Sets the value of the server property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setServer(DeviceIdentifier value) {
        this.server = value;
    }

    /**
     * Gets the value of the serverAlias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerAlias() {
        return serverAlias;
    }

    /**
     * Sets the value of the serverAlias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerAlias(String value) {
        this.serverAlias = value;
    }

    /**
     * Gets the value of the serverLastSyncTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getServerLastSyncTime() {
        return serverLastSyncTime;
    }

    /**
     * Sets the value of the serverLastSyncTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setServerLastSyncTime(XMLGregorianCalendar value) {
        this.serverLastSyncTime = value;
    }

    /**
     * Gets the value of the entitlementUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementUniqueId() {
        return entitlementUniqueId;
    }

    /**
     * Sets the value of the entitlementUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementUniqueId(String value) {
        this.entitlementUniqueId = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementId(String value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the activationUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationUniqueId() {
        return activationUniqueId;
    }

    /**
     * Sets the value of the activationUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationUniqueId(String value) {
        this.activationUniqueId = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationId(String value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the featureUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureUniqueId() {
        return featureUniqueId;
    }

    /**
     * Sets the value of the featureUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureUniqueId(String value) {
        this.featureUniqueId = value;
    }

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureName(String value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the featureVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFeatureVersion() {
        return featureVersion;
    }

    /**
     * Sets the value of the featureVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFeatureVersion(String value) {
        this.featureVersion = value;
    }

    /**
     * Gets the value of the orderedCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOrderedCount() {
        return orderedCount;
    }

    /**
     * Sets the value of the orderedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOrderedCount(BigInteger value) {
        this.orderedCount = value;
    }

    /**
     * Gets the value of the usedCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUsedCount() {
        return usedCount;
    }

    /**
     * Sets the value of the usedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUsedCount(BigInteger value) {
        this.usedCount = value;
    }

    /**
     * Gets the value of the provisionedCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getProvisionedCount() {
        return provisionedCount;
    }

    /**
     * Sets the value of the provisionedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setProvisionedCount(BigInteger value) {
        this.provisionedCount = value;
    }

}
