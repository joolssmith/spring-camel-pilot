
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedLineItemStateDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedLineItemStateDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedLineItem" type="{urn:com.macrovision:flexnet/operations}failedLineItemStateDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedLineItemStateDataListType", propOrder = {
    "failedLineItem"
})
public class FailedLineItemStateDataListType {

    protected List<FailedLineItemStateDataType> failedLineItem;

    /**
     * Gets the value of the failedLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedLineItemStateDataType }
     * 
     * 
     */
    public List<FailedLineItemStateDataType> getFailedLineItem() {
        if (failedLineItem == null) {
            failedLineItem = new ArrayList<FailedLineItemStateDataType>();
        }
        return this.failedLineItem;
    }

}
