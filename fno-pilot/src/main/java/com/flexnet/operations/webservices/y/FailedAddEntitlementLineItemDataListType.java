
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedAddEntitlementLineItemDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedAddEntitlementLineItemDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedLineItemData" type="{urn:v3.webservices.operations.flexnet.com}failedAddEntitlementLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedAddEntitlementLineItemDataListType", propOrder = {
    "failedLineItemData"
})
public class FailedAddEntitlementLineItemDataListType {

    protected List<FailedAddEntitlementLineItemDataType> failedLineItemData;

    /**
     * Gets the value of the failedLineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedLineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedAddEntitlementLineItemDataType }
     * 
     * 
     */
    public List<FailedAddEntitlementLineItemDataType> getFailedLineItemData() {
        if (failedLineItemData == null) {
            failedLineItemData = new ArrayList<FailedAddEntitlementLineItemDataType>();
        }
        return this.failedLineItemData;
    }

}
