
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedMatchingLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedMatchingLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemInfo" type="{urn:v2.webservices.operations.flexnet.com}getMatchingLineItemInfoType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedMatchingLineItemDataType", propOrder = {
    "lineItemInfo",
    "reason"
})
public class FailedMatchingLineItemDataType {

    @XmlElement(required = true)
    protected GetMatchingLineItemInfoType lineItemInfo;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the lineItemInfo property.
     * 
     * @return
     *     possible object is
     *     {@link GetMatchingLineItemInfoType }
     *     
     */
    public GetMatchingLineItemInfoType getLineItemInfo() {
        return lineItemInfo;
    }

    /**
     * Sets the value of the lineItemInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link GetMatchingLineItemInfoType }
     *     
     */
    public void setLineItemInfo(GetMatchingLineItemInfoType value) {
        this.lineItemInfo = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
