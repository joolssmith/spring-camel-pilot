
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStateChangeHistoryRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getStateChangeHistoryRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureList" type="{urn:com.macrovision:flexnet/operations}featureListType" minOccurs="0"/&gt;
 *         &lt;element name="featureBundleList" type="{urn:com.macrovision:flexnet/operations}featureBundleListType" minOccurs="0"/&gt;
 *         &lt;element name="productList" type="{urn:com.macrovision:flexnet/operations}productListType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModelList" type="{urn:com.macrovision:flexnet/operations}licenseModelListType" minOccurs="0"/&gt;
 *         &lt;element name="simpleEntitlementList" type="{urn:com.macrovision:flexnet/operations}entitlementListType" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntitlementList" type="{urn:com.macrovision:flexnet/operations}entitlementListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStateChangeHistoryRequestType", propOrder = {
    "featureList",
    "featureBundleList",
    "productList",
    "licenseModelList",
    "simpleEntitlementList",
    "bulkEntitlementList"
})
public class GetStateChangeHistoryRequestType {

    protected FeatureListType featureList;
    protected FeatureBundleListType featureBundleList;
    protected ProductListType productList;
    protected LicenseModelListType licenseModelList;
    protected EntitlementListType simpleEntitlementList;
    protected EntitlementListType bulkEntitlementList;

    /**
     * Gets the value of the featureList property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureListType }
     *     
     */
    public FeatureListType getFeatureList() {
        return featureList;
    }

    /**
     * Sets the value of the featureList property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureListType }
     *     
     */
    public void setFeatureList(FeatureListType value) {
        this.featureList = value;
    }

    /**
     * Gets the value of the featureBundleList property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleListType }
     *     
     */
    public FeatureBundleListType getFeatureBundleList() {
        return featureBundleList;
    }

    /**
     * Sets the value of the featureBundleList property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleListType }
     *     
     */
    public void setFeatureBundleList(FeatureBundleListType value) {
        this.featureBundleList = value;
    }

    /**
     * Gets the value of the productList property.
     * 
     * @return
     *     possible object is
     *     {@link ProductListType }
     *     
     */
    public ProductListType getProductList() {
        return productList;
    }

    /**
     * Sets the value of the productList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductListType }
     *     
     */
    public void setProductList(ProductListType value) {
        this.productList = value;
    }

    /**
     * Gets the value of the licenseModelList property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelListType }
     *     
     */
    public LicenseModelListType getLicenseModelList() {
        return licenseModelList;
    }

    /**
     * Sets the value of the licenseModelList property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelListType }
     *     
     */
    public void setLicenseModelList(LicenseModelListType value) {
        this.licenseModelList = value;
    }

    /**
     * Gets the value of the simpleEntitlementList property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementListType }
     *     
     */
    public EntitlementListType getSimpleEntitlementList() {
        return simpleEntitlementList;
    }

    /**
     * Sets the value of the simpleEntitlementList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementListType }
     *     
     */
    public void setSimpleEntitlementList(EntitlementListType value) {
        this.simpleEntitlementList = value;
    }

    /**
     * Gets the value of the bulkEntitlementList property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementListType }
     *     
     */
    public EntitlementListType getBulkEntitlementList() {
        return bulkEntitlementList;
    }

    /**
     * Sets the value of the bulkEntitlementList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementListType }
     *     
     */
    public void setBulkEntitlementList(EntitlementListType value) {
        this.bulkEntitlementList = value;
    }

}
