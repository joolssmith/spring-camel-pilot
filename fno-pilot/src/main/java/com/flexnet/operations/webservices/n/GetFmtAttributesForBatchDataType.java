
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFmtAttributesForBatchDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFmtAttributesForBatchDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="needStartDate" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needVersionDate" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needVersionStartDate" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needServerId" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needNodeLockId" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needCustomHost" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needCount" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needSoldTo" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="overDraftData" type="{urn:v1.webservices.operations.flexnet.com}overDraftDataListType"/&gt;
 *         &lt;element name="modelType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="modelAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeMetaDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="hostAttributes" type="{urn:v1.webservices.operations.flexnet.com}attributeMetaDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="needTimeZone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFmtAttributesForBatchDataType", propOrder = {
    "needStartDate",
    "needVersionDate",
    "needVersionStartDate",
    "needServerId",
    "needNodeLockId",
    "needCustomHost",
    "needCount",
    "needSoldTo",
    "overDraftData",
    "modelType",
    "modelAttributes",
    "hostAttributes",
    "needTimeZone"
})
public class GetFmtAttributesForBatchDataType {

    protected boolean needStartDate;
    protected boolean needVersionDate;
    protected boolean needVersionStartDate;
    protected boolean needServerId;
    protected boolean needNodeLockId;
    protected boolean needCustomHost;
    protected boolean needCount;
    protected boolean needSoldTo;
    @XmlElement(required = true)
    protected OverDraftDataListType overDraftData;
    @XmlElement(required = true)
    protected String modelType;
    protected AttributeMetaDescriptorDataType modelAttributes;
    protected AttributeMetaDescriptorDataType hostAttributes;
    protected Boolean needTimeZone;

    /**
     * Gets the value of the needStartDate property.
     * 
     */
    public boolean isNeedStartDate() {
        return needStartDate;
    }

    /**
     * Sets the value of the needStartDate property.
     * 
     */
    public void setNeedStartDate(boolean value) {
        this.needStartDate = value;
    }

    /**
     * Gets the value of the needVersionDate property.
     * 
     */
    public boolean isNeedVersionDate() {
        return needVersionDate;
    }

    /**
     * Sets the value of the needVersionDate property.
     * 
     */
    public void setNeedVersionDate(boolean value) {
        this.needVersionDate = value;
    }

    /**
     * Gets the value of the needVersionStartDate property.
     * 
     */
    public boolean isNeedVersionStartDate() {
        return needVersionStartDate;
    }

    /**
     * Sets the value of the needVersionStartDate property.
     * 
     */
    public void setNeedVersionStartDate(boolean value) {
        this.needVersionStartDate = value;
    }

    /**
     * Gets the value of the needServerId property.
     * 
     */
    public boolean isNeedServerId() {
        return needServerId;
    }

    /**
     * Sets the value of the needServerId property.
     * 
     */
    public void setNeedServerId(boolean value) {
        this.needServerId = value;
    }

    /**
     * Gets the value of the needNodeLockId property.
     * 
     */
    public boolean isNeedNodeLockId() {
        return needNodeLockId;
    }

    /**
     * Sets the value of the needNodeLockId property.
     * 
     */
    public void setNeedNodeLockId(boolean value) {
        this.needNodeLockId = value;
    }

    /**
     * Gets the value of the needCustomHost property.
     * 
     */
    public boolean isNeedCustomHost() {
        return needCustomHost;
    }

    /**
     * Sets the value of the needCustomHost property.
     * 
     */
    public void setNeedCustomHost(boolean value) {
        this.needCustomHost = value;
    }

    /**
     * Gets the value of the needCount property.
     * 
     */
    public boolean isNeedCount() {
        return needCount;
    }

    /**
     * Sets the value of the needCount property.
     * 
     */
    public void setNeedCount(boolean value) {
        this.needCount = value;
    }

    /**
     * Gets the value of the needSoldTo property.
     * 
     */
    public boolean isNeedSoldTo() {
        return needSoldTo;
    }

    /**
     * Sets the value of the needSoldTo property.
     * 
     */
    public void setNeedSoldTo(boolean value) {
        this.needSoldTo = value;
    }

    /**
     * Gets the value of the overDraftData property.
     * 
     * @return
     *     possible object is
     *     {@link OverDraftDataListType }
     *     
     */
    public OverDraftDataListType getOverDraftData() {
        return overDraftData;
    }

    /**
     * Sets the value of the overDraftData property.
     * 
     * @param value
     *     allowed object is
     *     {@link OverDraftDataListType }
     *     
     */
    public void setOverDraftData(OverDraftDataListType value) {
        this.overDraftData = value;
    }

    /**
     * Gets the value of the modelType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelType() {
        return modelType;
    }

    /**
     * Sets the value of the modelType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelType(String value) {
        this.modelType = value;
    }

    /**
     * Gets the value of the modelAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public AttributeMetaDescriptorDataType getModelAttributes() {
        return modelAttributes;
    }

    /**
     * Sets the value of the modelAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public void setModelAttributes(AttributeMetaDescriptorDataType value) {
        this.modelAttributes = value;
    }

    /**
     * Gets the value of the hostAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public AttributeMetaDescriptorDataType getHostAttributes() {
        return hostAttributes;
    }

    /**
     * Sets the value of the hostAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeMetaDescriptorDataType }
     *     
     */
    public void setHostAttributes(AttributeMetaDescriptorDataType value) {
        this.hostAttributes = value;
    }

    /**
     * Gets the value of the needTimeZone property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNeedTimeZone() {
        return needTimeZone;
    }

    /**
     * Sets the value of the needTimeZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNeedTimeZone(Boolean value) {
        this.needTimeZone = value;
    }

}
