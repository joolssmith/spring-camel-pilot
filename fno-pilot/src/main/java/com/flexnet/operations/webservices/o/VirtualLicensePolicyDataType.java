
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for virtualLicensePolicyDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="virtualLicensePolicyDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="isVirtualLicense" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "virtualLicensePolicyDataType", propOrder = {
    "isVirtualLicense"
})
public class VirtualLicensePolicyDataType {

    protected boolean isVirtualLicense;

    /**
     * Gets the value of the isVirtualLicense property.
     * 
     */
    public boolean isIsVirtualLicense() {
        return isVirtualLicense;
    }

    /**
     * Sets the value of the isVirtualLicense property.
     * 
     */
    public void setIsVirtualLicense(boolean value) {
        this.isVirtualLicense = value;
    }

}
