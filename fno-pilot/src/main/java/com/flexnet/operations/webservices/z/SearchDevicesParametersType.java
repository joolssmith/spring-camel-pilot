
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchDevicesParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchDevicesParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="name" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceIdType" type="{urn:v3.fne.webservices.operations.flexnet.com}DeviceIdTypeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceClasses" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceClassList" minOccurs="0"/&gt;
 *         &lt;element name="hosted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="parentId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldToAcctId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{urn:v3.fne.webservices.operations.flexnet.com}DeviceStatusQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnEntitlementId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnActivationId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnProductName" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnProductVersion" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnPartNumber" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureName" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="userString" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchDevicesParametersType", propOrder = {
    "name",
    "deviceId",
    "deviceIdType",
    "deviceClasses",
    "hosted",
    "parentId",
    "hostTypeName",
    "soldTo",
    "soldToAcctId",
    "description",
    "status",
    "addOnEntitlementId",
    "addOnActivationId",
    "addOnProductName",
    "addOnProductVersion",
    "addOnPartNumber",
    "featureName",
    "userString"
})
public class SearchDevicesParametersType {

    protected SimpleQueryType name;
    protected SimpleQueryType deviceId;
    protected DeviceIdTypeQueryType deviceIdType;
    protected DeviceClassList deviceClasses;
    protected Boolean hosted;
    protected SimpleQueryType parentId;
    protected SimpleQueryType hostTypeName;
    protected SimpleQueryType soldTo;
    protected SimpleQueryType soldToAcctId;
    protected SimpleQueryType description;
    protected DeviceStatusQueryType status;
    protected SimpleQueryType addOnEntitlementId;
    protected SimpleQueryType addOnActivationId;
    protected SimpleQueryType addOnProductName;
    protected SimpleQueryType addOnProductVersion;
    protected SimpleQueryType addOnPartNumber;
    protected SimpleQueryType featureName;
    protected SimpleQueryType userString;

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setName(SimpleQueryType value) {
        this.name = value;
    }

    /**
     * Gets the value of the deviceId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDeviceId(SimpleQueryType value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the deviceIdType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public DeviceIdTypeQueryType getDeviceIdType() {
        return deviceIdType;
    }

    /**
     * Sets the value of the deviceIdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public void setDeviceIdType(DeviceIdTypeQueryType value) {
        this.deviceIdType = value;
    }

    /**
     * Gets the value of the deviceClasses property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceClassList }
     *     
     */
    public DeviceClassList getDeviceClasses() {
        return deviceClasses;
    }

    /**
     * Sets the value of the deviceClasses property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceClassList }
     *     
     */
    public void setDeviceClasses(DeviceClassList value) {
        this.deviceClasses = value;
    }

    /**
     * Gets the value of the hosted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHosted() {
        return hosted;
    }

    /**
     * Sets the value of the hosted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHosted(Boolean value) {
        this.hosted = value;
    }

    /**
     * Gets the value of the parentId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getParentId() {
        return parentId;
    }

    /**
     * Sets the value of the parentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setParentId(SimpleQueryType value) {
        this.parentId = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setHostTypeName(SimpleQueryType value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldTo(SimpleQueryType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the soldToAcctId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldToAcctId() {
        return soldToAcctId;
    }

    /**
     * Sets the value of the soldToAcctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldToAcctId(SimpleQueryType value) {
        this.soldToAcctId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDescription(SimpleQueryType value) {
        this.description = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceStatusQueryType }
     *     
     */
    public DeviceStatusQueryType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceStatusQueryType }
     *     
     */
    public void setStatus(DeviceStatusQueryType value) {
        this.status = value;
    }

    /**
     * Gets the value of the addOnEntitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnEntitlementId() {
        return addOnEntitlementId;
    }

    /**
     * Sets the value of the addOnEntitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnEntitlementId(SimpleQueryType value) {
        this.addOnEntitlementId = value;
    }

    /**
     * Gets the value of the addOnActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnActivationId() {
        return addOnActivationId;
    }

    /**
     * Sets the value of the addOnActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnActivationId(SimpleQueryType value) {
        this.addOnActivationId = value;
    }

    /**
     * Gets the value of the addOnProductName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnProductName() {
        return addOnProductName;
    }

    /**
     * Sets the value of the addOnProductName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnProductName(SimpleQueryType value) {
        this.addOnProductName = value;
    }

    /**
     * Gets the value of the addOnProductVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnProductVersion() {
        return addOnProductVersion;
    }

    /**
     * Sets the value of the addOnProductVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnProductVersion(SimpleQueryType value) {
        this.addOnProductVersion = value;
    }

    /**
     * Gets the value of the addOnPartNumber property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnPartNumber() {
        return addOnPartNumber;
    }

    /**
     * Sets the value of the addOnPartNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnPartNumber(SimpleQueryType value) {
        this.addOnPartNumber = value;
    }

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFeatureName(SimpleQueryType value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the userString property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getUserString() {
        return userString;
    }

    /**
     * Sets the value of the userString property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setUserString(SimpleQueryType value) {
        this.userString = value;
    }

}
