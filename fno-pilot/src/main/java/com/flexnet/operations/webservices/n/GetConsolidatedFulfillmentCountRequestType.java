
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getConsolidatedFulfillmentCountRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getConsolidatedFulfillmentCountRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v1.webservices.operations.flexnet.com}consolidatedFulfillmentsQPType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getConsolidatedFulfillmentCountRequestType", propOrder = {
    "queryParams"
})
public class GetConsolidatedFulfillmentCountRequestType {

    protected ConsolidatedFulfillmentsQPType queryParams;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link ConsolidatedFulfillmentsQPType }
     *     
     */
    public ConsolidatedFulfillmentsQPType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsolidatedFulfillmentsQPType }
     *     
     */
    public void setQueryParams(ConsolidatedFulfillmentsQPType value) {
        this.queryParams = value;
    }

}
