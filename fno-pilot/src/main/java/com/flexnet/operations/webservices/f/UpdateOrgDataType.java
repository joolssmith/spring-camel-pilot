
package com.flexnet.operations.webservices.f;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateOrgDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateOrgDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="organization" type="{urn:com.macrovision:flexnet/operations}organizationIdentifierType"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="displayName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="address" type="{urn:com.macrovision:flexnet/operations}addressDataType" minOccurs="0"/&gt;
 *         &lt;element name="subOrganizations" type="{urn:com.macrovision:flexnet/operations}updateSubOrganizationsListType" minOccurs="0"/&gt;
 *         &lt;element name="relatedOrganizations" type="{urn:com.macrovision:flexnet/operations}updateRelatedOrganizationsListType" minOccurs="0"/&gt;
 *         &lt;element name="visible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:com.macrovision:flexnet/operations}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateOrgDataType", propOrder = {
    "organization",
    "name",
    "displayName",
    "description",
    "address",
    "subOrganizations",
    "relatedOrganizations",
    "visible",
    "customAttributes"
})
public class UpdateOrgDataType {

    @XmlElement(required = true)
    protected OrganizationIdentifierType organization;
    protected String name;
    protected String displayName;
    protected String description;
    protected AddressDataType address;
    protected UpdateSubOrganizationsListType subOrganizations;
    protected UpdateRelatedOrganizationsListType relatedOrganizations;
    protected Boolean visible;
    protected AttributeDescriptorDataType customAttributes;

    /**
     * Gets the value of the organization property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getOrganization() {
        return organization;
    }

    /**
     * Sets the value of the organization property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setOrganization(OrganizationIdentifierType value) {
        this.organization = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the displayName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the value of the displayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisplayName(String value) {
        this.displayName = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link AddressDataType }
     *     
     */
    public AddressDataType getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressDataType }
     *     
     */
    public void setAddress(AddressDataType value) {
        this.address = value;
    }

    /**
     * Gets the value of the subOrganizations property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateSubOrganizationsListType }
     *     
     */
    public UpdateSubOrganizationsListType getSubOrganizations() {
        return subOrganizations;
    }

    /**
     * Sets the value of the subOrganizations property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateSubOrganizationsListType }
     *     
     */
    public void setSubOrganizations(UpdateSubOrganizationsListType value) {
        this.subOrganizations = value;
    }

    /**
     * Gets the value of the relatedOrganizations property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateRelatedOrganizationsListType }
     *     
     */
    public UpdateRelatedOrganizationsListType getRelatedOrganizations() {
        return relatedOrganizations;
    }

    /**
     * Sets the value of the relatedOrganizations property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateRelatedOrganizationsListType }
     *     
     */
    public void setRelatedOrganizations(UpdateRelatedOrganizationsListType value) {
        this.relatedOrganizations = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisible(Boolean value) {
        this.visible = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

}
