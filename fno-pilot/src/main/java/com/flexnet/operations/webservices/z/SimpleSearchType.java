
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for simpleSearchType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="simpleSearchType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="STARTS_WITH"/&gt;
 *     &lt;enumeration value="CONTAINS"/&gt;
 *     &lt;enumeration value="ENDS_WITH"/&gt;
 *     &lt;enumeration value="EQUALS"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "simpleSearchType")
@XmlEnum
public enum SimpleSearchType {

    STARTS_WITH,
    CONTAINS,
    ENDS_WITH,
    EQUALS;

    public String value() {
        return name();
    }

    public static SimpleSearchType fromValue(String v) {
        return valueOf(v);
    }

}
