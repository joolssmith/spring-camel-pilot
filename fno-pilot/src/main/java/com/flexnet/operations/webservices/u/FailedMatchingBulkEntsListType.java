
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedMatchingBulkEntsListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedMatchingBulkEntsListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedBulkEnt" type="{urn:v2.webservices.operations.flexnet.com}failedMatchingBulkEntDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedMatchingBulkEntsListType", propOrder = {
    "failedBulkEnt"
})
public class FailedMatchingBulkEntsListType {

    @XmlElement(required = true)
    protected List<FailedMatchingBulkEntDataType> failedBulkEnt;

    /**
     * Gets the value of the failedBulkEnt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedBulkEnt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedBulkEnt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedMatchingBulkEntDataType }
     * 
     * 
     */
    public List<FailedMatchingBulkEntDataType> getFailedBulkEnt() {
        if (failedBulkEnt == null) {
            failedBulkEnt = new ArrayList<FailedMatchingBulkEntDataType>();
        }
        return this.failedBulkEnt;
    }

}
