package com.flexnet.operations.webservices.e;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.4
 * 2018-05-09T18:59:04.324+01:00
 * Generated source version: 3.2.4
 *
 */
@WebServiceClient(name = "TestService",
                  wsdlLocation = "https://flex1113-uat.flexnetoperations.com/flexnet/services/TestService?wsdl",
                  targetNamespace = "urn:com.flexerasoftware:operations/test")
public class TestService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("urn:com.flexerasoftware:operations/test", "TestService");
    public final static QName TestService = new QName("urn:com.flexerasoftware:operations/test", "TestService");
    static {
        URL url = null;
        try {
            url = new URL("https://flex1113-uat.flexnetoperations.com/flexnet/services/TestService?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(TestService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "https://flex1113-uat.flexnetoperations.com/flexnet/services/TestService?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public TestService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public TestService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public TestService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public TestService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public TestService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public TestService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns TestServiceInterface
     */
    @WebEndpoint(name = "TestService")
    public TestServiceInterface getTestService() {
        return super.getPort(TestService, TestServiceInterface.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns TestServiceInterface
     */
    @WebEndpoint(name = "TestService")
    public TestServiceInterface getTestService(WebServiceFeature... features) {
        return super.getPort(TestService, TestServiceInterface.class, features);
    }

}
