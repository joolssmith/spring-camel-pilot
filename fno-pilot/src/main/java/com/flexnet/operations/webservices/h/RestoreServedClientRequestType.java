
package com.flexnet.operations.webservices.h;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for restoreServedClientRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="restoreServedClientRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="servedClientHostId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="servedClientIdType" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdTypeType"/&gt;
 *         &lt;element name="newHostID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serverHostIdTypes" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdTypeType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "restoreServedClientRequestType", propOrder = {
    "servedClientHostId",
    "servedClientIdType",
    "newHostID",
    "serverHostIdTypes"
})
public class RestoreServedClientRequestType {

    @XmlElement(required = true)
    protected String servedClientHostId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceIdTypeType servedClientIdType;
    protected String newHostID;
    @XmlSchemaType(name = "NMTOKEN")
    protected List<DeviceIdTypeType> serverHostIdTypes;

    /**
     * Gets the value of the servedClientHostId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServedClientHostId() {
        return servedClientHostId;
    }

    /**
     * Sets the value of the servedClientHostId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServedClientHostId(String value) {
        this.servedClientHostId = value;
    }

    /**
     * Gets the value of the servedClientIdType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdTypeType }
     *     
     */
    public DeviceIdTypeType getServedClientIdType() {
        return servedClientIdType;
    }

    /**
     * Sets the value of the servedClientIdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdTypeType }
     *     
     */
    public void setServedClientIdType(DeviceIdTypeType value) {
        this.servedClientIdType = value;
    }

    /**
     * Gets the value of the newHostID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNewHostID() {
        return newHostID;
    }

    /**
     * Sets the value of the newHostID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNewHostID(String value) {
        this.newHostID = value;
    }

    /**
     * Gets the value of the serverHostIdTypes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the serverHostIdTypes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getServerHostIdTypes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeviceIdTypeType }
     * 
     * 
     */
    public List<DeviceIdTypeType> getServerHostIdTypes() {
        if (serverHostIdTypes == null) {
            serverHostIdTypes = new ArrayList<DeviceIdTypeType>();
        }
        return this.serverHostIdTypes;
    }

}
