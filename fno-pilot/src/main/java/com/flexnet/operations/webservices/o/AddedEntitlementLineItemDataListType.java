
package com.flexnet.operations.webservices.o;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addedEntitlementLineItemDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addedEntitlementLineItemDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addedLineItems" type="{urn:v1.webservices.operations.flexnet.com}addedEntitlementLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addedEntitlementLineItemDataListType", propOrder = {
    "addedLineItems"
})
public class AddedEntitlementLineItemDataListType {

    protected List<AddedEntitlementLineItemDataType> addedLineItems;

    /**
     * Gets the value of the addedLineItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addedLineItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddedLineItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddedEntitlementLineItemDataType }
     * 
     * 
     */
    public List<AddedEntitlementLineItemDataType> getAddedLineItems() {
        if (addedLineItems == null) {
            addedLineItems = new ArrayList<AddedEntitlementLineItemDataType>();
        }
        return this.addedLineItems;
    }

}
