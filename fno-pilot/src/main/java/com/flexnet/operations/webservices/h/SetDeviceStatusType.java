
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setDeviceStatusType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setDeviceStatusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceIdentifier" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdentifier"/&gt;
 *         &lt;element name="status" type="{urn:com.macrovision:flexnet/opsembedded}deviceStatusSettableType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setDeviceStatusType", propOrder = {
    "deviceIdentifier",
    "status"
})
public class SetDeviceStatusType {

    @XmlElement(required = true)
    protected DeviceIdentifier deviceIdentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceStatusSettableType status;

    /**
     * Gets the value of the deviceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getDeviceIdentifier() {
        return deviceIdentifier;
    }

    /**
     * Sets the value of the deviceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setDeviceIdentifier(DeviceIdentifier value) {
        this.deviceIdentifier = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceStatusSettableType }
     *     
     */
    public DeviceStatusSettableType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceStatusSettableType }
     *     
     */
    public void setStatus(DeviceStatusSettableType value) {
        this.status = value;
    }

}
