
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for trustedRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="trustedRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="inputData" type="{urn:v1.webservices.operations.flexnet.com}activationDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "trustedRequestType", propOrder = {
    "inputData"
})
public class TrustedRequestType {

    @XmlElement(required = true)
    protected ActivationDataType inputData;

    /**
     * Gets the value of the inputData property.
     * 
     * @return
     *     possible object is
     *     {@link ActivationDataType }
     *     
     */
    public ActivationDataType getInputData() {
        return inputData;
    }

    /**
     * Sets the value of the inputData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivationDataType }
     *     
     */
    public void setInputData(ActivationDataType value) {
        this.inputData = value;
    }

}
