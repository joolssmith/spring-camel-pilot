
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for returnShortCodeDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="returnShortCodeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shortCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="webRegKey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="publisherAttributes" type="{urn:v1.webservices.operations.flexnet.com}publisherAttributesListDataType" minOccurs="0"/&gt;
 *         &lt;element name="overridePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "returnShortCodeDataType", propOrder = {
    "shortCode",
    "webRegKey",
    "publisherAttributes",
    "overridePolicy"
})
public class ReturnShortCodeDataType {

    @XmlElement(required = true)
    protected String shortCode;
    @XmlElement(required = true)
    protected String webRegKey;
    protected PublisherAttributesListDataType publisherAttributes;
    protected Boolean overridePolicy;

    /**
     * Gets the value of the shortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortCode() {
        return shortCode;
    }

    /**
     * Sets the value of the shortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortCode(String value) {
        this.shortCode = value;
    }

    /**
     * Gets the value of the webRegKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebRegKey() {
        return webRegKey;
    }

    /**
     * Sets the value of the webRegKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebRegKey(String value) {
        this.webRegKey = value;
    }

    /**
     * Gets the value of the publisherAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link PublisherAttributesListDataType }
     *     
     */
    public PublisherAttributesListDataType getPublisherAttributes() {
        return publisherAttributes;
    }

    /**
     * Sets the value of the publisherAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublisherAttributesListDataType }
     *     
     */
    public void setPublisherAttributes(PublisherAttributesListDataType value) {
        this.publisherAttributes = value;
    }

    /**
     * Gets the value of the overridePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverridePolicy() {
        return overridePolicy;
    }

    /**
     * Sets the value of the overridePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverridePolicy(Boolean value) {
        this.overridePolicy = value;
    }

}
