
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateFeatureBundleDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateFeatureBundleDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundleIdentifier" type="{urn:v2.webservices.operations.flexnet.com}featureBundleIdentifierType"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="features" type="{urn:v2.webservices.operations.flexnet.com}updateFeaturesListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateFeatureBundleDataType", propOrder = {
    "featureBundleIdentifier",
    "name",
    "description",
    "features"
})
public class UpdateFeatureBundleDataType {

    @XmlElement(required = true)
    protected FeatureBundleIdentifierType featureBundleIdentifier;
    protected String name;
    protected String description;
    protected UpdateFeaturesListType features;

    /**
     * Gets the value of the featureBundleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public FeatureBundleIdentifierType getFeatureBundleIdentifier() {
        return featureBundleIdentifier;
    }

    /**
     * Sets the value of the featureBundleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public void setFeatureBundleIdentifier(FeatureBundleIdentifierType value) {
        this.featureBundleIdentifier = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the features property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateFeaturesListType }
     *     
     */
    public UpdateFeaturesListType getFeatures() {
        return features;
    }

    /**
     * Sets the value of the features property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateFeaturesListType }
     *     
     */
    public void setFeatures(UpdateFeaturesListType value) {
        this.features = value;
    }

}
