
package com.flexnet.operations.webservices.a;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.a package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OwnerDoesNotExist_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "OwnerDoesNotExist");
    private final static QName _NotLicensed_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "NotLicensed");
    private final static QName _UnexpectedError_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "UnexpectedError");
    private final static QName _LineItemDoesNotExist_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "LineItemDoesNotExist");
    private final static QName _IneligibleForFulfillment_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "IneligibleForFulfillment");
    private final static QName _Overdraw_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "Overdraw");
    private final static QName _IncreaseBeyondEntitled_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "IncreaseBeyondEntitled");
    private final static QName _CompositeRightsIdException_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "CompositeRightsIdException");
    private final static QName _InsufficientCountForSplit_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "InsufficientCountForSplit");
    private final static QName _IneligibleForSplit_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "IneligibleForSplit");
    private final static QName _EnterpriseProblem_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "EnterpriseProblem");
    private final static QName _LicenseModelInvalid_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "LicenseModelInvalid");
    private final static QName _BaseProductDoesNotExist_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "BaseProductDoesNotExist");
    private final static QName _UnRedeemedWebKey_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "UnRedeemedWebKey");
    private final static QName _OrderableDoesNotExist_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "OrderableDoesNotExist");
    private final static QName _WebKeyDoesNotExist_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "WebKeyDoesNotExist");
    private final static QName _WebKeyRedemptionError_QNAME = new QName("http://producersuite.flexerasoftware.com/EntitlementService/", "WebKeyRedemptionError");
    private final static QName _OverdraftTypeAmount_QNAME = new QName("", "Amount");
    private final static QName _DurationBasedExpirationStartDate_QNAME = new QName("", "StartDate");
    private final static QName _FeatureCountTypeCount_QNAME = new QName("", "Count");
    private final static QName _RightsIdProblemMessage_QNAME = new QName("", "Message");
    private final static QName _RightsIdProblemRightsId_QNAME = new QName("", "RightsId");
    private final static QName _UserValidationInfoUserId_QNAME = new QName("", "UserId");
    private final static QName _ActivationIdDetailsActivationIdProblem_QNAME = new QName("", "ActivationIdProblem");
    private final static QName _ActivationIdDetailsOwner_QNAME = new QName("", "Owner");
    private final static QName _ActivationIdDetailsAvailableCount_QNAME = new QName("", "AvailableCount");
    private final static QName _ActivationIdDetailsEntitledCount_QNAME = new QName("", "EntitledCount");
    private final static QName _ActivationIdDetailsFeatures_QNAME = new QName("", "Features");
    private final static QName _ActivationIdDetailsState_QNAME = new QName("", "State");
    private final static QName _ActivationIdDetailsVirtualClientProhibited_QNAME = new QName("", "VirtualClientProhibited");
    private final static QName _ActivationIdDetailsSubstitutionVariablePattern_QNAME = new QName("", "SubstitutionVariablePattern");
    private final static QName _ActivationIdDetailsParentActivationList_QNAME = new QName("", "ParentActivationList");
    private final static QName _ActivationIdDetailsLicenseModel_QNAME = new QName("", "LicenseModel");
    private final static QName _WrappedFeaturesFeaturesWithStatus_QNAME = new QName("", "FeaturesWithStatus");
    private final static QName _WrappedFeaturesProblem_QNAME = new QName("", "Problem");
    private final static QName _FeatureVersion_QNAME = new QName("", "Version");
    private final static QName _FeatureVersionFormat_QNAME = new QName("", "VersionFormat");
    private final static QName _FeatureVersionDate_QNAME = new QName("", "VersionDate");
    private final static QName _FeatureVersionDateOption_QNAME = new QName("", "VersionDateOption");
    private final static QName _FeatureVersionDateDuration_QNAME = new QName("", "VersionDateDuration");
    private final static QName _FeatureVendorString_QNAME = new QName("", "VendorString");
    private final static QName _FeatureNotice_QNAME = new QName("", "Notice");
    private final static QName _FeatureIssuer_QNAME = new QName("", "Issuer");
    private final static QName _FeatureSerialNumber_QNAME = new QName("", "SerialNumber");
    private final static QName _FeatureMetered_QNAME = new QName("", "Metered");
    private final static QName _FeatureOverdraft_QNAME = new QName("", "Overdraft");
    private final static QName _FeatureReturnable_QNAME = new QName("", "Returnable");
    private final static QName _FeatureUndoInterval_QNAME = new QName("", "UndoInterval");
    private final static QName _FeatureBorrowInterval_QNAME = new QName("", "BorrowInterval");
    private final static QName _FeatureRenewInterval_QNAME = new QName("", "RenewInterval");
    private final static QName _FeatureGracePeriod_QNAME = new QName("", "GracePeriod");
    private final static QName _HostDataOwnerId_QNAME = new QName("", "OwnerId");
    private final static QName _HostDataHostName_QNAME = new QName("", "HostName");
    private final static QName _HostDataModelId_QNAME = new QName("", "ModelId");
    private final static QName _HostDataModelName_QNAME = new QName("", "ModelName");
    private final static QName _HostDataSeriesId_QNAME = new QName("", "SeriesId");
    private final static QName _HostDataSeriesName_QNAME = new QName("", "SeriesName");
    private final static QName _UnexpectedErrorIncidentID_QNAME = new QName("", "IncidentID");
    private final static QName _OwnerDataDisplayName_QNAME = new QName("", "DisplayName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.a
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetEntitlementOwner }
     * 
     */
    public GetEntitlementOwner createGetEntitlementOwner() {
        return new GetEntitlementOwner();
    }

    /**
     * Create an instance of {@link GetEntitlementOwnerResponse }
     * 
     */
    public GetEntitlementOwnerResponse createGetEntitlementOwnerResponse() {
        return new GetEntitlementOwnerResponse();
    }

    /**
     * Create an instance of {@link OwnerData }
     * 
     */
    public OwnerData createOwnerData() {
        return new OwnerData();
    }

    /**
     * Create an instance of {@link OwnerDoesNotExist }
     * 
     */
    public OwnerDoesNotExist createOwnerDoesNotExist() {
        return new OwnerDoesNotExist();
    }

    /**
     * Create an instance of {@link NotLicensed }
     * 
     */
    public NotLicensed createNotLicensed() {
        return new NotLicensed();
    }

    /**
     * Create an instance of {@link UnexpectedError }
     * 
     */
    public UnexpectedError createUnexpectedError() {
        return new UnexpectedError();
    }

    /**
     * Create an instance of {@link GetAvailableCount }
     * 
     */
    public GetAvailableCount createGetAvailableCount() {
        return new GetAvailableCount();
    }

    /**
     * Create an instance of {@link GetAvailableCountResponse }
     * 
     */
    public GetAvailableCountResponse createGetAvailableCountResponse() {
        return new GetAvailableCountResponse();
    }

    /**
     * Create an instance of {@link LineItemDoesNotExist }
     * 
     */
    public LineItemDoesNotExist createLineItemDoesNotExist() {
        return new LineItemDoesNotExist();
    }

    /**
     * Create an instance of {@link GetEntitledCount }
     * 
     */
    public GetEntitledCount createGetEntitledCount() {
        return new GetEntitledCount();
    }

    /**
     * Create an instance of {@link GetEntitledCountResponse }
     * 
     */
    public GetEntitledCountResponse createGetEntitledCountResponse() {
        return new GetEntitledCountResponse();
    }

    /**
     * Create an instance of {@link DecreaseAvailableCount }
     * 
     */
    public DecreaseAvailableCount createDecreaseAvailableCount() {
        return new DecreaseAvailableCount();
    }

    /**
     * Create an instance of {@link DecreaseAvailableCountResponse }
     * 
     */
    public DecreaseAvailableCountResponse createDecreaseAvailableCountResponse() {
        return new DecreaseAvailableCountResponse();
    }

    /**
     * Create an instance of {@link IneligibleForFulfillment }
     * 
     */
    public IneligibleForFulfillment createIneligibleForFulfillment() {
        return new IneligibleForFulfillment();
    }

    /**
     * Create an instance of {@link Overdraw }
     * 
     */
    public Overdraw createOverdraw() {
        return new Overdraw();
    }

    /**
     * Create an instance of {@link IncreaseAvailableCount }
     * 
     */
    public IncreaseAvailableCount createIncreaseAvailableCount() {
        return new IncreaseAvailableCount();
    }

    /**
     * Create an instance of {@link IncreaseAvailableCountResponse }
     * 
     */
    public IncreaseAvailableCountResponse createIncreaseAvailableCountResponse() {
        return new IncreaseAvailableCountResponse();
    }

    /**
     * Create an instance of {@link IncreaseBeyondEntitled }
     * 
     */
    public IncreaseBeyondEntitled createIncreaseBeyondEntitled() {
        return new IncreaseBeyondEntitled();
    }

    /**
     * Create an instance of {@link UpdateAvailableCount }
     * 
     */
    public UpdateAvailableCount createUpdateAvailableCount() {
        return new UpdateAvailableCount();
    }

    /**
     * Create an instance of {@link LicenseCountChange }
     * 
     */
    public LicenseCountChange createLicenseCountChange() {
        return new LicenseCountChange();
    }

    /**
     * Create an instance of {@link UpdateAvailableCountResponse }
     * 
     */
    public UpdateAvailableCountResponse createUpdateAvailableCountResponse() {
        return new UpdateAvailableCountResponse();
    }

    /**
     * Create an instance of {@link CompositeRightsIdException }
     * 
     */
    public CompositeRightsIdException createCompositeRightsIdException() {
        return new CompositeRightsIdException();
    }

    /**
     * Create an instance of {@link SplitLineItems }
     * 
     */
    public SplitLineItems createSplitLineItems() {
        return new SplitLineItems();
    }

    /**
     * Create an instance of {@link SplitLineItem }
     * 
     */
    public SplitLineItem createSplitLineItem() {
        return new SplitLineItem();
    }

    /**
     * Create an instance of {@link SplitLineItemsResponse }
     * 
     */
    public SplitLineItemsResponse createSplitLineItemsResponse() {
        return new SplitLineItemsResponse();
    }

    /**
     * Create an instance of {@link InsufficientCountForSplit }
     * 
     */
    public InsufficientCountForSplit createInsufficientCountForSplit() {
        return new InsufficientCountForSplit();
    }

    /**
     * Create an instance of {@link IneligibleForSplit }
     * 
     */
    public IneligibleForSplit createIneligibleForSplit() {
        return new IneligibleForSplit();
    }

    /**
     * Create an instance of {@link EnterpriseProblem }
     * 
     */
    public EnterpriseProblem createEnterpriseProblem() {
        return new EnterpriseProblem();
    }

    /**
     * Create an instance of {@link GetFlexNetFeaturesForRightsId }
     * 
     */
    public GetFlexNetFeaturesForRightsId createGetFlexNetFeaturesForRightsId() {
        return new GetFlexNetFeaturesForRightsId();
    }

    /**
     * Create an instance of {@link GetFlexNetFeaturesForRightsIdResponse }
     * 
     */
    public GetFlexNetFeaturesForRightsIdResponse createGetFlexNetFeaturesForRightsIdResponse() {
        return new GetFlexNetFeaturesForRightsIdResponse();
    }

    /**
     * Create an instance of {@link FeaturesWithStatus }
     * 
     */
    public FeaturesWithStatus createFeaturesWithStatus() {
        return new FeaturesWithStatus();
    }

    /**
     * Create an instance of {@link LicenseModelInvalid }
     * 
     */
    public LicenseModelInvalid createLicenseModelInvalid() {
        return new LicenseModelInvalid();
    }

    /**
     * Create an instance of {@link GetFlexNetFeaturesForBaseProduct }
     * 
     */
    public GetFlexNetFeaturesForBaseProduct createGetFlexNetFeaturesForBaseProduct() {
        return new GetFlexNetFeaturesForBaseProduct();
    }

    /**
     * Create an instance of {@link HostData }
     * 
     */
    public HostData createHostData() {
        return new HostData();
    }

    /**
     * Create an instance of {@link GetFlexNetFeaturesForBaseProductResponse }
     * 
     */
    public GetFlexNetFeaturesForBaseProductResponse createGetFlexNetFeaturesForBaseProductResponse() {
        return new GetFlexNetFeaturesForBaseProductResponse();
    }

    /**
     * Create an instance of {@link Feature }
     * 
     */
    public Feature createFeature() {
        return new Feature();
    }

    /**
     * Create an instance of {@link BaseProductDoesNotExist }
     * 
     */
    public BaseProductDoesNotExist createBaseProductDoesNotExist() {
        return new BaseProductDoesNotExist();
    }

    /**
     * Create an instance of {@link GetMultipleFlexNetFeatures }
     * 
     */
    public GetMultipleFlexNetFeatures createGetMultipleFlexNetFeatures() {
        return new GetMultipleFlexNetFeatures();
    }

    /**
     * Create an instance of {@link FeatureLimit }
     * 
     */
    public FeatureLimit createFeatureLimit() {
        return new FeatureLimit();
    }

    /**
     * Create an instance of {@link GetMultipleFlexNetFeaturesResponse }
     * 
     */
    public GetMultipleFlexNetFeaturesResponse createGetMultipleFlexNetFeaturesResponse() {
        return new GetMultipleFlexNetFeaturesResponse();
    }

    /**
     * Create an instance of {@link WrappedFeatures }
     * 
     */
    public WrappedFeatures createWrappedFeatures() {
        return new WrappedFeatures();
    }

    /**
     * Create an instance of {@link GetRightsIdOwner }
     * 
     */
    public GetRightsIdOwner createGetRightsIdOwner() {
        return new GetRightsIdOwner();
    }

    /**
     * Create an instance of {@link GetRightsIdOwnerResponse }
     * 
     */
    public GetRightsIdOwnerResponse createGetRightsIdOwnerResponse() {
        return new GetRightsIdOwnerResponse();
    }

    /**
     * Create an instance of {@link UnRedeemedWebKey }
     * 
     */
    public UnRedeemedWebKey createUnRedeemedWebKey() {
        return new UnRedeemedWebKey();
    }

    /**
     * Create an instance of {@link FindLineItemsForOwner }
     * 
     */
    public FindLineItemsForOwner createFindLineItemsForOwner() {
        return new FindLineItemsForOwner();
    }

    /**
     * Create an instance of {@link FindLineItemsForOwnerResponse }
     * 
     */
    public FindLineItemsForOwnerResponse createFindLineItemsForOwnerResponse() {
        return new FindLineItemsForOwnerResponse();
    }

    /**
     * Create an instance of {@link LineItemsAndProblems }
     * 
     */
    public LineItemsAndProblems createLineItemsAndProblems() {
        return new LineItemsAndProblems();
    }

    /**
     * Create an instance of {@link GetOrderableFeatureCounts }
     * 
     */
    public GetOrderableFeatureCounts createGetOrderableFeatureCounts() {
        return new GetOrderableFeatureCounts();
    }

    /**
     * Create an instance of {@link GetOrderableFeatureCountsResponse }
     * 
     */
    public GetOrderableFeatureCountsResponse createGetOrderableFeatureCountsResponse() {
        return new GetOrderableFeatureCountsResponse();
    }

    /**
     * Create an instance of {@link FeatureCount }
     * 
     */
    public FeatureCount createFeatureCount() {
        return new FeatureCount();
    }

    /**
     * Create an instance of {@link OrderableDoesNotExist }
     * 
     */
    public OrderableDoesNotExist createOrderableDoesNotExist() {
        return new OrderableDoesNotExist();
    }

    /**
     * Create an instance of {@link RedeemWebKey }
     * 
     */
    public RedeemWebKey createRedeemWebKey() {
        return new RedeemWebKey();
    }

    /**
     * Create an instance of {@link RedeemWebKeyResponse }
     * 
     */
    public RedeemWebKeyResponse createRedeemWebKeyResponse() {
        return new RedeemWebKeyResponse();
    }

    /**
     * Create an instance of {@link WebKeyDoesNotExist }
     * 
     */
    public WebKeyDoesNotExist createWebKeyDoesNotExist() {
        return new WebKeyDoesNotExist();
    }

    /**
     * Create an instance of {@link WebKeyRedemptionError }
     * 
     */
    public WebKeyRedemptionError createWebKeyRedemptionError() {
        return new WebKeyRedemptionError();
    }

    /**
     * Create an instance of {@link UpdateRightsOwner }
     * 
     */
    public UpdateRightsOwner createUpdateRightsOwner() {
        return new UpdateRightsOwner();
    }

    /**
     * Create an instance of {@link UpdateRightsOwnerResponse }
     * 
     */
    public UpdateRightsOwnerResponse createUpdateRightsOwnerResponse() {
        return new UpdateRightsOwnerResponse();
    }

    /**
     * Create an instance of {@link GetActivationIdDetails }
     * 
     */
    public GetActivationIdDetails createGetActivationIdDetails() {
        return new GetActivationIdDetails();
    }

    /**
     * Create an instance of {@link GetActivationIdDetailsResponse }
     * 
     */
    public GetActivationIdDetailsResponse createGetActivationIdDetailsResponse() {
        return new GetActivationIdDetailsResponse();
    }

    /**
     * Create an instance of {@link ActivationIdDetails }
     * 
     */
    public ActivationIdDetails createActivationIdDetails() {
        return new ActivationIdDetails();
    }

    /**
     * Create an instance of {@link SetLicensingStartDateForActivationId }
     * 
     */
    public SetLicensingStartDateForActivationId createSetLicensingStartDateForActivationId() {
        return new SetLicensingStartDateForActivationId();
    }

    /**
     * Create an instance of {@link SetLicensingStartDateForActivationIdResponse }
     * 
     */
    public SetLicensingStartDateForActivationIdResponse createSetLicensingStartDateForActivationIdResponse() {
        return new SetLicensingStartDateForActivationIdResponse();
    }

    /**
     * Create an instance of {@link GetOrganizationRelationship }
     * 
     */
    public GetOrganizationRelationship createGetOrganizationRelationship() {
        return new GetOrganizationRelationship();
    }

    /**
     * Create an instance of {@link GetOrganizationRelationshipResponse }
     * 
     */
    public GetOrganizationRelationshipResponse createGetOrganizationRelationshipResponse() {
        return new GetOrganizationRelationshipResponse();
    }

    /**
     * Create an instance of {@link ValidateUserId }
     * 
     */
    public ValidateUserId createValidateUserId() {
        return new ValidateUserId();
    }

    /**
     * Create an instance of {@link ValidateUserIdResponse }
     * 
     */
    public ValidateUserIdResponse createValidateUserIdResponse() {
        return new ValidateUserIdResponse();
    }

    /**
     * Create an instance of {@link UserValidationInfo }
     * 
     */
    public UserValidationInfo createUserValidationInfo() {
        return new UserValidationInfo();
    }

    /**
     * Create an instance of {@link RightsIdProblem }
     * 
     */
    public RightsIdProblem createRightsIdProblem() {
        return new RightsIdProblem();
    }

    /**
     * Create an instance of {@link OwnerMismatchIncreaseNotAllowed }
     * 
     */
    public OwnerMismatchIncreaseNotAllowed createOwnerMismatchIncreaseNotAllowed() {
        return new OwnerMismatchIncreaseNotAllowed();
    }

    /**
     * Create an instance of {@link FeatureListUnavailable }
     * 
     */
    public FeatureListUnavailable createFeatureListUnavailable() {
        return new FeatureListUnavailable();
    }

    /**
     * Create an instance of {@link LineItemQuantityChangeNotAllowed }
     * 
     */
    public LineItemQuantityChangeNotAllowed createLineItemQuantityChangeNotAllowed() {
        return new LineItemQuantityChangeNotAllowed();
    }

    /**
     * Create an instance of {@link UnexpectedRightsIdError }
     * 
     */
    public UnexpectedRightsIdError createUnexpectedRightsIdError() {
        return new UnexpectedRightsIdError();
    }

    /**
     * Create an instance of {@link LineItemQuantityIncreaseNotAllowed }
     * 
     */
    public LineItemQuantityIncreaseNotAllowed createLineItemQuantityIncreaseNotAllowed() {
        return new LineItemQuantityIncreaseNotAllowed();
    }

    /**
     * Create an instance of {@link OverFeatureLimits }
     * 
     */
    public OverFeatureLimits createOverFeatureLimits() {
        return new OverFeatureLimits();
    }

    /**
     * Create an instance of {@link OwnerMismatch }
     * 
     */
    public OwnerMismatch createOwnerMismatch() {
        return new OwnerMismatch();
    }

    /**
     * Create an instance of {@link Duration }
     * 
     */
    public Duration createDuration() {
        return new Duration();
    }

    /**
     * Create an instance of {@link FeatureCountType }
     * 
     */
    public FeatureCountType createFeatureCountType() {
        return new FeatureCountType();
    }

    /**
     * Create an instance of {@link PermanentExpiration }
     * 
     */
    public PermanentExpiration createPermanentExpiration() {
        return new PermanentExpiration();
    }

    /**
     * Create an instance of {@link FulfillmentStartDate }
     * 
     */
    public FulfillmentStartDate createFulfillmentStartDate() {
        return new FulfillmentStartDate();
    }

    /**
     * Create an instance of {@link FixedStartDate }
     * 
     */
    public FixedStartDate createFixedStartDate() {
        return new FixedStartDate();
    }

    /**
     * Create an instance of {@link FirstFulfillmentStartDate }
     * 
     */
    public FirstFulfillmentStartDate createFirstFulfillmentStartDate() {
        return new FirstFulfillmentStartDate();
    }

    /**
     * Create an instance of {@link DurationBasedExpiration }
     * 
     */
    public DurationBasedExpiration createDurationBasedExpiration() {
        return new DurationBasedExpiration();
    }

    /**
     * Create an instance of {@link FixedExpiration }
     * 
     */
    public FixedExpiration createFixedExpiration() {
        return new FixedExpiration();
    }

    /**
     * Create an instance of {@link OverdraftType }
     * 
     */
    public OverdraftType createOverdraftType() {
        return new OverdraftType();
    }

    /**
     * Create an instance of {@link ArrayOfFeature }
     * 
     */
    public ArrayOfFeature createArrayOfFeature() {
        return new ArrayOfFeature();
    }

    /**
     * Create an instance of {@link LineItem }
     * 
     */
    public LineItem createLineItem() {
        return new LineItem();
    }

    /**
     * Create an instance of {@link LineItemProblem }
     * 
     */
    public LineItemProblem createLineItemProblem() {
        return new LineItemProblem();
    }

    /**
     * Create an instance of {@link ParentActivationListType }
     * 
     */
    public ParentActivationListType createParentActivationListType() {
        return new ParentActivationListType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OwnerDoesNotExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "OwnerDoesNotExist")
    public JAXBElement<OwnerDoesNotExist> createOwnerDoesNotExist(OwnerDoesNotExist value) {
        return new JAXBElement<OwnerDoesNotExist>(_OwnerDoesNotExist_QNAME, OwnerDoesNotExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotLicensed }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "NotLicensed")
    public JAXBElement<NotLicensed> createNotLicensed(NotLicensed value) {
        return new JAXBElement<NotLicensed>(_NotLicensed_QNAME, NotLicensed.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnexpectedError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "UnexpectedError")
    public JAXBElement<UnexpectedError> createUnexpectedError(UnexpectedError value) {
        return new JAXBElement<UnexpectedError>(_UnexpectedError_QNAME, UnexpectedError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LineItemDoesNotExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "LineItemDoesNotExist")
    public JAXBElement<LineItemDoesNotExist> createLineItemDoesNotExist(LineItemDoesNotExist value) {
        return new JAXBElement<LineItemDoesNotExist>(_LineItemDoesNotExist_QNAME, LineItemDoesNotExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IneligibleForFulfillment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "IneligibleForFulfillment")
    public JAXBElement<IneligibleForFulfillment> createIneligibleForFulfillment(IneligibleForFulfillment value) {
        return new JAXBElement<IneligibleForFulfillment>(_IneligibleForFulfillment_QNAME, IneligibleForFulfillment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Overdraw }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "Overdraw")
    public JAXBElement<Overdraw> createOverdraw(Overdraw value) {
        return new JAXBElement<Overdraw>(_Overdraw_QNAME, Overdraw.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IncreaseBeyondEntitled }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "IncreaseBeyondEntitled")
    public JAXBElement<IncreaseBeyondEntitled> createIncreaseBeyondEntitled(IncreaseBeyondEntitled value) {
        return new JAXBElement<IncreaseBeyondEntitled>(_IncreaseBeyondEntitled_QNAME, IncreaseBeyondEntitled.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CompositeRightsIdException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "CompositeRightsIdException")
    public JAXBElement<CompositeRightsIdException> createCompositeRightsIdException(CompositeRightsIdException value) {
        return new JAXBElement<CompositeRightsIdException>(_CompositeRightsIdException_QNAME, CompositeRightsIdException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InsufficientCountForSplit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "InsufficientCountForSplit")
    public JAXBElement<InsufficientCountForSplit> createInsufficientCountForSplit(InsufficientCountForSplit value) {
        return new JAXBElement<InsufficientCountForSplit>(_InsufficientCountForSplit_QNAME, InsufficientCountForSplit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IneligibleForSplit }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "IneligibleForSplit")
    public JAXBElement<IneligibleForSplit> createIneligibleForSplit(IneligibleForSplit value) {
        return new JAXBElement<IneligibleForSplit>(_IneligibleForSplit_QNAME, IneligibleForSplit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnterpriseProblem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "EnterpriseProblem")
    public JAXBElement<EnterpriseProblem> createEnterpriseProblem(EnterpriseProblem value) {
        return new JAXBElement<EnterpriseProblem>(_EnterpriseProblem_QNAME, EnterpriseProblem.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LicenseModelInvalid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "LicenseModelInvalid")
    public JAXBElement<LicenseModelInvalid> createLicenseModelInvalid(LicenseModelInvalid value) {
        return new JAXBElement<LicenseModelInvalid>(_LicenseModelInvalid_QNAME, LicenseModelInvalid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseProductDoesNotExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "BaseProductDoesNotExist")
    public JAXBElement<BaseProductDoesNotExist> createBaseProductDoesNotExist(BaseProductDoesNotExist value) {
        return new JAXBElement<BaseProductDoesNotExist>(_BaseProductDoesNotExist_QNAME, BaseProductDoesNotExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnRedeemedWebKey }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "UnRedeemedWebKey")
    public JAXBElement<UnRedeemedWebKey> createUnRedeemedWebKey(UnRedeemedWebKey value) {
        return new JAXBElement<UnRedeemedWebKey>(_UnRedeemedWebKey_QNAME, UnRedeemedWebKey.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OrderableDoesNotExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "OrderableDoesNotExist")
    public JAXBElement<OrderableDoesNotExist> createOrderableDoesNotExist(OrderableDoesNotExist value) {
        return new JAXBElement<OrderableDoesNotExist>(_OrderableDoesNotExist_QNAME, OrderableDoesNotExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebKeyDoesNotExist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "WebKeyDoesNotExist")
    public JAXBElement<WebKeyDoesNotExist> createWebKeyDoesNotExist(WebKeyDoesNotExist value) {
        return new JAXBElement<WebKeyDoesNotExist>(_WebKeyDoesNotExist_QNAME, WebKeyDoesNotExist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WebKeyRedemptionError }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://producersuite.flexerasoftware.com/EntitlementService/", name = "WebKeyRedemptionError")
    public JAXBElement<WebKeyRedemptionError> createWebKeyRedemptionError(WebKeyRedemptionError value) {
        return new JAXBElement<WebKeyRedemptionError>(_WebKeyRedemptionError_QNAME, WebKeyRedemptionError.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Amount", scope = OverdraftType.class)
    public JAXBElement<BigDecimal> createOverdraftTypeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_OverdraftTypeAmount_QNAME, BigDecimal.class, OverdraftType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StartDate", scope = DurationBasedExpiration.class)
    public JAXBElement<StartDate> createDurationBasedExpirationStartDate(StartDate value) {
        return new JAXBElement<StartDate>(_DurationBasedExpirationStartDate_QNAME, StartDate.class, DurationBasedExpiration.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Count", scope = FeatureCountType.class)
    public JAXBElement<BigInteger> createFeatureCountTypeCount(BigInteger value) {
        return new JAXBElement<BigInteger>(_FeatureCountTypeCount_QNAME, BigInteger.class, FeatureCountType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Message", scope = RightsIdProblem.class)
    public JAXBElement<String> createRightsIdProblemMessage(String value) {
        return new JAXBElement<String>(_RightsIdProblemMessage_QNAME, String.class, RightsIdProblem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RightsId", scope = RightsIdProblem.class)
    public JAXBElement<String> createRightsIdProblemRightsId(String value) {
        return new JAXBElement<String>(_RightsIdProblemRightsId_QNAME, String.class, RightsIdProblem.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UserId", scope = UserValidationInfo.class)
    public JAXBElement<String> createUserValidationInfoUserId(String value) {
        return new JAXBElement<String>(_UserValidationInfoUserId_QNAME, String.class, UserValidationInfo.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RightsIdProblem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ActivationIdProblem", scope = ActivationIdDetails.class)
    public JAXBElement<RightsIdProblem> createActivationIdDetailsActivationIdProblem(RightsIdProblem value) {
        return new JAXBElement<RightsIdProblem>(_ActivationIdDetailsActivationIdProblem_QNAME, RightsIdProblem.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OwnerData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Owner", scope = ActivationIdDetails.class)
    public JAXBElement<OwnerData> createActivationIdDetailsOwner(OwnerData value) {
        return new JAXBElement<OwnerData>(_ActivationIdDetailsOwner_QNAME, OwnerData.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AvailableCount", scope = ActivationIdDetails.class)
    public JAXBElement<BigInteger> createActivationIdDetailsAvailableCount(BigInteger value) {
        return new JAXBElement<BigInteger>(_ActivationIdDetailsAvailableCount_QNAME, BigInteger.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "EntitledCount", scope = ActivationIdDetails.class)
    public JAXBElement<BigInteger> createActivationIdDetailsEntitledCount(BigInteger value) {
        return new JAXBElement<BigInteger>(_ActivationIdDetailsEntitledCount_QNAME, BigInteger.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Features", scope = ActivationIdDetails.class)
    public JAXBElement<ArrayOfFeature> createActivationIdDetailsFeatures(ArrayOfFeature value) {
        return new JAXBElement<ArrayOfFeature>(_ActivationIdDetailsFeatures_QNAME, ArrayOfFeature.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntityState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "State", scope = ActivationIdDetails.class)
    public JAXBElement<EntityState> createActivationIdDetailsState(EntityState value) {
        return new JAXBElement<EntityState>(_ActivationIdDetailsState_QNAME, EntityState.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VirtualClientProhibited", scope = ActivationIdDetails.class)
    public JAXBElement<Boolean> createActivationIdDetailsVirtualClientProhibited(Boolean value) {
        return new JAXBElement<Boolean>(_ActivationIdDetailsVirtualClientProhibited_QNAME, Boolean.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SubstitutionVariablePattern", scope = ActivationIdDetails.class)
    public JAXBElement<String> createActivationIdDetailsSubstitutionVariablePattern(String value) {
        return new JAXBElement<String>(_ActivationIdDetailsSubstitutionVariablePattern_QNAME, String.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParentActivationListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ParentActivationList", scope = ActivationIdDetails.class)
    public JAXBElement<ParentActivationListType> createActivationIdDetailsParentActivationList(ParentActivationListType value) {
        return new JAXBElement<ParentActivationListType>(_ActivationIdDetailsParentActivationList_QNAME, ParentActivationListType.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "LicenseModel", scope = ActivationIdDetails.class)
    public JAXBElement<String> createActivationIdDetailsLicenseModel(String value) {
        return new JAXBElement<String>(_ActivationIdDetailsLicenseModel_QNAME, String.class, ActivationIdDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "AvailableCount", scope = WrappedFeatures.class)
    public JAXBElement<BigInteger> createWrappedFeaturesAvailableCount(BigInteger value) {
        return new JAXBElement<BigInteger>(_ActivationIdDetailsAvailableCount_QNAME, BigInteger.class, WrappedFeatures.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FeaturesWithStatus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "FeaturesWithStatus", scope = WrappedFeatures.class)
    public JAXBElement<FeaturesWithStatus> createWrappedFeaturesFeaturesWithStatus(FeaturesWithStatus value) {
        return new JAXBElement<FeaturesWithStatus>(_WrappedFeaturesFeaturesWithStatus_QNAME, FeaturesWithStatus.class, WrappedFeatures.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RightsIdProblem }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Problem", scope = WrappedFeatures.class)
    public JAXBElement<RightsIdProblem> createWrappedFeaturesProblem(RightsIdProblem value) {
        return new JAXBElement<RightsIdProblem>(_WrappedFeaturesProblem_QNAME, RightsIdProblem.class, WrappedFeatures.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Count", scope = FeatureLimit.class)
    public JAXBElement<BigInteger> createFeatureLimitCount(BigInteger value) {
        return new JAXBElement<BigInteger>(_FeatureCountTypeCount_QNAME, BigInteger.class, FeatureLimit.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Version", scope = Feature.class)
    public JAXBElement<String> createFeatureVersion(String value) {
        return new JAXBElement<String>(_FeatureVersion_QNAME, String.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionFormatOpt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionFormat", scope = Feature.class)
    public JAXBElement<VersionFormatOpt> createFeatureVersionFormat(VersionFormatOpt value) {
        return new JAXBElement<VersionFormatOpt>(_FeatureVersionFormat_QNAME, VersionFormatOpt.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionDate", scope = Feature.class)
    public JAXBElement<XMLGregorianCalendar> createFeatureVersionDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FeatureVersionDate_QNAME, XMLGregorianCalendar.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VersionDateOpt }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionDateOption", scope = Feature.class)
    public JAXBElement<VersionDateOpt> createFeatureVersionDateOption(VersionDateOpt value) {
        return new JAXBElement<VersionDateOpt>(_FeatureVersionDateOption_QNAME, VersionDateOpt.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VersionDateDuration", scope = Feature.class)
    public JAXBElement<Duration> createFeatureVersionDateDuration(Duration value) {
        return new JAXBElement<Duration>(_FeatureVersionDateDuration_QNAME, Duration.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StartDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "StartDate", scope = Feature.class)
    public JAXBElement<StartDate> createFeatureStartDate(StartDate value) {
        return new JAXBElement<StartDate>(_DurationBasedExpirationStartDate_QNAME, StartDate.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VendorString", scope = Feature.class)
    public JAXBElement<String> createFeatureVendorString(String value) {
        return new JAXBElement<String>(_FeatureVendorString_QNAME, String.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Notice", scope = Feature.class)
    public JAXBElement<String> createFeatureNotice(String value) {
        return new JAXBElement<String>(_FeatureNotice_QNAME, String.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Issuer", scope = Feature.class)
    public JAXBElement<String> createFeatureIssuer(String value) {
        return new JAXBElement<String>(_FeatureIssuer_QNAME, String.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SerialNumber", scope = Feature.class)
    public JAXBElement<String> createFeatureSerialNumber(String value) {
        return new JAXBElement<String>(_FeatureSerialNumber_QNAME, String.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Metered", scope = Feature.class)
    public JAXBElement<Boolean> createFeatureMetered(Boolean value) {
        return new JAXBElement<Boolean>(_FeatureMetered_QNAME, Boolean.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OverdraftType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Overdraft", scope = Feature.class)
    public JAXBElement<OverdraftType> createFeatureOverdraft(OverdraftType value) {
        return new JAXBElement<OverdraftType>(_FeatureOverdraft_QNAME, OverdraftType.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Returnable", scope = Feature.class)
    public JAXBElement<Boolean> createFeatureReturnable(Boolean value) {
        return new JAXBElement<Boolean>(_FeatureReturnable_QNAME, Boolean.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "UndoInterval", scope = Feature.class)
    public JAXBElement<BigInteger> createFeatureUndoInterval(BigInteger value) {
        return new JAXBElement<BigInteger>(_FeatureUndoInterval_QNAME, BigInteger.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Duration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "BorrowInterval", scope = Feature.class)
    public JAXBElement<Duration> createFeatureBorrowInterval(Duration value) {
        return new JAXBElement<Duration>(_FeatureBorrowInterval_QNAME, Duration.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "RenewInterval", scope = Feature.class)
    public JAXBElement<BigInteger> createFeatureRenewInterval(BigInteger value) {
        return new JAXBElement<BigInteger>(_FeatureRenewInterval_QNAME, BigInteger.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "GracePeriod", scope = Feature.class)
    public JAXBElement<BigInteger> createFeatureGracePeriod(BigInteger value) {
        return new JAXBElement<BigInteger>(_FeatureGracePeriod_QNAME, BigInteger.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "VirtualClientProhibited", scope = Feature.class)
    public JAXBElement<Boolean> createFeatureVirtualClientProhibited(Boolean value) {
        return new JAXBElement<Boolean>(_ActivationIdDetailsVirtualClientProhibited_QNAME, Boolean.class, Feature.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "OwnerId", scope = HostData.class)
    public JAXBElement<String> createHostDataOwnerId(String value) {
        return new JAXBElement<String>(_HostDataOwnerId_QNAME, String.class, HostData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "HostName", scope = HostData.class)
    public JAXBElement<String> createHostDataHostName(String value) {
        return new JAXBElement<String>(_HostDataHostName_QNAME, String.class, HostData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ModelId", scope = HostData.class)
    public JAXBElement<String> createHostDataModelId(String value) {
        return new JAXBElement<String>(_HostDataModelId_QNAME, String.class, HostData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ModelName", scope = HostData.class)
    public JAXBElement<String> createHostDataModelName(String value) {
        return new JAXBElement<String>(_HostDataModelName_QNAME, String.class, HostData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SeriesId", scope = HostData.class)
    public JAXBElement<String> createHostDataSeriesId(String value) {
        return new JAXBElement<String>(_HostDataSeriesId_QNAME, String.class, HostData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "SeriesName", scope = HostData.class)
    public JAXBElement<String> createHostDataSeriesName(String value) {
        return new JAXBElement<String>(_HostDataSeriesName_QNAME, String.class, HostData.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfFeature }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Features", scope = FeaturesWithStatus.class)
    public JAXBElement<ArrayOfFeature> createFeaturesWithStatusFeatures(ArrayOfFeature value) {
        return new JAXBElement<ArrayOfFeature>(_ActivationIdDetailsFeatures_QNAME, ArrayOfFeature.class, FeaturesWithStatus.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IncidentID", scope = UnexpectedError.class)
    public JAXBElement<String> createUnexpectedErrorIncidentID(String value) {
        return new JAXBElement<String>(_UnexpectedErrorIncidentID_QNAME, String.class, UnexpectedError.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DisplayName", scope = OwnerData.class)
    public JAXBElement<String> createOwnerDataDisplayName(String value) {
        return new JAXBElement<String>(_OwnerDataDisplayName_QNAME, String.class, OwnerData.class, value);
    }

}
