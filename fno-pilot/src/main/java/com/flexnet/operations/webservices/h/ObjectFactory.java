
package com.flexnet.operations.webservices.h;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.h package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateBaseProductRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "createBaseProductRequest");
    private final static QName _CreateBaseProductResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "createBaseProductResponse");
    private final static QName _UpdateBaseProductRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "updateBaseProductRequest");
    private final static QName _UpdateBaseProductResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "updateBaseProductResponse");
    private final static QName _DeleteBaseProductRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteBaseProductRequest");
    private final static QName _DeleteBaseProductResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteBaseProductResponse");
    private final static QName _CreateDeviceRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "createDeviceRequest");
    private final static QName _CreateDeviceResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "createDeviceResponse");
    private final static QName _CreateTestDeviceRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "createTestDeviceRequest");
    private final static QName _CreateTestDeviceResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "createTestDeviceResponse");
    private final static QName _DeleteDeviceRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteDeviceRequest");
    private final static QName _DeleteDeviceResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteDeviceResponse");
    private final static QName _UpdateDeviceRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "updateDeviceRequest");
    private final static QName _UpdateDeviceResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "updateDeviceResponse");
    private final static QName _GeneratePrebuiltLicenseRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "generatePrebuiltLicenseRequest");
    private final static QName _GeneratePrebuiltLicenseResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "generatePrebuiltLicenseResponse");
    private final static QName _GetDevicesRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getDevicesRequest");
    private final static QName _GetDevicesResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getDevicesResponse");
    private final static QName _GetDeviceCountRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getDeviceCountRequest");
    private final static QName _GetDeviceCountResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getDeviceCountResponse");
    private final static QName _LinkAddonLineItemsRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "linkAddonLineItemsRequest");
    private final static QName _LinkAddonLineItemsResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "linkAddonLineItemsResponse");
    private final static QName _DeleteAddonLineItemsRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteAddonLineItemsRequest");
    private final static QName _DeleteAddonLineItemsResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteAddonLineItemsResponse");
    private final static QName _IncrementAddonLineItemsRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "incrementAddonLineItemsRequest");
    private final static QName _IncrementAddonLineItemsResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "incrementAddonLineItemsResponse");
    private final static QName _DecrementAddonLineItemsRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "decrementAddonLineItemsRequest");
    private final static QName _DecrementAddonLineItemsResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "decrementAddonLineItemsResponse");
    private final static QName _SetDeviceStatusRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "setDeviceStatusRequest");
    private final static QName _SetDeviceStatusResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "setDeviceStatusResponse");
    private final static QName _GenerateCapabilityResponseRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "generateCapabilityResponseRequest");
    private final static QName _GenerateCapabilityResponseResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "generateCapabilityResponseResponse");
    private final static QName _GetUsageSummaryRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getUsageSummaryRequest");
    private final static QName _GetUsageSummaryResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getUsageSummaryResponse");
    private final static QName _GetUsageSummaryTimesRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getUsageSummaryTimesRequest");
    private final static QName _GetUsageSummaryTimesResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getUsageSummaryTimesResponse");
    private final static QName _GetUsageHistoryRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getUsageHistoryRequest");
    private final static QName _GetUsageHistoryResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getUsageHistoryResponse");
    private final static QName _DeleteUsageHistoryRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteUsageHistoryRequest");
    private final static QName _DeleteUsageHistoryResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "deleteUsageHistoryResponse");
    private final static QName _MoveDeviceRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "moveDeviceRequest");
    private final static QName _MoveDeviceResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "moveDeviceResponse");
    private final static QName _ReturnHostRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "returnHostRequest");
    private final static QName _ReturnHostResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "returnHostResponse");
    private final static QName _ObsoleteHostRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "obsoleteHostRequest");
    private final static QName _ObsoleteHostResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "obsoleteHostResponse");
    private final static QName _GetDeletedSyncRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getDeletedSyncRequest");
    private final static QName _GetDeletedSyncResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getDeletedSyncResponse");
    private final static QName _RestoreServedClientRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "restoreServedClientRequest");
    private final static QName _RestoreServedClientResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "restoreServedClientResponse");
    private final static QName _GetAutoProvisionedServerRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getAutoProvisionedServerRequest");
    private final static QName _GetAutoProvisionedServerResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "getAutoProvisionedServerResponse");
    private final static QName _GenerateCloneDetectionReportRequest_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "generateCloneDetectionReportRequest");
    private final static QName _GenerateCloneDetectionReportResponse_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "generateCloneDetectionReportResponse");
    private final static QName _GenerateCloneDetectionReportRequestEnterpriseIds_QNAME = new QName("urn:com.macrovision:flexnet/opsembedded", "enterpriseIds");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.h
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CloneSuspect }
     * 
     */
    public CloneSuspect createCloneSuspect() {
        return new CloneSuspect();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportResponse }
     * 
     */
    public GenerateCloneDetectionReportResponse createGenerateCloneDetectionReportResponse() {
        return new GenerateCloneDetectionReportResponse();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportRequest }
     * 
     */
    public GenerateCloneDetectionReportRequest createGenerateCloneDetectionReportRequest() {
        return new GenerateCloneDetectionReportRequest();
    }

    /**
     * Create an instance of {@link BaseProductRequestType }
     * 
     */
    public BaseProductRequestType createBaseProductRequestType() {
        return new BaseProductRequestType();
    }

    /**
     * Create an instance of {@link BaseProductResponseType }
     * 
     */
    public BaseProductResponseType createBaseProductResponseType() {
        return new BaseProductResponseType();
    }

    /**
     * Create an instance of {@link DeleteBaseProductRequestType }
     * 
     */
    public DeleteBaseProductRequestType createDeleteBaseProductRequestType() {
        return new DeleteBaseProductRequestType();
    }

    /**
     * Create an instance of {@link CreateDevRequestType }
     * 
     */
    public CreateDevRequestType createCreateDevRequestType() {
        return new CreateDevRequestType();
    }

    /**
     * Create an instance of {@link CreateDevResponseType }
     * 
     */
    public CreateDevResponseType createCreateDevResponseType() {
        return new CreateDevResponseType();
    }

    /**
     * Create an instance of {@link DeleteDeviceRequestType }
     * 
     */
    public DeleteDeviceRequestType createDeleteDeviceRequestType() {
        return new DeleteDeviceRequestType();
    }

    /**
     * Create an instance of {@link DeleteDeviceResponseType }
     * 
     */
    public DeleteDeviceResponseType createDeleteDeviceResponseType() {
        return new DeleteDeviceResponseType();
    }

    /**
     * Create an instance of {@link UpdateDevRequestType }
     * 
     */
    public UpdateDevRequestType createUpdateDevRequestType() {
        return new UpdateDevRequestType();
    }

    /**
     * Create an instance of {@link UpdateDevResponseType }
     * 
     */
    public UpdateDevResponseType createUpdateDevResponseType() {
        return new UpdateDevResponseType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseRequestType }
     * 
     */
    public GeneratePrebuiltLicenseRequestType createGeneratePrebuiltLicenseRequestType() {
        return new GeneratePrebuiltLicenseRequestType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseResponseType }
     * 
     */
    public GeneratePrebuiltLicenseResponseType createGeneratePrebuiltLicenseResponseType() {
        return new GeneratePrebuiltLicenseResponseType();
    }

    /**
     * Create an instance of {@link GetDevicesRequestType }
     * 
     */
    public GetDevicesRequestType createGetDevicesRequestType() {
        return new GetDevicesRequestType();
    }

    /**
     * Create an instance of {@link GetDevicesResponseType }
     * 
     */
    public GetDevicesResponseType createGetDevicesResponseType() {
        return new GetDevicesResponseType();
    }

    /**
     * Create an instance of {@link GetDevicesCountRequestType }
     * 
     */
    public GetDevicesCountRequestType createGetDevicesCountRequestType() {
        return new GetDevicesCountRequestType();
    }

    /**
     * Create an instance of {@link GetDeviceCountResponseType }
     * 
     */
    public GetDeviceCountResponseType createGetDeviceCountResponseType() {
        return new GetDeviceCountResponseType();
    }

    /**
     * Create an instance of {@link LinkAddonLineItemsRequestType }
     * 
     */
    public LinkAddonLineItemsRequestType createLinkAddonLineItemsRequestType() {
        return new LinkAddonLineItemsRequestType();
    }

    /**
     * Create an instance of {@link LinkAddonLineItemsResponseType }
     * 
     */
    public LinkAddonLineItemsResponseType createLinkAddonLineItemsResponseType() {
        return new LinkAddonLineItemsResponseType();
    }

    /**
     * Create an instance of {@link DeleteAddonLineItemsRequestType }
     * 
     */
    public DeleteAddonLineItemsRequestType createDeleteAddonLineItemsRequestType() {
        return new DeleteAddonLineItemsRequestType();
    }

    /**
     * Create an instance of {@link DeleteAddonLineItemsResponseType }
     * 
     */
    public DeleteAddonLineItemsResponseType createDeleteAddonLineItemsResponseType() {
        return new DeleteAddonLineItemsResponseType();
    }

    /**
     * Create an instance of {@link SetDeviceStatusRequestListType }
     * 
     */
    public SetDeviceStatusRequestListType createSetDeviceStatusRequestListType() {
        return new SetDeviceStatusRequestListType();
    }

    /**
     * Create an instance of {@link SetDeviceStatusResponseType }
     * 
     */
    public SetDeviceStatusResponseType createSetDeviceStatusResponseType() {
        return new SetDeviceStatusResponseType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseRequestType }
     * 
     */
    public GenerateCapabilityResponseRequestType createGenerateCapabilityResponseRequestType() {
        return new GenerateCapabilityResponseRequestType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseResponseType }
     * 
     */
    public GenerateCapabilityResponseResponseType createGenerateCapabilityResponseResponseType() {
        return new GenerateCapabilityResponseResponseType();
    }

    /**
     * Create an instance of {@link GetUsageSummaryRequestType }
     * 
     */
    public GetUsageSummaryRequestType createGetUsageSummaryRequestType() {
        return new GetUsageSummaryRequestType();
    }

    /**
     * Create an instance of {@link GetUsageSummaryResponseType }
     * 
     */
    public GetUsageSummaryResponseType createGetUsageSummaryResponseType() {
        return new GetUsageSummaryResponseType();
    }

    /**
     * Create an instance of {@link GetUsageSummaryTimesRequestType }
     * 
     */
    public GetUsageSummaryTimesRequestType createGetUsageSummaryTimesRequestType() {
        return new GetUsageSummaryTimesRequestType();
    }

    /**
     * Create an instance of {@link GetUsageSummaryTimesResponseType }
     * 
     */
    public GetUsageSummaryTimesResponseType createGetUsageSummaryTimesResponseType() {
        return new GetUsageSummaryTimesResponseType();
    }

    /**
     * Create an instance of {@link GetUsageHistoryRequestType }
     * 
     */
    public GetUsageHistoryRequestType createGetUsageHistoryRequestType() {
        return new GetUsageHistoryRequestType();
    }

    /**
     * Create an instance of {@link GetUsageHistoryResponseType }
     * 
     */
    public GetUsageHistoryResponseType createGetUsageHistoryResponseType() {
        return new GetUsageHistoryResponseType();
    }

    /**
     * Create an instance of {@link DeleteUsageHistoryRequestType }
     * 
     */
    public DeleteUsageHistoryRequestType createDeleteUsageHistoryRequestType() {
        return new DeleteUsageHistoryRequestType();
    }

    /**
     * Create an instance of {@link DeleteUsageHistoryResponseType }
     * 
     */
    public DeleteUsageHistoryResponseType createDeleteUsageHistoryResponseType() {
        return new DeleteUsageHistoryResponseType();
    }

    /**
     * Create an instance of {@link MoveDeviceRequestType }
     * 
     */
    public MoveDeviceRequestType createMoveDeviceRequestType() {
        return new MoveDeviceRequestType();
    }

    /**
     * Create an instance of {@link MoveDeviceResponseType }
     * 
     */
    public MoveDeviceResponseType createMoveDeviceResponseType() {
        return new MoveDeviceResponseType();
    }

    /**
     * Create an instance of {@link ReturnHostRequestListType }
     * 
     */
    public ReturnHostRequestListType createReturnHostRequestListType() {
        return new ReturnHostRequestListType();
    }

    /**
     * Create an instance of {@link ReturnHostResponseType }
     * 
     */
    public ReturnHostResponseType createReturnHostResponseType() {
        return new ReturnHostResponseType();
    }

    /**
     * Create an instance of {@link ObsoleteHostRequestListType }
     * 
     */
    public ObsoleteHostRequestListType createObsoleteHostRequestListType() {
        return new ObsoleteHostRequestListType();
    }

    /**
     * Create an instance of {@link ObsoleteHostResponseType }
     * 
     */
    public ObsoleteHostResponseType createObsoleteHostResponseType() {
        return new ObsoleteHostResponseType();
    }

    /**
     * Create an instance of {@link GetDeletedSyncRequestType }
     * 
     */
    public GetDeletedSyncRequestType createGetDeletedSyncRequestType() {
        return new GetDeletedSyncRequestType();
    }

    /**
     * Create an instance of {@link GetDeletedSyncResponseType }
     * 
     */
    public GetDeletedSyncResponseType createGetDeletedSyncResponseType() {
        return new GetDeletedSyncResponseType();
    }

    /**
     * Create an instance of {@link RestoreServedClientRequestType }
     * 
     */
    public RestoreServedClientRequestType createRestoreServedClientRequestType() {
        return new RestoreServedClientRequestType();
    }

    /**
     * Create an instance of {@link RestoreServedClientResponseType }
     * 
     */
    public RestoreServedClientResponseType createRestoreServedClientResponseType() {
        return new RestoreServedClientResponseType();
    }

    /**
     * Create an instance of {@link GetAutoProvisionedServerRequest }
     * 
     */
    public GetAutoProvisionedServerRequest createGetAutoProvisionedServerRequest() {
        return new GetAutoProvisionedServerRequest();
    }

    /**
     * Create an instance of {@link GetAutoProvisionedServerResponse }
     * 
     */
    public GetAutoProvisionedServerResponse createGetAutoProvisionedServerResponse() {
        return new GetAutoProvisionedServerResponse();
    }

    /**
     * Create an instance of {@link ProductPKType }
     * 
     */
    public ProductPKType createProductPKType() {
        return new ProductPKType();
    }

    /**
     * Create an instance of {@link ProductIdentifierType }
     * 
     */
    public ProductIdentifierType createProductIdentifierType() {
        return new ProductIdentifierType();
    }

    /**
     * Create an instance of {@link LicenseModelPKType }
     * 
     */
    public LicenseModelPKType createLicenseModelPKType() {
        return new LicenseModelPKType();
    }

    /**
     * Create an instance of {@link LicenseModelIdentifierType }
     * 
     */
    public LicenseModelIdentifierType createLicenseModelIdentifierType() {
        return new LicenseModelIdentifierType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorType }
     * 
     */
    public AttributeDescriptorType createAttributeDescriptorType() {
        return new AttributeDescriptorType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorDataType }
     * 
     */
    public AttributeDescriptorDataType createAttributeDescriptorDataType() {
        return new AttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link DurationType }
     * 
     */
    public DurationType createDurationType() {
        return new DurationType();
    }

    /**
     * Create an instance of {@link BaseProductDataType }
     * 
     */
    public BaseProductDataType createBaseProductDataType() {
        return new BaseProductDataType();
    }

    /**
     * Create an instance of {@link OpsEmbeddedStatusInfoType }
     * 
     */
    public OpsEmbeddedStatusInfoType createOpsEmbeddedStatusInfoType() {
        return new OpsEmbeddedStatusInfoType();
    }

    /**
     * Create an instance of {@link FailedBaseProductDataType }
     * 
     */
    public FailedBaseProductDataType createFailedBaseProductDataType() {
        return new FailedBaseProductDataType();
    }

    /**
     * Create an instance of {@link FailedBaseProductDataListType }
     * 
     */
    public FailedBaseProductDataListType createFailedBaseProductDataListType() {
        return new FailedBaseProductDataListType();
    }

    /**
     * Create an instance of {@link BaseProductDataListType }
     * 
     */
    public BaseProductDataListType createBaseProductDataListType() {
        return new BaseProductDataListType();
    }

    /**
     * Create an instance of {@link ServerIdsType }
     * 
     */
    public ServerIdsType createServerIdsType() {
        return new ServerIdsType();
    }

    /**
     * Create an instance of {@link CreateDeviceIdentifier }
     * 
     */
    public CreateDeviceIdentifier createCreateDeviceIdentifier() {
        return new CreateDeviceIdentifier();
    }

    /**
     * Create an instance of {@link HostTypeIdentifier }
     * 
     */
    public HostTypeIdentifier createHostTypeIdentifier() {
        return new HostTypeIdentifier();
    }

    /**
     * Create an instance of {@link PublisherIdentifier }
     * 
     */
    public PublisherIdentifier createPublisherIdentifier() {
        return new PublisherIdentifier();
    }

    /**
     * Create an instance of {@link OrganizationPKType }
     * 
     */
    public OrganizationPKType createOrganizationPKType() {
        return new OrganizationPKType();
    }

    /**
     * Create an instance of {@link OrganizationIdentifierType }
     * 
     */
    public OrganizationIdentifierType createOrganizationIdentifierType() {
        return new OrganizationIdentifierType();
    }

    /**
     * Create an instance of {@link UserPKType }
     * 
     */
    public UserPKType createUserPKType() {
        return new UserPKType();
    }

    /**
     * Create an instance of {@link UserIdentifierType }
     * 
     */
    public UserIdentifierType createUserIdentifierType() {
        return new UserIdentifierType();
    }

    /**
     * Create an instance of {@link ChannelPartnerDataType }
     * 
     */
    public ChannelPartnerDataType createChannelPartnerDataType() {
        return new ChannelPartnerDataType();
    }

    /**
     * Create an instance of {@link ChannelPartnerDataListType }
     * 
     */
    public ChannelPartnerDataListType createChannelPartnerDataListType() {
        return new ChannelPartnerDataListType();
    }

    /**
     * Create an instance of {@link DeviceDataType }
     * 
     */
    public DeviceDataType createDeviceDataType() {
        return new DeviceDataType();
    }

    /**
     * Create an instance of {@link FailedCreateDeviceDataType }
     * 
     */
    public FailedCreateDeviceDataType createFailedCreateDeviceDataType() {
        return new FailedCreateDeviceDataType();
    }

    /**
     * Create an instance of {@link FailedCreateDevDataListType }
     * 
     */
    public FailedCreateDevDataListType createFailedCreateDevDataListType() {
        return new FailedCreateDevDataListType();
    }

    /**
     * Create an instance of {@link DeviceIdentifier }
     * 
     */
    public DeviceIdentifier createDeviceIdentifier() {
        return new DeviceIdentifier();
    }

    /**
     * Create an instance of {@link CreatedDeviceDataListType }
     * 
     */
    public CreatedDeviceDataListType createCreatedDeviceDataListType() {
        return new CreatedDeviceDataListType();
    }

    /**
     * Create an instance of {@link FailedDeleteDevDataType }
     * 
     */
    public FailedDeleteDevDataType createFailedDeleteDevDataType() {
        return new FailedDeleteDevDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteDevDataListType }
     * 
     */
    public FailedDeleteDevDataListType createFailedDeleteDevDataListType() {
        return new FailedDeleteDevDataListType();
    }

    /**
     * Create an instance of {@link UpdateChannelPartnerDataListType }
     * 
     */
    public UpdateChannelPartnerDataListType createUpdateChannelPartnerDataListType() {
        return new UpdateChannelPartnerDataListType();
    }

    /**
     * Create an instance of {@link UpdateDevDataType }
     * 
     */
    public UpdateDevDataType createUpdateDevDataType() {
        return new UpdateDevDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateDeviceDataType }
     * 
     */
    public FailedUpdateDeviceDataType createFailedUpdateDeviceDataType() {
        return new FailedUpdateDeviceDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateDevDataListType }
     * 
     */
    public FailedUpdateDevDataListType createFailedUpdateDevDataListType() {
        return new FailedUpdateDevDataListType();
    }

    /**
     * Create an instance of {@link UpdatedDeviceDataListType }
     * 
     */
    public UpdatedDeviceDataListType createUpdatedDeviceDataListType() {
        return new UpdatedDeviceDataListType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseDataType }
     * 
     */
    public GeneratePrebuiltLicenseDataType createGeneratePrebuiltLicenseDataType() {
        return new GeneratePrebuiltLicenseDataType();
    }

    /**
     * Create an instance of {@link FailedGeneratePrebuiltLicenseDataType }
     * 
     */
    public FailedGeneratePrebuiltLicenseDataType createFailedGeneratePrebuiltLicenseDataType() {
        return new FailedGeneratePrebuiltLicenseDataType();
    }

    /**
     * Create an instance of {@link FailedGeneratePrebuiltLicenseDataListType }
     * 
     */
    public FailedGeneratePrebuiltLicenseDataListType createFailedGeneratePrebuiltLicenseDataListType() {
        return new FailedGeneratePrebuiltLicenseDataListType();
    }

    /**
     * Create an instance of {@link GeneratePrebuiltLicenseDataListType }
     * 
     */
    public GeneratePrebuiltLicenseDataListType createGeneratePrebuiltLicenseDataListType() {
        return new GeneratePrebuiltLicenseDataListType();
    }

    /**
     * Create an instance of {@link SimpleQueryType }
     * 
     */
    public SimpleQueryType createSimpleQueryType() {
        return new SimpleQueryType();
    }

    /**
     * Create an instance of {@link DeviceIdTypeQueryType }
     * 
     */
    public DeviceIdTypeQueryType createDeviceIdTypeQueryType() {
        return new DeviceIdTypeQueryType();
    }

    /**
     * Create an instance of {@link PartnerTierQueryType }
     * 
     */
    public PartnerTierQueryType createPartnerTierQueryType() {
        return new PartnerTierQueryType();
    }

    /**
     * Create an instance of {@link DeviceStateQueryType }
     * 
     */
    public DeviceStateQueryType createDeviceStateQueryType() {
        return new DeviceStateQueryType();
    }

    /**
     * Create an instance of {@link DeviceServedStateQueryType }
     * 
     */
    public DeviceServedStateQueryType createDeviceServedStateQueryType() {
        return new DeviceServedStateQueryType();
    }

    /**
     * Create an instance of {@link NumberQueryType }
     * 
     */
    public NumberQueryType createNumberQueryType() {
        return new NumberQueryType();
    }

    /**
     * Create an instance of {@link DeviceTypeList }
     * 
     */
    public DeviceTypeList createDeviceTypeList() {
        return new DeviceTypeList();
    }

    /**
     * Create an instance of {@link DateQueryType }
     * 
     */
    public DateQueryType createDateQueryType() {
        return new DateQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributeQueryType }
     * 
     */
    public CustomAttributeQueryType createCustomAttributeQueryType() {
        return new CustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributesQueryListType }
     * 
     */
    public CustomAttributesQueryListType createCustomAttributesQueryListType() {
        return new CustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link DeviceMachineTypeQueryType }
     * 
     */
    public DeviceMachineTypeQueryType createDeviceMachineTypeQueryType() {
        return new DeviceMachineTypeQueryType();
    }

    /**
     * Create an instance of {@link GetDevicesParametersType }
     * 
     */
    public GetDevicesParametersType createGetDevicesParametersType() {
        return new GetDevicesParametersType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorType }
     * 
     */
    public CustomAttributeDescriptorType createCustomAttributeDescriptorType() {
        return new CustomAttributeDescriptorType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorDataType }
     * 
     */
    public CustomAttributeDescriptorDataType createCustomAttributeDescriptorDataType() {
        return new CustomAttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link DeviceResponseConfigRequestType }
     * 
     */
    public DeviceResponseConfigRequestType createDeviceResponseConfigRequestType() {
        return new DeviceResponseConfigRequestType();
    }

    /**
     * Create an instance of {@link FailedGetDevicesDataType }
     * 
     */
    public FailedGetDevicesDataType createFailedGetDevicesDataType() {
        return new FailedGetDevicesDataType();
    }

    /**
     * Create an instance of {@link SoldToType }
     * 
     */
    public SoldToType createSoldToType() {
        return new SoldToType();
    }

    /**
     * Create an instance of {@link EntitledProductDataType }
     * 
     */
    public EntitledProductDataType createEntitledProductDataType() {
        return new EntitledProductDataType();
    }

    /**
     * Create an instance of {@link EntitledProductDataListType }
     * 
     */
    public EntitledProductDataListType createEntitledProductDataListType() {
        return new EntitledProductDataListType();
    }

    /**
     * Create an instance of {@link AddonLineItemDataDataType }
     * 
     */
    public AddonLineItemDataDataType createAddonLineItemDataDataType() {
        return new AddonLineItemDataDataType();
    }

    /**
     * Create an instance of {@link FeatureDataDataType }
     * 
     */
    public FeatureDataDataType createFeatureDataDataType() {
        return new FeatureDataDataType();
    }

    /**
     * Create an instance of {@link DictionaryEntry }
     * 
     */
    public DictionaryEntry createDictionaryEntry() {
        return new DictionaryEntry();
    }

    /**
     * Create an instance of {@link DictionaryType }
     * 
     */
    public DictionaryType createDictionaryType() {
        return new DictionaryType();
    }

    /**
     * Create an instance of {@link DeviceQueryDataType }
     * 
     */
    public DeviceQueryDataType createDeviceQueryDataType() {
        return new DeviceQueryDataType();
    }

    /**
     * Create an instance of {@link GetDevicesResponseDataType }
     * 
     */
    public GetDevicesResponseDataType createGetDevicesResponseDataType() {
        return new GetDevicesResponseDataType();
    }

    /**
     * Create an instance of {@link GetDeviceCountResponseDataType }
     * 
     */
    public GetDeviceCountResponseDataType createGetDeviceCountResponseDataType() {
        return new GetDeviceCountResponseDataType();
    }

    /**
     * Create an instance of {@link LinkLineItemIdentifier }
     * 
     */
    public LinkLineItemIdentifier createLinkLineItemIdentifier() {
        return new LinkLineItemIdentifier();
    }

    /**
     * Create an instance of {@link LinkLineItemDataType }
     * 
     */
    public LinkLineItemDataType createLinkLineItemDataType() {
        return new LinkLineItemDataType();
    }

    /**
     * Create an instance of {@link LinkAddonLineItemDataType }
     * 
     */
    public LinkAddonLineItemDataType createLinkAddonLineItemDataType() {
        return new LinkAddonLineItemDataType();
    }

    /**
     * Create an instance of {@link LinkFailAddonDataType }
     * 
     */
    public LinkFailAddonDataType createLinkFailAddonDataType() {
        return new LinkFailAddonDataType();
    }

    /**
     * Create an instance of {@link LinkFailAddonDataListType }
     * 
     */
    public LinkFailAddonDataListType createLinkFailAddonDataListType() {
        return new LinkFailAddonDataListType();
    }

    /**
     * Create an instance of {@link SuccessAddonDataListType }
     * 
     */
    public SuccessAddonDataListType createSuccessAddonDataListType() {
        return new SuccessAddonDataListType();
    }

    /**
     * Create an instance of {@link DeleteLineItemIdentifier }
     * 
     */
    public DeleteLineItemIdentifier createDeleteLineItemIdentifier() {
        return new DeleteLineItemIdentifier();
    }

    /**
     * Create an instance of {@link DeleteLineItemDataType }
     * 
     */
    public DeleteLineItemDataType createDeleteLineItemDataType() {
        return new DeleteLineItemDataType();
    }

    /**
     * Create an instance of {@link DeleteAddonLineItemDataType }
     * 
     */
    public DeleteAddonLineItemDataType createDeleteAddonLineItemDataType() {
        return new DeleteAddonLineItemDataType();
    }

    /**
     * Create an instance of {@link DeleteFailAddonDataType }
     * 
     */
    public DeleteFailAddonDataType createDeleteFailAddonDataType() {
        return new DeleteFailAddonDataType();
    }

    /**
     * Create an instance of {@link DeleteFailAddonDataListType }
     * 
     */
    public DeleteFailAddonDataListType createDeleteFailAddonDataListType() {
        return new DeleteFailAddonDataListType();
    }

    /**
     * Create an instance of {@link SetDeviceStatusType }
     * 
     */
    public SetDeviceStatusType createSetDeviceStatusType() {
        return new SetDeviceStatusType();
    }

    /**
     * Create an instance of {@link FailedSetDeviceStatusDataType }
     * 
     */
    public FailedSetDeviceStatusDataType createFailedSetDeviceStatusDataType() {
        return new FailedSetDeviceStatusDataType();
    }

    /**
     * Create an instance of {@link FailedSetDeviceStatusDataListType }
     * 
     */
    public FailedSetDeviceStatusDataListType createFailedSetDeviceStatusDataListType() {
        return new FailedSetDeviceStatusDataListType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseDictionaryEntry }
     * 
     */
    public GenerateCapabilityResponseDictionaryEntry createGenerateCapabilityResponseDictionaryEntry() {
        return new GenerateCapabilityResponseDictionaryEntry();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseDictionary }
     * 
     */
    public GenerateCapabilityResponseDictionary createGenerateCapabilityResponseDictionary() {
        return new GenerateCapabilityResponseDictionary();
    }

    /**
     * Create an instance of {@link CapabilityRequestType }
     * 
     */
    public CapabilityRequestType createCapabilityRequestType() {
        return new CapabilityRequestType();
    }

    /**
     * Create an instance of {@link FailedCapabilityResponseDataType }
     * 
     */
    public FailedCapabilityResponseDataType createFailedCapabilityResponseDataType() {
        return new FailedCapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link FailedGenerateCapabilityResponseDataType }
     * 
     */
    public FailedGenerateCapabilityResponseDataType createFailedGenerateCapabilityResponseDataType() {
        return new FailedGenerateCapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link CapabilityResponseDataType }
     * 
     */
    public CapabilityResponseDataType createCapabilityResponseDataType() {
        return new CapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link GenerateCapabilityResponseDataType }
     * 
     */
    public GenerateCapabilityResponseDataType createGenerateCapabilityResponseDataType() {
        return new GenerateCapabilityResponseDataType();
    }

    /**
     * Create an instance of {@link ExternalIdQueryType }
     * 
     */
    public ExternalIdQueryType createExternalIdQueryType() {
        return new ExternalIdQueryType();
    }

    /**
     * Create an instance of {@link DateTimeQueryType }
     * 
     */
    public DateTimeQueryType createDateTimeQueryType() {
        return new DateTimeQueryType();
    }

    /**
     * Create an instance of {@link GetUsageSummaryParametersType }
     * 
     */
    public GetUsageSummaryParametersType createGetUsageSummaryParametersType() {
        return new GetUsageSummaryParametersType();
    }

    /**
     * Create an instance of {@link FailedGetUsageSummaryDataType }
     * 
     */
    public FailedGetUsageSummaryDataType createFailedGetUsageSummaryDataType() {
        return new FailedGetUsageSummaryDataType();
    }

    /**
     * Create an instance of {@link UsageSummaryDataType }
     * 
     */
    public UsageSummaryDataType createUsageSummaryDataType() {
        return new UsageSummaryDataType();
    }

    /**
     * Create an instance of {@link GetUsageSummaryDataType }
     * 
     */
    public GetUsageSummaryDataType createGetUsageSummaryDataType() {
        return new GetUsageSummaryDataType();
    }

    /**
     * Create an instance of {@link FailedGetUsageSummaryTimesDataType }
     * 
     */
    public FailedGetUsageSummaryTimesDataType createFailedGetUsageSummaryTimesDataType() {
        return new FailedGetUsageSummaryTimesDataType();
    }

    /**
     * Create an instance of {@link UsageSummaryTimesDataType }
     * 
     */
    public UsageSummaryTimesDataType createUsageSummaryTimesDataType() {
        return new UsageSummaryTimesDataType();
    }

    /**
     * Create an instance of {@link GetUsageSummaryTimesDataType }
     * 
     */
    public GetUsageSummaryTimesDataType createGetUsageSummaryTimesDataType() {
        return new GetUsageSummaryTimesDataType();
    }

    /**
     * Create an instance of {@link GetUsageHistoryParametersType }
     * 
     */
    public GetUsageHistoryParametersType createGetUsageHistoryParametersType() {
        return new GetUsageHistoryParametersType();
    }

    /**
     * Create an instance of {@link GetUsageHistoryConfigType }
     * 
     */
    public GetUsageHistoryConfigType createGetUsageHistoryConfigType() {
        return new GetUsageHistoryConfigType();
    }

    /**
     * Create an instance of {@link FailedGetUsageHistoryDataType }
     * 
     */
    public FailedGetUsageHistoryDataType createFailedGetUsageHistoryDataType() {
        return new FailedGetUsageHistoryDataType();
    }

    /**
     * Create an instance of {@link UsageHistoryDataType }
     * 
     */
    public UsageHistoryDataType createUsageHistoryDataType() {
        return new UsageHistoryDataType();
    }

    /**
     * Create an instance of {@link GetUsageHistoryDataType }
     * 
     */
    public GetUsageHistoryDataType createGetUsageHistoryDataType() {
        return new GetUsageHistoryDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteUsageHistoryDataType }
     * 
     */
    public FailedDeleteUsageHistoryDataType createFailedDeleteUsageHistoryDataType() {
        return new FailedDeleteUsageHistoryDataType();
    }

    /**
     * Create an instance of {@link MoveDeviceList }
     * 
     */
    public MoveDeviceList createMoveDeviceList() {
        return new MoveDeviceList();
    }

    /**
     * Create an instance of {@link FailedMoveDeviceDataType }
     * 
     */
    public FailedMoveDeviceDataType createFailedMoveDeviceDataType() {
        return new FailedMoveDeviceDataType();
    }

    /**
     * Create an instance of {@link FailedMoveDeviceListDataType }
     * 
     */
    public FailedMoveDeviceListDataType createFailedMoveDeviceListDataType() {
        return new FailedMoveDeviceListDataType();
    }

    /**
     * Create an instance of {@link ReturnHostType }
     * 
     */
    public ReturnHostType createReturnHostType() {
        return new ReturnHostType();
    }

    /**
     * Create an instance of {@link FailedReturnHostDataType }
     * 
     */
    public FailedReturnHostDataType createFailedReturnHostDataType() {
        return new FailedReturnHostDataType();
    }

    /**
     * Create an instance of {@link FailedReturnHostListDataType }
     * 
     */
    public FailedReturnHostListDataType createFailedReturnHostListDataType() {
        return new FailedReturnHostListDataType();
    }

    /**
     * Create an instance of {@link ObsoleteHostType }
     * 
     */
    public ObsoleteHostType createObsoleteHostType() {
        return new ObsoleteHostType();
    }

    /**
     * Create an instance of {@link FailedObsoleteHostDataType }
     * 
     */
    public FailedObsoleteHostDataType createFailedObsoleteHostDataType() {
        return new FailedObsoleteHostDataType();
    }

    /**
     * Create an instance of {@link FailedObsoleteHostListDataType }
     * 
     */
    public FailedObsoleteHostListDataType createFailedObsoleteHostListDataType() {
        return new FailedObsoleteHostListDataType();
    }

    /**
     * Create an instance of {@link GetDeletedSyncParametersType }
     * 
     */
    public GetDeletedSyncParametersType createGetDeletedSyncParametersType() {
        return new GetDeletedSyncParametersType();
    }

    /**
     * Create an instance of {@link FailedGetDeletedSyncDataType }
     * 
     */
    public FailedGetDeletedSyncDataType createFailedGetDeletedSyncDataType() {
        return new FailedGetDeletedSyncDataType();
    }

    /**
     * Create an instance of {@link DeletedSyncDataType }
     * 
     */
    public DeletedSyncDataType createDeletedSyncDataType() {
        return new DeletedSyncDataType();
    }

    /**
     * Create an instance of {@link GetDeletedSyncDataType }
     * 
     */
    public GetDeletedSyncDataType createGetDeletedSyncDataType() {
        return new GetDeletedSyncDataType();
    }

    /**
     * Create an instance of {@link FailedRestoreServedClientDataType }
     * 
     */
    public FailedRestoreServedClientDataType createFailedRestoreServedClientDataType() {
        return new FailedRestoreServedClientDataType();
    }

    /**
     * Create an instance of {@link RestoreServedClientDataType }
     * 
     */
    public RestoreServedClientDataType createRestoreServedClientDataType() {
        return new RestoreServedClientDataType();
    }

    /**
     * Create an instance of {@link FeatureIds }
     * 
     */
    public FeatureIds createFeatureIds() {
        return new FeatureIds();
    }

    /**
     * Create an instance of {@link CloneSuspect.LineItemActivationIds }
     * 
     */
    public CloneSuspect.LineItemActivationIds createCloneSuspectLineItemActivationIds() {
        return new CloneSuspect.LineItemActivationIds();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportResponse.CloneSuspects }
     * 
     */
    public GenerateCloneDetectionReportResponse.CloneSuspects createGenerateCloneDetectionReportResponseCloneSuspects() {
        return new GenerateCloneDetectionReportResponse.CloneSuspects();
    }

    /**
     * Create an instance of {@link GenerateCloneDetectionReportRequest.EnterpriseIds }
     * 
     */
    public GenerateCloneDetectionReportRequest.EnterpriseIds createGenerateCloneDetectionReportRequestEnterpriseIds() {
        return new GenerateCloneDetectionReportRequest.EnterpriseIds();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseProductRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "createBaseProductRequest")
    public JAXBElement<BaseProductRequestType> createCreateBaseProductRequest(BaseProductRequestType value) {
        return new JAXBElement<BaseProductRequestType>(_CreateBaseProductRequest_QNAME, BaseProductRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseProductResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "createBaseProductResponse")
    public JAXBElement<BaseProductResponseType> createCreateBaseProductResponse(BaseProductResponseType value) {
        return new JAXBElement<BaseProductResponseType>(_CreateBaseProductResponse_QNAME, BaseProductResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseProductRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "updateBaseProductRequest")
    public JAXBElement<BaseProductRequestType> createUpdateBaseProductRequest(BaseProductRequestType value) {
        return new JAXBElement<BaseProductRequestType>(_UpdateBaseProductRequest_QNAME, BaseProductRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseProductResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "updateBaseProductResponse")
    public JAXBElement<BaseProductResponseType> createUpdateBaseProductResponse(BaseProductResponseType value) {
        return new JAXBElement<BaseProductResponseType>(_UpdateBaseProductResponse_QNAME, BaseProductResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteBaseProductRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteBaseProductRequest")
    public JAXBElement<DeleteBaseProductRequestType> createDeleteBaseProductRequest(DeleteBaseProductRequestType value) {
        return new JAXBElement<DeleteBaseProductRequestType>(_DeleteBaseProductRequest_QNAME, DeleteBaseProductRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseProductResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteBaseProductResponse")
    public JAXBElement<BaseProductResponseType> createDeleteBaseProductResponse(BaseProductResponseType value) {
        return new JAXBElement<BaseProductResponseType>(_DeleteBaseProductResponse_QNAME, BaseProductResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDevRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "createDeviceRequest")
    public JAXBElement<CreateDevRequestType> createCreateDeviceRequest(CreateDevRequestType value) {
        return new JAXBElement<CreateDevRequestType>(_CreateDeviceRequest_QNAME, CreateDevRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDevResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "createDeviceResponse")
    public JAXBElement<CreateDevResponseType> createCreateDeviceResponse(CreateDevResponseType value) {
        return new JAXBElement<CreateDevResponseType>(_CreateDeviceResponse_QNAME, CreateDevResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDevRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "createTestDeviceRequest")
    public JAXBElement<CreateDevRequestType> createCreateTestDeviceRequest(CreateDevRequestType value) {
        return new JAXBElement<CreateDevRequestType>(_CreateTestDeviceRequest_QNAME, CreateDevRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateDevResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "createTestDeviceResponse")
    public JAXBElement<CreateDevResponseType> createCreateTestDeviceResponse(CreateDevResponseType value) {
        return new JAXBElement<CreateDevResponseType>(_CreateTestDeviceResponse_QNAME, CreateDevResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteDeviceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteDeviceRequest")
    public JAXBElement<DeleteDeviceRequestType> createDeleteDeviceRequest(DeleteDeviceRequestType value) {
        return new JAXBElement<DeleteDeviceRequestType>(_DeleteDeviceRequest_QNAME, DeleteDeviceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteDeviceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteDeviceResponse")
    public JAXBElement<DeleteDeviceResponseType> createDeleteDeviceResponse(DeleteDeviceResponseType value) {
        return new JAXBElement<DeleteDeviceResponseType>(_DeleteDeviceResponse_QNAME, DeleteDeviceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDevRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "updateDeviceRequest")
    public JAXBElement<UpdateDevRequestType> createUpdateDeviceRequest(UpdateDevRequestType value) {
        return new JAXBElement<UpdateDevRequestType>(_UpdateDeviceRequest_QNAME, UpdateDevRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDevResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "updateDeviceResponse")
    public JAXBElement<UpdateDevResponseType> createUpdateDeviceResponse(UpdateDevResponseType value) {
        return new JAXBElement<UpdateDevResponseType>(_UpdateDeviceResponse_QNAME, UpdateDevResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneratePrebuiltLicenseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "generatePrebuiltLicenseRequest")
    public JAXBElement<GeneratePrebuiltLicenseRequestType> createGeneratePrebuiltLicenseRequest(GeneratePrebuiltLicenseRequestType value) {
        return new JAXBElement<GeneratePrebuiltLicenseRequestType>(_GeneratePrebuiltLicenseRequest_QNAME, GeneratePrebuiltLicenseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GeneratePrebuiltLicenseResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "generatePrebuiltLicenseResponse")
    public JAXBElement<GeneratePrebuiltLicenseResponseType> createGeneratePrebuiltLicenseResponse(GeneratePrebuiltLicenseResponseType value) {
        return new JAXBElement<GeneratePrebuiltLicenseResponseType>(_GeneratePrebuiltLicenseResponse_QNAME, GeneratePrebuiltLicenseResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDevicesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getDevicesRequest")
    public JAXBElement<GetDevicesRequestType> createGetDevicesRequest(GetDevicesRequestType value) {
        return new JAXBElement<GetDevicesRequestType>(_GetDevicesRequest_QNAME, GetDevicesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDevicesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getDevicesResponse")
    public JAXBElement<GetDevicesResponseType> createGetDevicesResponse(GetDevicesResponseType value) {
        return new JAXBElement<GetDevicesResponseType>(_GetDevicesResponse_QNAME, GetDevicesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDevicesCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getDeviceCountRequest")
    public JAXBElement<GetDevicesCountRequestType> createGetDeviceCountRequest(GetDevicesCountRequestType value) {
        return new JAXBElement<GetDevicesCountRequestType>(_GetDeviceCountRequest_QNAME, GetDevicesCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeviceCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getDeviceCountResponse")
    public JAXBElement<GetDeviceCountResponseType> createGetDeviceCountResponse(GetDeviceCountResponseType value) {
        return new JAXBElement<GetDeviceCountResponseType>(_GetDeviceCountResponse_QNAME, GetDeviceCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "linkAddonLineItemsRequest")
    public JAXBElement<LinkAddonLineItemsRequestType> createLinkAddonLineItemsRequest(LinkAddonLineItemsRequestType value) {
        return new JAXBElement<LinkAddonLineItemsRequestType>(_LinkAddonLineItemsRequest_QNAME, LinkAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "linkAddonLineItemsResponse")
    public JAXBElement<LinkAddonLineItemsResponseType> createLinkAddonLineItemsResponse(LinkAddonLineItemsResponseType value) {
        return new JAXBElement<LinkAddonLineItemsResponseType>(_LinkAddonLineItemsResponse_QNAME, LinkAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteAddonLineItemsRequest")
    public JAXBElement<DeleteAddonLineItemsRequestType> createDeleteAddonLineItemsRequest(DeleteAddonLineItemsRequestType value) {
        return new JAXBElement<DeleteAddonLineItemsRequestType>(_DeleteAddonLineItemsRequest_QNAME, DeleteAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteAddonLineItemsResponse")
    public JAXBElement<DeleteAddonLineItemsResponseType> createDeleteAddonLineItemsResponse(DeleteAddonLineItemsResponseType value) {
        return new JAXBElement<DeleteAddonLineItemsResponseType>(_DeleteAddonLineItemsResponse_QNAME, DeleteAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "incrementAddonLineItemsRequest")
    public JAXBElement<LinkAddonLineItemsRequestType> createIncrementAddonLineItemsRequest(LinkAddonLineItemsRequestType value) {
        return new JAXBElement<LinkAddonLineItemsRequestType>(_IncrementAddonLineItemsRequest_QNAME, LinkAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "incrementAddonLineItemsResponse")
    public JAXBElement<LinkAddonLineItemsResponseType> createIncrementAddonLineItemsResponse(LinkAddonLineItemsResponseType value) {
        return new JAXBElement<LinkAddonLineItemsResponseType>(_IncrementAddonLineItemsResponse_QNAME, LinkAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "decrementAddonLineItemsRequest")
    public JAXBElement<LinkAddonLineItemsRequestType> createDecrementAddonLineItemsRequest(LinkAddonLineItemsRequestType value) {
        return new JAXBElement<LinkAddonLineItemsRequestType>(_DecrementAddonLineItemsRequest_QNAME, LinkAddonLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkAddonLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "decrementAddonLineItemsResponse")
    public JAXBElement<LinkAddonLineItemsResponseType> createDecrementAddonLineItemsResponse(LinkAddonLineItemsResponseType value) {
        return new JAXBElement<LinkAddonLineItemsResponseType>(_DecrementAddonLineItemsResponse_QNAME, LinkAddonLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDeviceStatusRequestListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "setDeviceStatusRequest")
    public JAXBElement<SetDeviceStatusRequestListType> createSetDeviceStatusRequest(SetDeviceStatusRequestListType value) {
        return new JAXBElement<SetDeviceStatusRequestListType>(_SetDeviceStatusRequest_QNAME, SetDeviceStatusRequestListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDeviceStatusResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "setDeviceStatusResponse")
    public JAXBElement<SetDeviceStatusResponseType> createSetDeviceStatusResponse(SetDeviceStatusResponseType value) {
        return new JAXBElement<SetDeviceStatusResponseType>(_SetDeviceStatusResponse_QNAME, SetDeviceStatusResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCapabilityResponseRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "generateCapabilityResponseRequest")
    public JAXBElement<GenerateCapabilityResponseRequestType> createGenerateCapabilityResponseRequest(GenerateCapabilityResponseRequestType value) {
        return new JAXBElement<GenerateCapabilityResponseRequestType>(_GenerateCapabilityResponseRequest_QNAME, GenerateCapabilityResponseRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCapabilityResponseResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "generateCapabilityResponseResponse")
    public JAXBElement<GenerateCapabilityResponseResponseType> createGenerateCapabilityResponseResponse(GenerateCapabilityResponseResponseType value) {
        return new JAXBElement<GenerateCapabilityResponseResponseType>(_GenerateCapabilityResponseResponse_QNAME, GenerateCapabilityResponseResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsageSummaryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getUsageSummaryRequest")
    public JAXBElement<GetUsageSummaryRequestType> createGetUsageSummaryRequest(GetUsageSummaryRequestType value) {
        return new JAXBElement<GetUsageSummaryRequestType>(_GetUsageSummaryRequest_QNAME, GetUsageSummaryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsageSummaryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getUsageSummaryResponse")
    public JAXBElement<GetUsageSummaryResponseType> createGetUsageSummaryResponse(GetUsageSummaryResponseType value) {
        return new JAXBElement<GetUsageSummaryResponseType>(_GetUsageSummaryResponse_QNAME, GetUsageSummaryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsageSummaryTimesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getUsageSummaryTimesRequest")
    public JAXBElement<GetUsageSummaryTimesRequestType> createGetUsageSummaryTimesRequest(GetUsageSummaryTimesRequestType value) {
        return new JAXBElement<GetUsageSummaryTimesRequestType>(_GetUsageSummaryTimesRequest_QNAME, GetUsageSummaryTimesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsageSummaryTimesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getUsageSummaryTimesResponse")
    public JAXBElement<GetUsageSummaryTimesResponseType> createGetUsageSummaryTimesResponse(GetUsageSummaryTimesResponseType value) {
        return new JAXBElement<GetUsageSummaryTimesResponseType>(_GetUsageSummaryTimesResponse_QNAME, GetUsageSummaryTimesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsageHistoryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getUsageHistoryRequest")
    public JAXBElement<GetUsageHistoryRequestType> createGetUsageHistoryRequest(GetUsageHistoryRequestType value) {
        return new JAXBElement<GetUsageHistoryRequestType>(_GetUsageHistoryRequest_QNAME, GetUsageHistoryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsageHistoryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getUsageHistoryResponse")
    public JAXBElement<GetUsageHistoryResponseType> createGetUsageHistoryResponse(GetUsageHistoryResponseType value) {
        return new JAXBElement<GetUsageHistoryResponseType>(_GetUsageHistoryResponse_QNAME, GetUsageHistoryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUsageHistoryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteUsageHistoryRequest")
    public JAXBElement<DeleteUsageHistoryRequestType> createDeleteUsageHistoryRequest(DeleteUsageHistoryRequestType value) {
        return new JAXBElement<DeleteUsageHistoryRequestType>(_DeleteUsageHistoryRequest_QNAME, DeleteUsageHistoryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteUsageHistoryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "deleteUsageHistoryResponse")
    public JAXBElement<DeleteUsageHistoryResponseType> createDeleteUsageHistoryResponse(DeleteUsageHistoryResponseType value) {
        return new JAXBElement<DeleteUsageHistoryResponseType>(_DeleteUsageHistoryResponse_QNAME, DeleteUsageHistoryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveDeviceRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "moveDeviceRequest")
    public JAXBElement<MoveDeviceRequestType> createMoveDeviceRequest(MoveDeviceRequestType value) {
        return new JAXBElement<MoveDeviceRequestType>(_MoveDeviceRequest_QNAME, MoveDeviceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveDeviceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "moveDeviceResponse")
    public JAXBElement<MoveDeviceResponseType> createMoveDeviceResponse(MoveDeviceResponseType value) {
        return new JAXBElement<MoveDeviceResponseType>(_MoveDeviceResponse_QNAME, MoveDeviceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnHostRequestListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "returnHostRequest")
    public JAXBElement<ReturnHostRequestListType> createReturnHostRequest(ReturnHostRequestListType value) {
        return new JAXBElement<ReturnHostRequestListType>(_ReturnHostRequest_QNAME, ReturnHostRequestListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReturnHostResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "returnHostResponse")
    public JAXBElement<ReturnHostResponseType> createReturnHostResponse(ReturnHostResponseType value) {
        return new JAXBElement<ReturnHostResponseType>(_ReturnHostResponse_QNAME, ReturnHostResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObsoleteHostRequestListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "obsoleteHostRequest")
    public JAXBElement<ObsoleteHostRequestListType> createObsoleteHostRequest(ObsoleteHostRequestListType value) {
        return new JAXBElement<ObsoleteHostRequestListType>(_ObsoleteHostRequest_QNAME, ObsoleteHostRequestListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ObsoleteHostResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "obsoleteHostResponse")
    public JAXBElement<ObsoleteHostResponseType> createObsoleteHostResponse(ObsoleteHostResponseType value) {
        return new JAXBElement<ObsoleteHostResponseType>(_ObsoleteHostResponse_QNAME, ObsoleteHostResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeletedSyncRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getDeletedSyncRequest")
    public JAXBElement<GetDeletedSyncRequestType> createGetDeletedSyncRequest(GetDeletedSyncRequestType value) {
        return new JAXBElement<GetDeletedSyncRequestType>(_GetDeletedSyncRequest_QNAME, GetDeletedSyncRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDeletedSyncResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getDeletedSyncResponse")
    public JAXBElement<GetDeletedSyncResponseType> createGetDeletedSyncResponse(GetDeletedSyncResponseType value) {
        return new JAXBElement<GetDeletedSyncResponseType>(_GetDeletedSyncResponse_QNAME, GetDeletedSyncResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestoreServedClientRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "restoreServedClientRequest")
    public JAXBElement<RestoreServedClientRequestType> createRestoreServedClientRequest(RestoreServedClientRequestType value) {
        return new JAXBElement<RestoreServedClientRequestType>(_RestoreServedClientRequest_QNAME, RestoreServedClientRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestoreServedClientResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "restoreServedClientResponse")
    public JAXBElement<RestoreServedClientResponseType> createRestoreServedClientResponse(RestoreServedClientResponseType value) {
        return new JAXBElement<RestoreServedClientResponseType>(_RestoreServedClientResponse_QNAME, RestoreServedClientResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAutoProvisionedServerRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getAutoProvisionedServerRequest")
    public JAXBElement<GetAutoProvisionedServerRequest> createGetAutoProvisionedServerRequest(GetAutoProvisionedServerRequest value) {
        return new JAXBElement<GetAutoProvisionedServerRequest>(_GetAutoProvisionedServerRequest_QNAME, GetAutoProvisionedServerRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAutoProvisionedServerResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "getAutoProvisionedServerResponse")
    public JAXBElement<GetAutoProvisionedServerResponse> createGetAutoProvisionedServerResponse(GetAutoProvisionedServerResponse value) {
        return new JAXBElement<GetAutoProvisionedServerResponse>(_GetAutoProvisionedServerResponse_QNAME, GetAutoProvisionedServerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCloneDetectionReportRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "generateCloneDetectionReportRequest")
    public JAXBElement<GenerateCloneDetectionReportRequest> createGenerateCloneDetectionReportRequest(GenerateCloneDetectionReportRequest value) {
        return new JAXBElement<GenerateCloneDetectionReportRequest>(_GenerateCloneDetectionReportRequest_QNAME, GenerateCloneDetectionReportRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCloneDetectionReportResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "generateCloneDetectionReportResponse")
    public JAXBElement<GenerateCloneDetectionReportResponse> createGenerateCloneDetectionReportResponse(GenerateCloneDetectionReportResponse value) {
        return new JAXBElement<GenerateCloneDetectionReportResponse>(_GenerateCloneDetectionReportResponse_QNAME, GenerateCloneDetectionReportResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateCloneDetectionReportRequest.EnterpriseIds }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com.macrovision:flexnet/opsembedded", name = "enterpriseIds", scope = GenerateCloneDetectionReportRequest.class)
    public JAXBElement<GenerateCloneDetectionReportRequest.EnterpriseIds> createGenerateCloneDetectionReportRequestEnterpriseIds(GenerateCloneDetectionReportRequest.EnterpriseIds value) {
        return new JAXBElement<GenerateCloneDetectionReportRequest.EnterpriseIds>(_GenerateCloneDetectionReportRequestEnterpriseIds_QNAME, GenerateCloneDetectionReportRequest.EnterpriseIds.class, GenerateCloneDetectionReportRequest.class, value);
    }

}
