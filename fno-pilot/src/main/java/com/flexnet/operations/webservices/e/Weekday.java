
package com.flexnet.operations.webservices.e;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for weekday.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="weekday"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="MONDAY"/&gt;
 *     &lt;enumeration value="TUESDAY"/&gt;
 *     &lt;enumeration value="WEDNESDAY"/&gt;
 *     &lt;enumeration value="THURSDAY"/&gt;
 *     &lt;enumeration value="FRIDAY"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "weekday")
@XmlEnum
public enum Weekday {

    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY;

    public String value() {
        return name();
    }

    public static Weekday fromValue(String v) {
        return valueOf(v);
    }

}
