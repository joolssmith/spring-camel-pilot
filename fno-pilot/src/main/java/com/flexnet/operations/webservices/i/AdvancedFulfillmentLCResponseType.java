
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for advancedFulfillmentLCResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="advancedFulfillmentLCResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:com.macrovision:flexnet/operations}StatusInfoType"/&gt;
 *         &lt;element name="responseData" type="{urn:com.macrovision:flexnet/operations}advancedFmtLCResponseDataListType" minOccurs="0"/&gt;
 *         &lt;element name="failedData" type="{urn:com.macrovision:flexnet/operations}failedAdvancedFmtLCResponseDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "advancedFulfillmentLCResponseType", propOrder = {
    "statusInfo",
    "responseData",
    "failedData"
})
public class AdvancedFulfillmentLCResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected AdvancedFmtLCResponseDataListType responseData;
    protected FailedAdvancedFmtLCResponseDataType failedData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link AdvancedFmtLCResponseDataListType }
     *     
     */
    public AdvancedFmtLCResponseDataListType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvancedFmtLCResponseDataListType }
     *     
     */
    public void setResponseData(AdvancedFmtLCResponseDataListType value) {
        this.responseData = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedAdvancedFmtLCResponseDataType }
     *     
     */
    public FailedAdvancedFmtLCResponseDataType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedAdvancedFmtLCResponseDataType }
     *     
     */
    public void setFailedData(FailedAdvancedFmtLCResponseDataType value) {
        this.failedData = value;
    }

}
