
package com.flexnet.operations.webservices.a;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RightsIdProblem complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RightsIdProblem"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="RightsId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RightsIdProblem", propOrder = {
    "message",
    "rightsId"
})
@XmlSeeAlso({
    NotLicensed.class,
    LineItemDoesNotExist.class,
    IneligibleForFulfillment.class,
    Overdraw.class,
    IncreaseBeyondEntitled.class,
    CompositeRightsIdException.class,
    InsufficientCountForSplit.class,
    IneligibleForSplit.class,
    LicenseModelInvalid.class,
    BaseProductDoesNotExist.class,
    UnRedeemedWebKey.class,
    WebKeyDoesNotExist.class,
    WebKeyRedemptionError.class,
    OwnerMismatchIncreaseNotAllowed.class,
    FeatureListUnavailable.class,
    LineItemQuantityChangeNotAllowed.class,
    UnexpectedRightsIdError.class,
    LineItemQuantityIncreaseNotAllowed.class,
    OverFeatureLimits.class,
    OwnerMismatch.class
})
public class RightsIdProblem {

    @XmlElementRef(name = "Message", type = JAXBElement.class, required = false)
    protected JAXBElement<String> message;
    @XmlElementRef(name = "RightsId", type = JAXBElement.class, required = false)
    protected JAXBElement<String> rightsId;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setMessage(JAXBElement<String> value) {
        this.message = value;
    }

    /**
     * Gets the value of the rightsId property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getRightsId() {
        return rightsId;
    }

    /**
     * Sets the value of the rightsId property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setRightsId(JAXBElement<String> value) {
        this.rightsId = value;
    }

}
