
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for advancedFulfillmentLCRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="advancedFulfillmentLCRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fulfillmentList" type="{urn:v1.webservices.operations.flexnet.com}advancedFulfillmentLCListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "advancedFulfillmentLCRequestType", propOrder = {
    "fulfillmentList"
})
public class AdvancedFulfillmentLCRequestType {

    @XmlElement(required = true)
    protected AdvancedFulfillmentLCListType fulfillmentList;

    /**
     * Gets the value of the fulfillmentList property.
     * 
     * @return
     *     possible object is
     *     {@link AdvancedFulfillmentLCListType }
     *     
     */
    public AdvancedFulfillmentLCListType getFulfillmentList() {
        return fulfillmentList;
    }

    /**
     * Sets the value of the fulfillmentList property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvancedFulfillmentLCListType }
     *     
     */
    public void setFulfillmentList(AdvancedFulfillmentLCListType value) {
        this.fulfillmentList = value;
    }

}
