
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedMapEntitlementsToUserDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedMapEntitlementsToUserDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedId" type="{urn:v2.webservices.operations.flexnet.com}failedIdDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedMapEntitlementsToUserDataListType", propOrder = {
    "failedId"
})
public class FailedMapEntitlementsToUserDataListType {

    @XmlElement(required = true)
    protected List<FailedIdDataType> failedId;

    /**
     * Gets the value of the failedId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedIdDataType }
     * 
     * 
     */
    public List<FailedIdDataType> getFailedId() {
        if (failedId == null) {
            failedId = new ArrayList<FailedIdDataType>();
        }
        return this.failedId;
    }

}
