
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createShortCodeDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createShortCodeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlementId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="webRegkey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="shortCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="publisherAttributes" type="{urn:com.macrovision:flexnet/operations}publisherAttributesListDataType" minOccurs="0"/&gt;
 *         &lt;element name="overridePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="shortCodeActivationType" type="{urn:com.macrovision:flexnet/operations}ShortCodeActivationType" minOccurs="0"/&gt;
 *         &lt;element name="reinstallFulfillment" type="{urn:com.macrovision:flexnet/operations}fulfillmentIdentifierType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createShortCodeDataType", propOrder = {
    "bulkEntitlementId",
    "webRegkey",
    "shortCode",
    "publisherAttributes",
    "overridePolicy",
    "shortCodeActivationType",
    "reinstallFulfillment"
})
public class CreateShortCodeDataType {

    protected String bulkEntitlementId;
    @XmlElement(required = true)
    protected String webRegkey;
    @XmlElement(required = true)
    protected String shortCode;
    protected PublisherAttributesListDataType publisherAttributes;
    protected Boolean overridePolicy;
    @XmlSchemaType(name = "NMTOKEN")
    protected ShortCodeActivationType shortCodeActivationType;
    protected FulfillmentIdentifierType reinstallFulfillment;

    /**
     * Gets the value of the bulkEntitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBulkEntitlementId() {
        return bulkEntitlementId;
    }

    /**
     * Sets the value of the bulkEntitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBulkEntitlementId(String value) {
        this.bulkEntitlementId = value;
    }

    /**
     * Gets the value of the webRegkey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebRegkey() {
        return webRegkey;
    }

    /**
     * Sets the value of the webRegkey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebRegkey(String value) {
        this.webRegkey = value;
    }

    /**
     * Gets the value of the shortCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortCode() {
        return shortCode;
    }

    /**
     * Sets the value of the shortCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortCode(String value) {
        this.shortCode = value;
    }

    /**
     * Gets the value of the publisherAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link PublisherAttributesListDataType }
     *     
     */
    public PublisherAttributesListDataType getPublisherAttributes() {
        return publisherAttributes;
    }

    /**
     * Sets the value of the publisherAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublisherAttributesListDataType }
     *     
     */
    public void setPublisherAttributes(PublisherAttributesListDataType value) {
        this.publisherAttributes = value;
    }

    /**
     * Gets the value of the overridePolicy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOverridePolicy() {
        return overridePolicy;
    }

    /**
     * Sets the value of the overridePolicy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOverridePolicy(Boolean value) {
        this.overridePolicy = value;
    }

    /**
     * Gets the value of the shortCodeActivationType property.
     * 
     * @return
     *     possible object is
     *     {@link ShortCodeActivationType }
     *     
     */
    public ShortCodeActivationType getShortCodeActivationType() {
        return shortCodeActivationType;
    }

    /**
     * Sets the value of the shortCodeActivationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ShortCodeActivationType }
     *     
     */
    public void setShortCodeActivationType(ShortCodeActivationType value) {
        this.shortCodeActivationType = value;
    }

    /**
     * Gets the value of the reinstallFulfillment property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getReinstallFulfillment() {
        return reinstallFulfillment;
    }

    /**
     * Sets the value of the reinstallFulfillment property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setReinstallFulfillment(FulfillmentIdentifierType value) {
        this.reinstallFulfillment = value;
    }

}
