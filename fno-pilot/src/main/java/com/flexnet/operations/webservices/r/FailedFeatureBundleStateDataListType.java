
package com.flexnet.operations.webservices.r;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedFeatureBundleStateDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedFeatureBundleStateDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedFeatureBundle" type="{urn:v1.webservices.operations.flexnet.com}failedFeatureBundleStateDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedFeatureBundleStateDataListType", propOrder = {
    "failedFeatureBundle"
})
public class FailedFeatureBundleStateDataListType {

    protected List<FailedFeatureBundleStateDataType> failedFeatureBundle;

    /**
     * Gets the value of the failedFeatureBundle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedFeatureBundle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedFeatureBundle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedFeatureBundleStateDataType }
     * 
     * 
     */
    public List<FailedFeatureBundleStateDataType> getFailedFeatureBundle() {
        if (failedFeatureBundle == null) {
            failedFeatureBundle = new ArrayList<FailedFeatureBundleStateDataType>();
        }
        return this.failedFeatureBundle;
    }

}
