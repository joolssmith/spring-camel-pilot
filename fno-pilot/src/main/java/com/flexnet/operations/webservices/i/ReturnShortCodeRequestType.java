
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for returnShortCodeRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="returnShortCodeRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="shortCodeData" type="{urn:com.macrovision:flexnet/operations}returnShortCodeDataType" minOccurs="0"/&gt;
 *         &lt;element name="returnReason" type="{urn:com.macrovision:flexnet/operations}ReturnedShortCodeReturnReason" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "returnShortCodeRequestType", propOrder = {
    "shortCodeData",
    "returnReason"
})
public class ReturnShortCodeRequestType {

    protected ReturnShortCodeDataType shortCodeData;
    @XmlSchemaType(name = "NMTOKEN")
    protected ReturnedShortCodeReturnReason returnReason;

    /**
     * Gets the value of the shortCodeData property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnShortCodeDataType }
     *     
     */
    public ReturnShortCodeDataType getShortCodeData() {
        return shortCodeData;
    }

    /**
     * Sets the value of the shortCodeData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnShortCodeDataType }
     *     
     */
    public void setShortCodeData(ReturnShortCodeDataType value) {
        this.shortCodeData = value;
    }

    /**
     * Gets the value of the returnReason property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnedShortCodeReturnReason }
     *     
     */
    public ReturnedShortCodeReturnReason getReturnReason() {
        return returnReason;
    }

    /**
     * Sets the value of the returnReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnedShortCodeReturnReason }
     *     
     */
    public void setReturnReason(ReturnedShortCodeReturnReason value) {
        this.returnReason = value;
    }

}
