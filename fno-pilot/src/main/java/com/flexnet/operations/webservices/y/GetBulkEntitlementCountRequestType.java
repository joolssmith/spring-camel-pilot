
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBulkEntitlementCountRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBulkEntitlementCountRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlementSearchCriteria" type="{urn:v3.webservices.operations.flexnet.com}searchBulkEntitlementDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBulkEntitlementCountRequestType", propOrder = {
    "bulkEntitlementSearchCriteria"
})
public class GetBulkEntitlementCountRequestType {

    @XmlElement(required = true)
    protected SearchBulkEntitlementDataType bulkEntitlementSearchCriteria;

    /**
     * Gets the value of the bulkEntitlementSearchCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link SearchBulkEntitlementDataType }
     *     
     */
    public SearchBulkEntitlementDataType getBulkEntitlementSearchCriteria() {
        return bulkEntitlementSearchCriteria;
    }

    /**
     * Sets the value of the bulkEntitlementSearchCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchBulkEntitlementDataType }
     *     
     */
    public void setBulkEntitlementSearchCriteria(SearchBulkEntitlementDataType value) {
        this.bulkEntitlementSearchCriteria = value;
    }

}
