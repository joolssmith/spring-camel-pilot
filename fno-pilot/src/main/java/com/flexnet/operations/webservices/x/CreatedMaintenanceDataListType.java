
package com.flexnet.operations.webservices.x;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdMaintenanceDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdMaintenanceDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="createdMaintenance" type="{urn:v2.webservices.operations.flexnet.com}createdMaintenaceDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdMaintenanceDataListType", propOrder = {
    "createdMaintenance"
})
public class CreatedMaintenanceDataListType {

    protected List<CreatedMaintenaceDataType> createdMaintenance;

    /**
     * Gets the value of the createdMaintenance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdMaintenance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedMaintenance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatedMaintenaceDataType }
     * 
     * 
     */
    public List<CreatedMaintenaceDataType> getCreatedMaintenance() {
        if (createdMaintenance == null) {
            createdMaintenance = new ArrayList<CreatedMaintenaceDataType>();
        }
        return this.createdMaintenance;
    }

}
