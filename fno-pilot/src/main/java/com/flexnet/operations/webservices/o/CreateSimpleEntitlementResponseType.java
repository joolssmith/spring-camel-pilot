
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createSimpleEntitlementResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createSimpleEntitlementResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="failedData" type="{urn:v1.webservices.operations.flexnet.com}failedSimpleEntitlementDataListType" minOccurs="0"/&gt;
 *         &lt;element name="responseData" type="{urn:v1.webservices.operations.flexnet.com}createdSimpleEntitlementDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createSimpleEntitlementResponseType", propOrder = {
    "statusInfo",
    "failedData",
    "responseData"
})
public class CreateSimpleEntitlementResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected FailedSimpleEntitlementDataListType failedData;
    protected CreatedSimpleEntitlementDataListType responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedSimpleEntitlementDataListType }
     *     
     */
    public FailedSimpleEntitlementDataListType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedSimpleEntitlementDataListType }
     *     
     */
    public void setFailedData(FailedSimpleEntitlementDataListType value) {
        this.failedData = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link CreatedSimpleEntitlementDataListType }
     *     
     */
    public CreatedSimpleEntitlementDataListType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatedSimpleEntitlementDataListType }
     *     
     */
    public void setResponseData(CreatedSimpleEntitlementDataListType value) {
        this.responseData = value;
    }

}
