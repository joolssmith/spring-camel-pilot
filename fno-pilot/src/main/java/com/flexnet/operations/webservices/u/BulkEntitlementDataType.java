
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for bulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="bulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{urn:v2.webservices.operations.flexnet.com}createBulkEntitlementDataType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="state" type="{urn:v2.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *         &lt;element name="createdUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:v2.webservices.operations.flexnet.com}licenseTechnologyIdentifierType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bulkEntitlementDataType", propOrder = {
    "state",
    "createdUserId",
    "licenseTechnology"
})
public class BulkEntitlementDataType
    extends CreateBulkEntitlementDataType
{

    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    protected String createdUserId;
    protected LicenseTechnologyIdentifierType licenseTechnology;

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the createdUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedUserId() {
        return createdUserId;
    }

    /**
     * Sets the value of the createdUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedUserId(String value) {
        this.createdUserId = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

}
