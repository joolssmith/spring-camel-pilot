
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateOrUpdateOperationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CreateOrUpdateOperationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="CREATE_OR_UPDATE"/&gt;
 *     &lt;enumeration value="CREATE_OR_IGNORE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CreateOrUpdateOperationType")
@XmlEnum
public enum CreateOrUpdateOperationType {

    CREATE_OR_UPDATE,
    CREATE_OR_IGNORE;

    public String value() {
        return name();
    }

    public static CreateOrUpdateOperationType fromValue(String v) {
        return valueOf(v);
    }

}
