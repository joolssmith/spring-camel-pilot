
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for entitlementLineItemResponseConfigRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="entitlementLineItemResponseConfigRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="activatableItemType" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowPortalLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="soldToDisplayName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="entitlementState" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="entitlementDescription" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="parentBulkEntitlementId" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntSoldTo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntSoldToDisplayName" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="productDescription" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="partNumberDescription" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="licenseModel" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="lineItemSupportAction" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="parentLineItem" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="startDateOption" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="term" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="versionDateAttributes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="numberOfCopies" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="fulfilledAmount" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="numberOfRemainingCopies" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isTrusted" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v3.webservices.operations.flexnet.com}customAttributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="maintenance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="maintenancePartNumber" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="FNPTimeZoneValue" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="createdOnDateTime" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="lineItemAttributes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItemAttributes" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="transferredFromLineItem" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="splitFromLineItem" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "entitlementLineItemResponseConfigRequestType", propOrder = {
    "activationId",
    "description",
    "state",
    "activatableItemType",
    "orderId",
    "orderLineNumber",
    "entitlementId",
    "allowPortalLogin",
    "soldTo",
    "soldToDisplayName",
    "entitlementState",
    "entitlementDescription",
    "shipToEmail",
    "shipToAddress",
    "parentBulkEntitlementId",
    "bulkEntSoldTo",
    "bulkEntSoldToDisplayName",
    "product",
    "productDescription",
    "partNumber",
    "partNumberDescription",
    "licenseTechnology",
    "licenseModel",
    "lineItemSupportAction",
    "parentLineItem",
    "startDate",
    "startDateOption",
    "isPermanent",
    "term",
    "expirationDate",
    "versionDate",
    "versionDateAttributes",
    "numberOfCopies",
    "fulfilledAmount",
    "numberOfRemainingCopies",
    "isTrusted",
    "customAttributes",
    "maintenance",
    "maintenancePartNumber",
    "fnpTimeZoneValue",
    "createdOnDateTime",
    "lastModifiedDateTime",
    "lineItemAttributes",
    "maintenanceLineItemAttributes",
    "transferredFromLineItem",
    "splitFromLineItem"
})
public class EntitlementLineItemResponseConfigRequestType {

    protected Boolean activationId;
    protected Boolean description;
    protected Boolean state;
    protected Boolean activatableItemType;
    protected Boolean orderId;
    protected Boolean orderLineNumber;
    protected Boolean entitlementId;
    protected Boolean allowPortalLogin;
    protected Boolean soldTo;
    protected Boolean soldToDisplayName;
    protected Boolean entitlementState;
    protected Boolean entitlementDescription;
    protected Boolean shipToEmail;
    protected Boolean shipToAddress;
    protected Boolean parentBulkEntitlementId;
    protected Boolean bulkEntSoldTo;
    protected Boolean bulkEntSoldToDisplayName;
    protected Boolean product;
    protected Boolean productDescription;
    protected Boolean partNumber;
    protected Boolean partNumberDescription;
    protected Boolean licenseTechnology;
    protected Boolean licenseModel;
    protected Boolean lineItemSupportAction;
    protected Boolean parentLineItem;
    protected Boolean startDate;
    protected Boolean startDateOption;
    protected Boolean isPermanent;
    protected Boolean term;
    protected Boolean expirationDate;
    protected Boolean versionDate;
    protected Boolean versionDateAttributes;
    protected Boolean numberOfCopies;
    protected Boolean fulfilledAmount;
    protected Boolean numberOfRemainingCopies;
    protected Boolean isTrusted;
    protected CustomAttributeDescriptorDataType customAttributes;
    protected Boolean maintenance;
    protected Boolean maintenancePartNumber;
    @XmlElement(name = "FNPTimeZoneValue")
    protected Boolean fnpTimeZoneValue;
    protected Boolean createdOnDateTime;
    protected Boolean lastModifiedDateTime;
    protected Boolean lineItemAttributes;
    protected Boolean maintenanceLineItemAttributes;
    protected Boolean transferredFromLineItem;
    protected Boolean splitFromLineItem;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActivationId(Boolean value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDescription(Boolean value) {
        this.description = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setState(Boolean value) {
        this.state = value;
    }

    /**
     * Gets the value of the activatableItemType property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isActivatableItemType() {
        return activatableItemType;
    }

    /**
     * Sets the value of the activatableItemType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setActivatableItemType(Boolean value) {
        this.activatableItemType = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOrderId(Boolean value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOrderLineNumber(Boolean value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEntitlementId(Boolean value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the allowPortalLogin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPortalLogin() {
        return allowPortalLogin;
    }

    /**
     * Sets the value of the allowPortalLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPortalLogin(Boolean value) {
        this.allowPortalLogin = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSoldTo(Boolean value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the soldToDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSoldToDisplayName() {
        return soldToDisplayName;
    }

    /**
     * Sets the value of the soldToDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSoldToDisplayName(Boolean value) {
        this.soldToDisplayName = value;
    }

    /**
     * Gets the value of the entitlementState property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEntitlementState() {
        return entitlementState;
    }

    /**
     * Sets the value of the entitlementState property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEntitlementState(Boolean value) {
        this.entitlementState = value;
    }

    /**
     * Gets the value of the entitlementDescription property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEntitlementDescription() {
        return entitlementDescription;
    }

    /**
     * Sets the value of the entitlementDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEntitlementDescription(Boolean value) {
        this.entitlementDescription = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipToEmail(Boolean value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShipToAddress(Boolean value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the parentBulkEntitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isParentBulkEntitlementId() {
        return parentBulkEntitlementId;
    }

    /**
     * Sets the value of the parentBulkEntitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setParentBulkEntitlementId(Boolean value) {
        this.parentBulkEntitlementId = value;
    }

    /**
     * Gets the value of the bulkEntSoldTo property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBulkEntSoldTo() {
        return bulkEntSoldTo;
    }

    /**
     * Sets the value of the bulkEntSoldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBulkEntSoldTo(Boolean value) {
        this.bulkEntSoldTo = value;
    }

    /**
     * Gets the value of the bulkEntSoldToDisplayName property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBulkEntSoldToDisplayName() {
        return bulkEntSoldToDisplayName;
    }

    /**
     * Sets the value of the bulkEntSoldToDisplayName property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBulkEntSoldToDisplayName(Boolean value) {
        this.bulkEntSoldToDisplayName = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProduct(Boolean value) {
        this.product = value;
    }

    /**
     * Gets the value of the productDescription property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProductDescription() {
        return productDescription;
    }

    /**
     * Sets the value of the productDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProductDescription(Boolean value) {
        this.productDescription = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumber(Boolean value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the partNumberDescription property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPartNumberDescription() {
        return partNumberDescription;
    }

    /**
     * Sets the value of the partNumberDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPartNumberDescription(Boolean value) {
        this.partNumberDescription = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLicenseTechnology(Boolean value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the licenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLicenseModel() {
        return licenseModel;
    }

    /**
     * Sets the value of the licenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLicenseModel(Boolean value) {
        this.licenseModel = value;
    }

    /**
     * Gets the value of the lineItemSupportAction property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLineItemSupportAction() {
        return lineItemSupportAction;
    }

    /**
     * Sets the value of the lineItemSupportAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLineItemSupportAction(Boolean value) {
        this.lineItemSupportAction = value;
    }

    /**
     * Gets the value of the parentLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isParentLineItem() {
        return parentLineItem;
    }

    /**
     * Sets the value of the parentLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setParentLineItem(Boolean value) {
        this.parentLineItem = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStartDate(Boolean value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the startDateOption property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isStartDateOption() {
        return startDateOption;
    }

    /**
     * Sets the value of the startDateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setStartDateOption(Boolean value) {
        this.startDateOption = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTerm(Boolean value) {
        this.term = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExpirationDate(Boolean value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVersionDate(Boolean value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the versionDateAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVersionDateAttributes() {
        return versionDateAttributes;
    }

    /**
     * Sets the value of the versionDateAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVersionDateAttributes(Boolean value) {
        this.versionDateAttributes = value;
    }

    /**
     * Gets the value of the numberOfCopies property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * Sets the value of the numberOfCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNumberOfCopies(Boolean value) {
        this.numberOfCopies = value;
    }

    /**
     * Gets the value of the fulfilledAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFulfilledAmount() {
        return fulfilledAmount;
    }

    /**
     * Sets the value of the fulfilledAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFulfilledAmount(Boolean value) {
        this.fulfilledAmount = value;
    }

    /**
     * Gets the value of the numberOfRemainingCopies property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNumberOfRemainingCopies() {
        return numberOfRemainingCopies;
    }

    /**
     * Sets the value of the numberOfRemainingCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNumberOfRemainingCopies(Boolean value) {
        this.numberOfRemainingCopies = value;
    }

    /**
     * Gets the value of the isTrusted property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTrusted() {
        return isTrusted;
    }

    /**
     * Sets the value of the isTrusted property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTrusted(Boolean value) {
        this.isTrusted = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link CustomAttributeDescriptorDataType }
     *     
     */
    public CustomAttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomAttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(CustomAttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the maintenance property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaintenance() {
        return maintenance;
    }

    /**
     * Sets the value of the maintenance property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaintenance(Boolean value) {
        this.maintenance = value;
    }

    /**
     * Gets the value of the maintenancePartNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaintenancePartNumber() {
        return maintenancePartNumber;
    }

    /**
     * Sets the value of the maintenancePartNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaintenancePartNumber(Boolean value) {
        this.maintenancePartNumber = value;
    }

    /**
     * Gets the value of the fnpTimeZoneValue property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFNPTimeZoneValue() {
        return fnpTimeZoneValue;
    }

    /**
     * Sets the value of the fnpTimeZoneValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFNPTimeZoneValue(Boolean value) {
        this.fnpTimeZoneValue = value;
    }

    /**
     * Gets the value of the createdOnDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCreatedOnDateTime() {
        return createdOnDateTime;
    }

    /**
     * Sets the value of the createdOnDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCreatedOnDateTime(Boolean value) {
        this.createdOnDateTime = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLastModifiedDateTime(Boolean value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the lineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLineItemAttributes() {
        return lineItemAttributes;
    }

    /**
     * Sets the value of the lineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLineItemAttributes(Boolean value) {
        this.lineItemAttributes = value;
    }

    /**
     * Gets the value of the maintenanceLineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMaintenanceLineItemAttributes() {
        return maintenanceLineItemAttributes;
    }

    /**
     * Sets the value of the maintenanceLineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMaintenanceLineItemAttributes(Boolean value) {
        this.maintenanceLineItemAttributes = value;
    }

    /**
     * Gets the value of the transferredFromLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTransferredFromLineItem() {
        return transferredFromLineItem;
    }

    /**
     * Sets the value of the transferredFromLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTransferredFromLineItem(Boolean value) {
        this.transferredFromLineItem = value;
    }

    /**
     * Gets the value of the splitFromLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSplitFromLineItem() {
        return splitFromLineItem;
    }

    /**
     * Sets the value of the splitFromLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSplitFromLineItem(Boolean value) {
        this.splitFromLineItem = value;
    }

}
