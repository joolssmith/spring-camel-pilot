
package com.flexnet.operations.webservices.r;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateFeatureBundlesListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateFeatureBundlesListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundle" type="{urn:v1.webservices.operations.flexnet.com}featureBundleIdentifierWithCountDataType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="opType" type="{urn:v1.webservices.operations.flexnet.com}CollectionOperationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateFeatureBundlesListType", propOrder = {
    "featureBundle",
    "opType"
})
public class UpdateFeatureBundlesListType {

    @XmlElement(required = true)
    protected List<FeatureBundleIdentifierWithCountDataType> featureBundle;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected CollectionOperationType opType;

    /**
     * Gets the value of the featureBundle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featureBundle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureBundle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureBundleIdentifierWithCountDataType }
     * 
     * 
     */
    public List<FeatureBundleIdentifierWithCountDataType> getFeatureBundle() {
        if (featureBundle == null) {
            featureBundle = new ArrayList<FeatureBundleIdentifierWithCountDataType>();
        }
        return this.featureBundle;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link CollectionOperationType }
     *     
     */
    public CollectionOperationType getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionOperationType }
     *     
     */
    public void setOpType(CollectionOperationType value) {
        this.opType = value;
    }

}
