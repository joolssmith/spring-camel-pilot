
package com.flexnet.operations.webservices.y;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for updateLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemIdentifier" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="activationId" type="{urn:v3.webservices.operations.flexnet.com}idType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{urn:v3.webservices.operations.flexnet.com}productIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{urn:v3.webservices.operations.flexnet.com}partNumberIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModel" type="{urn:v3.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel1" type="{urn:v3.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="alternateLicenseModel2" type="{urn:v3.webservices.operations.flexnet.com}licenseModelIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModelAttributes" type="{urn:v3.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="FNPTimeZoneValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="policyAttributes" type="{urn:v3.webservices.operations.flexnet.com}policyAttributesListType" minOccurs="0"/&gt;
 *         &lt;element name="orderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="orderLineNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numberOfCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="startDateOption" type="{urn:v3.webservices.operations.flexnet.com}StartDateOptionType" minOccurs="0"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="term" type="{urn:v3.webservices.operations.flexnet.com}DurationType" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="versionDateAttributes" type="{urn:v3.webservices.operations.flexnet.com}versionDateAttributesType" minOccurs="0"/&gt;
 *         &lt;element name="parentLineItem" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="updatedEntitledProducts" type="{urn:v3.webservices.operations.flexnet.com}updateEntitledProductDataListType" minOccurs="0"/&gt;
 *         &lt;element name="entitledProducts" type="{urn:v3.webservices.operations.flexnet.com}entitledProductDataListType" minOccurs="0"/&gt;
 *         &lt;element name="lineItemAttributes" type="{urn:v3.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateLineItemDataType", propOrder = {
    "lineItemIdentifier",
    "activationId",
    "description",
    "product",
    "partNumber",
    "licenseModel",
    "alternateLicenseModel1",
    "alternateLicenseModel2",
    "licenseModelAttributes",
    "fnpTimeZoneValue",
    "policyAttributes",
    "orderId",
    "orderLineNumber",
    "numberOfCopies",
    "startDate",
    "startDateOption",
    "isPermanent",
    "term",
    "expirationDate",
    "versionDate",
    "versionDateAttributes",
    "parentLineItem",
    "updatedEntitledProducts",
    "entitledProducts",
    "lineItemAttributes"
})
public class UpdateLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType lineItemIdentifier;
    protected IdType activationId;
    protected String description;
    protected ProductIdentifierType product;
    protected PartNumberIdentifierType partNumber;
    protected LicenseModelIdentifierType licenseModel;
    protected LicenseModelIdentifierType alternateLicenseModel1;
    protected LicenseModelIdentifierType alternateLicenseModel2;
    protected AttributeDescriptorDataType licenseModelAttributes;
    @XmlElement(name = "FNPTimeZoneValue")
    protected String fnpTimeZoneValue;
    protected PolicyAttributesListType policyAttributes;
    protected String orderId;
    protected String orderLineNumber;
    protected BigInteger numberOfCopies;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "NMTOKEN")
    protected StartDateOptionType startDateOption;
    protected Boolean isPermanent;
    protected DurationType term;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar versionDate;
    protected VersionDateAttributesType versionDateAttributes;
    protected EntitlementLineItemIdentifierType parentLineItem;
    protected UpdateEntitledProductDataListType updatedEntitledProducts;
    protected EntitledProductDataListType entitledProducts;
    protected AttributeDescriptorDataType lineItemAttributes;

    /**
     * Gets the value of the lineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getLineItemIdentifier() {
        return lineItemIdentifier;
    }

    /**
     * Sets the value of the lineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.lineItemIdentifier = value;
    }

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link IdType }
     *     
     */
    public IdType getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdType }
     *     
     */
    public void setActivationId(IdType value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setProduct(ProductIdentifierType value) {
        this.product = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public PartNumberIdentifierType getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumberIdentifierType }
     *     
     */
    public void setPartNumber(PartNumberIdentifierType value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the licenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModel() {
        return licenseModel;
    }

    /**
     * Sets the value of the licenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModel(LicenseModelIdentifierType value) {
        this.licenseModel = value;
    }

    /**
     * Gets the value of the alternateLicenseModel1 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel1() {
        return alternateLicenseModel1;
    }

    /**
     * Sets the value of the alternateLicenseModel1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel1(LicenseModelIdentifierType value) {
        this.alternateLicenseModel1 = value;
    }

    /**
     * Gets the value of the alternateLicenseModel2 property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getAlternateLicenseModel2() {
        return alternateLicenseModel2;
    }

    /**
     * Sets the value of the alternateLicenseModel2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setAlternateLicenseModel2(LicenseModelIdentifierType value) {
        this.alternateLicenseModel2 = value;
    }

    /**
     * Gets the value of the licenseModelAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLicenseModelAttributes() {
        return licenseModelAttributes;
    }

    /**
     * Sets the value of the licenseModelAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLicenseModelAttributes(AttributeDescriptorDataType value) {
        this.licenseModelAttributes = value;
    }

    /**
     * Gets the value of the fnpTimeZoneValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFNPTimeZoneValue() {
        return fnpTimeZoneValue;
    }

    /**
     * Sets the value of the fnpTimeZoneValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFNPTimeZoneValue(String value) {
        this.fnpTimeZoneValue = value;
    }

    /**
     * Gets the value of the policyAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyAttributesListType }
     *     
     */
    public PolicyAttributesListType getPolicyAttributes() {
        return policyAttributes;
    }

    /**
     * Sets the value of the policyAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyAttributesListType }
     *     
     */
    public void setPolicyAttributes(PolicyAttributesListType value) {
        this.policyAttributes = value;
    }

    /**
     * Gets the value of the orderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * Sets the value of the orderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderId(String value) {
        this.orderId = value;
    }

    /**
     * Gets the value of the orderLineNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderLineNumber() {
        return orderLineNumber;
    }

    /**
     * Sets the value of the orderLineNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderLineNumber(String value) {
        this.orderLineNumber = value;
    }

    /**
     * Gets the value of the numberOfCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberOfCopies() {
        return numberOfCopies;
    }

    /**
     * Sets the value of the numberOfCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberOfCopies(BigInteger value) {
        this.numberOfCopies = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the startDateOption property.
     * 
     * @return
     *     possible object is
     *     {@link StartDateOptionType }
     *     
     */
    public StartDateOptionType getStartDateOption() {
        return startDateOption;
    }

    /**
     * Sets the value of the startDateOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link StartDateOptionType }
     *     
     */
    public void setStartDateOption(StartDateOptionType value) {
        this.startDateOption = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsPermanent(Boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the term property.
     * 
     * @return
     *     possible object is
     *     {@link DurationType }
     *     
     */
    public DurationType getTerm() {
        return term;
    }

    /**
     * Sets the value of the term property.
     * 
     * @param value
     *     allowed object is
     *     {@link DurationType }
     *     
     */
    public void setTerm(DurationType value) {
        this.term = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the versionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getVersionDate() {
        return versionDate;
    }

    /**
     * Sets the value of the versionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setVersionDate(XMLGregorianCalendar value) {
        this.versionDate = value;
    }

    /**
     * Gets the value of the versionDateAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public VersionDateAttributesType getVersionDateAttributes() {
        return versionDateAttributes;
    }

    /**
     * Sets the value of the versionDateAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link VersionDateAttributesType }
     *     
     */
    public void setVersionDateAttributes(VersionDateAttributesType value) {
        this.versionDateAttributes = value;
    }

    /**
     * Gets the value of the parentLineItem property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getParentLineItem() {
        return parentLineItem;
    }

    /**
     * Sets the value of the parentLineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setParentLineItem(EntitlementLineItemIdentifierType value) {
        this.parentLineItem = value;
    }

    /**
     * Gets the value of the updatedEntitledProducts property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateEntitledProductDataListType }
     *     
     */
    public UpdateEntitledProductDataListType getUpdatedEntitledProducts() {
        return updatedEntitledProducts;
    }

    /**
     * Sets the value of the updatedEntitledProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateEntitledProductDataListType }
     *     
     */
    public void setUpdatedEntitledProducts(UpdateEntitledProductDataListType value) {
        this.updatedEntitledProducts = value;
    }

    /**
     * Gets the value of the entitledProducts property.
     * 
     * @return
     *     possible object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public EntitledProductDataListType getEntitledProducts() {
        return entitledProducts;
    }

    /**
     * Sets the value of the entitledProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public void setEntitledProducts(EntitledProductDataListType value) {
        this.entitledProducts = value;
    }

    /**
     * Gets the value of the lineItemAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLineItemAttributes() {
        return lineItemAttributes;
    }

    /**
     * Sets the value of the lineItemAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLineItemAttributes(AttributeDescriptorDataType value) {
        this.lineItemAttributes = value;
    }

}
