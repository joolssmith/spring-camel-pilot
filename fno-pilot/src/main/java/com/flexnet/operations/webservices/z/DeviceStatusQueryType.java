
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeviceStatusQueryType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DeviceStatusQueryType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="value" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceStatusType"/&gt;
 *         &lt;element name="searchType" type="{urn:v3.fne.webservices.operations.flexnet.com}enumSearchType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceStatusQueryType", propOrder = {
    "value",
    "searchType"
})
public class DeviceStatusQueryType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceStatusType value;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected EnumSearchType searchType;

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceStatusType }
     *     
     */
    public DeviceStatusType getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceStatusType }
     *     
     */
    public void setValue(DeviceStatusType value) {
        this.value = value;
    }

    /**
     * Gets the value of the searchType property.
     * 
     * @return
     *     possible object is
     *     {@link EnumSearchType }
     *     
     */
    public EnumSearchType getSearchType() {
        return searchType;
    }

    /**
     * Sets the value of the searchType property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumSearchType }
     *     
     */
    public void setSearchType(EnumSearchType value) {
        this.searchType = value;
    }

}
