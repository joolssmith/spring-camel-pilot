
package com.flexnet.operations.webservices.p;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for linkFailAddonDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="linkFailAddonDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failAddon" type="{urn:v1.fne.webservices.operations.flexnet.com}linkFailAddonDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linkFailAddonDataListType", propOrder = {
    "failAddon"
})
public class LinkFailAddonDataListType {

    protected List<LinkFailAddonDataType> failAddon;

    /**
     * Gets the value of the failAddon property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failAddon property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailAddon().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LinkFailAddonDataType }
     * 
     * 
     */
    public List<LinkFailAddonDataType> getFailAddon() {
        if (failAddon == null) {
            failAddon = new ArrayList<LinkFailAddonDataType>();
        }
        return this.failAddon;
    }

}
