
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for licenseTechnologyDetailsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="licenseTechnologyDetailsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="licenseTechnologyIdentifier" type="{urn:com.macrovision:flexnet/operations}licenseTechnologyIdentifierType"/&gt;
 *         &lt;element name="licenseGenerators" type="{urn:com.macrovision:flexnet/operations}licenseGeneratorsDetailsType"/&gt;
 *         &lt;element name="hostTypes" type="{urn:com.macrovision:flexnet/operations}hostTypeListType"/&gt;
 *         &lt;element name="state" type="{urn:com.macrovision:flexnet/operations}StateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "licenseTechnologyDetailsType", propOrder = {
    "licenseTechnologyIdentifier",
    "licenseGenerators",
    "hostTypes",
    "state"
})
public class LicenseTechnologyDetailsType {

    @XmlElement(required = true)
    protected LicenseTechnologyIdentifierType licenseTechnologyIdentifier;
    @XmlElement(required = true)
    protected LicenseGeneratorsDetailsType licenseGenerators;
    @XmlElement(required = true)
    protected HostTypeListType hostTypes;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;

    /**
     * Gets the value of the licenseTechnologyIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnologyIdentifier() {
        return licenseTechnologyIdentifier;
    }

    /**
     * Sets the value of the licenseTechnologyIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnologyIdentifier(LicenseTechnologyIdentifierType value) {
        this.licenseTechnologyIdentifier = value;
    }

    /**
     * Gets the value of the licenseGenerators property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseGeneratorsDetailsType }
     *     
     */
    public LicenseGeneratorsDetailsType getLicenseGenerators() {
        return licenseGenerators;
    }

    /**
     * Sets the value of the licenseGenerators property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseGeneratorsDetailsType }
     *     
     */
    public void setLicenseGenerators(LicenseGeneratorsDetailsType value) {
        this.licenseGenerators = value;
    }

    /**
     * Gets the value of the hostTypes property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypeListType }
     *     
     */
    public HostTypeListType getHostTypes() {
        return hostTypes;
    }

    /**
     * Sets the value of the hostTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypeListType }
     *     
     */
    public void setHostTypes(HostTypeListType value) {
        this.hostTypes = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

}
