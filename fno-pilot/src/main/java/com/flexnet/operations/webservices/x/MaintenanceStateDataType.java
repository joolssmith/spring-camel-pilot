
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maintenanceStateDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maintenanceStateDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="maintenanceIdentifier" type="{urn:v2.webservices.operations.flexnet.com}maintenanceIdentifierType"/&gt;
 *         &lt;element name="stateToSet" type="{urn:v2.webservices.operations.flexnet.com}StateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maintenanceStateDataType", propOrder = {
    "maintenanceIdentifier",
    "stateToSet"
})
public class MaintenanceStateDataType {

    @XmlElement(required = true)
    protected MaintenanceIdentifierType maintenanceIdentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType stateToSet;

    /**
     * Gets the value of the maintenanceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public MaintenanceIdentifierType getMaintenanceIdentifier() {
        return maintenanceIdentifier;
    }

    /**
     * Sets the value of the maintenanceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public void setMaintenanceIdentifier(MaintenanceIdentifierType value) {
        this.maintenanceIdentifier = value;
    }

    /**
     * Gets the value of the stateToSet property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getStateToSet() {
        return stateToSet;
    }

    /**
     * Sets the value of the stateToSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setStateToSet(StateType value) {
        this.stateToSet = value;
    }

}
