
package com.flexnet.operations.webservices.p;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for generateCloneDetectionReportResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generateCloneDetectionReportResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.fne.webservices.operations.flexnet.com}OpsEmbeddedStatusInfoType"/&gt;
 *         &lt;element name="cloneSuspects"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="cloneSuspect" type="{urn:v1.fne.webservices.operations.flexnet.com}cloneSuspect" maxOccurs="unbounded"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generateCloneDetectionReportResponse", propOrder = {
    "statusInfo",
    "cloneSuspects"
})
public class GenerateCloneDetectionReportResponse {

    @XmlElement(required = true)
    protected OpsEmbeddedStatusInfoType statusInfo;
    @XmlElement(required = true, nillable = true)
    protected GenerateCloneDetectionReportResponse.CloneSuspects cloneSuspects;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public OpsEmbeddedStatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpsEmbeddedStatusInfoType }
     *     
     */
    public void setStatusInfo(OpsEmbeddedStatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the cloneSuspects property.
     * 
     * @return
     *     possible object is
     *     {@link GenerateCloneDetectionReportResponse.CloneSuspects }
     *     
     */
    public GenerateCloneDetectionReportResponse.CloneSuspects getCloneSuspects() {
        return cloneSuspects;
    }

    /**
     * Sets the value of the cloneSuspects property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenerateCloneDetectionReportResponse.CloneSuspects }
     *     
     */
    public void setCloneSuspects(GenerateCloneDetectionReportResponse.CloneSuspects value) {
        this.cloneSuspects = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="cloneSuspect" type="{urn:v1.fne.webservices.operations.flexnet.com}cloneSuspect" maxOccurs="unbounded"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cloneSuspect"
    })
    public static class CloneSuspects {

        @XmlElement(required = true)
        protected List<CloneSuspect> cloneSuspect;

        /**
         * Gets the value of the cloneSuspect property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the cloneSuspect property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCloneSuspect().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link CloneSuspect }
         * 
         * 
         */
        public List<CloneSuspect> getCloneSuspect() {
            if (cloneSuspect == null) {
                cloneSuspect = new ArrayList<CloneSuspect>();
            }
            return this.cloneSuspect;
        }

    }

}
