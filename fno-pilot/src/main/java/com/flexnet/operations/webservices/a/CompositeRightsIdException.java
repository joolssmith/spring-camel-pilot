
package com.flexnet.operations.webservices.a;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CompositeRightsIdException complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CompositeRightsIdException"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://producersuite.flexerasoftware.com/EntitlementService/}RightsIdProblem"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Errors" type="{http://producersuite.flexerasoftware.com/EntitlementService/}RightsIdProblem" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CompositeRightsIdException", propOrder = {
    "errors"
})
public class CompositeRightsIdException
    extends RightsIdProblem
{

    @XmlElement(name = "Errors", required = true, nillable = true)
    protected List<RightsIdProblem> errors;

    /**
     * Gets the value of the errors property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errors property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrors().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RightsIdProblem }
     * 
     * 
     */
    public List<RightsIdProblem> getErrors() {
        if (errors == null) {
            errors = new ArrayList<RightsIdProblem>();
        }
        return this.errors;
    }

}
