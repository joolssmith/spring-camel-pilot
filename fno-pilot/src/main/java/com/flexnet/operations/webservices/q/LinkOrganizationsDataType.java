
package com.flexnet.operations.webservices.q;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for linkOrganizationsDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="linkOrganizationsDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentOrg" type="{urn:v1.webservices.operations.flexnet.com}organizationIdentifierType"/&gt;
 *         &lt;element name="subOrg" type="{urn:v1.webservices.operations.flexnet.com}organizationIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linkOrganizationsDataType", propOrder = {
    "parentOrg",
    "subOrg"
})
public class LinkOrganizationsDataType {

    @XmlElement(required = true)
    protected OrganizationIdentifierType parentOrg;
    @XmlElement(required = true)
    protected OrganizationIdentifierType subOrg;

    /**
     * Gets the value of the parentOrg property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getParentOrg() {
        return parentOrg;
    }

    /**
     * Sets the value of the parentOrg property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setParentOrg(OrganizationIdentifierType value) {
        this.parentOrg = value;
    }

    /**
     * Gets the value of the subOrg property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public OrganizationIdentifierType getSubOrg() {
        return subOrg;
    }

    /**
     * Sets the value of the subOrg property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationIdentifierType }
     *     
     */
    public void setSubOrg(OrganizationIdentifierType value) {
        this.subOrg = value;
    }

}
