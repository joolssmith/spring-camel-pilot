
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDevicesParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDevicesParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="alias" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceIdType" type="{urn:v3.fne.webservices.operations.flexnet.com}DeviceIdTypeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="parentId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="soldToAcctId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{urn:v3.fne.webservices.operations.flexnet.com}DeviceStateQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnActivationId" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnProductName" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="addOnProductVersion" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="featureName" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *         &lt;element name="isServer" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="deviceTypes" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceTypeList" minOccurs="0"/&gt;
 *         &lt;element name="userString" type="{urn:v3.fne.webservices.operations.flexnet.com}SimpleQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDevicesParametersType", propOrder = {
    "alias",
    "deviceId",
    "deviceIdType",
    "parentId",
    "hostTypeName",
    "soldTo",
    "soldToAcctId",
    "description",
    "status",
    "addOnActivationId",
    "addOnProductName",
    "addOnProductVersion",
    "featureName",
    "isServer",
    "deviceTypes",
    "userString"
})
public class GetDevicesParametersType {

    protected SimpleQueryType alias;
    protected SimpleQueryType deviceId;
    protected DeviceIdTypeQueryType deviceIdType;
    protected SimpleQueryType parentId;
    protected SimpleQueryType hostTypeName;
    protected SimpleQueryType soldTo;
    protected SimpleQueryType soldToAcctId;
    protected SimpleQueryType description;
    protected DeviceStateQueryType status;
    protected SimpleQueryType addOnActivationId;
    protected SimpleQueryType addOnProductName;
    protected SimpleQueryType addOnProductVersion;
    protected SimpleQueryType featureName;
    protected Boolean isServer;
    protected DeviceTypeList deviceTypes;
    protected SimpleQueryType userString;

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAlias(SimpleQueryType value) {
        this.alias = value;
    }

    /**
     * Gets the value of the deviceId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDeviceId() {
        return deviceId;
    }

    /**
     * Sets the value of the deviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDeviceId(SimpleQueryType value) {
        this.deviceId = value;
    }

    /**
     * Gets the value of the deviceIdType property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public DeviceIdTypeQueryType getDeviceIdType() {
        return deviceIdType;
    }

    /**
     * Sets the value of the deviceIdType property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdTypeQueryType }
     *     
     */
    public void setDeviceIdType(DeviceIdTypeQueryType value) {
        this.deviceIdType = value;
    }

    /**
     * Gets the value of the parentId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getParentId() {
        return parentId;
    }

    /**
     * Sets the value of the parentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setParentId(SimpleQueryType value) {
        this.parentId = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setHostTypeName(SimpleQueryType value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldTo(SimpleQueryType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the soldToAcctId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getSoldToAcctId() {
        return soldToAcctId;
    }

    /**
     * Sets the value of the soldToAcctId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setSoldToAcctId(SimpleQueryType value) {
        this.soldToAcctId = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setDescription(SimpleQueryType value) {
        this.description = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceStateQueryType }
     *     
     */
    public DeviceStateQueryType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceStateQueryType }
     *     
     */
    public void setStatus(DeviceStateQueryType value) {
        this.status = value;
    }

    /**
     * Gets the value of the addOnActivationId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnActivationId() {
        return addOnActivationId;
    }

    /**
     * Sets the value of the addOnActivationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnActivationId(SimpleQueryType value) {
        this.addOnActivationId = value;
    }

    /**
     * Gets the value of the addOnProductName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnProductName() {
        return addOnProductName;
    }

    /**
     * Sets the value of the addOnProductName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnProductName(SimpleQueryType value) {
        this.addOnProductName = value;
    }

    /**
     * Gets the value of the addOnProductVersion property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getAddOnProductVersion() {
        return addOnProductVersion;
    }

    /**
     * Sets the value of the addOnProductVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setAddOnProductVersion(SimpleQueryType value) {
        this.addOnProductVersion = value;
    }

    /**
     * Gets the value of the featureName property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getFeatureName() {
        return featureName;
    }

    /**
     * Sets the value of the featureName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setFeatureName(SimpleQueryType value) {
        this.featureName = value;
    }

    /**
     * Gets the value of the isServer property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsServer() {
        return isServer;
    }

    /**
     * Sets the value of the isServer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsServer(Boolean value) {
        this.isServer = value;
    }

    /**
     * Gets the value of the deviceTypes property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceTypeList }
     *     
     */
    public DeviceTypeList getDeviceTypes() {
        return deviceTypes;
    }

    /**
     * Sets the value of the deviceTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceTypeList }
     *     
     */
    public void setDeviceTypes(DeviceTypeList value) {
        this.deviceTypes = value;
    }

    /**
     * Gets the value of the userString property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getUserString() {
        return userString;
    }

    /**
     * Sets the value of the userString property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setUserString(SimpleQueryType value) {
        this.userString = value;
    }

}
