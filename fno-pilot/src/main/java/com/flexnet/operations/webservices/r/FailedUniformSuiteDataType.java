
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUniformSuiteDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUniformSuiteDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uniformSuite" type="{urn:v1.webservices.operations.flexnet.com}createUniformSuiteDataType" minOccurs="0"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUniformSuiteDataType", propOrder = {
    "uniformSuite",
    "reason"
})
public class FailedUniformSuiteDataType {

    protected CreateUniformSuiteDataType uniformSuite;
    protected String reason;

    /**
     * Gets the value of the uniformSuite property.
     * 
     * @return
     *     possible object is
     *     {@link CreateUniformSuiteDataType }
     *     
     */
    public CreateUniformSuiteDataType getUniformSuite() {
        return uniformSuite;
    }

    /**
     * Sets the value of the uniformSuite property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateUniformSuiteDataType }
     *     
     */
    public void setUniformSuite(CreateUniformSuiteDataType value) {
        this.uniformSuite = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
