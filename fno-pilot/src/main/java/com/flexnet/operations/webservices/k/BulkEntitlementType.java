
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BulkEntitlementType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="BulkEntitlementType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="UPGRADE"/&gt;
 *     &lt;enumeration value="UPSELL"/&gt;
 *     &lt;enumeration value="RENEWAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "BulkEntitlementType")
@XmlEnum
public enum BulkEntitlementType {

    UPGRADE,
    UPSELL,
    RENEWAL;

    public String value() {
        return name();
    }

    public static BulkEntitlementType fromValue(String v) {
        return valueOf(v);
    }

}
