
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceServedStatusType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="deviceServedStatusType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="NORMAL"/&gt;
 *     &lt;enumeration value="RETURNED"/&gt;
 *     &lt;enumeration value="EXPIRED"/&gt;
 *     &lt;enumeration value="DROPPED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "deviceServedStatusType")
@XmlEnum
public enum DeviceServedStatusType {

    NORMAL,
    RETURNED,
    EXPIRED,
    DROPPED;

    public String value() {
        return name();
    }

    public static DeviceServedStatusType fromValue(String v) {
        return valueOf(v);
    }

}
