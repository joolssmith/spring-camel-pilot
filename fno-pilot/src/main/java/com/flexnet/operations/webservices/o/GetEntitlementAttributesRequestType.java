
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getEntitlementAttributesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getEntitlementAttributesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="licenseModelIdentifier" type="{urn:v1.webservices.operations.flexnet.com}licenseModelIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEntitlementAttributesRequestType", propOrder = {
    "licenseModelIdentifier"
})
public class GetEntitlementAttributesRequestType {

    @XmlElement(required = true)
    protected LicenseModelIdentifierType licenseModelIdentifier;

    /**
     * Gets the value of the licenseModelIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public LicenseModelIdentifierType getLicenseModelIdentifier() {
        return licenseModelIdentifier;
    }

    /**
     * Sets the value of the licenseModelIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelIdentifierType }
     *     
     */
    public void setLicenseModelIdentifier(LicenseModelIdentifierType value) {
        this.licenseModelIdentifier = value;
    }

}
