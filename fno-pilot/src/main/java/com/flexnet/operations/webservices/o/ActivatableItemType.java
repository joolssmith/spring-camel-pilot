
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivatableItemType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ActivatableItemType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="WEBREGKEY"/&gt;
 *     &lt;enumeration value="LINEITEM"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ActivatableItemType")
@XmlEnum
public enum ActivatableItemType {

    WEBREGKEY,
    LINEITEM;

    public String value() {
        return name();
    }

    public static ActivatableItemType fromValue(String v) {
        return valueOf(v);
    }

}
