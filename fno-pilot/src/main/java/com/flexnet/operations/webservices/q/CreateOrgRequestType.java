
package com.flexnet.operations.webservices.q;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createOrgRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createOrgRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="organization" type="{urn:v1.webservices.operations.flexnet.com}organizationDataType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="opType" type="{urn:v1.webservices.operations.flexnet.com}CreateOrUpdateOperationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createOrgRequestType", propOrder = {
    "organization",
    "opType"
})
public class CreateOrgRequestType {

    @XmlElement(required = true)
    protected List<OrganizationDataType> organization;
    @XmlSchemaType(name = "NMTOKEN")
    protected CreateOrUpdateOperationType opType;

    /**
     * Gets the value of the organization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the organization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOrganization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OrganizationDataType }
     * 
     * 
     */
    public List<OrganizationDataType> getOrganization() {
        if (organization == null) {
            organization = new ArrayList<OrganizationDataType>();
        }
        return this.organization;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link CreateOrUpdateOperationType }
     *     
     */
    public CreateOrUpdateOperationType getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateOrUpdateOperationType }
     *     
     */
    public void setOpType(CreateOrUpdateOperationType value) {
        this.opType = value;
    }

}
