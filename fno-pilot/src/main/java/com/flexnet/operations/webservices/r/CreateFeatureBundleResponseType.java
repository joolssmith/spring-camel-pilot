
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createFeatureBundleResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createFeatureBundleResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="failedData" type="{urn:v1.webservices.operations.flexnet.com}failedFeatureBundleDataListType" minOccurs="0"/&gt;
 *         &lt;element name="responseData" type="{urn:v1.webservices.operations.flexnet.com}createdFeatureBundleDataListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createFeatureBundleResponseType", propOrder = {
    "statusInfo",
    "failedData",
    "responseData"
})
public class CreateFeatureBundleResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected FailedFeatureBundleDataListType failedData;
    protected CreatedFeatureBundleDataListType responseData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedFeatureBundleDataListType }
     *     
     */
    public FailedFeatureBundleDataListType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedFeatureBundleDataListType }
     *     
     */
    public void setFailedData(FailedFeatureBundleDataListType value) {
        this.failedData = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link CreatedFeatureBundleDataListType }
     *     
     */
    public CreatedFeatureBundleDataListType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreatedFeatureBundleDataListType }
     *     
     */
    public void setResponseData(CreatedFeatureBundleDataListType value) {
        this.responseData = value;
    }

}
