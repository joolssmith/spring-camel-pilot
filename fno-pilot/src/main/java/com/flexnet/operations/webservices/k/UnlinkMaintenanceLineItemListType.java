
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for unlinkMaintenanceLineItemListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="unlinkMaintenanceLineItemListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="unlinkMaintenanceLineItem" type="{urn:com.macrovision:flexnet/operations}unlinkMaintenanceLineItemDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unlinkMaintenanceLineItemListType", propOrder = {
    "unlinkMaintenanceLineItem"
})
public class UnlinkMaintenanceLineItemListType {

    @XmlElement(required = true)
    protected List<UnlinkMaintenanceLineItemDataType> unlinkMaintenanceLineItem;

    /**
     * Gets the value of the unlinkMaintenanceLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the unlinkMaintenanceLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnlinkMaintenanceLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UnlinkMaintenanceLineItemDataType }
     * 
     * 
     */
    public List<UnlinkMaintenanceLineItemDataType> getUnlinkMaintenanceLineItem() {
        if (unlinkMaintenanceLineItem == null) {
            unlinkMaintenanceLineItem = new ArrayList<UnlinkMaintenanceLineItemDataType>();
        }
        return this.unlinkMaintenanceLineItem;
    }

}
