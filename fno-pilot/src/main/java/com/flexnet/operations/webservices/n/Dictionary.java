
package com.flexnet.operations.webservices.n;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Dictionary complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Dictionary"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Entries" type="{urn:v1.webservices.operations.flexnet.com}DictionaryEntriesCollection" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Dictionary", propOrder = {
    "entries"
})
public class Dictionary {

    @XmlElementRef(name = "Entries", namespace = "urn:v1.webservices.operations.flexnet.com", type = JAXBElement.class, required = false)
    protected JAXBElement<DictionaryEntriesCollection> entries;

    /**
     * Gets the value of the entries property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DictionaryEntriesCollection }{@code >}
     *     
     */
    public JAXBElement<DictionaryEntriesCollection> getEntries() {
        return entries;
    }

    /**
     * Sets the value of the entries property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DictionaryEntriesCollection }{@code >}
     *     
     */
    public void setEntries(JAXBElement<DictionaryEntriesCollection> value) {
        this.entries = value;
    }

}
