
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdEntitlementLifeCycleDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdEntitlementLifeCycleDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementRecordRefNo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="parentEntitlementIdentifier" type="{urn:v3.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="createdLineItemData" type="{urn:v3.webservices.operations.flexnet.com}lifeCycleLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdEntitlementLifeCycleDataType", propOrder = {
    "entitlementRecordRefNo",
    "parentEntitlementIdentifier",
    "createdLineItemData"
})
public class CreatedEntitlementLifeCycleDataType {

    @XmlElement(required = true)
    protected String entitlementRecordRefNo;
    @XmlElement(required = true)
    protected EntitlementIdentifierType parentEntitlementIdentifier;
    protected List<LifeCycleLineItemDataType> createdLineItemData;

    /**
     * Gets the value of the entitlementRecordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementRecordRefNo() {
        return entitlementRecordRefNo;
    }

    /**
     * Sets the value of the entitlementRecordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementRecordRefNo(String value) {
        this.entitlementRecordRefNo = value;
    }

    /**
     * Gets the value of the parentEntitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getParentEntitlementIdentifier() {
        return parentEntitlementIdentifier;
    }

    /**
     * Sets the value of the parentEntitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setParentEntitlementIdentifier(EntitlementIdentifierType value) {
        this.parentEntitlementIdentifier = value;
    }

    /**
     * Gets the value of the createdLineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdLineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LifeCycleLineItemDataType }
     * 
     * 
     */
    public List<LifeCycleLineItemDataType> getCreatedLineItemData() {
        if (createdLineItemData == null) {
            createdLineItemData = new ArrayList<LifeCycleLineItemDataType>();
        }
        return this.createdLineItemData;
    }

}
