
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteFeatureDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteFeatureDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureIdentifier" type="{urn:v2.webservices.operations.flexnet.com}featureIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteFeatureDataType", propOrder = {
    "featureIdentifier"
})
public class DeleteFeatureDataType {

    @XmlElement(required = true)
    protected FeatureIdentifierType featureIdentifier;

    /**
     * Gets the value of the featureIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureIdentifierType }
     *     
     */
    public FeatureIdentifierType getFeatureIdentifier() {
        return featureIdentifier;
    }

    /**
     * Sets the value of the featureIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureIdentifierType }
     *     
     */
    public void setFeatureIdentifier(FeatureIdentifierType value) {
        this.featureIdentifier = value;
    }

}
