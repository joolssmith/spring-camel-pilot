
package com.flexnet.operations.webservices.v;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteAddonLineItemsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteAddonLineItemsRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestList" type="{urn:v2.fne.webservices.operations.flexnet.com}deleteAddonLineItemDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteAddonLineItemsRequestType", propOrder = {
    "requestList"
})
public class DeleteAddonLineItemsRequestType {

    @XmlElement(required = true)
    protected List<DeleteAddonLineItemDataType> requestList;

    /**
     * Gets the value of the requestList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the requestList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRequestList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DeleteAddonLineItemDataType }
     * 
     * 
     */
    public List<DeleteAddonLineItemDataType> getRequestList() {
        if (requestList == null) {
            requestList = new ArrayList<DeleteAddonLineItemDataType>();
        }
        return this.requestList;
    }

}
