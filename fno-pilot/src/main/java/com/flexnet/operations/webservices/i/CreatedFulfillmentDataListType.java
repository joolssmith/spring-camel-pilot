
package com.flexnet.operations.webservices.i;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdFulfillmentDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdFulfillmentDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="createdFulfillment" type="{urn:com.macrovision:flexnet/operations}createdFulfillmentDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="verifiedFulfillment" type="{urn:com.macrovision:flexnet/operations}verifiedFulfillmentDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdFulfillmentDataListType", propOrder = {
    "createdFulfillment",
    "verifiedFulfillment"
})
public class CreatedFulfillmentDataListType {

    protected List<CreatedFulfillmentDataType> createdFulfillment;
    protected List<VerifiedFulfillmentDataType> verifiedFulfillment;

    /**
     * Gets the value of the createdFulfillment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdFulfillment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedFulfillment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatedFulfillmentDataType }
     * 
     * 
     */
    public List<CreatedFulfillmentDataType> getCreatedFulfillment() {
        if (createdFulfillment == null) {
            createdFulfillment = new ArrayList<CreatedFulfillmentDataType>();
        }
        return this.createdFulfillment;
    }

    /**
     * Gets the value of the verifiedFulfillment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the verifiedFulfillment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVerifiedFulfillment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VerifiedFulfillmentDataType }
     * 
     * 
     */
    public List<VerifiedFulfillmentDataType> getVerifiedFulfillment() {
        if (verifiedFulfillment == null) {
            verifiedFulfillment = new ArrayList<VerifiedFulfillmentDataType>();
        }
        return this.verifiedFulfillment;
    }

}
