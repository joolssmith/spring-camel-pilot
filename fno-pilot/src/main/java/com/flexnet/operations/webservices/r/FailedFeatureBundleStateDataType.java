
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedFeatureBundleStateDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedFeatureBundleStateDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundle" type="{urn:v1.webservices.operations.flexnet.com}featureBundleStateDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedFeatureBundleStateDataType", propOrder = {
    "featureBundle",
    "reason"
})
public class FailedFeatureBundleStateDataType {

    @XmlElement(required = true)
    protected FeatureBundleStateDataType featureBundle;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the featureBundle property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleStateDataType }
     *     
     */
    public FeatureBundleStateDataType getFeatureBundle() {
        return featureBundle;
    }

    /**
     * Sets the value of the featureBundle property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleStateDataType }
     *     
     */
    public void setFeatureBundle(FeatureBundleStateDataType value) {
        this.featureBundle = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
