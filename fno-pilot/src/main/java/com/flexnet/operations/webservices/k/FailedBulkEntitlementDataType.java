
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedBulkEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedBulkEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlement" type="{urn:com.macrovision:flexnet/operations}createBulkEntitlementDataType" minOccurs="0"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedBulkEntitlementDataType", propOrder = {
    "bulkEntitlement",
    "reason"
})
public class FailedBulkEntitlementDataType {

    protected CreateBulkEntitlementDataType bulkEntitlement;
    protected String reason;

    /**
     * Gets the value of the bulkEntitlement property.
     * 
     * @return
     *     possible object is
     *     {@link CreateBulkEntitlementDataType }
     *     
     */
    public CreateBulkEntitlementDataType getBulkEntitlement() {
        return bulkEntitlement;
    }

    /**
     * Sets the value of the bulkEntitlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link CreateBulkEntitlementDataType }
     *     
     */
    public void setBulkEntitlement(CreateBulkEntitlementDataType value) {
        this.bulkEntitlement = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
