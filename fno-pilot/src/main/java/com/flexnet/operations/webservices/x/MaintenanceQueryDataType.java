
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maintenanceQueryDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maintenanceQueryDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="uniqueId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="maintenanceName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v2.webservices.operations.flexnet.com}StateType"/&gt;
 *         &lt;element name="allowUpgrades" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowUpsells" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowRenewals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="partNumbers" type="{urn:v2.webservices.operations.flexnet.com}partNumbersSimpleListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maintenanceQueryDataType", propOrder = {
    "uniqueId",
    "maintenanceName",
    "version",
    "description",
    "state",
    "allowUpgrades",
    "allowUpsells",
    "allowRenewals",
    "partNumbers"
})
public class MaintenanceQueryDataType {

    @XmlElement(required = true)
    protected String uniqueId;
    @XmlElement(required = true)
    protected String maintenanceName;
    protected String version;
    protected String description;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    protected Boolean allowUpgrades;
    protected Boolean allowUpsells;
    protected Boolean allowRenewals;
    protected PartNumbersSimpleListType partNumbers;

    /**
     * Gets the value of the uniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Sets the value of the uniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUniqueId(String value) {
        this.uniqueId = value;
    }

    /**
     * Gets the value of the maintenanceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaintenanceName() {
        return maintenanceName;
    }

    /**
     * Sets the value of the maintenanceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaintenanceName(String value) {
        this.maintenanceName = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the allowUpgrades property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUpgrades() {
        return allowUpgrades;
    }

    /**
     * Sets the value of the allowUpgrades property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUpgrades(Boolean value) {
        this.allowUpgrades = value;
    }

    /**
     * Gets the value of the allowUpsells property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUpsells() {
        return allowUpsells;
    }

    /**
     * Sets the value of the allowUpsells property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUpsells(Boolean value) {
        this.allowUpsells = value;
    }

    /**
     * Gets the value of the allowRenewals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowRenewals() {
        return allowRenewals;
    }

    /**
     * Sets the value of the allowRenewals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowRenewals(Boolean value) {
        this.allowRenewals = value;
    }

    /**
     * Gets the value of the partNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link PartNumbersSimpleListType }
     *     
     */
    public PartNumbersSimpleListType getPartNumbers() {
        return partNumbers;
    }

    /**
     * Sets the value of the partNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link PartNumbersSimpleListType }
     *     
     */
    public void setPartNumbers(PartNumbersSimpleListType value) {
        this.partNumbers = value;
    }

}
