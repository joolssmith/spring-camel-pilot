
package com.flexnet.operations.webservices.b;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Usage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Usage"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productLine" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="meter" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="usageUnitName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="usageSinceReset" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="entitled" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="overageSinceStatement" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="overageSinceReset" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="percentSinceReset" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="resetIntervalUnitName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="resetInterval" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="period" type="{urn:com.macrovision:flexnet/operations}Period"/&gt;
 *         &lt;element name="entitledScaled" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="friendlyName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="gracePeriod" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="gracePeriodUnitName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="overageSinceResetScaled" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="overageSinceStatementScaled" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="percentSinceStatement" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="reconciliationTimeZone" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="scale" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="statementInterval" type="{http://www.w3.org/2001/XMLSchema}long"/&gt;
 *         &lt;element name="statementIntervalUnitName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="usageSinceResetScaled" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *         &lt;element name="usageSinceStatement" type="{http://www.w3.org/2001/XMLSchema}double"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Usage", propOrder = {
    "productLine",
    "meter",
    "usageUnitName",
    "usageSinceReset",
    "entitled",
    "overageSinceStatement",
    "overageSinceReset",
    "percentSinceReset",
    "resetIntervalUnitName",
    "resetInterval",
    "period",
    "entitledScaled",
    "friendlyName",
    "gracePeriod",
    "gracePeriodUnitName",
    "overageSinceResetScaled",
    "overageSinceStatementScaled",
    "percentSinceStatement",
    "reconciliationTimeZone",
    "scale",
    "statementInterval",
    "statementIntervalUnitName",
    "usageSinceResetScaled",
    "usageSinceStatement"
})
public class Usage {

    @XmlElement(required = true)
    protected String productLine;
    @XmlElement(required = true)
    protected String meter;
    @XmlElement(required = true)
    protected String usageUnitName;
    protected double usageSinceReset;
    protected double entitled;
    protected double overageSinceStatement;
    protected double overageSinceReset;
    protected double percentSinceReset;
    @XmlElement(required = true)
    protected String resetIntervalUnitName;
    protected long resetInterval;
    @XmlElement(required = true)
    protected Period period;
    protected double entitledScaled;
    @XmlElement(required = true)
    protected String friendlyName;
    protected long gracePeriod;
    @XmlElement(required = true)
    protected String gracePeriodUnitName;
    protected double overageSinceResetScaled;
    protected double overageSinceStatementScaled;
    protected double percentSinceStatement;
    @XmlElement(required = true)
    protected String reconciliationTimeZone;
    protected double scale;
    protected long statementInterval;
    @XmlElement(required = true)
    protected String statementIntervalUnitName;
    protected double usageSinceResetScaled;
    protected double usageSinceStatement;

    /**
     * Gets the value of the productLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductLine() {
        return productLine;
    }

    /**
     * Sets the value of the productLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductLine(String value) {
        this.productLine = value;
    }

    /**
     * Gets the value of the meter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeter() {
        return meter;
    }

    /**
     * Sets the value of the meter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeter(String value) {
        this.meter = value;
    }

    /**
     * Gets the value of the usageUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsageUnitName() {
        return usageUnitName;
    }

    /**
     * Sets the value of the usageUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsageUnitName(String value) {
        this.usageUnitName = value;
    }

    /**
     * Gets the value of the usageSinceReset property.
     * 
     */
    public double getUsageSinceReset() {
        return usageSinceReset;
    }

    /**
     * Sets the value of the usageSinceReset property.
     * 
     */
    public void setUsageSinceReset(double value) {
        this.usageSinceReset = value;
    }

    /**
     * Gets the value of the entitled property.
     * 
     */
    public double getEntitled() {
        return entitled;
    }

    /**
     * Sets the value of the entitled property.
     * 
     */
    public void setEntitled(double value) {
        this.entitled = value;
    }

    /**
     * Gets the value of the overageSinceStatement property.
     * 
     */
    public double getOverageSinceStatement() {
        return overageSinceStatement;
    }

    /**
     * Sets the value of the overageSinceStatement property.
     * 
     */
    public void setOverageSinceStatement(double value) {
        this.overageSinceStatement = value;
    }

    /**
     * Gets the value of the overageSinceReset property.
     * 
     */
    public double getOverageSinceReset() {
        return overageSinceReset;
    }

    /**
     * Sets the value of the overageSinceReset property.
     * 
     */
    public void setOverageSinceReset(double value) {
        this.overageSinceReset = value;
    }

    /**
     * Gets the value of the percentSinceReset property.
     * 
     */
    public double getPercentSinceReset() {
        return percentSinceReset;
    }

    /**
     * Sets the value of the percentSinceReset property.
     * 
     */
    public void setPercentSinceReset(double value) {
        this.percentSinceReset = value;
    }

    /**
     * Gets the value of the resetIntervalUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResetIntervalUnitName() {
        return resetIntervalUnitName;
    }

    /**
     * Sets the value of the resetIntervalUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResetIntervalUnitName(String value) {
        this.resetIntervalUnitName = value;
    }

    /**
     * Gets the value of the resetInterval property.
     * 
     */
    public long getResetInterval() {
        return resetInterval;
    }

    /**
     * Sets the value of the resetInterval property.
     * 
     */
    public void setResetInterval(long value) {
        this.resetInterval = value;
    }

    /**
     * Gets the value of the period property.
     * 
     * @return
     *     possible object is
     *     {@link Period }
     *     
     */
    public Period getPeriod() {
        return period;
    }

    /**
     * Sets the value of the period property.
     * 
     * @param value
     *     allowed object is
     *     {@link Period }
     *     
     */
    public void setPeriod(Period value) {
        this.period = value;
    }

    /**
     * Gets the value of the entitledScaled property.
     * 
     */
    public double getEntitledScaled() {
        return entitledScaled;
    }

    /**
     * Sets the value of the entitledScaled property.
     * 
     */
    public void setEntitledScaled(double value) {
        this.entitledScaled = value;
    }

    /**
     * Gets the value of the friendlyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFriendlyName() {
        return friendlyName;
    }

    /**
     * Sets the value of the friendlyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFriendlyName(String value) {
        this.friendlyName = value;
    }

    /**
     * Gets the value of the gracePeriod property.
     * 
     */
    public long getGracePeriod() {
        return gracePeriod;
    }

    /**
     * Sets the value of the gracePeriod property.
     * 
     */
    public void setGracePeriod(long value) {
        this.gracePeriod = value;
    }

    /**
     * Gets the value of the gracePeriodUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGracePeriodUnitName() {
        return gracePeriodUnitName;
    }

    /**
     * Sets the value of the gracePeriodUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGracePeriodUnitName(String value) {
        this.gracePeriodUnitName = value;
    }

    /**
     * Gets the value of the overageSinceResetScaled property.
     * 
     */
    public double getOverageSinceResetScaled() {
        return overageSinceResetScaled;
    }

    /**
     * Sets the value of the overageSinceResetScaled property.
     * 
     */
    public void setOverageSinceResetScaled(double value) {
        this.overageSinceResetScaled = value;
    }

    /**
     * Gets the value of the overageSinceStatementScaled property.
     * 
     */
    public double getOverageSinceStatementScaled() {
        return overageSinceStatementScaled;
    }

    /**
     * Sets the value of the overageSinceStatementScaled property.
     * 
     */
    public void setOverageSinceStatementScaled(double value) {
        this.overageSinceStatementScaled = value;
    }

    /**
     * Gets the value of the percentSinceStatement property.
     * 
     */
    public double getPercentSinceStatement() {
        return percentSinceStatement;
    }

    /**
     * Sets the value of the percentSinceStatement property.
     * 
     */
    public void setPercentSinceStatement(double value) {
        this.percentSinceStatement = value;
    }

    /**
     * Gets the value of the reconciliationTimeZone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReconciliationTimeZone() {
        return reconciliationTimeZone;
    }

    /**
     * Sets the value of the reconciliationTimeZone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReconciliationTimeZone(String value) {
        this.reconciliationTimeZone = value;
    }

    /**
     * Gets the value of the scale property.
     * 
     */
    public double getScale() {
        return scale;
    }

    /**
     * Sets the value of the scale property.
     * 
     */
    public void setScale(double value) {
        this.scale = value;
    }

    /**
     * Gets the value of the statementInterval property.
     * 
     */
    public long getStatementInterval() {
        return statementInterval;
    }

    /**
     * Sets the value of the statementInterval property.
     * 
     */
    public void setStatementInterval(long value) {
        this.statementInterval = value;
    }

    /**
     * Gets the value of the statementIntervalUnitName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatementIntervalUnitName() {
        return statementIntervalUnitName;
    }

    /**
     * Sets the value of the statementIntervalUnitName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatementIntervalUnitName(String value) {
        this.statementIntervalUnitName = value;
    }

    /**
     * Gets the value of the usageSinceResetScaled property.
     * 
     */
    public double getUsageSinceResetScaled() {
        return usageSinceResetScaled;
    }

    /**
     * Sets the value of the usageSinceResetScaled property.
     * 
     */
    public void setUsageSinceResetScaled(double value) {
        this.usageSinceResetScaled = value;
    }

    /**
     * Gets the value of the usageSinceStatement property.
     * 
     */
    public double getUsageSinceStatement() {
        return usageSinceStatement;
    }

    /**
     * Sets the value of the usageSinceStatement property.
     * 
     */
    public void setUsageSinceStatement(double value) {
        this.usageSinceStatement = value;
    }

}
