
package com.flexnet.operations.webservices.o;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VersionDateOptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="VersionDateOptionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="DEFINE_NOW"/&gt;
 *     &lt;enumeration value="USE_START_DATE"/&gt;
 *     &lt;enumeration value="USE_ACTIVATION_DATE"/&gt;
 *     &lt;enumeration value="DEFINE_VERSION_STARTDATE_LATER"/&gt;
 *     &lt;enumeration value="DEFINE_VERSION_DATE_LATER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "VersionDateOptionType")
@XmlEnum
public enum VersionDateOptionType {

    DEFINE_NOW,
    USE_START_DATE,
    USE_ACTIVATION_DATE,
    DEFINE_VERSION_STARTDATE_LATER,
    DEFINE_VERSION_DATE_LATER;

    public String value() {
        return name();
    }

    public static VersionDateOptionType fromValue(String v) {
        return valueOf(v);
    }

}
