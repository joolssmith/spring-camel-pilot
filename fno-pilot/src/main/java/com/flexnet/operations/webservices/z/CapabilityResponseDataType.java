
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for capabilityResponseDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="capabilityResponseDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceIdentifier"/&gt;
 *         &lt;element name="addonLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/&gt;
 *         &lt;element name="bufferLicense" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "capabilityResponseDataType", propOrder = {
    "deviceIdentifier",
    "addonLicense",
    "bufferLicense"
})
public class CapabilityResponseDataType {

    @XmlElement(required = true)
    protected DeviceIdentifier deviceIdentifier;
    @XmlElement(required = true)
    protected byte[] addonLicense;
    protected Boolean bufferLicense;

    /**
     * Gets the value of the deviceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getDeviceIdentifier() {
        return deviceIdentifier;
    }

    /**
     * Sets the value of the deviceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setDeviceIdentifier(DeviceIdentifier value) {
        this.deviceIdentifier = value;
    }

    /**
     * Gets the value of the addonLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAddonLicense() {
        return addonLicense;
    }

    /**
     * Sets the value of the addonLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAddonLicense(byte[] value) {
        this.addonLicense = value;
    }

    /**
     * Gets the value of the bufferLicense property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isBufferLicense() {
        return bufferLicense;
    }

    /**
     * Sets the value of the bufferLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setBufferLicense(Boolean value) {
        this.bufferLicense = value;
    }

}
