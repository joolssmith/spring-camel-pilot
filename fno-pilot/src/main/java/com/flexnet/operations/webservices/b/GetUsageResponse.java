
package com.flexnet.operations.webservices.b;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfoType" type="{urn:com.macrovision:flexnet/operations}StatusInfoType"/&gt;
 *         &lt;element name="usages" type="{urn:com.macrovision:flexnet/operations}Usage" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "statusInfoType",
    "usages"
})
@XmlRootElement(name = "GetUsageResponse")
public class GetUsageResponse {

    @XmlElement(required = true)
    protected StatusInfoType statusInfoType;
    @XmlElement(required = true)
    protected List<Usage> usages;

    /**
     * Gets the value of the statusInfoType property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfoType() {
        return statusInfoType;
    }

    /**
     * Sets the value of the statusInfoType property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfoType(StatusInfoType value) {
        this.statusInfoType = value;
    }

    /**
     * Gets the value of the usages property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the usages property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUsages().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Usage }
     * 
     * 
     */
    public List<Usage> getUsages() {
        if (usages == null) {
            usages = new ArrayList<Usage>();
        }
        return this.usages;
    }

}
