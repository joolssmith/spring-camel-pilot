
package com.flexnet.operations.webservices.i;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hostIdDataSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hostIdDataSetType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hostIdData" type="{urn:com.macrovision:flexnet/operations}hostIdDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hostIdDataSetType", propOrder = {
    "hostIdData"
})
public class HostIdDataSetType {

    @XmlElement(required = true)
    protected List<HostIdDataType> hostIdData;

    /**
     * Gets the value of the hostIdData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hostIdData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHostIdData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HostIdDataType }
     * 
     * 
     */
    public List<HostIdDataType> getHostIdData() {
        if (hostIdData == null) {
            hostIdData = new ArrayList<HostIdDataType>();
        }
        return this.hostIdData;
    }

}
