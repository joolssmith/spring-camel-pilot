
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for usageSummaryTimesDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="usageSummaryTimesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="summaryTimeId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="summaryTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "usageSummaryTimesDataType", propOrder = {
    "summaryTimeId",
    "summaryTime"
})
public class UsageSummaryTimesDataType {

    @XmlElement(required = true)
    protected String summaryTimeId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar summaryTime;

    /**
     * Gets the value of the summaryTimeId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSummaryTimeId() {
        return summaryTimeId;
    }

    /**
     * Sets the value of the summaryTimeId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSummaryTimeId(String value) {
        this.summaryTimeId = value;
    }

    /**
     * Gets the value of the summaryTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSummaryTime() {
        return summaryTime;
    }

    /**
     * Sets the value of the summaryTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSummaryTime(XMLGregorianCalendar value) {
        this.summaryTime = value;
    }

}
