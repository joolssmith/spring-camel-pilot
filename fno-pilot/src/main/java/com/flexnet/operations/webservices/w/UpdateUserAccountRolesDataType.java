
package com.flexnet.operations.webservices.w;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateUserAccountRolesDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateUserAccountRolesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="account" type="{urn:v2.webservices.operations.flexnet.com}accountIdentifierType"/&gt;
 *         &lt;element name="roles" type="{urn:v2.webservices.operations.flexnet.com}updateUserRolesListType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateUserAccountRolesDataType", propOrder = {
    "account",
    "roles"
})
public class UpdateUserAccountRolesDataType {

    @XmlElement(required = true)
    protected AccountIdentifierType account;
    @XmlElement(required = true)
    protected UpdateUserRolesListType roles;

    /**
     * Gets the value of the account property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifierType }
     *     
     */
    public AccountIdentifierType getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifierType }
     *     
     */
    public void setAccount(AccountIdentifierType value) {
        this.account = value;
    }

    /**
     * Gets the value of the roles property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateUserRolesListType }
     *     
     */
    public UpdateUserRolesListType getRoles() {
        return roles;
    }

    /**
     * Sets the value of the roles property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateUserRolesListType }
     *     
     */
    public void setRoles(UpdateUserRolesListType value) {
        this.roles = value;
    }

}
