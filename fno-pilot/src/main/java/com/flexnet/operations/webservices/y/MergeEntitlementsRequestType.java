
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for mergeEntitlementsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="mergeEntitlementsRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="accountFrom" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="accountTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mergeUsers" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="mergeEntitlements" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mergeEntitlementsRequestType", propOrder = {
    "accountFrom",
    "accountTo",
    "mergeUsers",
    "mergeEntitlements"
})
public class MergeEntitlementsRequestType {

    @XmlElement(required = true)
    protected String accountFrom;
    @XmlElement(required = true)
    protected String accountTo;
    protected Boolean mergeUsers;
    protected Boolean mergeEntitlements;

    /**
     * Gets the value of the accountFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountFrom() {
        return accountFrom;
    }

    /**
     * Sets the value of the accountFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountFrom(String value) {
        this.accountFrom = value;
    }

    /**
     * Gets the value of the accountTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountTo() {
        return accountTo;
    }

    /**
     * Sets the value of the accountTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountTo(String value) {
        this.accountTo = value;
    }

    /**
     * Gets the value of the mergeUsers property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMergeUsers() {
        return mergeUsers;
    }

    /**
     * Sets the value of the mergeUsers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMergeUsers(Boolean value) {
        this.mergeUsers = value;
    }

    /**
     * Gets the value of the mergeEntitlements property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMergeEntitlements() {
        return mergeEntitlements;
    }

    /**
     * Sets the value of the mergeEntitlements property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMergeEntitlements(Boolean value) {
        this.mergeEntitlements = value;
    }

}
