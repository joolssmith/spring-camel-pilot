
package com.flexnet.operations.webservices.h;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedGeneratePrebuiltLicenseDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedGeneratePrebuiltLicenseDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedDevice" type="{urn:com.macrovision:flexnet/opsembedded}failedGeneratePrebuiltLicenseDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedGeneratePrebuiltLicenseDataListType", propOrder = {
    "failedDevice"
})
public class FailedGeneratePrebuiltLicenseDataListType {

    protected List<FailedGeneratePrebuiltLicenseDataType> failedDevice;

    /**
     * Gets the value of the failedDevice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedDevice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedDevice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedGeneratePrebuiltLicenseDataType }
     * 
     * 
     */
    public List<FailedGeneratePrebuiltLicenseDataType> getFailedDevice() {
        if (failedDevice == null) {
            failedDevice = new ArrayList<FailedGeneratePrebuiltLicenseDataType>();
        }
        return this.failedDevice;
    }

}
