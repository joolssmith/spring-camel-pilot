
package com.flexnet.operations.webservices.u;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getBulkEntitlementPropertiesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getBulkEntitlementPropertiesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlementSearchCriteria" type="{urn:v2.webservices.operations.flexnet.com}searchBulkEntitlementDataType"/&gt;
 *         &lt;element name="bulkEntitlementResponseConfig" type="{urn:v2.webservices.operations.flexnet.com}bulkEntitlementResponseConfigRequestType"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getBulkEntitlementPropertiesRequestType", propOrder = {
    "bulkEntitlementSearchCriteria",
    "bulkEntitlementResponseConfig",
    "batchSize",
    "pageNumber"
})
public class GetBulkEntitlementPropertiesRequestType {

    @XmlElement(required = true)
    protected SearchBulkEntitlementDataType bulkEntitlementSearchCriteria;
    @XmlElement(required = true)
    protected BulkEntitlementResponseConfigRequestType bulkEntitlementResponseConfig;
    @XmlElement(required = true)
    protected BigInteger batchSize;
    protected BigInteger pageNumber;

    /**
     * Gets the value of the bulkEntitlementSearchCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link SearchBulkEntitlementDataType }
     *     
     */
    public SearchBulkEntitlementDataType getBulkEntitlementSearchCriteria() {
        return bulkEntitlementSearchCriteria;
    }

    /**
     * Sets the value of the bulkEntitlementSearchCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchBulkEntitlementDataType }
     *     
     */
    public void setBulkEntitlementSearchCriteria(SearchBulkEntitlementDataType value) {
        this.bulkEntitlementSearchCriteria = value;
    }

    /**
     * Gets the value of the bulkEntitlementResponseConfig property.
     * 
     * @return
     *     possible object is
     *     {@link BulkEntitlementResponseConfigRequestType }
     *     
     */
    public BulkEntitlementResponseConfigRequestType getBulkEntitlementResponseConfig() {
        return bulkEntitlementResponseConfig;
    }

    /**
     * Sets the value of the bulkEntitlementResponseConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link BulkEntitlementResponseConfigRequestType }
     *     
     */
    public void setBulkEntitlementResponseConfig(BulkEntitlementResponseConfigRequestType value) {
        this.bulkEntitlementResponseConfig = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

}
