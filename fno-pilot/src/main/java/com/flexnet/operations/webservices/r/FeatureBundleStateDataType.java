
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for featureBundleStateDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="featureBundleStateDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundleIdentifier" type="{urn:v1.webservices.operations.flexnet.com}featureBundleIdentifierType"/&gt;
 *         &lt;element name="stateToSet" type="{urn:v1.webservices.operations.flexnet.com}StateType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "featureBundleStateDataType", propOrder = {
    "featureBundleIdentifier",
    "stateToSet"
})
public class FeatureBundleStateDataType {

    @XmlElement(required = true)
    protected FeatureBundleIdentifierType featureBundleIdentifier;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType stateToSet;

    /**
     * Gets the value of the featureBundleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public FeatureBundleIdentifierType getFeatureBundleIdentifier() {
        return featureBundleIdentifier;
    }

    /**
     * Sets the value of the featureBundleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public void setFeatureBundleIdentifier(FeatureBundleIdentifierType value) {
        this.featureBundleIdentifier = value;
    }

    /**
     * Gets the value of the stateToSet property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getStateToSet() {
        return stateToSet;
    }

    /**
     * Sets the value of the stateToSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setStateToSet(StateType value) {
        this.stateToSet = value;
    }

}
