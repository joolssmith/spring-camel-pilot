
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StateChangeEventType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StateChangeEventType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="EVENT_CREATED"/&gt;
 *     &lt;enumeration value="EVENT_PROPERTY_CHANGED"/&gt;
 *     &lt;enumeration value="EVENT_STATE_CHANGED"/&gt;
 *     &lt;enumeration value="EVENT_TRANSFERRED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "StateChangeEventType")
@XmlEnum
public enum StateChangeEventType {

    EVENT_CREATED,
    EVENT_PROPERTY_CHANGED,
    EVENT_STATE_CHANGED,
    EVENT_TRANSFERRED;

    public String value() {
        return name();
    }

    public static StateChangeEventType fromValue(String v) {
        return valueOf(v);
    }

}
