
package com.flexnet.operations.webservices.p;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceIdTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="deviceIdTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="UNKNOWN"/&gt;
 *     &lt;enumeration value="STRING"/&gt;
 *     &lt;enumeration value="ETHERNET"/&gt;
 *     &lt;enumeration value="FLEXID9"/&gt;
 *     &lt;enumeration value="FLEXID10"/&gt;
 *     &lt;enumeration value="VM_UUID"/&gt;
 *     &lt;enumeration value="INTERNET"/&gt;
 *     &lt;enumeration value="INTERNET6"/&gt;
 *     &lt;enumeration value="USER"/&gt;
 *     &lt;enumeration value="TOLERANT"/&gt;
 *     &lt;enumeration value="EXTENDED"/&gt;
 *     &lt;enumeration value="PUBLISHER_DEFINED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "deviceIdTypeType")
@XmlEnum
public enum DeviceIdTypeType {

    UNKNOWN("UNKNOWN"),
    STRING("STRING"),
    ETHERNET("ETHERNET"),
    @XmlEnumValue("FLEXID9")
    FLEXID_9("FLEXID9"),
    @XmlEnumValue("FLEXID10")
    FLEXID_10("FLEXID10"),
    VM_UUID("VM_UUID"),
    INTERNET("INTERNET"),
    @XmlEnumValue("INTERNET6")
    INTERNET_6("INTERNET6"),
    USER("USER"),
    TOLERANT("TOLERANT"),
    EXTENDED("EXTENDED"),
    PUBLISHER_DEFINED("PUBLISHER_DEFINED");
    private final String value;

    DeviceIdTypeType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DeviceIdTypeType fromValue(String v) {
        for (DeviceIdTypeType c: DeviceIdTypeType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
