
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for featureOverrideParamsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="featureOverrideParamsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="vendorString" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="notice" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serialNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dupGroup" type="{urn:com.macrovision:flexnet/operations}dupGroupDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "featureOverrideParamsType", propOrder = {
    "vendorString",
    "notice",
    "serialNumber",
    "dupGroup"
})
public class FeatureOverrideParamsType {

    protected String vendorString;
    protected String notice;
    protected String serialNumber;
    protected DupGroupDataType dupGroup;

    /**
     * Gets the value of the vendorString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVendorString() {
        return vendorString;
    }

    /**
     * Sets the value of the vendorString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVendorString(String value) {
        this.vendorString = value;
    }

    /**
     * Gets the value of the notice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotice() {
        return notice;
    }

    /**
     * Sets the value of the notice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotice(String value) {
        this.notice = value;
    }

    /**
     * Gets the value of the serialNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets the value of the serialNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNumber(String value) {
        this.serialNumber = value;
    }

    /**
     * Gets the value of the dupGroup property.
     * 
     * @return
     *     possible object is
     *     {@link DupGroupDataType }
     *     
     */
    public DupGroupDataType getDupGroup() {
        return dupGroup;
    }

    /**
     * Sets the value of the dupGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link DupGroupDataType }
     *     
     */
    public void setDupGroup(DupGroupDataType value) {
        this.dupGroup = value;
    }

}
