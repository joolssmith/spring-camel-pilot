
package com.flexnet.operations.webservices.u;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchEntitlementRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchEntitlementRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementSearchCriteria" type="{urn:v2.webservices.operations.flexnet.com}searchEntitlementDataType"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchEntitlementRequestType", propOrder = {
    "entitlementSearchCriteria",
    "batchSize",
    "pageNumber"
})
public class SearchEntitlementRequestType {

    @XmlElement(required = true)
    protected SearchEntitlementDataType entitlementSearchCriteria;
    @XmlElement(required = true)
    protected BigInteger batchSize;
    protected BigInteger pageNumber;

    /**
     * Gets the value of the entitlementSearchCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link SearchEntitlementDataType }
     *     
     */
    public SearchEntitlementDataType getEntitlementSearchCriteria() {
        return entitlementSearchCriteria;
    }

    /**
     * Sets the value of the entitlementSearchCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchEntitlementDataType }
     *     
     */
    public void setEntitlementSearchCriteria(SearchEntitlementDataType value) {
        this.entitlementSearchCriteria = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

}
