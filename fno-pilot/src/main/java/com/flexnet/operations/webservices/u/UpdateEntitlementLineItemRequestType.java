
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateEntitlementLineItemRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateEntitlementLineItemRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="lineItemData" type="{urn:v2.webservices.operations.flexnet.com}updateEntitlementLineItemDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateEntitlementLineItemRequestType", propOrder = {
    "lineItemData"
})
public class UpdateEntitlementLineItemRequestType {

    @XmlElement(required = true)
    protected List<UpdateEntitlementLineItemDataType> lineItemData;

    /**
     * Gets the value of the lineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UpdateEntitlementLineItemDataType }
     * 
     * 
     */
    public List<UpdateEntitlementLineItemDataType> getLineItemData() {
        if (lineItemData == null) {
            lineItemData = new ArrayList<UpdateEntitlementLineItemDataType>();
        }
        return this.lineItemData;
    }

}
