
package com.flexnet.operations.webservices.r;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedDeleteFeatureBundleDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedDeleteFeatureBundleDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundleIdentifier" type="{urn:v1.webservices.operations.flexnet.com}featureBundleIdentifierType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedDeleteFeatureBundleDataType", propOrder = {
    "featureBundleIdentifier",
    "reason"
})
public class FailedDeleteFeatureBundleDataType {

    @XmlElement(required = true)
    protected FeatureBundleIdentifierType featureBundleIdentifier;
    protected String reason;

    /**
     * Gets the value of the featureBundleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public FeatureBundleIdentifierType getFeatureBundleIdentifier() {
        return featureBundleIdentifier;
    }

    /**
     * Sets the value of the featureBundleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public void setFeatureBundleIdentifier(FeatureBundleIdentifierType value) {
        this.featureBundleIdentifier = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
