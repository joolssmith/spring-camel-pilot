
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for stateChangeResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="stateChangeResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureList" type="{urn:v2.webservices.operations.flexnet.com}featureStateChangeListType" minOccurs="0"/&gt;
 *         &lt;element name="featureBundleList" type="{urn:v2.webservices.operations.flexnet.com}featureBundleStateChangeListType" minOccurs="0"/&gt;
 *         &lt;element name="productList" type="{urn:v2.webservices.operations.flexnet.com}productStateChangeListType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModelList" type="{urn:v2.webservices.operations.flexnet.com}licenseModelStateChangeListType" minOccurs="0"/&gt;
 *         &lt;element name="simpleEntitlementList" type="{urn:v2.webservices.operations.flexnet.com}entitlementStateChangeListType" minOccurs="0"/&gt;
 *         &lt;element name="bulkEntitlementList" type="{urn:v2.webservices.operations.flexnet.com}entitlementStateChangeListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stateChangeResponseType", propOrder = {
    "featureList",
    "featureBundleList",
    "productList",
    "licenseModelList",
    "simpleEntitlementList",
    "bulkEntitlementList"
})
public class StateChangeResponseType {

    protected FeatureStateChangeListType featureList;
    protected FeatureBundleStateChangeListType featureBundleList;
    protected ProductStateChangeListType productList;
    protected LicenseModelStateChangeListType licenseModelList;
    protected EntitlementStateChangeListType simpleEntitlementList;
    protected EntitlementStateChangeListType bulkEntitlementList;

    /**
     * Gets the value of the featureList property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureStateChangeListType }
     *     
     */
    public FeatureStateChangeListType getFeatureList() {
        return featureList;
    }

    /**
     * Sets the value of the featureList property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureStateChangeListType }
     *     
     */
    public void setFeatureList(FeatureStateChangeListType value) {
        this.featureList = value;
    }

    /**
     * Gets the value of the featureBundleList property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleStateChangeListType }
     *     
     */
    public FeatureBundleStateChangeListType getFeatureBundleList() {
        return featureBundleList;
    }

    /**
     * Sets the value of the featureBundleList property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleStateChangeListType }
     *     
     */
    public void setFeatureBundleList(FeatureBundleStateChangeListType value) {
        this.featureBundleList = value;
    }

    /**
     * Gets the value of the productList property.
     * 
     * @return
     *     possible object is
     *     {@link ProductStateChangeListType }
     *     
     */
    public ProductStateChangeListType getProductList() {
        return productList;
    }

    /**
     * Sets the value of the productList property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductStateChangeListType }
     *     
     */
    public void setProductList(ProductStateChangeListType value) {
        this.productList = value;
    }

    /**
     * Gets the value of the licenseModelList property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseModelStateChangeListType }
     *     
     */
    public LicenseModelStateChangeListType getLicenseModelList() {
        return licenseModelList;
    }

    /**
     * Sets the value of the licenseModelList property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseModelStateChangeListType }
     *     
     */
    public void setLicenseModelList(LicenseModelStateChangeListType value) {
        this.licenseModelList = value;
    }

    /**
     * Gets the value of the simpleEntitlementList property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementStateChangeListType }
     *     
     */
    public EntitlementStateChangeListType getSimpleEntitlementList() {
        return simpleEntitlementList;
    }

    /**
     * Sets the value of the simpleEntitlementList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementStateChangeListType }
     *     
     */
    public void setSimpleEntitlementList(EntitlementStateChangeListType value) {
        this.simpleEntitlementList = value;
    }

    /**
     * Gets the value of the bulkEntitlementList property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementStateChangeListType }
     *     
     */
    public EntitlementStateChangeListType getBulkEntitlementList() {
        return bulkEntitlementList;
    }

    /**
     * Sets the value of the bulkEntitlementList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementStateChangeListType }
     *     
     */
    public void setBulkEntitlementList(EntitlementStateChangeListType value) {
        this.bulkEntitlementList = value;
    }

}
