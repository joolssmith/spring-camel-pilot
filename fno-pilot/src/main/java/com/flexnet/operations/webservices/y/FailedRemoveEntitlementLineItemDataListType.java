
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedRemoveEntitlementLineItemDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedRemoveEntitlementLineItemDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedData" type="{urn:v3.webservices.operations.flexnet.com}failedRemoveEntitlementLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedRemoveEntitlementLineItemDataListType", propOrder = {
    "failedData"
})
public class FailedRemoveEntitlementLineItemDataListType {

    protected List<FailedRemoveEntitlementLineItemDataType> failedData;

    /**
     * Gets the value of the failedData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedRemoveEntitlementLineItemDataType }
     * 
     * 
     */
    public List<FailedRemoveEntitlementLineItemDataType> getFailedData() {
        if (failedData == null) {
            failedData = new ArrayList<FailedRemoveEntitlementLineItemDataType>();
        }
        return this.failedData;
    }

}
