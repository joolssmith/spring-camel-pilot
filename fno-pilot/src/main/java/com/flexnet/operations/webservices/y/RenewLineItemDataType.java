
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for renewLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="renewLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="parentLineItemIdentifier" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="renewParameters" type="{urn:v3.webservices.operations.flexnet.com}renewParametersDataType"/&gt;
 *         &lt;element name="isFull" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowActivationsOnParent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="autoDeploy" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "renewLineItemDataType", propOrder = {
    "parentLineItemIdentifier",
    "renewParameters",
    "isFull",
    "allowActivationsOnParent",
    "autoDeploy"
})
public class RenewLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType parentLineItemIdentifier;
    @XmlElement(required = true)
    protected RenewParametersDataType renewParameters;
    protected Boolean isFull;
    protected Boolean allowActivationsOnParent;
    protected Boolean autoDeploy;

    /**
     * Gets the value of the parentLineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getParentLineItemIdentifier() {
        return parentLineItemIdentifier;
    }

    /**
     * Sets the value of the parentLineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setParentLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.parentLineItemIdentifier = value;
    }

    /**
     * Gets the value of the renewParameters property.
     * 
     * @return
     *     possible object is
     *     {@link RenewParametersDataType }
     *     
     */
    public RenewParametersDataType getRenewParameters() {
        return renewParameters;
    }

    /**
     * Sets the value of the renewParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link RenewParametersDataType }
     *     
     */
    public void setRenewParameters(RenewParametersDataType value) {
        this.renewParameters = value;
    }

    /**
     * Gets the value of the isFull property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsFull() {
        return isFull;
    }

    /**
     * Sets the value of the isFull property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsFull(Boolean value) {
        this.isFull = value;
    }

    /**
     * Gets the value of the allowActivationsOnParent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowActivationsOnParent() {
        return allowActivationsOnParent;
    }

    /**
     * Sets the value of the allowActivationsOnParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowActivationsOnParent(Boolean value) {
        this.allowActivationsOnParent = value;
    }

    /**
     * Gets the value of the autoDeploy property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAutoDeploy() {
        return autoDeploy;
    }

    /**
     * Sets the value of the autoDeploy property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAutoDeploy(Boolean value) {
        this.autoDeploy = value;
    }

}
