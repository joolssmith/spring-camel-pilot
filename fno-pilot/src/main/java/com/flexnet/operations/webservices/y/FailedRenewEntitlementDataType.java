
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedRenewEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedRenewEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementData" type="{urn:v3.webservices.operations.flexnet.com}renewEntitlementDataType"/&gt;
 *         &lt;element name="reason" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedRenewEntitlementDataType", propOrder = {
    "entitlementData",
    "reason"
})
public class FailedRenewEntitlementDataType {

    @XmlElement(required = true)
    protected RenewEntitlementDataType entitlementData;
    @XmlElement(required = true)
    protected String reason;

    /**
     * Gets the value of the entitlementData property.
     * 
     * @return
     *     possible object is
     *     {@link RenewEntitlementDataType }
     *     
     */
    public RenewEntitlementDataType getEntitlementData() {
        return entitlementData;
    }

    /**
     * Sets the value of the entitlementData property.
     * 
     * @param value
     *     allowed object is
     *     {@link RenewEntitlementDataType }
     *     
     */
    public void setEntitlementData(RenewEntitlementDataType value) {
        this.entitlementData = value;
    }

    /**
     * Gets the value of the reason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the value of the reason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReason(String value) {
        this.reason = value;
    }

}
