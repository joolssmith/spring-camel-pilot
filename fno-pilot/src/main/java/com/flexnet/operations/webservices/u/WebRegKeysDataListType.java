
package com.flexnet.operations.webservices.u;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for webRegKeysDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="webRegKeysDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="webRegKeys" type="{urn:v2.webservices.operations.flexnet.com}webRegKeyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "webRegKeysDataListType", propOrder = {
    "webRegKeys"
})
public class WebRegKeysDataListType {

    protected WebRegKeyType webRegKeys;

    /**
     * Gets the value of the webRegKeys property.
     * 
     * @return
     *     possible object is
     *     {@link WebRegKeyType }
     *     
     */
    public WebRegKeyType getWebRegKeys() {
        return webRegKeys;
    }

    /**
     * Sets the value of the webRegKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebRegKeyType }
     *     
     */
    public void setWebRegKeys(WebRegKeyType value) {
        this.webRegKeys = value;
    }

}
