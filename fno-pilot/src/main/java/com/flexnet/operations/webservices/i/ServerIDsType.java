
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ServerIDsType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ServerIDsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="server1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="server2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="server3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ServerIDsType", propOrder = {
    "server1",
    "server2",
    "server3"
})
public class ServerIDsType {

    @XmlElement(required = true)
    protected String server1;
    protected String server2;
    protected String server3;

    /**
     * Gets the value of the server1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServer1() {
        return server1;
    }

    /**
     * Sets the value of the server1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServer1(String value) {
        this.server1 = value;
    }

    /**
     * Gets the value of the server2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServer2() {
        return server2;
    }

    /**
     * Sets the value of the server2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServer2(String value) {
        this.server2 = value;
    }

    /**
     * Gets the value of the server3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServer3() {
        return server3;
    }

    /**
     * Sets the value of the server3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServer3(String value) {
        this.server3 = value;
    }

}
