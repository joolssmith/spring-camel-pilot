
package com.flexnet.operations.webservices.a;

import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ActivationIdDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ActivationIdDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ActivationId" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ActivationIdProblem" type="{http://producersuite.flexerasoftware.com/EntitlementService/}RightsIdProblem" minOccurs="0"/&gt;
 *         &lt;element name="Owner" type="{http://producersuite.flexerasoftware.com/EntitlementService/}OwnerData" minOccurs="0"/&gt;
 *         &lt;element name="AvailableCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="EntitledCount" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/&gt;
 *         &lt;element name="Features" type="{http://producersuite.flexerasoftware.com/EntitlementService/}ArrayOfFeature" minOccurs="0"/&gt;
 *         &lt;element name="State" type="{http://producersuite.flexerasoftware.com/EntitlementService/}EntityState" minOccurs="0"/&gt;
 *         &lt;element name="VirtualClientProhibited" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="SubstitutionVariablePattern" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ParentActivationList" type="{http://producersuite.flexerasoftware.com/EntitlementService/}ParentActivationListType" minOccurs="0"/&gt;
 *         &lt;element name="LicenseModel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActivationIdDetails", propOrder = {
    "activationId",
    "activationIdProblem",
    "owner",
    "availableCount",
    "entitledCount",
    "features",
    "state",
    "virtualClientProhibited",
    "substitutionVariablePattern",
    "parentActivationList",
    "licenseModel"
})
public class ActivationIdDetails {

    @XmlElement(name = "ActivationId", required = true, nillable = true)
    protected String activationId;
    @XmlElementRef(name = "ActivationIdProblem", type = JAXBElement.class, required = false)
    protected JAXBElement<RightsIdProblem> activationIdProblem;
    @XmlElementRef(name = "Owner", type = JAXBElement.class, required = false)
    protected JAXBElement<OwnerData> owner;
    @XmlElementRef(name = "AvailableCount", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> availableCount;
    @XmlElementRef(name = "EntitledCount", type = JAXBElement.class, required = false)
    protected JAXBElement<BigInteger> entitledCount;
    @XmlElementRef(name = "Features", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfFeature> features;
    @XmlElementRef(name = "State", type = JAXBElement.class, required = false)
    protected JAXBElement<EntityState> state;
    @XmlElementRef(name = "VirtualClientProhibited", type = JAXBElement.class, required = false)
    protected JAXBElement<Boolean> virtualClientProhibited;
    @XmlElementRef(name = "SubstitutionVariablePattern", type = JAXBElement.class, required = false)
    protected JAXBElement<String> substitutionVariablePattern;
    @XmlElementRef(name = "ParentActivationList", type = JAXBElement.class, required = false)
    protected JAXBElement<ParentActivationListType> parentActivationList;
    @XmlElementRef(name = "LicenseModel", type = JAXBElement.class, required = false)
    protected JAXBElement<String> licenseModel;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationId(String value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the activationIdProblem property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RightsIdProblem }{@code >}
     *     
     */
    public JAXBElement<RightsIdProblem> getActivationIdProblem() {
        return activationIdProblem;
    }

    /**
     * Sets the value of the activationIdProblem property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RightsIdProblem }{@code >}
     *     
     */
    public void setActivationIdProblem(JAXBElement<RightsIdProblem> value) {
        this.activationIdProblem = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link OwnerData }{@code >}
     *     
     */
    public JAXBElement<OwnerData> getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link OwnerData }{@code >}
     *     
     */
    public void setOwner(JAXBElement<OwnerData> value) {
        this.owner = value;
    }

    /**
     * Gets the value of the availableCount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getAvailableCount() {
        return availableCount;
    }

    /**
     * Sets the value of the availableCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setAvailableCount(JAXBElement<BigInteger> value) {
        this.availableCount = value;
    }

    /**
     * Gets the value of the entitledCount property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public JAXBElement<BigInteger> getEntitledCount() {
        return entitledCount;
    }

    /**
     * Sets the value of the entitledCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link BigInteger }{@code >}
     *     
     */
    public void setEntitledCount(JAXBElement<BigInteger> value) {
        this.entitledCount = value;
    }

    /**
     * Gets the value of the features property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFeature }{@code >}
     *     
     */
    public JAXBElement<ArrayOfFeature> getFeatures() {
        return features;
    }

    /**
     * Sets the value of the features property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfFeature }{@code >}
     *     
     */
    public void setFeatures(JAXBElement<ArrayOfFeature> value) {
        this.features = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link EntityState }{@code >}
     *     
     */
    public JAXBElement<EntityState> getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link EntityState }{@code >}
     *     
     */
    public void setState(JAXBElement<EntityState> value) {
        this.state = value;
    }

    /**
     * Gets the value of the virtualClientProhibited property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public JAXBElement<Boolean> getVirtualClientProhibited() {
        return virtualClientProhibited;
    }

    /**
     * Sets the value of the virtualClientProhibited property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     *     
     */
    public void setVirtualClientProhibited(JAXBElement<Boolean> value) {
        this.virtualClientProhibited = value;
    }

    /**
     * Gets the value of the substitutionVariablePattern property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getSubstitutionVariablePattern() {
        return substitutionVariablePattern;
    }

    /**
     * Sets the value of the substitutionVariablePattern property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setSubstitutionVariablePattern(JAXBElement<String> value) {
        this.substitutionVariablePattern = value;
    }

    /**
     * Gets the value of the parentActivationList property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ParentActivationListType }{@code >}
     *     
     */
    public JAXBElement<ParentActivationListType> getParentActivationList() {
        return parentActivationList;
    }

    /**
     * Sets the value of the parentActivationList property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ParentActivationListType }{@code >}
     *     
     */
    public void setParentActivationList(JAXBElement<ParentActivationListType> value) {
        this.parentActivationList = value;
    }

    /**
     * Gets the value of the licenseModel property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getLicenseModel() {
        return licenseModel;
    }

    /**
     * Sets the value of the licenseModel property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setLicenseModel(JAXBElement<String> value) {
        this.licenseModel = value;
    }

}
