
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyAttributesDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyAttributesDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="needRehostsPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needReturnsPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needRepairsPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needExtraActivationsPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needCancelLicensePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needVirtualLicensePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needReinstallPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needServerHostIdPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needNodelockedHostIdPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needPortalServerHostIdPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needPortalNodelockedHostIdPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needRedundantServerPolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="needACPIGenerationIdLicensePolicy" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyAttributesDataType", propOrder = {
    "needRehostsPolicy",
    "needReturnsPolicy",
    "needRepairsPolicy",
    "needExtraActivationsPolicy",
    "needCancelLicensePolicy",
    "needVirtualLicensePolicy",
    "needReinstallPolicy",
    "needServerHostIdPolicy",
    "needNodelockedHostIdPolicy",
    "needPortalServerHostIdPolicy",
    "needPortalNodelockedHostIdPolicy",
    "needRedundantServerPolicy",
    "needACPIGenerationIdLicensePolicy"
})
public class PolicyAttributesDataType {

    protected boolean needRehostsPolicy;
    protected boolean needReturnsPolicy;
    protected boolean needRepairsPolicy;
    protected boolean needExtraActivationsPolicy;
    protected boolean needCancelLicensePolicy;
    protected boolean needVirtualLicensePolicy;
    protected boolean needReinstallPolicy;
    protected boolean needServerHostIdPolicy;
    protected boolean needNodelockedHostIdPolicy;
    protected boolean needPortalServerHostIdPolicy;
    protected boolean needPortalNodelockedHostIdPolicy;
    protected boolean needRedundantServerPolicy;
    protected boolean needACPIGenerationIdLicensePolicy;

    /**
     * Gets the value of the needRehostsPolicy property.
     * 
     */
    public boolean isNeedRehostsPolicy() {
        return needRehostsPolicy;
    }

    /**
     * Sets the value of the needRehostsPolicy property.
     * 
     */
    public void setNeedRehostsPolicy(boolean value) {
        this.needRehostsPolicy = value;
    }

    /**
     * Gets the value of the needReturnsPolicy property.
     * 
     */
    public boolean isNeedReturnsPolicy() {
        return needReturnsPolicy;
    }

    /**
     * Sets the value of the needReturnsPolicy property.
     * 
     */
    public void setNeedReturnsPolicy(boolean value) {
        this.needReturnsPolicy = value;
    }

    /**
     * Gets the value of the needRepairsPolicy property.
     * 
     */
    public boolean isNeedRepairsPolicy() {
        return needRepairsPolicy;
    }

    /**
     * Sets the value of the needRepairsPolicy property.
     * 
     */
    public void setNeedRepairsPolicy(boolean value) {
        this.needRepairsPolicy = value;
    }

    /**
     * Gets the value of the needExtraActivationsPolicy property.
     * 
     */
    public boolean isNeedExtraActivationsPolicy() {
        return needExtraActivationsPolicy;
    }

    /**
     * Sets the value of the needExtraActivationsPolicy property.
     * 
     */
    public void setNeedExtraActivationsPolicy(boolean value) {
        this.needExtraActivationsPolicy = value;
    }

    /**
     * Gets the value of the needCancelLicensePolicy property.
     * 
     */
    public boolean isNeedCancelLicensePolicy() {
        return needCancelLicensePolicy;
    }

    /**
     * Sets the value of the needCancelLicensePolicy property.
     * 
     */
    public void setNeedCancelLicensePolicy(boolean value) {
        this.needCancelLicensePolicy = value;
    }

    /**
     * Gets the value of the needVirtualLicensePolicy property.
     * 
     */
    public boolean isNeedVirtualLicensePolicy() {
        return needVirtualLicensePolicy;
    }

    /**
     * Sets the value of the needVirtualLicensePolicy property.
     * 
     */
    public void setNeedVirtualLicensePolicy(boolean value) {
        this.needVirtualLicensePolicy = value;
    }

    /**
     * Gets the value of the needReinstallPolicy property.
     * 
     */
    public boolean isNeedReinstallPolicy() {
        return needReinstallPolicy;
    }

    /**
     * Sets the value of the needReinstallPolicy property.
     * 
     */
    public void setNeedReinstallPolicy(boolean value) {
        this.needReinstallPolicy = value;
    }

    /**
     * Gets the value of the needServerHostIdPolicy property.
     * 
     */
    public boolean isNeedServerHostIdPolicy() {
        return needServerHostIdPolicy;
    }

    /**
     * Sets the value of the needServerHostIdPolicy property.
     * 
     */
    public void setNeedServerHostIdPolicy(boolean value) {
        this.needServerHostIdPolicy = value;
    }

    /**
     * Gets the value of the needNodelockedHostIdPolicy property.
     * 
     */
    public boolean isNeedNodelockedHostIdPolicy() {
        return needNodelockedHostIdPolicy;
    }

    /**
     * Sets the value of the needNodelockedHostIdPolicy property.
     * 
     */
    public void setNeedNodelockedHostIdPolicy(boolean value) {
        this.needNodelockedHostIdPolicy = value;
    }

    /**
     * Gets the value of the needPortalServerHostIdPolicy property.
     * 
     */
    public boolean isNeedPortalServerHostIdPolicy() {
        return needPortalServerHostIdPolicy;
    }

    /**
     * Sets the value of the needPortalServerHostIdPolicy property.
     * 
     */
    public void setNeedPortalServerHostIdPolicy(boolean value) {
        this.needPortalServerHostIdPolicy = value;
    }

    /**
     * Gets the value of the needPortalNodelockedHostIdPolicy property.
     * 
     */
    public boolean isNeedPortalNodelockedHostIdPolicy() {
        return needPortalNodelockedHostIdPolicy;
    }

    /**
     * Sets the value of the needPortalNodelockedHostIdPolicy property.
     * 
     */
    public void setNeedPortalNodelockedHostIdPolicy(boolean value) {
        this.needPortalNodelockedHostIdPolicy = value;
    }

    /**
     * Gets the value of the needRedundantServerPolicy property.
     * 
     */
    public boolean isNeedRedundantServerPolicy() {
        return needRedundantServerPolicy;
    }

    /**
     * Sets the value of the needRedundantServerPolicy property.
     * 
     */
    public void setNeedRedundantServerPolicy(boolean value) {
        this.needRedundantServerPolicy = value;
    }

    /**
     * Gets the value of the needACPIGenerationIdLicensePolicy property.
     * 
     */
    public boolean isNeedACPIGenerationIdLicensePolicy() {
        return needACPIGenerationIdLicensePolicy;
    }

    /**
     * Sets the value of the needACPIGenerationIdLicensePolicy property.
     * 
     */
    public void setNeedACPIGenerationIdLicensePolicy(boolean value) {
        this.needACPIGenerationIdLicensePolicy = value;
    }

}
