
package com.flexnet.operations.webservices.a;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DurationBasedExpiration complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DurationBasedExpiration"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://producersuite.flexerasoftware.com/EntitlementService/}Expiration"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="StartDate" type="{http://producersuite.flexerasoftware.com/EntitlementService/}StartDate" minOccurs="0"/&gt;
 *         &lt;element name="Duration" type="{http://producersuite.flexerasoftware.com/EntitlementService/}Duration"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DurationBasedExpiration", propOrder = {
    "startDate",
    "duration"
})
public class DurationBasedExpiration
    extends Expiration
{

    @XmlElementRef(name = "StartDate", type = JAXBElement.class, required = false)
    protected JAXBElement<StartDate> startDate;
    @XmlElement(name = "Duration", required = true, nillable = true)
    protected Duration duration;

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link StartDate }{@code >}
     *     
     */
    public JAXBElement<StartDate> getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link StartDate }{@code >}
     *     
     */
    public void setStartDate(JAXBElement<StartDate> value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setDuration(Duration value) {
        this.duration = value;
    }

}
