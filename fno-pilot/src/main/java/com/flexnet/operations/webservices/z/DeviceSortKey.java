
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceSortKey.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="deviceSortKey"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="DEVICE_ID"/&gt;
 *     &lt;enumeration value="NAME"/&gt;
 *     &lt;enumeration value="LAST_MODIFIED_DATE"/&gt;
 *     &lt;enumeration value="SOLD_TO_ACCOUNT_ID"/&gt;
 *     &lt;enumeration value="SOLD_TO_ACCOUNT_NAME"/&gt;
 *     &lt;enumeration value="DEVICE_ID_TYPE"/&gt;
 *     &lt;enumeration value="DEVICE_CLASS"/&gt;
 *     &lt;enumeration value="HOST_TYPE_NAME"/&gt;
 *     &lt;enumeration value="HOST_TYPE_ALIAS"/&gt;
 *     &lt;enumeration value="HOST_SERIES_NAME"/&gt;
 *     &lt;enumeration value="SIGNATURE_NAME"/&gt;
 *     &lt;enumeration value="STATUS"/&gt;
 *     &lt;enumeration value="PARENT_IDENTIFIER"/&gt;
 *     &lt;enumeration value="USER"/&gt;
 *     &lt;enumeration value="MACHINE_TYPE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "deviceSortKey")
@XmlEnum
public enum DeviceSortKey {

    DEVICE_ID,
    NAME,
    LAST_MODIFIED_DATE,
    SOLD_TO_ACCOUNT_ID,
    SOLD_TO_ACCOUNT_NAME,
    DEVICE_ID_TYPE,
    DEVICE_CLASS,
    HOST_TYPE_NAME,
    HOST_TYPE_ALIAS,
    HOST_SERIES_NAME,
    SIGNATURE_NAME,
    STATUS,
    PARENT_IDENTIFIER,
    USER,
    MACHINE_TYPE;

    public String value() {
        return name();
    }

    public static DeviceSortKey fromValue(String v) {
        return valueOf(v);
    }

}
