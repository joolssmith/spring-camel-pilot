
package com.flexnet.operations.webservices.i;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for onholdFulfillmentListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="onholdFulfillmentListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="onholdFmtLicenseData" type="{urn:com.macrovision:flexnet/operations}onHoldFmtLicenseDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "onholdFulfillmentListType", propOrder = {
    "onholdFmtLicenseData"
})
public class OnholdFulfillmentListType {

    @XmlElement(required = true)
    protected List<OnHoldFmtLicenseDataType> onholdFmtLicenseData;

    /**
     * Gets the value of the onholdFmtLicenseData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the onholdFmtLicenseData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOnholdFmtLicenseData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OnHoldFmtLicenseDataType }
     * 
     * 
     */
    public List<OnHoldFmtLicenseDataType> getOnholdFmtLicenseData() {
        if (onholdFmtLicenseData == null) {
            onholdFmtLicenseData = new ArrayList<OnHoldFmtLicenseDataType>();
        }
        return this.onholdFmtLicenseData;
    }

}
