
package com.flexnet.operations.webservices.z;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deviceQueryDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deviceQueryDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="deviceIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="alias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:v3.fne.webservices.operations.flexnet.com}hostTypeIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="deviceStatus" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceStatusType" minOccurs="0"/&gt;
 *         &lt;element name="deviceServedStatus" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceServedStatusType" minOccurs="0"/&gt;
 *         &lt;element name="channelPartners" type="{urn:v3.fne.webservices.operations.flexnet.com}channelPartnerDataListType" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{urn:v3.fne.webservices.operations.flexnet.com}soldToType" minOccurs="0"/&gt;
 *         &lt;element name="soldToAcctName" type="{urn:v3.fne.webservices.operations.flexnet.com}soldToAcctNameType" minOccurs="0"/&gt;
 *         &lt;element name="preBuiltProduct" type="{urn:v3.fne.webservices.operations.flexnet.com}productPKType" minOccurs="0"/&gt;
 *         &lt;element name="hasPrebuiltLicense" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="prebuiltLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="hasAddonLicense" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="addonLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="publisherIdName" type="{urn:v3.fne.webservices.operations.flexnet.com}publisherIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="addonLineItemData" type="{urn:v3.fne.webservices.operations.flexnet.com}addonLineItemDataDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="featureData" type="{urn:v3.fne.webservices.operations.flexnet.com}featureDataDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v3.fne.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="parentIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}deviceIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="machineType" type="{urn:v3.fne.webservices.operations.flexnet.com}machineTypeType" minOccurs="0"/&gt;
 *         &lt;element name="vmName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vmInfo" type="{urn:v3.fne.webservices.operations.flexnet.com}dictionaryType" minOccurs="0"/&gt;
 *         &lt;element name="vendorDictionary" type="{urn:v3.fne.webservices.operations.flexnet.com}dictionaryType" minOccurs="0"/&gt;
 *         &lt;element name="deviceUser" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="deviceUserIdentifier" type="{urn:v3.fne.webservices.operations.flexnet.com}userIdentifierType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deviceQueryDataType", propOrder = {
    "deviceIdentifier",
    "alias",
    "description",
    "hostTypeName",
    "deviceStatus",
    "deviceServedStatus",
    "channelPartners",
    "soldTo",
    "soldToAcctName",
    "preBuiltProduct",
    "hasPrebuiltLicense",
    "prebuiltLicense",
    "hasAddonLicense",
    "addonLicense",
    "publisherIdName",
    "addonLineItemData",
    "featureData",
    "customAttributes",
    "parentIdentifier",
    "machineType",
    "vmName",
    "vmInfo",
    "vendorDictionary",
    "deviceUser",
    "deviceUserIdentifier"
})
public class DeviceQueryDataType {

    protected DeviceIdentifier deviceIdentifier;
    protected String alias;
    protected String description;
    protected HostTypeIdentifier hostTypeName;
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceStatusType deviceStatus;
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceServedStatusType deviceServedStatus;
    protected ChannelPartnerDataListType channelPartners;
    protected SoldToType soldTo;
    protected SoldToAcctNameType soldToAcctName;
    protected ProductPKType preBuiltProduct;
    protected Boolean hasPrebuiltLicense;
    protected byte[] prebuiltLicense;
    protected Boolean hasAddonLicense;
    protected byte[] addonLicense;
    protected PublisherIdentifier publisherIdName;
    protected List<AddonLineItemDataDataType> addonLineItemData;
    protected List<FeatureDataDataType> featureData;
    protected AttributeDescriptorDataType customAttributes;
    protected DeviceIdentifier parentIdentifier;
    @XmlSchemaType(name = "NMTOKEN")
    protected MachineTypeType machineType;
    protected String vmName;
    protected DictionaryType vmInfo;
    protected DictionaryType vendorDictionary;
    protected String deviceUser;
    protected UserIdentifierType deviceUserIdentifier;

    /**
     * Gets the value of the deviceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getDeviceIdentifier() {
        return deviceIdentifier;
    }

    /**
     * Sets the value of the deviceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setDeviceIdentifier(DeviceIdentifier value) {
        this.deviceIdentifier = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public HostTypeIdentifier getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public void setHostTypeName(HostTypeIdentifier value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the deviceStatus property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceStatusType }
     *     
     */
    public DeviceStatusType getDeviceStatus() {
        return deviceStatus;
    }

    /**
     * Sets the value of the deviceStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceStatusType }
     *     
     */
    public void setDeviceStatus(DeviceStatusType value) {
        this.deviceStatus = value;
    }

    /**
     * Gets the value of the deviceServedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceServedStatusType }
     *     
     */
    public DeviceServedStatusType getDeviceServedStatus() {
        return deviceServedStatus;
    }

    /**
     * Sets the value of the deviceServedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceServedStatusType }
     *     
     */
    public void setDeviceServedStatus(DeviceServedStatusType value) {
        this.deviceServedStatus = value;
    }

    /**
     * Gets the value of the channelPartners property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public ChannelPartnerDataListType getChannelPartners() {
        return channelPartners;
    }

    /**
     * Sets the value of the channelPartners property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public void setChannelPartners(ChannelPartnerDataListType value) {
        this.channelPartners = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link SoldToType }
     *     
     */
    public SoldToType getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoldToType }
     *     
     */
    public void setSoldTo(SoldToType value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the soldToAcctName property.
     * 
     * @return
     *     possible object is
     *     {@link SoldToAcctNameType }
     *     
     */
    public SoldToAcctNameType getSoldToAcctName() {
        return soldToAcctName;
    }

    /**
     * Sets the value of the soldToAcctName property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoldToAcctNameType }
     *     
     */
    public void setSoldToAcctName(SoldToAcctNameType value) {
        this.soldToAcctName = value;
    }

    /**
     * Gets the value of the preBuiltProduct property.
     * 
     * @return
     *     possible object is
     *     {@link ProductPKType }
     *     
     */
    public ProductPKType getPreBuiltProduct() {
        return preBuiltProduct;
    }

    /**
     * Sets the value of the preBuiltProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductPKType }
     *     
     */
    public void setPreBuiltProduct(ProductPKType value) {
        this.preBuiltProduct = value;
    }

    /**
     * Gets the value of the hasPrebuiltLicense property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasPrebuiltLicense() {
        return hasPrebuiltLicense;
    }

    /**
     * Sets the value of the hasPrebuiltLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasPrebuiltLicense(Boolean value) {
        this.hasPrebuiltLicense = value;
    }

    /**
     * Gets the value of the prebuiltLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPrebuiltLicense() {
        return prebuiltLicense;
    }

    /**
     * Sets the value of the prebuiltLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPrebuiltLicense(byte[] value) {
        this.prebuiltLicense = value;
    }

    /**
     * Gets the value of the hasAddonLicense property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isHasAddonLicense() {
        return hasAddonLicense;
    }

    /**
     * Sets the value of the hasAddonLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setHasAddonLicense(Boolean value) {
        this.hasAddonLicense = value;
    }

    /**
     * Gets the value of the addonLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAddonLicense() {
        return addonLicense;
    }

    /**
     * Sets the value of the addonLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAddonLicense(byte[] value) {
        this.addonLicense = value;
    }

    /**
     * Gets the value of the publisherIdName property.
     * 
     * @return
     *     possible object is
     *     {@link PublisherIdentifier }
     *     
     */
    public PublisherIdentifier getPublisherIdName() {
        return publisherIdName;
    }

    /**
     * Sets the value of the publisherIdName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublisherIdentifier }
     *     
     */
    public void setPublisherIdName(PublisherIdentifier value) {
        this.publisherIdName = value;
    }

    /**
     * Gets the value of the addonLineItemData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the addonLineItemData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddonLineItemData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AddonLineItemDataDataType }
     * 
     * 
     */
    public List<AddonLineItemDataDataType> getAddonLineItemData() {
        if (addonLineItemData == null) {
            addonLineItemData = new ArrayList<AddonLineItemDataDataType>();
        }
        return this.addonLineItemData;
    }

    /**
     * Gets the value of the featureData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featureData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureDataDataType }
     * 
     * 
     */
    public List<FeatureDataDataType> getFeatureData() {
        if (featureData == null) {
            featureData = new ArrayList<FeatureDataDataType>();
        }
        return this.featureData;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

    /**
     * Gets the value of the parentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getParentIdentifier() {
        return parentIdentifier;
    }

    /**
     * Sets the value of the parentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setParentIdentifier(DeviceIdentifier value) {
        this.parentIdentifier = value;
    }

    /**
     * Gets the value of the machineType property.
     * 
     * @return
     *     possible object is
     *     {@link MachineTypeType }
     *     
     */
    public MachineTypeType getMachineType() {
        return machineType;
    }

    /**
     * Sets the value of the machineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MachineTypeType }
     *     
     */
    public void setMachineType(MachineTypeType value) {
        this.machineType = value;
    }

    /**
     * Gets the value of the vmName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVmName() {
        return vmName;
    }

    /**
     * Sets the value of the vmName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVmName(String value) {
        this.vmName = value;
    }

    /**
     * Gets the value of the vmInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DictionaryType }
     *     
     */
    public DictionaryType getVmInfo() {
        return vmInfo;
    }

    /**
     * Sets the value of the vmInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DictionaryType }
     *     
     */
    public void setVmInfo(DictionaryType value) {
        this.vmInfo = value;
    }

    /**
     * Gets the value of the vendorDictionary property.
     * 
     * @return
     *     possible object is
     *     {@link DictionaryType }
     *     
     */
    public DictionaryType getVendorDictionary() {
        return vendorDictionary;
    }

    /**
     * Sets the value of the vendorDictionary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DictionaryType }
     *     
     */
    public void setVendorDictionary(DictionaryType value) {
        this.vendorDictionary = value;
    }

    /**
     * Gets the value of the deviceUser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeviceUser() {
        return deviceUser;
    }

    /**
     * Sets the value of the deviceUser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeviceUser(String value) {
        this.deviceUser = value;
    }

    /**
     * Gets the value of the deviceUserIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link UserIdentifierType }
     *     
     */
    public UserIdentifierType getDeviceUserIdentifier() {
        return deviceUserIdentifier;
    }

    /**
     * Sets the value of the deviceUserIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserIdentifierType }
     *     
     */
    public void setDeviceUserIdentifier(UserIdentifierType value) {
        this.deviceUserIdentifier = value;
    }

}
