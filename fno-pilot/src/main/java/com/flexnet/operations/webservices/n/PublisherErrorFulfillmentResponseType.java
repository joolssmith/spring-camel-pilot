
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for publisherErrorFulfillmentResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="publisherErrorFulfillmentResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="responseData" type="{urn:v1.webservices.operations.flexnet.com}publisherErrorResponseDataType" minOccurs="0"/&gt;
 *         &lt;element name="failedData" type="{urn:v1.webservices.operations.flexnet.com}failedPublisherErrorResponselistDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "publisherErrorFulfillmentResponseType", propOrder = {
    "statusInfo",
    "responseData",
    "failedData"
})
public class PublisherErrorFulfillmentResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected PublisherErrorResponseDataType responseData;
    protected FailedPublisherErrorResponselistDataType failedData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link PublisherErrorResponseDataType }
     *     
     */
    public PublisherErrorResponseDataType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link PublisherErrorResponseDataType }
     *     
     */
    public void setResponseData(PublisherErrorResponseDataType value) {
        this.responseData = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedPublisherErrorResponselistDataType }
     *     
     */
    public FailedPublisherErrorResponselistDataType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedPublisherErrorResponselistDataType }
     *     
     */
    public void setFailedData(FailedPublisherErrorResponselistDataType value) {
        this.failedData = value;
    }

}
