
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for moveDeviceRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="moveDeviceRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sourceDevices" type="{urn:com.macrovision:flexnet/opsembedded}moveDeviceList"/&gt;
 *         &lt;element name="soldToUniqueId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="soldToName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="poolEntitlements" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "moveDeviceRequestType", propOrder = {
    "sourceDevices",
    "soldToUniqueId",
    "soldToName",
    "poolEntitlements"
})
public class MoveDeviceRequestType {

    @XmlElement(required = true)
    protected MoveDeviceList sourceDevices;
    protected String soldToUniqueId;
    protected String soldToName;
    protected Boolean poolEntitlements;

    /**
     * Gets the value of the sourceDevices property.
     * 
     * @return
     *     possible object is
     *     {@link MoveDeviceList }
     *     
     */
    public MoveDeviceList getSourceDevices() {
        return sourceDevices;
    }

    /**
     * Sets the value of the sourceDevices property.
     * 
     * @param value
     *     allowed object is
     *     {@link MoveDeviceList }
     *     
     */
    public void setSourceDevices(MoveDeviceList value) {
        this.sourceDevices = value;
    }

    /**
     * Gets the value of the soldToUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldToUniqueId() {
        return soldToUniqueId;
    }

    /**
     * Sets the value of the soldToUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldToUniqueId(String value) {
        this.soldToUniqueId = value;
    }

    /**
     * Gets the value of the soldToName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldToName() {
        return soldToName;
    }

    /**
     * Sets the value of the soldToName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldToName(String value) {
        this.soldToName = value;
    }

    /**
     * Gets the value of the poolEntitlements property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPoolEntitlements() {
        return poolEntitlements;
    }

    /**
     * Sets the value of the poolEntitlements property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPoolEntitlements(Boolean value) {
        this.poolEntitlements = value;
    }

}
