package com.flexnet.operations.webservices.n;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 3.2.4
 * 2018-05-09T18:59:48.498+01:00
 * Generated source version: 3.2.4
 *
 */
@WebServiceClient(name = "v1/LicenseService",
                  wsdlLocation = "https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/LicenseService?wsdl",
                  targetNamespace = "urn:v1.webservices.operations.flexnet.com")
public class V1_002fLicenseService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("urn:v1.webservices.operations.flexnet.com", "v1/LicenseService");
    public final static QName V1_002fLicenseService = new QName("urn:v1.webservices.operations.flexnet.com", "v1/LicenseService");
    static {
        URL url = null;
        try {
            url = new URL("https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/LicenseService?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(V1_002fLicenseService.class.getName())
                .log(java.util.logging.Level.INFO,
                     "Can not initialize the default wsdl from {0}", "https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/LicenseService?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public V1_002fLicenseService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public V1_002fLicenseService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public V1_002fLicenseService() {
        super(WSDL_LOCATION, SERVICE);
    }

    public V1_002fLicenseService(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    public V1_002fLicenseService(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    public V1_002fLicenseService(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }




    /**
     *
     * @return
     *     returns LicenseFulfillmentServiceInterfaceV1
     */
    @WebEndpoint(name = "v1/LicenseService")
    public LicenseFulfillmentServiceInterfaceV1 getV1_002fLicenseService() {
        return super.getPort(V1_002fLicenseService, LicenseFulfillmentServiceInterfaceV1.class);
    }

    /**
     *
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns LicenseFulfillmentServiceInterfaceV1
     */
    @WebEndpoint(name = "v1/LicenseService")
    public LicenseFulfillmentServiceInterfaceV1 getV1_002fLicenseService(WebServiceFeature... features) {
        return super.getPort(V1_002fLicenseService, LicenseFulfillmentServiceInterfaceV1.class, features);
    }

}
