
package com.flexnet.operations.webservices.x;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dupGroupDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dupGroupDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dupGroupOption" type="{urn:v2.webservices.operations.flexnet.com}DupGroupType"/&gt;
 *         &lt;element name="groupMask" type="{urn:v2.webservices.operations.flexnet.com}groupMaskDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dupGroupDataType", propOrder = {
    "dupGroupOption",
    "groupMask"
})
public class DupGroupDataType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected DupGroupType dupGroupOption;
    protected GroupMaskDataType groupMask;

    /**
     * Gets the value of the dupGroupOption property.
     * 
     * @return
     *     possible object is
     *     {@link DupGroupType }
     *     
     */
    public DupGroupType getDupGroupOption() {
        return dupGroupOption;
    }

    /**
     * Sets the value of the dupGroupOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link DupGroupType }
     *     
     */
    public void setDupGroupOption(DupGroupType value) {
        this.dupGroupOption = value;
    }

    /**
     * Gets the value of the groupMask property.
     * 
     * @return
     *     possible object is
     *     {@link GroupMaskDataType }
     *     
     */
    public GroupMaskDataType getGroupMask() {
        return groupMask;
    }

    /**
     * Sets the value of the groupMask property.
     * 
     * @param value
     *     allowed object is
     *     {@link GroupMaskDataType }
     *     
     */
    public void setGroupMask(GroupMaskDataType value) {
        this.groupMask = value;
    }

}
