
package com.flexnet.operations.webservices.w;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateUserRolesListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateUserRolesListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="role" type="{urn:v2.webservices.operations.flexnet.com}roleIdentifierType" maxOccurs="unbounded"/&gt;
 *         &lt;element name="opType" type="{urn:v2.webservices.operations.flexnet.com}CollectionOperationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateUserRolesListType", propOrder = {
    "role",
    "opType"
})
public class UpdateUserRolesListType {

    @XmlElement(required = true)
    protected List<RoleIdentifierType> role;
    @XmlElement(required = true)
    @XmlSchemaType(name = "NMTOKEN")
    protected CollectionOperationType opType;

    /**
     * Gets the value of the role property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the role property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRole().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RoleIdentifierType }
     * 
     * 
     */
    public List<RoleIdentifierType> getRole() {
        if (role == null) {
            role = new ArrayList<RoleIdentifierType>();
        }
        return this.role;
    }

    /**
     * Gets the value of the opType property.
     * 
     * @return
     *     possible object is
     *     {@link CollectionOperationType }
     *     
     */
    public CollectionOperationType getOpType() {
        return opType;
    }

    /**
     * Sets the value of the opType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CollectionOperationType }
     *     
     */
    public void setOpType(CollectionOperationType value) {
        this.opType = value;
    }

}
