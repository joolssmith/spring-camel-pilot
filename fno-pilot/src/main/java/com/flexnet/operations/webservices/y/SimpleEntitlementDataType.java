
package com.flexnet.operations.webservices.y;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for simpleEntitlementDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="simpleEntitlementDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementId" type="{urn:v3.webservices.operations.flexnet.com}idType"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="soldToName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailTemplateVariation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:v3.webservices.operations.flexnet.com}StateType" minOccurs="0"/&gt;
 *         &lt;element name="createdUserId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="lineItems" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="maintenanceLineItems" type="{urn:v3.webservices.operations.flexnet.com}maintenanceLineItemDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="channelPartners" type="{urn:v3.webservices.operations.flexnet.com}channelPartnerDataListType" minOccurs="0"/&gt;
 *         &lt;element name="allowPortalLogin" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="entitlementAttributes" type="{urn:v3.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "simpleEntitlementDataType", propOrder = {
    "entitlementId",
    "lastModifiedDateTime",
    "description",
    "soldTo",
    "soldToName",
    "shipToEmail",
    "shipToAddress",
    "emailTemplateVariation",
    "state",
    "createdUserId",
    "lineItems",
    "maintenanceLineItems",
    "channelPartners",
    "allowPortalLogin",
    "entitlementAttributes"
})
public class SimpleEntitlementDataType {

    @XmlElement(required = true)
    protected IdType entitlementId;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    protected String description;
    @XmlElement(required = true)
    protected String soldTo;
    protected String soldToName;
    protected String shipToEmail;
    protected String shipToAddress;
    protected String emailTemplateVariation;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    protected String createdUserId;
    protected List<EntitlementLineItemDataType> lineItems;
    protected List<MaintenanceLineItemDataType> maintenanceLineItems;
    protected ChannelPartnerDataListType channelPartners;
    protected Boolean allowPortalLogin;
    protected AttributeDescriptorDataType entitlementAttributes;

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link IdType }
     *     
     */
    public IdType getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdType }
     *     
     */
    public void setEntitlementId(IdType value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldTo(String value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the soldToName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldToName() {
        return soldToName;
    }

    /**
     * Sets the value of the soldToName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldToName(String value) {
        this.soldToName = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToEmail(String value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddress(String value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the emailTemplateVariation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailTemplateVariation() {
        return emailTemplateVariation;
    }

    /**
     * Sets the value of the emailTemplateVariation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailTemplateVariation(String value) {
        this.emailTemplateVariation = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the createdUserId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreatedUserId() {
        return createdUserId;
    }

    /**
     * Sets the value of the createdUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreatedUserId(String value) {
        this.createdUserId = value;
    }

    /**
     * Gets the value of the lineItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the lineItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLineItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntitlementLineItemDataType }
     * 
     * 
     */
    public List<EntitlementLineItemDataType> getLineItems() {
        if (lineItems == null) {
            lineItems = new ArrayList<EntitlementLineItemDataType>();
        }
        return this.lineItems;
    }

    /**
     * Gets the value of the maintenanceLineItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the maintenanceLineItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMaintenanceLineItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MaintenanceLineItemDataType }
     * 
     * 
     */
    public List<MaintenanceLineItemDataType> getMaintenanceLineItems() {
        if (maintenanceLineItems == null) {
            maintenanceLineItems = new ArrayList<MaintenanceLineItemDataType>();
        }
        return this.maintenanceLineItems;
    }

    /**
     * Gets the value of the channelPartners property.
     * 
     * @return
     *     possible object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public ChannelPartnerDataListType getChannelPartners() {
        return channelPartners;
    }

    /**
     * Sets the value of the channelPartners property.
     * 
     * @param value
     *     allowed object is
     *     {@link ChannelPartnerDataListType }
     *     
     */
    public void setChannelPartners(ChannelPartnerDataListType value) {
        this.channelPartners = value;
    }

    /**
     * Gets the value of the allowPortalLogin property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowPortalLogin() {
        return allowPortalLogin;
    }

    /**
     * Sets the value of the allowPortalLogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowPortalLogin(Boolean value) {
        this.allowPortalLogin = value;
    }

    /**
     * Gets the value of the entitlementAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getEntitlementAttributes() {
        return entitlementAttributes;
    }

    /**
     * Sets the value of the entitlementAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setEntitlementAttributes(AttributeDescriptorDataType value) {
        this.entitlementAttributes = value;
    }

}
