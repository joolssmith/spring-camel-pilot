
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getMatchingBulkEntInfoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getMatchingBulkEntInfoType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntIdentifier" type="{urn:v3.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="targetTierName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="targetAccountUnit" type="{urn:v3.webservices.operations.flexnet.com}accountIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMatchingBulkEntInfoType", propOrder = {
    "bulkEntIdentifier",
    "targetTierName",
    "targetAccountUnit"
})
public class GetMatchingBulkEntInfoType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType bulkEntIdentifier;
    @XmlElement(required = true)
    protected String targetTierName;
    @XmlElement(required = true)
    protected AccountIdentifierType targetAccountUnit;

    /**
     * Gets the value of the bulkEntIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getBulkEntIdentifier() {
        return bulkEntIdentifier;
    }

    /**
     * Sets the value of the bulkEntIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setBulkEntIdentifier(EntitlementIdentifierType value) {
        this.bulkEntIdentifier = value;
    }

    /**
     * Gets the value of the targetTierName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetTierName() {
        return targetTierName;
    }

    /**
     * Sets the value of the targetTierName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetTierName(String value) {
        this.targetTierName = value;
    }

    /**
     * Gets the value of the targetAccountUnit property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifierType }
     *     
     */
    public AccountIdentifierType getTargetAccountUnit() {
        return targetAccountUnit;
    }

    /**
     * Sets the value of the targetAccountUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifierType }
     *     
     */
    public void setTargetAccountUnit(AccountIdentifierType value) {
        this.targetAccountUnit = value;
    }

}
