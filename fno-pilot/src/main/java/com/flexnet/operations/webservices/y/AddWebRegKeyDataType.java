
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addWebRegKeyDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addWebRegKeyDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="bulkEntitlement" type="{urn:v3.webservices.operations.flexnet.com}entitlementIdentifierType"/&gt;
 *         &lt;element name="webRegKeys" type="{urn:v3.webservices.operations.flexnet.com}webRegKeyDataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addWebRegKeyDataType", propOrder = {
    "bulkEntitlement",
    "webRegKeys"
})
public class AddWebRegKeyDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType bulkEntitlement;
    @XmlElement(required = true)
    protected WebRegKeyDataType webRegKeys;

    /**
     * Gets the value of the bulkEntitlement property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getBulkEntitlement() {
        return bulkEntitlement;
    }

    /**
     * Sets the value of the bulkEntitlement property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setBulkEntitlement(EntitlementIdentifierType value) {
        this.bulkEntitlement = value;
    }

    /**
     * Gets the value of the webRegKeys property.
     * 
     * @return
     *     possible object is
     *     {@link WebRegKeyDataType }
     *     
     */
    public WebRegKeyDataType getWebRegKeys() {
        return webRegKeys;
    }

    /**
     * Sets the value of the webRegKeys property.
     * 
     * @param value
     *     allowed object is
     *     {@link WebRegKeyDataType }
     *     
     */
    public void setWebRegKeys(WebRegKeyDataType value) {
        this.webRegKeys = value;
    }

}
