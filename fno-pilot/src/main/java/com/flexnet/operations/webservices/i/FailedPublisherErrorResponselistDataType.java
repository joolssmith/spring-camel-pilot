
package com.flexnet.operations.webservices.i;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedPublisherErrorResponselistDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedPublisherErrorResponselistDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedFulfillment" type="{urn:com.macrovision:flexnet/operations}failedPublisherErrorResponseDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedPublisherErrorResponselistDataType", propOrder = {
    "failedFulfillment"
})
public class FailedPublisherErrorResponselistDataType {

    protected List<FailedPublisherErrorResponseDataType> failedFulfillment;

    /**
     * Gets the value of the failedFulfillment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedFulfillment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedFulfillment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedPublisherErrorResponseDataType }
     * 
     * 
     */
    public List<FailedPublisherErrorResponseDataType> getFailedFulfillment() {
        if (failedFulfillment == null) {
            failedFulfillment = new ArrayList<FailedPublisherErrorResponseDataType>();
        }
        return this.failedFulfillment;
    }

}
