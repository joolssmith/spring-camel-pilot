
package com.flexnet.operations.webservices.n;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for stopGapFulfillmentResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="stopGapFulfillmentResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusInfo" type="{urn:v1.webservices.operations.flexnet.com}StatusInfoType"/&gt;
 *         &lt;element name="responseData" type="{urn:v1.webservices.operations.flexnet.com}stopGapResponseDataType" minOccurs="0"/&gt;
 *         &lt;element name="failedData" type="{urn:v1.webservices.operations.flexnet.com}failedStopGapResponselistDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stopGapFulfillmentResponseType", propOrder = {
    "statusInfo",
    "responseData",
    "failedData"
})
public class StopGapFulfillmentResponseType {

    @XmlElement(required = true)
    protected StatusInfoType statusInfo;
    protected StopGapResponseDataType responseData;
    protected FailedStopGapResponselistDataType failedData;

    /**
     * Gets the value of the statusInfo property.
     * 
     * @return
     *     possible object is
     *     {@link StatusInfoType }
     *     
     */
    public StatusInfoType getStatusInfo() {
        return statusInfo;
    }

    /**
     * Sets the value of the statusInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusInfoType }
     *     
     */
    public void setStatusInfo(StatusInfoType value) {
        this.statusInfo = value;
    }

    /**
     * Gets the value of the responseData property.
     * 
     * @return
     *     possible object is
     *     {@link StopGapResponseDataType }
     *     
     */
    public StopGapResponseDataType getResponseData() {
        return responseData;
    }

    /**
     * Sets the value of the responseData property.
     * 
     * @param value
     *     allowed object is
     *     {@link StopGapResponseDataType }
     *     
     */
    public void setResponseData(StopGapResponseDataType value) {
        this.responseData = value;
    }

    /**
     * Gets the value of the failedData property.
     * 
     * @return
     *     possible object is
     *     {@link FailedStopGapResponselistDataType }
     *     
     */
    public FailedStopGapResponselistDataType getFailedData() {
        return failedData;
    }

    /**
     * Sets the value of the failedData property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailedStopGapResponselistDataType }
     *     
     */
    public void setFailedData(FailedStopGapResponselistDataType value) {
        this.failedData = value;
    }

}
