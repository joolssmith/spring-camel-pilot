
package com.flexnet.operations.webservices.n;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdChildLIFulfillmentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdChildLIFulfillmentDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="recordRefNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="fulfillmentInfo" type="{urn:v1.webservices.operations.flexnet.com}createdChildLIFulfillmentInfoType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdChildLIFulfillmentDataType", propOrder = {
    "recordRefNo",
    "fulfillmentInfo"
})
public class CreatedChildLIFulfillmentDataType {

    @XmlElement(required = true)
    protected BigInteger recordRefNo;
    @XmlElement(required = true)
    protected List<CreatedChildLIFulfillmentInfoType> fulfillmentInfo;

    /**
     * Gets the value of the recordRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRecordRefNo() {
        return recordRefNo;
    }

    /**
     * Sets the value of the recordRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRecordRefNo(BigInteger value) {
        this.recordRefNo = value;
    }

    /**
     * Gets the value of the fulfillmentInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fulfillmentInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFulfillmentInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CreatedChildLIFulfillmentInfoType }
     * 
     * 
     */
    public List<CreatedChildLIFulfillmentInfoType> getFulfillmentInfo() {
        if (fulfillmentInfo == null) {
            fulfillmentInfo = new ArrayList<CreatedChildLIFulfillmentInfoType>();
        }
        return this.fulfillmentInfo;
    }

}
