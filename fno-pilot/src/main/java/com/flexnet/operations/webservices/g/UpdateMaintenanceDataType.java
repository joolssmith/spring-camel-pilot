
package com.flexnet.operations.webservices.g;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateMaintenanceDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateMaintenanceDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="maintenanceIdentifier" type="{urn:com.macrovision:flexnet/operations}maintenanceIdentifierType"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="allowUpgrades" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowUpsells" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="allowRenewals" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="partNumbers" type="{urn:com.macrovision:flexnet/operations}updatePartNumbersSimpleListType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateMaintenanceDataType", propOrder = {
    "maintenanceIdentifier",
    "name",
    "version",
    "description",
    "allowUpgrades",
    "allowUpsells",
    "allowRenewals",
    "partNumbers"
})
public class UpdateMaintenanceDataType {

    @XmlElement(required = true)
    protected MaintenanceIdentifierType maintenanceIdentifier;
    protected String name;
    protected String version;
    protected String description;
    protected Boolean allowUpgrades;
    protected Boolean allowUpsells;
    protected Boolean allowRenewals;
    protected UpdatePartNumbersSimpleListType partNumbers;

    /**
     * Gets the value of the maintenanceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public MaintenanceIdentifierType getMaintenanceIdentifier() {
        return maintenanceIdentifier;
    }

    /**
     * Sets the value of the maintenanceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintenanceIdentifierType }
     *     
     */
    public void setMaintenanceIdentifier(MaintenanceIdentifierType value) {
        this.maintenanceIdentifier = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the version property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Sets the value of the version property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the allowUpgrades property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUpgrades() {
        return allowUpgrades;
    }

    /**
     * Sets the value of the allowUpgrades property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUpgrades(Boolean value) {
        this.allowUpgrades = value;
    }

    /**
     * Gets the value of the allowUpsells property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowUpsells() {
        return allowUpsells;
    }

    /**
     * Sets the value of the allowUpsells property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowUpsells(Boolean value) {
        this.allowUpsells = value;
    }

    /**
     * Gets the value of the allowRenewals property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAllowRenewals() {
        return allowRenewals;
    }

    /**
     * Sets the value of the allowRenewals property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAllowRenewals(Boolean value) {
        this.allowRenewals = value;
    }

    /**
     * Gets the value of the partNumbers property.
     * 
     * @return
     *     possible object is
     *     {@link UpdatePartNumbersSimpleListType }
     *     
     */
    public UpdatePartNumbersSimpleListType getPartNumbers() {
        return partNumbers;
    }

    /**
     * Sets the value of the partNumbers property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdatePartNumbersSimpleListType }
     *     
     */
    public void setPartNumbers(UpdatePartNumbersSimpleListType value) {
        this.partNumbers = value;
    }

}
