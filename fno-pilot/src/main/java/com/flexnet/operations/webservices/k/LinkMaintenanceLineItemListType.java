
package com.flexnet.operations.webservices.k;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for linkMaintenanceLineItemListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="linkMaintenanceLineItemListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="linkMaintenanceLineItem" type="{urn:com.macrovision:flexnet/operations}linkMaintenanceLineItemDataType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "linkMaintenanceLineItemListType", propOrder = {
    "linkMaintenanceLineItem"
})
public class LinkMaintenanceLineItemListType {

    @XmlElement(required = true)
    protected List<LinkMaintenanceLineItemDataType> linkMaintenanceLineItem;

    /**
     * Gets the value of the linkMaintenanceLineItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkMaintenanceLineItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkMaintenanceLineItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LinkMaintenanceLineItemDataType }
     * 
     * 
     */
    public List<LinkMaintenanceLineItemDataType> getLinkMaintenanceLineItem() {
        if (linkMaintenanceLineItem == null) {
            linkMaintenanceLineItem = new ArrayList<LinkMaintenanceLineItemDataType>();
        }
        return this.linkMaintenanceLineItem;
    }

}
