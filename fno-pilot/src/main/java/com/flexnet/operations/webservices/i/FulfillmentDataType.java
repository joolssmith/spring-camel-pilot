
package com.flexnet.operations.webservices.i;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for fulfillmentDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fulfillmentDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entitlementIdentifier" type="{urn:com.macrovision:flexnet/operations}entitlementIdentifierType"/&gt;
 *         &lt;element name="fulfillmentIdentifier" type="{urn:com.macrovision:flexnet/operations}fulfillmentIdentifierType"/&gt;
 *         &lt;element name="fulfillmentType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="lineItem" type="{urn:com.macrovision:flexnet/operations}entitlementLineItemIdentifierType"/&gt;
 *         &lt;element name="product" type="{urn:com.macrovision:flexnet/operations}productIdentifierType"/&gt;
 *         &lt;element name="soldTo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="shipToEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="shipToAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="serverIds" type="{urn:com.macrovision:flexnet/operations}ServerIDsType" minOccurs="0"/&gt;
 *         &lt;element name="nodeIds" type="{urn:com.macrovision:flexnet/operations}NodeIDsType" minOccurs="0"/&gt;
 *         &lt;element name="customHost" type="{urn:com.macrovision:flexnet/operations}CustomHostIDType" minOccurs="0"/&gt;
 *         &lt;element name="fulfilledCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="overDraftCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fulfillDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="fulfillDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="isPermanent" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="startDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="licenseText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="binaryLicense" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="consolidatedHostLicense" type="{urn:com.macrovision:flexnet/operations}consolidatedHostLicenseDataType" minOccurs="0"/&gt;
 *         &lt;element name="supportAction" type="{urn:com.macrovision:flexnet/operations}SupportLicenseType" minOccurs="0"/&gt;
 *         &lt;element name="lastModifiedDateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="parentFulfillmentId" type="{urn:com.macrovision:flexnet/operations}fulfillmentIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseTechnology" type="{urn:com.macrovision:flexnet/operations}licenseTechnologyIdentifierType" minOccurs="0"/&gt;
 *         &lt;element name="licenseModelAttributes" type="{urn:com.macrovision:flexnet/operations}attributeDescriptorDataType" minOccurs="0"/&gt;
 *         &lt;element name="state" type="{urn:com.macrovision:flexnet/operations}StateType" minOccurs="0"/&gt;
 *         &lt;element name="fulfillmentSource" type="{urn:com.macrovision:flexnet/operations}FulfillmentSourceType" minOccurs="0"/&gt;
 *         &lt;element name="licenseFiles" type="{urn:com.macrovision:flexnet/operations}licenseFileDataListType" minOccurs="0"/&gt;
 *         &lt;element name="entitledProducts" type="{urn:com.macrovision:flexnet/operations}entitledProductDataListType" minOccurs="0"/&gt;
 *         &lt;element name="activationType" type="{urn:com.macrovision:flexnet/operations}ActivationType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fulfillmentDataType", propOrder = {
    "entitlementIdentifier",
    "fulfillmentIdentifier",
    "fulfillmentType",
    "lineItem",
    "product",
    "soldTo",
    "shipToEmail",
    "shipToAddress",
    "serverIds",
    "nodeIds",
    "customHost",
    "fulfilledCount",
    "overDraftCount",
    "fulfillDate",
    "fulfillDateTime",
    "isPermanent",
    "startDate",
    "expirationDate",
    "licenseText",
    "binaryLicense",
    "consolidatedHostLicense",
    "supportAction",
    "lastModifiedDateTime",
    "parentFulfillmentId",
    "licenseTechnology",
    "licenseModelAttributes",
    "state",
    "fulfillmentSource",
    "licenseFiles",
    "entitledProducts",
    "activationType"
})
public class FulfillmentDataType {

    @XmlElement(required = true)
    protected EntitlementIdentifierType entitlementIdentifier;
    @XmlElement(required = true)
    protected FulfillmentIdentifierType fulfillmentIdentifier;
    @XmlElement(required = true)
    protected String fulfillmentType;
    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType lineItem;
    @XmlElement(required = true)
    protected ProductIdentifierType product;
    @XmlElement(required = true)
    protected String soldTo;
    protected String shipToEmail;
    protected String shipToAddress;
    protected ServerIDsType serverIds;
    protected NodeIDsType nodeIds;
    protected CustomHostIDType customHost;
    @XmlElement(required = true)
    protected String fulfilledCount;
    @XmlElement(required = true)
    protected String overDraftCount;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fulfillDate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fulfillDateTime;
    protected boolean isPermanent;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar startDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDate;
    protected String licenseText;
    protected byte[] binaryLicense;
    protected ConsolidatedHostLicenseDataType consolidatedHostLicense;
    @XmlSchemaType(name = "NMTOKEN")
    protected SupportLicenseType supportAction;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar lastModifiedDateTime;
    protected FulfillmentIdentifierType parentFulfillmentId;
    protected LicenseTechnologyIdentifierType licenseTechnology;
    protected AttributeDescriptorDataType licenseModelAttributes;
    @XmlSchemaType(name = "NMTOKEN")
    protected StateType state;
    @XmlSchemaType(name = "NMTOKEN")
    protected FulfillmentSourceType fulfillmentSource;
    protected LicenseFileDataListType licenseFiles;
    protected EntitledProductDataListType entitledProducts;
    @XmlSchemaType(name = "NMTOKEN")
    protected ActivationType activationType;

    /**
     * Gets the value of the entitlementIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public EntitlementIdentifierType getEntitlementIdentifier() {
        return entitlementIdentifier;
    }

    /**
     * Sets the value of the entitlementIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementIdentifierType }
     *     
     */
    public void setEntitlementIdentifier(EntitlementIdentifierType value) {
        this.entitlementIdentifier = value;
    }

    /**
     * Gets the value of the fulfillmentIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getFulfillmentIdentifier() {
        return fulfillmentIdentifier;
    }

    /**
     * Sets the value of the fulfillmentIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setFulfillmentIdentifier(FulfillmentIdentifierType value) {
        this.fulfillmentIdentifier = value;
    }

    /**
     * Gets the value of the fulfillmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfillmentType() {
        return fulfillmentType;
    }

    /**
     * Sets the value of the fulfillmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfillmentType(String value) {
        this.fulfillmentType = value;
    }

    /**
     * Gets the value of the lineItem property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getLineItem() {
        return lineItem;
    }

    /**
     * Sets the value of the lineItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setLineItem(EntitlementLineItemIdentifierType value) {
        this.lineItem = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdentifierType }
     *     
     */
    public ProductIdentifierType getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdentifierType }
     *     
     */
    public void setProduct(ProductIdentifierType value) {
        this.product = value;
    }

    /**
     * Gets the value of the soldTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoldTo() {
        return soldTo;
    }

    /**
     * Sets the value of the soldTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoldTo(String value) {
        this.soldTo = value;
    }

    /**
     * Gets the value of the shipToEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToEmail() {
        return shipToEmail;
    }

    /**
     * Sets the value of the shipToEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToEmail(String value) {
        this.shipToEmail = value;
    }

    /**
     * Gets the value of the shipToAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipToAddress() {
        return shipToAddress;
    }

    /**
     * Sets the value of the shipToAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipToAddress(String value) {
        this.shipToAddress = value;
    }

    /**
     * Gets the value of the serverIds property.
     * 
     * @return
     *     possible object is
     *     {@link ServerIDsType }
     *     
     */
    public ServerIDsType getServerIds() {
        return serverIds;
    }

    /**
     * Sets the value of the serverIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link ServerIDsType }
     *     
     */
    public void setServerIds(ServerIDsType value) {
        this.serverIds = value;
    }

    /**
     * Gets the value of the nodeIds property.
     * 
     * @return
     *     possible object is
     *     {@link NodeIDsType }
     *     
     */
    public NodeIDsType getNodeIds() {
        return nodeIds;
    }

    /**
     * Sets the value of the nodeIds property.
     * 
     * @param value
     *     allowed object is
     *     {@link NodeIDsType }
     *     
     */
    public void setNodeIds(NodeIDsType value) {
        this.nodeIds = value;
    }

    /**
     * Gets the value of the customHost property.
     * 
     * @return
     *     possible object is
     *     {@link CustomHostIDType }
     *     
     */
    public CustomHostIDType getCustomHost() {
        return customHost;
    }

    /**
     * Sets the value of the customHost property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomHostIDType }
     *     
     */
    public void setCustomHost(CustomHostIDType value) {
        this.customHost = value;
    }

    /**
     * Gets the value of the fulfilledCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFulfilledCount() {
        return fulfilledCount;
    }

    /**
     * Sets the value of the fulfilledCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFulfilledCount(String value) {
        this.fulfilledCount = value;
    }

    /**
     * Gets the value of the overDraftCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverDraftCount() {
        return overDraftCount;
    }

    /**
     * Sets the value of the overDraftCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverDraftCount(String value) {
        this.overDraftCount = value;
    }

    /**
     * Gets the value of the fulfillDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFulfillDate() {
        return fulfillDate;
    }

    /**
     * Sets the value of the fulfillDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFulfillDate(XMLGregorianCalendar value) {
        this.fulfillDate = value;
    }

    /**
     * Gets the value of the fulfillDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFulfillDateTime() {
        return fulfillDateTime;
    }

    /**
     * Sets the value of the fulfillDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFulfillDateTime(XMLGregorianCalendar value) {
        this.fulfillDateTime = value;
    }

    /**
     * Gets the value of the isPermanent property.
     * 
     */
    public boolean isIsPermanent() {
        return isPermanent;
    }

    /**
     * Sets the value of the isPermanent property.
     * 
     */
    public void setIsPermanent(boolean value) {
        this.isPermanent = value;
    }

    /**
     * Gets the value of the startDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStartDate() {
        return startDate;
    }

    /**
     * Sets the value of the startDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStartDate(XMLGregorianCalendar value) {
        this.startDate = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the licenseText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseText() {
        return licenseText;
    }

    /**
     * Sets the value of the licenseText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseText(String value) {
        this.licenseText = value;
    }

    /**
     * Gets the value of the binaryLicense property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBinaryLicense() {
        return binaryLicense;
    }

    /**
     * Sets the value of the binaryLicense property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBinaryLicense(byte[] value) {
        this.binaryLicense = value;
    }

    /**
     * Gets the value of the consolidatedHostLicense property.
     * 
     * @return
     *     possible object is
     *     {@link ConsolidatedHostLicenseDataType }
     *     
     */
    public ConsolidatedHostLicenseDataType getConsolidatedHostLicense() {
        return consolidatedHostLicense;
    }

    /**
     * Sets the value of the consolidatedHostLicense property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConsolidatedHostLicenseDataType }
     *     
     */
    public void setConsolidatedHostLicense(ConsolidatedHostLicenseDataType value) {
        this.consolidatedHostLicense = value;
    }

    /**
     * Gets the value of the supportAction property.
     * 
     * @return
     *     possible object is
     *     {@link SupportLicenseType }
     *     
     */
    public SupportLicenseType getSupportAction() {
        return supportAction;
    }

    /**
     * Sets the value of the supportAction property.
     * 
     * @param value
     *     allowed object is
     *     {@link SupportLicenseType }
     *     
     */
    public void setSupportAction(SupportLicenseType value) {
        this.supportAction = value;
    }

    /**
     * Gets the value of the lastModifiedDateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    /**
     * Sets the value of the lastModifiedDateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLastModifiedDateTime(XMLGregorianCalendar value) {
        this.lastModifiedDateTime = value;
    }

    /**
     * Gets the value of the parentFulfillmentId property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public FulfillmentIdentifierType getParentFulfillmentId() {
        return parentFulfillmentId;
    }

    /**
     * Sets the value of the parentFulfillmentId property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentIdentifierType }
     *     
     */
    public void setParentFulfillmentId(FulfillmentIdentifierType value) {
        this.parentFulfillmentId = value;
    }

    /**
     * Gets the value of the licenseTechnology property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public LicenseTechnologyIdentifierType getLicenseTechnology() {
        return licenseTechnology;
    }

    /**
     * Sets the value of the licenseTechnology property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseTechnologyIdentifierType }
     *     
     */
    public void setLicenseTechnology(LicenseTechnologyIdentifierType value) {
        this.licenseTechnology = value;
    }

    /**
     * Gets the value of the licenseModelAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getLicenseModelAttributes() {
        return licenseModelAttributes;
    }

    /**
     * Sets the value of the licenseModelAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setLicenseModelAttributes(AttributeDescriptorDataType value) {
        this.licenseModelAttributes = value;
    }

    /**
     * Gets the value of the state property.
     * 
     * @return
     *     possible object is
     *     {@link StateType }
     *     
     */
    public StateType getState() {
        return state;
    }

    /**
     * Sets the value of the state property.
     * 
     * @param value
     *     allowed object is
     *     {@link StateType }
     *     
     */
    public void setState(StateType value) {
        this.state = value;
    }

    /**
     * Gets the value of the fulfillmentSource property.
     * 
     * @return
     *     possible object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public FulfillmentSourceType getFulfillmentSource() {
        return fulfillmentSource;
    }

    /**
     * Sets the value of the fulfillmentSource property.
     * 
     * @param value
     *     allowed object is
     *     {@link FulfillmentSourceType }
     *     
     */
    public void setFulfillmentSource(FulfillmentSourceType value) {
        this.fulfillmentSource = value;
    }

    /**
     * Gets the value of the licenseFiles property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public LicenseFileDataListType getLicenseFiles() {
        return licenseFiles;
    }

    /**
     * Sets the value of the licenseFiles property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseFileDataListType }
     *     
     */
    public void setLicenseFiles(LicenseFileDataListType value) {
        this.licenseFiles = value;
    }

    /**
     * Gets the value of the entitledProducts property.
     * 
     * @return
     *     possible object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public EntitledProductDataListType getEntitledProducts() {
        return entitledProducts;
    }

    /**
     * Sets the value of the entitledProducts property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public void setEntitledProducts(EntitledProductDataListType value) {
        this.entitledProducts = value;
    }

    /**
     * Gets the value of the activationType property.
     * 
     * @return
     *     possible object is
     *     {@link ActivationType }
     *     
     */
    public ActivationType getActivationType() {
        return activationType;
    }

    /**
     * Sets the value of the activationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ActivationType }
     *     
     */
    public void setActivationType(ActivationType value) {
        this.activationType = value;
    }

}
