
package com.flexnet.operations.webservices.k;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="allowedCount" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="policyTerm" type="{urn:com.macrovision:flexnet/operations}policyTermType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyDataType", propOrder = {
    "allowedCount",
    "policyTerm"
})
public class PolicyDataType {

    @XmlElement(required = true)
    protected BigInteger allowedCount;
    @XmlElement(required = true)
    protected PolicyTermType policyTerm;

    /**
     * Gets the value of the allowedCount property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAllowedCount() {
        return allowedCount;
    }

    /**
     * Sets the value of the allowedCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAllowedCount(BigInteger value) {
        this.allowedCount = value;
    }

    /**
     * Gets the value of the policyTerm property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyTermType }
     *     
     */
    public PolicyTermType getPolicyTerm() {
        return policyTerm;
    }

    /**
     * Sets the value of the policyTerm property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyTermType }
     *     
     */
    public void setPolicyTerm(PolicyTermType value) {
        this.policyTerm = value;
    }

}
