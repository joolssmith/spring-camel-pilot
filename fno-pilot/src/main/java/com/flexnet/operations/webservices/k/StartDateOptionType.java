
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StartDateOptionType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StartDateOptionType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="DEFINE_NOW"/&gt;
 *     &lt;enumeration value="DEFINE_AT_FIRST_ACTIVATION"/&gt;
 *     &lt;enumeration value="DEFINE_AT_EACH_ACTIVATION"/&gt;
 *     &lt;enumeration value="DEFINE_ACTDATE_AT_FIRST_ACTIVATION"/&gt;
 *     &lt;enumeration value="DEFINE_ACTDATE_AT_EACH_ACTIVATION"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "StartDateOptionType")
@XmlEnum
public enum StartDateOptionType {

    DEFINE_NOW,
    DEFINE_AT_FIRST_ACTIVATION,
    DEFINE_AT_EACH_ACTIVATION,
    DEFINE_ACTDATE_AT_FIRST_ACTIVATION,
    DEFINE_ACTDATE_AT_EACH_ACTIVATION;

    public String value() {
        return name();
    }

    public static StartDateOptionType fromValue(String v) {
        return valueOf(v);
    }

}
