
package com.flexnet.operations.webservices.o;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.flexnet.operations.webservices.o package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateBulkEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementRequest");
    private final static QName _CreateBulkEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createBulkEntitlementResponse");
    private final static QName _CreateSimpleEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementRequest");
    private final static QName _CreateSimpleEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createSimpleEntitlementResponse");
    private final static QName _DeleteEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementRequest");
    private final static QName _DeleteEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteEntitlementResponse");
    private final static QName _AddWebRegKeyRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyRequest");
    private final static QName _AddWebRegKeyResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "addWebRegKeyResponse");
    private final static QName _UpdateBulkEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementRequest");
    private final static QName _UpdateBulkEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "updateBulkEntitlementResponse");
    private final static QName _UpdateSimpleEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementRequest");
    private final static QName _UpdateSimpleEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "updateSimpleEntitlementResponse");
    private final static QName _CreateEntitlementLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createEntitlementLineItemRequest");
    private final static QName _CreateEntitlementLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "createEntitlementLineItemResponse");
    private final static QName _ReplaceEntitlementLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "replaceEntitlementLineItemRequest");
    private final static QName _ReplaceEntitlementLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "replaceEntitlementLineItemResponse");
    private final static QName _RemoveEntitlementLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemRequest");
    private final static QName _RemoveEntitlementLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "removeEntitlementLineItemResponse");
    private final static QName _UpdateEntitlementLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemRequest");
    private final static QName _UpdateEntitlementLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "updateEntitlementLineItemResponse");
    private final static QName _SearchEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementRequest");
    private final static QName _SearchEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementResponse");
    private final static QName _GetBulkEntitlementPropertiesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesRequest");
    private final static QName _GetBulkEntitlementPropertiesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementPropertiesResponse");
    private final static QName _GetBulkEntitlementCountRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountRequest");
    private final static QName _GetBulkEntitlementCountResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getBulkEntitlementCountResponse");
    private final static QName _SearchActivatableItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemRequest");
    private final static QName _SearchActivatableItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "searchActivatableItemResponse");
    private final static QName _SearchEntitlementLineItemPropertiesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesRequest");
    private final static QName _SearchEntitlementLineItemPropertiesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "searchEntitlementLineItemPropertiesResponse");
    private final static QName _GetEntitlementCountRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountRequest");
    private final static QName _GetEntitlementCountResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementCountResponse");
    private final static QName _GetActivatableItemCountRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountRequest");
    private final static QName _GetActivatableItemCountResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getActivatableItemCountResponse");
    private final static QName _GetExactAvailableCountRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountRequest");
    private final static QName _GetExactAvailableCountResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getExactAvailableCountResponse");
    private final static QName _SetEntitlementStateRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateRequest");
    private final static QName _SetEntitlementStateResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setEntitlementStateResponse");
    private final static QName _GetWebRegKeyCountRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountRequest");
    private final static QName _GetWebRegKeyCountResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeyCountResponse");
    private final static QName _GetWebRegKeysQueryRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryRequest");
    private final static QName _GetWebRegKeysQueryResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getWebRegKeysQueryResponse");
    private final static QName _GetEntitlementAttributesRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesRequest");
    private final static QName _GetEntitlementAttributesResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getEntitlementAttributesResponse");
    private final static QName _RenewLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "renewLicenseRequest");
    private final static QName _RenewLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "renewLicenseResponse");
    private final static QName _UpgradeLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upgradeLicenseRequest");
    private final static QName _UpgradeLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upgradeLicenseResponse");
    private final static QName _UpsellLicenseRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upsellLicenseRequest");
    private final static QName _UpsellLicenseResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "upsellLicenseResponse");
    private final static QName _MapEntitlementsToUserRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserRequest");
    private final static QName _MapEntitlementsToUserResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "mapEntitlementsToUserResponse");
    private final static QName _EmailEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementRequest");
    private final static QName _EmailEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailEntitlementResponse");
    private final static QName _EmailActivatableItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemRequest");
    private final static QName _EmailActivatableItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "emailActivatableItemResponse");
    private final static QName _SetLineItemStateRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateRequest");
    private final static QName _SetLineItemStateResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setLineItemStateResponse");
    private final static QName _SetMaintenanceLineItemStateRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateRequest");
    private final static QName _SetMaintenanceLineItemStateResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "setMaintenanceLineItemStateResponse");
    private final static QName _DeleteWebRegKeyRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyRequest");
    private final static QName _DeleteWebRegKeyResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteWebRegKeyResponse");
    private final static QName _MergeEntitlementsRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsRequest");
    private final static QName _MergeEntitlementsResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "mergeEntitlementsResponse");
    private final static QName _TransferEntitlementsRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsRequest");
    private final static QName _TransferEntitlementsResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "transferEntitlementsResponse");
    private final static QName _TransferLineItemsRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsRequest");
    private final static QName _TransferLineItemsResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "transferLineItemsResponse");
    private final static QName _GetStateChangeHistoryRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryRequest");
    private final static QName _GetStateChangeHistoryResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getStateChangeHistoryResponse");
    private final static QName _LinkMaintenanceLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemRequest");
    private final static QName _LinkMaintenanceLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "linkMaintenanceLineItemResponse");
    private final static QName _SplitLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemRequest");
    private final static QName _SplitLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "splitLineItemResponse");
    private final static QName _SplitBulkEntitlementRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementRequest");
    private final static QName _SplitBulkEntitlementResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "splitBulkEntitlementResponse");
    private final static QName _GetMatchingLineItemsRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsRequest");
    private final static QName _GetMatchingLineItemsResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getMatchingLineItemsResponse");
    private final static QName _GetMatchingBulkEntsRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsRequest");
    private final static QName _GetMatchingBulkEntsResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "getMatchingBulkEntsResponse");
    private final static QName _DeleteMaintenanceLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemRequest");
    private final static QName _DeleteMaintenanceLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "deleteMaintenanceLineItemResponse");
    private final static QName _UnlinkMaintenanceLineItemRequest_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemRequest");
    private final static QName _UnlinkMaintenanceLineItemResponse_QNAME = new QName("urn:v1.webservices.operations.flexnet.com", "unlinkMaintenanceLineItemResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.flexnet.operations.webservices.o
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateBulkEntitlementRequestType }
     * 
     */
    public CreateBulkEntitlementRequestType createCreateBulkEntitlementRequestType() {
        return new CreateBulkEntitlementRequestType();
    }

    /**
     * Create an instance of {@link CreateBulkEntitlementResponseType }
     * 
     */
    public CreateBulkEntitlementResponseType createCreateBulkEntitlementResponseType() {
        return new CreateBulkEntitlementResponseType();
    }

    /**
     * Create an instance of {@link CreateSimpleEntitlementRequestType }
     * 
     */
    public CreateSimpleEntitlementRequestType createCreateSimpleEntitlementRequestType() {
        return new CreateSimpleEntitlementRequestType();
    }

    /**
     * Create an instance of {@link CreateSimpleEntitlementResponseType }
     * 
     */
    public CreateSimpleEntitlementResponseType createCreateSimpleEntitlementResponseType() {
        return new CreateSimpleEntitlementResponseType();
    }

    /**
     * Create an instance of {@link DeleteEntitlementRequestType }
     * 
     */
    public DeleteEntitlementRequestType createDeleteEntitlementRequestType() {
        return new DeleteEntitlementRequestType();
    }

    /**
     * Create an instance of {@link DeleteEntitlementResponseType }
     * 
     */
    public DeleteEntitlementResponseType createDeleteEntitlementResponseType() {
        return new DeleteEntitlementResponseType();
    }

    /**
     * Create an instance of {@link AddWebRegKeyRequestType }
     * 
     */
    public AddWebRegKeyRequestType createAddWebRegKeyRequestType() {
        return new AddWebRegKeyRequestType();
    }

    /**
     * Create an instance of {@link AddWebRegKeyResponseType }
     * 
     */
    public AddWebRegKeyResponseType createAddWebRegKeyResponseType() {
        return new AddWebRegKeyResponseType();
    }

    /**
     * Create an instance of {@link UpdateBulkEntitlementRequestType }
     * 
     */
    public UpdateBulkEntitlementRequestType createUpdateBulkEntitlementRequestType() {
        return new UpdateBulkEntitlementRequestType();
    }

    /**
     * Create an instance of {@link UpdateBulkEntitlementResponseType }
     * 
     */
    public UpdateBulkEntitlementResponseType createUpdateBulkEntitlementResponseType() {
        return new UpdateBulkEntitlementResponseType();
    }

    /**
     * Create an instance of {@link UpdateSimpleEntitlementRequestType }
     * 
     */
    public UpdateSimpleEntitlementRequestType createUpdateSimpleEntitlementRequestType() {
        return new UpdateSimpleEntitlementRequestType();
    }

    /**
     * Create an instance of {@link UpdateSimpleEntitlementResponseType }
     * 
     */
    public UpdateSimpleEntitlementResponseType createUpdateSimpleEntitlementResponseType() {
        return new UpdateSimpleEntitlementResponseType();
    }

    /**
     * Create an instance of {@link AddOnlyEntitlementLineItemRequestType }
     * 
     */
    public AddOnlyEntitlementLineItemRequestType createAddOnlyEntitlementLineItemRequestType() {
        return new AddOnlyEntitlementLineItemRequestType();
    }

    /**
     * Create an instance of {@link AddOnlyEntitlementLineItemResponseType }
     * 
     */
    public AddOnlyEntitlementLineItemResponseType createAddOnlyEntitlementLineItemResponseType() {
        return new AddOnlyEntitlementLineItemResponseType();
    }

    /**
     * Create an instance of {@link ReplaceOnlyEntitlementLineItemRequestType }
     * 
     */
    public ReplaceOnlyEntitlementLineItemRequestType createReplaceOnlyEntitlementLineItemRequestType() {
        return new ReplaceOnlyEntitlementLineItemRequestType();
    }

    /**
     * Create an instance of {@link ReplaceOnlyEntitlementLineItemResponseType }
     * 
     */
    public ReplaceOnlyEntitlementLineItemResponseType createReplaceOnlyEntitlementLineItemResponseType() {
        return new ReplaceOnlyEntitlementLineItemResponseType();
    }

    /**
     * Create an instance of {@link RemoveEntitlementLineItemRequestType }
     * 
     */
    public RemoveEntitlementLineItemRequestType createRemoveEntitlementLineItemRequestType() {
        return new RemoveEntitlementLineItemRequestType();
    }

    /**
     * Create an instance of {@link RemoveEntitlementLineItemResponseType }
     * 
     */
    public RemoveEntitlementLineItemResponseType createRemoveEntitlementLineItemResponseType() {
        return new RemoveEntitlementLineItemResponseType();
    }

    /**
     * Create an instance of {@link UpdateEntitlementLineItemRequestType }
     * 
     */
    public UpdateEntitlementLineItemRequestType createUpdateEntitlementLineItemRequestType() {
        return new UpdateEntitlementLineItemRequestType();
    }

    /**
     * Create an instance of {@link UpdateEntitlementLineItemResponseType }
     * 
     */
    public UpdateEntitlementLineItemResponseType createUpdateEntitlementLineItemResponseType() {
        return new UpdateEntitlementLineItemResponseType();
    }

    /**
     * Create an instance of {@link SearchEntitlementRequestType }
     * 
     */
    public SearchEntitlementRequestType createSearchEntitlementRequestType() {
        return new SearchEntitlementRequestType();
    }

    /**
     * Create an instance of {@link SearchEntitlementResponseType }
     * 
     */
    public SearchEntitlementResponseType createSearchEntitlementResponseType() {
        return new SearchEntitlementResponseType();
    }

    /**
     * Create an instance of {@link GetBulkEntitlementPropertiesRequestType }
     * 
     */
    public GetBulkEntitlementPropertiesRequestType createGetBulkEntitlementPropertiesRequestType() {
        return new GetBulkEntitlementPropertiesRequestType();
    }

    /**
     * Create an instance of {@link GetBulkEntitlementPropertiesResponseType }
     * 
     */
    public GetBulkEntitlementPropertiesResponseType createGetBulkEntitlementPropertiesResponseType() {
        return new GetBulkEntitlementPropertiesResponseType();
    }

    /**
     * Create an instance of {@link GetBulkEntitlementCountRequestType }
     * 
     */
    public GetBulkEntitlementCountRequestType createGetBulkEntitlementCountRequestType() {
        return new GetBulkEntitlementCountRequestType();
    }

    /**
     * Create an instance of {@link GetBulkEntitlementCountResponseType }
     * 
     */
    public GetBulkEntitlementCountResponseType createGetBulkEntitlementCountResponseType() {
        return new GetBulkEntitlementCountResponseType();
    }

    /**
     * Create an instance of {@link SearchActivatableItemRequestType }
     * 
     */
    public SearchActivatableItemRequestType createSearchActivatableItemRequestType() {
        return new SearchActivatableItemRequestType();
    }

    /**
     * Create an instance of {@link SearchActivatableItemResponseType }
     * 
     */
    public SearchActivatableItemResponseType createSearchActivatableItemResponseType() {
        return new SearchActivatableItemResponseType();
    }

    /**
     * Create an instance of {@link SearchEntitlementLineItemPropertiesRequestType }
     * 
     */
    public SearchEntitlementLineItemPropertiesRequestType createSearchEntitlementLineItemPropertiesRequestType() {
        return new SearchEntitlementLineItemPropertiesRequestType();
    }

    /**
     * Create an instance of {@link SearchEntitlementLineItemPropertiesResponseType }
     * 
     */
    public SearchEntitlementLineItemPropertiesResponseType createSearchEntitlementLineItemPropertiesResponseType() {
        return new SearchEntitlementLineItemPropertiesResponseType();
    }

    /**
     * Create an instance of {@link GetEntitlementCountRequestType }
     * 
     */
    public GetEntitlementCountRequestType createGetEntitlementCountRequestType() {
        return new GetEntitlementCountRequestType();
    }

    /**
     * Create an instance of {@link GetEntitlementCountResponseType }
     * 
     */
    public GetEntitlementCountResponseType createGetEntitlementCountResponseType() {
        return new GetEntitlementCountResponseType();
    }

    /**
     * Create an instance of {@link GetActivatableItemCountRequestType }
     * 
     */
    public GetActivatableItemCountRequestType createGetActivatableItemCountRequestType() {
        return new GetActivatableItemCountRequestType();
    }

    /**
     * Create an instance of {@link GetActivatableItemCountResponseType }
     * 
     */
    public GetActivatableItemCountResponseType createGetActivatableItemCountResponseType() {
        return new GetActivatableItemCountResponseType();
    }

    /**
     * Create an instance of {@link GetExactAvailableCountRequestType }
     * 
     */
    public GetExactAvailableCountRequestType createGetExactAvailableCountRequestType() {
        return new GetExactAvailableCountRequestType();
    }

    /**
     * Create an instance of {@link GetExactAvailableCountResponseType }
     * 
     */
    public GetExactAvailableCountResponseType createGetExactAvailableCountResponseType() {
        return new GetExactAvailableCountResponseType();
    }

    /**
     * Create an instance of {@link SetEntitlementStateRequestType }
     * 
     */
    public SetEntitlementStateRequestType createSetEntitlementStateRequestType() {
        return new SetEntitlementStateRequestType();
    }

    /**
     * Create an instance of {@link SetEntitlementStateResponseType }
     * 
     */
    public SetEntitlementStateResponseType createSetEntitlementStateResponseType() {
        return new SetEntitlementStateResponseType();
    }

    /**
     * Create an instance of {@link GetWebRegKeyCountRequestType }
     * 
     */
    public GetWebRegKeyCountRequestType createGetWebRegKeyCountRequestType() {
        return new GetWebRegKeyCountRequestType();
    }

    /**
     * Create an instance of {@link GetWebRegKeyCountResponseType }
     * 
     */
    public GetWebRegKeyCountResponseType createGetWebRegKeyCountResponseType() {
        return new GetWebRegKeyCountResponseType();
    }

    /**
     * Create an instance of {@link GetWebRegKeysQueryRequestType }
     * 
     */
    public GetWebRegKeysQueryRequestType createGetWebRegKeysQueryRequestType() {
        return new GetWebRegKeysQueryRequestType();
    }

    /**
     * Create an instance of {@link GetWebRegKeysQueryResponseType }
     * 
     */
    public GetWebRegKeysQueryResponseType createGetWebRegKeysQueryResponseType() {
        return new GetWebRegKeysQueryResponseType();
    }

    /**
     * Create an instance of {@link GetEntitlementAttributesRequestType }
     * 
     */
    public GetEntitlementAttributesRequestType createGetEntitlementAttributesRequestType() {
        return new GetEntitlementAttributesRequestType();
    }

    /**
     * Create an instance of {@link GetEntitlementAttributesResponseType }
     * 
     */
    public GetEntitlementAttributesResponseType createGetEntitlementAttributesResponseType() {
        return new GetEntitlementAttributesResponseType();
    }

    /**
     * Create an instance of {@link RenewEntitlementRequestType }
     * 
     */
    public RenewEntitlementRequestType createRenewEntitlementRequestType() {
        return new RenewEntitlementRequestType();
    }

    /**
     * Create an instance of {@link RenewEntitlementResponseType }
     * 
     */
    public RenewEntitlementResponseType createRenewEntitlementResponseType() {
        return new RenewEntitlementResponseType();
    }

    /**
     * Create an instance of {@link EntitlementLifeCycleRequestType }
     * 
     */
    public EntitlementLifeCycleRequestType createEntitlementLifeCycleRequestType() {
        return new EntitlementLifeCycleRequestType();
    }

    /**
     * Create an instance of {@link EntitlementLifeCycleResponseType }
     * 
     */
    public EntitlementLifeCycleResponseType createEntitlementLifeCycleResponseType() {
        return new EntitlementLifeCycleResponseType();
    }

    /**
     * Create an instance of {@link MapEntitlementsToUserRequestType }
     * 
     */
    public MapEntitlementsToUserRequestType createMapEntitlementsToUserRequestType() {
        return new MapEntitlementsToUserRequestType();
    }

    /**
     * Create an instance of {@link MapEntitlementsToUserResponseType }
     * 
     */
    public MapEntitlementsToUserResponseType createMapEntitlementsToUserResponseType() {
        return new MapEntitlementsToUserResponseType();
    }

    /**
     * Create an instance of {@link EmailEntitlementRequestType }
     * 
     */
    public EmailEntitlementRequestType createEmailEntitlementRequestType() {
        return new EmailEntitlementRequestType();
    }

    /**
     * Create an instance of {@link EmailEntitlementResponseType }
     * 
     */
    public EmailEntitlementResponseType createEmailEntitlementResponseType() {
        return new EmailEntitlementResponseType();
    }

    /**
     * Create an instance of {@link EmailActivatableItemRequestType }
     * 
     */
    public EmailActivatableItemRequestType createEmailActivatableItemRequestType() {
        return new EmailActivatableItemRequestType();
    }

    /**
     * Create an instance of {@link EmailActivatableItemResponseType }
     * 
     */
    public EmailActivatableItemResponseType createEmailActivatableItemResponseType() {
        return new EmailActivatableItemResponseType();
    }

    /**
     * Create an instance of {@link SetLineItemStateRequestType }
     * 
     */
    public SetLineItemStateRequestType createSetLineItemStateRequestType() {
        return new SetLineItemStateRequestType();
    }

    /**
     * Create an instance of {@link SetLineItemStateResponseType }
     * 
     */
    public SetLineItemStateResponseType createSetLineItemStateResponseType() {
        return new SetLineItemStateResponseType();
    }

    /**
     * Create an instance of {@link SetMaintenanceLineItemStateRequestType }
     * 
     */
    public SetMaintenanceLineItemStateRequestType createSetMaintenanceLineItemStateRequestType() {
        return new SetMaintenanceLineItemStateRequestType();
    }

    /**
     * Create an instance of {@link SetMaintenanceLineItemStateResponseType }
     * 
     */
    public SetMaintenanceLineItemStateResponseType createSetMaintenanceLineItemStateResponseType() {
        return new SetMaintenanceLineItemStateResponseType();
    }

    /**
     * Create an instance of {@link DeleteWebRegKeyRequestType }
     * 
     */
    public DeleteWebRegKeyRequestType createDeleteWebRegKeyRequestType() {
        return new DeleteWebRegKeyRequestType();
    }

    /**
     * Create an instance of {@link DeleteWebRegKeyResponseType }
     * 
     */
    public DeleteWebRegKeyResponseType createDeleteWebRegKeyResponseType() {
        return new DeleteWebRegKeyResponseType();
    }

    /**
     * Create an instance of {@link MergeEntitlementsRequestType }
     * 
     */
    public MergeEntitlementsRequestType createMergeEntitlementsRequestType() {
        return new MergeEntitlementsRequestType();
    }

    /**
     * Create an instance of {@link MergeEntitlementsResponseType }
     * 
     */
    public MergeEntitlementsResponseType createMergeEntitlementsResponseType() {
        return new MergeEntitlementsResponseType();
    }

    /**
     * Create an instance of {@link TransferEntitlementsRequestType }
     * 
     */
    public TransferEntitlementsRequestType createTransferEntitlementsRequestType() {
        return new TransferEntitlementsRequestType();
    }

    /**
     * Create an instance of {@link TransferEntitlementsResponseType }
     * 
     */
    public TransferEntitlementsResponseType createTransferEntitlementsResponseType() {
        return new TransferEntitlementsResponseType();
    }

    /**
     * Create an instance of {@link TransferLineItemsRequestType }
     * 
     */
    public TransferLineItemsRequestType createTransferLineItemsRequestType() {
        return new TransferLineItemsRequestType();
    }

    /**
     * Create an instance of {@link TransferLineItemsResponseType }
     * 
     */
    public TransferLineItemsResponseType createTransferLineItemsResponseType() {
        return new TransferLineItemsResponseType();
    }

    /**
     * Create an instance of {@link GetStateChangeHistoryRequestType }
     * 
     */
    public GetStateChangeHistoryRequestType createGetStateChangeHistoryRequestType() {
        return new GetStateChangeHistoryRequestType();
    }

    /**
     * Create an instance of {@link GetStateChangeHistoryResponseType }
     * 
     */
    public GetStateChangeHistoryResponseType createGetStateChangeHistoryResponseType() {
        return new GetStateChangeHistoryResponseType();
    }

    /**
     * Create an instance of {@link LinkMaintenanceLineItemRequestType }
     * 
     */
    public LinkMaintenanceLineItemRequestType createLinkMaintenanceLineItemRequestType() {
        return new LinkMaintenanceLineItemRequestType();
    }

    /**
     * Create an instance of {@link LinkMaintenanceLineItemResponseType }
     * 
     */
    public LinkMaintenanceLineItemResponseType createLinkMaintenanceLineItemResponseType() {
        return new LinkMaintenanceLineItemResponseType();
    }

    /**
     * Create an instance of {@link SplitLineItemRequestType }
     * 
     */
    public SplitLineItemRequestType createSplitLineItemRequestType() {
        return new SplitLineItemRequestType();
    }

    /**
     * Create an instance of {@link SplitLineItemResponseType }
     * 
     */
    public SplitLineItemResponseType createSplitLineItemResponseType() {
        return new SplitLineItemResponseType();
    }

    /**
     * Create an instance of {@link SplitBulkEntitlementRequestType }
     * 
     */
    public SplitBulkEntitlementRequestType createSplitBulkEntitlementRequestType() {
        return new SplitBulkEntitlementRequestType();
    }

    /**
     * Create an instance of {@link SplitBulkEntitlementResponseType }
     * 
     */
    public SplitBulkEntitlementResponseType createSplitBulkEntitlementResponseType() {
        return new SplitBulkEntitlementResponseType();
    }

    /**
     * Create an instance of {@link GetMatchingLineItemsRequestType }
     * 
     */
    public GetMatchingLineItemsRequestType createGetMatchingLineItemsRequestType() {
        return new GetMatchingLineItemsRequestType();
    }

    /**
     * Create an instance of {@link GetMatchingLineItemsResponseType }
     * 
     */
    public GetMatchingLineItemsResponseType createGetMatchingLineItemsResponseType() {
        return new GetMatchingLineItemsResponseType();
    }

    /**
     * Create an instance of {@link GetMatchingBulkEntsRequestType }
     * 
     */
    public GetMatchingBulkEntsRequestType createGetMatchingBulkEntsRequestType() {
        return new GetMatchingBulkEntsRequestType();
    }

    /**
     * Create an instance of {@link GetMatchingBulkEntsResponseType }
     * 
     */
    public GetMatchingBulkEntsResponseType createGetMatchingBulkEntsResponseType() {
        return new GetMatchingBulkEntsResponseType();
    }

    /**
     * Create an instance of {@link DeleteMaintenanceLineItemRequestType }
     * 
     */
    public DeleteMaintenanceLineItemRequestType createDeleteMaintenanceLineItemRequestType() {
        return new DeleteMaintenanceLineItemRequestType();
    }

    /**
     * Create an instance of {@link DeleteMaintenanceLineItemResponseType }
     * 
     */
    public DeleteMaintenanceLineItemResponseType createDeleteMaintenanceLineItemResponseType() {
        return new DeleteMaintenanceLineItemResponseType();
    }

    /**
     * Create an instance of {@link UnlinkMaintenanceLineItemRequestType }
     * 
     */
    public UnlinkMaintenanceLineItemRequestType createUnlinkMaintenanceLineItemRequestType() {
        return new UnlinkMaintenanceLineItemRequestType();
    }

    /**
     * Create an instance of {@link UnlinkMaintenanceLineItemResponseType }
     * 
     */
    public UnlinkMaintenanceLineItemResponseType createUnlinkMaintenanceLineItemResponseType() {
        return new UnlinkMaintenanceLineItemResponseType();
    }

    /**
     * Create an instance of {@link IdType }
     * 
     */
    public IdType createIdType() {
        return new IdType();
    }

    /**
     * Create an instance of {@link ProductPKType }
     * 
     */
    public ProductPKType createProductPKType() {
        return new ProductPKType();
    }

    /**
     * Create an instance of {@link ProductIdentifierType }
     * 
     */
    public ProductIdentifierType createProductIdentifierType() {
        return new ProductIdentifierType();
    }

    /**
     * Create an instance of {@link PartNumberPKType }
     * 
     */
    public PartNumberPKType createPartNumberPKType() {
        return new PartNumberPKType();
    }

    /**
     * Create an instance of {@link PartNumberIdentifierType }
     * 
     */
    public PartNumberIdentifierType createPartNumberIdentifierType() {
        return new PartNumberIdentifierType();
    }

    /**
     * Create an instance of {@link LicenseModelPKType }
     * 
     */
    public LicenseModelPKType createLicenseModelPKType() {
        return new LicenseModelPKType();
    }

    /**
     * Create an instance of {@link LicenseModelIdentifierType }
     * 
     */
    public LicenseModelIdentifierType createLicenseModelIdentifierType() {
        return new LicenseModelIdentifierType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorType }
     * 
     */
    public AttributeDescriptorType createAttributeDescriptorType() {
        return new AttributeDescriptorType();
    }

    /**
     * Create an instance of {@link AttributeDescriptorDataType }
     * 
     */
    public AttributeDescriptorDataType createAttributeDescriptorDataType() {
        return new AttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link PolicyTermType }
     * 
     */
    public PolicyTermType createPolicyTermType() {
        return new PolicyTermType();
    }

    /**
     * Create an instance of {@link PolicyDataType }
     * 
     */
    public PolicyDataType createPolicyDataType() {
        return new PolicyDataType();
    }

    /**
     * Create an instance of {@link ExtraActivationDataType }
     * 
     */
    public ExtraActivationDataType createExtraActivationDataType() {
        return new ExtraActivationDataType();
    }

    /**
     * Create an instance of {@link CancelLicensePolicyDataType }
     * 
     */
    public CancelLicensePolicyDataType createCancelLicensePolicyDataType() {
        return new CancelLicensePolicyDataType();
    }

    /**
     * Create an instance of {@link VirtualLicensePolicyDataType }
     * 
     */
    public VirtualLicensePolicyDataType createVirtualLicensePolicyDataType() {
        return new VirtualLicensePolicyDataType();
    }

    /**
     * Create an instance of {@link AdvancedReinstallPolicyType }
     * 
     */
    public AdvancedReinstallPolicyType createAdvancedReinstallPolicyType() {
        return new AdvancedReinstallPolicyType();
    }

    /**
     * Create an instance of {@link ReinstallPolicyDataType }
     * 
     */
    public ReinstallPolicyDataType createReinstallPolicyDataType() {
        return new ReinstallPolicyDataType();
    }

    /**
     * Create an instance of {@link AcpiGenerationIdLicensePolicyDataType }
     * 
     */
    public AcpiGenerationIdLicensePolicyDataType createAcpiGenerationIdLicensePolicyDataType() {
        return new AcpiGenerationIdLicensePolicyDataType();
    }

    /**
     * Create an instance of {@link PolicyAttributesListType }
     * 
     */
    public PolicyAttributesListType createPolicyAttributesListType() {
        return new PolicyAttributesListType();
    }

    /**
     * Create an instance of {@link DurationType }
     * 
     */
    public DurationType createDurationType() {
        return new DurationType();
    }

    /**
     * Create an instance of {@link VersionDateAttributesType }
     * 
     */
    public VersionDateAttributesType createVersionDateAttributesType() {
        return new VersionDateAttributesType();
    }

    /**
     * Create an instance of {@link EntitledProductDataType }
     * 
     */
    public EntitledProductDataType createEntitledProductDataType() {
        return new EntitledProductDataType();
    }

    /**
     * Create an instance of {@link EntitledProductDataListType }
     * 
     */
    public EntitledProductDataListType createEntitledProductDataListType() {
        return new EntitledProductDataListType();
    }

    /**
     * Create an instance of {@link OrganizationPKType }
     * 
     */
    public OrganizationPKType createOrganizationPKType() {
        return new OrganizationPKType();
    }

    /**
     * Create an instance of {@link OrganizationIdentifierType }
     * 
     */
    public OrganizationIdentifierType createOrganizationIdentifierType() {
        return new OrganizationIdentifierType();
    }

    /**
     * Create an instance of {@link UserPKType }
     * 
     */
    public UserPKType createUserPKType() {
        return new UserPKType();
    }

    /**
     * Create an instance of {@link UserIdentifierType }
     * 
     */
    public UserIdentifierType createUserIdentifierType() {
        return new UserIdentifierType();
    }

    /**
     * Create an instance of {@link ChannelPartnerDataType }
     * 
     */
    public ChannelPartnerDataType createChannelPartnerDataType() {
        return new ChannelPartnerDataType();
    }

    /**
     * Create an instance of {@link ChannelPartnerDataListType }
     * 
     */
    public ChannelPartnerDataListType createChannelPartnerDataListType() {
        return new ChannelPartnerDataListType();
    }

    /**
     * Create an instance of {@link CreateBulkEntitlementDataType }
     * 
     */
    public CreateBulkEntitlementDataType createCreateBulkEntitlementDataType() {
        return new CreateBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyPKType }
     * 
     */
    public LicenseTechnologyPKType createLicenseTechnologyPKType() {
        return new LicenseTechnologyPKType();
    }

    /**
     * Create an instance of {@link LicenseTechnologyIdentifierType }
     * 
     */
    public LicenseTechnologyIdentifierType createLicenseTechnologyIdentifierType() {
        return new LicenseTechnologyIdentifierType();
    }

    /**
     * Create an instance of {@link BulkEntitlementDataType }
     * 
     */
    public BulkEntitlementDataType createBulkEntitlementDataType() {
        return new BulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link StatusInfoType }
     * 
     */
    public StatusInfoType createStatusInfoType() {
        return new StatusInfoType();
    }

    /**
     * Create an instance of {@link FailedBulkEntitlementDataType }
     * 
     */
    public FailedBulkEntitlementDataType createFailedBulkEntitlementDataType() {
        return new FailedBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedBulkEntitlementDataListType }
     * 
     */
    public FailedBulkEntitlementDataListType createFailedBulkEntitlementDataListType() {
        return new FailedBulkEntitlementDataListType();
    }

    /**
     * Create an instance of {@link CreatedBulkEntitlementDataType }
     * 
     */
    public CreatedBulkEntitlementDataType createCreatedBulkEntitlementDataType() {
        return new CreatedBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link CreatedBulkEntitlementDataListType }
     * 
     */
    public CreatedBulkEntitlementDataListType createCreatedBulkEntitlementDataListType() {
        return new CreatedBulkEntitlementDataListType();
    }

    /**
     * Create an instance of {@link CreateEntitlementLineItemDataType }
     * 
     */
    public CreateEntitlementLineItemDataType createCreateEntitlementLineItemDataType() {
        return new CreateEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link EntitlementLineItemPKType }
     * 
     */
    public EntitlementLineItemPKType createEntitlementLineItemPKType() {
        return new EntitlementLineItemPKType();
    }

    /**
     * Create an instance of {@link EntitlementLineItemIdentifierType }
     * 
     */
    public EntitlementLineItemIdentifierType createEntitlementLineItemIdentifierType() {
        return new EntitlementLineItemIdentifierType();
    }

    /**
     * Create an instance of {@link EntitlementLineItemDataType }
     * 
     */
    public EntitlementLineItemDataType createEntitlementLineItemDataType() {
        return new EntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link CreateMaintenanceLineItemDataType }
     * 
     */
    public CreateMaintenanceLineItemDataType createCreateMaintenanceLineItemDataType() {
        return new CreateMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link MaintenanceLineItemDataType }
     * 
     */
    public MaintenanceLineItemDataType createMaintenanceLineItemDataType() {
        return new MaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link CreateSimpleEntitlementDataType }
     * 
     */
    public CreateSimpleEntitlementDataType createCreateSimpleEntitlementDataType() {
        return new CreateSimpleEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedSimpleEntitlementDataType }
     * 
     */
    public FailedSimpleEntitlementDataType createFailedSimpleEntitlementDataType() {
        return new FailedSimpleEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedSimpleEntitlementDataListType }
     * 
     */
    public FailedSimpleEntitlementDataListType createFailedSimpleEntitlementDataListType() {
        return new FailedSimpleEntitlementDataListType();
    }

    /**
     * Create an instance of {@link CreatedSimpleEntitlementDataType }
     * 
     */
    public CreatedSimpleEntitlementDataType createCreatedSimpleEntitlementDataType() {
        return new CreatedSimpleEntitlementDataType();
    }

    /**
     * Create an instance of {@link CreatedSimpleEntitlementDataListType }
     * 
     */
    public CreatedSimpleEntitlementDataListType createCreatedSimpleEntitlementDataListType() {
        return new CreatedSimpleEntitlementDataListType();
    }

    /**
     * Create an instance of {@link EntitlementPKType }
     * 
     */
    public EntitlementPKType createEntitlementPKType() {
        return new EntitlementPKType();
    }

    /**
     * Create an instance of {@link EntitlementIdentifierType }
     * 
     */
    public EntitlementIdentifierType createEntitlementIdentifierType() {
        return new EntitlementIdentifierType();
    }

    /**
     * Create an instance of {@link DeleteEntitlementDataType }
     * 
     */
    public DeleteEntitlementDataType createDeleteEntitlementDataType() {
        return new DeleteEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteEntitlementDataType }
     * 
     */
    public FailedDeleteEntitlementDataType createFailedDeleteEntitlementDataType() {
        return new FailedDeleteEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteEntitlementDataListType }
     * 
     */
    public FailedDeleteEntitlementDataListType createFailedDeleteEntitlementDataListType() {
        return new FailedDeleteEntitlementDataListType();
    }

    /**
     * Create an instance of {@link WebRegKeyDataType }
     * 
     */
    public WebRegKeyDataType createWebRegKeyDataType() {
        return new WebRegKeyDataType();
    }

    /**
     * Create an instance of {@link AddWebRegKeyDataType }
     * 
     */
    public AddWebRegKeyDataType createAddWebRegKeyDataType() {
        return new AddWebRegKeyDataType();
    }

    /**
     * Create an instance of {@link WebRegKeyDataListType }
     * 
     */
    public WebRegKeyDataListType createWebRegKeyDataListType() {
        return new WebRegKeyDataListType();
    }

    /**
     * Create an instance of {@link FailedAddWebRegKeyDataType }
     * 
     */
    public FailedAddWebRegKeyDataType createFailedAddWebRegKeyDataType() {
        return new FailedAddWebRegKeyDataType();
    }

    /**
     * Create an instance of {@link FailedAddWebRegKeyDataListType }
     * 
     */
    public FailedAddWebRegKeyDataListType createFailedAddWebRegKeyDataListType() {
        return new FailedAddWebRegKeyDataListType();
    }

    /**
     * Create an instance of {@link UpdateEntitledProductDataListType }
     * 
     */
    public UpdateEntitledProductDataListType createUpdateEntitledProductDataListType() {
        return new UpdateEntitledProductDataListType();
    }

    /**
     * Create an instance of {@link UpdateBulkEntitlementDataType }
     * 
     */
    public UpdateBulkEntitlementDataType createUpdateBulkEntitlementDataType() {
        return new UpdateBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateBulkEntitlementDataType }
     * 
     */
    public FailedUpdateBulkEntitlementDataType createFailedUpdateBulkEntitlementDataType() {
        return new FailedUpdateBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateBulkEntitlementDataListType }
     * 
     */
    public FailedUpdateBulkEntitlementDataListType createFailedUpdateBulkEntitlementDataListType() {
        return new FailedUpdateBulkEntitlementDataListType();
    }

    /**
     * Create an instance of {@link UpdateSimpleEntitlementDataType }
     * 
     */
    public UpdateSimpleEntitlementDataType createUpdateSimpleEntitlementDataType() {
        return new UpdateSimpleEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateSimpleEntitlementDataType }
     * 
     */
    public FailedUpdateSimpleEntitlementDataType createFailedUpdateSimpleEntitlementDataType() {
        return new FailedUpdateSimpleEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateSimpleEntitlementDataListType }
     * 
     */
    public FailedUpdateSimpleEntitlementDataListType createFailedUpdateSimpleEntitlementDataListType() {
        return new FailedUpdateSimpleEntitlementDataListType();
    }

    /**
     * Create an instance of {@link AddEntitlementLineItemDataType }
     * 
     */
    public AddEntitlementLineItemDataType createAddEntitlementLineItemDataType() {
        return new AddEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedAddEntitlementLineItemDataType }
     * 
     */
    public FailedAddEntitlementLineItemDataType createFailedAddEntitlementLineItemDataType() {
        return new FailedAddEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedAddEntitlementLineItemDataListType }
     * 
     */
    public FailedAddEntitlementLineItemDataListType createFailedAddEntitlementLineItemDataListType() {
        return new FailedAddEntitlementLineItemDataListType();
    }

    /**
     * Create an instance of {@link AddedEntitlementLineItemDataType }
     * 
     */
    public AddedEntitlementLineItemDataType createAddedEntitlementLineItemDataType() {
        return new AddedEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link AddedEntitlementLineItemDataListType }
     * 
     */
    public AddedEntitlementLineItemDataListType createAddedEntitlementLineItemDataListType() {
        return new AddedEntitlementLineItemDataListType();
    }

    /**
     * Create an instance of {@link RemoveEntitlementLineItemDataType }
     * 
     */
    public RemoveEntitlementLineItemDataType createRemoveEntitlementLineItemDataType() {
        return new RemoveEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedRemoveEntitlementLineItemDataType }
     * 
     */
    public FailedRemoveEntitlementLineItemDataType createFailedRemoveEntitlementLineItemDataType() {
        return new FailedRemoveEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedRemoveEntitlementLineItemDataListType }
     * 
     */
    public FailedRemoveEntitlementLineItemDataListType createFailedRemoveEntitlementLineItemDataListType() {
        return new FailedRemoveEntitlementLineItemDataListType();
    }

    /**
     * Create an instance of {@link UpdateLineItemDataType }
     * 
     */
    public UpdateLineItemDataType createUpdateLineItemDataType() {
        return new UpdateLineItemDataType();
    }

    /**
     * Create an instance of {@link UpdateMaintenanceLineItemDataType }
     * 
     */
    public UpdateMaintenanceLineItemDataType createUpdateMaintenanceLineItemDataType() {
        return new UpdateMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link UpdateEntitlementLineItemDataType }
     * 
     */
    public UpdateEntitlementLineItemDataType createUpdateEntitlementLineItemDataType() {
        return new UpdateEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateEntitlementLineItemDataType }
     * 
     */
    public FailedUpdateEntitlementLineItemDataType createFailedUpdateEntitlementLineItemDataType() {
        return new FailedUpdateEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedUpdateEntitlementLineItemDataListType }
     * 
     */
    public FailedUpdateEntitlementLineItemDataListType createFailedUpdateEntitlementLineItemDataListType() {
        return new FailedUpdateEntitlementLineItemDataListType();
    }

    /**
     * Create an instance of {@link SimpleQueryType }
     * 
     */
    public SimpleQueryType createSimpleQueryType() {
        return new SimpleQueryType();
    }

    /**
     * Create an instance of {@link DateQueryType }
     * 
     */
    public DateQueryType createDateQueryType() {
        return new DateQueryType();
    }

    /**
     * Create an instance of {@link StateQueryType }
     * 
     */
    public StateQueryType createStateQueryType() {
        return new StateQueryType();
    }

    /**
     * Create an instance of {@link PartnerTierQueryType }
     * 
     */
    public PartnerTierQueryType createPartnerTierQueryType() {
        return new PartnerTierQueryType();
    }

    /**
     * Create an instance of {@link EntCustomAttributeQueryType }
     * 
     */
    public EntCustomAttributeQueryType createEntCustomAttributeQueryType() {
        return new EntCustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link EntCustomAttributesQueryListType }
     * 
     */
    public EntCustomAttributesQueryListType createEntCustomAttributesQueryListType() {
        return new EntCustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link SearchEntitlementDataType }
     * 
     */
    public SearchEntitlementDataType createSearchEntitlementDataType() {
        return new SearchEntitlementDataType();
    }

    /**
     * Create an instance of {@link SimpleEntitlementDataType }
     * 
     */
    public SimpleEntitlementDataType createSimpleEntitlementDataType() {
        return new SimpleEntitlementDataType();
    }

    /**
     * Create an instance of {@link EntitlementDataType }
     * 
     */
    public EntitlementDataType createEntitlementDataType() {
        return new EntitlementDataType();
    }

    /**
     * Create an instance of {@link NumberQueryType }
     * 
     */
    public NumberQueryType createNumberQueryType() {
        return new NumberQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributeQueryType }
     * 
     */
    public CustomAttributeQueryType createCustomAttributeQueryType() {
        return new CustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link CustomAttributesQueryListType }
     * 
     */
    public CustomAttributesQueryListType createCustomAttributesQueryListType() {
        return new CustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link SearchBulkEntitlementDataType }
     * 
     */
    public SearchBulkEntitlementDataType createSearchBulkEntitlementDataType() {
        return new SearchBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorType }
     * 
     */
    public CustomAttributeDescriptorType createCustomAttributeDescriptorType() {
        return new CustomAttributeDescriptorType();
    }

    /**
     * Create an instance of {@link CustomAttributeDescriptorDataType }
     * 
     */
    public CustomAttributeDescriptorDataType createCustomAttributeDescriptorDataType() {
        return new CustomAttributeDescriptorDataType();
    }

    /**
     * Create an instance of {@link BulkEntitlementResponseConfigRequestType }
     * 
     */
    public BulkEntitlementResponseConfigRequestType createBulkEntitlementResponseConfigRequestType() {
        return new BulkEntitlementResponseConfigRequestType();
    }

    /**
     * Create an instance of {@link BulkEntitlementPropertiesType }
     * 
     */
    public BulkEntitlementPropertiesType createBulkEntitlementPropertiesType() {
        return new BulkEntitlementPropertiesType();
    }

    /**
     * Create an instance of {@link DateTimeQueryType }
     * 
     */
    public DateTimeQueryType createDateTimeQueryType() {
        return new DateTimeQueryType();
    }

    /**
     * Create an instance of {@link LineItemCustomAttributeQueryType }
     * 
     */
    public LineItemCustomAttributeQueryType createLineItemCustomAttributeQueryType() {
        return new LineItemCustomAttributeQueryType();
    }

    /**
     * Create an instance of {@link LineItemCustomAttributesQueryListType }
     * 
     */
    public LineItemCustomAttributesQueryListType createLineItemCustomAttributesQueryListType() {
        return new LineItemCustomAttributesQueryListType();
    }

    /**
     * Create an instance of {@link SearchActivatableItemDataType }
     * 
     */
    public SearchActivatableItemDataType createSearchActivatableItemDataType() {
        return new SearchActivatableItemDataType();
    }

    /**
     * Create an instance of {@link ActivatableItemDetailType }
     * 
     */
    public ActivatableItemDetailType createActivatableItemDetailType() {
        return new ActivatableItemDetailType();
    }

    /**
     * Create an instance of {@link EntitlementLineItemResponseConfigRequestType }
     * 
     */
    public EntitlementLineItemResponseConfigRequestType createEntitlementLineItemResponseConfigRequestType() {
        return new EntitlementLineItemResponseConfigRequestType();
    }

    /**
     * Create an instance of {@link MaintenanceLineItemPropertiesType }
     * 
     */
    public MaintenanceLineItemPropertiesType createMaintenanceLineItemPropertiesType() {
        return new MaintenanceLineItemPropertiesType();
    }

    /**
     * Create an instance of {@link EntitlementLineItemPropertiesType }
     * 
     */
    public EntitlementLineItemPropertiesType createEntitlementLineItemPropertiesType() {
        return new EntitlementLineItemPropertiesType();
    }

    /**
     * Create an instance of {@link EntitlementStateDataType }
     * 
     */
    public EntitlementStateDataType createEntitlementStateDataType() {
        return new EntitlementStateDataType();
    }

    /**
     * Create an instance of {@link FailedEntitlementStateDataType }
     * 
     */
    public FailedEntitlementStateDataType createFailedEntitlementStateDataType() {
        return new FailedEntitlementStateDataType();
    }

    /**
     * Create an instance of {@link FailedEntitlementStateDataListType }
     * 
     */
    public FailedEntitlementStateDataListType createFailedEntitlementStateDataListType() {
        return new FailedEntitlementStateDataListType();
    }

    /**
     * Create an instance of {@link WebRegKeyCountDataType }
     * 
     */
    public WebRegKeyCountDataType createWebRegKeyCountDataType() {
        return new WebRegKeyCountDataType();
    }

    /**
     * Create an instance of {@link WebRegKeyType }
     * 
     */
    public WebRegKeyType createWebRegKeyType() {
        return new WebRegKeyType();
    }

    /**
     * Create an instance of {@link WebRegKeysDataListType }
     * 
     */
    public WebRegKeysDataListType createWebRegKeysDataListType() {
        return new WebRegKeysDataListType();
    }

    /**
     * Create an instance of {@link ValueType }
     * 
     */
    public ValueType createValueType() {
        return new ValueType();
    }

    /**
     * Create an instance of {@link AttributeMetaDescriptorType }
     * 
     */
    public AttributeMetaDescriptorType createAttributeMetaDescriptorType() {
        return new AttributeMetaDescriptorType();
    }

    /**
     * Create an instance of {@link AttributeMetaDescriptorDataType }
     * 
     */
    public AttributeMetaDescriptorDataType createAttributeMetaDescriptorDataType() {
        return new AttributeMetaDescriptorDataType();
    }

    /**
     * Create an instance of {@link PolicyAttributesDataType }
     * 
     */
    public PolicyAttributesDataType createPolicyAttributesDataType() {
        return new PolicyAttributesDataType();
    }

    /**
     * Create an instance of {@link ExpirationTermsDataType }
     * 
     */
    public ExpirationTermsDataType createExpirationTermsDataType() {
        return new ExpirationTermsDataType();
    }

    /**
     * Create an instance of {@link RenewParametersDataType }
     * 
     */
    public RenewParametersDataType createRenewParametersDataType() {
        return new RenewParametersDataType();
    }

    /**
     * Create an instance of {@link RenewLineItemDataType }
     * 
     */
    public RenewLineItemDataType createRenewLineItemDataType() {
        return new RenewLineItemDataType();
    }

    /**
     * Create an instance of {@link RenewEntitlementDataType }
     * 
     */
    public RenewEntitlementDataType createRenewEntitlementDataType() {
        return new RenewEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedRenewEntitlementDataType }
     * 
     */
    public FailedRenewEntitlementDataType createFailedRenewEntitlementDataType() {
        return new FailedRenewEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedRenewEntitlementDataListType }
     * 
     */
    public FailedRenewEntitlementDataListType createFailedRenewEntitlementDataListType() {
        return new FailedRenewEntitlementDataListType();
    }

    /**
     * Create an instance of {@link NewEntitlementLineItemDataType }
     * 
     */
    public NewEntitlementLineItemDataType createNewEntitlementLineItemDataType() {
        return new NewEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link RenewedEntitlementLineItemDataType }
     * 
     */
    public RenewedEntitlementLineItemDataType createRenewedEntitlementLineItemDataType() {
        return new RenewedEntitlementLineItemDataType();
    }

    /**
     * Create an instance of {@link CreatedRenewEntitlementDataType }
     * 
     */
    public CreatedRenewEntitlementDataType createCreatedRenewEntitlementDataType() {
        return new CreatedRenewEntitlementDataType();
    }

    /**
     * Create an instance of {@link CreatedRenewEntitlementDataListType }
     * 
     */
    public CreatedRenewEntitlementDataListType createCreatedRenewEntitlementDataListType() {
        return new CreatedRenewEntitlementDataListType();
    }

    /**
     * Create an instance of {@link LineItemLifeCycleDataType }
     * 
     */
    public LineItemLifeCycleDataType createLineItemLifeCycleDataType() {
        return new LineItemLifeCycleDataType();
    }

    /**
     * Create an instance of {@link EntitlementLifeCycleDataType }
     * 
     */
    public EntitlementLifeCycleDataType createEntitlementLifeCycleDataType() {
        return new EntitlementLifeCycleDataType();
    }

    /**
     * Create an instance of {@link FailedEntitlementLifeCycleDataType }
     * 
     */
    public FailedEntitlementLifeCycleDataType createFailedEntitlementLifeCycleDataType() {
        return new FailedEntitlementLifeCycleDataType();
    }

    /**
     * Create an instance of {@link FailedEntitlementLifeCycleDataListType }
     * 
     */
    public FailedEntitlementLifeCycleDataListType createFailedEntitlementLifeCycleDataListType() {
        return new FailedEntitlementLifeCycleDataListType();
    }

    /**
     * Create an instance of {@link LifeCycleLineItemDataType }
     * 
     */
    public LifeCycleLineItemDataType createLifeCycleLineItemDataType() {
        return new LifeCycleLineItemDataType();
    }

    /**
     * Create an instance of {@link CreatedEntitlementLifeCycleDataType }
     * 
     */
    public CreatedEntitlementLifeCycleDataType createCreatedEntitlementLifeCycleDataType() {
        return new CreatedEntitlementLifeCycleDataType();
    }

    /**
     * Create an instance of {@link CreatedEntitlementLifeCycleDataListType }
     * 
     */
    public CreatedEntitlementLifeCycleDataListType createCreatedEntitlementLifeCycleDataListType() {
        return new CreatedEntitlementLifeCycleDataListType();
    }

    /**
     * Create an instance of {@link IdListType }
     * 
     */
    public IdListType createIdListType() {
        return new IdListType();
    }

    /**
     * Create an instance of {@link FailedIdDataType }
     * 
     */
    public FailedIdDataType createFailedIdDataType() {
        return new FailedIdDataType();
    }

    /**
     * Create an instance of {@link FailedMapEntitlementsToUserDataListType }
     * 
     */
    public FailedMapEntitlementsToUserDataListType createFailedMapEntitlementsToUserDataListType() {
        return new FailedMapEntitlementsToUserDataListType();
    }

    /**
     * Create an instance of {@link EmailContactListType }
     * 
     */
    public EmailContactListType createEmailContactListType() {
        return new EmailContactListType();
    }

    /**
     * Create an instance of {@link LineItemStateDataType }
     * 
     */
    public LineItemStateDataType createLineItemStateDataType() {
        return new LineItemStateDataType();
    }

    /**
     * Create an instance of {@link FailedLineItemStateDataType }
     * 
     */
    public FailedLineItemStateDataType createFailedLineItemStateDataType() {
        return new FailedLineItemStateDataType();
    }

    /**
     * Create an instance of {@link FailedLineItemStateDataListType }
     * 
     */
    public FailedLineItemStateDataListType createFailedLineItemStateDataListType() {
        return new FailedLineItemStateDataListType();
    }

    /**
     * Create an instance of {@link MaintenanceLineItemStateDataType }
     * 
     */
    public MaintenanceLineItemStateDataType createMaintenanceLineItemStateDataType() {
        return new MaintenanceLineItemStateDataType();
    }

    /**
     * Create an instance of {@link FailedMaintenanceLineItemStateDataType }
     * 
     */
    public FailedMaintenanceLineItemStateDataType createFailedMaintenanceLineItemStateDataType() {
        return new FailedMaintenanceLineItemStateDataType();
    }

    /**
     * Create an instance of {@link FailedMaintenanceLineItemStateDataListType }
     * 
     */
    public FailedMaintenanceLineItemStateDataListType createFailedMaintenanceLineItemStateDataListType() {
        return new FailedMaintenanceLineItemStateDataListType();
    }

    /**
     * Create an instance of {@link WebRegKeysListType }
     * 
     */
    public WebRegKeysListType createWebRegKeysListType() {
        return new WebRegKeysListType();
    }

    /**
     * Create an instance of {@link FailedDeleteWebRegKeyDataType }
     * 
     */
    public FailedDeleteWebRegKeyDataType createFailedDeleteWebRegKeyDataType() {
        return new FailedDeleteWebRegKeyDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteWebRegKeyListType }
     * 
     */
    public FailedDeleteWebRegKeyListType createFailedDeleteWebRegKeyListType() {
        return new FailedDeleteWebRegKeyListType();
    }

    /**
     * Create an instance of {@link TransferEntitlementInfoType }
     * 
     */
    public TransferEntitlementInfoType createTransferEntitlementInfoType() {
        return new TransferEntitlementInfoType();
    }

    /**
     * Create an instance of {@link TransferEntitlementsListType }
     * 
     */
    public TransferEntitlementsListType createTransferEntitlementsListType() {
        return new TransferEntitlementsListType();
    }

    /**
     * Create an instance of {@link TransferredLineItemMapType }
     * 
     */
    public TransferredLineItemMapType createTransferredLineItemMapType() {
        return new TransferredLineItemMapType();
    }

    /**
     * Create an instance of {@link TransferredEntitlementDataType }
     * 
     */
    public TransferredEntitlementDataType createTransferredEntitlementDataType() {
        return new TransferredEntitlementDataType();
    }

    /**
     * Create an instance of {@link TransferredEntitlementsListType }
     * 
     */
    public TransferredEntitlementsListType createTransferredEntitlementsListType() {
        return new TransferredEntitlementsListType();
    }

    /**
     * Create an instance of {@link FailedTransferEntitlementDataType }
     * 
     */
    public FailedTransferEntitlementDataType createFailedTransferEntitlementDataType() {
        return new FailedTransferEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedTransferEntitlementListType }
     * 
     */
    public FailedTransferEntitlementListType createFailedTransferEntitlementListType() {
        return new FailedTransferEntitlementListType();
    }

    /**
     * Create an instance of {@link TransferLineItemInfoType }
     * 
     */
    public TransferLineItemInfoType createTransferLineItemInfoType() {
        return new TransferLineItemInfoType();
    }

    /**
     * Create an instance of {@link TransferLineItemsListType }
     * 
     */
    public TransferLineItemsListType createTransferLineItemsListType() {
        return new TransferLineItemsListType();
    }

    /**
     * Create an instance of {@link TransferredLineItemDataType }
     * 
     */
    public TransferredLineItemDataType createTransferredLineItemDataType() {
        return new TransferredLineItemDataType();
    }

    /**
     * Create an instance of {@link TransferredLineItemsListType }
     * 
     */
    public TransferredLineItemsListType createTransferredLineItemsListType() {
        return new TransferredLineItemsListType();
    }

    /**
     * Create an instance of {@link FailedTransferLineItemDataType }
     * 
     */
    public FailedTransferLineItemDataType createFailedTransferLineItemDataType() {
        return new FailedTransferLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedTransferLineItemListType }
     * 
     */
    public FailedTransferLineItemListType createFailedTransferLineItemListType() {
        return new FailedTransferLineItemListType();
    }

    /**
     * Create an instance of {@link FeaturePKType }
     * 
     */
    public FeaturePKType createFeaturePKType() {
        return new FeaturePKType();
    }

    /**
     * Create an instance of {@link FeatureIdentifierType }
     * 
     */
    public FeatureIdentifierType createFeatureIdentifierType() {
        return new FeatureIdentifierType();
    }

    /**
     * Create an instance of {@link FeatureListType }
     * 
     */
    public FeatureListType createFeatureListType() {
        return new FeatureListType();
    }

    /**
     * Create an instance of {@link FeatureBundlePKType }
     * 
     */
    public FeatureBundlePKType createFeatureBundlePKType() {
        return new FeatureBundlePKType();
    }

    /**
     * Create an instance of {@link FeatureBundleIdentifierType }
     * 
     */
    public FeatureBundleIdentifierType createFeatureBundleIdentifierType() {
        return new FeatureBundleIdentifierType();
    }

    /**
     * Create an instance of {@link FeatureBundleListType }
     * 
     */
    public FeatureBundleListType createFeatureBundleListType() {
        return new FeatureBundleListType();
    }

    /**
     * Create an instance of {@link ProductListType }
     * 
     */
    public ProductListType createProductListType() {
        return new ProductListType();
    }

    /**
     * Create an instance of {@link LicenseModelListType }
     * 
     */
    public LicenseModelListType createLicenseModelListType() {
        return new LicenseModelListType();
    }

    /**
     * Create an instance of {@link EntitlementListType }
     * 
     */
    public EntitlementListType createEntitlementListType() {
        return new EntitlementListType();
    }

    /**
     * Create an instance of {@link StateChangeDataType }
     * 
     */
    public StateChangeDataType createStateChangeDataType() {
        return new StateChangeDataType();
    }

    /**
     * Create an instance of {@link FeatureStateChangeDataType }
     * 
     */
    public FeatureStateChangeDataType createFeatureStateChangeDataType() {
        return new FeatureStateChangeDataType();
    }

    /**
     * Create an instance of {@link FeatureStateChangeListType }
     * 
     */
    public FeatureStateChangeListType createFeatureStateChangeListType() {
        return new FeatureStateChangeListType();
    }

    /**
     * Create an instance of {@link FeatureBundleStateChangeDataType }
     * 
     */
    public FeatureBundleStateChangeDataType createFeatureBundleStateChangeDataType() {
        return new FeatureBundleStateChangeDataType();
    }

    /**
     * Create an instance of {@link FeatureBundleStateChangeListType }
     * 
     */
    public FeatureBundleStateChangeListType createFeatureBundleStateChangeListType() {
        return new FeatureBundleStateChangeListType();
    }

    /**
     * Create an instance of {@link ProductStateChangeDataType }
     * 
     */
    public ProductStateChangeDataType createProductStateChangeDataType() {
        return new ProductStateChangeDataType();
    }

    /**
     * Create an instance of {@link ProductStateChangeListType }
     * 
     */
    public ProductStateChangeListType createProductStateChangeListType() {
        return new ProductStateChangeListType();
    }

    /**
     * Create an instance of {@link LicenseModelStateChangeDataType }
     * 
     */
    public LicenseModelStateChangeDataType createLicenseModelStateChangeDataType() {
        return new LicenseModelStateChangeDataType();
    }

    /**
     * Create an instance of {@link LicenseModelStateChangeListType }
     * 
     */
    public LicenseModelStateChangeListType createLicenseModelStateChangeListType() {
        return new LicenseModelStateChangeListType();
    }

    /**
     * Create an instance of {@link EntitlementStateChangeDataType }
     * 
     */
    public EntitlementStateChangeDataType createEntitlementStateChangeDataType() {
        return new EntitlementStateChangeDataType();
    }

    /**
     * Create an instance of {@link EntitlementStateChangeListType }
     * 
     */
    public EntitlementStateChangeListType createEntitlementStateChangeListType() {
        return new EntitlementStateChangeListType();
    }

    /**
     * Create an instance of {@link StateChangeResponseType }
     * 
     */
    public StateChangeResponseType createStateChangeResponseType() {
        return new StateChangeResponseType();
    }

    /**
     * Create an instance of {@link LinkMaintenanceLineItemDataType }
     * 
     */
    public LinkMaintenanceLineItemDataType createLinkMaintenanceLineItemDataType() {
        return new LinkMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link LinkMaintenanceLineItemListType }
     * 
     */
    public LinkMaintenanceLineItemListType createLinkMaintenanceLineItemListType() {
        return new LinkMaintenanceLineItemListType();
    }

    /**
     * Create an instance of {@link FailedLinkMaintenanceLineItemDataType }
     * 
     */
    public FailedLinkMaintenanceLineItemDataType createFailedLinkMaintenanceLineItemDataType() {
        return new FailedLinkMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedLinkMaintenanceLineItemListType }
     * 
     */
    public FailedLinkMaintenanceLineItemListType createFailedLinkMaintenanceLineItemListType() {
        return new FailedLinkMaintenanceLineItemListType();
    }

    /**
     * Create an instance of {@link SplitLineItemInfoType }
     * 
     */
    public SplitLineItemInfoType createSplitLineItemInfoType() {
        return new SplitLineItemInfoType();
    }

    /**
     * Create an instance of {@link SplitLineItemListType }
     * 
     */
    public SplitLineItemListType createSplitLineItemListType() {
        return new SplitLineItemListType();
    }

    /**
     * Create an instance of {@link SplitLineItemDataType }
     * 
     */
    public SplitLineItemDataType createSplitLineItemDataType() {
        return new SplitLineItemDataType();
    }

    /**
     * Create an instance of {@link SplitLineItemResponseListType }
     * 
     */
    public SplitLineItemResponseListType createSplitLineItemResponseListType() {
        return new SplitLineItemResponseListType();
    }

    /**
     * Create an instance of {@link FailedSplitLineItemDataType }
     * 
     */
    public FailedSplitLineItemDataType createFailedSplitLineItemDataType() {
        return new FailedSplitLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedSplitLineItemListType }
     * 
     */
    public FailedSplitLineItemListType createFailedSplitLineItemListType() {
        return new FailedSplitLineItemListType();
    }

    /**
     * Create an instance of {@link SplitBulkEntitlementInfoType }
     * 
     */
    public SplitBulkEntitlementInfoType createSplitBulkEntitlementInfoType() {
        return new SplitBulkEntitlementInfoType();
    }

    /**
     * Create an instance of {@link SplitBulkEntitlementListType }
     * 
     */
    public SplitBulkEntitlementListType createSplitBulkEntitlementListType() {
        return new SplitBulkEntitlementListType();
    }

    /**
     * Create an instance of {@link SplitBulkEntitlementDataType }
     * 
     */
    public SplitBulkEntitlementDataType createSplitBulkEntitlementDataType() {
        return new SplitBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link SplitBulkEntitlementResponseListType }
     * 
     */
    public SplitBulkEntitlementResponseListType createSplitBulkEntitlementResponseListType() {
        return new SplitBulkEntitlementResponseListType();
    }

    /**
     * Create an instance of {@link FailedSplitBulkEntitlementDataType }
     * 
     */
    public FailedSplitBulkEntitlementDataType createFailedSplitBulkEntitlementDataType() {
        return new FailedSplitBulkEntitlementDataType();
    }

    /**
     * Create an instance of {@link FailedSplitBulkEntitlementListType }
     * 
     */
    public FailedSplitBulkEntitlementListType createFailedSplitBulkEntitlementListType() {
        return new FailedSplitBulkEntitlementListType();
    }

    /**
     * Create an instance of {@link GetMatchingLineItemInfoType }
     * 
     */
    public GetMatchingLineItemInfoType createGetMatchingLineItemInfoType() {
        return new GetMatchingLineItemInfoType();
    }

    /**
     * Create an instance of {@link GetMatchingLineItemsListType }
     * 
     */
    public GetMatchingLineItemsListType createGetMatchingLineItemsListType() {
        return new GetMatchingLineItemsListType();
    }

    /**
     * Create an instance of {@link MatchingLineItemDataType }
     * 
     */
    public MatchingLineItemDataType createMatchingLineItemDataType() {
        return new MatchingLineItemDataType();
    }

    /**
     * Create an instance of {@link GetMatchingLineItemsResponseListType }
     * 
     */
    public GetMatchingLineItemsResponseListType createGetMatchingLineItemsResponseListType() {
        return new GetMatchingLineItemsResponseListType();
    }

    /**
     * Create an instance of {@link FailedMatchingLineItemDataType }
     * 
     */
    public FailedMatchingLineItemDataType createFailedMatchingLineItemDataType() {
        return new FailedMatchingLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedMatchingLineItemsListType }
     * 
     */
    public FailedMatchingLineItemsListType createFailedMatchingLineItemsListType() {
        return new FailedMatchingLineItemsListType();
    }

    /**
     * Create an instance of {@link GetMatchingBulkEntInfoType }
     * 
     */
    public GetMatchingBulkEntInfoType createGetMatchingBulkEntInfoType() {
        return new GetMatchingBulkEntInfoType();
    }

    /**
     * Create an instance of {@link GetMatchingBulkEntsListType }
     * 
     */
    public GetMatchingBulkEntsListType createGetMatchingBulkEntsListType() {
        return new GetMatchingBulkEntsListType();
    }

    /**
     * Create an instance of {@link MatchingBulkEntDataType }
     * 
     */
    public MatchingBulkEntDataType createMatchingBulkEntDataType() {
        return new MatchingBulkEntDataType();
    }

    /**
     * Create an instance of {@link GetMatchingBulkEntsResponseListType }
     * 
     */
    public GetMatchingBulkEntsResponseListType createGetMatchingBulkEntsResponseListType() {
        return new GetMatchingBulkEntsResponseListType();
    }

    /**
     * Create an instance of {@link FailedMatchingBulkEntDataType }
     * 
     */
    public FailedMatchingBulkEntDataType createFailedMatchingBulkEntDataType() {
        return new FailedMatchingBulkEntDataType();
    }

    /**
     * Create an instance of {@link FailedMatchingBulkEntsListType }
     * 
     */
    public FailedMatchingBulkEntsListType createFailedMatchingBulkEntsListType() {
        return new FailedMatchingBulkEntsListType();
    }

    /**
     * Create an instance of {@link DeleteMaintenanceLineItemDataType }
     * 
     */
    public DeleteMaintenanceLineItemDataType createDeleteMaintenanceLineItemDataType() {
        return new DeleteMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteMaintenanceLineItemDataType }
     * 
     */
    public FailedDeleteMaintenanceLineItemDataType createFailedDeleteMaintenanceLineItemDataType() {
        return new FailedDeleteMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedDeleteMaintenanceLineItemDataListType }
     * 
     */
    public FailedDeleteMaintenanceLineItemDataListType createFailedDeleteMaintenanceLineItemDataListType() {
        return new FailedDeleteMaintenanceLineItemDataListType();
    }

    /**
     * Create an instance of {@link UnlinkMaintenanceLineItemDataType }
     * 
     */
    public UnlinkMaintenanceLineItemDataType createUnlinkMaintenanceLineItemDataType() {
        return new UnlinkMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link UnlinkMaintenanceLineItemListType }
     * 
     */
    public UnlinkMaintenanceLineItemListType createUnlinkMaintenanceLineItemListType() {
        return new UnlinkMaintenanceLineItemListType();
    }

    /**
     * Create an instance of {@link FailedUnlinkMaintenanceLineItemDataType }
     * 
     */
    public FailedUnlinkMaintenanceLineItemDataType createFailedUnlinkMaintenanceLineItemDataType() {
        return new FailedUnlinkMaintenanceLineItemDataType();
    }

    /**
     * Create an instance of {@link FailedUnlinkMaintenanceLineItemListType }
     * 
     */
    public FailedUnlinkMaintenanceLineItemListType createFailedUnlinkMaintenanceLineItemListType() {
        return new FailedUnlinkMaintenanceLineItemListType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateBulkEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createBulkEntitlementRequest")
    public JAXBElement<CreateBulkEntitlementRequestType> createCreateBulkEntitlementRequest(CreateBulkEntitlementRequestType value) {
        return new JAXBElement<CreateBulkEntitlementRequestType>(_CreateBulkEntitlementRequest_QNAME, CreateBulkEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateBulkEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createBulkEntitlementResponse")
    public JAXBElement<CreateBulkEntitlementResponseType> createCreateBulkEntitlementResponse(CreateBulkEntitlementResponseType value) {
        return new JAXBElement<CreateBulkEntitlementResponseType>(_CreateBulkEntitlementResponse_QNAME, CreateBulkEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSimpleEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createSimpleEntitlementRequest")
    public JAXBElement<CreateSimpleEntitlementRequestType> createCreateSimpleEntitlementRequest(CreateSimpleEntitlementRequestType value) {
        return new JAXBElement<CreateSimpleEntitlementRequestType>(_CreateSimpleEntitlementRequest_QNAME, CreateSimpleEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateSimpleEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createSimpleEntitlementResponse")
    public JAXBElement<CreateSimpleEntitlementResponseType> createCreateSimpleEntitlementResponse(CreateSimpleEntitlementResponseType value) {
        return new JAXBElement<CreateSimpleEntitlementResponseType>(_CreateSimpleEntitlementResponse_QNAME, CreateSimpleEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteEntitlementRequest")
    public JAXBElement<DeleteEntitlementRequestType> createDeleteEntitlementRequest(DeleteEntitlementRequestType value) {
        return new JAXBElement<DeleteEntitlementRequestType>(_DeleteEntitlementRequest_QNAME, DeleteEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteEntitlementResponse")
    public JAXBElement<DeleteEntitlementResponseType> createDeleteEntitlementResponse(DeleteEntitlementResponseType value) {
        return new JAXBElement<DeleteEntitlementResponseType>(_DeleteEntitlementResponse_QNAME, DeleteEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddWebRegKeyRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "addWebRegKeyRequest")
    public JAXBElement<AddWebRegKeyRequestType> createAddWebRegKeyRequest(AddWebRegKeyRequestType value) {
        return new JAXBElement<AddWebRegKeyRequestType>(_AddWebRegKeyRequest_QNAME, AddWebRegKeyRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddWebRegKeyResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "addWebRegKeyResponse")
    public JAXBElement<AddWebRegKeyResponseType> createAddWebRegKeyResponse(AddWebRegKeyResponseType value) {
        return new JAXBElement<AddWebRegKeyResponseType>(_AddWebRegKeyResponse_QNAME, AddWebRegKeyResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBulkEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "updateBulkEntitlementRequest")
    public JAXBElement<UpdateBulkEntitlementRequestType> createUpdateBulkEntitlementRequest(UpdateBulkEntitlementRequestType value) {
        return new JAXBElement<UpdateBulkEntitlementRequestType>(_UpdateBulkEntitlementRequest_QNAME, UpdateBulkEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateBulkEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "updateBulkEntitlementResponse")
    public JAXBElement<UpdateBulkEntitlementResponseType> createUpdateBulkEntitlementResponse(UpdateBulkEntitlementResponseType value) {
        return new JAXBElement<UpdateBulkEntitlementResponseType>(_UpdateBulkEntitlementResponse_QNAME, UpdateBulkEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSimpleEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "updateSimpleEntitlementRequest")
    public JAXBElement<UpdateSimpleEntitlementRequestType> createUpdateSimpleEntitlementRequest(UpdateSimpleEntitlementRequestType value) {
        return new JAXBElement<UpdateSimpleEntitlementRequestType>(_UpdateSimpleEntitlementRequest_QNAME, UpdateSimpleEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateSimpleEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "updateSimpleEntitlementResponse")
    public JAXBElement<UpdateSimpleEntitlementResponseType> createUpdateSimpleEntitlementResponse(UpdateSimpleEntitlementResponseType value) {
        return new JAXBElement<UpdateSimpleEntitlementResponseType>(_UpdateSimpleEntitlementResponse_QNAME, UpdateSimpleEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOnlyEntitlementLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createEntitlementLineItemRequest")
    public JAXBElement<AddOnlyEntitlementLineItemRequestType> createCreateEntitlementLineItemRequest(AddOnlyEntitlementLineItemRequestType value) {
        return new JAXBElement<AddOnlyEntitlementLineItemRequestType>(_CreateEntitlementLineItemRequest_QNAME, AddOnlyEntitlementLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddOnlyEntitlementLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "createEntitlementLineItemResponse")
    public JAXBElement<AddOnlyEntitlementLineItemResponseType> createCreateEntitlementLineItemResponse(AddOnlyEntitlementLineItemResponseType value) {
        return new JAXBElement<AddOnlyEntitlementLineItemResponseType>(_CreateEntitlementLineItemResponse_QNAME, AddOnlyEntitlementLineItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReplaceOnlyEntitlementLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "replaceEntitlementLineItemRequest")
    public JAXBElement<ReplaceOnlyEntitlementLineItemRequestType> createReplaceEntitlementLineItemRequest(ReplaceOnlyEntitlementLineItemRequestType value) {
        return new JAXBElement<ReplaceOnlyEntitlementLineItemRequestType>(_ReplaceEntitlementLineItemRequest_QNAME, ReplaceOnlyEntitlementLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReplaceOnlyEntitlementLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "replaceEntitlementLineItemResponse")
    public JAXBElement<ReplaceOnlyEntitlementLineItemResponseType> createReplaceEntitlementLineItemResponse(ReplaceOnlyEntitlementLineItemResponseType value) {
        return new JAXBElement<ReplaceOnlyEntitlementLineItemResponseType>(_ReplaceEntitlementLineItemResponse_QNAME, ReplaceOnlyEntitlementLineItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveEntitlementLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "removeEntitlementLineItemRequest")
    public JAXBElement<RemoveEntitlementLineItemRequestType> createRemoveEntitlementLineItemRequest(RemoveEntitlementLineItemRequestType value) {
        return new JAXBElement<RemoveEntitlementLineItemRequestType>(_RemoveEntitlementLineItemRequest_QNAME, RemoveEntitlementLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveEntitlementLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "removeEntitlementLineItemResponse")
    public JAXBElement<RemoveEntitlementLineItemResponseType> createRemoveEntitlementLineItemResponse(RemoveEntitlementLineItemResponseType value) {
        return new JAXBElement<RemoveEntitlementLineItemResponseType>(_RemoveEntitlementLineItemResponse_QNAME, RemoveEntitlementLineItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateEntitlementLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "updateEntitlementLineItemRequest")
    public JAXBElement<UpdateEntitlementLineItemRequestType> createUpdateEntitlementLineItemRequest(UpdateEntitlementLineItemRequestType value) {
        return new JAXBElement<UpdateEntitlementLineItemRequestType>(_UpdateEntitlementLineItemRequest_QNAME, UpdateEntitlementLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateEntitlementLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "updateEntitlementLineItemResponse")
    public JAXBElement<UpdateEntitlementLineItemResponseType> createUpdateEntitlementLineItemResponse(UpdateEntitlementLineItemResponseType value) {
        return new JAXBElement<UpdateEntitlementLineItemResponseType>(_UpdateEntitlementLineItemResponse_QNAME, UpdateEntitlementLineItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "searchEntitlementRequest")
    public JAXBElement<SearchEntitlementRequestType> createSearchEntitlementRequest(SearchEntitlementRequestType value) {
        return new JAXBElement<SearchEntitlementRequestType>(_SearchEntitlementRequest_QNAME, SearchEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "searchEntitlementResponse")
    public JAXBElement<SearchEntitlementResponseType> createSearchEntitlementResponse(SearchEntitlementResponseType value) {
        return new JAXBElement<SearchEntitlementResponseType>(_SearchEntitlementResponse_QNAME, SearchEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBulkEntitlementPropertiesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getBulkEntitlementPropertiesRequest")
    public JAXBElement<GetBulkEntitlementPropertiesRequestType> createGetBulkEntitlementPropertiesRequest(GetBulkEntitlementPropertiesRequestType value) {
        return new JAXBElement<GetBulkEntitlementPropertiesRequestType>(_GetBulkEntitlementPropertiesRequest_QNAME, GetBulkEntitlementPropertiesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBulkEntitlementPropertiesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getBulkEntitlementPropertiesResponse")
    public JAXBElement<GetBulkEntitlementPropertiesResponseType> createGetBulkEntitlementPropertiesResponse(GetBulkEntitlementPropertiesResponseType value) {
        return new JAXBElement<GetBulkEntitlementPropertiesResponseType>(_GetBulkEntitlementPropertiesResponse_QNAME, GetBulkEntitlementPropertiesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBulkEntitlementCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getBulkEntitlementCountRequest")
    public JAXBElement<GetBulkEntitlementCountRequestType> createGetBulkEntitlementCountRequest(GetBulkEntitlementCountRequestType value) {
        return new JAXBElement<GetBulkEntitlementCountRequestType>(_GetBulkEntitlementCountRequest_QNAME, GetBulkEntitlementCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBulkEntitlementCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getBulkEntitlementCountResponse")
    public JAXBElement<GetBulkEntitlementCountResponseType> createGetBulkEntitlementCountResponse(GetBulkEntitlementCountResponseType value) {
        return new JAXBElement<GetBulkEntitlementCountResponseType>(_GetBulkEntitlementCountResponse_QNAME, GetBulkEntitlementCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchActivatableItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "searchActivatableItemRequest")
    public JAXBElement<SearchActivatableItemRequestType> createSearchActivatableItemRequest(SearchActivatableItemRequestType value) {
        return new JAXBElement<SearchActivatableItemRequestType>(_SearchActivatableItemRequest_QNAME, SearchActivatableItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchActivatableItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "searchActivatableItemResponse")
    public JAXBElement<SearchActivatableItemResponseType> createSearchActivatableItemResponse(SearchActivatableItemResponseType value) {
        return new JAXBElement<SearchActivatableItemResponseType>(_SearchActivatableItemResponse_QNAME, SearchActivatableItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchEntitlementLineItemPropertiesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "searchEntitlementLineItemPropertiesRequest")
    public JAXBElement<SearchEntitlementLineItemPropertiesRequestType> createSearchEntitlementLineItemPropertiesRequest(SearchEntitlementLineItemPropertiesRequestType value) {
        return new JAXBElement<SearchEntitlementLineItemPropertiesRequestType>(_SearchEntitlementLineItemPropertiesRequest_QNAME, SearchEntitlementLineItemPropertiesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchEntitlementLineItemPropertiesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "searchEntitlementLineItemPropertiesResponse")
    public JAXBElement<SearchEntitlementLineItemPropertiesResponseType> createSearchEntitlementLineItemPropertiesResponse(SearchEntitlementLineItemPropertiesResponseType value) {
        return new JAXBElement<SearchEntitlementLineItemPropertiesResponseType>(_SearchEntitlementLineItemPropertiesResponse_QNAME, SearchEntitlementLineItemPropertiesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEntitlementCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getEntitlementCountRequest")
    public JAXBElement<GetEntitlementCountRequestType> createGetEntitlementCountRequest(GetEntitlementCountRequestType value) {
        return new JAXBElement<GetEntitlementCountRequestType>(_GetEntitlementCountRequest_QNAME, GetEntitlementCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEntitlementCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getEntitlementCountResponse")
    public JAXBElement<GetEntitlementCountResponseType> createGetEntitlementCountResponse(GetEntitlementCountResponseType value) {
        return new JAXBElement<GetEntitlementCountResponseType>(_GetEntitlementCountResponse_QNAME, GetEntitlementCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivatableItemCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getActivatableItemCountRequest")
    public JAXBElement<GetActivatableItemCountRequestType> createGetActivatableItemCountRequest(GetActivatableItemCountRequestType value) {
        return new JAXBElement<GetActivatableItemCountRequestType>(_GetActivatableItemCountRequest_QNAME, GetActivatableItemCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetActivatableItemCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getActivatableItemCountResponse")
    public JAXBElement<GetActivatableItemCountResponseType> createGetActivatableItemCountResponse(GetActivatableItemCountResponseType value) {
        return new JAXBElement<GetActivatableItemCountResponseType>(_GetActivatableItemCountResponse_QNAME, GetActivatableItemCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExactAvailableCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getExactAvailableCountRequest")
    public JAXBElement<GetExactAvailableCountRequestType> createGetExactAvailableCountRequest(GetExactAvailableCountRequestType value) {
        return new JAXBElement<GetExactAvailableCountRequestType>(_GetExactAvailableCountRequest_QNAME, GetExactAvailableCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetExactAvailableCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getExactAvailableCountResponse")
    public JAXBElement<GetExactAvailableCountResponseType> createGetExactAvailableCountResponse(GetExactAvailableCountResponseType value) {
        return new JAXBElement<GetExactAvailableCountResponseType>(_GetExactAvailableCountResponse_QNAME, GetExactAvailableCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetEntitlementStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setEntitlementStateRequest")
    public JAXBElement<SetEntitlementStateRequestType> createSetEntitlementStateRequest(SetEntitlementStateRequestType value) {
        return new JAXBElement<SetEntitlementStateRequestType>(_SetEntitlementStateRequest_QNAME, SetEntitlementStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetEntitlementStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setEntitlementStateResponse")
    public JAXBElement<SetEntitlementStateResponseType> createSetEntitlementStateResponse(SetEntitlementStateResponseType value) {
        return new JAXBElement<SetEntitlementStateResponseType>(_SetEntitlementStateResponse_QNAME, SetEntitlementStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWebRegKeyCountRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getWebRegKeyCountRequest")
    public JAXBElement<GetWebRegKeyCountRequestType> createGetWebRegKeyCountRequest(GetWebRegKeyCountRequestType value) {
        return new JAXBElement<GetWebRegKeyCountRequestType>(_GetWebRegKeyCountRequest_QNAME, GetWebRegKeyCountRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWebRegKeyCountResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getWebRegKeyCountResponse")
    public JAXBElement<GetWebRegKeyCountResponseType> createGetWebRegKeyCountResponse(GetWebRegKeyCountResponseType value) {
        return new JAXBElement<GetWebRegKeyCountResponseType>(_GetWebRegKeyCountResponse_QNAME, GetWebRegKeyCountResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWebRegKeysQueryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getWebRegKeysQueryRequest")
    public JAXBElement<GetWebRegKeysQueryRequestType> createGetWebRegKeysQueryRequest(GetWebRegKeysQueryRequestType value) {
        return new JAXBElement<GetWebRegKeysQueryRequestType>(_GetWebRegKeysQueryRequest_QNAME, GetWebRegKeysQueryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetWebRegKeysQueryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getWebRegKeysQueryResponse")
    public JAXBElement<GetWebRegKeysQueryResponseType> createGetWebRegKeysQueryResponse(GetWebRegKeysQueryResponseType value) {
        return new JAXBElement<GetWebRegKeysQueryResponseType>(_GetWebRegKeysQueryResponse_QNAME, GetWebRegKeysQueryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEntitlementAttributesRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getEntitlementAttributesRequest")
    public JAXBElement<GetEntitlementAttributesRequestType> createGetEntitlementAttributesRequest(GetEntitlementAttributesRequestType value) {
        return new JAXBElement<GetEntitlementAttributesRequestType>(_GetEntitlementAttributesRequest_QNAME, GetEntitlementAttributesRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetEntitlementAttributesResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getEntitlementAttributesResponse")
    public JAXBElement<GetEntitlementAttributesResponseType> createGetEntitlementAttributesResponse(GetEntitlementAttributesResponseType value) {
        return new JAXBElement<GetEntitlementAttributesResponseType>(_GetEntitlementAttributesResponse_QNAME, GetEntitlementAttributesResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenewEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "renewLicenseRequest")
    public JAXBElement<RenewEntitlementRequestType> createRenewLicenseRequest(RenewEntitlementRequestType value) {
        return new JAXBElement<RenewEntitlementRequestType>(_RenewLicenseRequest_QNAME, RenewEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RenewEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "renewLicenseResponse")
    public JAXBElement<RenewEntitlementResponseType> createRenewLicenseResponse(RenewEntitlementResponseType value) {
        return new JAXBElement<RenewEntitlementResponseType>(_RenewLicenseResponse_QNAME, RenewEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntitlementLifeCycleRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upgradeLicenseRequest")
    public JAXBElement<EntitlementLifeCycleRequestType> createUpgradeLicenseRequest(EntitlementLifeCycleRequestType value) {
        return new JAXBElement<EntitlementLifeCycleRequestType>(_UpgradeLicenseRequest_QNAME, EntitlementLifeCycleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntitlementLifeCycleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upgradeLicenseResponse")
    public JAXBElement<EntitlementLifeCycleResponseType> createUpgradeLicenseResponse(EntitlementLifeCycleResponseType value) {
        return new JAXBElement<EntitlementLifeCycleResponseType>(_UpgradeLicenseResponse_QNAME, EntitlementLifeCycleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntitlementLifeCycleRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upsellLicenseRequest")
    public JAXBElement<EntitlementLifeCycleRequestType> createUpsellLicenseRequest(EntitlementLifeCycleRequestType value) {
        return new JAXBElement<EntitlementLifeCycleRequestType>(_UpsellLicenseRequest_QNAME, EntitlementLifeCycleRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EntitlementLifeCycleResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "upsellLicenseResponse")
    public JAXBElement<EntitlementLifeCycleResponseType> createUpsellLicenseResponse(EntitlementLifeCycleResponseType value) {
        return new JAXBElement<EntitlementLifeCycleResponseType>(_UpsellLicenseResponse_QNAME, EntitlementLifeCycleResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MapEntitlementsToUserRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "mapEntitlementsToUserRequest")
    public JAXBElement<MapEntitlementsToUserRequestType> createMapEntitlementsToUserRequest(MapEntitlementsToUserRequestType value) {
        return new JAXBElement<MapEntitlementsToUserRequestType>(_MapEntitlementsToUserRequest_QNAME, MapEntitlementsToUserRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MapEntitlementsToUserResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "mapEntitlementsToUserResponse")
    public JAXBElement<MapEntitlementsToUserResponseType> createMapEntitlementsToUserResponse(MapEntitlementsToUserResponseType value) {
        return new JAXBElement<MapEntitlementsToUserResponseType>(_MapEntitlementsToUserResponse_QNAME, MapEntitlementsToUserResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailEntitlementRequest")
    public JAXBElement<EmailEntitlementRequestType> createEmailEntitlementRequest(EmailEntitlementRequestType value) {
        return new JAXBElement<EmailEntitlementRequestType>(_EmailEntitlementRequest_QNAME, EmailEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailEntitlementResponse")
    public JAXBElement<EmailEntitlementResponseType> createEmailEntitlementResponse(EmailEntitlementResponseType value) {
        return new JAXBElement<EmailEntitlementResponseType>(_EmailEntitlementResponse_QNAME, EmailEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailActivatableItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailActivatableItemRequest")
    public JAXBElement<EmailActivatableItemRequestType> createEmailActivatableItemRequest(EmailActivatableItemRequestType value) {
        return new JAXBElement<EmailActivatableItemRequestType>(_EmailActivatableItemRequest_QNAME, EmailActivatableItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailActivatableItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "emailActivatableItemResponse")
    public JAXBElement<EmailActivatableItemResponseType> createEmailActivatableItemResponse(EmailActivatableItemResponseType value) {
        return new JAXBElement<EmailActivatableItemResponseType>(_EmailActivatableItemResponse_QNAME, EmailActivatableItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetLineItemStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setLineItemStateRequest")
    public JAXBElement<SetLineItemStateRequestType> createSetLineItemStateRequest(SetLineItemStateRequestType value) {
        return new JAXBElement<SetLineItemStateRequestType>(_SetLineItemStateRequest_QNAME, SetLineItemStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetLineItemStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setLineItemStateResponse")
    public JAXBElement<SetLineItemStateResponseType> createSetLineItemStateResponse(SetLineItemStateResponseType value) {
        return new JAXBElement<SetLineItemStateResponseType>(_SetLineItemStateResponse_QNAME, SetLineItemStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetMaintenanceLineItemStateRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setMaintenanceLineItemStateRequest")
    public JAXBElement<SetMaintenanceLineItemStateRequestType> createSetMaintenanceLineItemStateRequest(SetMaintenanceLineItemStateRequestType value) {
        return new JAXBElement<SetMaintenanceLineItemStateRequestType>(_SetMaintenanceLineItemStateRequest_QNAME, SetMaintenanceLineItemStateRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetMaintenanceLineItemStateResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "setMaintenanceLineItemStateResponse")
    public JAXBElement<SetMaintenanceLineItemStateResponseType> createSetMaintenanceLineItemStateResponse(SetMaintenanceLineItemStateResponseType value) {
        return new JAXBElement<SetMaintenanceLineItemStateResponseType>(_SetMaintenanceLineItemStateResponse_QNAME, SetMaintenanceLineItemStateResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteWebRegKeyRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteWebRegKeyRequest")
    public JAXBElement<DeleteWebRegKeyRequestType> createDeleteWebRegKeyRequest(DeleteWebRegKeyRequestType value) {
        return new JAXBElement<DeleteWebRegKeyRequestType>(_DeleteWebRegKeyRequest_QNAME, DeleteWebRegKeyRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteWebRegKeyResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteWebRegKeyResponse")
    public JAXBElement<DeleteWebRegKeyResponseType> createDeleteWebRegKeyResponse(DeleteWebRegKeyResponseType value) {
        return new JAXBElement<DeleteWebRegKeyResponseType>(_DeleteWebRegKeyResponse_QNAME, DeleteWebRegKeyResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeEntitlementsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "mergeEntitlementsRequest")
    public JAXBElement<MergeEntitlementsRequestType> createMergeEntitlementsRequest(MergeEntitlementsRequestType value) {
        return new JAXBElement<MergeEntitlementsRequestType>(_MergeEntitlementsRequest_QNAME, MergeEntitlementsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeEntitlementsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "mergeEntitlementsResponse")
    public JAXBElement<MergeEntitlementsResponseType> createMergeEntitlementsResponse(MergeEntitlementsResponseType value) {
        return new JAXBElement<MergeEntitlementsResponseType>(_MergeEntitlementsResponse_QNAME, MergeEntitlementsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferEntitlementsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "transferEntitlementsRequest")
    public JAXBElement<TransferEntitlementsRequestType> createTransferEntitlementsRequest(TransferEntitlementsRequestType value) {
        return new JAXBElement<TransferEntitlementsRequestType>(_TransferEntitlementsRequest_QNAME, TransferEntitlementsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferEntitlementsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "transferEntitlementsResponse")
    public JAXBElement<TransferEntitlementsResponseType> createTransferEntitlementsResponse(TransferEntitlementsResponseType value) {
        return new JAXBElement<TransferEntitlementsResponseType>(_TransferEntitlementsResponse_QNAME, TransferEntitlementsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "transferLineItemsRequest")
    public JAXBElement<TransferLineItemsRequestType> createTransferLineItemsRequest(TransferLineItemsRequestType value) {
        return new JAXBElement<TransferLineItemsRequestType>(_TransferLineItemsRequest_QNAME, TransferLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "transferLineItemsResponse")
    public JAXBElement<TransferLineItemsResponseType> createTransferLineItemsResponse(TransferLineItemsResponseType value) {
        return new JAXBElement<TransferLineItemsResponseType>(_TransferLineItemsResponse_QNAME, TransferLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStateChangeHistoryRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getStateChangeHistoryRequest")
    public JAXBElement<GetStateChangeHistoryRequestType> createGetStateChangeHistoryRequest(GetStateChangeHistoryRequestType value) {
        return new JAXBElement<GetStateChangeHistoryRequestType>(_GetStateChangeHistoryRequest_QNAME, GetStateChangeHistoryRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStateChangeHistoryResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getStateChangeHistoryResponse")
    public JAXBElement<GetStateChangeHistoryResponseType> createGetStateChangeHistoryResponse(GetStateChangeHistoryResponseType value) {
        return new JAXBElement<GetStateChangeHistoryResponseType>(_GetStateChangeHistoryResponse_QNAME, GetStateChangeHistoryResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkMaintenanceLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "linkMaintenanceLineItemRequest")
    public JAXBElement<LinkMaintenanceLineItemRequestType> createLinkMaintenanceLineItemRequest(LinkMaintenanceLineItemRequestType value) {
        return new JAXBElement<LinkMaintenanceLineItemRequestType>(_LinkMaintenanceLineItemRequest_QNAME, LinkMaintenanceLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkMaintenanceLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "linkMaintenanceLineItemResponse")
    public JAXBElement<LinkMaintenanceLineItemResponseType> createLinkMaintenanceLineItemResponse(LinkMaintenanceLineItemResponseType value) {
        return new JAXBElement<LinkMaintenanceLineItemResponseType>(_LinkMaintenanceLineItemResponse_QNAME, LinkMaintenanceLineItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "splitLineItemRequest")
    public JAXBElement<SplitLineItemRequestType> createSplitLineItemRequest(SplitLineItemRequestType value) {
        return new JAXBElement<SplitLineItemRequestType>(_SplitLineItemRequest_QNAME, SplitLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "splitLineItemResponse")
    public JAXBElement<SplitLineItemResponseType> createSplitLineItemResponse(SplitLineItemResponseType value) {
        return new JAXBElement<SplitLineItemResponseType>(_SplitLineItemResponse_QNAME, SplitLineItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitBulkEntitlementRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "splitBulkEntitlementRequest")
    public JAXBElement<SplitBulkEntitlementRequestType> createSplitBulkEntitlementRequest(SplitBulkEntitlementRequestType value) {
        return new JAXBElement<SplitBulkEntitlementRequestType>(_SplitBulkEntitlementRequest_QNAME, SplitBulkEntitlementRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SplitBulkEntitlementResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "splitBulkEntitlementResponse")
    public JAXBElement<SplitBulkEntitlementResponseType> createSplitBulkEntitlementResponse(SplitBulkEntitlementResponseType value) {
        return new JAXBElement<SplitBulkEntitlementResponseType>(_SplitBulkEntitlementResponse_QNAME, SplitBulkEntitlementResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingLineItemsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getMatchingLineItemsRequest")
    public JAXBElement<GetMatchingLineItemsRequestType> createGetMatchingLineItemsRequest(GetMatchingLineItemsRequestType value) {
        return new JAXBElement<GetMatchingLineItemsRequestType>(_GetMatchingLineItemsRequest_QNAME, GetMatchingLineItemsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingLineItemsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getMatchingLineItemsResponse")
    public JAXBElement<GetMatchingLineItemsResponseType> createGetMatchingLineItemsResponse(GetMatchingLineItemsResponseType value) {
        return new JAXBElement<GetMatchingLineItemsResponseType>(_GetMatchingLineItemsResponse_QNAME, GetMatchingLineItemsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingBulkEntsRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getMatchingBulkEntsRequest")
    public JAXBElement<GetMatchingBulkEntsRequestType> createGetMatchingBulkEntsRequest(GetMatchingBulkEntsRequestType value) {
        return new JAXBElement<GetMatchingBulkEntsRequestType>(_GetMatchingBulkEntsRequest_QNAME, GetMatchingBulkEntsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMatchingBulkEntsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "getMatchingBulkEntsResponse")
    public JAXBElement<GetMatchingBulkEntsResponseType> createGetMatchingBulkEntsResponse(GetMatchingBulkEntsResponseType value) {
        return new JAXBElement<GetMatchingBulkEntsResponseType>(_GetMatchingBulkEntsResponse_QNAME, GetMatchingBulkEntsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteMaintenanceLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteMaintenanceLineItemRequest")
    public JAXBElement<DeleteMaintenanceLineItemRequestType> createDeleteMaintenanceLineItemRequest(DeleteMaintenanceLineItemRequestType value) {
        return new JAXBElement<DeleteMaintenanceLineItemRequestType>(_DeleteMaintenanceLineItemRequest_QNAME, DeleteMaintenanceLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeleteMaintenanceLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "deleteMaintenanceLineItemResponse")
    public JAXBElement<DeleteMaintenanceLineItemResponseType> createDeleteMaintenanceLineItemResponse(DeleteMaintenanceLineItemResponseType value) {
        return new JAXBElement<DeleteMaintenanceLineItemResponseType>(_DeleteMaintenanceLineItemResponse_QNAME, DeleteMaintenanceLineItemResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlinkMaintenanceLineItemRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "unlinkMaintenanceLineItemRequest")
    public JAXBElement<UnlinkMaintenanceLineItemRequestType> createUnlinkMaintenanceLineItemRequest(UnlinkMaintenanceLineItemRequestType value) {
        return new JAXBElement<UnlinkMaintenanceLineItemRequestType>(_UnlinkMaintenanceLineItemRequest_QNAME, UnlinkMaintenanceLineItemRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlinkMaintenanceLineItemResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:v1.webservices.operations.flexnet.com", name = "unlinkMaintenanceLineItemResponse")
    public JAXBElement<UnlinkMaintenanceLineItemResponseType> createUnlinkMaintenanceLineItemResponse(UnlinkMaintenanceLineItemResponseType value) {
        return new JAXBElement<UnlinkMaintenanceLineItemResponseType>(_UnlinkMaintenanceLineItemResponse_QNAME, UnlinkMaintenanceLineItemResponseType.class, null, value);
    }

}
