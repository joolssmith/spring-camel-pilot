
package com.flexnet.operations.webservices.g;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedUpdateMaintenanceDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedUpdateMaintenanceDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedMaintenance" type="{urn:com.macrovision:flexnet/operations}failedUpdateMaintenanceDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedUpdateMaintenanceDataListType", propOrder = {
    "failedMaintenance"
})
public class FailedUpdateMaintenanceDataListType {

    protected List<FailedUpdateMaintenanceDataType> failedMaintenance;

    /**
     * Gets the value of the failedMaintenance property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedMaintenance property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedMaintenance().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedUpdateMaintenanceDataType }
     * 
     * 
     */
    public List<FailedUpdateMaintenanceDataType> getFailedMaintenance() {
        if (failedMaintenance == null) {
            failedMaintenance = new ArrayList<FailedUpdateMaintenanceDataType>();
        }
        return this.failedMaintenance;
    }

}
