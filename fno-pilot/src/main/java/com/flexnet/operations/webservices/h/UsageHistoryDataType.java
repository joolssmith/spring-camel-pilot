
package com.flexnet.operations.webservices.h;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for usageHistoryDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="usageHistoryDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="updateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="serverIdentifier" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="deviceIdentifier" type="{urn:com.macrovision:flexnet/opsembedded}deviceIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="alias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="hostTypeName" type="{urn:com.macrovision:flexnet/opsembedded}hostTypeIdentifier" minOccurs="0"/&gt;
 *         &lt;element name="deviceServedStatus" type="{urn:com.macrovision:flexnet/opsembedded}deviceServedStatusType" minOccurs="0"/&gt;
 *         &lt;element name="featureData" type="{urn:com.macrovision:flexnet/opsembedded}featureDataDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="machineType" type="{urn:com.macrovision:flexnet/opsembedded}machineTypeType" minOccurs="0"/&gt;
 *         &lt;element name="vmName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vmInfo" type="{urn:com.macrovision:flexnet/opsembedded}dictionaryType" minOccurs="0"/&gt;
 *         &lt;element name="vendorDictionary" type="{urn:com.macrovision:flexnet/opsembedded}dictionaryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "usageHistoryDataType", propOrder = {
    "updateTime",
    "serverIdentifier",
    "deviceIdentifier",
    "alias",
    "hostTypeName",
    "deviceServedStatus",
    "featureData",
    "machineType",
    "vmName",
    "vmInfo",
    "vendorDictionary"
})
public class UsageHistoryDataType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateTime;
    protected DeviceIdentifier serverIdentifier;
    protected DeviceIdentifier deviceIdentifier;
    protected String alias;
    protected HostTypeIdentifier hostTypeName;
    @XmlSchemaType(name = "NMTOKEN")
    protected DeviceServedStatusType deviceServedStatus;
    protected List<FeatureDataDataType> featureData;
    @XmlSchemaType(name = "NMTOKEN")
    protected MachineTypeType machineType;
    protected String vmName;
    protected DictionaryType vmInfo;
    protected DictionaryType vendorDictionary;

    /**
     * Gets the value of the updateTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUpdateTime(XMLGregorianCalendar value) {
        this.updateTime = value;
    }

    /**
     * Gets the value of the serverIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getServerIdentifier() {
        return serverIdentifier;
    }

    /**
     * Sets the value of the serverIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setServerIdentifier(DeviceIdentifier value) {
        this.serverIdentifier = value;
    }

    /**
     * Gets the value of the deviceIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceIdentifier }
     *     
     */
    public DeviceIdentifier getDeviceIdentifier() {
        return deviceIdentifier;
    }

    /**
     * Sets the value of the deviceIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceIdentifier }
     *     
     */
    public void setDeviceIdentifier(DeviceIdentifier value) {
        this.deviceIdentifier = value;
    }

    /**
     * Gets the value of the alias property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    /**
     * Gets the value of the hostTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public HostTypeIdentifier getHostTypeName() {
        return hostTypeName;
    }

    /**
     * Sets the value of the hostTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link HostTypeIdentifier }
     *     
     */
    public void setHostTypeName(HostTypeIdentifier value) {
        this.hostTypeName = value;
    }

    /**
     * Gets the value of the deviceServedStatus property.
     * 
     * @return
     *     possible object is
     *     {@link DeviceServedStatusType }
     *     
     */
    public DeviceServedStatusType getDeviceServedStatus() {
        return deviceServedStatus;
    }

    /**
     * Sets the value of the deviceServedStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeviceServedStatusType }
     *     
     */
    public void setDeviceServedStatus(DeviceServedStatusType value) {
        this.deviceServedStatus = value;
    }

    /**
     * Gets the value of the featureData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the featureData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFeatureData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeatureDataDataType }
     * 
     * 
     */
    public List<FeatureDataDataType> getFeatureData() {
        if (featureData == null) {
            featureData = new ArrayList<FeatureDataDataType>();
        }
        return this.featureData;
    }

    /**
     * Gets the value of the machineType property.
     * 
     * @return
     *     possible object is
     *     {@link MachineTypeType }
     *     
     */
    public MachineTypeType getMachineType() {
        return machineType;
    }

    /**
     * Sets the value of the machineType property.
     * 
     * @param value
     *     allowed object is
     *     {@link MachineTypeType }
     *     
     */
    public void setMachineType(MachineTypeType value) {
        this.machineType = value;
    }

    /**
     * Gets the value of the vmName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVmName() {
        return vmName;
    }

    /**
     * Sets the value of the vmName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVmName(String value) {
        this.vmName = value;
    }

    /**
     * Gets the value of the vmInfo property.
     * 
     * @return
     *     possible object is
     *     {@link DictionaryType }
     *     
     */
    public DictionaryType getVmInfo() {
        return vmInfo;
    }

    /**
     * Sets the value of the vmInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DictionaryType }
     *     
     */
    public void setVmInfo(DictionaryType value) {
        this.vmInfo = value;
    }

    /**
     * Gets the value of the vendorDictionary property.
     * 
     * @return
     *     possible object is
     *     {@link DictionaryType }
     *     
     */
    public DictionaryType getVendorDictionary() {
        return vendorDictionary;
    }

    /**
     * Sets the value of the vendorDictionary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DictionaryType }
     *     
     */
    public void setVendorDictionary(DictionaryType value) {
        this.vendorDictionary = value;
    }

}
