
package com.flexnet.operations.webservices.y;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteMaintenanceLineItemDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteMaintenanceLineItemDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="maintenanceLineItemIdentifier" type="{urn:v3.webservices.operations.flexnet.com}entitlementLineItemIdentifierType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteMaintenanceLineItemDataType", propOrder = {
    "maintenanceLineItemIdentifier"
})
public class DeleteMaintenanceLineItemDataType {

    @XmlElement(required = true)
    protected EntitlementLineItemIdentifierType maintenanceLineItemIdentifier;

    /**
     * Gets the value of the maintenanceLineItemIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public EntitlementLineItemIdentifierType getMaintenanceLineItemIdentifier() {
        return maintenanceLineItemIdentifier;
    }

    /**
     * Sets the value of the maintenanceLineItemIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemIdentifierType }
     *     
     */
    public void setMaintenanceLineItemIdentifier(EntitlementLineItemIdentifierType value) {
        this.maintenanceLineItemIdentifier = value;
    }

}
