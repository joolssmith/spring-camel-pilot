
package com.flexnet.operations.webservices.z;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for machineTypeType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="machineTypeType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="UNKNOWN"/&gt;
 *     &lt;enumeration value="PHYSICAL"/&gt;
 *     &lt;enumeration value="VIRTUAL"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "machineTypeType")
@XmlEnum
public enum MachineTypeType {

    UNKNOWN,
    PHYSICAL,
    VIRTUAL;

    public String value() {
        return name();
    }

    public static MachineTypeType fromValue(String v) {
        return valueOf(v);
    }

}
