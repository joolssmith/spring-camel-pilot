
package com.flexnet.operations.webservices.k;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CollectionOperationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CollectionOperationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN"&gt;
 *     &lt;enumeration value="REPLACE"/&gt;
 *     &lt;enumeration value="ADD"/&gt;
 *     &lt;enumeration value="DELETE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CollectionOperationType")
@XmlEnum
public enum CollectionOperationType {

    REPLACE,
    ADD,
    DELETE;

    public String value() {
        return name();
    }

    public static CollectionOperationType fromValue(String v) {
        return valueOf(v);
    }

}
