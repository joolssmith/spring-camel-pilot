
package com.flexnet.operations.webservices.f;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for failedRelateOrganizationsDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="failedRelateOrganizationsDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="failedOrgData" type="{urn:com.macrovision:flexnet/operations}failedRelateOrganizationsDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "failedRelateOrganizationsDataListType", propOrder = {
    "failedOrgData"
})
public class FailedRelateOrganizationsDataListType {

    protected List<FailedRelateOrganizationsDataType> failedOrgData;

    /**
     * Gets the value of the failedOrgData property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the failedOrgData property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFailedOrgData().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FailedRelateOrganizationsDataType }
     * 
     * 
     */
    public List<FailedRelateOrganizationsDataType> getFailedOrgData() {
        if (failedOrgData == null) {
            failedOrgData = new ArrayList<FailedRelateOrganizationsDataType>();
        }
        return this.failedOrgData;
    }

}
