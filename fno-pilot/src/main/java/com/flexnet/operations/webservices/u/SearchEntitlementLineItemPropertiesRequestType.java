
package com.flexnet.operations.webservices.u;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for searchEntitlementLineItemPropertiesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="searchEntitlementLineItemPropertiesRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="queryParams" type="{urn:v2.webservices.operations.flexnet.com}searchActivatableItemDataType"/&gt;
 *         &lt;element name="entitlementLineItemResponseConfig" type="{urn:v2.webservices.operations.flexnet.com}entitlementLineItemResponseConfigRequestType"/&gt;
 *         &lt;element name="batchSize" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="pageNumber" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "searchEntitlementLineItemPropertiesRequestType", propOrder = {
    "queryParams",
    "entitlementLineItemResponseConfig",
    "batchSize",
    "pageNumber"
})
public class SearchEntitlementLineItemPropertiesRequestType {

    @XmlElement(required = true)
    protected SearchActivatableItemDataType queryParams;
    @XmlElement(required = true)
    protected EntitlementLineItemResponseConfigRequestType entitlementLineItemResponseConfig;
    @XmlElement(required = true)
    protected BigInteger batchSize;
    protected BigInteger pageNumber;

    /**
     * Gets the value of the queryParams property.
     * 
     * @return
     *     possible object is
     *     {@link SearchActivatableItemDataType }
     *     
     */
    public SearchActivatableItemDataType getQueryParams() {
        return queryParams;
    }

    /**
     * Sets the value of the queryParams property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchActivatableItemDataType }
     *     
     */
    public void setQueryParams(SearchActivatableItemDataType value) {
        this.queryParams = value;
    }

    /**
     * Gets the value of the entitlementLineItemResponseConfig property.
     * 
     * @return
     *     possible object is
     *     {@link EntitlementLineItemResponseConfigRequestType }
     *     
     */
    public EntitlementLineItemResponseConfigRequestType getEntitlementLineItemResponseConfig() {
        return entitlementLineItemResponseConfig;
    }

    /**
     * Sets the value of the entitlementLineItemResponseConfig property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitlementLineItemResponseConfigRequestType }
     *     
     */
    public void setEntitlementLineItemResponseConfig(EntitlementLineItemResponseConfigRequestType value) {
        this.entitlementLineItemResponseConfig = value;
    }

    /**
     * Gets the value of the batchSize property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBatchSize() {
        return batchSize;
    }

    /**
     * Sets the value of the batchSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBatchSize(BigInteger value) {
        this.batchSize = value;
    }

    /**
     * Gets the value of the pageNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPageNumber() {
        return pageNumber;
    }

    /**
     * Sets the value of the pageNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPageNumber(BigInteger value) {
        this.pageNumber = value;
    }

}
