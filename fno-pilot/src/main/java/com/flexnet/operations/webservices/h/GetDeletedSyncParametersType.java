
package com.flexnet.operations.webservices.h;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getDeletedSyncParametersType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getDeletedSyncParametersType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="startTime" type="{urn:com.macrovision:flexnet/opsembedded}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="endTime" type="{urn:com.macrovision:flexnet/opsembedded}DateTimeQueryType" minOccurs="0"/&gt;
 *         &lt;element name="serverUniqueId" type="{urn:com.macrovision:flexnet/opsembedded}ExternalIdQueryType" minOccurs="0"/&gt;
 *         &lt;element name="serverId" type="{urn:com.macrovision:flexnet/opsembedded}SimpleQueryType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDeletedSyncParametersType", propOrder = {
    "startTime",
    "endTime",
    "serverUniqueId",
    "serverId"
})
public class GetDeletedSyncParametersType {

    protected DateTimeQueryType startTime;
    protected DateTimeQueryType endTime;
    protected ExternalIdQueryType serverUniqueId;
    protected SimpleQueryType serverId;

    /**
     * Gets the value of the startTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getStartTime() {
        return startTime;
    }

    /**
     * Sets the value of the startTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setStartTime(DateTimeQueryType value) {
        this.startTime = value;
    }

    /**
     * Gets the value of the endTime property.
     * 
     * @return
     *     possible object is
     *     {@link DateTimeQueryType }
     *     
     */
    public DateTimeQueryType getEndTime() {
        return endTime;
    }

    /**
     * Sets the value of the endTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateTimeQueryType }
     *     
     */
    public void setEndTime(DateTimeQueryType value) {
        this.endTime = value;
    }

    /**
     * Gets the value of the serverUniqueId property.
     * 
     * @return
     *     possible object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public ExternalIdQueryType getServerUniqueId() {
        return serverUniqueId;
    }

    /**
     * Sets the value of the serverUniqueId property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExternalIdQueryType }
     *     
     */
    public void setServerUniqueId(ExternalIdQueryType value) {
        this.serverUniqueId = value;
    }

    /**
     * Gets the value of the serverId property.
     * 
     * @return
     *     possible object is
     *     {@link SimpleQueryType }
     *     
     */
    public SimpleQueryType getServerId() {
        return serverId;
    }

    /**
     * Sets the value of the serverId property.
     * 
     * @param value
     *     allowed object is
     *     {@link SimpleQueryType }
     *     
     */
    public void setServerId(SimpleQueryType value) {
        this.serverId = value;
    }

}
