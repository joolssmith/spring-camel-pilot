
package com.flexnet.operations.webservices.x;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for createdFeatureBundleDataListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="createdFeatureBundleDataListType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="createdFeatureBundle" type="{urn:v2.webservices.operations.flexnet.com}correlationDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createdFeatureBundleDataListType", propOrder = {
    "createdFeatureBundle"
})
public class CreatedFeatureBundleDataListType {

    protected List<CorrelationDataType> createdFeatureBundle;

    /**
     * Gets the value of the createdFeatureBundle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the createdFeatureBundle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCreatedFeatureBundle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorrelationDataType }
     * 
     * 
     */
    public List<CorrelationDataType> getCreatedFeatureBundle() {
        if (createdFeatureBundle == null) {
            createdFeatureBundle = new ArrayList<CorrelationDataType>();
        }
        return this.createdFeatureBundle;
    }

}
