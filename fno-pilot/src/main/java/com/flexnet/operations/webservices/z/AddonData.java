
package com.flexnet.operations.webservices.z;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for addonData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addonData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="activationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="entitlementId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="requestedCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="consumedCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="generatedCopies" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="licenseState" type="{urn:v3.fne.webservices.operations.flexnet.com}licenseStateType" minOccurs="0"/&gt;
 *         &lt;element name="productList" type="{urn:v3.fne.webservices.operations.flexnet.com}entitledProductDataListType" minOccurs="0"/&gt;
 *         &lt;element name="partNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="licenseExpirationDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="licensePermanent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="expirationDateOverride" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="licenseModelName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addonData", propOrder = {
    "activationId",
    "entitlementId",
    "requestedCopies",
    "consumedCopies",
    "generatedCopies",
    "licenseState",
    "productList",
    "partNumber",
    "licenseExpirationDate",
    "licensePermanent",
    "expirationDateOverride",
    "licenseModelName"
})
public class AddonData {

    protected String activationId;
    protected String entitlementId;
    protected BigInteger requestedCopies;
    protected BigInteger consumedCopies;
    protected BigInteger generatedCopies;
    @XmlSchemaType(name = "NMTOKEN")
    protected LicenseStateType licenseState;
    protected EntitledProductDataListType productList;
    protected String partNumber;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar licenseExpirationDate;
    protected Boolean licensePermanent;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar expirationDateOverride;
    protected String licenseModelName;

    /**
     * Gets the value of the activationId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActivationId() {
        return activationId;
    }

    /**
     * Sets the value of the activationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActivationId(String value) {
        this.activationId = value;
    }

    /**
     * Gets the value of the entitlementId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEntitlementId() {
        return entitlementId;
    }

    /**
     * Sets the value of the entitlementId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEntitlementId(String value) {
        this.entitlementId = value;
    }

    /**
     * Gets the value of the requestedCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRequestedCopies() {
        return requestedCopies;
    }

    /**
     * Sets the value of the requestedCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRequestedCopies(BigInteger value) {
        this.requestedCopies = value;
    }

    /**
     * Gets the value of the consumedCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getConsumedCopies() {
        return consumedCopies;
    }

    /**
     * Sets the value of the consumedCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setConsumedCopies(BigInteger value) {
        this.consumedCopies = value;
    }

    /**
     * Gets the value of the generatedCopies property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getGeneratedCopies() {
        return generatedCopies;
    }

    /**
     * Sets the value of the generatedCopies property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setGeneratedCopies(BigInteger value) {
        this.generatedCopies = value;
    }

    /**
     * Gets the value of the licenseState property.
     * 
     * @return
     *     possible object is
     *     {@link LicenseStateType }
     *     
     */
    public LicenseStateType getLicenseState() {
        return licenseState;
    }

    /**
     * Sets the value of the licenseState property.
     * 
     * @param value
     *     allowed object is
     *     {@link LicenseStateType }
     *     
     */
    public void setLicenseState(LicenseStateType value) {
        this.licenseState = value;
    }

    /**
     * Gets the value of the productList property.
     * 
     * @return
     *     possible object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public EntitledProductDataListType getProductList() {
        return productList;
    }

    /**
     * Sets the value of the productList property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntitledProductDataListType }
     *     
     */
    public void setProductList(EntitledProductDataListType value) {
        this.productList = value;
    }

    /**
     * Gets the value of the partNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartNumber() {
        return partNumber;
    }

    /**
     * Sets the value of the partNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartNumber(String value) {
        this.partNumber = value;
    }

    /**
     * Gets the value of the licenseExpirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getLicenseExpirationDate() {
        return licenseExpirationDate;
    }

    /**
     * Sets the value of the licenseExpirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setLicenseExpirationDate(XMLGregorianCalendar value) {
        this.licenseExpirationDate = value;
    }

    /**
     * Gets the value of the licensePermanent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLicensePermanent() {
        return licensePermanent;
    }

    /**
     * Sets the value of the licensePermanent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLicensePermanent(Boolean value) {
        this.licensePermanent = value;
    }

    /**
     * Gets the value of the expirationDateOverride property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDateOverride() {
        return expirationDateOverride;
    }

    /**
     * Sets the value of the expirationDateOverride property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDateOverride(XMLGregorianCalendar value) {
        this.expirationDateOverride = value;
    }

    /**
     * Gets the value of the licenseModelName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLicenseModelName() {
        return licenseModelName;
    }

    /**
     * Sets the value of the licenseModelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLicenseModelName(String value) {
        this.licenseModelName = value;
    }

}
