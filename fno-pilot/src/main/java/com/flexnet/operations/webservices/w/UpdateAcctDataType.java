
package com.flexnet.operations.webservices.w;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for updateAcctDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="updateAcctDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="account" type="{urn:v2.webservices.operations.flexnet.com}accountIdentifierType"/&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="address" type="{urn:v2.webservices.operations.flexnet.com}addressDataType" minOccurs="0"/&gt;
 *         &lt;element name="subAccounts" type="{urn:v2.webservices.operations.flexnet.com}updateSubAccountsListType" minOccurs="0"/&gt;
 *         &lt;element name="relatedAccounts" type="{urn:v2.webservices.operations.flexnet.com}updateRelatedAccountsListType" minOccurs="0"/&gt;
 *         &lt;element name="visible" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="customAttributes" type="{urn:v2.webservices.operations.flexnet.com}attributeDescriptorDataType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateAcctDataType", propOrder = {
    "account",
    "id",
    "name",
    "description",
    "address",
    "subAccounts",
    "relatedAccounts",
    "visible",
    "customAttributes"
})
public class UpdateAcctDataType {

    @XmlElement(required = true)
    protected AccountIdentifierType account;
    protected String id;
    protected String name;
    protected String description;
    protected AddressDataType address;
    protected UpdateSubAccountsListType subAccounts;
    protected UpdateRelatedAccountsListType relatedAccounts;
    protected Boolean visible;
    protected AttributeDescriptorDataType customAttributes;

    /**
     * Gets the value of the account property.
     * 
     * @return
     *     possible object is
     *     {@link AccountIdentifierType }
     *     
     */
    public AccountIdentifierType getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountIdentifierType }
     *     
     */
    public void setAccount(AccountIdentifierType value) {
        this.account = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * @return
     *     possible object is
     *     {@link AddressDataType }
     *     
     */
    public AddressDataType getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressDataType }
     *     
     */
    public void setAddress(AddressDataType value) {
        this.address = value;
    }

    /**
     * Gets the value of the subAccounts property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateSubAccountsListType }
     *     
     */
    public UpdateSubAccountsListType getSubAccounts() {
        return subAccounts;
    }

    /**
     * Sets the value of the subAccounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateSubAccountsListType }
     *     
     */
    public void setSubAccounts(UpdateSubAccountsListType value) {
        this.subAccounts = value;
    }

    /**
     * Gets the value of the relatedAccounts property.
     * 
     * @return
     *     possible object is
     *     {@link UpdateRelatedAccountsListType }
     *     
     */
    public UpdateRelatedAccountsListType getRelatedAccounts() {
        return relatedAccounts;
    }

    /**
     * Sets the value of the relatedAccounts property.
     * 
     * @param value
     *     allowed object is
     *     {@link UpdateRelatedAccountsListType }
     *     
     */
    public void setRelatedAccounts(UpdateRelatedAccountsListType value) {
        this.relatedAccounts = value;
    }

    /**
     * Gets the value of the visible property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVisible() {
        return visible;
    }

    /**
     * Sets the value of the visible property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVisible(Boolean value) {
        this.visible = value;
    }

    /**
     * Gets the value of the customAttributes property.
     * 
     * @return
     *     possible object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public AttributeDescriptorDataType getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets the value of the customAttributes property.
     * 
     * @param value
     *     allowed object is
     *     {@link AttributeDescriptorDataType }
     *     
     */
    public void setCustomAttributes(AttributeDescriptorDataType value) {
        this.customAttributes = value;
    }

}
