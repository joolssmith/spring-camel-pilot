
package com.flexnet.operations.webservices.u;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for featureBundleStateChangeDataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="featureBundleStateChangeDataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="featureBundleIdentifier" type="{urn:v2.webservices.operations.flexnet.com}featureBundleIdentifierType"/&gt;
 *         &lt;element name="stateChangeRecord" type="{urn:v2.webservices.operations.flexnet.com}stateChangeDataType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "featureBundleStateChangeDataType", propOrder = {
    "featureBundleIdentifier",
    "stateChangeRecord"
})
public class FeatureBundleStateChangeDataType {

    @XmlElement(required = true)
    protected FeatureBundleIdentifierType featureBundleIdentifier;
    protected List<StateChangeDataType> stateChangeRecord;

    /**
     * Gets the value of the featureBundleIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public FeatureBundleIdentifierType getFeatureBundleIdentifier() {
        return featureBundleIdentifier;
    }

    /**
     * Sets the value of the featureBundleIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link FeatureBundleIdentifierType }
     *     
     */
    public void setFeatureBundleIdentifier(FeatureBundleIdentifierType value) {
        this.featureBundleIdentifier = value;
    }

    /**
     * Gets the value of the stateChangeRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stateChangeRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStateChangeRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StateChangeDataType }
     * 
     * 
     */
    public List<StateChangeDataType> getStateChangeRecord() {
        if (stateChangeRecord == null) {
            stateChangeRecord = new ArrayList<StateChangeDataType>();
        }
        return this.stateChangeRecord;
    }

}
