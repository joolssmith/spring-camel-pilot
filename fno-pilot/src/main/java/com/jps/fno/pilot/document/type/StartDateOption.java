package com.jps.fno.pilot.document.type;

public enum StartDateOption {
  DEFINE_NOW, DEFINE_AT_FIRST_ACTIVATION, DEFINE_AT_EACH_ACTIVATION
}
