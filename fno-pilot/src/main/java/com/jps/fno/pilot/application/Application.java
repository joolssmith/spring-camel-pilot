package com.jps.fno.pilot.application;

import java.io.FileReader;
import java.io.Reader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.jps.fno.pilot.document.Document;
import com.jps.fno.pilot.document.OrderDocument;
import com.jps.fno.pilot.document.type.DocumentType;
import com.jps.fno.pilot.document.type.Duration;
import com.jps.fno.pilot.document.type.DurationUnit;
import com.jps.fno.pilot.document.type.OrderItem;
import com.jps.fno.pilot.document.type.StartDateOption;
import com.jps.fno.pilot.document.type.Term;

@SpringBootApplication
@EnableScheduling
@Component
public class Application extends Loggable {

  private static Application instance;

  private final LinkedBlockingQueue<JsonNode> nodeQueue = new LinkedBlockingQueue<>();

  private volatile ApplicationState state = ApplicationState.INACTIVE;

  private WatchService watchService;

  private final ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory())
      .configure(SerializationFeature.INDENT_OUTPUT, true).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
      .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
      .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
      .configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false)
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  private final ObjectMapper jsonMapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true)
      .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
      .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
      .configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false)
      .configure(DeserializationFeature.FAIL_ON_UNRESOLVED_OBJECT_IDS, false)
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  private final Map<DocumentType, DocumentProcessor> documentProcessors = new HashMap<>();

  @Autowired
  private ApplicationConfiguration config;

  @Autowired
  private com.flexnet.operations.webservices.h.ManageDeviceServiceInterface manageDeviceService;

  @Autowired
  private com.flexnet.operations.webservices.p.ManageDeviceServiceInterfaceV1 manageDeviceServiceV1;

  @Autowired
  private com.flexnet.operations.webservices.v.ManageDeviceServiceInterfaceV2 manageDeviceServiceV2;

  @Autowired
  private com.flexnet.operations.webservices.z.ManageDeviceServiceInterfaceV3 manageDeviceServiceV3;

  @Autowired
  private com.flexnet.operations.webservices.k.EntitlementOrderServiceInterface entitlementOrderService;

  @Autowired
  private com.flexnet.operations.webservices.o.EntitlementOrderServiceInterfaceV1 entitlementOrderServicev1;

  @Autowired
  private com.flexnet.operations.webservices.u.EntitlementOrderServiceInterfaceV2 entitlementOrderServicev2;

  @Autowired
  private com.flexnet.operations.webservices.y.EntitlementOrderServiceInterfaceV3 entitlementOrderServicev3;

  @Autowired
  private com.flexnet.operations.webservices.g.ProductPackagingServiceInterface productPackagingService;

  @Autowired
  private com.flexnet.operations.webservices.r.ProductPackagingServiceInterfaceV1 productPackagingServiceV1;

  @Autowired
  private com.flexnet.operations.webservices.x.ProductPackagingServiceInterfaceV2 productPackagingServiceV2;

  @Autowired
  private com.flexnet.operations.webservices.w.UserAcctHierarchyServiceInterfaceV2 userAccountHierarchyServiceV2;

  @SuppressWarnings("unused")
  private void dumpYamlTest() throws JsonProcessingException {
    final OrderDocument order = new OrderDocument() {
      {
        this.Order_ID = "8769876876876";
        this.Purchase_Order = "9768768768";
        this.Sold_To = "ACC-100";
        this.Items = new OrderItem[] { new OrderItem() {
          {
            this.Item_Number = "1010";
            this.Material_Number = "93286987326487";
            this.Description = "Blah blah blah";
            this.Quantity = 1;
            this.Term = new Term() {
              {
                this.ExpirationDate = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS));
                this.StartDate = java.sql.Date.valueOf(LocalDate.now());
                this.DateOption = StartDateOption.DEFINE_NOW;
              }
            };
          }
        }, new OrderItem() {
          {
            this.Item_Number = "1010";
            this.Material_Number = "93286987326487";
            this.Description = "Blah blah blah";
            this.Quantity = 1;
            this.Term = new Term() {
              {
                this.ExpirationDate = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS));
                this.StartDate = java.sql.Date.valueOf(LocalDate.now());
                this.DateOption = StartDateOption.DEFINE_AT_FIRST_ACTIVATION;
              }
            };
          }
        }, new OrderItem() {
          {
            this.Item_Number = "1030";
            this.Material_Number = "523236574365846";
            this.Description = "Blah blah blah";
            this.Quantity = 1;
            this.Term = new Term() {
              {
                this.Duration = new Duration() {
                  {
                    this.Length = 1;
                    this.Unit = DurationUnit.MONTHS;
                  }
                };
                this.ExpirationDate = java.sql.Date.valueOf(LocalDate.now().plus(1, ChronoUnit.MONTHS));
                this.StartDate = java.sql.Date.valueOf(LocalDate.now());
                this.DateOption = StartDateOption.DEFINE_AT_EACH_ACTIVATION;
              }
            };
          }
        } };
      }
    };

    this.logger.debug(this.yamlMapper.writeValueAsString(order));
    this.logger.debug(this.jsonMapper.writeValueAsString(order));
  }

  private void logMe() {
    this.logger.debug("Aplication {} {}", this.getClass().getName(), hashCode());
  }

  public static Application getInstance() {
    return Application.instance;
  }

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  public Application() {
    in();
    try {
      logMe();
      Application.instance = this;
    }
    finally {
      out();
    }
  }

  @PostConstruct
  public void postConstruct() {
    in();
    try {
      logMe();
      this.state = ApplicationState.STARTING;
    }
    finally {
      out();
    }
  }

  @PreDestroy
  public void preDestroy() {
    in();
    try {
      logMe();
    }
    finally {
      out();
    }
  }

  @Scheduled(fixedDelay = 10, initialDelay = 3000)
  public void loadYaml() {
    in();
    try {
      if (this.state == ApplicationState.RUNNING) {

        WatchKey key = null;

        while ((key = this.watchService.poll(1, TimeUnit.MINUTES)) != null) {

          final Path directory = Path.class.cast(key.watchable());

          for (final WatchEvent<?> event : key.pollEvents()) {

            final Path path = directory.resolve(Path.class.cast(event.context()));

            this.logger.debug("{} -> {}", event.kind(), path.toString());

            if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {

              do {
                try (final Reader stream = new FileReader(path.toFile())) {
                  this.logger.debug("queueing document...");
                  this.nodeQueue.put(this.yamlMapper.readTree(stream));
                  break;
                }
                catch (final Exception e) {
                  try {
                    Thread.sleep(50);
                  }
                  catch (final InterruptedException e1) {
                  }
                }
              }
              while (true);

              Files.delete(path);
            }
            else {
              this.logger.debug("ignoring update");
            }
          }
          this.logger.debug("resetting key");
          key.reset();
        }
      }
    }
    catch (final InterruptedException t) {
      /**
       * this is expected on shutdown...
       */
      this.logger.warn("shutdown detected");
    }
    catch (final Throwable t) {
      this.logger.error("OOPS", t);
    }
    finally {
      out();
    }
  }

  @Scheduled(fixedDelay = 10)
  public void processYaml() {
    in();
    try {

      JsonNode node = null;

      while ((node = this.nodeQueue.poll(1, TimeUnit.MINUTES)) != null) {

        this.logger.debug("dequeueing document...");

        final Document doc = this.yamlMapper.treeToValue(node, Document.class);

        final Transaction transaction = this.documentProcessors.get(doc.Type).process(node);
        
        super.logger.debug("transaction {}", this.jsonMapper.writeValueAsString(transaction));
      }
    }
    catch (final InterruptedException t) {
      /**
       * this is expected on shutdown...
       */
      this.logger.warn("shutdown detected");
    }
    catch (final Throwable t) {
      this.logger.error("OOPS", t);
    }
    finally {
      out();
    }
  }

  public ApplicationConfiguration getApplicationConfiguration() {
    return this.config;
  }

  @Bean
  public CommandLineRunner initialize(ApplicationContext ctx) {
    return args -> {
      try {
        in();
        logMe();

        this.watchService = FileSystems.getDefault().newWatchService();

        Paths.get(this.config.getImportFilepath()).register(this.watchService, StandardWatchEventKinds.ENTRY_CREATE,
            StandardWatchEventKinds.ENTRY_DELETE);

        // dumpYamlTest();

        this.logger.debug("getting processor beans...");
        for (final DocumentProcessor entry : ctx.getBeansOfType(DocumentProcessor.class).values()) {
          this.logger.debug("{}", entry.getClass().getName());

          this.documentProcessors.put(entry.getDocumentType(), entry);
        }

        this.state = ApplicationState.RUNNING;
      }
      catch (final Exception ex) {
        this.logger.error("Error", ex);
      }
      finally {
        out();
      }
    };
  }

  /**
   * GETTERS
   */
  public ObjectMapper getJsonMapper() {
    return this.jsonMapper;
  }

  public ObjectMapper getYamlMapper() {
    return this.yamlMapper;
  }

  public ApplicationConfiguration getConfig() {
    return this.config;
  }

  public LinkedBlockingQueue<JsonNode> getNodeQueue() {
    return this.nodeQueue;
  }

  public ApplicationState getState() {
    return this.state;
  }

  public com.flexnet.operations.webservices.h.ManageDeviceServiceInterface getManageDeviceService() {
    return this.manageDeviceService;
  }

  public com.flexnet.operations.webservices.p.ManageDeviceServiceInterfaceV1 getManageDeviceServiceV1() {
    return this.manageDeviceServiceV1;
  }

  public com.flexnet.operations.webservices.v.ManageDeviceServiceInterfaceV2 getManageDeviceServiceV2() {
    return this.manageDeviceServiceV2;
  }

  public com.flexnet.operations.webservices.z.ManageDeviceServiceInterfaceV3 getManageDeviceServiceV3() {
    return this.manageDeviceServiceV3;
  }

  public com.flexnet.operations.webservices.k.EntitlementOrderServiceInterface getEntitlementOrderService() {
    return this.entitlementOrderService;
  }

  public com.flexnet.operations.webservices.o.EntitlementOrderServiceInterfaceV1 getEntitlementOrderServicev1() {
    return this.entitlementOrderServicev1;
  }

  public com.flexnet.operations.webservices.u.EntitlementOrderServiceInterfaceV2 getEntitlementOrderServicev2() {
    return this.entitlementOrderServicev2;
  }

  public com.flexnet.operations.webservices.y.EntitlementOrderServiceInterfaceV3 getEntitlementOrderServicev3() {
    return this.entitlementOrderServicev3;
  }

  public com.flexnet.operations.webservices.g.ProductPackagingServiceInterface getProductPackagingService() {
    return this.productPackagingService;
  }

  public com.flexnet.operations.webservices.r.ProductPackagingServiceInterfaceV1 getProductPackagingServiceV1() {
    return this.productPackagingServiceV1;
  }

  public com.flexnet.operations.webservices.x.ProductPackagingServiceInterfaceV2 getProductPackagingServiceV2() {
    return this.productPackagingServiceV2;
  }

  public com.flexnet.operations.webservices.w.UserAcctHierarchyServiceInterfaceV2 getUserAccountHierarchyServiceV2() {
    return this.userAccountHierarchyServiceV2;
  }

}
