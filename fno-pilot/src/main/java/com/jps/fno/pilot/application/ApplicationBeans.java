package com.jps.fno.pilot.application;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

@Configuration
public class ApplicationBeans extends Loggable {
  private void setupBinding(final Object port, final QName endpoint) {
    final BindingProvider prov = (BindingProvider) port;

    prov.getRequestContext().put(BindingProvider.USERNAME_PROPERTY,
        Application.getInstance().getApplicationConfiguration().getFnoUserName());

    prov.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY,
        Application.getInstance().getApplicationConfiguration().getFnoPassword());

    prov.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
        Application.getInstance().getApplicationConfiguration().getFnoHostName() + endpoint.getLocalPart());
  }

  @Bean
  public ThreadPoolTaskScheduler beanThreadPoolTaskScheduler() {
    in();
    try {
      final ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
      threadPoolTaskScheduler.setPoolSize(4);
      return threadPoolTaskScheduler;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.z.ManageDeviceServiceInterfaceV3 getManageDeviceServiceInterfaceV3() {
    in();
    try {
      final com.flexnet.operations.webservices.z.ManageDeviceServiceInterfaceV3 service = new com.flexnet.operations.webservices.z.V3_002fManageDeviceService()
          .getPort(com.flexnet.operations.webservices.z.ManageDeviceServiceInterfaceV3.class);

      setupBinding(service, com.flexnet.operations.webservices.z.V3_002fManageDeviceService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.v.ManageDeviceServiceInterfaceV2 getManageDeviceServiceInterfaceV2() {
    in();
    try {
      final com.flexnet.operations.webservices.v.ManageDeviceServiceInterfaceV2 service = new com.flexnet.operations.webservices.v.V2_002fManageDeviceService()
          .getPort(com.flexnet.operations.webservices.v.ManageDeviceServiceInterfaceV2.class);

      setupBinding(service, com.flexnet.operations.webservices.v.V2_002fManageDeviceService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.p.ManageDeviceServiceInterfaceV1 getManageDeviceServiceInterfaceV1() {
    in();
    try {
      final com.flexnet.operations.webservices.p.ManageDeviceServiceInterfaceV1 service = new com.flexnet.operations.webservices.p.V1_002fManageDeviceService()
          .getPort(com.flexnet.operations.webservices.p.ManageDeviceServiceInterfaceV1.class);

      setupBinding(service, com.flexnet.operations.webservices.p.V1_002fManageDeviceService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.h.ManageDeviceServiceInterface getManageDeviceServiceInterface() {
    in();
    try {
      final com.flexnet.operations.webservices.h.ManageDeviceServiceInterface service = new com.flexnet.operations.webservices.h.ManageDeviceService()
          .getPort(com.flexnet.operations.webservices.h.ManageDeviceServiceInterface.class);

      setupBinding(service, com.flexnet.operations.webservices.h.ManageDeviceService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.k.EntitlementOrderServiceInterface getEntitlementOrderServiceInterface() {
    in();
    try {
      final com.flexnet.operations.webservices.k.EntitlementOrderServiceInterface service = new com.flexnet.operations.webservices.k.EntitlementOrderService()
          .getPort(com.flexnet.operations.webservices.k.EntitlementOrderServiceInterface.class);

      setupBinding(service, com.flexnet.operations.webservices.k.EntitlementOrderService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.o.EntitlementOrderServiceInterfaceV1 getEntitlementOrderServiceInterfaceV1() {
    in();
    try {
      final com.flexnet.operations.webservices.o.EntitlementOrderServiceInterfaceV1 service = new com.flexnet.operations.webservices.o.V1_002fEntitlementOrderService()
          .getPort(com.flexnet.operations.webservices.o.EntitlementOrderServiceInterfaceV1.class);

      setupBinding(service, com.flexnet.operations.webservices.o.V1_002fEntitlementOrderService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.u.EntitlementOrderServiceInterfaceV2 getEntitlementOrderServiceInterfaceV2() {
    in();
    try {
      final com.flexnet.operations.webservices.u.EntitlementOrderServiceInterfaceV2 service = new com.flexnet.operations.webservices.u.V2_002fEntitlementOrderService()
          .getPort(com.flexnet.operations.webservices.u.EntitlementOrderServiceInterfaceV2.class);

      setupBinding(service, com.flexnet.operations.webservices.u.V2_002fEntitlementOrderService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.y.EntitlementOrderServiceInterfaceV3 getEntitlementOrderServiceInterfaceV3() {
    in();
    try {
      final com.flexnet.operations.webservices.y.EntitlementOrderServiceInterfaceV3 service = new com.flexnet.operations.webservices.y.V3_002fEntitlementOrderService()
          .getPort(com.flexnet.operations.webservices.y.EntitlementOrderServiceInterfaceV3.class);

      setupBinding(service, com.flexnet.operations.webservices.y.V3_002fEntitlementOrderService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.g.ProductPackagingServiceInterface getProductPackagingServiceInterface() {
    in();
    try {
      final com.flexnet.operations.webservices.g.ProductPackagingServiceInterface service = new com.flexnet.operations.webservices.g.ProductPackagingService()
          .getPort(com.flexnet.operations.webservices.g.ProductPackagingServiceInterface.class);

      setupBinding(service, com.flexnet.operations.webservices.g.ProductPackagingService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.r.ProductPackagingServiceInterfaceV1 getProductPackagingServiceInterfaceV1() {
    in();
    try {
      final com.flexnet.operations.webservices.r.ProductPackagingServiceInterfaceV1 service = new com.flexnet.operations.webservices.r.V1_002fProductPackagingService()
          .getPort(com.flexnet.operations.webservices.r.ProductPackagingServiceInterfaceV1.class);

      setupBinding(service, com.flexnet.operations.webservices.r.V1_002fProductPackagingService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.x.ProductPackagingServiceInterfaceV2 getProductPackagingServiceInterfaceV2() {
    in();
    try {
      final com.flexnet.operations.webservices.x.ProductPackagingServiceInterfaceV2 service = new com.flexnet.operations.webservices.x.V2_002fProductPackagingService()
          .getPort(com.flexnet.operations.webservices.x.ProductPackagingServiceInterfaceV2.class);

      setupBinding(service, com.flexnet.operations.webservices.x.V2_002fProductPackagingService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }

  @Bean
  public com.flexnet.operations.webservices.w.UserAcctHierarchyServiceInterfaceV2 getUserAcctHierarchyServiceInterfaceV2() {
    in();
    try {
      final com.flexnet.operations.webservices.w.UserAcctHierarchyServiceInterfaceV2 service = new com.flexnet.operations.webservices.w.V2_002fUserAcctHierarchyService()
          .getPort(com.flexnet.operations.webservices.w.UserAcctHierarchyServiceInterfaceV2.class);

      setupBinding(service, com.flexnet.operations.webservices.w.V2_002fUserAcctHierarchyService.SERVICE);

      return service;
    }
    finally {
      out();
    }
  }
}
