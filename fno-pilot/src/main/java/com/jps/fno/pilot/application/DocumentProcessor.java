package com.jps.fno.pilot.application;

import com.fasterxml.jackson.databind.JsonNode;
import com.jps.fno.pilot.document.type.DocumentType;

public interface DocumentProcessor {

  DocumentType getDocumentType();

  Transaction process(JsonNode document) throws Exception;
}
