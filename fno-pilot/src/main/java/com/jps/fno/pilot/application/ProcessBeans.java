package com.jps.fno.pilot.application;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.JsonNode;
import com.jps.fno.pilot.document.AccountDocument;
import com.jps.fno.pilot.document.OrderDocument;
import com.jps.fno.pilot.document.type.DocumentType;

@Configuration
public class ProcessBeans extends Loggable {

  @Bean
  public DocumentProcessor orderProcessor() {

    return new DocumentProcessor() {

      @Override
      public Transaction process(final JsonNode node) throws Exception {


        final OrderDocument document = Application.getInstance().getYamlMapper().treeToValue(node, OrderDocument.class);

        final String yaml = Application.getInstance().getYamlMapper().writeValueAsString(node);

        logger.warn("{} {}", getDocumentType(), yaml);

        return new Transaction() {
          {
            this.request = document;
          }
        };
      }

      @Override
      public DocumentType getDocumentType() {
        return DocumentType.OrderDocument;
      }
    };
  }

  @Bean
  public DocumentProcessor accountProcessor() {

    return new DocumentProcessor() {

      @Override
      public Transaction process(final JsonNode node) throws Exception {

        final AccountDocument document = Application.getInstance().getYamlMapper().treeToValue(node,
            AccountDocument.class);

        final String yaml = Application.getInstance().getYamlMapper().writeValueAsString(node);

        logger.warn("{} {}", getDocumentType(), yaml);

        return new Transaction() {
          {
            this.request = document;
          }
        };
      }

      @Override
      public DocumentType getDocumentType() {
        return DocumentType.AccountDocument;
      }
    };
  }
}
