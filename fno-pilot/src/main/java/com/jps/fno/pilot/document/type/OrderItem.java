package com.jps.fno.pilot.document.type;

public class OrderItem {
  public String Item_Number;
  public String Material_Number;
  public String Description;
  public int Quantity;
  public Term Term;
}