package com.jps.fno.pilot.application;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration {

  @Value("${fno.username}")
  private String fnoUserName;

  @Value("${fno.password}")
  private String fnoPassword;

  @Value("${fno.hostname}")
  private String fnoHostname;

  @Value("${fno.domain}")
  private String fnoDomain;

  @Value("${fno.batchsize}")
  private String fnoBatchSize;

  @Value("${import.filepath}")
  private String importFilepath;

  public ApplicationConfiguration() {
  }

  public String getFnoUserName() {
    return this.fnoUserName;
  }

  public String getFnoPassword() {
    return this.fnoPassword;
  }

  public String getFnoHostName() {
    return this.fnoHostname;
  }

  public String getFnoDomain() {
    return this.fnoDomain;
  }

  public long getFnoBatchSize() {
    return Long.valueOf(this.fnoBatchSize);
  }

  public String getImportFilepath() {
    return this.importFilepath;
  }

}
