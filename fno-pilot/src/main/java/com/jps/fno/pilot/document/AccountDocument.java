package com.jps.fno.pilot.document;

import com.jps.fno.pilot.document.type.DocumentType;

public class AccountDocument extends Document {

  public String Account_Id;
  public String Account_Name;
  public String City;
  public String Region;
  public String PostCode;
  public String State;
  public String[] Address;

  public AccountDocument() {
    super.Type = DocumentType.AccountDocument;
  }
}
