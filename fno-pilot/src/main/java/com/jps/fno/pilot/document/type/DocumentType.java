package com.jps.fno.pilot.document.type;

public enum DocumentType {
  OrderDocument, AccountDocument
}
