package com.jps.fno.pilot.document;

import com.jps.fno.pilot.document.type.DocumentType;
import com.jps.fno.pilot.document.type.OrderItem;

public class OrderDocument extends Document {
  public String Order_ID;
  public String Purchase_Order;
  public String Sold_To;
  public OrderItem[] Items;

  public OrderDocument() {
    super.Type = DocumentType.OrderDocument;
  }
}
