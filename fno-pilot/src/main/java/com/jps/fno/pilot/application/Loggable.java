package com.jps.fno.pilot.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Loggable {
  protected final Logger logger = LoggerFactory.getLogger(this.getClass());

  public Logger getLogger() {
    return this.logger;
  }

  public void in() {
    logger.debug("==> {}", Thread.currentThread().getStackTrace()[2].getMethodName());
  }

  public void out() {
    logger.debug("<== {}", Thread.currentThread().getStackTrace()[2].getMethodName());
  }
  
  public void exception(final Throwable t) {
    logger.error(Thread.currentThread().getStackTrace()[2].getMethodName(), t);
  }
}
