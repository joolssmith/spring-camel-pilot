package com.jps.fno.pilot.document.type;

import java.util.Date;

public class Term {
  public Date StartDate;
  public StartDateOption DateOption;
  public Date ExpirationDate;
  public Duration Duration;
  public Boolean Permanent;
}
