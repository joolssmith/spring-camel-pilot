package com.jps.fno.pilot.document.type;

public enum DurationUnit {
  DAYS, WEEKS, MONTHS, YEARS
}
