package com.jps.fno.pilot.application;

public enum ApplicationState {
  INACTIVE, STARTING, RUNNING, SHUTTING_DOWN, ERROR
}
