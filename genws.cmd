@echo off

call wsdl2java.bat -p com.flexnet.operations.webservices.a https://flex1113-uat.flexnetoperations.com/flexnet/services/EntitlementServiceSOAP?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.b https://flex1113-uat.flexnetoperations.com/flexnet/services/UsageService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.c https://flex1113-uat.flexnetoperations.com/flexnet/services/Version?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.d https://flex1113-uat.flexnetoperations.com/flexnet/services/FlexnetAuthentication?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.e https://flex1113-uat.flexnetoperations.com/flexnet/services/TestService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.f https://flex1113-uat.flexnetoperations.com/flexnet/services/UserOrgHierarchyService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.g https://flex1113-uat.flexnetoperations.com/flexnet/services/ProductPackagingService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.h https://flex1113-uat.flexnetoperations.com/flexnet/services/ManageDeviceService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.i https://flex1113-uat.flexnetoperations.com/flexnet/services/LicenseService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.j https://flex1113-uat.flexnetoperations.com/flexnet/services/AdminService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.k https://flex1113-uat.flexnetoperations.com/flexnet/services/EntitlementOrderService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.l https://flex1113-uat.flexnetoperations.com/flexnet/services/UserAcctHierarchyService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.m https://flex1113-uat.flexnetoperations.com/flexnet/services/ActivationService?wsdl

call wsdl2java.bat -p com.flexnet.operations.webservices.n https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/LicenseService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.o https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/EntitlementOrderService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.p https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/ManageDeviceService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.q https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/UserOrgHierarchyService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.r https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/ProductPackagingService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.s https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/UsageService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.t https://flex1113-uat.flexnetoperations.com/flexnet/services/v1/FlexnetAuthentication?wsdl

call wsdl2java.bat -p com.flexnet.operations.webservices.u https://flex1113-uat.flexnetoperations.com/flexnet/services/v2/EntitlementOrderService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.v https://flex1113-uat.flexnetoperations.com/flexnet/services/v2/ManageDeviceService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.w https://flex1113-uat.flexnetoperations.com/flexnet/services/v2/UserAcctHierarchyService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.x https://flex1113-uat.flexnetoperations.com/flexnet/services/v2/ProductPackagingService?wsdl

call wsdl2java.bat -p com.flexnet.operations.webservices.y https://flex1113-uat.flexnetoperations.com/flexnet/services/v3/EntitlementOrderService?wsdl
call wsdl2java.bat -p com.flexnet.operations.webservices.z https://flex1113-uat.flexnetoperations.com/flexnet/services/v3/ManageDeviceService?wsdl